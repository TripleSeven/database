import unitypack, os, json
from lib import listFiles

PATH=os.path.dirname(os.path.realpath(__file__))
PATH_ASSETS=os.path.join(PATH,'assets')

def MapAssetTypes(originFolder):
	MAP={}
	for fasset in listFiles(originFolder, False):
		with open(os.path.join(originFolder,fasset),'rb') as fh:
			bundle = unitypack.load(fh)
			MAP[fasset]=[]
			for asset in bundle.assets:
				for id, obj in asset.objects.items():
					try:
						data=obj.read()
						name = getattr(data,'name',None)
						MAP[fasset].append((name,obj.type))
					except:
						MAP[fasset].append(('Failed_To_Read',obj.type))

					
	open(os.path.join(originFolder,'MappedAssets.json'),'wb').write(json.dumps(MAP,indent='\t', ensure_ascii=False).encode('utf8'))

MapAssetTypes(os.path.join(PATH_ASSETS,*['raw_global','ExportAssetBundle']))
MapAssetTypes(os.path.join(PATH_ASSETS,*['raw_china','ExportAssetBundle']))