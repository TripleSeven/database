
from urllib.request import Request,urlopen  #page download
import unitypack
import io
import os
import json
from ConvertAssets import modifiedPath, processUnityFile

PATH = os.path.dirname(os.path.realpath(__file__))
PATH_ASSETS=os.path.join(PATH,'assets')

def download(url):
    req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
    return urlopen(req).read()

def main():
	####	global
	#AssetDownloader('global',"http://us-clientupdate.zlongame.com/USMHMNZ/UpdateClientServerURL_android_d.txt", 'Android', 'bundledataandroid_asset.b')
	AssetDownloader('global',"http://us-clientupdate.zlongame.com/USMHMNZ/UpdateClientServerURL_pc.txt", 'Android', 'bundledataandroid_asset.b')
                              
	####	china
	#AssetDownloader('china',"http://clientupdate.zlongame.com/MHMNZ/UpdateClientServerURL_pc.txt",	"StandaloneWindows", 'bundledatastandalonewindows_asset.b')
	AssetDownloader('china',"http://clientupdate.zlongame.com/MHMNZ/UpdateClientServerURL_ty.txt",	"StandaloneWindows", 'bundledatastandalonewindows_asset.b')

def AssetDownloader(name,url_client_update, dtype, bundledata):
	path_raw = os.path.join(PATH_ASSETS,'raw_%s'%name)
	path_dest = os.path.join(PATH_ASSETS,name)
	#get link for latest version
	url_update,url_update_bak = download(url_client_update).decode('utf8').split(',')
	# USMHMNZ/android_d/android_serverList.txt ~ server list

	# normal assets
	#Android/assets_gameproject_runtimeassets_bundledataandroid_asset.b -> all normal assets in ExportAssetBundle
	os.makedirs(os.path.join(path_raw,*['ExportAssetBundle']), exist_ok=True)
	try:
		bundledata = download('%s/%s/%s'%(url_update,dtype, bundledata))
	except:
		url_update=url_update_bak[:-1]
		bundledata = download('%s/%s/%s'%(url_update,dtype,bundledata))
		
	f=io.BytesIO(bundledata)
	f.name= "bundledataandroid"
	bundle = unitypack.load(f)
	for asset in bundle.assets:
		for id, object in asset.objects.items():
			if object.type == 'BundleData':
				data = object.read()
				
				with open('BundleData.json', 'wb') as fh:
					fh.write(json.dumps(data['m_bundleList'], indent='\t', ensure_ascii=False).encode('utf8'))

				for item in data['m_bundleList']:
					# check if it has to be updated
					a_path = os.path.join(path_raw,*['ExportAssetBundle',item['m_bundleName']])
					if not os.path.isfile(a_path) or os.path.getsize(a_path) != item['m_size']:
						print(a_path)
						dl_asset = download('%s/%s/%s'%(url_update,dtype,item['m_bundleName']))
						open(a_path,'wb').write(dl_asset)

						#extract content from new asset
						destFolder = os.path.join(path_dest,'ExportAssetBundle')
						processUnityFile(dl_asset,modifiedPath(destFolder,item['m_bundleName']),destFolder)
	# music
	#http://us-mhmnzupdate.zlongame.com/USMHMNZ/ClientPatch/201902141855/CRIRes/AllFilesPath.txt.md5
	os.makedirs(os.path.join(path_raw,*['CRIRes']), exist_ok=True)
	crilist = download(url_update+'/CRIRes/AllFilesPath.txt').decode('utf8')
	with open(os.path.join(path_raw,*['CRIRes','ServerAllFilesPath.txt']),'wt',encoding='utf8') as f:
		f.write(crilist)

	for line in crilist.split('\n'):
		if not line:
			continue
		fp,checksum,fsize=line.split(',')
		#Breakless_Work_0/Action.acb,33306B6B833A89FD4ADC90BA02A2320A,39552
		a_path = os.path.join(path_raw,*['CRIRes',*fp.split('/')])
		if not os.path.isfile(a_path) or os.path.getsize(a_path) != int(fsize):
			print(a_path)
			os.makedirs(os.path.dirname(a_path),exist_ok=True)
			dl_asset = download(url_update + '/CRIRes/' + fp)
			open(a_path,'wb').write(dl_asset)

if __name__ == '__main__':
	main()