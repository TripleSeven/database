from PIL import Image
import os, json, unitypack
from ConvertAssets import modifiedPath, listFiles

PATH=os.path.dirname(os.path.realpath(__file__))

def main(version):
	originFolder=os.path.join(PATH,*['assets','raw_%s'%version,'ExportAssetBundle'])
	destFolder  =os.path.join(PATH,*['assets','%s'%version,'ExportAssetBundle'])
	#FixImages('global')
	#FixImages('china')
	FixImages('images')
	MAP={}
	for fasset in listFiles(originFolder, False):
		with open(os.path.join(originFolder,fasset),'rb') as fh:
			lpath = modifiedPath(destFolder,fasset,fullPath=True)

			#	map asset
			try:
				bundle = unitypack.load(fh)
				print(fh)
			except:
				print('Can\'t load %s'%fh)

			cobjs={'NONAME':[]}
			for asset in bundle.assets:
				try:
					for id, obj in asset.objects.items():
						try:
							data=obj.read()
							name = getattr(data,'name',None)
							cobjs[name]=obj.type
						except:
							cobjs['NONAME'].append((id,obj.type))
				except:	#runtime error
					pass
					
			#	alpha images in there?
			imgPairs={'Texture2D':[],'Sprite':[]}
			for key,ktyp in cobjs.items():
				#correct typ?
				if ktyp not in ['Texture2D','Sprite']:
					continue
				#alpha image?
				for add in ['_a','_Alpha']:
					if key+add in cobjs:
						imgPairs[ktyp].append((key,key+add))

			#	texture2d assets which require a fix
			if imgPairs['Texture2D']:
				#	apply alpha img
				for img,alp in imgPairs['Texture2D']:
					path_i = os.path.join(lpath,'%s.png')
					ApplyAlphaImage(path_i%img,path_i%alp)
				#	reextracting the sprites
				for asset in bundle.assets:
					try:
						for id, obj in asset.objects.items():
							if obj.type != 'Sprite':
								continue
							sprite = obj.read()
							texture_path = os.path.join(
								lpath,
								sprite.rd['texture'].object.read().name+'.png'
							)
							sprite.image(Image.open(texture_path)).save(os.path.join(lpath,sprite.name))
					except:
						pass

			#	sprites
			if imgPairs['Sprite']:
				#	apply alpha img
				for img,alp in imgPairs['Texture2D']:
					path_i = os.path.join(lpath,'%s.png')
					ApplyAlphaImage(path_i%img,path_i%alp)

		MAP[fasset]=cobjs
	open('MappedAssets%s.json'%version,'wb').write(json.dumps(MAP,ensure_ascii=False,indent='\t').encode('utf8'))

def ApplyAlphaImage(target,alpha):
	try:
		#print(target)
		# Alpha -> IMG images
		t=Image.open(target)
		a=Image.open(alpha).resize((t.width, t.height))
		channels=(*t.split()[:3],a.getchannel('R'))
		Image.merge("RGBA", channels).save(target[:-4]+'.png')
	except Exception as e:
		print(e)
def FixFolder(folder,alpha_name):
	alpha=os.path.join(folder,alpha_name)
	for img in os.listdir(folder):
		if 'SpriteAtlas' not in img and img != alpha_name:
			ApplyAlphaImage(os.path.join(folder,img),alpha)

def FixImages(version):
	print(version)
	dest_path=os.path.join(PATH,*['assets',version,'ExportAssetBundle'])
	raw_path=os.path.join(PATH,*['assets','raw_%s'%version,'ExportAssetBundle'])

	#	mass fix
	icon_path=os.path.join(dest_path,*['ui','icon'])
	for folder in ['drill','card']:
		FixFolder(os.path.join(icon_path,folder),'%sIcon_a.png'%(folder.title()))

	#	Hero SKin
	skin_path=os.path.join(icon_path,'heroskin')
	icon_alpha=os.path.join(skin_path,'HeroSkinIcon_a.png')
	skin_alpha=os.path.join(skin_path,'HeroSkin_a.png')
	for img in os.listdir(skin_path):
		typ=img.split('_',1)[0]	#
		if typ == 'Icon':
			ApplyAlphaImage(os.path.join(skin_path,img),icon_alpha)
		elif typ == 'Skin':
			ApplyAlphaImage(os.path.join(skin_path,img),skin_alpha)

	#	Skills and Talents
	skill_path=os.path.join(dest_path,*['ui','icon','skill'])
	skill_alpha=os.path.join(skill_path,'SkillIcon_a.png')
	talent_alpha=os.path.join(skill_path,'TalentSkillIcon_a.png')
	for img in os.listdir(skill_path):
		typ=img.split('_',1)[0]	#
		if typ in ['Passive','Skill','SuperBuff']:
			ApplyAlphaImage(os.path.join(skill_path,img),skill_alpha)
		elif typ in ['Gift','Command']:
			ApplyAlphaImage(os.path.join(skill_path,img),talent_alpha)

	#	Soldierskin
	FixFolder(os.path.join(icon_path,'soldierskin'),'SoldierSkinIcon_a.png')

	return

	assets = json.loads(open(os.path.join(raw_path,'MappedAssets.json'),'rb').read())
	####	ALPHA & IMG COMBINATION
	for aname,objs in assets.items():
		try:
			names=[]
			fuse=[]
			fuse2=[]
			#	try to find alpha + img combination
			for (name,typ) in objs:
				names.append(name)
				if type(name) == str and typ == "Texture2D":
					if ('_Img' == name[-4:] or '_Alpha' == name[-6:]) :
						fuse.append(name)
					elif name[-2:]=='_a':
						fuse2.append(name)
			fuse = [name for name in fuse if '_Img' == name[-4:] and name[:-4]+'_Alpha' in fuse]
			fuse2 = [name for name in fuse2 if name[:-2] in names]

			l_path=os.path.join(dest_path,*[modifiedPath(dest_path,aname)])
			if not os.path.isdir(l_path):
				l_path=os.path.join(dest_path,*[modifiedPath(dest_path,aname)[1:]])
			# fuse them
			for name in fuse2:
				fp = os.path.join(l_path,name[:-2]+'.png')
				if not os.path.isfile(fp):
					continue
				print(fp)
				# Alpha -> IMG images
				ApplyAlphaImage(fp,fp[:-4]+'_a.png')
			for name in fuse:
				fp = os.path.join(l_path,name+'.png')
				if not os.path.isfile(fp):
					continue
				print(fp)
				# Alpha -> IMG images
				ApplyAlphaImage(fp,fp[:-8]+'_Alpha.png')
			#	reprocess the sprites of the package
			if fuse:
				bundle = open(os.path.join(raw_path,aname),'rb')
				bundle = unitypack.load(bundle)
				for asset in bundle.assets:
					for id, obj in asset.objects.items():
						if obj.type != 'Sprite':
							continue
						sprite = obj.read()
						texture_path = os.path.join(
							l_path,
							sprite.rd['texture'].object.read().name+'.png'
						)
						sprite.image(Image.open(texture_path)).save(os.path.join(l_path,sprite.name))
		except Exception as e:
			print(e)

if __name__ == '__main__':
	main('global')
