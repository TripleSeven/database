# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataRiftChapterInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataRiftChapterInfo.proto',
  package='ConfigDataRiftChapterInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1f\x43onfigDataRiftChapterInfo.proto\x12\x19\x43onfigDataRiftChapterInfo\"\x89\x01\n\"SUBRiftChapterInfoUnlockConditions\x12T\n\rConditionType\x18\x01 \x02(\x0e\x32=.ConfigDataRiftChapterInfo.ENUMRiftChapterUnlockConditionType\x12\r\n\x05Param\x18\x02 \x02(\x05\"b\n\x08SUBGoods\x12;\n\tGoodsType\x18\x01 \x02(\x0e\x32(.ConfigDataRiftChapterInfo.ENUMGoodsType\x12\n\n\x02Id\x18\x02 \x02(\x05\x12\r\n\x05\x43ount\x18\x03 \x02(\x05\"\xb3\x04\n\x19\x43onfigDataRiftChapterInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Name\x18\x03 \x02(\t\x12\x11\n\tTitleName\x18\x05 \x02(\t\x12\x0c\n\x04\x44\x65sc\x18\x06 \x02(\t\x12\r\n\x05Place\x18\x08 \x02(\t\x12W\n\x10UnlockConditions\x18\n \x03(\x0b\x32=.ConfigDataRiftChapterInfo.SUBRiftChapterInfoUnlockConditions\x12\x0c\n\x04Hard\x18\x0b \x02(\x05\x12\x13\n\x0bHardChapter\x18\x0c \x02(\x05\x12\x15\n\rRiftLevels_ID\x18\r \x03(\x05\x12\x13\n\x0bReward1Star\x18\x0e \x02(\x05\x12\x34\n\x07Reward1\x18\x0f \x03(\x0b\x32#.ConfigDataRiftChapterInfo.SUBGoods\x12\x13\n\x0bReward2Star\x18\x10 \x02(\x05\x12\x34\n\x07Reward2\x18\x11 \x03(\x0b\x32#.ConfigDataRiftChapterInfo.SUBGoods\x12\x13\n\x0bReward3Star\x18\x12 \x02(\x05\x12\x34\n\x07Reward3\x18\x13 \x03(\x0b\x32#.ConfigDataRiftChapterInfo.SUBGoods\x12\r\n\x05Image\x18\x14 \x02(\t\x12\x1b\n\x13\x43hapterBGPrefabName\x18\x15 \x02(\t\x12\x10\n\x08IsOpened\x18\x16 \x02(\x08\x12\x1a\n\x12StoryOutlineInfoID\x18\x17 \x02(\x05\"L\n\x05Items\x12\x43\n\x05items\x18\x01 \x03(\x0b\x32\x34.ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo*\xda\x01\n\"ENUMRiftChapterUnlockConditionType\x12\'\n#RiftChapterUnlockConditionType_None\x10\x00\x12.\n*RiftChapterUnlockConditionType_PlayerLevel\x10\x01\x12+\n\'RiftChapterUnlockConditionType_Scenario\x10\x02\x12.\n*RiftChapterUnlockConditionType_ChapterStar\x10\x03*\xf9\x05\n\rENUMGoodsType\x12\x12\n\x0eGoodsType_None\x10\x00\x12\x12\n\x0eGoodsType_Gold\x10\x01\x12\x15\n\x11GoodsType_Crystal\x10\x02\x12\x14\n\x10GoodsType_Energy\x10\x03\x12\x12\n\x0eGoodsType_Hero\x10\x04\x12\x19\n\x15GoodsType_JobMaterial\x10\x05\x12\x12\n\x0eGoodsType_Item\x10\x06\x12\x17\n\x13GoodsType_Equipment\x10\x07\x12\x19\n\x15GoodsType_ArenaTicket\x10\x08\x12\x19\n\x15GoodsType_ArenaHonour\x10\t\x12\x17\n\x13GoodsType_PlayerExp\x10\n\x12(\n$GoodsType_TrainingGroundTechMaterial\x10\x0b\x12\x1e\n\x1aGoodsType_FriendshipPoints\x10\x0c\x12\x1a\n\x16GoodsType_EnchantStone\x10\r\x12\x17\n\x13GoodsType_MonthCard\x10\x0e\x12\x17\n\x13GoodsType_HeadFrame\x10\x0f\x12\x16\n\x12GoodsType_HeroSkin\x10\x10\x12\x19\n\x15GoodsType_SoldierSkin\x10\x11\x12\x18\n\x14GoodsType_SkinTicket\x10\x12\x12\x1e\n\x1aGoodsType_RealTimePVPHonor\x10\x13\x12\x1b\n\x17GoodsType_MemoryEssence\x10\x14\x12\x1a\n\x16GoodsType_MithralStone\x10\x15\x12$\n GoodsType_BrillianceMithralStone\x10\x16\x12\x18\n\x14GoodsType_GuildMedal\x10\x17\x12\x1c\n\x18GoodsType_ChallengePoint\x10\x18\x12\x1a\n\x16GoodsType_FashionPoint\x10\x19\x12\x13\n\x0fGoodsType_Title\x10\x1a\x12\x1b\n\x17GoodsType_RefineryStone\x10\x1b')
)

_ENUMRIFTCHAPTERUNLOCKCONDITIONTYPE = _descriptor.EnumDescriptor(
  name='ENUMRiftChapterUnlockConditionType',
  full_name='ConfigDataRiftChapterInfo.ENUMRiftChapterUnlockConditionType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='RiftChapterUnlockConditionType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RiftChapterUnlockConditionType_PlayerLevel', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RiftChapterUnlockConditionType_Scenario', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='RiftChapterUnlockConditionType_ChapterStar', index=3, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=947,
  serialized_end=1165,
)
_sym_db.RegisterEnumDescriptor(_ENUMRIFTCHAPTERUNLOCKCONDITIONTYPE)

ENUMRiftChapterUnlockConditionType = enum_type_wrapper.EnumTypeWrapper(_ENUMRIFTCHAPTERUNLOCKCONDITIONTYPE)
_ENUMGOODSTYPE = _descriptor.EnumDescriptor(
  name='ENUMGoodsType',
  full_name='ConfigDataRiftChapterInfo.ENUMGoodsType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GoodsType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Gold', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Crystal', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Energy', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Hero', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_JobMaterial', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Item', index=6, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Equipment', index=7, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaTicket', index=8, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaHonour', index=9, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_PlayerExp', index=10, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_TrainingGroundTechMaterial', index=11, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FriendshipPoints', index=12, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_EnchantStone', index=13, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MonthCard', index=14, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeadFrame', index=15, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeroSkin', index=16, number=16,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SoldierSkin', index=17, number=17,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SkinTicket', index=18, number=18,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RealTimePVPHonor', index=19, number=19,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MemoryEssence', index=20, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MithralStone', index=21, number=21,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_BrillianceMithralStone', index=22, number=22,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_GuildMedal', index=23, number=23,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ChallengePoint', index=24, number=24,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FashionPoint', index=25, number=25,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Title', index=26, number=26,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RefineryStone', index=27, number=27,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1168,
  serialized_end=1929,
)
_sym_db.RegisterEnumDescriptor(_ENUMGOODSTYPE)

ENUMGoodsType = enum_type_wrapper.EnumTypeWrapper(_ENUMGOODSTYPE)
RiftChapterUnlockConditionType_None = 0
RiftChapterUnlockConditionType_PlayerLevel = 1
RiftChapterUnlockConditionType_Scenario = 2
RiftChapterUnlockConditionType_ChapterStar = 3
GoodsType_None = 0
GoodsType_Gold = 1
GoodsType_Crystal = 2
GoodsType_Energy = 3
GoodsType_Hero = 4
GoodsType_JobMaterial = 5
GoodsType_Item = 6
GoodsType_Equipment = 7
GoodsType_ArenaTicket = 8
GoodsType_ArenaHonour = 9
GoodsType_PlayerExp = 10
GoodsType_TrainingGroundTechMaterial = 11
GoodsType_FriendshipPoints = 12
GoodsType_EnchantStone = 13
GoodsType_MonthCard = 14
GoodsType_HeadFrame = 15
GoodsType_HeroSkin = 16
GoodsType_SoldierSkin = 17
GoodsType_SkinTicket = 18
GoodsType_RealTimePVPHonor = 19
GoodsType_MemoryEssence = 20
GoodsType_MithralStone = 21
GoodsType_BrillianceMithralStone = 22
GoodsType_GuildMedal = 23
GoodsType_ChallengePoint = 24
GoodsType_FashionPoint = 25
GoodsType_Title = 26
GoodsType_RefineryStone = 27



_SUBRIFTCHAPTERINFOUNLOCKCONDITIONS = _descriptor.Descriptor(
  name='SUBRiftChapterInfoUnlockConditions',
  full_name='ConfigDataRiftChapterInfo.SUBRiftChapterInfoUnlockConditions',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ConditionType', full_name='ConfigDataRiftChapterInfo.SUBRiftChapterInfoUnlockConditions.ConditionType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Param', full_name='ConfigDataRiftChapterInfo.SUBRiftChapterInfoUnlockConditions.Param', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=63,
  serialized_end=200,
)


_SUBGOODS = _descriptor.Descriptor(
  name='SUBGoods',
  full_name='ConfigDataRiftChapterInfo.SUBGoods',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='GoodsType', full_name='ConfigDataRiftChapterInfo.SUBGoods.GoodsType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataRiftChapterInfo.SUBGoods.Id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Count', full_name='ConfigDataRiftChapterInfo.SUBGoods.Count', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=202,
  serialized_end=300,
)


_CONFIGDATARIFTCHAPTERINFO = _descriptor.Descriptor(
  name='ConfigDataRiftChapterInfo',
  full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Name', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='TitleName', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.TitleName', index=2,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Desc', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Desc', index=3,
      number=6, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Place', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Place', index=4,
      number=8, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='UnlockConditions', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.UnlockConditions', index=5,
      number=10, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Hard', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Hard', index=6,
      number=11, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='HardChapter', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.HardChapter', index=7,
      number=12, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RiftLevels_ID', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.RiftLevels_ID', index=8,
      number=13, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Reward1Star', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Reward1Star', index=9,
      number=14, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Reward1', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Reward1', index=10,
      number=15, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Reward2Star', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Reward2Star', index=11,
      number=16, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Reward2', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Reward2', index=12,
      number=17, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Reward3Star', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Reward3Star', index=13,
      number=18, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Reward3', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Reward3', index=14,
      number=19, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Image', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.Image', index=15,
      number=20, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ChapterBGPrefabName', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.ChapterBGPrefabName', index=16,
      number=21, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='IsOpened', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.IsOpened', index=17,
      number=22, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='StoryOutlineInfoID', full_name='ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo.StoryOutlineInfoID', index=18,
      number=23, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=303,
  serialized_end=866,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataRiftChapterInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataRiftChapterInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=868,
  serialized_end=944,
)

_SUBRIFTCHAPTERINFOUNLOCKCONDITIONS.fields_by_name['ConditionType'].enum_type = _ENUMRIFTCHAPTERUNLOCKCONDITIONTYPE
_SUBGOODS.fields_by_name['GoodsType'].enum_type = _ENUMGOODSTYPE
_CONFIGDATARIFTCHAPTERINFO.fields_by_name['UnlockConditions'].message_type = _SUBRIFTCHAPTERINFOUNLOCKCONDITIONS
_CONFIGDATARIFTCHAPTERINFO.fields_by_name['Reward1'].message_type = _SUBGOODS
_CONFIGDATARIFTCHAPTERINFO.fields_by_name['Reward2'].message_type = _SUBGOODS
_CONFIGDATARIFTCHAPTERINFO.fields_by_name['Reward3'].message_type = _SUBGOODS
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATARIFTCHAPTERINFO
DESCRIPTOR.message_types_by_name['SUBRiftChapterInfoUnlockConditions'] = _SUBRIFTCHAPTERINFOUNLOCKCONDITIONS
DESCRIPTOR.message_types_by_name['SUBGoods'] = _SUBGOODS
DESCRIPTOR.message_types_by_name['ConfigDataRiftChapterInfo'] = _CONFIGDATARIFTCHAPTERINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMRiftChapterUnlockConditionType'] = _ENUMRIFTCHAPTERUNLOCKCONDITIONTYPE
DESCRIPTOR.enum_types_by_name['ENUMGoodsType'] = _ENUMGOODSTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBRiftChapterInfoUnlockConditions = _reflection.GeneratedProtocolMessageType('SUBRiftChapterInfoUnlockConditions', (_message.Message,), {
  'DESCRIPTOR' : _SUBRIFTCHAPTERINFOUNLOCKCONDITIONS,
  '__module__' : 'ConfigDataRiftChapterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataRiftChapterInfo.SUBRiftChapterInfoUnlockConditions)
  })
_sym_db.RegisterMessage(SUBRiftChapterInfoUnlockConditions)

SUBGoods = _reflection.GeneratedProtocolMessageType('SUBGoods', (_message.Message,), {
  'DESCRIPTOR' : _SUBGOODS,
  '__module__' : 'ConfigDataRiftChapterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataRiftChapterInfo.SUBGoods)
  })
_sym_db.RegisterMessage(SUBGoods)

ConfigDataRiftChapterInfo = _reflection.GeneratedProtocolMessageType('ConfigDataRiftChapterInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATARIFTCHAPTERINFO,
  '__module__' : 'ConfigDataRiftChapterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataRiftChapterInfo.ConfigDataRiftChapterInfo)
  })
_sym_db.RegisterMessage(ConfigDataRiftChapterInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataRiftChapterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataRiftChapterInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
