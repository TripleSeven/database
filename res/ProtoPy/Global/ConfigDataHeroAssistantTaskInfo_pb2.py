# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataHeroAssistantTaskInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataHeroAssistantTaskInfo.proto',
  package='ConfigDataHeroAssistantTaskInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n%ConfigDataHeroAssistantTaskInfo.proto\x12\x1f\x43onfigDataHeroAssistantTaskInfo\"?\n\x16SUBCompleteValueDropID\x12\x15\n\rCompleteValue\x18\x01 \x02(\x05\x12\x0e\n\x06\x44ropID\x18\x02 \x02(\x05\"\xca\x01\n\x1f\x43onfigDataHeroAssistantTaskInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Name\x18\x03 \x02(\t\x12\x10\n\x08Resource\x18\x04 \x03(\t\x12\x19\n\x11RequiredUserLevel\x18\x05 \x02(\x05\x12\x16\n\x0e\x43ompletePoints\x18\x06 \x02(\x05\x12H\n\x07Rewards\x18\x07 \x03(\x0b\x32\x37.ConfigDataHeroAssistantTaskInfo.SUBCompleteValueDropID\"X\n\x05Items\x12O\n\x05items\x18\x01 \x03(\x0b\x32@.ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo')
)




_SUBCOMPLETEVALUEDROPID = _descriptor.Descriptor(
  name='SUBCompleteValueDropID',
  full_name='ConfigDataHeroAssistantTaskInfo.SUBCompleteValueDropID',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='CompleteValue', full_name='ConfigDataHeroAssistantTaskInfo.SUBCompleteValueDropID.CompleteValue', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DropID', full_name='ConfigDataHeroAssistantTaskInfo.SUBCompleteValueDropID.DropID', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=74,
  serialized_end=137,
)


_CONFIGDATAHEROASSISTANTTASKINFO = _descriptor.Descriptor(
  name='ConfigDataHeroAssistantTaskInfo',
  full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo.Name', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Resource', full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo.Resource', index=2,
      number=4, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RequiredUserLevel', full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo.RequiredUserLevel', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='CompletePoints', full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo.CompletePoints', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Rewards', full_name='ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo.Rewards', index=5,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=140,
  serialized_end=342,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataHeroAssistantTaskInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataHeroAssistantTaskInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=344,
  serialized_end=432,
)

_CONFIGDATAHEROASSISTANTTASKINFO.fields_by_name['Rewards'].message_type = _SUBCOMPLETEVALUEDROPID
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAHEROASSISTANTTASKINFO
DESCRIPTOR.message_types_by_name['SUBCompleteValueDropID'] = _SUBCOMPLETEVALUEDROPID
DESCRIPTOR.message_types_by_name['ConfigDataHeroAssistantTaskInfo'] = _CONFIGDATAHEROASSISTANTTASKINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBCompleteValueDropID = _reflection.GeneratedProtocolMessageType('SUBCompleteValueDropID', (_message.Message,), {
  'DESCRIPTOR' : _SUBCOMPLETEVALUEDROPID,
  '__module__' : 'ConfigDataHeroAssistantTaskInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroAssistantTaskInfo.SUBCompleteValueDropID)
  })
_sym_db.RegisterMessage(SUBCompleteValueDropID)

ConfigDataHeroAssistantTaskInfo = _reflection.GeneratedProtocolMessageType('ConfigDataHeroAssistantTaskInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAHEROASSISTANTTASKINFO,
  '__module__' : 'ConfigDataHeroAssistantTaskInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroAssistantTaskInfo.ConfigDataHeroAssistantTaskInfo)
  })
_sym_db.RegisterMessage(ConfigDataHeroAssistantTaskInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataHeroAssistantTaskInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroAssistantTaskInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
