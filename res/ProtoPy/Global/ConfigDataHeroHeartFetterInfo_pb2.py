# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataHeroHeartFetterInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataHeroHeartFetterInfo.proto',
  package='ConfigDataHeroHeartFetterInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n#ConfigDataHeroHeartFetterInfo.proto\x12\x1d\x43onfigDataHeroHeartFetterInfo\"J\n\x1dSUBHeroHeartFetterUnlockSkill\x12\x0f\n\x07SkillId\x18\x01 \x02(\x05\x12\x18\n\x10HeartFetterLevel\x18\x02 \x02(\x05\"\x90\x01\n!SUBHeroHeartFetterUnlockCondition\x12\\\n\rConditionType\x18\x01 \x02(\x0e\x32\x45.ConfigDataHeroHeartFetterInfo.ENUMHeroHeartFetterUnlockConditionType\x12\r\n\x05Parm1\x18\x02 \x02(\x05\"f\n\x08SUBGoods\x12?\n\tGoodsType\x18\x01 \x02(\x0e\x32,.ConfigDataHeroHeartFetterInfo.ENUMGoodsType\x12\n\n\x02Id\x18\x02 \x02(\x05\x12\r\n\x05\x43ount\x18\x03 \x02(\x05\"\x88\x01\n\x18SUBHeroFetterLevelUpCost\x12\r\n\x05Level\x18\x01 \x02(\x05\x12>\n\x08ItemType\x18\x02 \x02(\x0e\x32,.ConfigDataHeroHeartFetterInfo.ENUMGoodsType\x12\x0e\n\x06ItemId\x18\x03 \x02(\x05\x12\r\n\x05\x43ount\x18\x04 \x02(\x05\"\xdd\x03\n\x1d\x43onfigDataHeroHeartFetterInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Name\x18\x03 \x02(\t\x12\x10\n\x08MaxLevel\x18\x05 \x02(\x05\x12Z\n\x10UnlockConditions\x18\x06 \x03(\x0b\x32@.ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockCondition\x12=\n\x0cUnlockReward\x18\x07 \x03(\x0b\x32\'.ConfigDataHeroHeartFetterInfo.SUBGoods\x12\x1d\n\x15HeroHeartFetterSkills\x18\x08 \x03(\x05\x12Q\n\x10LevelUpMaterials\x18\t \x03(\x0b\x32\x37.ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost\x12\x13\n\x0bLevelUpGold\x18\n \x03(\x05\x12U\n\x0fUnlockSkills_ID\x18\x0b \x03(\x0b\x32<.ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockSkill\x12\x17\n\x0fShownSkillCount\x18\x0c \x02(\x05\"T\n\x05Items\x12K\n\x05items\x18\x01 \x03(\x0b\x32<.ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo*\xbf\x01\n&ENUMHeroHeartFetterUnlockConditionType\x12+\n\'HeroHeartFetterUnlockConditionType_None\x10\x00\x12\x36\n2HeroHeartFetterUnlockConditionType_HeroFetterLevel\x10\x01\x12\x30\n,HeroHeartFetterUnlockConditionType_HeroLevel\x10\x02*\xf9\x05\n\rENUMGoodsType\x12\x12\n\x0eGoodsType_None\x10\x00\x12\x12\n\x0eGoodsType_Gold\x10\x01\x12\x15\n\x11GoodsType_Crystal\x10\x02\x12\x14\n\x10GoodsType_Energy\x10\x03\x12\x12\n\x0eGoodsType_Hero\x10\x04\x12\x19\n\x15GoodsType_JobMaterial\x10\x05\x12\x12\n\x0eGoodsType_Item\x10\x06\x12\x17\n\x13GoodsType_Equipment\x10\x07\x12\x19\n\x15GoodsType_ArenaTicket\x10\x08\x12\x19\n\x15GoodsType_ArenaHonour\x10\t\x12\x17\n\x13GoodsType_PlayerExp\x10\n\x12(\n$GoodsType_TrainingGroundTechMaterial\x10\x0b\x12\x1e\n\x1aGoodsType_FriendshipPoints\x10\x0c\x12\x1a\n\x16GoodsType_EnchantStone\x10\r\x12\x17\n\x13GoodsType_MonthCard\x10\x0e\x12\x17\n\x13GoodsType_HeadFrame\x10\x0f\x12\x16\n\x12GoodsType_HeroSkin\x10\x10\x12\x19\n\x15GoodsType_SoldierSkin\x10\x11\x12\x18\n\x14GoodsType_SkinTicket\x10\x12\x12\x1e\n\x1aGoodsType_RealTimePVPHonor\x10\x13\x12\x1b\n\x17GoodsType_MemoryEssence\x10\x14\x12\x1a\n\x16GoodsType_MithralStone\x10\x15\x12$\n GoodsType_BrillianceMithralStone\x10\x16\x12\x18\n\x14GoodsType_GuildMedal\x10\x17\x12\x1c\n\x18GoodsType_ChallengePoint\x10\x18\x12\x1a\n\x16GoodsType_FashionPoint\x10\x19\x12\x13\n\x0fGoodsType_Title\x10\x1a\x12\x1b\n\x17GoodsType_RefineryStone\x10\x1b')
)

_ENUMHEROHEARTFETTERUNLOCKCONDITIONTYPE = _descriptor.EnumDescriptor(
  name='ENUMHeroHeartFetterUnlockConditionType',
  full_name='ConfigDataHeroHeartFetterInfo.ENUMHeroHeartFetterUnlockConditionType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='HeroHeartFetterUnlockConditionType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HeroHeartFetterUnlockConditionType_HeroFetterLevel', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HeroHeartFetterUnlockConditionType_HeroLevel', index=2, number=2,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1103,
  serialized_end=1294,
)
_sym_db.RegisterEnumDescriptor(_ENUMHEROHEARTFETTERUNLOCKCONDITIONTYPE)

ENUMHeroHeartFetterUnlockConditionType = enum_type_wrapper.EnumTypeWrapper(_ENUMHEROHEARTFETTERUNLOCKCONDITIONTYPE)
_ENUMGOODSTYPE = _descriptor.EnumDescriptor(
  name='ENUMGoodsType',
  full_name='ConfigDataHeroHeartFetterInfo.ENUMGoodsType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GoodsType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Gold', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Crystal', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Energy', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Hero', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_JobMaterial', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Item', index=6, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Equipment', index=7, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaTicket', index=8, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaHonour', index=9, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_PlayerExp', index=10, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_TrainingGroundTechMaterial', index=11, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FriendshipPoints', index=12, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_EnchantStone', index=13, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MonthCard', index=14, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeadFrame', index=15, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeroSkin', index=16, number=16,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SoldierSkin', index=17, number=17,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SkinTicket', index=18, number=18,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RealTimePVPHonor', index=19, number=19,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MemoryEssence', index=20, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MithralStone', index=21, number=21,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_BrillianceMithralStone', index=22, number=22,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_GuildMedal', index=23, number=23,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ChallengePoint', index=24, number=24,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FashionPoint', index=25, number=25,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Title', index=26, number=26,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RefineryStone', index=27, number=27,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=1297,
  serialized_end=2058,
)
_sym_db.RegisterEnumDescriptor(_ENUMGOODSTYPE)

ENUMGoodsType = enum_type_wrapper.EnumTypeWrapper(_ENUMGOODSTYPE)
HeroHeartFetterUnlockConditionType_None = 0
HeroHeartFetterUnlockConditionType_HeroFetterLevel = 1
HeroHeartFetterUnlockConditionType_HeroLevel = 2
GoodsType_None = 0
GoodsType_Gold = 1
GoodsType_Crystal = 2
GoodsType_Energy = 3
GoodsType_Hero = 4
GoodsType_JobMaterial = 5
GoodsType_Item = 6
GoodsType_Equipment = 7
GoodsType_ArenaTicket = 8
GoodsType_ArenaHonour = 9
GoodsType_PlayerExp = 10
GoodsType_TrainingGroundTechMaterial = 11
GoodsType_FriendshipPoints = 12
GoodsType_EnchantStone = 13
GoodsType_MonthCard = 14
GoodsType_HeadFrame = 15
GoodsType_HeroSkin = 16
GoodsType_SoldierSkin = 17
GoodsType_SkinTicket = 18
GoodsType_RealTimePVPHonor = 19
GoodsType_MemoryEssence = 20
GoodsType_MithralStone = 21
GoodsType_BrillianceMithralStone = 22
GoodsType_GuildMedal = 23
GoodsType_ChallengePoint = 24
GoodsType_FashionPoint = 25
GoodsType_Title = 26
GoodsType_RefineryStone = 27



_SUBHEROHEARTFETTERUNLOCKSKILL = _descriptor.Descriptor(
  name='SUBHeroHeartFetterUnlockSkill',
  full_name='ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockSkill',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='SkillId', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockSkill.SkillId', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='HeartFetterLevel', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockSkill.HeartFetterLevel', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=70,
  serialized_end=144,
)


_SUBHEROHEARTFETTERUNLOCKCONDITION = _descriptor.Descriptor(
  name='SUBHeroHeartFetterUnlockCondition',
  full_name='ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockCondition',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ConditionType', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockCondition.ConditionType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Parm1', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockCondition.Parm1', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=147,
  serialized_end=291,
)


_SUBGOODS = _descriptor.Descriptor(
  name='SUBGoods',
  full_name='ConfigDataHeroHeartFetterInfo.SUBGoods',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='GoodsType', full_name='ConfigDataHeroHeartFetterInfo.SUBGoods.GoodsType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataHeroHeartFetterInfo.SUBGoods.Id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Count', full_name='ConfigDataHeroHeartFetterInfo.SUBGoods.Count', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=293,
  serialized_end=395,
)


_SUBHEROFETTERLEVELUPCOST = _descriptor.Descriptor(
  name='SUBHeroFetterLevelUpCost',
  full_name='ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='Level', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost.Level', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ItemType', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost.ItemType', index=1,
      number=2, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ItemId', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost.ItemId', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Count', full_name='ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost.Count', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=398,
  serialized_end=534,
)


_CONFIGDATAHEROHEARTFETTERINFO = _descriptor.Descriptor(
  name='ConfigDataHeroHeartFetterInfo',
  full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.Name', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MaxLevel', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.MaxLevel', index=2,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='UnlockConditions', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.UnlockConditions', index=3,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='UnlockReward', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.UnlockReward', index=4,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='HeroHeartFetterSkills', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.HeroHeartFetterSkills', index=5,
      number=8, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelUpMaterials', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.LevelUpMaterials', index=6,
      number=9, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelUpGold', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.LevelUpGold', index=7,
      number=10, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='UnlockSkills_ID', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.UnlockSkills_ID', index=8,
      number=11, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ShownSkillCount', full_name='ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo.ShownSkillCount', index=9,
      number=12, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=537,
  serialized_end=1014,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataHeroHeartFetterInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataHeroHeartFetterInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1016,
  serialized_end=1100,
)

_SUBHEROHEARTFETTERUNLOCKCONDITION.fields_by_name['ConditionType'].enum_type = _ENUMHEROHEARTFETTERUNLOCKCONDITIONTYPE
_SUBGOODS.fields_by_name['GoodsType'].enum_type = _ENUMGOODSTYPE
_SUBHEROFETTERLEVELUPCOST.fields_by_name['ItemType'].enum_type = _ENUMGOODSTYPE
_CONFIGDATAHEROHEARTFETTERINFO.fields_by_name['UnlockConditions'].message_type = _SUBHEROHEARTFETTERUNLOCKCONDITION
_CONFIGDATAHEROHEARTFETTERINFO.fields_by_name['UnlockReward'].message_type = _SUBGOODS
_CONFIGDATAHEROHEARTFETTERINFO.fields_by_name['LevelUpMaterials'].message_type = _SUBHEROFETTERLEVELUPCOST
_CONFIGDATAHEROHEARTFETTERINFO.fields_by_name['UnlockSkills_ID'].message_type = _SUBHEROHEARTFETTERUNLOCKSKILL
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAHEROHEARTFETTERINFO
DESCRIPTOR.message_types_by_name['SUBHeroHeartFetterUnlockSkill'] = _SUBHEROHEARTFETTERUNLOCKSKILL
DESCRIPTOR.message_types_by_name['SUBHeroHeartFetterUnlockCondition'] = _SUBHEROHEARTFETTERUNLOCKCONDITION
DESCRIPTOR.message_types_by_name['SUBGoods'] = _SUBGOODS
DESCRIPTOR.message_types_by_name['SUBHeroFetterLevelUpCost'] = _SUBHEROFETTERLEVELUPCOST
DESCRIPTOR.message_types_by_name['ConfigDataHeroHeartFetterInfo'] = _CONFIGDATAHEROHEARTFETTERINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMHeroHeartFetterUnlockConditionType'] = _ENUMHEROHEARTFETTERUNLOCKCONDITIONTYPE
DESCRIPTOR.enum_types_by_name['ENUMGoodsType'] = _ENUMGOODSTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBHeroHeartFetterUnlockSkill = _reflection.GeneratedProtocolMessageType('SUBHeroHeartFetterUnlockSkill', (_message.Message,), {
  'DESCRIPTOR' : _SUBHEROHEARTFETTERUNLOCKSKILL,
  '__module__' : 'ConfigDataHeroHeartFetterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockSkill)
  })
_sym_db.RegisterMessage(SUBHeroHeartFetterUnlockSkill)

SUBHeroHeartFetterUnlockCondition = _reflection.GeneratedProtocolMessageType('SUBHeroHeartFetterUnlockCondition', (_message.Message,), {
  'DESCRIPTOR' : _SUBHEROHEARTFETTERUNLOCKCONDITION,
  '__module__' : 'ConfigDataHeroHeartFetterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroHeartFetterInfo.SUBHeroHeartFetterUnlockCondition)
  })
_sym_db.RegisterMessage(SUBHeroHeartFetterUnlockCondition)

SUBGoods = _reflection.GeneratedProtocolMessageType('SUBGoods', (_message.Message,), {
  'DESCRIPTOR' : _SUBGOODS,
  '__module__' : 'ConfigDataHeroHeartFetterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroHeartFetterInfo.SUBGoods)
  })
_sym_db.RegisterMessage(SUBGoods)

SUBHeroFetterLevelUpCost = _reflection.GeneratedProtocolMessageType('SUBHeroFetterLevelUpCost', (_message.Message,), {
  'DESCRIPTOR' : _SUBHEROFETTERLEVELUPCOST,
  '__module__' : 'ConfigDataHeroHeartFetterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroHeartFetterInfo.SUBHeroFetterLevelUpCost)
  })
_sym_db.RegisterMessage(SUBHeroFetterLevelUpCost)

ConfigDataHeroHeartFetterInfo = _reflection.GeneratedProtocolMessageType('ConfigDataHeroHeartFetterInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAHEROHEARTFETTERINFO,
  '__module__' : 'ConfigDataHeroHeartFetterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroHeartFetterInfo.ConfigDataHeroHeartFetterInfo)
  })
_sym_db.RegisterMessage(ConfigDataHeroHeartFetterInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataHeroHeartFetterInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroHeartFetterInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
