# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataHeroAssistantTaskGeneralInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataHeroAssistantTaskGeneralInfo.proto',
  package='ConfigDataHeroAssistantTaskGeneralInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n,ConfigDataHeroAssistantTaskGeneralInfo.proto\x12&ConfigDataHeroAssistantTaskGeneralInfo\"\xeb\x01\n&ConfigDataHeroAssistantTaskGeneralInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x14\n\x0cWorkSeconds1\x18\x03 \x02(\x05\x12\x12\n\nDropCount1\x18\x04 \x02(\x05\x12\x14\n\x0cWorkSeconds2\x18\x05 \x02(\x05\x12\x12\n\nDropCount2\x18\x06 \x02(\x05\x12\x14\n\x0cWorkSeconds3\x18\x07 \x02(\x05\x12\x12\n\nDropCount3\x18\x08 \x02(\x05\x12\x1d\n\x15RecommandHeroMultiply\x18\t \x02(\x05\x12\x18\n\x10RecommandHeroAdd\x18\n \x02(\x05\"f\n\x05Items\x12]\n\x05items\x18\x01 \x03(\x0b\x32N.ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo')
)




_CONFIGDATAHEROASSISTANTTASKGENERALINFO = _descriptor.Descriptor(
  name='ConfigDataHeroAssistantTaskGeneralInfo',
  full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='WorkSeconds1', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.WorkSeconds1', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DropCount1', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.DropCount1', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='WorkSeconds2', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.WorkSeconds2', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DropCount2', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.DropCount2', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='WorkSeconds3', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.WorkSeconds3', index=5,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DropCount3', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.DropCount3', index=6,
      number=8, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RecommandHeroMultiply', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.RecommandHeroMultiply', index=7,
      number=9, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RecommandHeroAdd', full_name='ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo.RecommandHeroAdd', index=8,
      number=10, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=89,
  serialized_end=324,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataHeroAssistantTaskGeneralInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataHeroAssistantTaskGeneralInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=326,
  serialized_end=428,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAHEROASSISTANTTASKGENERALINFO
DESCRIPTOR.message_types_by_name['ConfigDataHeroAssistantTaskGeneralInfo'] = _CONFIGDATAHEROASSISTANTTASKGENERALINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataHeroAssistantTaskGeneralInfo = _reflection.GeneratedProtocolMessageType('ConfigDataHeroAssistantTaskGeneralInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAHEROASSISTANTTASKGENERALINFO,
  '__module__' : 'ConfigDataHeroAssistantTaskGeneralInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroAssistantTaskGeneralInfo.ConfigDataHeroAssistantTaskGeneralInfo)
  })
_sym_db.RegisterMessage(ConfigDataHeroAssistantTaskGeneralInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataHeroAssistantTaskGeneralInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroAssistantTaskGeneralInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
