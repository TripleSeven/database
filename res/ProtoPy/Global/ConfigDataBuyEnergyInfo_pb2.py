# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataBuyEnergyInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataBuyEnergyInfo.proto',
  package='ConfigDataBuyEnergyInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1d\x43onfigDataBuyEnergyInfo.proto\x12\x17\x43onfigDataBuyEnergyInfo\"4\n\x17\x43onfigDataBuyEnergyInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\r\n\x05Price\x18\x03 \x02(\x05\"H\n\x05Items\x12?\n\x05items\x18\x01 \x03(\x0b\x32\x30.ConfigDataBuyEnergyInfo.ConfigDataBuyEnergyInfo')
)




_CONFIGDATABUYENERGYINFO = _descriptor.Descriptor(
  name='ConfigDataBuyEnergyInfo',
  full_name='ConfigDataBuyEnergyInfo.ConfigDataBuyEnergyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataBuyEnergyInfo.ConfigDataBuyEnergyInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Price', full_name='ConfigDataBuyEnergyInfo.ConfigDataBuyEnergyInfo.Price', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=58,
  serialized_end=110,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataBuyEnergyInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataBuyEnergyInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=112,
  serialized_end=184,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATABUYENERGYINFO
DESCRIPTOR.message_types_by_name['ConfigDataBuyEnergyInfo'] = _CONFIGDATABUYENERGYINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataBuyEnergyInfo = _reflection.GeneratedProtocolMessageType('ConfigDataBuyEnergyInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATABUYENERGYINFO,
  '__module__' : 'ConfigDataBuyEnergyInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataBuyEnergyInfo.ConfigDataBuyEnergyInfo)
  })
_sym_db.RegisterMessage(ConfigDataBuyEnergyInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataBuyEnergyInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataBuyEnergyInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
