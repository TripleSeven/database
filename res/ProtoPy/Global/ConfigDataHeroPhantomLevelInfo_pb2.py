# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataHeroPhantomLevelInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataHeroPhantomLevelInfo.proto',
  package='ConfigDataHeroPhantomLevelInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n$ConfigDataHeroPhantomLevelInfo.proto\x12\x1e\x43onfigDataHeroPhantomLevelInfo\"g\n\x08SUBGoods\x12@\n\tGoodsType\x18\x01 \x02(\x0e\x32-.ConfigDataHeroPhantomLevelInfo.ENUMGoodsType\x12\n\n\x02Id\x18\x02 \x02(\x05\x12\r\n\x05\x43ount\x18\x03 \x02(\x05\"\xfa\x04\n\x1e\x43onfigDataHeroPhantomLevelInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Name\x18\x03 \x02(\t\x12\x10\n\x08PreLevel\x18\x04 \x02(\x05\x12\x15\n\rEnergySuccess\x18\x05 \x02(\x05\x12\x12\n\nEnergyFail\x18\x06 \x02(\x05\x12\x14\n\x0cMonsterLevel\x18\x07 \x02(\x05\x12\x11\n\tBattle_ID\x18\x08 \x02(\x05\x12\x11\n\tPlayerExp\x18\t \x02(\x05\x12\x0f\n\x07HeroExp\x18\n \x02(\x05\x12\x0c\n\x04Gold\x18\x0b \x02(\x05\x12\x0e\n\x06\x44ropID\x18\x0c \x02(\x05\x12\x45\n\x13\x46irstClearDropItems\x18\r \x03(\x0b\x32(.ConfigDataHeroPhantomLevelInfo.SUBGoods\x12\x1a\n\x12\x44isplayRewardCount\x18\x0e \x02(\x05\x12\x16\n\x0e\x41\x63hievement1ID\x18\x0f \x02(\x05\x12G\n\x15\x41\x63hievement1BonusItem\x18\x10 \x03(\x0b\x32(.ConfigDataHeroPhantomLevelInfo.SUBGoods\x12\x16\n\x0e\x41\x63hievement2ID\x18\x11 \x02(\x05\x12G\n\x15\x41\x63hievement2BonusItem\x18\x12 \x03(\x0b\x32(.ConfigDataHeroPhantomLevelInfo.SUBGoods\x12\x16\n\x0e\x41\x63hievement3ID\x18\x13 \x02(\x05\x12G\n\x15\x41\x63hievement3BonusItem\x18\x14 \x03(\x0b\x32(.ConfigDataHeroPhantomLevelInfo.SUBGoods\x12\x10\n\x08Strategy\x18\x16 \x02(\t\"V\n\x05Items\x12M\n\x05items\x18\x01 \x03(\x0b\x32>.ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo*\xf9\x05\n\rENUMGoodsType\x12\x12\n\x0eGoodsType_None\x10\x00\x12\x12\n\x0eGoodsType_Gold\x10\x01\x12\x15\n\x11GoodsType_Crystal\x10\x02\x12\x14\n\x10GoodsType_Energy\x10\x03\x12\x12\n\x0eGoodsType_Hero\x10\x04\x12\x19\n\x15GoodsType_JobMaterial\x10\x05\x12\x12\n\x0eGoodsType_Item\x10\x06\x12\x17\n\x13GoodsType_Equipment\x10\x07\x12\x19\n\x15GoodsType_ArenaTicket\x10\x08\x12\x19\n\x15GoodsType_ArenaHonour\x10\t\x12\x17\n\x13GoodsType_PlayerExp\x10\n\x12(\n$GoodsType_TrainingGroundTechMaterial\x10\x0b\x12\x1e\n\x1aGoodsType_FriendshipPoints\x10\x0c\x12\x1a\n\x16GoodsType_EnchantStone\x10\r\x12\x17\n\x13GoodsType_MonthCard\x10\x0e\x12\x17\n\x13GoodsType_HeadFrame\x10\x0f\x12\x16\n\x12GoodsType_HeroSkin\x10\x10\x12\x19\n\x15GoodsType_SoldierSkin\x10\x11\x12\x18\n\x14GoodsType_SkinTicket\x10\x12\x12\x1e\n\x1aGoodsType_RealTimePVPHonor\x10\x13\x12\x1b\n\x17GoodsType_MemoryEssence\x10\x14\x12\x1a\n\x16GoodsType_MithralStone\x10\x15\x12$\n GoodsType_BrillianceMithralStone\x10\x16\x12\x18\n\x14GoodsType_GuildMedal\x10\x17\x12\x1c\n\x18GoodsType_ChallengePoint\x10\x18\x12\x1a\n\x16GoodsType_FashionPoint\x10\x19\x12\x13\n\x0fGoodsType_Title\x10\x1a\x12\x1b\n\x17GoodsType_RefineryStone\x10\x1b')
)

_ENUMGOODSTYPE = _descriptor.EnumDescriptor(
  name='ENUMGoodsType',
  full_name='ConfigDataHeroPhantomLevelInfo.ENUMGoodsType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='GoodsType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Gold', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Crystal', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Energy', index=3, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Hero', index=4, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_JobMaterial', index=5, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Item', index=6, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Equipment', index=7, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaTicket', index=8, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ArenaHonour', index=9, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_PlayerExp', index=10, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_TrainingGroundTechMaterial', index=11, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FriendshipPoints', index=12, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_EnchantStone', index=13, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MonthCard', index=14, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeadFrame', index=15, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_HeroSkin', index=16, number=16,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SoldierSkin', index=17, number=17,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_SkinTicket', index=18, number=18,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RealTimePVPHonor', index=19, number=19,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MemoryEssence', index=20, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_MithralStone', index=21, number=21,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_BrillianceMithralStone', index=22, number=22,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_GuildMedal', index=23, number=23,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_ChallengePoint', index=24, number=24,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_FashionPoint', index=25, number=25,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_Title', index=26, number=26,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='GoodsType_RefineryStone', index=27, number=27,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=903,
  serialized_end=1664,
)
_sym_db.RegisterEnumDescriptor(_ENUMGOODSTYPE)

ENUMGoodsType = enum_type_wrapper.EnumTypeWrapper(_ENUMGOODSTYPE)
GoodsType_None = 0
GoodsType_Gold = 1
GoodsType_Crystal = 2
GoodsType_Energy = 3
GoodsType_Hero = 4
GoodsType_JobMaterial = 5
GoodsType_Item = 6
GoodsType_Equipment = 7
GoodsType_ArenaTicket = 8
GoodsType_ArenaHonour = 9
GoodsType_PlayerExp = 10
GoodsType_TrainingGroundTechMaterial = 11
GoodsType_FriendshipPoints = 12
GoodsType_EnchantStone = 13
GoodsType_MonthCard = 14
GoodsType_HeadFrame = 15
GoodsType_HeroSkin = 16
GoodsType_SoldierSkin = 17
GoodsType_SkinTicket = 18
GoodsType_RealTimePVPHonor = 19
GoodsType_MemoryEssence = 20
GoodsType_MithralStone = 21
GoodsType_BrillianceMithralStone = 22
GoodsType_GuildMedal = 23
GoodsType_ChallengePoint = 24
GoodsType_FashionPoint = 25
GoodsType_Title = 26
GoodsType_RefineryStone = 27



_SUBGOODS = _descriptor.Descriptor(
  name='SUBGoods',
  full_name='ConfigDataHeroPhantomLevelInfo.SUBGoods',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='GoodsType', full_name='ConfigDataHeroPhantomLevelInfo.SUBGoods.GoodsType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Id', full_name='ConfigDataHeroPhantomLevelInfo.SUBGoods.Id', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Count', full_name='ConfigDataHeroPhantomLevelInfo.SUBGoods.Count', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=175,
)


_CONFIGDATAHEROPHANTOMLEVELINFO = _descriptor.Descriptor(
  name='ConfigDataHeroPhantomLevelInfo',
  full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Name', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PreLevel', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.PreLevel', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='EnergySuccess', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.EnergySuccess', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='EnergyFail', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.EnergyFail', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MonsterLevel', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.MonsterLevel', index=5,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Battle_ID', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Battle_ID', index=6,
      number=8, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayerExp', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.PlayerExp', index=7,
      number=9, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='HeroExp', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.HeroExp', index=8,
      number=10, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Gold', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Gold', index=9,
      number=11, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DropID', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.DropID', index=10,
      number=12, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FirstClearDropItems', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.FirstClearDropItems', index=11,
      number=13, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DisplayRewardCount', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.DisplayRewardCount', index=12,
      number=14, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement1ID', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Achievement1ID', index=13,
      number=15, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement1BonusItem', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Achievement1BonusItem', index=14,
      number=16, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement2ID', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Achievement2ID', index=15,
      number=17, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement2BonusItem', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Achievement2BonusItem', index=16,
      number=18, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement3ID', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Achievement3ID', index=17,
      number=19, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Achievement3BonusItem', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Achievement3BonusItem', index=18,
      number=20, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Strategy', full_name='ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo.Strategy', index=19,
      number=22, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=178,
  serialized_end=812,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataHeroPhantomLevelInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataHeroPhantomLevelInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=814,
  serialized_end=900,
)

_SUBGOODS.fields_by_name['GoodsType'].enum_type = _ENUMGOODSTYPE
_CONFIGDATAHEROPHANTOMLEVELINFO.fields_by_name['FirstClearDropItems'].message_type = _SUBGOODS
_CONFIGDATAHEROPHANTOMLEVELINFO.fields_by_name['Achievement1BonusItem'].message_type = _SUBGOODS
_CONFIGDATAHEROPHANTOMLEVELINFO.fields_by_name['Achievement2BonusItem'].message_type = _SUBGOODS
_CONFIGDATAHEROPHANTOMLEVELINFO.fields_by_name['Achievement3BonusItem'].message_type = _SUBGOODS
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAHEROPHANTOMLEVELINFO
DESCRIPTOR.message_types_by_name['SUBGoods'] = _SUBGOODS
DESCRIPTOR.message_types_by_name['ConfigDataHeroPhantomLevelInfo'] = _CONFIGDATAHEROPHANTOMLEVELINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMGoodsType'] = _ENUMGOODSTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBGoods = _reflection.GeneratedProtocolMessageType('SUBGoods', (_message.Message,), {
  'DESCRIPTOR' : _SUBGOODS,
  '__module__' : 'ConfigDataHeroPhantomLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroPhantomLevelInfo.SUBGoods)
  })
_sym_db.RegisterMessage(SUBGoods)

ConfigDataHeroPhantomLevelInfo = _reflection.GeneratedProtocolMessageType('ConfigDataHeroPhantomLevelInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAHEROPHANTOMLEVELINFO,
  '__module__' : 'ConfigDataHeroPhantomLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroPhantomLevelInfo.ConfigDataHeroPhantomLevelInfo)
  })
_sym_db.RegisterMessage(ConfigDataHeroPhantomLevelInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataHeroPhantomLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroPhantomLevelInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
