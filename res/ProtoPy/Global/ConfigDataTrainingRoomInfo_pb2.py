# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataTrainingRoomInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataTrainingRoomInfo.proto',
  package='ConfigDataTrainingRoomInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n ConfigDataTrainingRoomInfo.proto\x12\x1a\x43onfigDataTrainingRoomInfo\"b\n\x1a\x43onfigDataTrainingRoomInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Name\x18\x03 \x02(\t\x12\x13\n\x0bSoldierType\x18\x04 \x03(\x05\x12\x15\n\rLevelToUnlock\x18\x05 \x02(\x05\"N\n\x05Items\x12\x45\n\x05items\x18\x01 \x03(\x0b\x32\x36.ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo')
)




_CONFIGDATATRAININGROOMINFO = _descriptor.Descriptor(
  name='ConfigDataTrainingRoomInfo',
  full_name='ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo.Name', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SoldierType', full_name='ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo.SoldierType', index=2,
      number=4, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LevelToUnlock', full_name='ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo.LevelToUnlock', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=64,
  serialized_end=162,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataTrainingRoomInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataTrainingRoomInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=164,
  serialized_end=242,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATATRAININGROOMINFO
DESCRIPTOR.message_types_by_name['ConfigDataTrainingRoomInfo'] = _CONFIGDATATRAININGROOMINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataTrainingRoomInfo = _reflection.GeneratedProtocolMessageType('ConfigDataTrainingRoomInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATATRAININGROOMINFO,
  '__module__' : 'ConfigDataTrainingRoomInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataTrainingRoomInfo.ConfigDataTrainingRoomInfo)
  })
_sym_db.RegisterMessage(ConfigDataTrainingRoomInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataTrainingRoomInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataTrainingRoomInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
