# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataErrorCodeStringTable.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataErrorCodeStringTable.proto',
  package='ConfigDataErrorCodeStringTable',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n$ConfigDataErrorCodeStringTable.proto\x12\x1e\x43onfigDataErrorCodeStringTable\":\n\x1e\x43onfigDataErrorCodeStringTable\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Text\x18\x03 \x02(\t\"V\n\x05Items\x12M\n\x05items\x18\x01 \x03(\x0b\x32>.ConfigDataErrorCodeStringTable.ConfigDataErrorCodeStringTable')
)




_CONFIGDATAERRORCODESTRINGTABLE = _descriptor.Descriptor(
  name='ConfigDataErrorCodeStringTable',
  full_name='ConfigDataErrorCodeStringTable.ConfigDataErrorCodeStringTable',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataErrorCodeStringTable.ConfigDataErrorCodeStringTable.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Text', full_name='ConfigDataErrorCodeStringTable.ConfigDataErrorCodeStringTable.Text', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=130,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataErrorCodeStringTable.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataErrorCodeStringTable.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=132,
  serialized_end=218,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAERRORCODESTRINGTABLE
DESCRIPTOR.message_types_by_name['ConfigDataErrorCodeStringTable'] = _CONFIGDATAERRORCODESTRINGTABLE
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataErrorCodeStringTable = _reflection.GeneratedProtocolMessageType('ConfigDataErrorCodeStringTable', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAERRORCODESTRINGTABLE,
  '__module__' : 'ConfigDataErrorCodeStringTable_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataErrorCodeStringTable.ConfigDataErrorCodeStringTable)
  })
_sym_db.RegisterMessage(ConfigDataErrorCodeStringTable)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataErrorCodeStringTable_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataErrorCodeStringTable.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
