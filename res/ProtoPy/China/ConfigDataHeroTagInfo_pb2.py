# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataHeroTagInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataHeroTagInfo.proto',
  package='ConfigDataHeroTagInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1b\x43onfigDataHeroTagInfo.proto\x12\x15\x43onfigDataHeroTagInfo\"|\n\x15\x43onfigDataHeroTagInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04Name\x18\x03 \x02(\t\x12\x0c\n\x04\x44\x65sc\x18\x04 \x02(\t\x12\x17\n\x0fRelatedHeros_ID\x18\x05 \x03(\x05\x12\x0c\n\x04Icon\x18\x06 \x02(\t\x12\x14\n\x0cSuperHero_ID\x18\x07 \x03(\x05\"D\n\x05Items\x12;\n\x05items\x18\x01 \x03(\x0b\x32,.ConfigDataHeroTagInfo.ConfigDataHeroTagInfo')
)




_CONFIGDATAHEROTAGINFO = _descriptor.Descriptor(
  name='ConfigDataHeroTagInfo',
  full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Name', full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo.Name', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Desc', full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo.Desc', index=2,
      number=4, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RelatedHeros_ID', full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo.RelatedHeros_ID', index=3,
      number=5, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Icon', full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo.Icon', index=4,
      number=6, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SuperHero_ID', full_name='ConfigDataHeroTagInfo.ConfigDataHeroTagInfo.SuperHero_ID', index=5,
      number=7, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=54,
  serialized_end=178,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataHeroTagInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataHeroTagInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=180,
  serialized_end=248,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAHEROTAGINFO
DESCRIPTOR.message_types_by_name['ConfigDataHeroTagInfo'] = _CONFIGDATAHEROTAGINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataHeroTagInfo = _reflection.GeneratedProtocolMessageType('ConfigDataHeroTagInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAHEROTAGINFO,
  '__module__' : 'ConfigDataHeroTagInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroTagInfo.ConfigDataHeroTagInfo)
  })
_sym_db.RegisterMessage(ConfigDataHeroTagInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataHeroTagInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroTagInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
