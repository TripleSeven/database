# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataArenaRobotInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataArenaRobotInfo.proto',
  package='ConfigDataArenaRobotInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1e\x43onfigDataArenaRobotInfo.proto\x12\x18\x43onfigDataArenaRobotInfo\"&\n\x18\x43onfigDataArenaRobotInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\"J\n\x05Items\x12\x41\n\x05items\x18\x01 \x03(\x0b\x32\x32.ConfigDataArenaRobotInfo.ConfigDataArenaRobotInfo')
)




_CONFIGDATAARENAROBOTINFO = _descriptor.Descriptor(
  name='ConfigDataArenaRobotInfo',
  full_name='ConfigDataArenaRobotInfo.ConfigDataArenaRobotInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataArenaRobotInfo.ConfigDataArenaRobotInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=60,
  serialized_end=98,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataArenaRobotInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataArenaRobotInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=100,
  serialized_end=174,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAARENAROBOTINFO
DESCRIPTOR.message_types_by_name['ConfigDataArenaRobotInfo'] = _CONFIGDATAARENAROBOTINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataArenaRobotInfo = _reflection.GeneratedProtocolMessageType('ConfigDataArenaRobotInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAARENAROBOTINFO,
  '__module__' : 'ConfigDataArenaRobotInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataArenaRobotInfo.ConfigDataArenaRobotInfo)
  })
_sym_db.RegisterMessage(ConfigDataArenaRobotInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataArenaRobotInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataArenaRobotInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
