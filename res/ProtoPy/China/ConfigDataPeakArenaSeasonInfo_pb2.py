# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataPeakArenaSeasonInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataPeakArenaSeasonInfo.proto',
  package='ConfigDataPeakArenaSeasonInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n#ConfigDataPeakArenaSeasonInfo.proto\x12\x1d\x43onfigDataPeakArenaSeasonInfo\"R\n\x16SUBPointRaceRewardInfo\x12\x11\n\tBattleNum\x18\x01 \x02(\x05\x12\r\n\x05\x44\x61nId\x18\x02 \x02(\x05\x12\x16\n\x0eTemplateMailId\x18\x03 \x02(\x05\"\x99\x05\n\x1d\x43onfigDataPeakArenaSeasonInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0c\n\x04\x44\x65sc\x18\x03 \x02(\t\x12\x16\n\x0ePointRaceWeeks\x18\x04 \x02(\x05\x12\x17\n\x0fSeasonStartTime\x18\x05 \x02(\t\x12O\n\x10PointRaceRewards\x18\x06 \x03(\x0b\x32\x35.ConfigDataPeakArenaSeasonInfo.SUBPointRaceRewardInfo\x12\x1b\n\x13PlayOff256StartTime\x18\x07 \x02(\t\x12\x1b\n\x13PlayOff128StartTime\x18\x08 \x02(\t\x12\x1a\n\x12PlayOff64StartTime\x18\t \x02(\t\x12\x1a\n\x12PlayOff32StartTime\x18\n \x02(\t\x12\x1a\n\x12PlayOff16StartTime\x18\x0b \x02(\t\x12\x19\n\x11PlayOff8StartTime\x18\x0c \x02(\t\x12\x19\n\x11PlayOff4StartTime\x18\r \x02(\t\x12\x19\n\x11PlayOff2StartTime\x18\x0e \x02(\t\x12\x1f\n\x17PlayOff256BattlesNumber\x18\x0f \x02(\x05\x12\x1f\n\x17PlayOff128BattlesNumber\x18\x10 \x02(\x05\x12\x1e\n\x16PlayOff64BattlesNumber\x18\x11 \x02(\x05\x12\x1e\n\x16PlayOff32BattlesNumber\x18\x12 \x02(\x05\x12\x1e\n\x16PlayOff16BattlesNumber\x18\x13 \x02(\x05\x12\x1d\n\x15PlayOff8BattlesNumber\x18\x14 \x02(\x05\x12\x1d\n\x15PlayOff4BattlesNumber\x18\x15 \x02(\x05\x12\x1d\n\x15PlayOff2BattlesNumber\x18\x16 \x02(\x05\"T\n\x05Items\x12K\n\x05items\x18\x01 \x03(\x0b\x32<.ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo')
)




_SUBPOINTRACEREWARDINFO = _descriptor.Descriptor(
  name='SUBPointRaceRewardInfo',
  full_name='ConfigDataPeakArenaSeasonInfo.SUBPointRaceRewardInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='BattleNum', full_name='ConfigDataPeakArenaSeasonInfo.SUBPointRaceRewardInfo.BattleNum', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='DanId', full_name='ConfigDataPeakArenaSeasonInfo.SUBPointRaceRewardInfo.DanId', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='TemplateMailId', full_name='ConfigDataPeakArenaSeasonInfo.SUBPointRaceRewardInfo.TemplateMailId', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=70,
  serialized_end=152,
)


_CONFIGDATAPEAKARENASEASONINFO = _descriptor.Descriptor(
  name='ConfigDataPeakArenaSeasonInfo',
  full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Desc', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.Desc', index=1,
      number=3, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PointRaceWeeks', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PointRaceWeeks', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SeasonStartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.SeasonStartTime', index=3,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PointRaceRewards', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PointRaceRewards', index=4,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff256StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff256StartTime', index=5,
      number=7, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff128StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff128StartTime', index=6,
      number=8, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff64StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff64StartTime', index=7,
      number=9, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff32StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff32StartTime', index=8,
      number=10, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff16StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff16StartTime', index=9,
      number=11, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff8StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff8StartTime', index=10,
      number=12, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff4StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff4StartTime', index=11,
      number=13, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff2StartTime', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff2StartTime', index=12,
      number=14, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff256BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff256BattlesNumber', index=13,
      number=15, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff128BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff128BattlesNumber', index=14,
      number=16, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff64BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff64BattlesNumber', index=15,
      number=17, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff32BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff32BattlesNumber', index=16,
      number=18, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff16BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff16BattlesNumber', index=17,
      number=19, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff8BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff8BattlesNumber', index=18,
      number=20, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff4BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff4BattlesNumber', index=19,
      number=21, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='PlayOff2BattlesNumber', full_name='ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo.PlayOff2BattlesNumber', index=20,
      number=22, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=155,
  serialized_end=820,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataPeakArenaSeasonInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataPeakArenaSeasonInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=822,
  serialized_end=906,
)

_CONFIGDATAPEAKARENASEASONINFO.fields_by_name['PointRaceRewards'].message_type = _SUBPOINTRACEREWARDINFO
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAPEAKARENASEASONINFO
DESCRIPTOR.message_types_by_name['SUBPointRaceRewardInfo'] = _SUBPOINTRACEREWARDINFO
DESCRIPTOR.message_types_by_name['ConfigDataPeakArenaSeasonInfo'] = _CONFIGDATAPEAKARENASEASONINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBPointRaceRewardInfo = _reflection.GeneratedProtocolMessageType('SUBPointRaceRewardInfo', (_message.Message,), {
  'DESCRIPTOR' : _SUBPOINTRACEREWARDINFO,
  '__module__' : 'ConfigDataPeakArenaSeasonInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPeakArenaSeasonInfo.SUBPointRaceRewardInfo)
  })
_sym_db.RegisterMessage(SUBPointRaceRewardInfo)

ConfigDataPeakArenaSeasonInfo = _reflection.GeneratedProtocolMessageType('ConfigDataPeakArenaSeasonInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAPEAKARENASEASONINFO,
  '__module__' : 'ConfigDataPeakArenaSeasonInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPeakArenaSeasonInfo.ConfigDataPeakArenaSeasonInfo)
  })
_sym_db.RegisterMessage(ConfigDataPeakArenaSeasonInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataPeakArenaSeasonInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPeakArenaSeasonInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
