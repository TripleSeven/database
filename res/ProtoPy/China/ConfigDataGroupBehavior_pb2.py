# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGroupBehavior.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGroupBehavior.proto',
  package='ConfigDataGroupBehavior',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n\x1d\x43onfigDataGroupBehavior.proto\x12\x17\x43onfigDataGroupBehavior\"\xb6\x01\n\x17\x43onfigDataGroupBehavior\x12\n\n\x02ID\x18\x02 \x02(\x05\x12M\n\x15SelectLeaderCondition\x18\x04 \x02(\x0e\x32..ConfigDataGroupBehavior.ENUMBehaviorCondition\x12\x10\n\x08SLCParam\x18\x05 \x02(\t\x12\x16\n\x0eLeaderBehavior\x18\x06 \x02(\x05\x12\x16\n\x0eMemberBehavior\x18\x07 \x02(\x05\"H\n\x05Items\x12?\n\x05items\x18\x01 \x03(\x0b\x32\x30.ConfigDataGroupBehavior.ConfigDataGroupBehavior*\xc0\x0c\n\x15\x45NUMBehaviorCondition\x12\x32\n.BehaviorCondition_EnemyEnterMoveAndAttackRange\x10\x01\x12\'\n#BehaviorCondition_LeaderStartAttack\x10\x02\x12-\n)BehaviorCondition_EnemyHPPercentLessEqual\x10\x03\x12\x34\n0BehaviorCondition_NoEnemyEnterMoveAndAttackRange\x10\x04\x12\x1f\n\x1b\x42\x65haviorCondition_EveryTurn\x10\x05\x12\x1b\n\x17\x42\x65haviorCondition_Never\x10\x06\x12\x1b\n\x17\x42\x65haviorCondition_TurnN\x10\x07\x12%\n!BehaviorCondition_AttackedByEnemy\x10\x08\x12>\n:BehaviorCondition_EnemyEnterMoveAndAttackRangeOfMoveTarget\x10\t\x12@\n<BehaviorCondition_NoEnemyEnterMoveAndAttackRangeOfMoveTarget\x10\n\x12\x33\n/BehaviorCondition_DistanceToMoveTargetLestEqual\x10\x0b\x12,\n(BehaviorCondition_SelfHPPercentLessEqual\x10\x0c\x12&\n\"BehaviorCondition_LeaderStopAttack\x10\r\x12$\n BehaviorCondition_DoingBehaviorN\x10\x0e\x12/\n+BehaviorCondition_AttackedByEnemyInLastTurn\x10\x0f\x12)\n%BehaviorCondition_FoundEnemyWithBuffN\x10\x10\x12&\n\"BehaviorCondition_NoEnemyWithBuffN\x10\x11\x12+\n\'BehaviorCondition_MemberAttackedByEnemy\x10\x12\x12.\n*BehaviorCondition_MemberHPPercentLessEqual\x10\x13\x12\x38\n4BehaviorCondition_EnemyEnterMoveAndAttackRangeExcept\x10\x14\x12:\n6BehaviorCondition_NoEnemyEnterMoveAndAttackRangeExcept\x10\x15\x12/\n+BehaviorCondition_SelfHPPercentGreaterEqual\x10\x16\x12 \n\x1c\x42\x65haviorCondition_HeroNExist\x10\x17\x12%\n!BehaviorCondition_GroupMemberDead\x10\x18\x12 \n\x1c\x42\x65haviorCondition_TurnCountN\x10\x19\x12/\n+BehaviorCondition_EnemyBlockWayToMoveTarget\x10\x1a\x12(\n$BehaviorCondition_HasWayToMoveTarget\x10\x1b\x12*\n&BehaviorCondition_FoundMemberWithBuffN\x10\x1c\x12\'\n#BehaviorCondition_NoMemberWithBuffN\x10\x1d\x12(\n$BehaviorCondition_FoundSelfWithBuffN\x10\x1e\x12+\n\'BehaviorCondition_FoundSelfWithoutBuffN\x10\x1f\x12!\n\x1d\x42\x65haviorCondition_HPDecreased\x10 \x12/\n+BehaviorCondition_AttackedOrDebuffedByEnemy\x10!\x12/\n+BehaviorCondition_EnemyEnterNGridsLineRange\x10\"\x12\x31\n-BehaviorCondition_NoEnemyEnterNGridsLineRange\x10#')
)

_ENUMBEHAVIORCONDITION = _descriptor.EnumDescriptor(
  name='ENUMBehaviorCondition',
  full_name='ConfigDataGroupBehavior.ENUMBehaviorCondition',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EnemyEnterMoveAndAttackRange', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_LeaderStartAttack', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EnemyHPPercentLessEqual', index=2, number=3,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_NoEnemyEnterMoveAndAttackRange', index=3, number=4,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EveryTurn', index=4, number=5,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_Never', index=5, number=6,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_TurnN', index=6, number=7,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_AttackedByEnemy', index=7, number=8,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EnemyEnterMoveAndAttackRangeOfMoveTarget', index=8, number=9,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_NoEnemyEnterMoveAndAttackRangeOfMoveTarget', index=9, number=10,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_DistanceToMoveTargetLestEqual', index=10, number=11,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_SelfHPPercentLessEqual', index=11, number=12,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_LeaderStopAttack', index=12, number=13,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_DoingBehaviorN', index=13, number=14,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_AttackedByEnemyInLastTurn', index=14, number=15,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_FoundEnemyWithBuffN', index=15, number=16,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_NoEnemyWithBuffN', index=16, number=17,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_MemberAttackedByEnemy', index=17, number=18,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_MemberHPPercentLessEqual', index=18, number=19,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EnemyEnterMoveAndAttackRangeExcept', index=19, number=20,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_NoEnemyEnterMoveAndAttackRangeExcept', index=20, number=21,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_SelfHPPercentGreaterEqual', index=21, number=22,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_HeroNExist', index=22, number=23,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_GroupMemberDead', index=23, number=24,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_TurnCountN', index=24, number=25,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EnemyBlockWayToMoveTarget', index=25, number=26,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_HasWayToMoveTarget', index=26, number=27,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_FoundMemberWithBuffN', index=27, number=28,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_NoMemberWithBuffN', index=28, number=29,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_FoundSelfWithBuffN', index=29, number=30,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_FoundSelfWithoutBuffN', index=30, number=31,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_HPDecreased', index=31, number=32,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_AttackedOrDebuffedByEnemy', index=32, number=33,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_EnemyEnterNGridsLineRange', index=33, number=34,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='BehaviorCondition_NoEnemyEnterNGridsLineRange', index=34, number=35,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=318,
  serialized_end=1918,
)
_sym_db.RegisterEnumDescriptor(_ENUMBEHAVIORCONDITION)

ENUMBehaviorCondition = enum_type_wrapper.EnumTypeWrapper(_ENUMBEHAVIORCONDITION)
BehaviorCondition_EnemyEnterMoveAndAttackRange = 1
BehaviorCondition_LeaderStartAttack = 2
BehaviorCondition_EnemyHPPercentLessEqual = 3
BehaviorCondition_NoEnemyEnterMoveAndAttackRange = 4
BehaviorCondition_EveryTurn = 5
BehaviorCondition_Never = 6
BehaviorCondition_TurnN = 7
BehaviorCondition_AttackedByEnemy = 8
BehaviorCondition_EnemyEnterMoveAndAttackRangeOfMoveTarget = 9
BehaviorCondition_NoEnemyEnterMoveAndAttackRangeOfMoveTarget = 10
BehaviorCondition_DistanceToMoveTargetLestEqual = 11
BehaviorCondition_SelfHPPercentLessEqual = 12
BehaviorCondition_LeaderStopAttack = 13
BehaviorCondition_DoingBehaviorN = 14
BehaviorCondition_AttackedByEnemyInLastTurn = 15
BehaviorCondition_FoundEnemyWithBuffN = 16
BehaviorCondition_NoEnemyWithBuffN = 17
BehaviorCondition_MemberAttackedByEnemy = 18
BehaviorCondition_MemberHPPercentLessEqual = 19
BehaviorCondition_EnemyEnterMoveAndAttackRangeExcept = 20
BehaviorCondition_NoEnemyEnterMoveAndAttackRangeExcept = 21
BehaviorCondition_SelfHPPercentGreaterEqual = 22
BehaviorCondition_HeroNExist = 23
BehaviorCondition_GroupMemberDead = 24
BehaviorCondition_TurnCountN = 25
BehaviorCondition_EnemyBlockWayToMoveTarget = 26
BehaviorCondition_HasWayToMoveTarget = 27
BehaviorCondition_FoundMemberWithBuffN = 28
BehaviorCondition_NoMemberWithBuffN = 29
BehaviorCondition_FoundSelfWithBuffN = 30
BehaviorCondition_FoundSelfWithoutBuffN = 31
BehaviorCondition_HPDecreased = 32
BehaviorCondition_AttackedOrDebuffedByEnemy = 33
BehaviorCondition_EnemyEnterNGridsLineRange = 34
BehaviorCondition_NoEnemyEnterNGridsLineRange = 35



_CONFIGDATAGROUPBEHAVIOR = _descriptor.Descriptor(
  name='ConfigDataGroupBehavior',
  full_name='ConfigDataGroupBehavior.ConfigDataGroupBehavior',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGroupBehavior.ConfigDataGroupBehavior.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SelectLeaderCondition', full_name='ConfigDataGroupBehavior.ConfigDataGroupBehavior.SelectLeaderCondition', index=1,
      number=4, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SLCParam', full_name='ConfigDataGroupBehavior.ConfigDataGroupBehavior.SLCParam', index=2,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='LeaderBehavior', full_name='ConfigDataGroupBehavior.ConfigDataGroupBehavior.LeaderBehavior', index=3,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MemberBehavior', full_name='ConfigDataGroupBehavior.ConfigDataGroupBehavior.MemberBehavior', index=4,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=59,
  serialized_end=241,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGroupBehavior.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGroupBehavior.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=243,
  serialized_end=315,
)

_CONFIGDATAGROUPBEHAVIOR.fields_by_name['SelectLeaderCondition'].enum_type = _ENUMBEHAVIORCONDITION
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGROUPBEHAVIOR
DESCRIPTOR.message_types_by_name['ConfigDataGroupBehavior'] = _CONFIGDATAGROUPBEHAVIOR
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMBehaviorCondition'] = _ENUMBEHAVIORCONDITION
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGroupBehavior = _reflection.GeneratedProtocolMessageType('ConfigDataGroupBehavior', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGROUPBEHAVIOR,
  '__module__' : 'ConfigDataGroupBehavior_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGroupBehavior.ConfigDataGroupBehavior)
  })
_sym_db.RegisterMessage(ConfigDataGroupBehavior)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGroupBehavior_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGroupBehavior.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
