# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataGuildMassiveCombatLevelInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataGuildMassiveCombatLevelInfo.proto',
  package='ConfigDataGuildMassiveCombatLevelInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n+ConfigDataGuildMassiveCombatLevelInfo.proto\x12%ConfigDataGuildMassiveCombatLevelInfo\"q\n%ConfigDataGuildMassiveCombatLevelInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x10\n\x08\x42\x61ttleId\x18\x03 \x02(\x05\x12\x18\n\x10MiniMapResources\x18\x04 \x02(\t\x12\x10\n\x08Strategy\x18\x05 \x02(\t\"d\n\x05Items\x12[\n\x05items\x18\x01 \x03(\x0b\x32L.ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo')
)




_CONFIGDATAGUILDMASSIVECOMBATLEVELINFO = _descriptor.Descriptor(
  name='ConfigDataGuildMassiveCombatLevelInfo',
  full_name='ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BattleId', full_name='ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo.BattleId', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='MiniMapResources', full_name='ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo.MiniMapResources', index=2,
      number=4, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Strategy', full_name='ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo.Strategy', index=3,
      number=5, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=86,
  serialized_end=199,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataGuildMassiveCombatLevelInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataGuildMassiveCombatLevelInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=201,
  serialized_end=301,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAGUILDMASSIVECOMBATLEVELINFO
DESCRIPTOR.message_types_by_name['ConfigDataGuildMassiveCombatLevelInfo'] = _CONFIGDATAGUILDMASSIVECOMBATLEVELINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataGuildMassiveCombatLevelInfo = _reflection.GeneratedProtocolMessageType('ConfigDataGuildMassiveCombatLevelInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAGUILDMASSIVECOMBATLEVELINFO,
  '__module__' : 'ConfigDataGuildMassiveCombatLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildMassiveCombatLevelInfo.ConfigDataGuildMassiveCombatLevelInfo)
  })
_sym_db.RegisterMessage(ConfigDataGuildMassiveCombatLevelInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataGuildMassiveCombatLevelInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataGuildMassiveCombatLevelInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
