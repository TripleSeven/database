# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataBattleRandomArmyInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataBattleRandomArmyInfo.proto',
  package='ConfigDataBattleRandomArmyInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n$ConfigDataBattleRandomArmyInfo.proto\x12\x1e\x43onfigDataBattleRandomArmyInfo\"O\n\x12SUBRandomArmyActor\x12\x0e\n\x06HeroID\x18\x01 \x02(\x05\x12\r\n\x05Level\x18\x02 \x02(\x05\x12\n\n\x02\x41I\x18\x03 \x02(\x05\x12\x0e\n\x06Weight\x18\x04 \x02(\x05\"z\n\x1e\x43onfigDataBattleRandomArmyInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12L\n\x10RandomArmyActors\x18\x03 \x03(\x0b\x32\x32.ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor\"V\n\x05Items\x12M\n\x05items\x18\x01 \x03(\x0b\x32>.ConfigDataBattleRandomArmyInfo.ConfigDataBattleRandomArmyInfo')
)




_SUBRANDOMARMYACTOR = _descriptor.Descriptor(
  name='SUBRandomArmyActor',
  full_name='ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='HeroID', full_name='ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor.HeroID', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Level', full_name='ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor.Level', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='AI', full_name='ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor.AI', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Weight', full_name='ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor.Weight', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=72,
  serialized_end=151,
)


_CONFIGDATABATTLERANDOMARMYINFO = _descriptor.Descriptor(
  name='ConfigDataBattleRandomArmyInfo',
  full_name='ConfigDataBattleRandomArmyInfo.ConfigDataBattleRandomArmyInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataBattleRandomArmyInfo.ConfigDataBattleRandomArmyInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='RandomArmyActors', full_name='ConfigDataBattleRandomArmyInfo.ConfigDataBattleRandomArmyInfo.RandomArmyActors', index=1,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=153,
  serialized_end=275,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataBattleRandomArmyInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataBattleRandomArmyInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=277,
  serialized_end=363,
)

_CONFIGDATABATTLERANDOMARMYINFO.fields_by_name['RandomArmyActors'].message_type = _SUBRANDOMARMYACTOR
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATABATTLERANDOMARMYINFO
DESCRIPTOR.message_types_by_name['SUBRandomArmyActor'] = _SUBRANDOMARMYACTOR
DESCRIPTOR.message_types_by_name['ConfigDataBattleRandomArmyInfo'] = _CONFIGDATABATTLERANDOMARMYINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBRandomArmyActor = _reflection.GeneratedProtocolMessageType('SUBRandomArmyActor', (_message.Message,), {
  'DESCRIPTOR' : _SUBRANDOMARMYACTOR,
  '__module__' : 'ConfigDataBattleRandomArmyInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataBattleRandomArmyInfo.SUBRandomArmyActor)
  })
_sym_db.RegisterMessage(SUBRandomArmyActor)

ConfigDataBattleRandomArmyInfo = _reflection.GeneratedProtocolMessageType('ConfigDataBattleRandomArmyInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATABATTLERANDOMARMYINFO,
  '__module__' : 'ConfigDataBattleRandomArmyInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataBattleRandomArmyInfo.ConfigDataBattleRandomArmyInfo)
  })
_sym_db.RegisterMessage(ConfigDataBattleRandomArmyInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataBattleRandomArmyInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataBattleRandomArmyInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
