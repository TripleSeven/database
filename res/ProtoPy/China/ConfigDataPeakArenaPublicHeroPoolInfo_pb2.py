# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataPeakArenaPublicHeroPoolInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataPeakArenaPublicHeroPoolInfo.proto',
  package='ConfigDataPeakArenaPublicHeroPoolInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n+ConfigDataPeakArenaPublicHeroPoolInfo.proto\x12%ConfigDataPeakArenaPublicHeroPoolInfo\"\xc8\x01\n%ConfigDataPeakArenaPublicHeroPoolInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12\x0e\n\x06HeroId\x18\x03 \x02(\x05\x12\x18\n\x10InclusiveMinRank\x18\x04 \x02(\x05\x12\x18\n\x10\x45xclusiveMaxRank\x18\x05 \x02(\x05\x12\x19\n\x11InclusiveMinRange\x18\x06 \x02(\x05\x12\x19\n\x11\x45xclusiveMaxRange\x18\x07 \x02(\x05\x12\x19\n\x11InclusiveMinPower\x18\x08 \x02(\x05\"d\n\x05Items\x12[\n\x05items\x18\x01 \x03(\x0b\x32L.ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo')
)




_CONFIGDATAPEAKARENAPUBLICHEROPOOLINFO = _descriptor.Descriptor(
  name='ConfigDataPeakArenaPublicHeroPoolInfo',
  full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='HeroId', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.HeroId', index=1,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='InclusiveMinRank', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.InclusiveMinRank', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ExclusiveMaxRank', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.ExclusiveMaxRank', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='InclusiveMinRange', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.InclusiveMinRange', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='ExclusiveMaxRange', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.ExclusiveMaxRange', index=5,
      number=7, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='InclusiveMinPower', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo.InclusiveMinPower', index=6,
      number=8, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=87,
  serialized_end=287,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataPeakArenaPublicHeroPoolInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataPeakArenaPublicHeroPoolInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=289,
  serialized_end=389,
)

_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAPEAKARENAPUBLICHEROPOOLINFO
DESCRIPTOR.message_types_by_name['ConfigDataPeakArenaPublicHeroPoolInfo'] = _CONFIGDATAPEAKARENAPUBLICHEROPOOLINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ConfigDataPeakArenaPublicHeroPoolInfo = _reflection.GeneratedProtocolMessageType('ConfigDataPeakArenaPublicHeroPoolInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAPEAKARENAPUBLICHEROPOOLINFO,
  '__module__' : 'ConfigDataPeakArenaPublicHeroPoolInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPeakArenaPublicHeroPoolInfo.ConfigDataPeakArenaPublicHeroPoolInfo)
  })
_sym_db.RegisterMessage(ConfigDataPeakArenaPublicHeroPoolInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataPeakArenaPublicHeroPoolInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataPeakArenaPublicHeroPoolInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
