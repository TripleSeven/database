# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ConfigDataHeroInteractionInfo.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ConfigDataHeroInteractionInfo.proto',
  package='ConfigDataHeroInteractionInfo',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=_b('\n#ConfigDataHeroInteractionInfo.proto\x12\x1d\x43onfigDataHeroInteractionInfo\"\x9c\x01\n\x1eSUBHeroInteractionWeightResult\x12P\n\nResultType\x18\x01 \x02(\x0e\x32<.ConfigDataHeroInteractionInfo.ENUMHeroInteractionResultType\x12\x0e\n\x06Weight\x18\x02 \x02(\x05\x12\x18\n\x10\x46\x61vourabilityExp\x18\x03 \x02(\x05\"\xde\x01\n\x1d\x43onfigDataHeroInteractionInfo\x12\n\n\x02ID\x18\x02 \x02(\x05\x12N\n\x07Results\x18\x03 \x03(\x0b\x32=.ConfigDataHeroInteractionInfo.SUBHeroInteractionWeightResult\x12\x1f\n\x17NormalResultPerformance\x18\x04 \x02(\x05\x12 \n\x18SmallUpResultPerformance\x18\x05 \x02(\x05\x12\x1e\n\x16\x42igUpResultPerformance\x18\x06 \x02(\x05\"T\n\x05Items\x12K\n\x05items\x18\x01 \x03(\x0b\x32<.ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo*\xb4\x01\n\x1d\x45NUMHeroInteractionResultType\x12\"\n\x1eHeroInteractionResultType_None\x10\x00\x12#\n\x1fHeroInteractionResultType_Norml\x10\x01\x12%\n!HeroInteractionResultType_SmallUp\x10\x02\x12#\n\x1fHeroInteractionResultType_BigUp\x10\x03')
)

_ENUMHEROINTERACTIONRESULTTYPE = _descriptor.EnumDescriptor(
  name='ENUMHeroInteractionResultType',
  full_name='ConfigDataHeroInteractionInfo.ENUMHeroInteractionResultType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='HeroInteractionResultType_None', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HeroInteractionResultType_Norml', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HeroInteractionResultType_SmallUp', index=2, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='HeroInteractionResultType_BigUp', index=3, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=541,
  serialized_end=721,
)
_sym_db.RegisterEnumDescriptor(_ENUMHEROINTERACTIONRESULTTYPE)

ENUMHeroInteractionResultType = enum_type_wrapper.EnumTypeWrapper(_ENUMHEROINTERACTIONRESULTTYPE)
HeroInteractionResultType_None = 0
HeroInteractionResultType_Norml = 1
HeroInteractionResultType_SmallUp = 2
HeroInteractionResultType_BigUp = 3



_SUBHEROINTERACTIONWEIGHTRESULT = _descriptor.Descriptor(
  name='SUBHeroInteractionWeightResult',
  full_name='ConfigDataHeroInteractionInfo.SUBHeroInteractionWeightResult',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ResultType', full_name='ConfigDataHeroInteractionInfo.SUBHeroInteractionWeightResult.ResultType', index=0,
      number=1, type=14, cpp_type=8, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Weight', full_name='ConfigDataHeroInteractionInfo.SUBHeroInteractionWeightResult.Weight', index=1,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='FavourabilityExp', full_name='ConfigDataHeroInteractionInfo.SUBHeroInteractionWeightResult.FavourabilityExp', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=71,
  serialized_end=227,
)


_CONFIGDATAHEROINTERACTIONINFO = _descriptor.Descriptor(
  name='ConfigDataHeroInteractionInfo',
  full_name='ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='ID', full_name='ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo.ID', index=0,
      number=2, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Results', full_name='ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo.Results', index=1,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='NormalResultPerformance', full_name='ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo.NormalResultPerformance', index=2,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='SmallUpResultPerformance', full_name='ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo.SmallUpResultPerformance', index=3,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='BigUpResultPerformance', full_name='ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo.BigUpResultPerformance', index=4,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=230,
  serialized_end=452,
)


_ITEMS = _descriptor.Descriptor(
  name='Items',
  full_name='ConfigDataHeroInteractionInfo.Items',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='items', full_name='ConfigDataHeroInteractionInfo.Items.items', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=454,
  serialized_end=538,
)

_SUBHEROINTERACTIONWEIGHTRESULT.fields_by_name['ResultType'].enum_type = _ENUMHEROINTERACTIONRESULTTYPE
_CONFIGDATAHEROINTERACTIONINFO.fields_by_name['Results'].message_type = _SUBHEROINTERACTIONWEIGHTRESULT
_ITEMS.fields_by_name['items'].message_type = _CONFIGDATAHEROINTERACTIONINFO
DESCRIPTOR.message_types_by_name['SUBHeroInteractionWeightResult'] = _SUBHEROINTERACTIONWEIGHTRESULT
DESCRIPTOR.message_types_by_name['ConfigDataHeroInteractionInfo'] = _CONFIGDATAHEROINTERACTIONINFO
DESCRIPTOR.message_types_by_name['Items'] = _ITEMS
DESCRIPTOR.enum_types_by_name['ENUMHeroInteractionResultType'] = _ENUMHEROINTERACTIONRESULTTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

SUBHeroInteractionWeightResult = _reflection.GeneratedProtocolMessageType('SUBHeroInteractionWeightResult', (_message.Message,), {
  'DESCRIPTOR' : _SUBHEROINTERACTIONWEIGHTRESULT,
  '__module__' : 'ConfigDataHeroInteractionInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroInteractionInfo.SUBHeroInteractionWeightResult)
  })
_sym_db.RegisterMessage(SUBHeroInteractionWeightResult)

ConfigDataHeroInteractionInfo = _reflection.GeneratedProtocolMessageType('ConfigDataHeroInteractionInfo', (_message.Message,), {
  'DESCRIPTOR' : _CONFIGDATAHEROINTERACTIONINFO,
  '__module__' : 'ConfigDataHeroInteractionInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroInteractionInfo.ConfigDataHeroInteractionInfo)
  })
_sym_db.RegisterMessage(ConfigDataHeroInteractionInfo)

Items = _reflection.GeneratedProtocolMessageType('Items', (_message.Message,), {
  'DESCRIPTOR' : _ITEMS,
  '__module__' : 'ConfigDataHeroInteractionInfo_pb2'
  # @@protoc_insertion_point(class_scope:ConfigDataHeroInteractionInfo.Items)
  })
_sym_db.RegisterMessage(Items)


# @@protoc_insertion_point(module_scope)
