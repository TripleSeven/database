﻿// Decompiled with JetBrains decompiler
// Type: PDWebViewManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using ZenFulcrum.EmbeddedBrowser;

public class PDWebViewManager : MonoBehaviour
{
  private string m_CurrentUrl = string.Empty;
  private string m_PageType = string.Empty;
  public RawImage MaskGraphic;
  public static PDWebViewManager Manager;
  private PDWebCallBackHandler m_callbackHandler;
  private Browser m_WebBrowser;
  private GameObject m_WebviewCloseBtn;
  private GameObject m_WebviewCloseBg;

  [MethodImpl((MethodImplOptions) 32768)]
  public PDWebViewManager()
  {
  }

  private Browser WebBrowser
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      if ((UnityEngine.Object) this.m_WebBrowser == (UnityEngine.Object) null)
      {
        GameObject gameObject = this.transform.Find("BrowserGUI").gameObject;
        if ((bool) ((UnityEngine.Object) gameObject) && (bool) ((UnityEngine.Object) gameObject.GetComponent<Browser>()))
          this.m_WebBrowser = gameObject.GetComponent<Browser>();
      }
      return this.m_WebBrowser;
    }
  }

  private GameObject WebviewCloseBtn
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  private GameObject WebviewCloseBg
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ShowWebviewCloseBtn(bool isShow, string pageType = "")
  {
    // ISSUE: unable to decompile the method.
  }

  public void ShowWebviewCloseBg(bool isShow)
  {
    this.WebviewCloseBg.SetActive(isShow);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Awake()
  {
    if (!((UnityEngine.Object) null == (UnityEngine.Object) PDWebViewManager.Manager))
      return;
    UserAgent.SetUserAgent("untiypcsdk");
    BrowserNative.ProfilePath = Application.dataPath + "/BrowserProfile/";
    PDWebViewManager.Manager = this;
    this.m_callbackHandler = new PDWebCallBackHandler();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Start()
  {
    this.WebBrowser.onNavStateChange += (Action) (() =>
    {
      // ISSUE: unable to decompile the method.
    });
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void RegisterJSFuncAll()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void OpenWebBrowser(string url, float width = 0.0f, float height = 0.0f)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void CloseWebBrowser()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ClearCookie()
  {
    // ISSUE: unable to decompile the method.
  }

  [DebuggerHidden]
  [MethodImpl((MethodImplOptions) 32768)]
  private IEnumerator DelayShow()
  {
    // ISSUE: unable to decompile the method.
  }
}
