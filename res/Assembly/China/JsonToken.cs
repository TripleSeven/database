﻿// Decompiled with JetBrains decompiler
// Type: JsonToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

public enum JsonToken
{
  None,
  ObjectStart,
  PropertyName,
  ObjectEnd,
  ArrayStart,
  ArrayEnd,
  Int,
  Long,
  Double,
  String,
  Boolean,
  Null,
}
