﻿// Decompiled with JetBrains decompiler
// Type: SaveCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

public class SaveCmd : IDebugCmd
{
  public void Execute(string strParams)
  {
    DebugConsoleMode.instance.Save();
  }

  public string GetHelpDesc()
  {
    return "save : Save the current log";
  }

  public string GetName()
  {
    return "Save";
  }
}
