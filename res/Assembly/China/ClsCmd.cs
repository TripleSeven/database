﻿// Decompiled with JetBrains decompiler
// Type: ClsCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

public class ClsCmd : IDebugCmd
{
  private const string _NAME = "cls";
  private const string _DESC = "cls: clear console screen.";

  public void Execute(string strParams)
  {
    DebugConsoleMode.instance.ClearLog();
  }

  public string GetHelpDesc()
  {
    return "cls: clear console screen.";
  }

  public string GetName()
  {
    return "cls";
  }
}
