﻿// Decompiled with JetBrains decompiler
// Type: Debug
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Log;
using SLua;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

[CustomLuaClass]
public static class Debug
{
  public static Thread m_mainThread;

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Log(string str)
  {
    if (Debug.m_mainThread == null || LogManager.Instance == null)
    {
      UnityEngine.Debug.Log((object) str);
    }
    else
    {
      if (LogManager.Instance.NeedFileLog)
        LogManager.Instance.FileLogger.WriteLog(str, "D");
      if (Thread.CurrentThread != Debug.m_mainThread || !LogManager.Instance.NeedEngineLog)
        return;
      LogManager.Instance.IsCallingEngineLog = true;
      UnityEngine.Debug.Log((object) str);
      LogManager.Instance.IsCallingEngineLog = false;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Log(params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogWarning(string str)
  {
    if (Debug.m_mainThread == null || LogManager.Instance == null)
    {
      UnityEngine.Debug.LogWarning((object) str);
    }
    else
    {
      if (LogManager.Instance.NeedFileLog)
        LogManager.Instance.FileLogger.WriteLog(str, "W");
      if (Thread.CurrentThread != Debug.m_mainThread || !LogManager.Instance.NeedEngineLog)
        return;
      LogManager.Instance.IsCallingEngineLog = true;
      UnityEngine.Debug.LogWarning((object) str);
      LogManager.Instance.IsCallingEngineLog = false;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogWarning(params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogError(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogError(params object[] paramList)
  {
    Debug.LogError(Debug.ParamListToString(paramList));
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Assert(bool value, string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Assert(bool value, params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void WriteLine(string str)
  {
    Debug.Log(str);
  }

  public static void WriteLine(params object[] paramList)
  {
    Debug.Log(paramList);
  }

  public static void SystemLogException(params object[] paramList)
  {
    Debug.LogError(paramList);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Break()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static string ParamListToString(object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(
    Vector3 start,
    Vector3 end,
    Color color,
    float duration,
    bool depthTest)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(Vector3 start, Vector3 end, Color color)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(Vector3 start, Vector3 end)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(
    Vector3 start,
    Vector3 dir,
    Color color,
    float duration,
    bool depthTest)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(Vector3 start, Vector3 dir, Color color)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(Vector3 start, Vector3 dir)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
  {
    // ISSUE: unable to decompile the method.
  }
}
