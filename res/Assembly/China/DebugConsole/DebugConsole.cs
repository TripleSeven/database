﻿// Decompiled with JetBrains decompiler
// Type: DebugConsole.DebugConsole
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace DebugConsole
{
  public class DebugConsole : MonoBehaviour
  {
    private DebugConsoleView _consoleView;
    public static DebugConsole.DebugConsole instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LogPlatformDefine()
    {
      Debug.Log(string.Format("Application.platform = {0}", (object) Application.platform));
      Debug.Log("UNITY_STANDALONE define.");
    }

    private void Awake()
    {
      DebugConsole.DebugConsole.instance = this;
      this.Init();
    }

    private void Start()
    {
      DebugConsole.DebugConsole.LogPlatformDefine();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      this._consoleView = DebugConsoleView.Create();
      DebugConsoleCtrl.Create(this._consoleView, DebugConsoleMode.Create());
    }

    protected void OnGUI()
    {
      this._consoleView.Show();
    }

    private void OnDestroy()
    {
      DebugConsole.DebugConsole.instance = (DebugConsole.DebugConsole) null;
    }
  }
}
