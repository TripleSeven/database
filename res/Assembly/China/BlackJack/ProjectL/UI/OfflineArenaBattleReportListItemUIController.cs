﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.OfflineArenaBattleReportListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class OfflineArenaBattleReportListItemUIController : UIControllerBase
  {
    [AutoBind("./PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./ModeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_modeUIStateCtrl;
    [AutoBind("./TimeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointsText;
    [AutoBind("./ButtonGroup/RevengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_revengeButton;
    [AutoBind("./ButtonGroup/HaveRevengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_haveRevengeButton;
    [AutoBind("./ButtonGroup/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_replayButton;
    [AutoBind("./ButtonGroup/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_replayUIStateController;
    private ArenaBattleReport m_battleReport;
    [DoNotToLua]
    private OfflineArenaBattleReportListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetArenaBattleReportArenaBattleReport_hotfix;
    private LuaFunction m_GetArenaBattleReport_hotfix;
    private LuaFunction m_OnRevengeButtonClick_hotfix;
    private LuaFunction m_OnHaveRevengeButtonClick_hotfix;
    private LuaFunction m_OnReplayButtonClick_hotfix;
    private LuaFunction m_add_EventOnRevengeButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnRevengeButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnReplayButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnReplayButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleReport(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport GetArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRevengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHaveRevengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<OfflineArenaBattleReportListItemUIController> EventOnRevengeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OfflineArenaBattleReportListItemUIController> EventOnReplayButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public OfflineArenaBattleReportListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRevengeButtonClick(
      OfflineArenaBattleReportListItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRevengeButtonClick(
      OfflineArenaBattleReportListItemUIController obj)
    {
      this.EventOnRevengeButtonClick = (Action<OfflineArenaBattleReportListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReplayButtonClick(
      OfflineArenaBattleReportListItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReplayButtonClick(
      OfflineArenaBattleReportListItemUIController obj)
    {
      this.EventOnReplayButtonClick = (Action<OfflineArenaBattleReportListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private OfflineArenaBattleReportListItemUIController m_owner;

      public LuaExportHelper(OfflineArenaBattleReportListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnRevengeButtonClick(
        OfflineArenaBattleReportListItemUIController obj)
      {
        this.m_owner.__callDele_EventOnRevengeButtonClick(obj);
      }

      public void __clearDele_EventOnRevengeButtonClick(
        OfflineArenaBattleReportListItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnRevengeButtonClick(obj);
      }

      public void __callDele_EventOnReplayButtonClick(
        OfflineArenaBattleReportListItemUIController obj)
      {
        this.m_owner.__callDele_EventOnReplayButtonClick(obj);
      }

      public void __clearDele_EventOnReplayButtonClick(
        OfflineArenaBattleReportListItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnReplayButtonClick(obj);
      }

      public Image m_playerIconImage
      {
        get
        {
          return this.m_owner.m_playerIconImage;
        }
        set
        {
          this.m_owner.m_playerIconImage = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Text m_playerLevelText
      {
        get
        {
          return this.m_owner.m_playerLevelText;
        }
        set
        {
          this.m_owner.m_playerLevelText = value;
        }
      }

      public CommonUIStateController m_modeUIStateCtrl
      {
        get
        {
          return this.m_owner.m_modeUIStateCtrl;
        }
        set
        {
          this.m_owner.m_modeUIStateCtrl = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public Text m_arenaPointsText
      {
        get
        {
          return this.m_owner.m_arenaPointsText;
        }
        set
        {
          this.m_owner.m_arenaPointsText = value;
        }
      }

      public Button m_revengeButton
      {
        get
        {
          return this.m_owner.m_revengeButton;
        }
        set
        {
          this.m_owner.m_revengeButton = value;
        }
      }

      public Button m_haveRevengeButton
      {
        get
        {
          return this.m_owner.m_haveRevengeButton;
        }
        set
        {
          this.m_owner.m_haveRevengeButton = value;
        }
      }

      public Button m_replayButton
      {
        get
        {
          return this.m_owner.m_replayButton;
        }
        set
        {
          this.m_owner.m_replayButton = value;
        }
      }

      public CommonUIStateController m_replayUIStateController
      {
        get
        {
          return this.m_owner.m_replayUIStateController;
        }
        set
        {
          this.m_owner.m_replayUIStateController = value;
        }
      }

      public ArenaBattleReport m_battleReport
      {
        get
        {
          return this.m_owner.m_battleReport;
        }
        set
        {
          this.m_owner.m_battleReport = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnRevengeButtonClick()
      {
        this.m_owner.OnRevengeButtonClick();
      }

      public void OnHaveRevengeButtonClick()
      {
        this.m_owner.OnHaveRevengeButtonClick();
      }

      public void OnReplayButtonClick()
      {
        this.m_owner.OnReplayButtonClick();
      }
    }
  }
}
