﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.OnDragTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class OnDragTrigger : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
  {
    private int m_pixelInterval;
    private GameObject m_touchImage;
    private Slider m_slider;
    private Slider m_effectSlider;
    public int m_totalNeedMovePixel;
    private float m_curMovePixel;
    private bool m_isDrag;
    private bool m_hasDoneEvent;
    private List<int> m_heroPerformanceIds;
    private int m_lastCount;
    private int m_lastHeroPerformanceId;
    private IAudioPlayback m_audioPlayback;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private OnDragTrigger.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_Start_hotfix;
    private LuaFunction m_SetSliderAndEffectSliderSliderSliderList`1_hotfix;
    private LuaFunction m_OnPointerClickPointerEventData_hotfix;
    private LuaFunction m_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_SliderValueDown_hotfix;
    private LuaFunction m_OnDragPointerEventData_hotfix;
    private LuaFunction m_SetDragObjPostionPointerEventData_hotfix;
    private LuaFunction m_add_EventOnBeginDragAction_hotfix;
    private LuaFunction m_remove_EventOnBeginDragAction_hotfix;
    private LuaFunction m_add_EventOnEndDragAction_hotfix;
    private LuaFunction m_remove_EventOnEndDragAction_hotfix;
    private LuaFunction m_add_EventOnClickAction_hotfix;
    private LuaFunction m_remove_EventOnClickAction_hotfix;
    private LuaFunction m_add_EventOnSliderFullAction_hotfix;
    private LuaFunction m_remove_EventOnSliderFullAction_hotfix;
    private LuaFunction m_add_EventOnSliderEmptyAction_hotfix;
    private LuaFunction m_remove_EventOnSliderEmptyAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public OnDragTrigger()
    {
      OnDragTrigger onDragTrigger = this;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSliderAndEffectSlider(
      Slider slider,
      Slider effectSlider,
      List<int> heroPerformanceIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SliderValueDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDragObjPostion(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSliderFull
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSliderEmpty
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public OnDragTrigger.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBeginDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBeginDrag()
    {
      this.EventOnBeginDrag = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndDrag()
    {
      this.EventOnEndDrag = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick()
    {
      this.EventOnClick = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSliderFull()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSliderFull()
    {
      this.EventOnSliderFull = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSliderEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSliderEmpty()
    {
      this.EventOnSliderEmpty = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private OnDragTrigger m_owner;

      public LuaExportHelper(OnDragTrigger owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnBeginDrag()
      {
        this.m_owner.__callDele_EventOnBeginDrag();
      }

      public void __clearDele_EventOnBeginDrag()
      {
        this.m_owner.__clearDele_EventOnBeginDrag();
      }

      public void __callDele_EventOnEndDrag()
      {
        this.m_owner.__callDele_EventOnEndDrag();
      }

      public void __clearDele_EventOnEndDrag()
      {
        this.m_owner.__clearDele_EventOnEndDrag();
      }

      public void __callDele_EventOnClick()
      {
        this.m_owner.__callDele_EventOnClick();
      }

      public void __clearDele_EventOnClick()
      {
        this.m_owner.__clearDele_EventOnClick();
      }

      public void __callDele_EventOnSliderFull()
      {
        this.m_owner.__callDele_EventOnSliderFull();
      }

      public void __clearDele_EventOnSliderFull()
      {
        this.m_owner.__clearDele_EventOnSliderFull();
      }

      public void __callDele_EventOnSliderEmpty()
      {
        this.m_owner.__callDele_EventOnSliderEmpty();
      }

      public void __clearDele_EventOnSliderEmpty()
      {
        this.m_owner.__clearDele_EventOnSliderEmpty();
      }

      public int m_pixelInterval
      {
        get
        {
          return this.m_owner.m_pixelInterval;
        }
        set
        {
          this.m_owner.m_pixelInterval = value;
        }
      }

      public GameObject m_touchImage
      {
        get
        {
          return this.m_owner.m_touchImage;
        }
        set
        {
          this.m_owner.m_touchImage = value;
        }
      }

      public Slider m_slider
      {
        get
        {
          return this.m_owner.m_slider;
        }
        set
        {
          this.m_owner.m_slider = value;
        }
      }

      public Slider m_effectSlider
      {
        get
        {
          return this.m_owner.m_effectSlider;
        }
        set
        {
          this.m_owner.m_effectSlider = value;
        }
      }

      public float m_curMovePixel
      {
        get
        {
          return this.m_owner.m_curMovePixel;
        }
        set
        {
          this.m_owner.m_curMovePixel = value;
        }
      }

      public bool m_isDrag
      {
        get
        {
          return this.m_owner.m_isDrag;
        }
        set
        {
          this.m_owner.m_isDrag = value;
        }
      }

      public bool m_hasDoneEvent
      {
        get
        {
          return this.m_owner.m_hasDoneEvent;
        }
        set
        {
          this.m_owner.m_hasDoneEvent = value;
        }
      }

      public List<int> m_heroPerformanceIds
      {
        get
        {
          return this.m_owner.m_heroPerformanceIds;
        }
        set
        {
          this.m_owner.m_heroPerformanceIds = value;
        }
      }

      public int m_lastCount
      {
        get
        {
          return this.m_owner.m_lastCount;
        }
        set
        {
          this.m_owner.m_lastCount = value;
        }
      }

      public int m_lastHeroPerformanceId
      {
        get
        {
          return this.m_owner.m_lastHeroPerformanceId;
        }
        set
        {
          this.m_owner.m_lastHeroPerformanceId = value;
        }
      }

      public IAudioPlayback m_audioPlayback
      {
        get
        {
          return this.m_owner.m_audioPlayback;
        }
        set
        {
          this.m_owner.m_audioPlayback = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void Start()
      {
        this.m_owner.Start();
      }

      public IEnumerator SliderValueDown()
      {
        return this.m_owner.SliderValueDown();
      }

      public void SetDragObjPostion(PointerEventData eventData)
      {
        this.m_owner.SetDragObjPostion(eventData);
      }
    }
  }
}
