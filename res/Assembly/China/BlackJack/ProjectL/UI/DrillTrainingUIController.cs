﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillTrainingUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class DrillTrainingUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./PlayerResource/Crystal/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_crystalText;
    [AutoBind("./PlayerResource/Crystal/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_crystalAddButton;
    [AutoBind("./PlayerResource/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./PlayerResource/Gold/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddButton;
    [AutoBind("./TrainingNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingNameText;
    [AutoBind("./TitleInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_roomInfoImageStateCtrl;
    [AutoBind("./TitleInfoGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_roomInfoLvValueText;
    [AutoBind("./TitleInfoGroup/ExpValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_roomInfoExpValueText;
    [AutoBind("./TitleInfoGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_roomInfoProgressBar;
    [AutoBind("./TrainingEventScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trainingCourseScrollViewContent;
    [AutoBind("./SkillPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillPanelGameObject;
    [AutoBind("./Prefab/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillItemPrefab;
    [AutoBind("./SkillItemInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillItemInfoPanelStateCtrl;
    [AutoBind("./SkillItemInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelBGButton;
    [AutoBind("./SkillItemInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelCloseButton;
    [AutoBind("./SkillItemInfoPanel/FastMaxButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelFastMaxButton;
    [AutoBind("./SkillItemInfoPanel/FastMaxButton/FastLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelFastLevelButton;
    [AutoBind("./SkillItemInfoPanel/FastMaxButton/FastLevelButton/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_skillItemInfoPanelFastLevelInputField;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailPanel;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/TypeAndQuality/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailPanelSoldierTypeIcon;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/TypeAndQuality/QualityIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailPanelSoldierQualityIcon;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/TypeAndQuality/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelSoldierNameText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupHPValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupATValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupDFValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupMagicDFValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupRangeValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Move/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupMoveValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupRestrainValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Week/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupWeakValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Type", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelPorpretyGroupTypeStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupDescText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/HeroList", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelHeroListStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/HeroList/HeroList/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailPanelHeroListContent;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierAndSkillInfoSkillIconImage;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SkillGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoSkillNameText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SoldierGroup/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoSoldierGraphic;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SoldierGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoSoldierNameText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SkillGroup/FrameImage/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoSkillUpdateEffectGroup;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierAndSkillInfoPanelStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/WatchDetailButton/WatchDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierAndSkillInfoPanelWatchDetailButton;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/WatchDetailButton/WatchDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelDetailStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/NowEffectGroup/NowLvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelNowLvValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/NowEffectGroup/NowEffectTextScroll/Mask/NowEffectText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelNowEffectText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/IntensifyEffectGroup/IntensifyLvValueTexxt", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelIntensifyLvValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/IntensifyEffectGroup/NowEffectTextScroll/Mask/NowEffectText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelIntensifyNowEffectText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/AfterIntensifyEffectGroup/AfterIntensifyLvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelAfterIntensifyLvValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/AfterIntensifyEffectGroup/TextScroll/Mask/AfterEffectText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelAfterIntensifyEffectText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/ItemGroup/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoPanelItemGroup;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/PromoteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierAndSkillInfoPanelPromoteButton;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/PromoteButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierAndSkillInfoPanelPromoteButtonStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelGoldenValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierAndSkillInfoPanelGoldenStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoPanelConditionGroup;
    [AutoBind("./SkillItemInfoPanel/EnhanceSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enhanceSuccessEffectPanelStateCtrl;
    private bool m_isFirstIn;
    private UISpineGraphic m_soldierInfoGraphic;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private TrainingRoom m_curTrainingRoom;
    private TrainingCourse m_curTrainingCourse;
    private TrainingSkillItemUIController m_curTrainingSkillItemCtrl;
    private CourseItemUIController m_lastCourseItemCtrl;
    private List<TrainingSkillItemUIController> m_trainingSkillItemUICtrlList;
    [DoNotToLua]
    private DrillTrainingUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateViewInDrillTrainingInt32Int32_hotfix;
    private LuaFunction m_ShowTechClickPanelByTechIdInt32_hotfix;
    private LuaFunction m_SetRoomCoachInfoPanel_hotfix;
    private LuaFunction m_SetCoursesListPanel_hotfix;
    private LuaFunction m_OnCourseItemClickCourseItemUIController_hotfix;
    private LuaFunction m_SetSkillPanel_hotfix;
    private LuaFunction m_OnTrainingSkillItemClickTrainingSkillItemUIController_hotfix;
    private LuaFunction m_SetSkillItemInfoPanelTrainingSkillItemUIController_hotfix;
    private LuaFunction m_OnEvolutionMaterialClickGoodsTypeInt32Int32_hotfix;
    private LuaFunction m_SetSoldierDetailPanelConfigDataSoldierInfo_hotfix;
    private LuaFunction m_OnInfoPanelPromoteButtonClick_hotfix;
    private LuaFunction m_OnShowSoldierDetailButtonClick_hotfix;
    private LuaFunction m_OnSkillItemInfoPanelBGButtonClick_hotfix;
    private LuaFunction m_OnGoldAddButtonClick_hotfix;
    private LuaFunction m_OnCrystalAddButtonClick_hotfix;
    private LuaFunction m_OnSkillItemInfoPanelFastMaxButtonClick_hotfix;
    private LuaFunction m_OnSkillItemInfoPanelFastLevelButtonClick_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnAddGoldAction_hotfix;
    private LuaFunction m_remove_EventOnAddGoldAction_hotfix;
    private LuaFunction m_add_EventOnAddCrystalAction_hotfix;
    private LuaFunction m_remove_EventOnAddCrystalAction_hotfix;
    private LuaFunction m_add_EventOnTechLevelupAction`2_hotfix;
    private LuaFunction m_remove_EventOnTechLevelupAction`2_hotfix;
    private LuaFunction m_add_EventOnEvolutionMaterialClickAction`4_hotfix;
    private LuaFunction m_remove_EventOnEvolutionMaterialClickAction`4_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillTrainingUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInDrillTraining(int courseId, int techId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTechClickPanelByTechId(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRoomCoachInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCoursesListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCourseItemClick(CourseItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingSkillItemClick(TrainingSkillItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillItemInfoPanel(TrainingSkillItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEvolutionMaterialClick(GoodsType goodsType, int id, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoPanelPromoteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowSoldierDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemInfoPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCrystalAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemInfoPanelFastMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemInfoPanelFastLevelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddGold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnTechLevelup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int, int> EventOnEvolutionMaterialClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public DrillTrainingUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddGold()
    {
      this.EventOnAddGold = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddCrystal()
    {
      this.EventOnAddCrystal = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTechLevelup(int arg1, Action arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTechLevelup(int arg1, Action arg2)
    {
      this.EventOnTechLevelup = (Action<int, Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEvolutionMaterialClick(
      GoodsType arg1,
      int arg2,
      int arg3,
      int arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEvolutionMaterialClick(
      GoodsType arg1,
      int arg2,
      int arg3,
      int arg4)
    {
      this.EventOnEvolutionMaterialClick = (Action<GoodsType, int, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private DrillTrainingUIController m_owner;

      public LuaExportHelper(DrillTrainingUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnAddGold()
      {
        this.m_owner.__callDele_EventOnAddGold();
      }

      public void __clearDele_EventOnAddGold()
      {
        this.m_owner.__clearDele_EventOnAddGold();
      }

      public void __callDele_EventOnAddCrystal()
      {
        this.m_owner.__callDele_EventOnAddCrystal();
      }

      public void __clearDele_EventOnAddCrystal()
      {
        this.m_owner.__clearDele_EventOnAddCrystal();
      }

      public void __callDele_EventOnTechLevelup(int arg1, Action arg2)
      {
        this.m_owner.__callDele_EventOnTechLevelup(arg1, arg2);
      }

      public void __clearDele_EventOnTechLevelup(int arg1, Action arg2)
      {
        this.m_owner.__clearDele_EventOnTechLevelup(arg1, arg2);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __callDele_EventOnEvolutionMaterialClick(
        GoodsType arg1,
        int arg2,
        int arg3,
        int arg4)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void __clearDele_EventOnEvolutionMaterialClick(
        GoodsType arg1,
        int arg2,
        int arg3,
        int arg4)
      {
        // ISSUE: unable to decompile the method.
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Text m_crystalText
      {
        get
        {
          return this.m_owner.m_crystalText;
        }
        set
        {
          this.m_owner.m_crystalText = value;
        }
      }

      public Button m_crystalAddButton
      {
        get
        {
          return this.m_owner.m_crystalAddButton;
        }
        set
        {
          this.m_owner.m_crystalAddButton = value;
        }
      }

      public Text m_goldText
      {
        get
        {
          return this.m_owner.m_goldText;
        }
        set
        {
          this.m_owner.m_goldText = value;
        }
      }

      public Button m_goldAddButton
      {
        get
        {
          return this.m_owner.m_goldAddButton;
        }
        set
        {
          this.m_owner.m_goldAddButton = value;
        }
      }

      public Text m_trainingNameText
      {
        get
        {
          return this.m_owner.m_trainingNameText;
        }
        set
        {
          this.m_owner.m_trainingNameText = value;
        }
      }

      public CommonUIStateController m_roomInfoImageStateCtrl
      {
        get
        {
          return this.m_owner.m_roomInfoImageStateCtrl;
        }
        set
        {
          this.m_owner.m_roomInfoImageStateCtrl = value;
        }
      }

      public Text m_roomInfoLvValueText
      {
        get
        {
          return this.m_owner.m_roomInfoLvValueText;
        }
        set
        {
          this.m_owner.m_roomInfoLvValueText = value;
        }
      }

      public Text m_roomInfoExpValueText
      {
        get
        {
          return this.m_owner.m_roomInfoExpValueText;
        }
        set
        {
          this.m_owner.m_roomInfoExpValueText = value;
        }
      }

      public Image m_roomInfoProgressBar
      {
        get
        {
          return this.m_owner.m_roomInfoProgressBar;
        }
        set
        {
          this.m_owner.m_roomInfoProgressBar = value;
        }
      }

      public GameObject m_trainingCourseScrollViewContent
      {
        get
        {
          return this.m_owner.m_trainingCourseScrollViewContent;
        }
        set
        {
          this.m_owner.m_trainingCourseScrollViewContent = value;
        }
      }

      public GameObject m_skillPanelGameObject
      {
        get
        {
          return this.m_owner.m_skillPanelGameObject;
        }
        set
        {
          this.m_owner.m_skillPanelGameObject = value;
        }
      }

      public GameObject m_skillItemPrefab
      {
        get
        {
          return this.m_owner.m_skillItemPrefab;
        }
        set
        {
          this.m_owner.m_skillItemPrefab = value;
        }
      }

      public CommonUIStateController m_skillItemInfoPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_skillItemInfoPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_skillItemInfoPanelStateCtrl = value;
        }
      }

      public Button m_skillItemInfoPanelBGButton
      {
        get
        {
          return this.m_owner.m_skillItemInfoPanelBGButton;
        }
        set
        {
          this.m_owner.m_skillItemInfoPanelBGButton = value;
        }
      }

      public Button m_skillItemInfoPanelCloseButton
      {
        get
        {
          return this.m_owner.m_skillItemInfoPanelCloseButton;
        }
        set
        {
          this.m_owner.m_skillItemInfoPanelCloseButton = value;
        }
      }

      public Button m_skillItemInfoPanelFastMaxButton
      {
        get
        {
          return this.m_owner.m_skillItemInfoPanelFastMaxButton;
        }
        set
        {
          this.m_owner.m_skillItemInfoPanelFastMaxButton = value;
        }
      }

      public Button m_skillItemInfoPanelFastLevelButton
      {
        get
        {
          return this.m_owner.m_skillItemInfoPanelFastLevelButton;
        }
        set
        {
          this.m_owner.m_skillItemInfoPanelFastLevelButton = value;
        }
      }

      public InputField m_skillItemInfoPanelFastLevelInputField
      {
        get
        {
          return this.m_owner.m_skillItemInfoPanelFastLevelInputField;
        }
        set
        {
          this.m_owner.m_skillItemInfoPanelFastLevelInputField = value;
        }
      }

      public GameObject m_soldierDetailPanel
      {
        get
        {
          return this.m_owner.m_soldierDetailPanel;
        }
        set
        {
          this.m_owner.m_soldierDetailPanel = value;
        }
      }

      public CommonUIStateController m_soldierDetailPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelStateCtrl = value;
        }
      }

      public Image m_soldierDetailPanelSoldierTypeIcon
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelSoldierTypeIcon;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelSoldierTypeIcon = value;
        }
      }

      public Image m_soldierDetailPanelSoldierQualityIcon
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelSoldierQualityIcon;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelSoldierQualityIcon = value;
        }
      }

      public Text m_soldierDetailPanelSoldierNameText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelSoldierNameText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelSoldierNameText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupHPValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupHPValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupHPValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupATValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupATValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupATValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupDFValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupDFValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupDFValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupMagicDFValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupMagicDFValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupMagicDFValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupRangeValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupRangeValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupRangeValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupMoveValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupMoveValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupMoveValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupRestrainValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupRestrainValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupRestrainValueText = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupWeakValueText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupWeakValueText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupWeakValueText = value;
        }
      }

      public CommonUIStateController m_soldierDetailPanelPorpretyGroupTypeStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupTypeStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupTypeStateCtrl = value;
        }
      }

      public Text m_soldierDetailPanelPorpretyGroupDescText
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelPorpretyGroupDescText;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelPorpretyGroupDescText = value;
        }
      }

      public CommonUIStateController m_soldierDetailPanelHeroListStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelHeroListStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelHeroListStateCtrl = value;
        }
      }

      public GameObject m_soldierDetailPanelHeroListContent
      {
        get
        {
          return this.m_owner.m_soldierDetailPanelHeroListContent;
        }
        set
        {
          this.m_owner.m_soldierDetailPanelHeroListContent = value;
        }
      }

      public Image m_soldierAndSkillInfoSkillIconImage
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoSkillIconImage;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoSkillIconImage = value;
        }
      }

      public Text m_soldierAndSkillInfoSkillNameText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoSkillNameText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoSkillNameText = value;
        }
      }

      public GameObject m_soldierAndSkillInfoSoldierGraphic
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoSoldierGraphic;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoSoldierGraphic = value;
        }
      }

      public Text m_soldierAndSkillInfoSoldierNameText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoSoldierNameText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoSoldierNameText = value;
        }
      }

      public GameObject m_soldierAndSkillInfoSkillUpdateEffectGroup
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoSkillUpdateEffectGroup;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoSkillUpdateEffectGroup = value;
        }
      }

      public CommonUIStateController m_soldierAndSkillInfoPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelStateCtrl = value;
        }
      }

      public Button m_soldierAndSkillInfoPanelWatchDetailButton
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelWatchDetailButton;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelWatchDetailButton = value;
        }
      }

      public CommonUIStateController m_infoPanelDetailStateCtrl
      {
        get
        {
          return this.m_owner.m_infoPanelDetailStateCtrl;
        }
        set
        {
          this.m_owner.m_infoPanelDetailStateCtrl = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelNowLvValueText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelNowLvValueText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelNowLvValueText = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelNowEffectText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelNowEffectText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelNowEffectText = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelIntensifyLvValueText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelIntensifyLvValueText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelIntensifyLvValueText = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelIntensifyNowEffectText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelIntensifyNowEffectText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelIntensifyNowEffectText = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelAfterIntensifyLvValueText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelAfterIntensifyLvValueText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelAfterIntensifyLvValueText = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelAfterIntensifyEffectText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelAfterIntensifyEffectText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelAfterIntensifyEffectText = value;
        }
      }

      public GameObject m_soldierAndSkillInfoPanelItemGroup
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelItemGroup;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelItemGroup = value;
        }
      }

      public Button m_soldierAndSkillInfoPanelPromoteButton
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelPromoteButton;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelPromoteButton = value;
        }
      }

      public CommonUIStateController m_soldierAndSkillInfoPanelPromoteButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelPromoteButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelPromoteButtonStateCtrl = value;
        }
      }

      public Text m_soldierAndSkillInfoPanelGoldenValueText
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelGoldenValueText;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelGoldenValueText = value;
        }
      }

      public CommonUIStateController m_soldierAndSkillInfoPanelGoldenStateCtrl
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelGoldenStateCtrl;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelGoldenStateCtrl = value;
        }
      }

      public GameObject m_soldierAndSkillInfoPanelConditionGroup
      {
        get
        {
          return this.m_owner.m_soldierAndSkillInfoPanelConditionGroup;
        }
        set
        {
          this.m_owner.m_soldierAndSkillInfoPanelConditionGroup = value;
        }
      }

      public CommonUIStateController m_enhanceSuccessEffectPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_enhanceSuccessEffectPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_enhanceSuccessEffectPanelStateCtrl = value;
        }
      }

      public bool m_isFirstIn
      {
        get
        {
          return this.m_owner.m_isFirstIn;
        }
        set
        {
          this.m_owner.m_isFirstIn = value;
        }
      }

      public UISpineGraphic m_soldierInfoGraphic
      {
        get
        {
          return this.m_owner.m_soldierInfoGraphic;
        }
        set
        {
          this.m_owner.m_soldierInfoGraphic = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public TrainingRoom m_curTrainingRoom
      {
        get
        {
          return this.m_owner.m_curTrainingRoom;
        }
        set
        {
          this.m_owner.m_curTrainingRoom = value;
        }
      }

      public TrainingCourse m_curTrainingCourse
      {
        get
        {
          return this.m_owner.m_curTrainingCourse;
        }
        set
        {
          this.m_owner.m_curTrainingCourse = value;
        }
      }

      public TrainingSkillItemUIController m_curTrainingSkillItemCtrl
      {
        get
        {
          return this.m_owner.m_curTrainingSkillItemCtrl;
        }
        set
        {
          this.m_owner.m_curTrainingSkillItemCtrl = value;
        }
      }

      public CourseItemUIController m_lastCourseItemCtrl
      {
        get
        {
          return this.m_owner.m_lastCourseItemCtrl;
        }
        set
        {
          this.m_owner.m_lastCourseItemCtrl = value;
        }
      }

      public List<TrainingSkillItemUIController> m_trainingSkillItemUICtrlList
      {
        get
        {
          return this.m_owner.m_trainingSkillItemUICtrlList;
        }
        set
        {
          this.m_owner.m_trainingSkillItemUICtrlList = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ShowTechClickPanelByTechId(int techId)
      {
        this.m_owner.ShowTechClickPanelByTechId(techId);
      }

      public void SetRoomCoachInfoPanel()
      {
        this.m_owner.SetRoomCoachInfoPanel();
      }

      public void SetCoursesListPanel()
      {
        this.m_owner.SetCoursesListPanel();
      }

      public void OnCourseItemClick(CourseItemUIController ctrl)
      {
        this.m_owner.OnCourseItemClick(ctrl);
      }

      public void SetSkillPanel()
      {
        this.m_owner.SetSkillPanel();
      }

      public void OnTrainingSkillItemClick(TrainingSkillItemUIController ctrl)
      {
        this.m_owner.OnTrainingSkillItemClick(ctrl);
      }

      public void SetSkillItemInfoPanel(TrainingSkillItemUIController ctrl)
      {
        this.m_owner.SetSkillItemInfoPanel(ctrl);
      }

      public void OnEvolutionMaterialClick(GoodsType goodsType, int id, int needCount)
      {
        this.m_owner.OnEvolutionMaterialClick(goodsType, id, needCount);
      }

      public void SetSoldierDetailPanel(ConfigDataSoldierInfo soldierInfo)
      {
        this.m_owner.SetSoldierDetailPanel(soldierInfo);
      }

      public void OnInfoPanelPromoteButtonClick()
      {
        this.m_owner.OnInfoPanelPromoteButtonClick();
      }

      public void OnShowSoldierDetailButtonClick()
      {
        this.m_owner.OnShowSoldierDetailButtonClick();
      }

      public void OnSkillItemInfoPanelBGButtonClick()
      {
        this.m_owner.OnSkillItemInfoPanelBGButtonClick();
      }

      public void OnGoldAddButtonClick()
      {
        this.m_owner.OnGoldAddButtonClick();
      }

      public void OnCrystalAddButtonClick()
      {
        this.m_owner.OnCrystalAddButtonClick();
      }

      public void OnSkillItemInfoPanelFastMaxButtonClick()
      {
        this.m_owner.OnSkillItemInfoPanelFastMaxButtonClick();
      }

      public void OnSkillItemInfoPanelFastLevelButtonClick()
      {
        this.m_owner.OnSkillItemInfoPanelFastLevelButtonClick();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }
    }
  }
}
