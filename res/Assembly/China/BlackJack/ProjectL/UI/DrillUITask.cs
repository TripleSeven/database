﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class DrillUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public static string DrillMode;
    public static string ManualMode;
    public static string TrainingMode;
    public static string TutorMode;
    private static string m_curMode;
    private DrillUIController m_drillUIController;
    private DrillSoldierManualUIController m_drillSoldierManualUIController;
    private DrillTrainingUIController m_drillTrainingUIController;
    private DrillTutorUIController m_drillTutorUIController;
    private static string m_trainingModeFrom;
    private static int m_courseId;
    private int m_curLayerDescIndex;
    private List<ConfigDataSoldierInfo> m_totalSoldierInfoList;
    private List<ConfigDataSoldierInfo> m_curSoldierInfoList;
    private int m_totalSoldierManualPage;
    private int m_curSoldierManualPage;
    private int m_soldierFilterRank;
    private ArmyTag m_armyTag;
    private int m_nowSeconds;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_slot;
    private static int m_getSoldierTechId;
    private bool m_isNeedShowFadeIn;
    [DoNotToLua]
    private DrillUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_SetTotalSoldierInfoListWithFilter_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_IsNeedLoadStaticRes_hotfix;
    private LuaFunction m_CollectAllStaticResDescForLoad_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_PushAndPopLayerByState_hotfix;
    private LuaFunction m_DrillUIController_OnReturn_hotfix;
    private LuaFunction m_DrillUIController_OnManualButtonClick_hotfix;
    private LuaFunction m_DrillUIController_OnTrainingButtonClickInt32_hotfix;
    private LuaFunction m_DrillUIController_OnAssistanceTrainingButtonClickInt32_hotfix;
    private LuaFunction m_DrillUIController_OnAssistanceStopInt32Int32Action_hotfix;
    private LuaFunction m_DrillUIController_OnAssistanceGetRewardInt32Int32_hotfix;
    private LuaFunction m_DrillSoldierManualUIController_OnReturn_hotfix;
    private LuaFunction m_DrillUIController_OnShowHelp_hotfix;
    private LuaFunction m_DrillSoldierManualUIController_OnNeedUpdateViewInt32Int32ArmyTag_hotfix;
    private LuaFunction m_DrillSoldierManualUIController_OnGoToTraingTechInt32_hotfix;
    private LuaFunction m_DrillTrainingUIController_OnReturn_hotfix;
    private LuaFunction m_DrillTrainingUIController_OnAddGold_hotfix;
    private LuaFunction m_DrillTrainingUIController_OnAddCrystal_hotfix;
    private LuaFunction m_DrillTrainingUIController_OnTechLevelupInt32Action_hotfix;
    private LuaFunction m_DrillTrainingUIController_OnEvolutionMaterialClickGoodsTypeInt32Int32Int32_hotfix;
    private LuaFunction m_DrillTrainingUIController_OnGotoGetPathGetPathDataNeedGoods_hotfix;
    private LuaFunction m_DrillTutorUIController_OnReturn_hotfix;
    private LuaFunction m_DrillTutorUIController_OnShowTutorHelp_hotfix;
    private LuaFunction m_DrillTutorUIController_OnConfirmAssistantInt32List`1Int32Int32_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_UpdateTimeText_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(UIIntent uiIntent, int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DrillUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTotalSoldierInfoListWithFilter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushAndPopLayerByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnManualButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnTrainingButtonClick(int courseId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnAssistanceTrainingButtonClick(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnAssistanceStop(int taskId, int slot, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnAssistanceGetReward(int taskId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillSoldierManualUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillSoldierManualUIController_OnNeedUpdateView(
      int curPage,
      int rank,
      ArmyTag armyTag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillSoldierManualUIController_OnGoToTraingTech(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnTechLevelup(int techId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnEvolutionMaterialClick(
      GoodsType goodsType,
      int goodsId,
      int needCount,
      int courseId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTutorUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTutorUIController_OnShowTutorHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTutorUIController_OnConfirmAssistant(
      int taskId,
      List<int> heroId,
      int workSeconds,
      int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTimeText()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public DrillUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return this.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return this.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      base.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static DrillUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private DrillUITask m_owner;

      public LuaExportHelper(DrillUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public static string m_curMode
      {
        get
        {
          return DrillUITask.m_curMode;
        }
        set
        {
          DrillUITask.m_curMode = value;
        }
      }

      public DrillUIController m_drillUIController
      {
        get
        {
          return this.m_owner.m_drillUIController;
        }
        set
        {
          this.m_owner.m_drillUIController = value;
        }
      }

      public DrillSoldierManualUIController m_drillSoldierManualUIController
      {
        get
        {
          return this.m_owner.m_drillSoldierManualUIController;
        }
        set
        {
          this.m_owner.m_drillSoldierManualUIController = value;
        }
      }

      public DrillTrainingUIController m_drillTrainingUIController
      {
        get
        {
          return this.m_owner.m_drillTrainingUIController;
        }
        set
        {
          this.m_owner.m_drillTrainingUIController = value;
        }
      }

      public DrillTutorUIController m_drillTutorUIController
      {
        get
        {
          return this.m_owner.m_drillTutorUIController;
        }
        set
        {
          this.m_owner.m_drillTutorUIController = value;
        }
      }

      public static string m_trainingModeFrom
      {
        get
        {
          return DrillUITask.m_trainingModeFrom;
        }
        set
        {
          DrillUITask.m_trainingModeFrom = value;
        }
      }

      public static int m_courseId
      {
        get
        {
          return DrillUITask.m_courseId;
        }
        set
        {
          DrillUITask.m_courseId = value;
        }
      }

      public int m_curLayerDescIndex
      {
        get
        {
          return this.m_owner.m_curLayerDescIndex;
        }
        set
        {
          this.m_owner.m_curLayerDescIndex = value;
        }
      }

      public List<ConfigDataSoldierInfo> m_totalSoldierInfoList
      {
        get
        {
          return this.m_owner.m_totalSoldierInfoList;
        }
        set
        {
          this.m_owner.m_totalSoldierInfoList = value;
        }
      }

      public List<ConfigDataSoldierInfo> m_curSoldierInfoList
      {
        get
        {
          return this.m_owner.m_curSoldierInfoList;
        }
        set
        {
          this.m_owner.m_curSoldierInfoList = value;
        }
      }

      public int m_totalSoldierManualPage
      {
        get
        {
          return this.m_owner.m_totalSoldierManualPage;
        }
        set
        {
          this.m_owner.m_totalSoldierManualPage = value;
        }
      }

      public int m_curSoldierManualPage
      {
        get
        {
          return this.m_owner.m_curSoldierManualPage;
        }
        set
        {
          this.m_owner.m_curSoldierManualPage = value;
        }
      }

      public int m_soldierFilterRank
      {
        get
        {
          return this.m_owner.m_soldierFilterRank;
        }
        set
        {
          this.m_owner.m_soldierFilterRank = value;
        }
      }

      public ArmyTag m_armyTag
      {
        get
        {
          return this.m_owner.m_armyTag;
        }
        set
        {
          this.m_owner.m_armyTag = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public int m_slot
      {
        get
        {
          return this.m_owner.m_slot;
        }
        set
        {
          this.m_owner.m_slot = value;
        }
      }

      public static int m_getSoldierTechId
      {
        get
        {
          return DrillUITask.m_getSoldierTechId;
        }
        set
        {
          DrillUITask.m_getSoldierTechId = value;
        }
      }

      public bool m_isNeedShowFadeIn
      {
        get
        {
          return this.m_owner.m_isNeedShowFadeIn;
        }
        set
        {
          this.m_owner.m_isNeedShowFadeIn = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void SetTotalSoldierInfoListWithFilter()
      {
        this.m_owner.SetTotalSoldierInfoListWithFilter();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public bool IsNeedLoadStaticRes()
      {
        return this.m_owner.IsNeedLoadStaticRes();
      }

      public List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
      {
        return this.m_owner.CollectAllStaticResDescForLoad();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void PushAndPopLayerByState()
      {
        this.m_owner.PushAndPopLayerByState();
      }

      public void DrillUIController_OnReturn()
      {
        this.m_owner.DrillUIController_OnReturn();
      }

      public void DrillUIController_OnManualButtonClick()
      {
        this.m_owner.DrillUIController_OnManualButtonClick();
      }

      public void DrillUIController_OnTrainingButtonClick(int courseId)
      {
        this.m_owner.DrillUIController_OnTrainingButtonClick(courseId);
      }

      public void DrillUIController_OnAssistanceTrainingButtonClick(int slot)
      {
        this.m_owner.DrillUIController_OnAssistanceTrainingButtonClick(slot);
      }

      public void DrillUIController_OnAssistanceStop(int taskId, int slot, Action OnSucceed)
      {
        this.m_owner.DrillUIController_OnAssistanceStop(taskId, slot, OnSucceed);
      }

      public void DrillUIController_OnAssistanceGetReward(int taskId, int slot)
      {
        this.m_owner.DrillUIController_OnAssistanceGetReward(taskId, slot);
      }

      public void DrillSoldierManualUIController_OnReturn()
      {
        this.m_owner.DrillSoldierManualUIController_OnReturn();
      }

      public void DrillUIController_OnShowHelp()
      {
        this.m_owner.DrillUIController_OnShowHelp();
      }

      public void DrillSoldierManualUIController_OnNeedUpdateView(
        int curPage,
        int rank,
        ArmyTag armyTag)
      {
        this.m_owner.DrillSoldierManualUIController_OnNeedUpdateView(curPage, rank, armyTag);
      }

      public void DrillSoldierManualUIController_OnGoToTraingTech(int techId)
      {
        this.m_owner.DrillSoldierManualUIController_OnGoToTraingTech(techId);
      }

      public void DrillTrainingUIController_OnReturn()
      {
        this.m_owner.DrillTrainingUIController_OnReturn();
      }

      public void DrillTrainingUIController_OnAddGold()
      {
        this.m_owner.DrillTrainingUIController_OnAddGold();
      }

      public void DrillTrainingUIController_OnAddCrystal()
      {
        this.m_owner.DrillTrainingUIController_OnAddCrystal();
      }

      public void DrillTrainingUIController_OnTechLevelup(int techId, Action OnSucceed)
      {
        this.m_owner.DrillTrainingUIController_OnTechLevelup(techId, OnSucceed);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void DrillTrainingUIController_OnEvolutionMaterialClick(
        GoodsType goodsType,
        int goodsId,
        int needCount,
        int courseId)
      {
        // ISSUE: unable to decompile the method.
      }

      public void DrillTrainingUIController_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods)
      {
        this.m_owner.DrillTrainingUIController_OnGotoGetPath(getPath, needGoods);
      }

      public void DrillTutorUIController_OnReturn()
      {
        this.m_owner.DrillTutorUIController_OnReturn();
      }

      public void DrillTutorUIController_OnShowTutorHelp()
      {
        this.m_owner.DrillTutorUIController_OnShowTutorHelp();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void DrillTutorUIController_OnConfirmAssistant(
        int taskId,
        List<int> heroId,
        int workSeconds,
        int slot)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void UpdateTimeText()
      {
        this.m_owner.UpdateTimeText();
      }
    }
  }
}
