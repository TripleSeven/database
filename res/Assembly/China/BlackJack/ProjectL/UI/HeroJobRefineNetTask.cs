﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobRefineNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroJobRefineNetTask : UINetTask
  {
    private int m_heroId;
    private int m_jobConnectionId;
    private int m_slotId;
    private int m_index;
    private int m_stoneId;
    private HeroJobSlotRefineryProperty m_tempProperty;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobRefineNetTask(
      int heroId,
      int jobConnectionId,
      int slotId,
      int index,
      int stoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetResponse(int result, HeroJobSlotRefineryProperty tempProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public HeroJobSlotRefineryProperty TempProperty
    {
      get
      {
        return this.m_tempProperty;
      }
    }
  }
}
