﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentForgeUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class EquipmentForgeUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Hero m_hero;
    private int m_slot;
    private ulong m_instanceId;
    private EquipmentForgeUIController.ForgeState m_forgeState;
    private EquipmentForgeUIController m_equipmentForgeUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentForgeUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void EquipmentForgeUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_EquipmentForge);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEquipmentEnhance(
      ulong instanceId,
      List<ulong> materialIds,
      Action OnSucceed,
      int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEquipmentStarLevelUp(
      ulong instanceId,
      ulong materialId,
      Action OnSucceed,
      int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnGoldAddButtonClick(
      ulong instanceId,
      int slot,
      EquipmentForgeUIController.ForgeState forgeState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEnhanceNeedItemClick(
      GoodsType goodsType,
      int goodsId,
      int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForge_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEnchantButtonClick(
      ulong instanceId,
      ulong stoneInstanceId,
      Action<int, List<CommonBattleProperty>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEnchantSaveButtonClick(Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnLockButtonClick(
      ulong instanceId,
      EquipmentForgeUIController.ForgeState forgeState,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
