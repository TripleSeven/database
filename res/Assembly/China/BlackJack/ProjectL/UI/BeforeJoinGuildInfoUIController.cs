﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BeforeJoinGuildInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BeforeJoinGuildInfoUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelAnimation;
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charLevelText;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_charIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameDummy;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charName;
    [AutoBind("./BGImages/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildNameText;
    [AutoBind("./Declaration/DeclarationText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildDeclarationText;
    [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildDetailButton;
    [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildDetailButtonAnimation;
    [AutoBind("./JoinInButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildJoinButton;
    [AutoBind("./JoinInButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildJoinButtonAnimation;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GuildSearchInfo m_selectGuildInfo;
    private BeforeJoinGuildUIController m_guildUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Init(BeforeJoinGuildUIController guildUIController)
    {
      this.m_guildUIController = guildUIController;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetInfoPanelState(bool hasData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectGuildInfo(GuildSearchInfo guildSearchInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildJoinClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildDetailClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
