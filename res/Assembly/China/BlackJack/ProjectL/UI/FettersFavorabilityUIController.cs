﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersFavorabilityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FettersFavorabilityUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./MaleOrFemale", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sexStateCtrl;
    [AutoBind("./TouchOrNot", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchInOutStateCtrl;
    [AutoBind("./LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_levelUpStateCtrl;
    [AutoBind("./TouchEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchStateCtrl;
    [AutoBind("./InfoPanelEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoStateCtrl;
    [AutoBind("./PresentEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentStateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_resourceContainer;
    [AutoBind("./EditModeUI", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_editModeGo;
    [AutoBind("./EditModeUI/BattleValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleValueText;
    [AutoBind("./EditModeUI/UnlockValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockValueText;
    [AutoBind("./EditModeUI/MaxValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxValueText;
    [AutoBind("./Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButton;
    [AutoBind("./Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionButtonStateCtrl;
    [AutoBind("./Margin/Confession/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionButtonNewImage;
    [AutoBind("./Confession", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionStateCtrl;
    [AutoBind("./ConfessionUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionPanelGo;
    [AutoBind("./ConfessionUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionPanelStateCtrl;
    [AutoBind("./ConfessionUIPrefab/Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionLetterButton;
    [AutoBind("./ConfessionUIPrefab/Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionLetterButtonStateCtrl;
    [AutoBind("./ConfessBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_confessBGImage;
    [AutoBind("./ConfessionUIShowBg", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionUIShowBgButton;
    [AutoBind("./ConfessionUIPrefab/PenButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelPenButton;
    [AutoBind("./ConfessionUIPrefab/UnderGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionPanelUnderTextPanel;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionButtonListPanelStateCtrl;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButtonListPanelButton1;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButtonListPanelButton2;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButtonListPanelButton3;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button1/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionButtonListPanelButton1Text;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button2/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionButtonListPanelButton2Text;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button3/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionButtonListPanelButton3Text;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionLetterPanelStateCtrl;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ConfessionImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionHeroCharGo;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ConfessionImage/Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_confessionHeroCharTransform;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionLetterTitleText;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/MainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionLetterMainText;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/UnderPart/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionLetterLastText;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/UnderPart/PenButton/Effect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionLetterPenButtonGo;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/LetterContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionLetterPenButton;
    [AutoBind("./RightTop", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionPanelRightTopGo;
    [AutoBind("./RightTop/ShareButton/HideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelHideButton;
    [AutoBind("./RightTop/ShareButton/HideButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionPanelHideButtonStateCtrl;
    [AutoBind("./RightTop/ShareButton/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelBackButton;
    [AutoBind("./RightTop/ShareButton/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelShareButton;
    [AutoBind("./ConfessionUIPrefab/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shareButtonDummy;
    [AutoBind("./ConfessionUIPrefab/ShareButtonDummy/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionSharePanelReturnButton;
    [AutoBind("./ConfessionUIPrefab/SharePhotpDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ReturnButton/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/Favorability/ValueTextGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_favorabilityValueTextStateCtrl;
    [AutoBind("./Margin/Favorability/ValueTextGroup/ValueNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityValueText;
    [AutoBind("./Margin/Favorability/UpArrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_favorabilityAddUpArrowGo;
    [AutoBind("./Margin/Favorability/UpArrowText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityAddValueText;
    [AutoBind("./Margin/Favorability/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityHeroNameText;
    [AutoBind("./Margin/Favorability/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_favorabilityHeroNameStateCtrl;
    [AutoBind("./Margin/Favorability/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityLvText;
    [AutoBind("./Margin/Favorability/MaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_favorabilityMaleSlider;
    [AutoBind("./Margin/Favorability/FemaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_favorabilityFemaleSlider;
    [AutoBind("./Margin/Favorability/LevelUpEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpEffectGroupGameObject;
    [AutoBind("./FemaleQinMiDu", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_femaleQinMiDuGameObject;
    [AutoBind("./MaleQinMiDu", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_maleQinMiDuGameObject;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Char/0/Happy_Effects", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charConfessEffectGameObject;
    [AutoBind("./Char/0/Love_Effects", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charConfessSucceedEffectGameObject;
    [AutoBind("./Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordGameObject;
    [AutoBind("./ContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_charContinueButton;
    [AutoBind("./Margin/Touch", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchNumberStateCtrl;
    [AutoBind("./Margin/Touch/CountGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchCountStateCtrl;
    [AutoBind("./Margin/Touch/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_touchTimeText;
    [AutoBind("./ProgressGroup/EffectSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressEffectSlider;
    [AutoBind("./ProgressGroup/MaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressMaleSlider;
    [AutoBind("./ProgressGroup/FrmaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressFemaleSlider;
    [AutoBind("./ProgressGroup/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressEffectGroup;
    [AutoBind("./MaleBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_maleBGImage;
    [AutoBind("./FemaleBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_femaleBGImage;
    [AutoBind("./LevelUpStarEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpStarEffectGroup;
    [AutoBind("./Margin/Information", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_informationButton;
    [AutoBind("./Margin/Information/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_informationButtonNewImage;
    [AutoBind("./Margin/ToggleGroup/Fetters", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fettersButton;
    [AutoBind("./Margin/ToggleGroup/Fetters/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fettersButtonRedMark;
    [AutoBind("./Margin/ToggleGroup/Fetters/PercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_fettersButtonPercentText;
    [AutoBind("./Margin/ToggleGroup/Memory", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryButton;
    [AutoBind("./Margin/ToggleGroup/Memory", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryButtonStateCtrl;
    [AutoBind("./Margin/ToggleGroup/Memory/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryButtonNewImage;
    [AutoBind("./Margin/ToggleGroup/Memory/PercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_memoryButtonPercentText;
    [AutoBind("./Margin/ToggleGroup/Present", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_presentButton;
    [AutoBind("./TouchImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_touchImage;
    [AutoBind("./UnlockEvent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockEventStateCtrl;
    [AutoBind("./UnlockEvent/Detail/Panel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockEventTitleText;
    [AutoBind("./UnlockEvent/Detail/Panel/InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockEventContentText;
    [AutoBind("./UnlockEvent/BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockEventBackButton;
    [AutoBind("./PresentPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentPanel;
    [AutoBind("./PresentPanel/ItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentContentStateCtrl;
    [AutoBind("./PresentPanel/ItemScrollView/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_presentScrollRect;
    [AutoBind("./PresentPanel/ItemScrollView/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentContent;
    [AutoBind("./PresentPanel/ItemCount", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentItemCountShowUIStateCtrl;
    [AutoBind("./PresentPanel/ItemCount/ContinuousTouch", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentContinuousTouchUIStateCtrl;
    [AutoBind("./PresentPanel/ItemCount/Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_presentItemCountText;
    [AutoBind("./PresentPanel/ItemCount/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_presentItemIconImage;
    [AutoBind("./PresentPanel/ItemCount/IconImage/IconImageEffect", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_presentItemIconEffectImage;
    [AutoBind("./PresentExpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentExpEffect;
    [AutoBind("./PresentPanel/ItemScrollView/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_presentSendButton;
    [AutoBind("./PresentPanel/ItemScrollView/Button", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentSendButtonStateCtrl;
    [AutoBind("./PresentPanel/ItemScrollView/ButtonMaskImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentSendButtonMaskImage;
    [AutoBind("./Prefab/ItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentItemPrefab;
    private bool m_isMale;
    private bool m_isInGiftMode;
    private bool m_isFirstInFavorability;
    private Hero m_hero;
    private HeroCharUIController m_heroCharUIController;
    private List<Goods> m_heroFavoribilityLevelUpRewards;
    private int m_heroFavoribilityOldLevel;
    private int m_curUnlockEventIndex;
    private List<string> m_unlockEventTitleList;
    private List<string> m_unlockEventContentList;
    private int m_tempAddExp;
    private int m_tempFavorabilityExp;
    private int m_totalUseExpItemCount;
    private int m_tempFavorabilityLevel;
    private FettersGiftItemUIController m_curGiftBagItemCtrl;
    private FettersGiftItemUIController m_sendButtonDownGiftBagItemCtrl;
    private List<BagItemBase> m_giftBagItemCache;
    private ConfessionDialogBoxUIController m_dialogBoxUIController;
    private ConfigDataConfessionDialogInfo m_curDialogInfo;
    private IAudioPlayback m_currentAudio;
    private FettersFavorabilityUIController.ConfessionProgressState m_confessionProgressState;
    private HeroCharUIController m_confessionHeroCharUIController;
    private ConfessionLetterTextUIController m_confessionLetterTitleCtrl;
    private ConfessionLetterTextUIController m_confessionLetterContentCtrl;
    private ConfessionLetterTextUIController m_confessionLetterSignatureCtrl;
    private Text m_confessionShareNameText;
    private Text m_confessionShareLvText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private bool m_hasConfessed;
    [DoNotToLua]
    private FettersFavorabilityUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetConfessionShareGameObjects_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_UpdateViewInFettersFavorabilityHero_hotfix;
    private LuaFunction m_SetEditModeUIGameObjects_hotfix;
    private LuaFunction m_OpenFettersFavorabilityHero_hotfix;
    private LuaFunction m_SetFavorabilityValue_hotfix;
    private LuaFunction m_SetTouchCountState_hotfix;
    private LuaFunction m_SetTouchRecoverTimeTimeSpan_hotfix;
    private LuaFunction m_DragTrigger_EventOnSliderFull_hotfix;
    private LuaFunction m_DragTrigger_EventOnClick_hotfix;
    private LuaFunction m_DragTrigger_EventOnEndDrag_hotfix;
    private LuaFunction m_DragTrigger_EventOnBeginDrag_hotfix;
    private LuaFunction m_DragTrigger_EventOnSliderEmpty_hotfix;
    private LuaFunction m_SetTouchStateCtrlOutString_hotfix;
    private LuaFunction m_SetCharGoTweenPosBoolean_hotfix;
    private LuaFunction m_OnHeroInteractSucceedList`1Int32HeroInteractionResultTypeInt32_hotfix;
    private LuaFunction m_AfterHeroInteractionResultEffectStringInt32_hotfix;
    private LuaFunction m_PlayFavorbilityLevelUpInt32_hotfix;
    private LuaFunction m_Co_PlayHeroInteractEffectInt32_hotfix;
    private LuaFunction m_OnCharContinueButtonClick_hotfix;
    private LuaFunction m_ShowUnlockEvents_hotfix;
    private LuaFunction m_GetUnlockEventsHero_hotfix;
    private LuaFunction m_OnUnlockEventBackButtonClick_hotfix;
    private LuaFunction m_UpdateViewInFettersGiftHero_hotfix;
    private LuaFunction m_OpenFettersGiftHero_hotfix;
    private LuaFunction m_SetPresentContent_hotfix;
    private LuaFunction m_GiftBagItemComparerBagItemBaseBagItemBase_hotfix;
    private LuaFunction m_OnSendButtonClickDown_hotfix;
    private LuaFunction m_SendUseGiftEvent_hotfix;
    private LuaFunction m_WaitTimeThenDoEventInt32_hotfix;
    private LuaFunction m_OnUseGiftItemSucceedList`1_hotfix;
    private LuaFunction m_OnGiftItemClickFettersGiftItemUIControllerBoolean_hotfix;
    private LuaFunction m_LocalAddExpTick_hotfix;
    private LuaFunction m_Co_SetFavorabilityValueInt32Int32Single_hotfix;
    private LuaFunction m_SetItemCountPanel_hotfix;
    private LuaFunction m_ResetGiftScrollViewPosition_hotfix;
    private LuaFunction m_StartHeroConfessionProgress_hotfix;
    private LuaFunction m_ShowDialogInt32_hotfix;
    private LuaFunction m_ShowNextDialog_hotfix;
    private LuaFunction m_OnConfessionPanelHideButtonClick_hotfix;
    private LuaFunction m_OnConfessionUIShowBgButtonClick_hotfix;
    private LuaFunction m_OnConfessionPanelPenButtonClick_hotfix;
    private LuaFunction m_ShowConfessAnswerButtonList_hotfix;
    private LuaFunction m_ShowDialogAfterAnswerInt32_hotfix;
    private LuaFunction m_OnConfessionLetterButtonClick_hotfix;
    private LuaFunction m_ShowConfessionLetterInt32_hotfix;
    private LuaFunction m_ShowLetterWordsInt32_hotfix;
    private LuaFunction m_OnConfessionLetterPenButtonClick_hotfix;
    private LuaFunction m_OnConfessionPanelBackButtonClick_hotfix;
    private LuaFunction m_CloseConfessionPanel_hotfix;
    private LuaFunction m_ShowConfessSucceedEffect_hotfix;
    private LuaFunction m_C_ShowConfessSicceedEffect_hotfix;
    private LuaFunction m_SendConfesionReq_hotfix;
    private LuaFunction m_OnConfessionPanelShareButtonClick_hotfix;
    private LuaFunction m_OnConfessionSharePanelReturnButtonClick_hotfix;
    private LuaFunction m_OnWeiBoClick_hotfix;
    private LuaFunction m_OnWeChatClick_hotfix;
    private LuaFunction m_ShareInt32_hotfix;
    private LuaFunction m_CaptureFrame_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_SetConfessionButtonStateHero_hotfix;
    private LuaFunction m_Co_OnAnimationFinishedEnableInputSingle_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnInformationButtonClick_hotfix;
    private LuaFunction m_OnFettersButtonClick_hotfix;
    private LuaFunction m_OnMemoryButtonClick_hotfix;
    private LuaFunction m_OnGiftButtonClick_hotfix;
    private LuaFunction m_OnConfessionButtonClick_hotfix;
    private LuaFunction m_InactiveWordGameObject_hotfix;
    private LuaFunction m_PlayHeroPerformanceInt32_hotfix;
    private LuaFunction m_GoToInformationPanelBooleanHero_hotfix;
    private LuaFunction m_SetIsFirstInFavorabilityTagBoolean_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnSliderFullAction_hotfix;
    private LuaFunction m_remove_EventOnSliderFullAction_hotfix;
    private LuaFunction m_add_EventOnGiftButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGiftButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnMemoryButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnMemoryButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnFettersButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnFettersButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnInformationButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnInformationButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnConfessionButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnConfessionButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnUseGiftAction`4_hotfix;
    private LuaFunction m_remove_EventOnUseGiftAction`4_hotfix;
    private LuaFunction m_add_EventOnGotoBagFullPanelAction_hotfix;
    private LuaFunction m_remove_EventOnGotoBagFullPanelAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersFavorabilityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConfessionShareGameObjects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersFavorability(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEditModeUIGameObjects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenFettersFavorability(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFavorabilityValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTouchCountState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchRecoverTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnSliderFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnBeginDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnSliderEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTouchStateCtrlOut(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCharGoTweenPos(bool isCharAtInState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHeroInteractSucceed(
      List<Goods> rewards,
      int heroPerformanceId,
      HeroInteractionResultType heroInteractionResultType,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AfterHeroInteractionResultEffect(string stateName, int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFavorbilityLevelUp(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayHeroInteractEffect(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCharContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowUnlockEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUnlockEvents(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockEventBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersGift(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenFettersGift(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPresentContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GiftBagItemComparer(BagItemBase a, BagItemBase b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClickDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendUseGiftEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitTimeThenDoEvent(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseGiftItemSucceed(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftItemClick(FettersGiftItemUIController ctrl, bool isNeedShowDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocalAddExpTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetFavorabilityValue(
      int curFavoribilityLv,
      int addLv,
      float finalSliderValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemCountPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetGiftScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartHeroConfessionProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialog(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelHideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionUIShowBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelPenButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowConfessAnswerButtonList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialogAfterAnswer(int dialoId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionLetterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowConfessionLetter(int letterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowLetterWords(int letterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionLetterPenButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseConfessionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowConfessSucceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator C_ShowConfessSicceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendConfesionReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelShareButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionSharePanelReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Share(int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConfessionButtonState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_OnAnimationFinishedEnableInput(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInformationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFettersButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemoryButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InactiveWordGameObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GoToInformationPanel(bool isGotoOrReturn, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsFirstInFavorabilityTag(bool v)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSliderFull
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnGiftButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnMemoryButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnFettersButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnInformationButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnConfessionButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int, Action<List<Goods>>> EventOnUseGift
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGotoBagFullPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FettersFavorabilityUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSliderFull()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSliderFull()
    {
      this.EventOnSliderFull = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGiftButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGiftButtonClick(Hero obj)
    {
      this.EventOnGiftButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMemoryButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMemoryButtonClick(Hero obj)
    {
      this.EventOnMemoryButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFettersButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFettersButtonClick(Hero obj)
    {
      this.EventOnFettersButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnInformationButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnInformationButtonClick(Hero obj)
    {
      this.EventOnInformationButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnConfessionButtonClick(Hero obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnConfessionButtonClick(Hero obj)
    {
      this.EventOnConfessionButtonClick = (Action<Hero>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUseGift(
      GoodsType arg1,
      int arg2,
      int arg3,
      Action<List<Goods>> arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUseGift(
      GoodsType arg1,
      int arg2,
      int arg3,
      Action<List<Goods>> arg4)
    {
      this.EventOnUseGift = (Action<GoodsType, int, int, Action<List<Goods>>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGotoBagFullPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGotoBagFullPanel()
    {
      this.EventOnGotoBagFullPanel = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum ConfessionProgressState
    {
      Begin,
      Question,
      Letter,
      End,
    }

    public class LuaExportHelper
    {
      private FettersFavorabilityUIController m_owner;

      public LuaExportHelper(FettersFavorabilityUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnSliderFull()
      {
        this.m_owner.__callDele_EventOnSliderFull();
      }

      public void __clearDele_EventOnSliderFull()
      {
        this.m_owner.__clearDele_EventOnSliderFull();
      }

      public void __callDele_EventOnGiftButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnGiftButtonClick(obj);
      }

      public void __clearDele_EventOnGiftButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnGiftButtonClick(obj);
      }

      public void __callDele_EventOnMemoryButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnMemoryButtonClick(obj);
      }

      public void __clearDele_EventOnMemoryButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnMemoryButtonClick(obj);
      }

      public void __callDele_EventOnFettersButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnFettersButtonClick(obj);
      }

      public void __clearDele_EventOnFettersButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnFettersButtonClick(obj);
      }

      public void __callDele_EventOnInformationButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnInformationButtonClick(obj);
      }

      public void __clearDele_EventOnInformationButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnInformationButtonClick(obj);
      }

      public void __callDele_EventOnConfessionButtonClick(Hero obj)
      {
        this.m_owner.__callDele_EventOnConfessionButtonClick(obj);
      }

      public void __clearDele_EventOnConfessionButtonClick(Hero obj)
      {
        this.m_owner.__clearDele_EventOnConfessionButtonClick(obj);
      }

      public void __callDele_EventOnUseGift(
        GoodsType arg1,
        int arg2,
        int arg3,
        Action<List<Goods>> arg4)
      {
        this.m_owner.__callDele_EventOnUseGift(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnUseGift(
        GoodsType arg1,
        int arg2,
        int arg3,
        Action<List<Goods>> arg4)
      {
        this.m_owner.__clearDele_EventOnUseGift(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnGotoBagFullPanel()
      {
        this.m_owner.__callDele_EventOnGotoBagFullPanel();
      }

      public void __clearDele_EventOnGotoBagFullPanel()
      {
        this.m_owner.__clearDele_EventOnGotoBagFullPanel();
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public CommonUIStateController m_commonUIStateCtrl
      {
        get
        {
          return this.m_owner.m_commonUIStateCtrl;
        }
        set
        {
          this.m_owner.m_commonUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_sexStateCtrl
      {
        get
        {
          return this.m_owner.m_sexStateCtrl;
        }
        set
        {
          this.m_owner.m_sexStateCtrl = value;
        }
      }

      public CommonUIStateController m_touchInOutStateCtrl
      {
        get
        {
          return this.m_owner.m_touchInOutStateCtrl;
        }
        set
        {
          this.m_owner.m_touchInOutStateCtrl = value;
        }
      }

      public CommonUIStateController m_levelUpStateCtrl
      {
        get
        {
          return this.m_owner.m_levelUpStateCtrl;
        }
        set
        {
          this.m_owner.m_levelUpStateCtrl = value;
        }
      }

      public CommonUIStateController m_touchStateCtrl
      {
        get
        {
          return this.m_owner.m_touchStateCtrl;
        }
        set
        {
          this.m_owner.m_touchStateCtrl = value;
        }
      }

      public CommonUIStateController m_infoStateCtrl
      {
        get
        {
          return this.m_owner.m_infoStateCtrl;
        }
        set
        {
          this.m_owner.m_infoStateCtrl = value;
        }
      }

      public CommonUIStateController m_presentStateCtrl
      {
        get
        {
          return this.m_owner.m_presentStateCtrl;
        }
        set
        {
          this.m_owner.m_presentStateCtrl = value;
        }
      }

      public PrefabResourceContainer m_resourceContainer
      {
        get
        {
          return this.m_owner.m_resourceContainer;
        }
        set
        {
          this.m_owner.m_resourceContainer = value;
        }
      }

      public GameObject m_editModeGo
      {
        get
        {
          return this.m_owner.m_editModeGo;
        }
        set
        {
          this.m_owner.m_editModeGo = value;
        }
      }

      public Text m_battleValueText
      {
        get
        {
          return this.m_owner.m_battleValueText;
        }
        set
        {
          this.m_owner.m_battleValueText = value;
        }
      }

      public Text m_unlockValueText
      {
        get
        {
          return this.m_owner.m_unlockValueText;
        }
        set
        {
          this.m_owner.m_unlockValueText = value;
        }
      }

      public Text m_maxValueText
      {
        get
        {
          return this.m_owner.m_maxValueText;
        }
        set
        {
          this.m_owner.m_maxValueText = value;
        }
      }

      public Button m_confessionButton
      {
        get
        {
          return this.m_owner.m_confessionButton;
        }
        set
        {
          this.m_owner.m_confessionButton = value;
        }
      }

      public CommonUIStateController m_confessionButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionButtonStateCtrl = value;
        }
      }

      public GameObject m_confessionButtonNewImage
      {
        get
        {
          return this.m_owner.m_confessionButtonNewImage;
        }
        set
        {
          this.m_owner.m_confessionButtonNewImage = value;
        }
      }

      public CommonUIStateController m_confessionStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionStateCtrl = value;
        }
      }

      public GameObject m_confessionPanelGo
      {
        get
        {
          return this.m_owner.m_confessionPanelGo;
        }
        set
        {
          this.m_owner.m_confessionPanelGo = value;
        }
      }

      public CommonUIStateController m_confessionPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionPanelStateCtrl = value;
        }
      }

      public Button m_confessionLetterButton
      {
        get
        {
          return this.m_owner.m_confessionLetterButton;
        }
        set
        {
          this.m_owner.m_confessionLetterButton = value;
        }
      }

      public CommonUIStateController m_confessionLetterButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionLetterButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionLetterButtonStateCtrl = value;
        }
      }

      public Image m_confessBGImage
      {
        get
        {
          return this.m_owner.m_confessBGImage;
        }
        set
        {
          this.m_owner.m_confessBGImage = value;
        }
      }

      public Button m_confessionUIShowBgButton
      {
        get
        {
          return this.m_owner.m_confessionUIShowBgButton;
        }
        set
        {
          this.m_owner.m_confessionUIShowBgButton = value;
        }
      }

      public Button m_confessionPanelPenButton
      {
        get
        {
          return this.m_owner.m_confessionPanelPenButton;
        }
        set
        {
          this.m_owner.m_confessionPanelPenButton = value;
        }
      }

      public GameObject m_confessionPanelUnderTextPanel
      {
        get
        {
          return this.m_owner.m_confessionPanelUnderTextPanel;
        }
        set
        {
          this.m_owner.m_confessionPanelUnderTextPanel = value;
        }
      }

      public CommonUIStateController m_confessionButtonListPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelStateCtrl = value;
        }
      }

      public Button m_confessionButtonListPanelButton1
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelButton1;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelButton1 = value;
        }
      }

      public Button m_confessionButtonListPanelButton2
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelButton2;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelButton2 = value;
        }
      }

      public Button m_confessionButtonListPanelButton3
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelButton3;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelButton3 = value;
        }
      }

      public Text m_confessionButtonListPanelButton1Text
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelButton1Text;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelButton1Text = value;
        }
      }

      public Text m_confessionButtonListPanelButton2Text
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelButton2Text;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelButton2Text = value;
        }
      }

      public Text m_confessionButtonListPanelButton3Text
      {
        get
        {
          return this.m_owner.m_confessionButtonListPanelButton3Text;
        }
        set
        {
          this.m_owner.m_confessionButtonListPanelButton3Text = value;
        }
      }

      public CommonUIStateController m_confessionLetterPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionLetterPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionLetterPanelStateCtrl = value;
        }
      }

      public GameObject m_confessionHeroCharGo
      {
        get
        {
          return this.m_owner.m_confessionHeroCharGo;
        }
        set
        {
          this.m_owner.m_confessionHeroCharGo = value;
        }
      }

      public RectTransform m_confessionHeroCharTransform
      {
        get
        {
          return this.m_owner.m_confessionHeroCharTransform;
        }
        set
        {
          this.m_owner.m_confessionHeroCharTransform = value;
        }
      }

      public Text m_confessionLetterTitleText
      {
        get
        {
          return this.m_owner.m_confessionLetterTitleText;
        }
        set
        {
          this.m_owner.m_confessionLetterTitleText = value;
        }
      }

      public Text m_confessionLetterMainText
      {
        get
        {
          return this.m_owner.m_confessionLetterMainText;
        }
        set
        {
          this.m_owner.m_confessionLetterMainText = value;
        }
      }

      public Text m_confessionLetterLastText
      {
        get
        {
          return this.m_owner.m_confessionLetterLastText;
        }
        set
        {
          this.m_owner.m_confessionLetterLastText = value;
        }
      }

      public GameObject m_confessionLetterPenButtonGo
      {
        get
        {
          return this.m_owner.m_confessionLetterPenButtonGo;
        }
        set
        {
          this.m_owner.m_confessionLetterPenButtonGo = value;
        }
      }

      public Button m_confessionLetterPenButton
      {
        get
        {
          return this.m_owner.m_confessionLetterPenButton;
        }
        set
        {
          this.m_owner.m_confessionLetterPenButton = value;
        }
      }

      public GameObject m_confessionPanelRightTopGo
      {
        get
        {
          return this.m_owner.m_confessionPanelRightTopGo;
        }
        set
        {
          this.m_owner.m_confessionPanelRightTopGo = value;
        }
      }

      public Button m_confessionPanelHideButton
      {
        get
        {
          return this.m_owner.m_confessionPanelHideButton;
        }
        set
        {
          this.m_owner.m_confessionPanelHideButton = value;
        }
      }

      public CommonUIStateController m_confessionPanelHideButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_confessionPanelHideButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_confessionPanelHideButtonStateCtrl = value;
        }
      }

      public Button m_confessionPanelBackButton
      {
        get
        {
          return this.m_owner.m_confessionPanelBackButton;
        }
        set
        {
          this.m_owner.m_confessionPanelBackButton = value;
        }
      }

      public Button m_confessionPanelShareButton
      {
        get
        {
          return this.m_owner.m_confessionPanelShareButton;
        }
        set
        {
          this.m_owner.m_confessionPanelShareButton = value;
        }
      }

      public GameObject m_shareButtonDummy
      {
        get
        {
          return this.m_owner.m_shareButtonDummy;
        }
        set
        {
          this.m_owner.m_shareButtonDummy = value;
        }
      }

      public Button m_confessionSharePanelReturnButton
      {
        get
        {
          return this.m_owner.m_confessionSharePanelReturnButton;
        }
        set
        {
          this.m_owner.m_confessionSharePanelReturnButton = value;
        }
      }

      public GameObject m_sharePhotoDummy
      {
        get
        {
          return this.m_owner.m_sharePhotoDummy;
        }
        set
        {
          this.m_owner.m_sharePhotoDummy = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public CommonUIStateController m_favorabilityValueTextStateCtrl
      {
        get
        {
          return this.m_owner.m_favorabilityValueTextStateCtrl;
        }
        set
        {
          this.m_owner.m_favorabilityValueTextStateCtrl = value;
        }
      }

      public Text m_favorabilityValueText
      {
        get
        {
          return this.m_owner.m_favorabilityValueText;
        }
        set
        {
          this.m_owner.m_favorabilityValueText = value;
        }
      }

      public GameObject m_favorabilityAddUpArrowGo
      {
        get
        {
          return this.m_owner.m_favorabilityAddUpArrowGo;
        }
        set
        {
          this.m_owner.m_favorabilityAddUpArrowGo = value;
        }
      }

      public Text m_favorabilityAddValueText
      {
        get
        {
          return this.m_owner.m_favorabilityAddValueText;
        }
        set
        {
          this.m_owner.m_favorabilityAddValueText = value;
        }
      }

      public Text m_favorabilityHeroNameText
      {
        get
        {
          return this.m_owner.m_favorabilityHeroNameText;
        }
        set
        {
          this.m_owner.m_favorabilityHeroNameText = value;
        }
      }

      public CommonUIStateController m_favorabilityHeroNameStateCtrl
      {
        get
        {
          return this.m_owner.m_favorabilityHeroNameStateCtrl;
        }
        set
        {
          this.m_owner.m_favorabilityHeroNameStateCtrl = value;
        }
      }

      public Text m_favorabilityLvText
      {
        get
        {
          return this.m_owner.m_favorabilityLvText;
        }
        set
        {
          this.m_owner.m_favorabilityLvText = value;
        }
      }

      public Slider m_favorabilityMaleSlider
      {
        get
        {
          return this.m_owner.m_favorabilityMaleSlider;
        }
        set
        {
          this.m_owner.m_favorabilityMaleSlider = value;
        }
      }

      public Slider m_favorabilityFemaleSlider
      {
        get
        {
          return this.m_owner.m_favorabilityFemaleSlider;
        }
        set
        {
          this.m_owner.m_favorabilityFemaleSlider = value;
        }
      }

      public GameObject m_levelUpEffectGroupGameObject
      {
        get
        {
          return this.m_owner.m_levelUpEffectGroupGameObject;
        }
        set
        {
          this.m_owner.m_levelUpEffectGroupGameObject = value;
        }
      }

      public GameObject m_femaleQinMiDuGameObject
      {
        get
        {
          return this.m_owner.m_femaleQinMiDuGameObject;
        }
        set
        {
          this.m_owner.m_femaleQinMiDuGameObject = value;
        }
      }

      public GameObject m_maleQinMiDuGameObject
      {
        get
        {
          return this.m_owner.m_maleQinMiDuGameObject;
        }
        set
        {
          this.m_owner.m_maleQinMiDuGameObject = value;
        }
      }

      public GameObject m_charGameObjectRoot
      {
        get
        {
          return this.m_owner.m_charGameObjectRoot;
        }
        set
        {
          this.m_owner.m_charGameObjectRoot = value;
        }
      }

      public GameObject m_charGameObject
      {
        get
        {
          return this.m_owner.m_charGameObject;
        }
        set
        {
          this.m_owner.m_charGameObject = value;
        }
      }

      public GameObject m_charConfessEffectGameObject
      {
        get
        {
          return this.m_owner.m_charConfessEffectGameObject;
        }
        set
        {
          this.m_owner.m_charConfessEffectGameObject = value;
        }
      }

      public GameObject m_charConfessSucceedEffectGameObject
      {
        get
        {
          return this.m_owner.m_charConfessSucceedEffectGameObject;
        }
        set
        {
          this.m_owner.m_charConfessSucceedEffectGameObject = value;
        }
      }

      public GameObject m_wordGameObject
      {
        get
        {
          return this.m_owner.m_wordGameObject;
        }
        set
        {
          this.m_owner.m_wordGameObject = value;
        }
      }

      public Button m_charContinueButton
      {
        get
        {
          return this.m_owner.m_charContinueButton;
        }
        set
        {
          this.m_owner.m_charContinueButton = value;
        }
      }

      public CommonUIStateController m_touchNumberStateCtrl
      {
        get
        {
          return this.m_owner.m_touchNumberStateCtrl;
        }
        set
        {
          this.m_owner.m_touchNumberStateCtrl = value;
        }
      }

      public CommonUIStateController m_touchCountStateCtrl
      {
        get
        {
          return this.m_owner.m_touchCountStateCtrl;
        }
        set
        {
          this.m_owner.m_touchCountStateCtrl = value;
        }
      }

      public Text m_touchTimeText
      {
        get
        {
          return this.m_owner.m_touchTimeText;
        }
        set
        {
          this.m_owner.m_touchTimeText = value;
        }
      }

      public Slider m_progressEffectSlider
      {
        get
        {
          return this.m_owner.m_progressEffectSlider;
        }
        set
        {
          this.m_owner.m_progressEffectSlider = value;
        }
      }

      public Slider m_progressMaleSlider
      {
        get
        {
          return this.m_owner.m_progressMaleSlider;
        }
        set
        {
          this.m_owner.m_progressMaleSlider = value;
        }
      }

      public Slider m_progressFemaleSlider
      {
        get
        {
          return this.m_owner.m_progressFemaleSlider;
        }
        set
        {
          this.m_owner.m_progressFemaleSlider = value;
        }
      }

      public GameObject m_progressEffectGroup
      {
        get
        {
          return this.m_owner.m_progressEffectGroup;
        }
        set
        {
          this.m_owner.m_progressEffectGroup = value;
        }
      }

      public GameObject m_maleBGImage
      {
        get
        {
          return this.m_owner.m_maleBGImage;
        }
        set
        {
          this.m_owner.m_maleBGImage = value;
        }
      }

      public GameObject m_femaleBGImage
      {
        get
        {
          return this.m_owner.m_femaleBGImage;
        }
        set
        {
          this.m_owner.m_femaleBGImage = value;
        }
      }

      public GameObject m_levelUpStarEffectGroup
      {
        get
        {
          return this.m_owner.m_levelUpStarEffectGroup;
        }
        set
        {
          this.m_owner.m_levelUpStarEffectGroup = value;
        }
      }

      public Button m_informationButton
      {
        get
        {
          return this.m_owner.m_informationButton;
        }
        set
        {
          this.m_owner.m_informationButton = value;
        }
      }

      public GameObject m_informationButtonNewImage
      {
        get
        {
          return this.m_owner.m_informationButtonNewImage;
        }
        set
        {
          this.m_owner.m_informationButtonNewImage = value;
        }
      }

      public Button m_fettersButton
      {
        get
        {
          return this.m_owner.m_fettersButton;
        }
        set
        {
          this.m_owner.m_fettersButton = value;
        }
      }

      public GameObject m_fettersButtonRedMark
      {
        get
        {
          return this.m_owner.m_fettersButtonRedMark;
        }
        set
        {
          this.m_owner.m_fettersButtonRedMark = value;
        }
      }

      public Text m_fettersButtonPercentText
      {
        get
        {
          return this.m_owner.m_fettersButtonPercentText;
        }
        set
        {
          this.m_owner.m_fettersButtonPercentText = value;
        }
      }

      public Button m_memoryButton
      {
        get
        {
          return this.m_owner.m_memoryButton;
        }
        set
        {
          this.m_owner.m_memoryButton = value;
        }
      }

      public CommonUIStateController m_memoryButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_memoryButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_memoryButtonStateCtrl = value;
        }
      }

      public GameObject m_memoryButtonNewImage
      {
        get
        {
          return this.m_owner.m_memoryButtonNewImage;
        }
        set
        {
          this.m_owner.m_memoryButtonNewImage = value;
        }
      }

      public Text m_memoryButtonPercentText
      {
        get
        {
          return this.m_owner.m_memoryButtonPercentText;
        }
        set
        {
          this.m_owner.m_memoryButtonPercentText = value;
        }
      }

      public Button m_presentButton
      {
        get
        {
          return this.m_owner.m_presentButton;
        }
        set
        {
          this.m_owner.m_presentButton = value;
        }
      }

      public GameObject m_touchImage
      {
        get
        {
          return this.m_owner.m_touchImage;
        }
        set
        {
          this.m_owner.m_touchImage = value;
        }
      }

      public CommonUIStateController m_unlockEventStateCtrl
      {
        get
        {
          return this.m_owner.m_unlockEventStateCtrl;
        }
        set
        {
          this.m_owner.m_unlockEventStateCtrl = value;
        }
      }

      public Text m_unlockEventTitleText
      {
        get
        {
          return this.m_owner.m_unlockEventTitleText;
        }
        set
        {
          this.m_owner.m_unlockEventTitleText = value;
        }
      }

      public Text m_unlockEventContentText
      {
        get
        {
          return this.m_owner.m_unlockEventContentText;
        }
        set
        {
          this.m_owner.m_unlockEventContentText = value;
        }
      }

      public Button m_unlockEventBackButton
      {
        get
        {
          return this.m_owner.m_unlockEventBackButton;
        }
        set
        {
          this.m_owner.m_unlockEventBackButton = value;
        }
      }

      public GameObject m_presentPanel
      {
        get
        {
          return this.m_owner.m_presentPanel;
        }
        set
        {
          this.m_owner.m_presentPanel = value;
        }
      }

      public CommonUIStateController m_presentContentStateCtrl
      {
        get
        {
          return this.m_owner.m_presentContentStateCtrl;
        }
        set
        {
          this.m_owner.m_presentContentStateCtrl = value;
        }
      }

      public ScrollRect m_presentScrollRect
      {
        get
        {
          return this.m_owner.m_presentScrollRect;
        }
        set
        {
          this.m_owner.m_presentScrollRect = value;
        }
      }

      public GameObject m_presentContent
      {
        get
        {
          return this.m_owner.m_presentContent;
        }
        set
        {
          this.m_owner.m_presentContent = value;
        }
      }

      public CommonUIStateController m_presentItemCountShowUIStateCtrl
      {
        get
        {
          return this.m_owner.m_presentItemCountShowUIStateCtrl;
        }
        set
        {
          this.m_owner.m_presentItemCountShowUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_presentContinuousTouchUIStateCtrl
      {
        get
        {
          return this.m_owner.m_presentContinuousTouchUIStateCtrl;
        }
        set
        {
          this.m_owner.m_presentContinuousTouchUIStateCtrl = value;
        }
      }

      public Text m_presentItemCountText
      {
        get
        {
          return this.m_owner.m_presentItemCountText;
        }
        set
        {
          this.m_owner.m_presentItemCountText = value;
        }
      }

      public Image m_presentItemIconImage
      {
        get
        {
          return this.m_owner.m_presentItemIconImage;
        }
        set
        {
          this.m_owner.m_presentItemIconImage = value;
        }
      }

      public Image m_presentItemIconEffectImage
      {
        get
        {
          return this.m_owner.m_presentItemIconEffectImage;
        }
        set
        {
          this.m_owner.m_presentItemIconEffectImage = value;
        }
      }

      public CommonUIStateController m_presentExpEffect
      {
        get
        {
          return this.m_owner.m_presentExpEffect;
        }
        set
        {
          this.m_owner.m_presentExpEffect = value;
        }
      }

      public Button m_presentSendButton
      {
        get
        {
          return this.m_owner.m_presentSendButton;
        }
        set
        {
          this.m_owner.m_presentSendButton = value;
        }
      }

      public CommonUIStateController m_presentSendButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_presentSendButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_presentSendButtonStateCtrl = value;
        }
      }

      public GameObject m_presentSendButtonMaskImage
      {
        get
        {
          return this.m_owner.m_presentSendButtonMaskImage;
        }
        set
        {
          this.m_owner.m_presentSendButtonMaskImage = value;
        }
      }

      public GameObject m_presentItemPrefab
      {
        get
        {
          return this.m_owner.m_presentItemPrefab;
        }
        set
        {
          this.m_owner.m_presentItemPrefab = value;
        }
      }

      public bool m_isMale
      {
        get
        {
          return this.m_owner.m_isMale;
        }
        set
        {
          this.m_owner.m_isMale = value;
        }
      }

      public bool m_isInGiftMode
      {
        get
        {
          return this.m_owner.m_isInGiftMode;
        }
        set
        {
          this.m_owner.m_isInGiftMode = value;
        }
      }

      public bool m_isFirstInFavorability
      {
        get
        {
          return this.m_owner.m_isFirstInFavorability;
        }
        set
        {
          this.m_owner.m_isFirstInFavorability = value;
        }
      }

      public Hero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public List<Goods> m_heroFavoribilityLevelUpRewards
      {
        get
        {
          return this.m_owner.m_heroFavoribilityLevelUpRewards;
        }
        set
        {
          this.m_owner.m_heroFavoribilityLevelUpRewards = value;
        }
      }

      public int m_heroFavoribilityOldLevel
      {
        get
        {
          return this.m_owner.m_heroFavoribilityOldLevel;
        }
        set
        {
          this.m_owner.m_heroFavoribilityOldLevel = value;
        }
      }

      public int m_curUnlockEventIndex
      {
        get
        {
          return this.m_owner.m_curUnlockEventIndex;
        }
        set
        {
          this.m_owner.m_curUnlockEventIndex = value;
        }
      }

      public List<string> m_unlockEventTitleList
      {
        get
        {
          return this.m_owner.m_unlockEventTitleList;
        }
        set
        {
          this.m_owner.m_unlockEventTitleList = value;
        }
      }

      public List<string> m_unlockEventContentList
      {
        get
        {
          return this.m_owner.m_unlockEventContentList;
        }
        set
        {
          this.m_owner.m_unlockEventContentList = value;
        }
      }

      public int m_tempAddExp
      {
        get
        {
          return this.m_owner.m_tempAddExp;
        }
        set
        {
          this.m_owner.m_tempAddExp = value;
        }
      }

      public int m_tempFavorabilityExp
      {
        get
        {
          return this.m_owner.m_tempFavorabilityExp;
        }
        set
        {
          this.m_owner.m_tempFavorabilityExp = value;
        }
      }

      public int m_totalUseExpItemCount
      {
        get
        {
          return this.m_owner.m_totalUseExpItemCount;
        }
        set
        {
          this.m_owner.m_totalUseExpItemCount = value;
        }
      }

      public int m_tempFavorabilityLevel
      {
        get
        {
          return this.m_owner.m_tempFavorabilityLevel;
        }
        set
        {
          this.m_owner.m_tempFavorabilityLevel = value;
        }
      }

      public FettersGiftItemUIController m_curGiftBagItemCtrl
      {
        get
        {
          return this.m_owner.m_curGiftBagItemCtrl;
        }
        set
        {
          this.m_owner.m_curGiftBagItemCtrl = value;
        }
      }

      public FettersGiftItemUIController m_sendButtonDownGiftBagItemCtrl
      {
        get
        {
          return this.m_owner.m_sendButtonDownGiftBagItemCtrl;
        }
        set
        {
          this.m_owner.m_sendButtonDownGiftBagItemCtrl = value;
        }
      }

      public List<BagItemBase> m_giftBagItemCache
      {
        get
        {
          return this.m_owner.m_giftBagItemCache;
        }
        set
        {
          this.m_owner.m_giftBagItemCache = value;
        }
      }

      public ConfessionDialogBoxUIController m_dialogBoxUIController
      {
        get
        {
          return this.m_owner.m_dialogBoxUIController;
        }
        set
        {
          this.m_owner.m_dialogBoxUIController = value;
        }
      }

      public ConfigDataConfessionDialogInfo m_curDialogInfo
      {
        get
        {
          return this.m_owner.m_curDialogInfo;
        }
        set
        {
          this.m_owner.m_curDialogInfo = value;
        }
      }

      public IAudioPlayback m_currentAudio
      {
        get
        {
          return this.m_owner.m_currentAudio;
        }
        set
        {
          this.m_owner.m_currentAudio = value;
        }
      }

      public FettersFavorabilityUIController.ConfessionProgressState m_confessionProgressState
      {
        get
        {
          return this.m_owner.m_confessionProgressState;
        }
        set
        {
          this.m_owner.m_confessionProgressState = value;
        }
      }

      public HeroCharUIController m_confessionHeroCharUIController
      {
        get
        {
          return this.m_owner.m_confessionHeroCharUIController;
        }
        set
        {
          this.m_owner.m_confessionHeroCharUIController = value;
        }
      }

      public ConfessionLetterTextUIController m_confessionLetterTitleCtrl
      {
        get
        {
          return this.m_owner.m_confessionLetterTitleCtrl;
        }
        set
        {
          this.m_owner.m_confessionLetterTitleCtrl = value;
        }
      }

      public ConfessionLetterTextUIController m_confessionLetterContentCtrl
      {
        get
        {
          return this.m_owner.m_confessionLetterContentCtrl;
        }
        set
        {
          this.m_owner.m_confessionLetterContentCtrl = value;
        }
      }

      public ConfessionLetterTextUIController m_confessionLetterSignatureCtrl
      {
        get
        {
          return this.m_owner.m_confessionLetterSignatureCtrl;
        }
        set
        {
          this.m_owner.m_confessionLetterSignatureCtrl = value;
        }
      }

      public Text m_confessionShareNameText
      {
        get
        {
          return this.m_owner.m_confessionShareNameText;
        }
        set
        {
          this.m_owner.m_confessionShareNameText = value;
        }
      }

      public Text m_confessionShareLvText
      {
        get
        {
          return this.m_owner.m_confessionShareLvText;
        }
        set
        {
          this.m_owner.m_confessionShareLvText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public bool m_hasConfessed
      {
        get
        {
          return this.m_owner.m_hasConfessed;
        }
        set
        {
          this.m_owner.m_hasConfessed = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetConfessionShareGameObjects()
      {
        this.m_owner.SetConfessionShareGameObjects();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void SetEditModeUIGameObjects()
      {
        this.m_owner.SetEditModeUIGameObjects();
      }

      public void SetFavorabilityValue()
      {
        this.m_owner.SetFavorabilityValue();
      }

      public void SetTouchCountState()
      {
        this.m_owner.SetTouchCountState();
      }

      public void DragTrigger_EventOnSliderFull()
      {
        this.m_owner.DragTrigger_EventOnSliderFull();
      }

      public void DragTrigger_EventOnClick()
      {
        this.m_owner.DragTrigger_EventOnClick();
      }

      public void DragTrigger_EventOnEndDrag()
      {
        this.m_owner.DragTrigger_EventOnEndDrag();
      }

      public void DragTrigger_EventOnBeginDrag()
      {
        this.m_owner.DragTrigger_EventOnBeginDrag();
      }

      public void DragTrigger_EventOnSliderEmpty()
      {
        this.m_owner.DragTrigger_EventOnSliderEmpty();
      }

      public void SetTouchStateCtrlOut(string stateName)
      {
        this.m_owner.SetTouchStateCtrlOut(stateName);
      }

      public void SetCharGoTweenPos(bool isCharAtInState)
      {
        this.m_owner.SetCharGoTweenPos(isCharAtInState);
      }

      public void AfterHeroInteractionResultEffect(string stateName, int heroPerformanceId)
      {
        this.m_owner.AfterHeroInteractionResultEffect(stateName, heroPerformanceId);
      }

      public void PlayFavorbilityLevelUp(int heroPerformanceId)
      {
        this.m_owner.PlayFavorbilityLevelUp(heroPerformanceId);
      }

      public IEnumerator Co_PlayHeroInteractEffect(int heroPerformanceId)
      {
        return this.m_owner.Co_PlayHeroInteractEffect(heroPerformanceId);
      }

      public void OnCharContinueButtonClick()
      {
        this.m_owner.OnCharContinueButtonClick();
      }

      public void ShowUnlockEvents()
      {
        this.m_owner.ShowUnlockEvents();
      }

      public void GetUnlockEvents(Hero hero)
      {
        this.m_owner.GetUnlockEvents(hero);
      }

      public void OnUnlockEventBackButtonClick()
      {
        this.m_owner.OnUnlockEventBackButtonClick();
      }

      public void SetPresentContent()
      {
        this.m_owner.SetPresentContent();
      }

      public int GiftBagItemComparer(BagItemBase a, BagItemBase b)
      {
        return this.m_owner.GiftBagItemComparer(a, b);
      }

      public void OnSendButtonClickDown()
      {
        this.m_owner.OnSendButtonClickDown();
      }

      public void SendUseGiftEvent()
      {
        this.m_owner.SendUseGiftEvent();
      }

      public IEnumerator WaitTimeThenDoEvent(int count)
      {
        return this.m_owner.WaitTimeThenDoEvent(count);
      }

      public void OnUseGiftItemSucceed(List<Goods> rewards)
      {
        this.m_owner.OnUseGiftItemSucceed(rewards);
      }

      public void OnGiftItemClick(FettersGiftItemUIController ctrl, bool isNeedShowDesc)
      {
        this.m_owner.OnGiftItemClick(ctrl, isNeedShowDesc);
      }

      public void LocalAddExpTick()
      {
        this.m_owner.LocalAddExpTick();
      }

      public IEnumerator Co_SetFavorabilityValue(
        int curFavoribilityLv,
        int addLv,
        float finalSliderValue)
      {
        return this.m_owner.Co_SetFavorabilityValue(curFavoribilityLv, addLv, finalSliderValue);
      }

      public void SetItemCountPanel()
      {
        this.m_owner.SetItemCountPanel();
      }

      public void ResetGiftScrollViewPosition()
      {
        this.m_owner.ResetGiftScrollViewPosition();
      }

      public void ShowDialog(int dialogId)
      {
        this.m_owner.ShowDialog(dialogId);
      }

      public void ShowNextDialog()
      {
        this.m_owner.ShowNextDialog();
      }

      public void OnConfessionPanelHideButtonClick()
      {
        this.m_owner.OnConfessionPanelHideButtonClick();
      }

      public void OnConfessionUIShowBgButtonClick()
      {
        this.m_owner.OnConfessionUIShowBgButtonClick();
      }

      public void OnConfessionPanelPenButtonClick()
      {
        this.m_owner.OnConfessionPanelPenButtonClick();
      }

      public void ShowConfessAnswerButtonList()
      {
        this.m_owner.ShowConfessAnswerButtonList();
      }

      public void ShowDialogAfterAnswer(int dialoId)
      {
        this.m_owner.ShowDialogAfterAnswer(dialoId);
      }

      public void OnConfessionLetterButtonClick()
      {
        this.m_owner.OnConfessionLetterButtonClick();
      }

      public void ShowConfessionLetter(int letterId)
      {
        this.m_owner.ShowConfessionLetter(letterId);
      }

      public IEnumerator ShowLetterWords(int letterId)
      {
        return this.m_owner.ShowLetterWords(letterId);
      }

      public void OnConfessionLetterPenButtonClick()
      {
        this.m_owner.OnConfessionLetterPenButtonClick();
      }

      public void OnConfessionPanelBackButtonClick()
      {
        this.m_owner.OnConfessionPanelBackButtonClick();
      }

      public void CloseConfessionPanel()
      {
        this.m_owner.CloseConfessionPanel();
      }

      public void ShowConfessSucceedEffect()
      {
        this.m_owner.ShowConfessSucceedEffect();
      }

      public IEnumerator C_ShowConfessSicceedEffect()
      {
        return this.m_owner.C_ShowConfessSicceedEffect();
      }

      public void SendConfesionReq()
      {
        this.m_owner.SendConfesionReq();
      }

      public void OnConfessionPanelShareButtonClick()
      {
        this.m_owner.OnConfessionPanelShareButtonClick();
      }

      public void OnConfessionSharePanelReturnButtonClick()
      {
        this.m_owner.OnConfessionSharePanelReturnButtonClick();
      }

      public void OnWeiBoClick()
      {
        this.m_owner.OnWeiBoClick();
      }

      public void OnWeChatClick()
      {
        this.m_owner.OnWeChatClick();
      }

      public IEnumerator Share(int typeId)
      {
        return this.m_owner.Share(typeId);
      }

      public IEnumerator CaptureFrame()
      {
        return this.m_owner.CaptureFrame();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void SetConfessionButtonState(Hero hero)
      {
        this.m_owner.SetConfessionButtonState(hero);
      }

      public IEnumerator Co_OnAnimationFinishedEnableInput(float time)
      {
        return this.m_owner.Co_OnAnimationFinishedEnableInput(time);
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnInformationButtonClick()
      {
        this.m_owner.OnInformationButtonClick();
      }

      public void OnFettersButtonClick()
      {
        this.m_owner.OnFettersButtonClick();
      }

      public void OnMemoryButtonClick()
      {
        this.m_owner.OnMemoryButtonClick();
      }

      public void OnGiftButtonClick()
      {
        this.m_owner.OnGiftButtonClick();
      }

      public void OnConfessionButtonClick()
      {
        this.m_owner.OnConfessionButtonClick();
      }

      public void InactiveWordGameObject()
      {
        this.m_owner.InactiveWordGameObject();
      }
    }
  }
}
