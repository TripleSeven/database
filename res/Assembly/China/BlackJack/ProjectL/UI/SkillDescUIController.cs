﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SkillDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SkillDescUIController : UIControllerBase
  {
    [AutoBind("./Lay/FrameImage/TOP/TitleNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Lay/FrameImage/TOP/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_costObj;
    [AutoBind("./Lay/FrameImage/TOP/Type/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_typeText;
    [AutoBind("./Lay/FrameImage/TOP/CD/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cdText;
    [AutoBind("./Lay/FrameImage/TOP/Distance/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_distanceText;
    [AutoBind("./Lay/FrameImage/TOP/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rangeText;
    [AutoBind("./Lay/FrameImage/SkillType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./Lay", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boundaryGo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSkillDesc(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSkillInfo SkillInfo { private set; get; }
  }
}
