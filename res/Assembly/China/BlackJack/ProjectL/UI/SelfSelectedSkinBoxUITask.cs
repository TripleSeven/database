﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedSkinBoxUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SelfSelectedSkinBoxUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode m_selfSelectedSkinBoxMode;
    private int m_selfSelectedSkinBoxID;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private SelfSelectedSkinBoxUIController m_selfSelectedSkinBoxUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedSkinBoxUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static SelfSelectedSkinBoxUITask StartSelfSelectedSkinBox(
      int selfSelectedSkinBoxID,
      SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode mode = SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode.Select)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void UpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadRes(List<string> assetList, Action onComplete)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmClick(List<int> selectSkinList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<int>> EventOnConfirmClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancelClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public enum SelfSelectedSkinBoxMode
    {
      None,
      View,
      Select,
    }
  }
}
