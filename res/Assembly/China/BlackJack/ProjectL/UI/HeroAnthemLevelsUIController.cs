﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemLevelsUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroAnthemLevelsUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./PlayerResource/Challenge/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeValueText;
    [AutoBind("./AchievementPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_chapterAchievementText;
    [AutoBind("./ChapterPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_chapterNameText;
    [AutoBind("./ChapterPanel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_chapterImage;
    [AutoBind("./StageListPanel/StageListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./AchievementListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementListPanelStateCtrl;
    [AutoBind("./AchievementListPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementListPanelBGButton;
    [AutoBind("./AchievementListPanel/Detail/AchievementListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementListScrollRect;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefab/StageButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroAnthemLevelListItemPrefab;
    private List<HeroAnthemLevelItemUIController> m_HeroAnthemLevelListItems;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private HeroAnthemLevelsUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_SetHeroAnthemConfigDataHeroAnthemInfo_hotfix;
    private LuaFunction m_SetAllHeroAnthemLevelListItemsList`1_hotfix;
    private LuaFunction m_AddHeroAnthemLevelListItemConfigDataHeroAnthemLevelInfoBoolean_hotfix;
    private LuaFunction m_ClearHeroAnthemLevelListItems_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_HeroAnthemLevelListItem_OnStartButtonClickHeroAnthemLevelItemUIController_hotfix;
    private LuaFunction m_HeroAnthemLevelListItem_OnAchievementsClickHeroAnthemLevelItemUIController_hotfix;
    private LuaFunction m_SetAchievementPanelConfigDataHeroAnthemLevelInfo_hotfix;
    private LuaFunction m_AddAchievementItemConfigDataBattleAchievementInfoList`1GameObjectBoolean_hotfix;
    private LuaFunction m_OnAchievementListPanelBGButtonClick_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnStartHeroAnthemLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartHeroAnthemLevelAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroAnthemLevelsUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAnthem(ConfigDataHeroAnthemInfo heroAnthemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllHeroAnthemLevelListItems(List<ConfigDataHeroAnthemLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroAnthemLevelListItem(ConfigDataHeroAnthemLevelInfo levelnfo, bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearHeroAnthemLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemLevelListItem_OnStartButtonClick(HeroAnthemLevelItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemLevelListItem_OnAchievementsClick(HeroAnthemLevelItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAchievementPanel(ConfigDataHeroAnthemLevelInfo anthemLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddAchievementItem(
      ConfigDataBattleAchievementInfo achievementInfo,
      List<Goods> rewards,
      GameObject prefab,
      bool complete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementListPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroAnthemLevelInfo> EventOnStartHeroAnthemLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroAnthemLevelsUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartHeroAnthemLevel(ConfigDataHeroAnthemLevelInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStartHeroAnthemLevel(ConfigDataHeroAnthemLevelInfo obj)
    {
      this.EventOnStartHeroAnthemLevel = (Action<ConfigDataHeroAnthemLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAnthemLevelsUIController m_owner;

      public LuaExportHelper(HeroAnthemLevelsUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnStartHeroAnthemLevel(ConfigDataHeroAnthemLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartHeroAnthemLevel(obj);
      }

      public void __clearDele_EventOnStartHeroAnthemLevel(ConfigDataHeroAnthemLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartHeroAnthemLevel(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Text m_challengeValueText
      {
        get
        {
          return this.m_owner.m_challengeValueText;
        }
        set
        {
          this.m_owner.m_challengeValueText = value;
        }
      }

      public Text m_chapterAchievementText
      {
        get
        {
          return this.m_owner.m_chapterAchievementText;
        }
        set
        {
          this.m_owner.m_chapterAchievementText = value;
        }
      }

      public Text m_chapterNameText
      {
        get
        {
          return this.m_owner.m_chapterNameText;
        }
        set
        {
          this.m_owner.m_chapterNameText = value;
        }
      }

      public Image m_chapterImage
      {
        get
        {
          return this.m_owner.m_chapterImage;
        }
        set
        {
          this.m_owner.m_chapterImage = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public CommonUIStateController m_achievementListPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_achievementListPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_achievementListPanelStateCtrl = value;
        }
      }

      public Button m_achievementListPanelBGButton
      {
        get
        {
          return this.m_owner.m_achievementListPanelBGButton;
        }
        set
        {
          this.m_owner.m_achievementListPanelBGButton = value;
        }
      }

      public ScrollRect m_achievementListScrollRect
      {
        get
        {
          return this.m_owner.m_achievementListScrollRect;
        }
        set
        {
          this.m_owner.m_achievementListScrollRect = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_heroAnthemLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_heroAnthemLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_heroAnthemLevelListItemPrefab = value;
        }
      }

      public List<HeroAnthemLevelItemUIController> m_HeroAnthemLevelListItems
      {
        get
        {
          return this.m_owner.m_HeroAnthemLevelListItems;
        }
        set
        {
          this.m_owner.m_HeroAnthemLevelListItems = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void AddHeroAnthemLevelListItem(ConfigDataHeroAnthemLevelInfo levelnfo, bool opened)
      {
        this.m_owner.AddHeroAnthemLevelListItem(levelnfo, opened);
      }

      public void ClearHeroAnthemLevelListItems()
      {
        this.m_owner.ClearHeroAnthemLevelListItems();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void HeroAnthemLevelListItem_OnStartButtonClick(HeroAnthemLevelItemUIController b)
      {
        this.m_owner.HeroAnthemLevelListItem_OnStartButtonClick(b);
      }

      public void HeroAnthemLevelListItem_OnAchievementsClick(HeroAnthemLevelItemUIController ctrl)
      {
        this.m_owner.HeroAnthemLevelListItem_OnAchievementsClick(ctrl);
      }

      public void SetAchievementPanel(ConfigDataHeroAnthemLevelInfo anthemLevelInfo)
      {
        this.m_owner.SetAchievementPanel(anthemLevelInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddAchievementItem(
        ConfigDataBattleAchievementInfo achievementInfo,
        List<Goods> rewards,
        GameObject prefab,
        bool complete)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnAchievementListPanelBGButtonClick()
      {
        this.m_owner.OnAchievementListPanelBGButtonClick();
      }
    }
  }
}
