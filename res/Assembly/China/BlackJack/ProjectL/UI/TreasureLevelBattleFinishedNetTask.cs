﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TreasureLevelBattleFinishedNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TreasureLevelBattleFinishedNetTask : UINetTask
  {
    private int m_treasureLevelID;
    private ProBattleReport m_battleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public TreasureLevelBattleFinishedNetTask(int treasureLevelID, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTreasureLevelBattleFinishedAck(int result, bool isWin, BattleReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsWin { private set; get; }

    public BattleReward Reward { private set; get; }
  }
}
