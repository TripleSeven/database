﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaBattlePrepareUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaBattlePrepareUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGCover", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageBGGameObject;
    [AutoBind("./RightTop/PauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pauseButton;
    [AutoBind("./Panel/FirstOrAfter", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_orderUIStateController;
    [AutoBind("./Panel/ShowFirst", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_orderFirstGameObject;
    [AutoBind("./Panel/ShowAfter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_orderAfterGameObject;
    [AutoBind("./Panel/BattleReportShowFirstAndAfter", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportOrderUIStateController;
    [AutoBind("./Panel/AutoToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_bpStageAutoToggle;
    [AutoBind("./Panel/PrepareState/DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTipUIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure/EffecctImage/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageTipText;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure/EffecctImage/FirstAndAfterImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTipFlagUIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bpStageConfirmButton;
    [AutoBind("./Panel/PrepareState/DetailGroup/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageConfirmButtonUIStateController;
    [AutoBind("./Panel/PrepareState/ActivePlayer/MyActive", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageMyActiveUIStateController;
    [AutoBind("./Panel/PrepareState/ActivePlayer/EnemyActive", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageEnemyActiveUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTimeUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTimeDetailUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageTimeText;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageTimeText2;
    [AutoBind("./ArenaBeginEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareCompleteCountdownEffectUIStateController;
    [AutoBind("./ArenaBeginEffectNoNum", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareCompleteNoCountdownEffectUIStateController;
    [AutoBind("./WatchPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleLivePanelGameObject;
    [AutoBind("./WatchPanel/WatchTip", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleLiveTipUIStateController;
    [AutoBind("./WatchPanel/WatchTip/WaitBegin/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleLiveWaitBeginTimeText;
    [AutoBind("./WatchPanel/DanmakuToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_danmakuToggle;
    [AutoBind("./WatchPanel/DanmakuToggle/InputWordButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputWordButton;
    [AutoBind("./WatchPanel/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_danmakuStateCtrl;
    [AutoBind("./WatchPanel/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputBackButton;
    [AutoBind("./WatchPanel/Danmaku/Input/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_danmakuInputField;
    [AutoBind("./WatchPanel/Danmaku/Input/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuSendButton;
    [AutoBind("./BanHeroMarginPanel", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_bpStagePanelMarginTransform;
    [AutoBind("./BanHeroMarginPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStagePanelUIStateController;
    [AutoBind("./BanHeroMarginPanel/LeftGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageLeftPanelGameObject;
    [AutoBind("./BanHeroMarginPanel/RightGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageRightPanelGameObject;
    [AutoBind("./BanHeroMarginPanel/MyKickOutHero", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageBanHeroUIStateController;
    [AutoBind("./BanHeroMarginPanel/MyKickOutHero/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_bpStageBanHeroGroupTransform;
    [AutoBind("./BanHeroMarginPanel/AttackButton/ButtonImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./BanHeroMarginPanel/AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageAttackButtonUIStateController;
    [AutoBind("./BanHeroMarginPanel/BattleMapPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageMapPreviewUIStateController;
    [AutoBind("./BanHeroMarginPanel/BattleMapPanel/Text/", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageMapPreviewName;
    [AutoBind("./BanHeroMarginPanel/BattleMapPanel/Image/", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bpStageMapPreviewImage;
    [AutoBind("./BanHeroMarginPanel/ExpressionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_expressionButton;
    [AutoBind("./BanHeroMarginPanel/ExpressionButton/ExpressionGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_expressionGroupUIStateController;
    [AutoBind("./BanHeroMarginPanel/ExpressionButton/ExpressionGroupDummy/BGMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_expressionGroupBGButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportButtonGroupUIStateController;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportSkipButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/StartAndPauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportPauseOrPlayButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/LastStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportBackwardButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/LastStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportBackwardUIStateController;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/NextStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportForwardButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/NextStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportForwardUIStateController;
    [AutoBind("./ExpressionMargin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_expressionMargionTransform;
    [AutoBind("./ExpressionMargin/Chat1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageLeftChatGameObject;
    [AutoBind("./ExpressionMargin/Chat2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageRightChatGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemPrefab;
    private PeakArenaBPStagePanelUIController[] m_bpStagePanelUIControllers;
    private PeakArenaBPStageChatUIController[] m_bpStageChatUIControllers;
    private CommonUIStateController[] m_bpStageTipFigureUIStateControllers;
    private BigExpressionController m_bigExpressionCtrl;
    private bool m_isClosingBpStageTime;
    private TimeSpan m_closingBpStageCountdown1;
    private TimeSpan m_closingBpStageCountdown2;
    private bool m_closingBpStageCountdownIsReserve;
    [DoNotToLua]
    private PeakArenaBattlePrepareUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_ShowBpStagteOpenAction_hotfix;
    private LuaFunction m_ShowOrderBoolean_hotfix;
    private LuaFunction m_SetBPStagePanelFinishedInt32_hotfix;
    private LuaFunction m_ShowPrepareCompleteCountdownAction_hotfix;
    private LuaFunction m_ShowBpStageMapPreviewConfigDataPeakArenaBattleInfo_hotfix;
    private LuaFunction m_HideBpStageMapPreview_hotfix;
    private LuaFunction m_ShowExpressionButtonBoolean_hotfix;
    private LuaFunction m_GetBPStagePanelUIControllerInt32_hotfix;
    private LuaFunction m_SetPlayerInt32BattleRoomPlayer_hotfix;
    private LuaFunction m_SetPlayerInt32PeakArenaBattleReportPlayerSummaryInfo_hotfix;
    private LuaFunction m_SetPlayerWinCountInt32Int32Boolean_hotfix;
    private LuaFunction m_SetPlayerStatusInt32PlayerBattleStatusBoolean_hotfix;
    private LuaFunction m_ShowPlayerExpressionInt32Int32_hotfix;
    private LuaFunction m_ActiveBpStagePanelInt32Boolean_hotfix;
    private LuaFunction m_ShowBpStagePanelActiveArrowBoolean_hotfix;
    private LuaFunction m_SetBpStageHerosInt32BPStageBooleanBoolean_hotfix;
    private LuaFunction m_AddHerosPeakArenaBPStagePanelUIControllerList`1PeakArenaBPStageHeroItemStateBoolean_hotfix;
    private LuaFunction m_ShowBpStageActivePlayerTipInt32Int32BPStageCommandType_hotfix;
    private LuaFunction m_HideBpStageActivePlayerTip_hotfix;
    private LuaFunction m_ShowBpStageTipStringInt32Int32BPStageCommandTypeBoolean_hotfix;
    private LuaFunction m_SetBPStageTipStringInt32Int32BPStageCommandType_hotfix;
    private LuaFunction m_HideBpStageTip_hotfix;
    private LuaFunction m_SetBpStageTipFigureCountInt32Int32String_hotfix;
    private LuaFunction m_ShowBpStageConfirmButtonBoolean_hotfix;
    private LuaFunction m_SetBpStageConfirmButtonStateBoolean_hotfix;
    private LuaFunction m_HideBpStageEnemyActiveDesc_hotfix;
    private LuaFunction m_ShowPickHeroEffectsInt32List`1_hotfix;
    private LuaFunction m_ShowBpStageWaitPickHeroEffectInt32Int32_hotfix;
    private LuaFunction m_ShowBanMyHeroEffectsList`1_hotfix;
    private LuaFunction m_SetAttackButtonReadyStateBoolean_hotfix;
    private LuaFunction m_ShowBpStageCountdownBoolean_hotfix;
    private LuaFunction m_HideBpStageCountdown_hotfix;
    private LuaFunction m_SetBpStageCountdownTimeSpanTimeSpan_hotfix;
    private LuaFunction m_SetBpStageCountdownTimeSpanBoolean_hotfix;
    private LuaFunction m_ResetBattleReport_hotfix;
    private LuaFunction m_ShowBattleReportButtonGroupBoolean_hotfix;
    private LuaFunction m_SetBattleReportPausedBoolean_hotfix;
    private LuaFunction m_SetBattleReportButtonStatusBooleanBoolean_hotfix;
    private LuaFunction m_ShowBattleReportOrder_hotfix;
    private LuaFunction m_ShowBattleReportMapPreviewConfigDataPeakArenaBattleInfo_hotfix;
    private LuaFunction m_HideBattleReportMapPreview_hotfix;
    private LuaFunction m_ShowBattleReportTipInt32StringBPStageCommandType_hotfix;
    private LuaFunction m_ShowBattleReportBanHeroEffectsInt32List`1_hotfix;
    private LuaFunction m_ShowBattleReportPrepareCompleteAction_hotfix;
    private LuaFunction m_ShowBattleLivePanelBoolean_hotfix;
    private LuaFunction m_ShowBattleLiveWaitPlayer_hotfix;
    private LuaFunction m_ShowBattleLiveWaitBeginCountdownTimeSpan_hotfix;
    private LuaFunction m_HideBattleLiveTip_hotfix;
    private LuaFunction m_ShowBattleLiveBpStageCountdownBoolean_hotfix;
    private LuaFunction m_SetBattleLiveBpStageCountdownInt32TimeSpanTimeSpan_hotfix;
    private LuaFunction m_ClearDanmakuUIInputField_hotfix;
    private LuaFunction m_IsBpStageAuto_hotfix;
    private LuaFunction m_OnPauseButtonClick_hotfix;
    private LuaFunction m_OnExpressionButtonClick_hotfix;
    private LuaFunction m_OnBigExpressionClickInt32_hotfix;
    private LuaFunction m_OnBigExpressionBGClick_hotfix;
    private LuaFunction m_ShowExpressionGroupBoolean_hotfix;
    private LuaFunction m_OnBpStageAutoToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnBPStageConfirmButtonClick_hotfix;
    private LuaFunction m_OnAttackButtonClick_hotfix;
    private LuaFunction m_OnBattleReportSkipButtonClick_hotfix;
    private LuaFunction m_OnBattleReportPauseOrPlayButtonClick_hotfix;
    private LuaFunction m_OnBattleReportBackwardButtonClick_hotfix;
    private LuaFunction m_OnBattleReportForwardButtonClick_hotfix;
    private LuaFunction m_OnDanmakuToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnDanmakuInputWordButtonClick_hotfix;
    private LuaFunction m_OnDanmakuInputBackButtonClick_hotfix;
    private LuaFunction m_OnDanmakuSendButtonClick_hotfix;
    private LuaFunction m_PeakArenaBanPickPanelUIController_OnHeroItemClickPeakArenaBpStageHeroItemUIController_hotfix;
    private LuaFunction m_add_EventOnPauseBattleAction_hotfix;
    private LuaFunction m_remove_EventOnPauseBattleAction_hotfix;
    private LuaFunction m_add_EventOnBpStageConfirmAction_hotfix;
    private LuaFunction m_remove_EventOnBpStageConfirmAction_hotfix;
    private LuaFunction m_add_EventOnBpStageAutoAction`1_hotfix;
    private LuaFunction m_remove_EventOnBpStageAutoAction`1_hotfix;
    private LuaFunction m_add_EventOnAttackAction_hotfix;
    private LuaFunction m_remove_EventOnAttackAction_hotfix;
    private LuaFunction m_add_EventOnBpStageHeroClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnBpStageHeroClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBattleReportSkipAction_hotfix;
    private LuaFunction m_remove_EventOnBattleReportSkipAction_hotfix;
    private LuaFunction m_add_EventOnBattleReportPauseAction_hotfix;
    private LuaFunction m_remove_EventOnBattleReportPauseAction_hotfix;
    private LuaFunction m_add_EventOnBattleReportPlayAction_hotfix;
    private LuaFunction m_remove_EventOnBattleReportPlayAction_hotfix;
    private LuaFunction m_add_EventOnBattleReportBackwardAction_hotfix;
    private LuaFunction m_remove_EventOnBattleReportBackwardAction_hotfix;
    private LuaFunction m_add_EventOnBattleReportForwardAction_hotfix;
    private LuaFunction m_remove_EventOnBattleReportForwardAction_hotfix;
    private LuaFunction m_add_EventOnSendDanmakuAction`1_hotfix;
    private LuaFunction m_remove_EventOnSendDanmakuAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStagteOpen(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrder(bool isFirst)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBPStagePanelFinished(int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPrepareCompleteCountdown(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageMapPreview(ConfigDataPeakArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageMapPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExpressionButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PeakArenaBPStagePanelUIController GetBPStagePanelUIController(
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(int playerIndex, BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(int playerIndex, PeakArenaBattleReportPlayerSummaryInfo player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerWinCount(int playerIndex, int winCount, bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerStatus(int playerIndex, PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerExpression(int playerIndex, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActiveBpStagePanel(int playerIndex, bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStagePanelActiveArrow(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBpStageHeros(int playerIndex, BPStage bpStage, bool showJob, bool selectable = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeros(
      PeakArenaBPStagePanelUIController ctrl,
      List<BPStageHeroSetupInfo> heroSetups,
      PeakArenaBPStageHeroItemState state,
      bool showJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageActivePlayerTip(
      int activePlayerIndex,
      int myPlayerIndex,
      BPStageCommandType bpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageActivePlayerTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageTip(
      string txt,
      int figureCount,
      int selectedFigureCount,
      BPStageCommandType bpType,
      bool isTurnChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBPStageTip(
      string text,
      int figureCount,
      int selectedFigureCount,
      BPStageCommandType bpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBpStageTipFigureCount(int count, int selectedCount, string selectedStateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBpStageConfirmButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBpStageConfirmButtonState(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBpStageEnemyActiveDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPickHeroEffects(int playerIndex, List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageWaitPickHeroEffect(int playerIndex, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanMyHeroEffects(List<BPStageHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackButtonReadyState(bool isReady)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageCountdown(bool isTurnChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBpStageCountdown(TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBpStageCountdown(TimeSpan ts, bool isReserve)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportButtonGroup(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportPaused(bool isPaused)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportButtonStatus(bool canBackward, bool canForward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportMapPreview(ConfigDataPeakArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleReportMapPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportTip(int activePlayerIndex, string txt, BPStageCommandType bpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportBanHeroEffects(
      int playerIndex,
      List<BPStageHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportPrepareComplete(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLivePanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveWaitPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveWaitBeginCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleLiveTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveBpStageCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLiveBpStageCountdown(int playerIndex, TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearDanmakuUIInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBpStageAuto()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPauseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowExpressionGroup(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBpStageAutoToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBPStageConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportPauseOrPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportBackwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportForwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputWordButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBanPickPanelUIController_OnHeroItemClick(
      PeakArenaBpStageHeroItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPauseBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBpStageConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnBpStageAuto
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaBpStageHeroItemUIController> EventOnBpStageHeroClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPause
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPlay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportBackward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportForward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSendDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaBattlePrepareUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPauseBattle()
    {
      this.EventOnPauseBattle = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBpStageConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBpStageConfirm()
    {
      this.EventOnBpStageConfirm = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBpStageAuto(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBpStageAuto(bool obj)
    {
      this.EventOnBpStageAuto = (Action<bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAttack()
    {
      this.EventOnAttack = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBpStageHeroClick(PeakArenaBpStageHeroItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBpStageHeroClick(PeakArenaBpStageHeroItemUIController obj)
    {
      this.EventOnBpStageHeroClick = (Action<PeakArenaBpStageHeroItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleReportSkip()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleReportSkip()
    {
      this.EventOnBattleReportSkip = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleReportPause()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleReportPause()
    {
      this.EventOnBattleReportPause = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleReportPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleReportPlay()
    {
      this.EventOnBattleReportPlay = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleReportBackward()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleReportBackward()
    {
      this.EventOnBattleReportBackward = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBattleReportForward()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBattleReportForward()
    {
      this.EventOnBattleReportForward = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSendDanmaku(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSendDanmaku(string obj)
    {
      this.EventOnSendDanmaku = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaBattlePrepareUIController m_owner;

      public LuaExportHelper(PeakArenaBattlePrepareUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnPauseBattle()
      {
        this.m_owner.__callDele_EventOnPauseBattle();
      }

      public void __clearDele_EventOnPauseBattle()
      {
        this.m_owner.__clearDele_EventOnPauseBattle();
      }

      public void __callDele_EventOnBpStageConfirm()
      {
        this.m_owner.__callDele_EventOnBpStageConfirm();
      }

      public void __clearDele_EventOnBpStageConfirm()
      {
        this.m_owner.__clearDele_EventOnBpStageConfirm();
      }

      public void __callDele_EventOnBpStageAuto(bool obj)
      {
        this.m_owner.__callDele_EventOnBpStageAuto(obj);
      }

      public void __clearDele_EventOnBpStageAuto(bool obj)
      {
        this.m_owner.__clearDele_EventOnBpStageAuto(obj);
      }

      public void __callDele_EventOnAttack()
      {
        this.m_owner.__callDele_EventOnAttack();
      }

      public void __clearDele_EventOnAttack()
      {
        this.m_owner.__clearDele_EventOnAttack();
      }

      public void __callDele_EventOnBpStageHeroClick(PeakArenaBpStageHeroItemUIController obj)
      {
        this.m_owner.__callDele_EventOnBpStageHeroClick(obj);
      }

      public void __clearDele_EventOnBpStageHeroClick(PeakArenaBpStageHeroItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnBpStageHeroClick(obj);
      }

      public void __callDele_EventOnBattleReportSkip()
      {
        this.m_owner.__callDele_EventOnBattleReportSkip();
      }

      public void __clearDele_EventOnBattleReportSkip()
      {
        this.m_owner.__clearDele_EventOnBattleReportSkip();
      }

      public void __callDele_EventOnBattleReportPause()
      {
        this.m_owner.__callDele_EventOnBattleReportPause();
      }

      public void __clearDele_EventOnBattleReportPause()
      {
        this.m_owner.__clearDele_EventOnBattleReportPause();
      }

      public void __callDele_EventOnBattleReportPlay()
      {
        this.m_owner.__callDele_EventOnBattleReportPlay();
      }

      public void __clearDele_EventOnBattleReportPlay()
      {
        this.m_owner.__clearDele_EventOnBattleReportPlay();
      }

      public void __callDele_EventOnBattleReportBackward()
      {
        this.m_owner.__callDele_EventOnBattleReportBackward();
      }

      public void __clearDele_EventOnBattleReportBackward()
      {
        this.m_owner.__clearDele_EventOnBattleReportBackward();
      }

      public void __callDele_EventOnBattleReportForward()
      {
        this.m_owner.__callDele_EventOnBattleReportForward();
      }

      public void __clearDele_EventOnBattleReportForward()
      {
        this.m_owner.__clearDele_EventOnBattleReportForward();
      }

      public void __callDele_EventOnSendDanmaku(string obj)
      {
        this.m_owner.__callDele_EventOnSendDanmaku(obj);
      }

      public void __clearDele_EventOnSendDanmaku(string obj)
      {
        this.m_owner.__clearDele_EventOnSendDanmaku(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public GameObject m_bpStageBGGameObject
      {
        get
        {
          return this.m_owner.m_bpStageBGGameObject;
        }
        set
        {
          this.m_owner.m_bpStageBGGameObject = value;
        }
      }

      public Button m_pauseButton
      {
        get
        {
          return this.m_owner.m_pauseButton;
        }
        set
        {
          this.m_owner.m_pauseButton = value;
        }
      }

      public CommonUIStateController m_orderUIStateController
      {
        get
        {
          return this.m_owner.m_orderUIStateController;
        }
        set
        {
          this.m_owner.m_orderUIStateController = value;
        }
      }

      public GameObject m_orderFirstGameObject
      {
        get
        {
          return this.m_owner.m_orderFirstGameObject;
        }
        set
        {
          this.m_owner.m_orderFirstGameObject = value;
        }
      }

      public GameObject m_orderAfterGameObject
      {
        get
        {
          return this.m_owner.m_orderAfterGameObject;
        }
        set
        {
          this.m_owner.m_orderAfterGameObject = value;
        }
      }

      public CommonUIStateController m_battleReportOrderUIStateController
      {
        get
        {
          return this.m_owner.m_battleReportOrderUIStateController;
        }
        set
        {
          this.m_owner.m_battleReportOrderUIStateController = value;
        }
      }

      public Toggle m_bpStageAutoToggle
      {
        get
        {
          return this.m_owner.m_bpStageAutoToggle;
        }
        set
        {
          this.m_owner.m_bpStageAutoToggle = value;
        }
      }

      public CommonUIStateController m_bpStageTipUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageTipUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageTipUIStateController = value;
        }
      }

      public Text m_bpStageTipText
      {
        get
        {
          return this.m_owner.m_bpStageTipText;
        }
        set
        {
          this.m_owner.m_bpStageTipText = value;
        }
      }

      public CommonUIStateController m_bpStageTipFlagUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageTipFlagUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageTipFlagUIStateController = value;
        }
      }

      public Button m_bpStageConfirmButton
      {
        get
        {
          return this.m_owner.m_bpStageConfirmButton;
        }
        set
        {
          this.m_owner.m_bpStageConfirmButton = value;
        }
      }

      public CommonUIStateController m_bpStageConfirmButtonUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageConfirmButtonUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageConfirmButtonUIStateController = value;
        }
      }

      public CommonUIStateController m_bpStageMyActiveUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageMyActiveUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageMyActiveUIStateController = value;
        }
      }

      public CommonUIStateController m_bpStageEnemyActiveUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageEnemyActiveUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageEnemyActiveUIStateController = value;
        }
      }

      public CommonUIStateController m_bpStageTimeUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageTimeUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageTimeUIStateController = value;
        }
      }

      public CommonUIStateController m_bpStageTimeDetailUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageTimeDetailUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageTimeDetailUIStateController = value;
        }
      }

      public Text m_bpStageTimeText
      {
        get
        {
          return this.m_owner.m_bpStageTimeText;
        }
        set
        {
          this.m_owner.m_bpStageTimeText = value;
        }
      }

      public Text m_bpStageTimeText2
      {
        get
        {
          return this.m_owner.m_bpStageTimeText2;
        }
        set
        {
          this.m_owner.m_bpStageTimeText2 = value;
        }
      }

      public CommonUIStateController m_prepareCompleteCountdownEffectUIStateController
      {
        get
        {
          return this.m_owner.m_prepareCompleteCountdownEffectUIStateController;
        }
        set
        {
          this.m_owner.m_prepareCompleteCountdownEffectUIStateController = value;
        }
      }

      public CommonUIStateController m_prepareCompleteNoCountdownEffectUIStateController
      {
        get
        {
          return this.m_owner.m_prepareCompleteNoCountdownEffectUIStateController;
        }
        set
        {
          this.m_owner.m_prepareCompleteNoCountdownEffectUIStateController = value;
        }
      }

      public GameObject m_battleLivePanelGameObject
      {
        get
        {
          return this.m_owner.m_battleLivePanelGameObject;
        }
        set
        {
          this.m_owner.m_battleLivePanelGameObject = value;
        }
      }

      public CommonUIStateController m_battleLiveTipUIStateController
      {
        get
        {
          return this.m_owner.m_battleLiveTipUIStateController;
        }
        set
        {
          this.m_owner.m_battleLiveTipUIStateController = value;
        }
      }

      public Text m_battleLiveWaitBeginTimeText
      {
        get
        {
          return this.m_owner.m_battleLiveWaitBeginTimeText;
        }
        set
        {
          this.m_owner.m_battleLiveWaitBeginTimeText = value;
        }
      }

      public Toggle m_danmakuToggle
      {
        get
        {
          return this.m_owner.m_danmakuToggle;
        }
        set
        {
          this.m_owner.m_danmakuToggle = value;
        }
      }

      public Button m_danmakuInputWordButton
      {
        get
        {
          return this.m_owner.m_danmakuInputWordButton;
        }
        set
        {
          this.m_owner.m_danmakuInputWordButton = value;
        }
      }

      public CommonUIStateController m_danmakuStateCtrl
      {
        get
        {
          return this.m_owner.m_danmakuStateCtrl;
        }
        set
        {
          this.m_owner.m_danmakuStateCtrl = value;
        }
      }

      public Button m_danmakuInputBackButton
      {
        get
        {
          return this.m_owner.m_danmakuInputBackButton;
        }
        set
        {
          this.m_owner.m_danmakuInputBackButton = value;
        }
      }

      public InputField m_danmakuInputField
      {
        get
        {
          return this.m_owner.m_danmakuInputField;
        }
        set
        {
          this.m_owner.m_danmakuInputField = value;
        }
      }

      public Button m_danmakuSendButton
      {
        get
        {
          return this.m_owner.m_danmakuSendButton;
        }
        set
        {
          this.m_owner.m_danmakuSendButton = value;
        }
      }

      public RectTransform m_bpStagePanelMarginTransform
      {
        get
        {
          return this.m_owner.m_bpStagePanelMarginTransform;
        }
        set
        {
          this.m_owner.m_bpStagePanelMarginTransform = value;
        }
      }

      public CommonUIStateController m_bpStagePanelUIStateController
      {
        get
        {
          return this.m_owner.m_bpStagePanelUIStateController;
        }
        set
        {
          this.m_owner.m_bpStagePanelUIStateController = value;
        }
      }

      public GameObject m_bpStageLeftPanelGameObject
      {
        get
        {
          return this.m_owner.m_bpStageLeftPanelGameObject;
        }
        set
        {
          this.m_owner.m_bpStageLeftPanelGameObject = value;
        }
      }

      public GameObject m_bpStageRightPanelGameObject
      {
        get
        {
          return this.m_owner.m_bpStageRightPanelGameObject;
        }
        set
        {
          this.m_owner.m_bpStageRightPanelGameObject = value;
        }
      }

      public CommonUIStateController m_bpStageBanHeroUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageBanHeroUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageBanHeroUIStateController = value;
        }
      }

      public Transform m_bpStageBanHeroGroupTransform
      {
        get
        {
          return this.m_owner.m_bpStageBanHeroGroupTransform;
        }
        set
        {
          this.m_owner.m_bpStageBanHeroGroupTransform = value;
        }
      }

      public Button m_attackButton
      {
        get
        {
          return this.m_owner.m_attackButton;
        }
        set
        {
          this.m_owner.m_attackButton = value;
        }
      }

      public CommonUIStateController m_bpStageAttackButtonUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageAttackButtonUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageAttackButtonUIStateController = value;
        }
      }

      public CommonUIStateController m_bpStageMapPreviewUIStateController
      {
        get
        {
          return this.m_owner.m_bpStageMapPreviewUIStateController;
        }
        set
        {
          this.m_owner.m_bpStageMapPreviewUIStateController = value;
        }
      }

      public Text m_bpStageMapPreviewName
      {
        get
        {
          return this.m_owner.m_bpStageMapPreviewName;
        }
        set
        {
          this.m_owner.m_bpStageMapPreviewName = value;
        }
      }

      public Image m_bpStageMapPreviewImage
      {
        get
        {
          return this.m_owner.m_bpStageMapPreviewImage;
        }
        set
        {
          this.m_owner.m_bpStageMapPreviewImage = value;
        }
      }

      public Button m_expressionButton
      {
        get
        {
          return this.m_owner.m_expressionButton;
        }
        set
        {
          this.m_owner.m_expressionButton = value;
        }
      }

      public CommonUIStateController m_expressionGroupUIStateController
      {
        get
        {
          return this.m_owner.m_expressionGroupUIStateController;
        }
        set
        {
          this.m_owner.m_expressionGroupUIStateController = value;
        }
      }

      public Button m_expressionGroupBGButton
      {
        get
        {
          return this.m_owner.m_expressionGroupBGButton;
        }
        set
        {
          this.m_owner.m_expressionGroupBGButton = value;
        }
      }

      public CommonUIStateController m_battleReportButtonGroupUIStateController
      {
        get
        {
          return this.m_owner.m_battleReportButtonGroupUIStateController;
        }
        set
        {
          this.m_owner.m_battleReportButtonGroupUIStateController = value;
        }
      }

      public Button m_battleReportSkipButton
      {
        get
        {
          return this.m_owner.m_battleReportSkipButton;
        }
        set
        {
          this.m_owner.m_battleReportSkipButton = value;
        }
      }

      public Button m_battleReportPauseOrPlayButton
      {
        get
        {
          return this.m_owner.m_battleReportPauseOrPlayButton;
        }
        set
        {
          this.m_owner.m_battleReportPauseOrPlayButton = value;
        }
      }

      public Button m_battleReportBackwardButton
      {
        get
        {
          return this.m_owner.m_battleReportBackwardButton;
        }
        set
        {
          this.m_owner.m_battleReportBackwardButton = value;
        }
      }

      public CommonUIStateController m_battleReportBackwardUIStateController
      {
        get
        {
          return this.m_owner.m_battleReportBackwardUIStateController;
        }
        set
        {
          this.m_owner.m_battleReportBackwardUIStateController = value;
        }
      }

      public Button m_battleReportForwardButton
      {
        get
        {
          return this.m_owner.m_battleReportForwardButton;
        }
        set
        {
          this.m_owner.m_battleReportForwardButton = value;
        }
      }

      public CommonUIStateController m_battleReportForwardUIStateController
      {
        get
        {
          return this.m_owner.m_battleReportForwardUIStateController;
        }
        set
        {
          this.m_owner.m_battleReportForwardUIStateController = value;
        }
      }

      public RectTransform m_expressionMargionTransform
      {
        get
        {
          return this.m_owner.m_expressionMargionTransform;
        }
        set
        {
          this.m_owner.m_expressionMargionTransform = value;
        }
      }

      public GameObject m_bpStageLeftChatGameObject
      {
        get
        {
          return this.m_owner.m_bpStageLeftChatGameObject;
        }
        set
        {
          this.m_owner.m_bpStageLeftChatGameObject = value;
        }
      }

      public GameObject m_bpStageRightChatGameObject
      {
        get
        {
          return this.m_owner.m_bpStageRightChatGameObject;
        }
        set
        {
          this.m_owner.m_bpStageRightChatGameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_heroItemPrefab
      {
        get
        {
          return this.m_owner.m_heroItemPrefab;
        }
        set
        {
          this.m_owner.m_heroItemPrefab = value;
        }
      }

      public PeakArenaBPStagePanelUIController[] m_bpStagePanelUIControllers
      {
        get
        {
          return this.m_owner.m_bpStagePanelUIControllers;
        }
        set
        {
          this.m_owner.m_bpStagePanelUIControllers = value;
        }
      }

      public PeakArenaBPStageChatUIController[] m_bpStageChatUIControllers
      {
        get
        {
          return this.m_owner.m_bpStageChatUIControllers;
        }
        set
        {
          this.m_owner.m_bpStageChatUIControllers = value;
        }
      }

      public CommonUIStateController[] m_bpStageTipFigureUIStateControllers
      {
        get
        {
          return this.m_owner.m_bpStageTipFigureUIStateControllers;
        }
        set
        {
          this.m_owner.m_bpStageTipFigureUIStateControllers = value;
        }
      }

      public BigExpressionController m_bigExpressionCtrl
      {
        get
        {
          return this.m_owner.m_bigExpressionCtrl;
        }
        set
        {
          this.m_owner.m_bigExpressionCtrl = value;
        }
      }

      public bool m_isClosingBpStageTime
      {
        get
        {
          return this.m_owner.m_isClosingBpStageTime;
        }
        set
        {
          this.m_owner.m_isClosingBpStageTime = value;
        }
      }

      public TimeSpan m_closingBpStageCountdown1
      {
        get
        {
          return this.m_owner.m_closingBpStageCountdown1;
        }
        set
        {
          this.m_owner.m_closingBpStageCountdown1 = value;
        }
      }

      public TimeSpan m_closingBpStageCountdown2
      {
        get
        {
          return this.m_owner.m_closingBpStageCountdown2;
        }
        set
        {
          this.m_owner.m_closingBpStageCountdown2 = value;
        }
      }

      public bool m_closingBpStageCountdownIsReserve
      {
        get
        {
          return this.m_owner.m_closingBpStageCountdownIsReserve;
        }
        set
        {
          this.m_owner.m_closingBpStageCountdownIsReserve = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public PeakArenaBPStagePanelUIController GetBPStagePanelUIController(
        int playerIndex)
      {
        return this.m_owner.GetBPStagePanelUIController(playerIndex);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddHeros(
        PeakArenaBPStagePanelUIController ctrl,
        List<BPStageHeroSetupInfo> heroSetups,
        PeakArenaBPStageHeroItemState state,
        bool showJob)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetBPStageTip(
        string text,
        int figureCount,
        int selectedFigureCount,
        BPStageCommandType bpType)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetBpStageTipFigureCount(int count, int selectedCount, string selectedStateName)
      {
        this.m_owner.SetBpStageTipFigureCount(count, selectedCount, selectedStateName);
      }

      public void ShowBpStageConfirmButton(bool show)
      {
        this.m_owner.ShowBpStageConfirmButton(show);
      }

      public void SetBpStageConfirmButtonState(bool enable)
      {
        this.m_owner.SetBpStageConfirmButtonState(enable);
      }

      public void HideBpStageEnemyActiveDesc()
      {
        this.m_owner.HideBpStageEnemyActiveDesc();
      }

      public void OnPauseButtonClick()
      {
        this.m_owner.OnPauseButtonClick();
      }

      public void OnExpressionButtonClick()
      {
        this.m_owner.OnExpressionButtonClick();
      }

      public void OnBigExpressionClick(int id)
      {
        this.m_owner.OnBigExpressionClick(id);
      }

      public void OnBigExpressionBGClick()
      {
        this.m_owner.OnBigExpressionBGClick();
      }

      public void ShowExpressionGroup(bool show)
      {
        this.m_owner.ShowExpressionGroup(show);
      }

      public void OnBpStageAutoToggleValueChanged(bool isOn)
      {
        this.m_owner.OnBpStageAutoToggleValueChanged(isOn);
      }

      public void OnBPStageConfirmButtonClick()
      {
        this.m_owner.OnBPStageConfirmButtonClick();
      }

      public void OnAttackButtonClick()
      {
        this.m_owner.OnAttackButtonClick();
      }

      public void OnBattleReportSkipButtonClick()
      {
        this.m_owner.OnBattleReportSkipButtonClick();
      }

      public void OnBattleReportPauseOrPlayButtonClick()
      {
        this.m_owner.OnBattleReportPauseOrPlayButtonClick();
      }

      public void OnBattleReportBackwardButtonClick()
      {
        this.m_owner.OnBattleReportBackwardButtonClick();
      }

      public void OnBattleReportForwardButtonClick()
      {
        this.m_owner.OnBattleReportForwardButtonClick();
      }

      public void OnDanmakuToggleValueChanged(bool isOn)
      {
        this.m_owner.OnDanmakuToggleValueChanged(isOn);
      }

      public void OnDanmakuInputWordButtonClick()
      {
        this.m_owner.OnDanmakuInputWordButtonClick();
      }

      public void OnDanmakuInputBackButtonClick()
      {
        this.m_owner.OnDanmakuInputBackButtonClick();
      }

      public void OnDanmakuSendButtonClick()
      {
        this.m_owner.OnDanmakuSendButtonClick();
      }

      public void PeakArenaBanPickPanelUIController_OnHeroItemClick(
        PeakArenaBpStageHeroItemUIController ctrl)
      {
        this.m_owner.PeakArenaBanPickPanelUIController_OnHeroItemClick(ctrl);
      }
    }
  }
}
