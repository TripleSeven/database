﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaOpponentViewNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArenaOpponentViewNetTask : UINetTask
  {
    private int m_opponentIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentViewNetTask(int opponentIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnArenaOpponentViewAck(int result, List<ProBattleHero> heros, int battlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ProBattleHero> Heros { private set; get; }

    public int BattlePower { private set; get; }
  }
}
