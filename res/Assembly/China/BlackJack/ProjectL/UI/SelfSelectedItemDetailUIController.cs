﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedItemDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SelfSelectedItemDetailUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemBuyPanelUIStateController;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemCountBgGo;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/GoodCount", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodCountObj;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemHaveCountText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/DescPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemPriceIcon;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPriceIconEffectGameObject;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemPriceText;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyPanelCloseButton;
    [AutoBind("./LayoutRoot/BuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyButton;
    [AutoBind("./LayoutRoot/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemUseButton;
    [AutoBind("./ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_resonanceInfoPanel;
    [AutoBind("./ResonanceInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanelNameText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/2SuitInfo/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel2SuitInfoText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/4SuitInfo/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel4SuitInfoText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private SelfSelectedItemBoxUITask m_selfSelectedItemBoxUITask;
    private SelfSelectedItemBoxUITask.SelfSelectedItemBoxMode mode;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    private int m_selfChooseIndex;
    private int m_itemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedItemDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBuyItem(
      SelfSelectedItemBoxUITask selfSelectedItemBoxUITask,
      StoreId storeId,
      int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitUseItem(
      SelfSelectedItemBoxUITask selfSelectedItemBoxUITask,
      int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClosePanel(Action OnCompleteClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelfChooseItemDetailPanel(int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetItemPrice(GoodsType currencyType, int price)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetItemDetailPanel(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetStoreGoods(int fixedStoreItemId, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Goods GetSelfChooseGoods(int fixedStoreItemId, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Goods GetSelfChooseGoods2(int itemID, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_CrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBuyItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnUseItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<Goods>> EventOnBuySuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<Goods>> EventOnUseSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
