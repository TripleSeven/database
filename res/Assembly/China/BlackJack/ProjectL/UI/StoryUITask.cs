﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoryUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public sealed class StoryUITask : UITask
  {
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private const string ParamKeyStoryOutlineInfoID = "ParamKeyStoryOutlineInfoID";
    private ConfigDataStoryOutlineInfo m_storyOutlineInfo;
    private const string ParamKeyStateType = "ParamKeyStateType";
    private StoryUITask.StateType m_stateType;
    private const string ParamKeyOnEnd = "ParamKeyOnEnd";
    private Action m_onEnd;
    private StoryUIController m_storyUIController;
    private readonly UITaskBase.LayerDesc[] m_layerDescArray;
    private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoryUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      UIIntent fromIntent,
      int storyOutlineInfoID,
      StoryUITask.StateType stateType,
      Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool IsNeedUpdateDataCache()
    {
      return false;
    }

    protected override void UpdateDataCache()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      base.InitLayerStateOnLoadAllResCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutomationInitUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void PostUpdateView()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoryUIController_Close()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnSkipButtonClick(UIControllerBase controllerBase)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromIntentForPrepare(UIIntentCustom intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    public enum StateType
    {
      UserGuide,
      Rift,
    }
  }
}
