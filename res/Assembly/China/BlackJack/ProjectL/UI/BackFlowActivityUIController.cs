﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BackFlowActivityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BackFlowActivityUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_backFlowActivityStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./RuleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoButton;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Detail/TitleToggleGroup/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_daysButtonContent;
    [AutoBind("./Detail/QuestGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_missionsScrollRect;
    [AutoBind("./Detail/QuestGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionsScrollViewContent;
    [AutoBind("./Prefab/MissonItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missonItemPrefab;
    [AutoBind("./Detail/UnderCountGroup/CountGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_integralContentGroup;
    [AutoBind("./Detail/UnderCountGroup/NowCountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralGroupCountValueText;
    [AutoBind("./Detail/ResidueTimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_residueTimeValueText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_curSelectday;
    private HeroCharUIController m_heroCharUIController;
    private BackFlowDayButtonUIController m_curSelectDayButtonCtrl;
    [DoNotToLua]
    private BackFlowActivityUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_UpdateViewInBackFlowActivity_hotfix;
    private LuaFunction m_InitDayButtons_hotfix;
    private LuaFunction m_OnDayButtonClickBackFlowDayButtonUIController_hotfix;
    private LuaFunction m_InitMissionPanel_hotfix;
    private LuaFunction m_OnMissionGotoButtonClickGetPathData_hotfix;
    private LuaFunction m_OnMissionGetButtonClickBackFlowMissonUIController_hotfix;
    private LuaFunction m_InitIntegralPanel_hotfix;
    private LuaFunction m_OnIntegralGoodsClickBackFlowIntegralGoodsUIController_hotfix;
    private LuaFunction m_SetResidueTimeTimeSpan_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnInfoButtonClick_hotfix;
    private LuaFunction m_ClearData_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnMissionGotoAction`1_hotfix;
    private LuaFunction m_remove_EventOnMissionGotoAction`1_hotfix;
    private LuaFunction m_add_EventOnMissionGetAction`1_hotfix;
    private LuaFunction m_remove_EventOnMissionGetAction`1_hotfix;
    private LuaFunction m_add_EventOnIntegralGoodsClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnIntegralGoodsClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInBackFlowActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDayButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDayButtonClick(BackFlowDayButtonUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitMissionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionGotoButtonClick(GetPathData pathInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionGetButtonClick(BackFlowMissonUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitIntegralPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnIntegralGoodsClick(BackFlowIntegralGoodsUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetResidueTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GetPathData> EventOnMissionGoto
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BackFlowMissonUIController> EventOnMissionGet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BackFlowIntegralGoodsUIController> EventOnIntegralGoodsClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BackFlowActivityUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMissionGoto(GetPathData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMissionGoto(GetPathData obj)
    {
      this.EventOnMissionGoto = (Action<GetPathData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMissionGet(BackFlowMissonUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMissionGet(BackFlowMissonUIController obj)
    {
      this.EventOnMissionGet = (Action<BackFlowMissonUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnIntegralGoodsClick(BackFlowIntegralGoodsUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnIntegralGoodsClick(BackFlowIntegralGoodsUIController obj)
    {
      this.EventOnIntegralGoodsClick = (Action<BackFlowIntegralGoodsUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BackFlowActivityUIController m_owner;

      public LuaExportHelper(BackFlowActivityUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnMissionGoto(GetPathData obj)
      {
        this.m_owner.__callDele_EventOnMissionGoto(obj);
      }

      public void __clearDele_EventOnMissionGoto(GetPathData obj)
      {
        this.m_owner.__clearDele_EventOnMissionGoto(obj);
      }

      public void __callDele_EventOnMissionGet(BackFlowMissonUIController obj)
      {
        this.m_owner.__callDele_EventOnMissionGet(obj);
      }

      public void __clearDele_EventOnMissionGet(BackFlowMissonUIController obj)
      {
        this.m_owner.__clearDele_EventOnMissionGet(obj);
      }

      public void __callDele_EventOnIntegralGoodsClick(BackFlowIntegralGoodsUIController obj)
      {
        this.m_owner.__callDele_EventOnIntegralGoodsClick(obj);
      }

      public void __clearDele_EventOnIntegralGoodsClick(BackFlowIntegralGoodsUIController obj)
      {
        this.m_owner.__clearDele_EventOnIntegralGoodsClick(obj);
      }

      public CommonUIStateController m_backFlowActivityStateCtrl
      {
        get
        {
          return this.m_owner.m_backFlowActivityStateCtrl;
        }
        set
        {
          this.m_owner.m_backFlowActivityStateCtrl = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_infoButton
      {
        get
        {
          return this.m_owner.m_infoButton;
        }
        set
        {
          this.m_owner.m_infoButton = value;
        }
      }

      public GameObject m_charGameObject
      {
        get
        {
          return this.m_owner.m_charGameObject;
        }
        set
        {
          this.m_owner.m_charGameObject = value;
        }
      }

      public GameObject m_daysButtonContent
      {
        get
        {
          return this.m_owner.m_daysButtonContent;
        }
        set
        {
          this.m_owner.m_daysButtonContent = value;
        }
      }

      public ScrollRect m_missionsScrollRect
      {
        get
        {
          return this.m_owner.m_missionsScrollRect;
        }
        set
        {
          this.m_owner.m_missionsScrollRect = value;
        }
      }

      public GameObject m_missionsScrollViewContent
      {
        get
        {
          return this.m_owner.m_missionsScrollViewContent;
        }
        set
        {
          this.m_owner.m_missionsScrollViewContent = value;
        }
      }

      public GameObject m_missonItemPrefab
      {
        get
        {
          return this.m_owner.m_missonItemPrefab;
        }
        set
        {
          this.m_owner.m_missonItemPrefab = value;
        }
      }

      public GameObject m_integralContentGroup
      {
        get
        {
          return this.m_owner.m_integralContentGroup;
        }
        set
        {
          this.m_owner.m_integralContentGroup = value;
        }
      }

      public Text m_integralGroupCountValueText
      {
        get
        {
          return this.m_owner.m_integralGroupCountValueText;
        }
        set
        {
          this.m_owner.m_integralGroupCountValueText = value;
        }
      }

      public Text m_residueTimeValueText
      {
        get
        {
          return this.m_owner.m_residueTimeValueText;
        }
        set
        {
          this.m_owner.m_residueTimeValueText = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_curSelectday
      {
        get
        {
          return this.m_owner.m_curSelectday;
        }
        set
        {
          this.m_owner.m_curSelectday = value;
        }
      }

      public HeroCharUIController m_heroCharUIController
      {
        get
        {
          return this.m_owner.m_heroCharUIController;
        }
        set
        {
          this.m_owner.m_heroCharUIController = value;
        }
      }

      public BackFlowDayButtonUIController m_curSelectDayButtonCtrl
      {
        get
        {
          return this.m_owner.m_curSelectDayButtonCtrl;
        }
        set
        {
          this.m_owner.m_curSelectDayButtonCtrl = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void InitDayButtons()
      {
        this.m_owner.InitDayButtons();
      }

      public void OnDayButtonClick(BackFlowDayButtonUIController ctrl)
      {
        this.m_owner.OnDayButtonClick(ctrl);
      }

      public void InitMissionPanel()
      {
        this.m_owner.InitMissionPanel();
      }

      public void OnMissionGotoButtonClick(GetPathData pathInfo)
      {
        this.m_owner.OnMissionGotoButtonClick(pathInfo);
      }

      public void OnMissionGetButtonClick(BackFlowMissonUIController ctrl)
      {
        this.m_owner.OnMissionGetButtonClick(ctrl);
      }

      public void InitIntegralPanel()
      {
        this.m_owner.InitIntegralPanel();
      }

      public void OnIntegralGoodsClick(BackFlowIntegralGoodsUIController ctrl)
      {
        this.m_owner.OnIntegralGoodsClick(ctrl);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnInfoButtonClick()
      {
        this.m_owner.OnInfoButtonClick();
      }

      public void ClearData()
      {
        this.m_owner.ClearData();
      }
    }
  }
}
