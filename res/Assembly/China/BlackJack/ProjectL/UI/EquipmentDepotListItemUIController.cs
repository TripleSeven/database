﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentDepotListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class EquipmentDepotListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_stateCtrl;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_icon;
    [AutoBind("./BgFrame", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_bgFrame;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_SSREffect;
    [AutoBind("./CheckImageMask", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_checkImageMask;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_starGroup;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_lvValueText;
    [AutoBind("./EquipingTag/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_equipingTagHeadIcon;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_lockImage;
    [AutoBind("./SelectFrame", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_selectFrame;
    [AutoBind("./Mask", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_mask;
    [AutoBind("./EnchantmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_enchantmentIcon;
    [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    public bool IsEquiped;
    public ulong EquipmentInstanceId;
    public BagItemBase BagItem;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEquipmentDepotListItemInfo(BagItemBase bagItem, bool canWear, bool isInDepot = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowCheckImage(bool isShow)
    {
      this.m_checkImageMask.SetActive(isShow);
    }

    public void ShowSelectFrame(bool isShow)
    {
      this.m_selectFrame.SetActive(isShow);
    }

    public void Init()
    {
      this.ScrollItemBaseUICtrl.Init((UIControllerBase) this, true);
    }

    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemClick = action;
    }

    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItemNeedFill = action;
    }

    public void RegisterItem3DTouchEvent(Action<UIControllerBase> action)
    {
      this.ScrollItemBaseUICtrl.EventOnUIItem3DTouch = action;
    }
  }
}
