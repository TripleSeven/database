﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogDetailChoiceUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public sealed class DialogDetailChoiceUIController : UIControllerBase
  {
    private DialogDetailChoiceItemUIController m_choiceItem;
    [AutoBind("./NameGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject NameGroupGameObject;

    public void SetDialogDetailChoice(DialogDetailChoiceItemUIController choiceItem)
    {
      this.m_choiceItem = choiceItem;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }
  }
}
