﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersConfessionSkillItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FettersConfessionSkillItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./SkillGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvValueText;
    [AutoBind("./SkillGroup/RecPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redPoint;
    [AutoBind("./BlackBG/UnlockConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockConditionText;
    [AutoBind("./SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillIconImage;
    [AutoBind("./SkillGroup/UnlockSelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlockSelectImage;
    [AutoBind("./LockSelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockSelectImage;
    private bool m_hasUnLocked;
    private int m_unlockFavoribilityLevel;
    private int m_curFavoribilityLevel;
    private int m_heroId;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_skillLv;
    [DoNotToLua]
    private FettersConfessionSkillItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitFettersConfessionSkillItemInt32Int32BooleanInt32_hotfix;
    private LuaFunction m_OnSkillToggleValueChangedBoolean_hotfix;
    private LuaFunction m_PlayUnlockEffectList`1_hotfix;
    private LuaFunction m_PlayPromoteEffect_hotfix;
    private LuaFunction m_ShowSelectImageBoolean_hotfix;
    private LuaFunction m_add_EventOnClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnClickAction`1_hotfix;
    private LuaFunction m_set_HeroFetterInfoConfigDataHeroFetterInfo_hotfix;
    private LuaFunction m_get_HeroFetterInfo_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFettersConfessionSkillItem(
      int fetterId,
      int curFavoribilityLevel,
      bool hasUnLock,
      int skillLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayUnlockEffect(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPromoteEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FettersConfessionSkillItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroFetterInfo HeroFetterInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FettersConfessionSkillItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(FettersConfessionSkillItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick(FettersConfessionSkillItemUIController obj)
    {
      this.EventOnClick = (Action<FettersConfessionSkillItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FettersConfessionSkillItemUIController m_owner;

      public LuaExportHelper(FettersConfessionSkillItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(FettersConfessionSkillItemUIController obj)
      {
        this.m_owner.__callDele_EventOnClick(obj);
      }

      public void __clearDele_EventOnClick(FettersConfessionSkillItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnClick(obj);
      }

      public ToggleEx m_toggle
      {
        get
        {
          return this.m_owner.m_toggle;
        }
        set
        {
          this.m_owner.m_toggle = value;
        }
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public Text m_lvValueText
      {
        get
        {
          return this.m_owner.m_lvValueText;
        }
        set
        {
          this.m_owner.m_lvValueText = value;
        }
      }

      public GameObject m_redPoint
      {
        get
        {
          return this.m_owner.m_redPoint;
        }
        set
        {
          this.m_owner.m_redPoint = value;
        }
      }

      public Text m_unlockConditionText
      {
        get
        {
          return this.m_owner.m_unlockConditionText;
        }
        set
        {
          this.m_owner.m_unlockConditionText = value;
        }
      }

      public Image m_skillIconImage
      {
        get
        {
          return this.m_owner.m_skillIconImage;
        }
        set
        {
          this.m_owner.m_skillIconImage = value;
        }
      }

      public GameObject m_unlockSelectImage
      {
        get
        {
          return this.m_owner.m_unlockSelectImage;
        }
        set
        {
          this.m_owner.m_unlockSelectImage = value;
        }
      }

      public GameObject m_lockSelectImage
      {
        get
        {
          return this.m_owner.m_lockSelectImage;
        }
        set
        {
          this.m_owner.m_lockSelectImage = value;
        }
      }

      public bool m_hasUnLocked
      {
        get
        {
          return this.m_owner.m_hasUnLocked;
        }
        set
        {
          this.m_owner.m_hasUnLocked = value;
        }
      }

      public int m_unlockFavoribilityLevel
      {
        get
        {
          return this.m_owner.m_unlockFavoribilityLevel;
        }
        set
        {
          this.m_owner.m_unlockFavoribilityLevel = value;
        }
      }

      public int m_curFavoribilityLevel
      {
        get
        {
          return this.m_owner.m_curFavoribilityLevel;
        }
        set
        {
          this.m_owner.m_curFavoribilityLevel = value;
        }
      }

      public int m_heroId
      {
        get
        {
          return this.m_owner.m_heroId;
        }
        set
        {
          this.m_owner.m_heroId = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public int m_skillLv
      {
        get
        {
          return this.m_owner.m_skillLv;
        }
        set
        {
          this.m_owner.m_skillLv = value;
        }
      }

      public ConfigDataHeroFetterInfo HeroFetterInfo
      {
        set
        {
          this.m_owner.HeroFetterInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnSkillToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSkillToggleValueChanged(isOn);
      }
    }
  }
}
