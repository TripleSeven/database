﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UserGuideUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class UserGuideUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Coroutine m_initCoroutine;
    private UserGuideUIController m_userGuideUIController;
    private ConfigDataUserGuideStep m_userGuideStepInfo;
    private const float m_waitForClickObjectTime = 0.2f;
    private const float m_maxWaitObjectTime = 20f;
    private ConfigDataBattleDialogInfo m_battleDialogInfo;
    private BattleDialogUITask m_battleDialogUITask;
    private UserGuideDialogUITask m_userGuideDialogUITask;
    private int m_pageIndex;
    private int m_nUserGuideId;
    private bool m_isDoingUpdateViewAsync;
    private List<GameObject> m_temporaryDisableObjects;
    private List<GameObject> m_temporaryDeactiveObjects;
    private bool m_isFinished;
    private bool m_isTemporaryDisableMoveBattleCamera;
    private bool m_isEnableSkip;
    private static Dictionary<int, int> m_triggerUserGuideCounts;
    private static Dictionary<int, ConfigDataUserGuide> m_userGuideConfigs;
    private static List<string[]> m_userGuideShowHideEventObjectPaths;
    private static bool m_isEnable;
    private static int m_dragHeroToBattleUserGuideID;

    [MethodImpl((MethodImplOptions) 32768)]
    public UserGuideUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddUserGuideTriggerCount(int userGuideId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetUserGuideTriggerCount(int userGuideId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SaveUserGuideProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LoadUserGuideProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    private static string UserGuideProgressFileName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsUserGuideBattle(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool NeedSkipBattlePrepareForUserGuide(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetEnforceHeros(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CollectUserGuideShowHideEventObjectPaths(List<string[]> paths)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsArrayEqual<T>(T[] arr1, T[] arr2, Comparison<T> compare)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnShowObject(string objectPath)
    {
      UserGuideUITask.Trigger(UserGuideTrigger.UserGuideTrigger_ShowObject, objectPath);
    }

    public static void OnHideObject(string objectPath)
    {
      UserGuideUITask.Trigger(UserGuideTrigger.UserGuideTrigger_HideObject, objectPath);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnUIStateEnd(GameObject obj, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnSelectBattleActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnWorldUIGetReady()
    {
      UserGuideUITask.Trigger(UserGuideTrigger.UserGuideTrigger_WorldUIGetReady, string.Empty);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnServerFinishUserGuide(int guideID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnDeselectBattleActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnGiftStoreGoodsBuy(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnUITaskShow(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void OnReturnToLoginUI(bool obj)
    {
      UserGuideUITask.m_triggerUserGuideCounts = (Dictionary<int, int>) null;
    }

    public static void OnPrefabAwake(GameObject awakeObj)
    {
      UserGuideUITask.AddShowHideEventForUserGuide(awakeObj);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddShowHideEventForUserGuide(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsUserGuideTriggerObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TriggerUserGuide(UserGuideTrigger trigger, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Trigger(UserGuideTrigger trigger, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool MatchTrigger(
      UserGuideTrigger trigger1,
      string param1,
      UserGuideTrigger trigger2,
      string param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckCondition(UserGuideCondition c, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartStep(int userGuideId, int userGuideStepId, bool bSkipEnable = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ForceStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitUserGuideUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitUserGuideUIController();
    }

    protected override void UpdateView()
    {
      this.DoUpdateView();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddShowHideEventForUserGuideStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator EnableInputLately(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetPostProcessFuncForFunctionOpenObj(
      ConfigDataUserGuideStep userGuideStepInfo,
      out Action<GameObject> postProcessFunc)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DoUpdateViewAsync()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFunctionOpenAnimEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool GetCenterScreenPosition(RectTransform rt, ref Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsInsideScreen(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsClickable(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsChildTransform(Transform parent, Transform child)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrepareForGetUIPosition(GameObject uiObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickObject(string[] objPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Click(GameObject o)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ArrayToPathString(string[] path)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DoAction(UserGuideAction action, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableBattleCameraTouchMove(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FinishCurrentUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitForObjectReady(string objPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ScrollToItem(string scrollObjPath, string itemObjPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableObject(GameObject obj, bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickBattleGrid(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCurrentPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitUserGuideUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitUserGuideUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleDialogUITask(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator StartUserGuideDialogUITask(
      ConfigDataUserGuideDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNextButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Finish(bool isClick = true, bool bForce = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Next()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideUIController_OnNextPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideUIController_OnPrevPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnUserGuideClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideDialogUITask_OnUserGuideClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    public static bool Enable
    {
      get
      {
        return UserGuideUITask.m_isEnable;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static UserGuideUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
