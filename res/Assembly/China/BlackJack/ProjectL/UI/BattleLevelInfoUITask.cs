﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleLevelInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleLevelInfoUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private BattleLevelInfoUIController m_battleLevelInfoUIController;
    private RaidLevelUITask m_raidLevelUITask;
    private ConfigDataRiftLevelInfo m_riftLevelInfo;
    private ConfigDataHeroDungeonLevelInfo m_heroDungeonLevelInfo;
    private BattleType m_battleType;
    private NeedGoods m_needGoods;
    private bool m_needOpenTween;
    [DoNotToLua]
    private BattleLevelInfoUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_InitDataFromUIIntentUIIntent_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateRiftLevelInfoConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_UpdateHeroDungeonLevelInfoConfigDataHeroDungeonLevelInfo_hotfix;
    private LuaFunction m_StartRiftRaidLevelUITaskInt32BattleRewardList`1NeedGoods_hotfix;
    private LuaFunction m_StartHeroDungeonLevelRaidUITaskInt32BattleRewardList`1_hotfix;
    private LuaFunction m_RaidUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartRiftLevelRaidNetTaskInt32Int32_hotfix;
    private LuaFunction m_StartHeroDungeonLevelRaidNetTaskInt32Int32_hotfix;
    private LuaFunction m_BattleLevelInfoUIController_OnStartBattle_hotfix;
    private LuaFunction m_BattleLevelInfoUIController_OnRaidBattleInt32_hotfix;
    private LuaFunction m_BattleLevelInfoUIController_OnShowAchievement_hotfix;
    private LuaFunction m_BattleLevelInfoUIController_OnClose_hotfix;
    private LuaFunction m_RaidLevelUITask_OnRiftRaidClose_hotfix;
    private LuaFunction m_RaidLevelUITask_OnHeroDungeonRaidClose_hotfix;
    private LuaFunction m_RaidLevelUITask_OnRiftRaidCompleteInt32_hotfix;
    private LuaFunction m_RaidLevelUITask_OnHeroDungeonRaidCompleteInt32_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnRiftRaidCompleteAction`1_hotfix;
    private LuaFunction m_remove_EventOnRiftRaidCompleteAction`1_hotfix;
    private LuaFunction m_add_EventOnHeroDungeonRaidCompleteAction`1_hotfix;
    private LuaFunction m_remove_EventOnHeroDungeonRaidCompleteAction`1_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleLevelInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRiftLevelInfo(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroDungeonLevelInfo(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftRaidLevelUITask(
      int riftLevelID,
      BattleReward reward,
      List<Goods> extraReward,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelRaidUITask(
      int heroDungeonLevelID,
      BattleReward reward,
      List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftLevelRaidNetTask(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelRaidNetTask(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnRaidBattle(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnShowAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnRiftRaidClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnHeroDungeonRaidClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnRiftRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnHeroDungeonRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRiftRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroDungeonRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleLevelInfoUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRiftRaidComplete(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRiftRaidComplete(int obj)
    {
      this.EventOnRiftRaidComplete = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroDungeonRaidComplete(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroDungeonRaidComplete(int obj)
    {
      this.EventOnHeroDungeonRaidComplete = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleLevelInfoUITask m_owner;

      public LuaExportHelper(BattleLevelInfoUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnRiftRaidComplete(int obj)
      {
        this.m_owner.__callDele_EventOnRiftRaidComplete(obj);
      }

      public void __clearDele_EventOnRiftRaidComplete(int obj)
      {
        this.m_owner.__clearDele_EventOnRiftRaidComplete(obj);
      }

      public void __callDele_EventOnHeroDungeonRaidComplete(int obj)
      {
        this.m_owner.__callDele_EventOnHeroDungeonRaidComplete(obj);
      }

      public void __clearDele_EventOnHeroDungeonRaidComplete(int obj)
      {
        this.m_owner.__clearDele_EventOnHeroDungeonRaidComplete(obj);
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public BattleLevelInfoUIController m_battleLevelInfoUIController
      {
        get
        {
          return this.m_owner.m_battleLevelInfoUIController;
        }
        set
        {
          this.m_owner.m_battleLevelInfoUIController = value;
        }
      }

      public RaidLevelUITask m_raidLevelUITask
      {
        get
        {
          return this.m_owner.m_raidLevelUITask;
        }
        set
        {
          this.m_owner.m_raidLevelUITask = value;
        }
      }

      public ConfigDataRiftLevelInfo m_riftLevelInfo
      {
        get
        {
          return this.m_owner.m_riftLevelInfo;
        }
        set
        {
          this.m_owner.m_riftLevelInfo = value;
        }
      }

      public ConfigDataHeroDungeonLevelInfo m_heroDungeonLevelInfo
      {
        get
        {
          return this.m_owner.m_heroDungeonLevelInfo;
        }
        set
        {
          this.m_owner.m_heroDungeonLevelInfo = value;
        }
      }

      public BattleType m_battleType
      {
        get
        {
          return this.m_owner.m_battleType;
        }
        set
        {
          this.m_owner.m_battleType = value;
        }
      }

      public NeedGoods m_needGoods
      {
        get
        {
          return this.m_owner.m_needGoods;
        }
        set
        {
          this.m_owner.m_needGoods = value;
        }
      }

      public bool m_needOpenTween
      {
        get
        {
          return this.m_owner.m_needOpenTween;
        }
        set
        {
          this.m_owner.m_needOpenTween = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void InitDataFromUIIntent(UIIntent intent)
      {
        this.m_owner.InitDataFromUIIntent(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void UpdateRiftLevelInfo(ConfigDataRiftLevelInfo levelInfo)
      {
        this.m_owner.UpdateRiftLevelInfo(levelInfo);
      }

      public void UpdateHeroDungeonLevelInfo(ConfigDataHeroDungeonLevelInfo levelInfo)
      {
        this.m_owner.UpdateHeroDungeonLevelInfo(levelInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartRiftRaidLevelUITask(
        int riftLevelID,
        BattleReward reward,
        List<Goods> extraReward,
        NeedGoods needGoods)
      {
        // ISSUE: unable to decompile the method.
      }

      public void StartHeroDungeonLevelRaidUITask(
        int heroDungeonLevelID,
        BattleReward reward,
        List<Goods> extraReward)
      {
        this.m_owner.StartHeroDungeonLevelRaidUITask(heroDungeonLevelID, reward, extraReward);
      }

      public void RaidUITask_OnLoadAllResCompleted()
      {
        this.m_owner.RaidUITask_OnLoadAllResCompleted();
      }

      public void StartRiftLevelRaidNetTask(int levelId, int count)
      {
        this.m_owner.StartRiftLevelRaidNetTask(levelId, count);
      }

      public void StartHeroDungeonLevelRaidNetTask(int levelId, int count)
      {
        this.m_owner.StartHeroDungeonLevelRaidNetTask(levelId, count);
      }

      public void BattleLevelInfoUIController_OnStartBattle()
      {
        this.m_owner.BattleLevelInfoUIController_OnStartBattle();
      }

      public void BattleLevelInfoUIController_OnRaidBattle(int count)
      {
        this.m_owner.BattleLevelInfoUIController_OnRaidBattle(count);
      }

      public void BattleLevelInfoUIController_OnShowAchievement()
      {
        this.m_owner.BattleLevelInfoUIController_OnShowAchievement();
      }

      public void BattleLevelInfoUIController_OnClose()
      {
        this.m_owner.BattleLevelInfoUIController_OnClose();
      }

      public void RaidLevelUITask_OnRiftRaidClose()
      {
        this.m_owner.RaidLevelUITask_OnRiftRaidClose();
      }

      public void RaidLevelUITask_OnHeroDungeonRaidClose()
      {
        this.m_owner.RaidLevelUITask_OnHeroDungeonRaidClose();
      }

      public void RaidLevelUITask_OnRiftRaidComplete(int levelId)
      {
        this.m_owner.RaidLevelUITask_OnRiftRaidComplete(levelId);
      }

      public void RaidLevelUITask_OnHeroDungeonRaidComplete(int levelId)
      {
        this.m_owner.RaidLevelUITask_OnHeroDungeonRaidComplete(levelId);
      }
    }
  }
}
