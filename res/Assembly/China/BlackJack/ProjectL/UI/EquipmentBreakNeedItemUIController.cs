﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentBreakNeedItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class EquipmentBreakNeedItemUIController : UIControllerBase
  {
    [AutoBind("./IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconBg;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_iconSSREffect;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveCount;
    [AutoBind("./ValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needCount;
    [AutoBind("./ValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_countStateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    public Goods m_goods;
    [DoNotToLua]
    private EquipmentBreakNeedItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitEquipmentBreakNeedItemGoods_hotfix;
    private LuaFunction m_OnClick_hotfix;
    private LuaFunction m_add_EventOnClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnClickAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEquipmentBreakNeedItem(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GoodsType, int, int> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public EquipmentBreakNeedItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(GoodsType arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick(GoodsType arg1, int arg2, int arg3)
    {
      this.EventOnClick = (Action<GoodsType, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private EquipmentBreakNeedItemUIController m_owner;

      public LuaExportHelper(EquipmentBreakNeedItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnClick(GoodsType arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnClick(arg1, arg2, arg3);
      }

      public Image m_iconBg
      {
        get
        {
          return this.m_owner.m_iconBg;
        }
        set
        {
          this.m_owner.m_iconBg = value;
        }
      }

      public GameObject m_iconSSREffect
      {
        get
        {
          return this.m_owner.m_iconSSREffect;
        }
        set
        {
          this.m_owner.m_iconSSREffect = value;
        }
      }

      public Image m_icon
      {
        get
        {
          return this.m_owner.m_icon;
        }
        set
        {
          this.m_owner.m_icon = value;
        }
      }

      public Text m_haveCount
      {
        get
        {
          return this.m_owner.m_haveCount;
        }
        set
        {
          this.m_owner.m_haveCount = value;
        }
      }

      public Text m_needCount
      {
        get
        {
          return this.m_owner.m_needCount;
        }
        set
        {
          this.m_owner.m_needCount = value;
        }
      }

      public CommonUIStateController m_countStateCtrl
      {
        get
        {
          return this.m_owner.m_countStateCtrl;
        }
        set
        {
          this.m_owner.m_countStateCtrl = value;
        }
      }

      public Button m_button
      {
        get
        {
          return this.m_owner.m_button;
        }
        set
        {
          this.m_owner.m_button = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnClick()
      {
        this.m_owner.OnClick();
      }
    }
  }
}
