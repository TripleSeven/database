﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaPlayOffInfoGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PeakArenaPlayOffInfoGetNetTask : UINetTask
  {
    private int m_sequenceId;
    private int m_season;
    public long m_version;
    public static PeakArenaPlayOffInfoGetNetTask m_curRunningTask;
    private ProjectLPlayerContext m_playerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffInfoGetNetTask(int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPeakArenaPlayOffInfoGetAck(
      int result,
      int sequenceId,
      int nextMatchIndex,
      long version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void GetUserSummary()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool SendRequest()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FinishTransaction(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
