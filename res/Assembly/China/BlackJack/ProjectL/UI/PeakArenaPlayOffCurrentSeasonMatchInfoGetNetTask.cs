﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.PlayerContext;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class PeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask : UINetTask
  {
    private int m_matchupId;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck(
      int result,
      PeakArenaPlayOffMatchupInfo matchupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public PeakArenaPlayOffMatchupInfo MatchupInfo { protected set; get; }
  }
}
