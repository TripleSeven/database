﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ExpressionParseController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ExpressionParseController : MonoBehaviour
  {
    private Image m_imageToClone;
    private string m_regexParamStr;
    private float m_offsetX;
    private float m_offsetY;
    private float m_fontSize;
    private int m_richTextIndexOffSet;
    private float m_gapSize;
    private string emSpace;
    private bool m_isInited;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public ExpressionParseController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject image, int gap, float offsetX = 0.0f, float offsetY = 0.0f, float fontSize = 25f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChatItemWithTextAndSmallExpression(Text text, string inputString)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SetUITextWithSmallExpression(Text textToEdit, string inputString)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ExpressionParseController.PosStringTuple> ParseEmoji(
      string inputString,
      out string changedStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class PosStringTuple
    {
      public int pos;
      public int expressionKey;

      [MethodImpl((MethodImplOptions) 32768)]
      public PosStringTuple(int p, int key)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
