﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroPhantomLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroPhantomLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./EnergyValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Locked", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockedButton;
    [AutoBind("./AchievementButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementButton;
    [AutoBind("./AchievementCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementCount;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGroupGameObject;
    [AutoBind("./FirstClean", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_firstCleanUIStateController;
    [AutoBind("./FirstClean/RewardDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstRewardGroupGameObject;
    private ConfigDataHeroPhantomLevelInfo m_heroPhantomLevelInfo;
    [DoNotToLua]
    private HeroPhantomLevelListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetHeroPhantomLevelInfoConfigDataHeroPhantomLevelInfo_hotfix;
    private LuaFunction m_GetHeroPhantomLevelInfo_hotfix;
    private LuaFunction m_SetLockedBoolean_hotfix;
    private LuaFunction m_OnStartButtonClick_hotfix;
    private LuaFunction m_OnAchievementButtonClick_hotfix;
    private LuaFunction m_OnLockedButtonClick_hotfix;
    private LuaFunction m_add_EventOnStartButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnAchievementButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnAchievementButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroPhantomLevelInfo(ConfigDataHeroPhantomLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomLevelInfo GetHeroPhantomLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool locked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockedButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroPhantomLevelListItemUIController> EventOnStartButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroPhantomLevelListItemUIController> EventOnAchievementButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroPhantomLevelListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartButtonClick(HeroPhantomLevelListItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStartButtonClick(HeroPhantomLevelListItemUIController obj)
    {
      this.EventOnStartButtonClick = (Action<HeroPhantomLevelListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAchievementButtonClick(HeroPhantomLevelListItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAchievementButtonClick(HeroPhantomLevelListItemUIController obj)
    {
      this.EventOnAchievementButtonClick = (Action<HeroPhantomLevelListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroPhantomLevelListItemUIController m_owner;

      public LuaExportHelper(HeroPhantomLevelListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnStartButtonClick(HeroPhantomLevelListItemUIController obj)
      {
        this.m_owner.__callDele_EventOnStartButtonClick(obj);
      }

      public void __clearDele_EventOnStartButtonClick(HeroPhantomLevelListItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnStartButtonClick(obj);
      }

      public void __callDele_EventOnAchievementButtonClick(HeroPhantomLevelListItemUIController obj)
      {
        this.m_owner.__callDele_EventOnAchievementButtonClick(obj);
      }

      public void __clearDele_EventOnAchievementButtonClick(HeroPhantomLevelListItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnAchievementButtonClick(obj);
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Text m_energyText
      {
        get
        {
          return this.m_owner.m_energyText;
        }
        set
        {
          this.m_owner.m_energyText = value;
        }
      }

      public Button m_lockedButton
      {
        get
        {
          return this.m_owner.m_lockedButton;
        }
        set
        {
          this.m_owner.m_lockedButton = value;
        }
      }

      public Button m_achievementButton
      {
        get
        {
          return this.m_owner.m_achievementButton;
        }
        set
        {
          this.m_owner.m_achievementButton = value;
        }
      }

      public Text m_achievementCount
      {
        get
        {
          return this.m_owner.m_achievementCount;
        }
        set
        {
          this.m_owner.m_achievementCount = value;
        }
      }

      public Button m_startButton
      {
        get
        {
          return this.m_owner.m_startButton;
        }
        set
        {
          this.m_owner.m_startButton = value;
        }
      }

      public GameObject m_rewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_rewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_rewardGroupGameObject = value;
        }
      }

      public CommonUIStateController m_firstCleanUIStateController
      {
        get
        {
          return this.m_owner.m_firstCleanUIStateController;
        }
        set
        {
          this.m_owner.m_firstCleanUIStateController = value;
        }
      }

      public GameObject m_firstRewardGroupGameObject
      {
        get
        {
          return this.m_owner.m_firstRewardGroupGameObject;
        }
        set
        {
          this.m_owner.m_firstRewardGroupGameObject = value;
        }
      }

      public ConfigDataHeroPhantomLevelInfo m_heroPhantomLevelInfo
      {
        get
        {
          return this.m_owner.m_heroPhantomLevelInfo;
        }
        set
        {
          this.m_owner.m_heroPhantomLevelInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnStartButtonClick()
      {
        this.m_owner.OnStartButtonClick();
      }

      public void OnAchievementButtonClick()
      {
        this.m_owner.OnAchievementButtonClick();
      }

      public void OnLockedButtonClick()
      {
        this.m_owner.OnLockedButtonClick();
      }
    }
  }
}
