﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedBoxUseNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class SelfSelectedBoxUseNetTask : UINetTask
  {
    private GoodsType m_goodsType;
    private int m_itemId;
    private int m_count;
    private int m_index;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedBoxUseNetTask(GoodsType goodsType, int itemId, int count, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSelfSelectedBoxUseAck(int result, List<Goods> reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public GoodsType GoodsType
    {
      get
      {
        return this.m_goodsType;
      }
    }

    public int ItemID
    {
      get
      {
        return this.m_itemId;
      }
    }

    public int Count
    {
      get
      {
        return this.m_count;
      }
    }

    public List<Goods> Reward { private set; get; }
  }
}
