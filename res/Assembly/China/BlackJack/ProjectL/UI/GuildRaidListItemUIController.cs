﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildRaidListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GuildRaidListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./EnergyValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_countText;
    [AutoBind("./ModoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_difficultTextStateCtrl;
    [AutoBind("./ConditionDescScrollView/Viewport/Content/ConditionDescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_conditionDescText;
    [AutoBind("./Locked/TextGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockedText;
    public ConfigDataGuildMassiveCombatDifficultyInfo GuildMassiveCombatDifficultyInfo;
    public int m_difficultLv;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGuildRaidListItemInfo(ConfigDataGuildMassiveCombatDifficultyInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GuildRaidListItemUIController> EventOnStartButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
