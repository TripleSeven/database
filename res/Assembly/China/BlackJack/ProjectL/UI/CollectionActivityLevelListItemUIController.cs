﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CollectionActivityLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./TypeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_levelTypeUIStateController;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./TypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_typeText;
    [AutoBind("./Level", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelGameObject;
    [AutoBind("./Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Energy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./RewardGoodsList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsListGameObject;
    [AutoBind("./Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockButton;
    [AutoBind("./Lock/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockText;
    private ConfigDataCollectionActivityScenarioLevelInfo m_scenarioLevelInfo;
    private ConfigDataCollectionActivityChallengeLevelInfo m_challengeLevelInfo;
    private ConfigDataCollectionActivityLootLevelInfo m_lootLevelInfo;
    [DoNotToLua]
    private CollectionActivityLevelListItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_AppendScoreItemToListConfigDataCollectionActivityInfoInt32List`1_hotfix;
    private LuaFunction m_SetLevelInfoConfigDataCollectionActivityScenarioLevelInfo_hotfix;
    private LuaFunction m_SetLevelInfoConfigDataCollectionActivityChallengeLevelInfo_hotfix;
    private LuaFunction m_SetLevelInfoConfigDataCollectionActivityLootLevelInfoBoolean_hotfix;
    private LuaFunction m_SetLevelTypeStringTableIdString_hotfix;
    private LuaFunction m_SetLevelTextStringInt32Int32Boolean_hotfix;
    private LuaFunction m_SetRewordGoodsList`1Int32_hotfix;
    private LuaFunction m_SetLockedBoolean_hotfix;
    private LuaFunction m_SetUnlocked_hotfix;
    private LuaFunction m_SetOpenDayLockedInt32_hotfix;
    private LuaFunction m_SetPreLevelLocked_hotfix;
    private LuaFunction m_SetPlayerLevelLockedInt32_hotfix;
    private LuaFunction m_SetFinished_hotfix;
    private LuaFunction m_GetScenarioLevelInfo_hotfix;
    private LuaFunction m_GetChallengeLevelInfo_hotfix;
    private LuaFunction m_GetLootLevelInfo_hotfix;
    private LuaFunction m_OnStartButtonClick_hotfix;
    private LuaFunction m_OnLockButtonClick_hotfix;
    private LuaFunction m_add_EventOnStartAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendScoreItemToList(
      ConfigDataCollectionActivityInfo collectionActivityInfo,
      int score,
      List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelInfo(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelInfo(
      ConfigDataCollectionActivityChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelInfo(
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      bool isFirst)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevelType(StringTableId typeName, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevelText(string name, int monsterLevel, int energy, bool showLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewordGoods(List<Goods> rewardList, int displayCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLocked(bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnlocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpenDayLocked(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPreLevelLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevelLocked(int playerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetScenarioLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityChallengeLevelInfo GetChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityLootLevelInfo GetLootLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<CollectionActivityLevelListItemUIController> EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityLevelListItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStart(CollectionActivityLevelListItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStart(CollectionActivityLevelListItemUIController obj)
    {
      this.EventOnStart = (Action<CollectionActivityLevelListItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityLevelListItemUIController m_owner;

      public LuaExportHelper(CollectionActivityLevelListItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnStart(CollectionActivityLevelListItemUIController obj)
      {
        this.m_owner.__callDele_EventOnStart(obj);
      }

      public void __clearDele_EventOnStart(CollectionActivityLevelListItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnStart(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public CommonUIStateController m_levelTypeUIStateController
      {
        get
        {
          return this.m_owner.m_levelTypeUIStateController;
        }
        set
        {
          this.m_owner.m_levelTypeUIStateController = value;
        }
      }

      public Button m_startButton
      {
        get
        {
          return this.m_owner.m_startButton;
        }
        set
        {
          this.m_owner.m_startButton = value;
        }
      }

      public Text m_nameText
      {
        get
        {
          return this.m_owner.m_nameText;
        }
        set
        {
          this.m_owner.m_nameText = value;
        }
      }

      public Text m_typeText
      {
        get
        {
          return this.m_owner.m_typeText;
        }
        set
        {
          this.m_owner.m_typeText = value;
        }
      }

      public GameObject m_levelGameObject
      {
        get
        {
          return this.m_owner.m_levelGameObject;
        }
        set
        {
          this.m_owner.m_levelGameObject = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public Text m_energyText
      {
        get
        {
          return this.m_owner.m_energyText;
        }
        set
        {
          this.m_owner.m_energyText = value;
        }
      }

      public GameObject m_rewardGoodsListGameObject
      {
        get
        {
          return this.m_owner.m_rewardGoodsListGameObject;
        }
        set
        {
          this.m_owner.m_rewardGoodsListGameObject = value;
        }
      }

      public Button m_lockButton
      {
        get
        {
          return this.m_owner.m_lockButton;
        }
        set
        {
          this.m_owner.m_lockButton = value;
        }
      }

      public Text m_lockText
      {
        get
        {
          return this.m_owner.m_lockText;
        }
        set
        {
          this.m_owner.m_lockText = value;
        }
      }

      public ConfigDataCollectionActivityScenarioLevelInfo m_scenarioLevelInfo
      {
        get
        {
          return this.m_owner.m_scenarioLevelInfo;
        }
        set
        {
          this.m_owner.m_scenarioLevelInfo = value;
        }
      }

      public ConfigDataCollectionActivityChallengeLevelInfo m_challengeLevelInfo
      {
        get
        {
          return this.m_owner.m_challengeLevelInfo;
        }
        set
        {
          this.m_owner.m_challengeLevelInfo = value;
        }
      }

      public ConfigDataCollectionActivityLootLevelInfo m_lootLevelInfo
      {
        get
        {
          return this.m_owner.m_lootLevelInfo;
        }
        set
        {
          this.m_owner.m_lootLevelInfo = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void AppendScoreItemToList(
        ConfigDataCollectionActivityInfo collectionActivityInfo,
        int score,
        List<Goods> goodsList)
      {
        this.m_owner.AppendScoreItemToList(collectionActivityInfo, score, goodsList);
      }

      public void SetLevelType(StringTableId typeName, string stateName)
      {
        this.m_owner.SetLevelType(typeName, stateName);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetLevelText(string name, int monsterLevel, int energy, bool showLevel)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetRewordGoods(List<Goods> rewardList, int displayCount)
      {
        this.m_owner.SetRewordGoods(rewardList, displayCount);
      }

      public void SetLocked(bool isLocked)
      {
        this.m_owner.SetLocked(isLocked);
      }

      public void OnStartButtonClick()
      {
        this.m_owner.OnStartButtonClick();
      }

      public void OnLockButtonClick()
      {
        this.m_owner.OnLockButtonClick();
      }
    }
  }
}
