﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoryUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public sealed class StoryUIController : UIControllerBase
  {
    private StoryUITask.StateType m_stateType;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_settingStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_panelBGButton;
    [AutoBind("./SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx SkipButton;
    [AutoBind("./Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text StoryText;
    [AutoBind("./BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image StoryImage;
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image SpaceImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController StoryUIPrefabCommonUIStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button BGButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStory(ConfigDataStoryOutlineInfo info, StoryUITask.StateType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PanelShow(StoryUITask.StateType stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GetTweenPosToVector3(float height)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float GetTweenPosduration(float height, bool isSpeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NormalPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void QuickPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private void SkipDialogDialogBoxCallback(DialogBoxResult r)
    {
      if (r != DialogBoxResult.Ok)
        return;
      this.StateClose();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
