﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityExchangeMissionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class CollectionActivityExchangeMissionItemUIController : UIControllerBase
  {
    [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemDummy;
    [AutoBind("./ConsumeItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_consumeItemGroup;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupStateCtrl;
    [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./ButtonGroup/GetButton/TimesGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_curCountText;
    [AutoBind("./ButtonGroup/GetButton/TimesGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxCountText;
    private List<CollectionAcitivityExchangeConsumeItemUIController> m_itemUICtrlList;
    private RewardGoodsUIController goodsCtrl;
    [DoNotToLua]
    private CollectionActivityExchangeMissionItemUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_UpdateItemCollectionActivityMission_hotfix;
    private LuaFunction m_HandleBeforeReturnToPool_hotfix;
    private LuaFunction m_OnGetButtonClick_hotfix;
    private LuaFunction m_PlayGetRewardEffectActionBoolean_hotfix;
    private LuaFunction m_add_EventOnGetFromConsumeItemPoolFunc`1_hotfix;
    private LuaFunction m_remove_EventOnGetFromConsumeItemPoolFunc`1_hotfix;
    private LuaFunction m_add_EventOnReturnFromConsumeItemPoolAction`1_hotfix;
    private LuaFunction m_remove_EventOnReturnFromConsumeItemPoolAction`1_hotfix;
    private LuaFunction m_add_EventOnGetButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetButtonClickAction`1_hotfix;
    private LuaFunction m_set_MissionCollectionActivityMission_hotfix;
    private LuaFunction m_get_Mission_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeMissionItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(CollectionActivityMission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HandleBeforeReturnToPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayGetRewardEffect(Action OnStateFinish, bool isMax)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Func<CollectionAcitivityExchangeConsumeItemUIController> EventOnGetFromConsumeItemPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionAcitivityExchangeConsumeItemUIController> EventOnReturnFromConsumeItemPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionActivityExchangeMissionItemUIController> EventOnGetButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CollectionActivityMission Mission
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityExchangeMissionItemUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionAcitivityExchangeConsumeItemUIController __callDele_EventOnGetFromConsumeItemPool()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetFromConsumeItemPool()
    {
      this.EventOnGetFromConsumeItemPool = (Func<CollectionAcitivityExchangeConsumeItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturnFromConsumeItemPool(
      CollectionAcitivityExchangeConsumeItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturnFromConsumeItemPool(
      CollectionAcitivityExchangeConsumeItemUIController obj)
    {
      this.EventOnReturnFromConsumeItemPool = (Action<CollectionAcitivityExchangeConsumeItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetButtonClick(
      CollectionActivityExchangeMissionItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetButtonClick(
      CollectionActivityExchangeMissionItemUIController obj)
    {
      this.EventOnGetButtonClick = (Action<CollectionActivityExchangeMissionItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityExchangeMissionItemUIController m_owner;

      public LuaExportHelper(
        CollectionActivityExchangeMissionItemUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public CollectionAcitivityExchangeConsumeItemUIController __callDele_EventOnGetFromConsumeItemPool()
      {
        // ISSUE: unable to decompile the method.
      }

      public void __clearDele_EventOnGetFromConsumeItemPool()
      {
        this.m_owner.__clearDele_EventOnGetFromConsumeItemPool();
      }

      public void __callDele_EventOnReturnFromConsumeItemPool(
        CollectionAcitivityExchangeConsumeItemUIController obj)
      {
        this.m_owner.__callDele_EventOnReturnFromConsumeItemPool(obj);
      }

      public void __clearDele_EventOnReturnFromConsumeItemPool(
        CollectionAcitivityExchangeConsumeItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnReturnFromConsumeItemPool(obj);
      }

      public void __callDele_EventOnGetButtonClick(
        CollectionActivityExchangeMissionItemUIController obj)
      {
        this.m_owner.__callDele_EventOnGetButtonClick(obj);
      }

      public void __clearDele_EventOnGetButtonClick(
        CollectionActivityExchangeMissionItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnGetButtonClick(obj);
      }

      public GameObject m_itemDummy
      {
        get
        {
          return this.m_owner.m_itemDummy;
        }
        set
        {
          this.m_owner.m_itemDummy = value;
        }
      }

      public GameObject m_consumeItemGroup
      {
        get
        {
          return this.m_owner.m_consumeItemGroup;
        }
        set
        {
          this.m_owner.m_consumeItemGroup = value;
        }
      }

      public CommonUIStateController m_buttonGroupStateCtrl
      {
        get
        {
          return this.m_owner.m_buttonGroupStateCtrl;
        }
        set
        {
          this.m_owner.m_buttonGroupStateCtrl = value;
        }
      }

      public Button m_getButton
      {
        get
        {
          return this.m_owner.m_getButton;
        }
        set
        {
          this.m_owner.m_getButton = value;
        }
      }

      public Text m_curCountText
      {
        get
        {
          return this.m_owner.m_curCountText;
        }
        set
        {
          this.m_owner.m_curCountText = value;
        }
      }

      public Text m_maxCountText
      {
        get
        {
          return this.m_owner.m_maxCountText;
        }
        set
        {
          this.m_owner.m_maxCountText = value;
        }
      }

      public List<CollectionAcitivityExchangeConsumeItemUIController> m_itemUICtrlList
      {
        get
        {
          return this.m_owner.m_itemUICtrlList;
        }
        set
        {
          this.m_owner.m_itemUICtrlList = value;
        }
      }

      public RewardGoodsUIController goodsCtrl
      {
        get
        {
          return this.m_owner.goodsCtrl;
        }
        set
        {
          this.m_owner.goodsCtrl = value;
        }
      }

      public CollectionActivityMission Mission
      {
        set
        {
          this.m_owner.Mission = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnGetButtonClick()
      {
        this.m_owner.OnGetButtonClick();
      }
    }
  }
}
