﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GmCommandBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public abstract class GmCommandBase
  {
    protected string m_type;
    protected string m_param;
    protected string[] m_paramArray;
    protected int m_result;
    protected ProjectLPlayerContext m_playerContext;
    protected IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    protected GmCommandBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(string type, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void Enter()
    {
    }

    protected virtual void Excute()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void Exit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Run()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
