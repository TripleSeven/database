﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class StoreListUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_storeUIStateCtrl;
    [AutoBind("./StoreUIPanel/RefreshPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_refreshPanelUIStateCtrl;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewStoreContent;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewHeroContent;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemBuyPanelUIStateController;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/Item/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemCountBgGo;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/GoodCount", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodCountObj;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemHaveCountText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/DescPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemPriceIcon;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPriceIconEffectGameObject;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel/Group/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemPriceText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyPanelCloseButton;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyButton;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_resonanceInfoPanel;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanelNameText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel/SuitInfo/2SuitInfo/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel2SuitInfoText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel/SuitInfo/4SuitInfo/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel4SuitInfoText;
    [AutoBind("./StoreUIPanel/PackagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_boxBuyPanelUIStateController;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_boxIconImage;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxNameText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ExplainInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxDescGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ExplainInfoPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxDescText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_boxDescUIStateController;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ItemPanel/ItemGroup/ItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxListScrollViewContent;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button/IconImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_boxPriceIcon;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button/IconImage/Image/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxPriceIconEffectGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxPriceText;
    [AutoBind("./StoreUIPanel/PackagePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_boxBuyPanelCloseButton;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_boxBuyButton;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxTimeLimitGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/TimeLimit/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxTimeLimitText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxBuyTimesGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/BuyTimes/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxBuyTimesText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/ValueOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxValueOffGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/ValueOffImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxValueOffText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxCountText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxHaveCountText;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefab/StoreBoxItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeBoxItemPrefab;
    [AutoBind("./Prefab/StoreItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeItemPrefab;
    [AutoBind("./Prefab/HeroSkinItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeHeroSkinItemPrefab;
    [AutoBind("./Prefab/StoreItemSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeSoldierSkinItemPrefab;
    [AutoBind("./StoreUIPanel/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Recharge", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rechargeToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Present", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_presentToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_crystalToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Skin", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Mysterious", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousToggle;
    [AutoBind("./StoreUIPanel/StorePackagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selfChoosePanel;
    [AutoBind("./StoreUIPanel/RefreshPanel/RefreshText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshTimeText;
    [AutoBind("./StoreUIPanel/RefreshPanel/PricePanel/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_refreshCurrencyTypeIcon;
    [AutoBind("./StoreUIPanel/RefreshPanel/PricePanel/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_refreshCurrencyTypeIconEffectGameObject;
    [AutoBind("./StoreUIPanel/RefreshPanel/PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshPriceText;
    [AutoBind("./StoreUIPanel/RefreshPanel/RefreshButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refreshButton;
    [AutoBind("./StoreUIPanel/RefreshPanel/TimesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshTimesText;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierSkinToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabHero", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroSkinToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/BlackMarket", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousBlackMarketToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Honor", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousHonorToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Medal", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousMedalToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Friendship", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousFriendshipToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Union", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousUnionToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Present", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_presentPresentToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Prerogative", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_presentPreogativeToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabHero", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinHeroToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinSoldierToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Memory", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousMemoryToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Equipment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousEquipmentToggle;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency1Obj;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency1Icon;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency1CountText;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1AddButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1DescriptionButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency2Obj;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency2Icon;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency2CountText;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2AddButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2DescriptionButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency3Obj;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency3Icon;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency3CountText;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3AddButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3DescriptionButton;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/BoxGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeNameText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/BoxGoodsDesc/ItemIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rechargeIconImage;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeHaveCountText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/SureBth/SureText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargePriceText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/ListPanel/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeGetCountText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/ListPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeGetExtraCountText;
    [AutoBind("./StoreUIPanel/RechargePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rechargePanelCloseButton;
    [AutoBind("./StoreUIPanel/RechargePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rechargePanelUIStateController;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/SureBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rechargeBuyButton;
    [AutoBind("./StoreUIPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_storePrivilegeBuyDetailPanelStateCtrl;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storePrivilegeBuyDetailPanelBGButon;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_storePrivilegeStateCtrl;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_StorePrivilegeItemName;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_StorePrivilegeItemIconImage;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemCountText;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ListPanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemDescText;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/Tape", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storePrivilegeItemLeftDayGameObject;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/Tape/TSurplusText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemLeftDay;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storePrivilegeItemBuyButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/BuyButton/SureText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemBuyPrice;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Month/Count/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storeMonthExtraRewardCount;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Subscribe/Count/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_subscribePrice;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect/AgreementButton1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subscribePanelPrivateButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect/AgreementButton2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subscribePanelCancelSubscribeButton;
    [AutoBind("./StoreUIPanel/MemoryEntranceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryEntranceButton;
    [AutoBind("./StoreUIPanel/MemoryEntranceButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryEntranceButtonStateCtrl;
    [AutoBind("./StoreUIPanel/MemoryEntranceButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryEntranceButtonRedPoint;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryExtractionPanelStateCtrl;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_memoryExtractionPanelScrollRect;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/ListScrollView/Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryExtractionPanelScrollContent;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelHelpButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/ExtractionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelExtractionButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_memoryExtractionPanelTotalValueText;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelCloseButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelBGReturnButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/Empty", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryExtractionPanelEmptyTip;
    [AutoBind("./Prefab/HeroFragmentItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HeroFragmentItemPrefab;
    private bool m_needRefreshPanel;
    private bool m_isIgnoreToggleEvent;
    private StoreType m_storeType;
    private StoreId m_storeId;
    private int m_fixedStoreItemGoodsID;
    private int m_goodsID;
    private int m_index;
    private StoreId m_fixedStoreID;
    private StoreId m_randomStoreID;
    private bool m_isStaticBox;
    public int m_selfChooseItemIndex;
    private Vector3 m_scrollViewStoreContentOriginalLocalPos;
    private Vector3 m_scrollViewHeroContentOriginalLocalPos;
    private GiftStoreItem m_giftStoreItem;
    private GameObjectPool<StoreItemUIController> m_storeUnderItemPool;
    private GameObjectPool<StoreHeroSkinItemUIController> m_storeHeroSkinItemPool;
    private GameObjectPool<StoreSoldierSkinItemUIController> m_storeSoldierSkinItemPool;
    private List<Goods> m_gainGoodsList;
    private Dictionary<StoreId, List<Toggle>> m_storeSubType2ToggleDic;
    private StoreSelfChooseUIController m_storeSelfChooseUIController;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    [DoNotToLua]
    private StoreListUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_GetHeroSkinItemCtrlByFixedStoreItemIdInt32_hotfix;
    private LuaFunction m_UpdateHeroSkinItemLimitTime_hotfix;
    private LuaFunction m_UpdateStoreStoreIdStoreType_hotfix;
    private LuaFunction m_UpdateRechargeStore_hotfix;
    private LuaFunction m_UpdateGiftStoreStoreId_hotfix;
    private LuaFunction m_UpdateFixedStoreStoreId_hotfix;
    private LuaFunction m_UpdateRandomStore_hotfix;
    private LuaFunction m_ClearPool_hotfix;
    private LuaFunction m_UpdateFixedStoreInfoStoreId_hotfix;
    private LuaFunction m_UpdateHeroSkinInfoStoreIdGameObjectPool`1_hotfix;
    private LuaFunction m_SetSkinTicketCountText_hotfix;
    private LuaFunction m_UpdateSoldierSkinInfoStoreIdGameObjectPool`1_hotfix;
    private LuaFunction m_UpdateRandomStoreInfoStoreId_hotfix;
    private LuaFunction m_UpdateRechargeStoreInfo_hotfix;
    private LuaFunction m_SetDefaultStateBoolean_hotfix;
    private LuaFunction m_SetFixedStoreInfoStoreIdGameObjectPool`1_hotfix;
    private LuaFunction m_SortFixedStoreItemByUISortFixedStoreItemFixedStoreItem_hotfix;
    private LuaFunction m_RefreshCurrencyDisplayList`1_hotfix;
    private LuaFunction m_SetRandomStoreInfoStoreIdGameObjectPool`1_hotfix;
    private LuaFunction m_SetRechargeStoreInfoGameObjectPool`1_hotfix;
    private LuaFunction m_ShowRefreshPanel_hotfix;
    private LuaFunction m_HideRefreshPanel_hotfix;
    private LuaFunction m_SetRefreshPanelStoreId_hotfix;
    private LuaFunction m_IsNeedRefreshPanel_hotfix;
    private LuaFunction m_StoreOpenTween_hotfix;
    private LuaFunction m_OnStoreItemClickStoreItemUIController_hotfix;
    private LuaFunction m_OnHeroSkinItemClickStoreHeroSkinItemUIController_hotfix;
    private LuaFunction m_OnSkinItemOutOfDate_hotfix;
    private LuaFunction m_OnSoldierSkinItemClickStoreSoldierSkinItemUIController_hotfix;
    private LuaFunction m_SetStoreBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_SetNormalItemBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_SetRechargeBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_SetBoxBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_SetItemBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_SetNormalItemBuyPanelGoodsStoreItemUIController_hotfix;
    private LuaFunction m_SetItemBuyPanelGoodsStoreItemUIController_hotfix;
    private LuaFunction m_SetEnchantStoneResonanceInfoPanelGoodsTypeInt32_hotfix;
    private LuaFunction m_SetRandomStoreCountDown_hotfix;
    private LuaFunction m_OpenBuyPanelStoreIdInt32_hotfix;
    private LuaFunction m_get_CurStoreId_hotfix;
    private LuaFunction m_UpdateGiftStoreInfoStoreIdGameObjectPool`1_hotfix;
    private LuaFunction m_SetGiftStoreInfoStoreIdGameObjectPool`1_hotfix;
    private LuaFunction m_SetGiftStoreBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_SetGiftStoreStaticBoxBuyPanelStoreItemUIControllerConfigDataItemInfo_hotfix;
    private LuaFunction m_SetMothCardBuyPanelStoreItemUIController_hotfix;
    private LuaFunction m_OnPresentPresentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnPresentPreogativeToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMonthCardBuyButtonClick_hotfix;
    private LuaFunction m_OnSubscribePrivateClick_hotfix;
    private LuaFunction m_OnCancelSubscribeClick_hotfix;
    private LuaFunction m_OnMonthCardPanelBGButtonClick_hotfix;
    private LuaFunction m_OnPresentToggleValueChangedBoolean_hotfix;
    private LuaFunction m_SetGiftStoreBuyTimesLimitGiftStoreItem_hotfix;
    private LuaFunction m_SetGiftStoreTimeLimitGiftStoreItem_hotfix;
    private LuaFunction m_SetGiftStoreValueOffGiftStoreItem_hotfix;
    private LuaFunction m_SetRecommendDescGiftStoreItem_hotfix;
    private LuaFunction m_OnCloseButtonClick_hotfix;
    private LuaFunction m_OnRefreshButtonClick_hotfix;
    private LuaFunction m_OnItemBuyPanelBGButtonClick_hotfix;
    private LuaFunction m_OnItemBuyButtonClick_hotfix;
    private LuaFunction m_OnBoxBuyPanelBGButtonClick_hotfix;
    private LuaFunction m_OnRechargePanelBGButtonClick_hotfix;
    private LuaFunction m_OnBoxBuyButtonClick_hotfix;
    private LuaFunction m_IsStoreItemIOSSubscribeItemStoreItemUIController_hotfix;
    private LuaFunction m_OnMemoryEntranceButtonClick_hotfix;
    private LuaFunction m_SetMemoryExtractionPanelInfo_hotfix;
    private LuaFunction m_SortFragmentItemComparerHeroFragmentBagItemHeroFragmentBagItem_hotfix;
    private LuaFunction m_OnMemoryExtractionPanelExtractionButtonClick_hotfix;
    private LuaFunction m_OnMemeryExtractionHelpButtonClick_hotfix;
    private LuaFunction m_CloseMemoryExtractionPanel_hotfix;
    private LuaFunction m_FireChangeStoreEventStoreId_hotfix;
    private LuaFunction m_OnRechargeBuyButtonClick_hotfix;
    private LuaFunction m_OnRechargeToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnCystalToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnSkinToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnSkinTab_HeroToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnSkinTab_SoldierToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousBlackMarketToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousMemoryToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousEquipmentToggleValueChangeBoolean_hotfix;
    private LuaFunction m_OnMysteriousHonorToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousMedalToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousFriendshipToggleValueChangedBoolean_hotfix;
    private LuaFunction m_OnMysteriousUnionToggleValueChangedBoolean_hotfix;
    private LuaFunction m_InitStoreSubType2Toggle_hotfix;
    private LuaFunction m_SaveOriginalScrollViewLocalPosition_hotfix;
    private LuaFunction m_UpdateTogglesStoreId_hotfix;
    private LuaFunction m_FixedStoreSkinListSortFuncFixedStoreItemFixedStoreItem_hotfix;
    private LuaFunction m_add_EventOnChangeStoreAction`1_hotfix;
    private LuaFunction m_remove_EventOnChangeStoreAction`1_hotfix;
    private LuaFunction m_add_EventOnFixedStoreHeroSkinItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnFixedStoreHeroSkinItemClickAction`1_hotfix;
    private LuaFunction m_add_EventOnFixedStoreSoldierSkinItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnFixedStoreSoldierSkinItemClickAction`1_hotfix;
    private LuaFunction m_add_EventOnSkinItemOutOfDateAction_hotfix;
    private LuaFunction m_remove_EventOnSkinItemOutOfDateAction_hotfix;
    private LuaFunction m_add_EventOnFixedStoreItemBuyClickAction`5_hotfix;
    private LuaFunction m_remove_EventOnFixedStoreItemBuyClickAction`5_hotfix;
    private LuaFunction m_add_EventOnFixedStoreBoxBuyClickAction`4_hotfix;
    private LuaFunction m_remove_EventOnFixedStoreBoxBuyClickAction`4_hotfix;
    private LuaFunction m_add_EventOnRandomStoreItemBuyClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnRandomStoreItemBuyClickAction`3_hotfix;
    private LuaFunction m_add_EventOnRandomStoreBoxBuyClickAction`4_hotfix;
    private LuaFunction m_remove_EventOnRandomStoreBoxBuyClickAction`4_hotfix;
    private LuaFunction m_add_EventOnRechargeStoreBuyClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnRechargeStoreBuyClickAction`3_hotfix;
    private LuaFunction m_add_EventOnGiftStoreBuyClickAction`3_hotfix;
    private LuaFunction m_remove_EventOnGiftStoreBuyClickAction`3_hotfix;
    private LuaFunction m_add_EventOnGetRandomStoreAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetRandomStoreAction`1_hotfix;
    private LuaFunction m_add_EventOnRefreshRandomStoreAction`1_hotfix;
    private LuaFunction m_remove_EventOnRefreshRandomStoreAction`1_hotfix;
    private LuaFunction m_add_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_remove_EventOnCrystalNotEnoughAction_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnExtraButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnExtraButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnMemoryExtractionAction`1_hotfix;
    private LuaFunction m_remove_EventOnMemoryExtractionAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreHeroSkinItemUIController GetHeroSkinItemCtrlByFixedStoreItemId(
      int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateHeroSkinItemLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateStore(StoreId storeId, StoreType storeType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRechargeStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateGiftStore(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateFixedStore(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRandomStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateFixedStoreInfo(StoreId storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateHeroSkinInfo(
      StoreId storeID,
      GameObjectPool<StoreHeroSkinItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkinTicketCountText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateSoldierSkinInfo(
      StoreId storeID,
      GameObjectPool<StoreSoldierSkinItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRandomStoreInfo(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRechargeStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetDefaultState(bool hideRefreshPanel = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetFixedStoreInfo(
      StoreId fixedStoreID,
      GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortFixedStoreItemByUISort(FixedStoreItem a, FixedStoreItem b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RefreshCurrencyDisplay(List<StoreItemUIController> currencyTypeList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRandomStoreInfo(StoreId storeId, GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRechargeStoreInfo(GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowRefreshPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void HideRefreshPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRefreshPanel(StoreId randomStoreID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedRefreshPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnStoreItemClick(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroSkinItemClick(StoreHeroSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinItemOutOfDate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSoldierSkinItemClick(StoreSoldierSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetStoreBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNormalItemBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRechargeBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetBoxBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetItemBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNormalItemBuyPanel(Goods goods, StoreItemUIController storeItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemBuyPanel(Goods goods, StoreItemUIController storeItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomStoreCountDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenBuyPanel(StoreId storeId, int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    public StoreId CurStoreId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateGiftStoreInfo(StoreId storeID, GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGiftStoreInfo(StoreId storeId, GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGiftStoreBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGiftStoreStaticBoxBuyPanel(
      StoreItemUIController ctrl,
      ConfigDataItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetMothCardBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPresentPresentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPresentPreogativeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubscribePrivateClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCancelSubscribeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPresentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGiftStoreBuyTimesLimit(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreTimeLimit(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreValueOff(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecommendDesc(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRefreshButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnItemBuyPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnItemBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBoxBuyPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargePanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBoxBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsStoreItemIOSSubscribeItem(StoreItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMemoryEntranceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMemoryExtractionPanelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortFragmentItemComparer(HeroFragmentBagItem f1, HeroFragmentBagItem f2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemoryExtractionPanelExtractionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemeryExtractionHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseMemoryExtractionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FireChangeStoreEvent(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCystalToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSkinToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSkinTab_HeroToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSkinTab_SoldierToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousBlackMarketToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousMemoryToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousEquipmentToggleValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousHonorToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousMedalToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousFriendshipToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousUnionToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitStoreSubType2Toggle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveOriginalScrollViewLocalPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool UpdateToggles(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int FixedStoreSkinListSortFunc(FixedStoreItem itemA, FixedStoreItem itemB)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreId> EventOnChangeStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreHeroSkinItemUIController> EventOnFixedStoreHeroSkinItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreSoldierSkinItemUIController> EventOnFixedStoreSoldierSkinItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinItemOutOfDate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event StoreListUIController.Action<StoreId, int, int, int, List<Goods>> EventOnFixedStoreItemBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId, int, int, List<Goods>> EventOnFixedStoreBoxBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId, int, List<Goods>> EventOnRandomStoreItemBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId, int, int, List<Goods>> EventOnRandomStoreBoxBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreType, int, int> EventOnRechargeStoreBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataGiftStoreItemInfo, bool, int> EventOnGiftStoreBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId> EventOnGetRandomStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId> EventOnRefreshRandomStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType> EventOnExtraButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnMemoryExtraction
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public StoreListUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeStore(StoreId obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeStore(StoreId obj)
    {
      this.EventOnChangeStore = (Action<StoreId>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFixedStoreHeroSkinItemClick(StoreHeroSkinItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFixedStoreHeroSkinItemClick(StoreHeroSkinItemUIController obj)
    {
      this.EventOnFixedStoreHeroSkinItemClick = (Action<StoreHeroSkinItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFixedStoreSoldierSkinItemClick(
      StoreSoldierSkinItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFixedStoreSoldierSkinItemClick(
      StoreSoldierSkinItemUIController obj)
    {
      this.EventOnFixedStoreSoldierSkinItemClick = (Action<StoreSoldierSkinItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSkinItemOutOfDate()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSkinItemOutOfDate()
    {
      this.EventOnSkinItemOutOfDate = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFixedStoreItemBuyClick(
      StoreId arg1,
      int arg2,
      int arg3,
      int arg4,
      List<Goods> arg5)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFixedStoreItemBuyClick(
      StoreId arg1,
      int arg2,
      int arg3,
      int arg4,
      List<Goods> arg5)
    {
      this.EventOnFixedStoreItemBuyClick = (StoreListUIController.Action<StoreId, int, int, int, List<Goods>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFixedStoreBoxBuyClick(
      StoreId arg1,
      int arg2,
      int arg3,
      List<Goods> arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFixedStoreBoxBuyClick(
      StoreId arg1,
      int arg2,
      int arg3,
      List<Goods> arg4)
    {
      this.EventOnFixedStoreBoxBuyClick = (Action<StoreId, int, int, List<Goods>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRandomStoreItemBuyClick(
      StoreId arg1,
      int arg2,
      List<Goods> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRandomStoreItemBuyClick(
      StoreId arg1,
      int arg2,
      List<Goods> arg3)
    {
      this.EventOnRandomStoreItemBuyClick = (Action<StoreId, int, List<Goods>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRandomStoreBoxBuyClick(
      StoreId arg1,
      int arg2,
      int arg3,
      List<Goods> arg4)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRandomStoreBoxBuyClick(
      StoreId arg1,
      int arg2,
      int arg3,
      List<Goods> arg4)
    {
      this.EventOnRandomStoreBoxBuyClick = (Action<StoreId, int, int, List<Goods>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRechargeStoreBuyClick(StoreType arg1, int arg2, int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRechargeStoreBuyClick(StoreType arg1, int arg2, int arg3)
    {
      this.EventOnRechargeStoreBuyClick = (Action<StoreType, int, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGiftStoreBuyClick(
      ConfigDataGiftStoreItemInfo arg1,
      bool arg2,
      int arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGiftStoreBuyClick(
      ConfigDataGiftStoreItemInfo arg1,
      bool arg2,
      int arg3)
    {
      this.EventOnGiftStoreBuyClick = (Action<ConfigDataGiftStoreItemInfo, bool, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetRandomStore(StoreId obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetRandomStore(StoreId obj)
    {
      this.EventOnGetRandomStore = (Action<StoreId>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnRefreshRandomStore(StoreId obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnRefreshRandomStore(StoreId obj)
    {
      this.EventOnRefreshRandomStore = (Action<StoreId>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCrystalNotEnough()
    {
      this.EventOnCrystalNotEnough = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnExtraButtonClick(GoodsType obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnExtraButtonClick(GoodsType obj)
    {
      this.EventOnExtraButtonClick = (Action<GoodsType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnMemoryExtraction(Action obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnMemoryExtraction(Action obj)
    {
      this.EventOnMemoryExtraction = (Action<Action>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public class LuaExportHelper
    {
      private StoreListUIController m_owner;

      public LuaExportHelper(StoreListUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        return this.m_owner.__callBase_BindFieldImpl(fieldType, path, initState, fieldName, ctrlName, optional);
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnChangeStore(StoreId obj)
      {
        this.m_owner.__callDele_EventOnChangeStore(obj);
      }

      public void __clearDele_EventOnChangeStore(StoreId obj)
      {
        this.m_owner.__clearDele_EventOnChangeStore(obj);
      }

      public void __callDele_EventOnFixedStoreHeroSkinItemClick(StoreHeroSkinItemUIController obj)
      {
        this.m_owner.__callDele_EventOnFixedStoreHeroSkinItemClick(obj);
      }

      public void __clearDele_EventOnFixedStoreHeroSkinItemClick(StoreHeroSkinItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnFixedStoreHeroSkinItemClick(obj);
      }

      public void __callDele_EventOnFixedStoreSoldierSkinItemClick(
        StoreSoldierSkinItemUIController obj)
      {
        this.m_owner.__callDele_EventOnFixedStoreSoldierSkinItemClick(obj);
      }

      public void __clearDele_EventOnFixedStoreSoldierSkinItemClick(
        StoreSoldierSkinItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnFixedStoreSoldierSkinItemClick(obj);
      }

      public void __callDele_EventOnSkinItemOutOfDate()
      {
        this.m_owner.__callDele_EventOnSkinItemOutOfDate();
      }

      public void __clearDele_EventOnSkinItemOutOfDate()
      {
        this.m_owner.__clearDele_EventOnSkinItemOutOfDate();
      }

      public void __callDele_EventOnFixedStoreItemBuyClick(
        StoreId arg1,
        int arg2,
        int arg3,
        int arg4,
        List<Goods> arg5)
      {
        this.m_owner.__callDele_EventOnFixedStoreItemBuyClick(arg1, arg2, arg3, arg4, arg5);
      }

      public void __clearDele_EventOnFixedStoreItemBuyClick(
        StoreId arg1,
        int arg2,
        int arg3,
        int arg4,
        List<Goods> arg5)
      {
        this.m_owner.__clearDele_EventOnFixedStoreItemBuyClick(arg1, arg2, arg3, arg4, arg5);
      }

      public void __callDele_EventOnFixedStoreBoxBuyClick(
        StoreId arg1,
        int arg2,
        int arg3,
        List<Goods> arg4)
      {
        this.m_owner.__callDele_EventOnFixedStoreBoxBuyClick(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnFixedStoreBoxBuyClick(
        StoreId arg1,
        int arg2,
        int arg3,
        List<Goods> arg4)
      {
        this.m_owner.__clearDele_EventOnFixedStoreBoxBuyClick(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnRandomStoreItemBuyClick(
        StoreId arg1,
        int arg2,
        List<Goods> arg3)
      {
        this.m_owner.__callDele_EventOnRandomStoreItemBuyClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnRandomStoreItemBuyClick(
        StoreId arg1,
        int arg2,
        List<Goods> arg3)
      {
        this.m_owner.__clearDele_EventOnRandomStoreItemBuyClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnRandomStoreBoxBuyClick(
        StoreId arg1,
        int arg2,
        int arg3,
        List<Goods> arg4)
      {
        this.m_owner.__callDele_EventOnRandomStoreBoxBuyClick(arg1, arg2, arg3, arg4);
      }

      public void __clearDele_EventOnRandomStoreBoxBuyClick(
        StoreId arg1,
        int arg2,
        int arg3,
        List<Goods> arg4)
      {
        this.m_owner.__clearDele_EventOnRandomStoreBoxBuyClick(arg1, arg2, arg3, arg4);
      }

      public void __callDele_EventOnRechargeStoreBuyClick(StoreType arg1, int arg2, int arg3)
      {
        this.m_owner.__callDele_EventOnRechargeStoreBuyClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnRechargeStoreBuyClick(StoreType arg1, int arg2, int arg3)
      {
        this.m_owner.__clearDele_EventOnRechargeStoreBuyClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnGiftStoreBuyClick(
        ConfigDataGiftStoreItemInfo arg1,
        bool arg2,
        int arg3)
      {
        this.m_owner.__callDele_EventOnGiftStoreBuyClick(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnGiftStoreBuyClick(
        ConfigDataGiftStoreItemInfo arg1,
        bool arg2,
        int arg3)
      {
        this.m_owner.__clearDele_EventOnGiftStoreBuyClick(arg1, arg2, arg3);
      }

      public void __callDele_EventOnGetRandomStore(StoreId obj)
      {
        this.m_owner.__callDele_EventOnGetRandomStore(obj);
      }

      public void __clearDele_EventOnGetRandomStore(StoreId obj)
      {
        this.m_owner.__clearDele_EventOnGetRandomStore(obj);
      }

      public void __callDele_EventOnRefreshRandomStore(StoreId obj)
      {
        this.m_owner.__callDele_EventOnRefreshRandomStore(obj);
      }

      public void __clearDele_EventOnRefreshRandomStore(StoreId obj)
      {
        this.m_owner.__clearDele_EventOnRefreshRandomStore(obj);
      }

      public void __callDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__callDele_EventOnCrystalNotEnough();
      }

      public void __clearDele_EventOnCrystalNotEnough()
      {
        this.m_owner.__clearDele_EventOnCrystalNotEnough();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnExtraButtonClick(GoodsType obj)
      {
        this.m_owner.__callDele_EventOnExtraButtonClick(obj);
      }

      public void __clearDele_EventOnExtraButtonClick(GoodsType obj)
      {
        this.m_owner.__clearDele_EventOnExtraButtonClick(obj);
      }

      public void __callDele_EventOnMemoryExtraction(Action obj)
      {
        this.m_owner.__callDele_EventOnMemoryExtraction(obj);
      }

      public void __clearDele_EventOnMemoryExtraction(Action obj)
      {
        this.m_owner.__clearDele_EventOnMemoryExtraction(obj);
      }

      public CommonUIStateController m_storeUIStateCtrl
      {
        get
        {
          return this.m_owner.m_storeUIStateCtrl;
        }
        set
        {
          this.m_owner.m_storeUIStateCtrl = value;
        }
      }

      public CommonUIStateController m_refreshPanelUIStateCtrl
      {
        get
        {
          return this.m_owner.m_refreshPanelUIStateCtrl;
        }
        set
        {
          this.m_owner.m_refreshPanelUIStateCtrl = value;
        }
      }

      public GameObject m_scrollViewStoreContent
      {
        get
        {
          return this.m_owner.m_scrollViewStoreContent;
        }
        set
        {
          this.m_owner.m_scrollViewStoreContent = value;
        }
      }

      public GameObject m_scrollViewHeroContent
      {
        get
        {
          return this.m_owner.m_scrollViewHeroContent;
        }
        set
        {
          this.m_owner.m_scrollViewHeroContent = value;
        }
      }

      public CommonUIStateController m_itemBuyPanelUIStateController
      {
        get
        {
          return this.m_owner.m_itemBuyPanelUIStateController;
        }
        set
        {
          this.m_owner.m_itemBuyPanelUIStateController = value;
        }
      }

      public Image m_itemIconImage
      {
        get
        {
          return this.m_owner.m_itemIconImage;
        }
        set
        {
          this.m_owner.m_itemIconImage = value;
        }
      }

      public Text m_itemCountText
      {
        get
        {
          return this.m_owner.m_itemCountText;
        }
        set
        {
          this.m_owner.m_itemCountText = value;
        }
      }

      public GameObject m_itemCountBgGo
      {
        get
        {
          return this.m_owner.m_itemCountBgGo;
        }
        set
        {
          this.m_owner.m_itemCountBgGo = value;
        }
      }

      public Text m_itemNameText
      {
        get
        {
          return this.m_owner.m_itemNameText;
        }
        set
        {
          this.m_owner.m_itemNameText = value;
        }
      }

      public GameObject m_itemGoodCountObj
      {
        get
        {
          return this.m_owner.m_itemGoodCountObj;
        }
        set
        {
          this.m_owner.m_itemGoodCountObj = value;
        }
      }

      public Text m_itemHaveCountText
      {
        get
        {
          return this.m_owner.m_itemHaveCountText;
        }
        set
        {
          this.m_owner.m_itemHaveCountText = value;
        }
      }

      public Text m_itemDescText
      {
        get
        {
          return this.m_owner.m_itemDescText;
        }
        set
        {
          this.m_owner.m_itemDescText = value;
        }
      }

      public Image m_itemPriceIcon
      {
        get
        {
          return this.m_owner.m_itemPriceIcon;
        }
        set
        {
          this.m_owner.m_itemPriceIcon = value;
        }
      }

      public GameObject m_itemPriceIconEffectGameObject
      {
        get
        {
          return this.m_owner.m_itemPriceIconEffectGameObject;
        }
        set
        {
          this.m_owner.m_itemPriceIconEffectGameObject = value;
        }
      }

      public Text m_itemPriceText
      {
        get
        {
          return this.m_owner.m_itemPriceText;
        }
        set
        {
          this.m_owner.m_itemPriceText = value;
        }
      }

      public Button m_itemBuyPanelCloseButton
      {
        get
        {
          return this.m_owner.m_itemBuyPanelCloseButton;
        }
        set
        {
          this.m_owner.m_itemBuyPanelCloseButton = value;
        }
      }

      public Button m_itemBuyButton
      {
        get
        {
          return this.m_owner.m_itemBuyButton;
        }
        set
        {
          this.m_owner.m_itemBuyButton = value;
        }
      }

      public GameObject m_resonanceInfoPanel
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanel;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanel = value;
        }
      }

      public Text m_resonanceInfoPanelNameText
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanelNameText;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanelNameText = value;
        }
      }

      public Text m_resonanceInfoPanel2SuitInfoText
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanel2SuitInfoText;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanel2SuitInfoText = value;
        }
      }

      public Text m_resonanceInfoPanel4SuitInfoText
      {
        get
        {
          return this.m_owner.m_resonanceInfoPanel4SuitInfoText;
        }
        set
        {
          this.m_owner.m_resonanceInfoPanel4SuitInfoText = value;
        }
      }

      public CommonUIStateController m_boxBuyPanelUIStateController
      {
        get
        {
          return this.m_owner.m_boxBuyPanelUIStateController;
        }
        set
        {
          this.m_owner.m_boxBuyPanelUIStateController = value;
        }
      }

      public Image m_boxIconImage
      {
        get
        {
          return this.m_owner.m_boxIconImage;
        }
        set
        {
          this.m_owner.m_boxIconImage = value;
        }
      }

      public Text m_boxNameText
      {
        get
        {
          return this.m_owner.m_boxNameText;
        }
        set
        {
          this.m_owner.m_boxNameText = value;
        }
      }

      public GameObject m_boxDescGameObject
      {
        get
        {
          return this.m_owner.m_boxDescGameObject;
        }
        set
        {
          this.m_owner.m_boxDescGameObject = value;
        }
      }

      public Text m_boxDescText
      {
        get
        {
          return this.m_owner.m_boxDescText;
        }
        set
        {
          this.m_owner.m_boxDescText = value;
        }
      }

      public CommonUIStateController m_boxDescUIStateController
      {
        get
        {
          return this.m_owner.m_boxDescUIStateController;
        }
        set
        {
          this.m_owner.m_boxDescUIStateController = value;
        }
      }

      public GameObject m_boxListScrollViewContent
      {
        get
        {
          return this.m_owner.m_boxListScrollViewContent;
        }
        set
        {
          this.m_owner.m_boxListScrollViewContent = value;
        }
      }

      public Image m_boxPriceIcon
      {
        get
        {
          return this.m_owner.m_boxPriceIcon;
        }
        set
        {
          this.m_owner.m_boxPriceIcon = value;
        }
      }

      public GameObject m_boxPriceIconEffectGameObject
      {
        get
        {
          return this.m_owner.m_boxPriceIconEffectGameObject;
        }
        set
        {
          this.m_owner.m_boxPriceIconEffectGameObject = value;
        }
      }

      public Text m_boxPriceText
      {
        get
        {
          return this.m_owner.m_boxPriceText;
        }
        set
        {
          this.m_owner.m_boxPriceText = value;
        }
      }

      public Button m_boxBuyPanelCloseButton
      {
        get
        {
          return this.m_owner.m_boxBuyPanelCloseButton;
        }
        set
        {
          this.m_owner.m_boxBuyPanelCloseButton = value;
        }
      }

      public Button m_boxBuyButton
      {
        get
        {
          return this.m_owner.m_boxBuyButton;
        }
        set
        {
          this.m_owner.m_boxBuyButton = value;
        }
      }

      public GameObject m_boxTimeLimitGameObject
      {
        get
        {
          return this.m_owner.m_boxTimeLimitGameObject;
        }
        set
        {
          this.m_owner.m_boxTimeLimitGameObject = value;
        }
      }

      public Text m_boxTimeLimitText
      {
        get
        {
          return this.m_owner.m_boxTimeLimitText;
        }
        set
        {
          this.m_owner.m_boxTimeLimitText = value;
        }
      }

      public GameObject m_boxBuyTimesGameObject
      {
        get
        {
          return this.m_owner.m_boxBuyTimesGameObject;
        }
        set
        {
          this.m_owner.m_boxBuyTimesGameObject = value;
        }
      }

      public Text m_boxBuyTimesText
      {
        get
        {
          return this.m_owner.m_boxBuyTimesText;
        }
        set
        {
          this.m_owner.m_boxBuyTimesText = value;
        }
      }

      public GameObject m_boxValueOffGameObject
      {
        get
        {
          return this.m_owner.m_boxValueOffGameObject;
        }
        set
        {
          this.m_owner.m_boxValueOffGameObject = value;
        }
      }

      public Text m_boxValueOffText
      {
        get
        {
          return this.m_owner.m_boxValueOffText;
        }
        set
        {
          this.m_owner.m_boxValueOffText = value;
        }
      }

      public Text m_boxCountText
      {
        get
        {
          return this.m_owner.m_boxCountText;
        }
        set
        {
          this.m_owner.m_boxCountText = value;
        }
      }

      public Text m_boxHaveCountText
      {
        get
        {
          return this.m_owner.m_boxHaveCountText;
        }
        set
        {
          this.m_owner.m_boxHaveCountText = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_storeBoxItemPrefab
      {
        get
        {
          return this.m_owner.m_storeBoxItemPrefab;
        }
        set
        {
          this.m_owner.m_storeBoxItemPrefab = value;
        }
      }

      public GameObject m_storeItemPrefab
      {
        get
        {
          return this.m_owner.m_storeItemPrefab;
        }
        set
        {
          this.m_owner.m_storeItemPrefab = value;
        }
      }

      public GameObject m_storeHeroSkinItemPrefab
      {
        get
        {
          return this.m_owner.m_storeHeroSkinItemPrefab;
        }
        set
        {
          this.m_owner.m_storeHeroSkinItemPrefab = value;
        }
      }

      public GameObject m_storeSoldierSkinItemPrefab
      {
        get
        {
          return this.m_owner.m_storeSoldierSkinItemPrefab;
        }
        set
        {
          this.m_owner.m_storeSoldierSkinItemPrefab = value;
        }
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Toggle m_rechargeToggle
      {
        get
        {
          return this.m_owner.m_rechargeToggle;
        }
        set
        {
          this.m_owner.m_rechargeToggle = value;
        }
      }

      public Toggle m_presentToggle
      {
        get
        {
          return this.m_owner.m_presentToggle;
        }
        set
        {
          this.m_owner.m_presentToggle = value;
        }
      }

      public Toggle m_crystalToggle
      {
        get
        {
          return this.m_owner.m_crystalToggle;
        }
        set
        {
          this.m_owner.m_crystalToggle = value;
        }
      }

      public Toggle m_skinToggle
      {
        get
        {
          return this.m_owner.m_skinToggle;
        }
        set
        {
          this.m_owner.m_skinToggle = value;
        }
      }

      public Toggle m_mysteriousToggle
      {
        get
        {
          return this.m_owner.m_mysteriousToggle;
        }
        set
        {
          this.m_owner.m_mysteriousToggle = value;
        }
      }

      public GameObject m_selfChoosePanel
      {
        get
        {
          return this.m_owner.m_selfChoosePanel;
        }
        set
        {
          this.m_owner.m_selfChoosePanel = value;
        }
      }

      public Text m_refreshTimeText
      {
        get
        {
          return this.m_owner.m_refreshTimeText;
        }
        set
        {
          this.m_owner.m_refreshTimeText = value;
        }
      }

      public Image m_refreshCurrencyTypeIcon
      {
        get
        {
          return this.m_owner.m_refreshCurrencyTypeIcon;
        }
        set
        {
          this.m_owner.m_refreshCurrencyTypeIcon = value;
        }
      }

      public GameObject m_refreshCurrencyTypeIconEffectGameObject
      {
        get
        {
          return this.m_owner.m_refreshCurrencyTypeIconEffectGameObject;
        }
        set
        {
          this.m_owner.m_refreshCurrencyTypeIconEffectGameObject = value;
        }
      }

      public Text m_refreshPriceText
      {
        get
        {
          return this.m_owner.m_refreshPriceText;
        }
        set
        {
          this.m_owner.m_refreshPriceText = value;
        }
      }

      public Button m_refreshButton
      {
        get
        {
          return this.m_owner.m_refreshButton;
        }
        set
        {
          this.m_owner.m_refreshButton = value;
        }
      }

      public Text m_refreshTimesText
      {
        get
        {
          return this.m_owner.m_refreshTimesText;
        }
        set
        {
          this.m_owner.m_refreshTimesText = value;
        }
      }

      public Toggle m_soldierSkinToggle
      {
        get
        {
          return this.m_owner.m_soldierSkinToggle;
        }
        set
        {
          this.m_owner.m_soldierSkinToggle = value;
        }
      }

      public Toggle m_heroSkinToggle
      {
        get
        {
          return this.m_owner.m_heroSkinToggle;
        }
        set
        {
          this.m_owner.m_heroSkinToggle = value;
        }
      }

      public Toggle m_mysteriousBlackMarketToggle
      {
        get
        {
          return this.m_owner.m_mysteriousBlackMarketToggle;
        }
        set
        {
          this.m_owner.m_mysteriousBlackMarketToggle = value;
        }
      }

      public Toggle m_mysteriousHonorToggle
      {
        get
        {
          return this.m_owner.m_mysteriousHonorToggle;
        }
        set
        {
          this.m_owner.m_mysteriousHonorToggle = value;
        }
      }

      public Toggle m_mysteriousMedalToggle
      {
        get
        {
          return this.m_owner.m_mysteriousMedalToggle;
        }
        set
        {
          this.m_owner.m_mysteriousMedalToggle = value;
        }
      }

      public Toggle m_mysteriousFriendshipToggle
      {
        get
        {
          return this.m_owner.m_mysteriousFriendshipToggle;
        }
        set
        {
          this.m_owner.m_mysteriousFriendshipToggle = value;
        }
      }

      public Toggle m_mysteriousUnionToggle
      {
        get
        {
          return this.m_owner.m_mysteriousUnionToggle;
        }
        set
        {
          this.m_owner.m_mysteriousUnionToggle = value;
        }
      }

      public Toggle m_presentPresentToggle
      {
        get
        {
          return this.m_owner.m_presentPresentToggle;
        }
        set
        {
          this.m_owner.m_presentPresentToggle = value;
        }
      }

      public Toggle m_presentPreogativeToggle
      {
        get
        {
          return this.m_owner.m_presentPreogativeToggle;
        }
        set
        {
          this.m_owner.m_presentPreogativeToggle = value;
        }
      }

      public Toggle m_skinHeroToggle
      {
        get
        {
          return this.m_owner.m_skinHeroToggle;
        }
        set
        {
          this.m_owner.m_skinHeroToggle = value;
        }
      }

      public Toggle m_skinSoldierToggle
      {
        get
        {
          return this.m_owner.m_skinSoldierToggle;
        }
        set
        {
          this.m_owner.m_skinSoldierToggle = value;
        }
      }

      public Toggle m_mysteriousMemoryToggle
      {
        get
        {
          return this.m_owner.m_mysteriousMemoryToggle;
        }
        set
        {
          this.m_owner.m_mysteriousMemoryToggle = value;
        }
      }

      public Toggle m_mysteriousEquipmentToggle
      {
        get
        {
          return this.m_owner.m_mysteriousEquipmentToggle;
        }
        set
        {
          this.m_owner.m_mysteriousEquipmentToggle = value;
        }
      }

      public GameObject m_currency1Obj
      {
        get
        {
          return this.m_owner.m_currency1Obj;
        }
        set
        {
          this.m_owner.m_currency1Obj = value;
        }
      }

      public Image m_currency1Icon
      {
        get
        {
          return this.m_owner.m_currency1Icon;
        }
        set
        {
          this.m_owner.m_currency1Icon = value;
        }
      }

      public Text m_currency1CountText
      {
        get
        {
          return this.m_owner.m_currency1CountText;
        }
        set
        {
          this.m_owner.m_currency1CountText = value;
        }
      }

      public Button m_currency1AddButton
      {
        get
        {
          return this.m_owner.m_currency1AddButton;
        }
        set
        {
          this.m_owner.m_currency1AddButton = value;
        }
      }

      public Button m_currency1DescriptionButton
      {
        get
        {
          return this.m_owner.m_currency1DescriptionButton;
        }
        set
        {
          this.m_owner.m_currency1DescriptionButton = value;
        }
      }

      public GameObject m_currency2Obj
      {
        get
        {
          return this.m_owner.m_currency2Obj;
        }
        set
        {
          this.m_owner.m_currency2Obj = value;
        }
      }

      public Image m_currency2Icon
      {
        get
        {
          return this.m_owner.m_currency2Icon;
        }
        set
        {
          this.m_owner.m_currency2Icon = value;
        }
      }

      public Text m_currency2CountText
      {
        get
        {
          return this.m_owner.m_currency2CountText;
        }
        set
        {
          this.m_owner.m_currency2CountText = value;
        }
      }

      public Button m_currency2AddButton
      {
        get
        {
          return this.m_owner.m_currency2AddButton;
        }
        set
        {
          this.m_owner.m_currency2AddButton = value;
        }
      }

      public Button m_currency2DescriptionButton
      {
        get
        {
          return this.m_owner.m_currency2DescriptionButton;
        }
        set
        {
          this.m_owner.m_currency2DescriptionButton = value;
        }
      }

      public GameObject m_currency3Obj
      {
        get
        {
          return this.m_owner.m_currency3Obj;
        }
        set
        {
          this.m_owner.m_currency3Obj = value;
        }
      }

      public Image m_currency3Icon
      {
        get
        {
          return this.m_owner.m_currency3Icon;
        }
        set
        {
          this.m_owner.m_currency3Icon = value;
        }
      }

      public Text m_currency3CountText
      {
        get
        {
          return this.m_owner.m_currency3CountText;
        }
        set
        {
          this.m_owner.m_currency3CountText = value;
        }
      }

      public Button m_currency3AddButton
      {
        get
        {
          return this.m_owner.m_currency3AddButton;
        }
        set
        {
          this.m_owner.m_currency3AddButton = value;
        }
      }

      public Button m_currency3DescriptionButton
      {
        get
        {
          return this.m_owner.m_currency3DescriptionButton;
        }
        set
        {
          this.m_owner.m_currency3DescriptionButton = value;
        }
      }

      public Text m_rechargeNameText
      {
        get
        {
          return this.m_owner.m_rechargeNameText;
        }
        set
        {
          this.m_owner.m_rechargeNameText = value;
        }
      }

      public Image m_rechargeIconImage
      {
        get
        {
          return this.m_owner.m_rechargeIconImage;
        }
        set
        {
          this.m_owner.m_rechargeIconImage = value;
        }
      }

      public Text m_rechargeHaveCountText
      {
        get
        {
          return this.m_owner.m_rechargeHaveCountText;
        }
        set
        {
          this.m_owner.m_rechargeHaveCountText = value;
        }
      }

      public Text m_rechargePriceText
      {
        get
        {
          return this.m_owner.m_rechargePriceText;
        }
        set
        {
          this.m_owner.m_rechargePriceText = value;
        }
      }

      public Text m_rechargeGetCountText
      {
        get
        {
          return this.m_owner.m_rechargeGetCountText;
        }
        set
        {
          this.m_owner.m_rechargeGetCountText = value;
        }
      }

      public Text m_rechargeGetExtraCountText
      {
        get
        {
          return this.m_owner.m_rechargeGetExtraCountText;
        }
        set
        {
          this.m_owner.m_rechargeGetExtraCountText = value;
        }
      }

      public Button m_rechargePanelCloseButton
      {
        get
        {
          return this.m_owner.m_rechargePanelCloseButton;
        }
        set
        {
          this.m_owner.m_rechargePanelCloseButton = value;
        }
      }

      public CommonUIStateController m_rechargePanelUIStateController
      {
        get
        {
          return this.m_owner.m_rechargePanelUIStateController;
        }
        set
        {
          this.m_owner.m_rechargePanelUIStateController = value;
        }
      }

      public Button m_rechargeBuyButton
      {
        get
        {
          return this.m_owner.m_rechargeBuyButton;
        }
        set
        {
          this.m_owner.m_rechargeBuyButton = value;
        }
      }

      public Button m_closeButton
      {
        get
        {
          return this.m_owner.m_closeButton;
        }
        set
        {
          this.m_owner.m_closeButton = value;
        }
      }

      public CommonUIStateController m_storePrivilegeBuyDetailPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_storePrivilegeBuyDetailPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_storePrivilegeBuyDetailPanelStateCtrl = value;
        }
      }

      public Button m_storePrivilegeBuyDetailPanelBGButon
      {
        get
        {
          return this.m_owner.m_storePrivilegeBuyDetailPanelBGButon;
        }
        set
        {
          this.m_owner.m_storePrivilegeBuyDetailPanelBGButon = value;
        }
      }

      public CommonUIStateController m_storePrivilegeStateCtrl
      {
        get
        {
          return this.m_owner.m_storePrivilegeStateCtrl;
        }
        set
        {
          this.m_owner.m_storePrivilegeStateCtrl = value;
        }
      }

      public Text m_StorePrivilegeItemName
      {
        get
        {
          return this.m_owner.m_StorePrivilegeItemName;
        }
        set
        {
          this.m_owner.m_StorePrivilegeItemName = value;
        }
      }

      public Image m_StorePrivilegeItemIconImage
      {
        get
        {
          return this.m_owner.m_StorePrivilegeItemIconImage;
        }
        set
        {
          this.m_owner.m_StorePrivilegeItemIconImage = value;
        }
      }

      public Text m_storePrivilegeItemCountText
      {
        get
        {
          return this.m_owner.m_storePrivilegeItemCountText;
        }
        set
        {
          this.m_owner.m_storePrivilegeItemCountText = value;
        }
      }

      public Text m_storePrivilegeItemDescText
      {
        get
        {
          return this.m_owner.m_storePrivilegeItemDescText;
        }
        set
        {
          this.m_owner.m_storePrivilegeItemDescText = value;
        }
      }

      public GameObject m_storePrivilegeItemLeftDayGameObject
      {
        get
        {
          return this.m_owner.m_storePrivilegeItemLeftDayGameObject;
        }
        set
        {
          this.m_owner.m_storePrivilegeItemLeftDayGameObject = value;
        }
      }

      public Text m_storePrivilegeItemLeftDay
      {
        get
        {
          return this.m_owner.m_storePrivilegeItemLeftDay;
        }
        set
        {
          this.m_owner.m_storePrivilegeItemLeftDay = value;
        }
      }

      public Button m_storePrivilegeItemBuyButton
      {
        get
        {
          return this.m_owner.m_storePrivilegeItemBuyButton;
        }
        set
        {
          this.m_owner.m_storePrivilegeItemBuyButton = value;
        }
      }

      public Text m_storePrivilegeItemBuyPrice
      {
        get
        {
          return this.m_owner.m_storePrivilegeItemBuyPrice;
        }
        set
        {
          this.m_owner.m_storePrivilegeItemBuyPrice = value;
        }
      }

      public Text m_storeMonthExtraRewardCount
      {
        get
        {
          return this.m_owner.m_storeMonthExtraRewardCount;
        }
        set
        {
          this.m_owner.m_storeMonthExtraRewardCount = value;
        }
      }

      public Text m_subscribePrice
      {
        get
        {
          return this.m_owner.m_subscribePrice;
        }
        set
        {
          this.m_owner.m_subscribePrice = value;
        }
      }

      public Button m_subscribePanelPrivateButton
      {
        get
        {
          return this.m_owner.m_subscribePanelPrivateButton;
        }
        set
        {
          this.m_owner.m_subscribePanelPrivateButton = value;
        }
      }

      public Button m_subscribePanelCancelSubscribeButton
      {
        get
        {
          return this.m_owner.m_subscribePanelCancelSubscribeButton;
        }
        set
        {
          this.m_owner.m_subscribePanelCancelSubscribeButton = value;
        }
      }

      public Button m_memoryEntranceButton
      {
        get
        {
          return this.m_owner.m_memoryEntranceButton;
        }
        set
        {
          this.m_owner.m_memoryEntranceButton = value;
        }
      }

      public CommonUIStateController m_memoryEntranceButtonStateCtrl
      {
        get
        {
          return this.m_owner.m_memoryEntranceButtonStateCtrl;
        }
        set
        {
          this.m_owner.m_memoryEntranceButtonStateCtrl = value;
        }
      }

      public GameObject m_memoryEntranceButtonRedPoint
      {
        get
        {
          return this.m_owner.m_memoryEntranceButtonRedPoint;
        }
        set
        {
          this.m_owner.m_memoryEntranceButtonRedPoint = value;
        }
      }

      public CommonUIStateController m_memoryExtractionPanelStateCtrl
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelStateCtrl;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelStateCtrl = value;
        }
      }

      public ScrollRect m_memoryExtractionPanelScrollRect
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelScrollRect;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelScrollRect = value;
        }
      }

      public GameObject m_memoryExtractionPanelScrollContent
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelScrollContent;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelScrollContent = value;
        }
      }

      public Button m_memoryExtractionPanelHelpButton
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelHelpButton;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelHelpButton = value;
        }
      }

      public Button m_memoryExtractionPanelExtractionButton
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelExtractionButton;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelExtractionButton = value;
        }
      }

      public Text m_memoryExtractionPanelTotalValueText
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelTotalValueText;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelTotalValueText = value;
        }
      }

      public Button m_memoryExtractionPanelCloseButton
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelCloseButton;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelCloseButton = value;
        }
      }

      public Button m_memoryExtractionPanelBGReturnButton
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelBGReturnButton;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelBGReturnButton = value;
        }
      }

      public GameObject m_memoryExtractionPanelEmptyTip
      {
        get
        {
          return this.m_owner.m_memoryExtractionPanelEmptyTip;
        }
        set
        {
          this.m_owner.m_memoryExtractionPanelEmptyTip = value;
        }
      }

      public GameObject m_HeroFragmentItemPrefab
      {
        get
        {
          return this.m_owner.m_HeroFragmentItemPrefab;
        }
        set
        {
          this.m_owner.m_HeroFragmentItemPrefab = value;
        }
      }

      public bool m_needRefreshPanel
      {
        get
        {
          return this.m_owner.m_needRefreshPanel;
        }
        set
        {
          this.m_owner.m_needRefreshPanel = value;
        }
      }

      public bool m_isIgnoreToggleEvent
      {
        get
        {
          return this.m_owner.m_isIgnoreToggleEvent;
        }
        set
        {
          this.m_owner.m_isIgnoreToggleEvent = value;
        }
      }

      public StoreType m_storeType
      {
        get
        {
          return this.m_owner.m_storeType;
        }
        set
        {
          this.m_owner.m_storeType = value;
        }
      }

      public StoreId m_storeId
      {
        get
        {
          return this.m_owner.m_storeId;
        }
        set
        {
          this.m_owner.m_storeId = value;
        }
      }

      public int m_fixedStoreItemGoodsID
      {
        get
        {
          return this.m_owner.m_fixedStoreItemGoodsID;
        }
        set
        {
          this.m_owner.m_fixedStoreItemGoodsID = value;
        }
      }

      public int m_goodsID
      {
        get
        {
          return this.m_owner.m_goodsID;
        }
        set
        {
          this.m_owner.m_goodsID = value;
        }
      }

      public int m_index
      {
        get
        {
          return this.m_owner.m_index;
        }
        set
        {
          this.m_owner.m_index = value;
        }
      }

      public StoreId m_fixedStoreID
      {
        get
        {
          return this.m_owner.m_fixedStoreID;
        }
        set
        {
          this.m_owner.m_fixedStoreID = value;
        }
      }

      public StoreId m_randomStoreID
      {
        get
        {
          return this.m_owner.m_randomStoreID;
        }
        set
        {
          this.m_owner.m_randomStoreID = value;
        }
      }

      public bool m_isStaticBox
      {
        get
        {
          return this.m_owner.m_isStaticBox;
        }
        set
        {
          this.m_owner.m_isStaticBox = value;
        }
      }

      public Vector3 m_scrollViewStoreContentOriginalLocalPos
      {
        get
        {
          return this.m_owner.m_scrollViewStoreContentOriginalLocalPos;
        }
        set
        {
          this.m_owner.m_scrollViewStoreContentOriginalLocalPos = value;
        }
      }

      public Vector3 m_scrollViewHeroContentOriginalLocalPos
      {
        get
        {
          return this.m_owner.m_scrollViewHeroContentOriginalLocalPos;
        }
        set
        {
          this.m_owner.m_scrollViewHeroContentOriginalLocalPos = value;
        }
      }

      public GiftStoreItem m_giftStoreItem
      {
        get
        {
          return this.m_owner.m_giftStoreItem;
        }
        set
        {
          this.m_owner.m_giftStoreItem = value;
        }
      }

      public GameObjectPool<StoreItemUIController> m_storeUnderItemPool
      {
        get
        {
          return this.m_owner.m_storeUnderItemPool;
        }
        set
        {
          this.m_owner.m_storeUnderItemPool = value;
        }
      }

      public GameObjectPool<StoreHeroSkinItemUIController> m_storeHeroSkinItemPool
      {
        get
        {
          return this.m_owner.m_storeHeroSkinItemPool;
        }
        set
        {
          this.m_owner.m_storeHeroSkinItemPool = value;
        }
      }

      public GameObjectPool<StoreSoldierSkinItemUIController> m_storeSoldierSkinItemPool
      {
        get
        {
          return this.m_owner.m_storeSoldierSkinItemPool;
        }
        set
        {
          this.m_owner.m_storeSoldierSkinItemPool = value;
        }
      }

      public List<Goods> m_gainGoodsList
      {
        get
        {
          return this.m_owner.m_gainGoodsList;
        }
        set
        {
          this.m_owner.m_gainGoodsList = value;
        }
      }

      public Dictionary<StoreId, List<Toggle>> m_storeSubType2ToggleDic
      {
        get
        {
          return this.m_owner.m_storeSubType2ToggleDic;
        }
        set
        {
          this.m_owner.m_storeSubType2ToggleDic = value;
        }
      }

      public StoreSelfChooseUIController m_storeSelfChooseUIController
      {
        get
        {
          return this.m_owner.m_storeSelfChooseUIController;
        }
        set
        {
          this.m_owner.m_storeSelfChooseUIController = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void UpdateRechargeStore()
      {
        this.m_owner.UpdateRechargeStore();
      }

      public void UpdateGiftStore(StoreId storeId)
      {
        this.m_owner.UpdateGiftStore(storeId);
      }

      public void UpdateFixedStore(StoreId storeId)
      {
        this.m_owner.UpdateFixedStore(storeId);
      }

      public void UpdateRandomStore()
      {
        this.m_owner.UpdateRandomStore();
      }

      public void ClearPool()
      {
        this.m_owner.ClearPool();
      }

      public void UpdateFixedStoreInfo(StoreId storeID)
      {
        this.m_owner.UpdateFixedStoreInfo(storeID);
      }

      public void UpdateHeroSkinInfo(
        StoreId storeID,
        GameObjectPool<StoreHeroSkinItemUIController> pool)
      {
        this.m_owner.UpdateHeroSkinInfo(storeID, pool);
      }

      public void UpdateSoldierSkinInfo(
        StoreId storeID,
        GameObjectPool<StoreSoldierSkinItemUIController> pool)
      {
        this.m_owner.UpdateSoldierSkinInfo(storeID, pool);
      }

      public void UpdateRandomStoreInfo(StoreId storeId)
      {
        this.m_owner.UpdateRandomStoreInfo(storeId);
      }

      public void UpdateRechargeStoreInfo()
      {
        this.m_owner.UpdateRechargeStoreInfo();
      }

      public void SetDefaultState(bool hideRefreshPanel)
      {
        this.m_owner.SetDefaultState(hideRefreshPanel);
      }

      public void SetFixedStoreInfo(
        StoreId fixedStoreID,
        GameObjectPool<StoreItemUIController> pool)
      {
        this.m_owner.SetFixedStoreInfo(fixedStoreID, pool);
      }

      public int SortFixedStoreItemByUISort(FixedStoreItem a, FixedStoreItem b)
      {
        return this.m_owner.SortFixedStoreItemByUISort(a, b);
      }

      public void RefreshCurrencyDisplay(List<StoreItemUIController> currencyTypeList)
      {
        this.m_owner.RefreshCurrencyDisplay(currencyTypeList);
      }

      public void SetRandomStoreInfo(StoreId storeId, GameObjectPool<StoreItemUIController> pool)
      {
        this.m_owner.SetRandomStoreInfo(storeId, pool);
      }

      public void SetRechargeStoreInfo(GameObjectPool<StoreItemUIController> pool)
      {
        this.m_owner.SetRechargeStoreInfo(pool);
      }

      public void ShowRefreshPanel()
      {
        this.m_owner.ShowRefreshPanel();
      }

      public void HideRefreshPanel()
      {
        this.m_owner.HideRefreshPanel();
      }

      public void SetRefreshPanel(StoreId randomStoreID)
      {
        this.m_owner.SetRefreshPanel(randomStoreID);
      }

      public void OnStoreItemClick(StoreItemUIController goCtrl)
      {
        this.m_owner.OnStoreItemClick(goCtrl);
      }

      public void OnHeroSkinItemClick(StoreHeroSkinItemUIController itemCtrl)
      {
        this.m_owner.OnHeroSkinItemClick(itemCtrl);
      }

      public void OnSkinItemOutOfDate()
      {
        this.m_owner.OnSkinItemOutOfDate();
      }

      public void OnSoldierSkinItemClick(StoreSoldierSkinItemUIController itemCtrl)
      {
        this.m_owner.OnSoldierSkinItemClick(itemCtrl);
      }

      public void SetStoreBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetStoreBuyPanel(goCtrl);
      }

      public void SetNormalItemBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetNormalItemBuyPanel(goCtrl);
      }

      public void SetRechargeBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetRechargeBuyPanel(goCtrl);
      }

      public void SetBoxBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetBoxBuyPanel(goCtrl);
      }

      public void SetItemBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetItemBuyPanel(goCtrl);
      }

      public void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
      {
        this.m_owner.SetEnchantStoneResonanceInfoPanel(goodsType, goodsID);
      }

      public void UpdateGiftStoreInfo(StoreId storeID, GameObjectPool<StoreItemUIController> pool)
      {
        this.m_owner.UpdateGiftStoreInfo(storeID, pool);
      }

      public void SetGiftStoreInfo(StoreId storeId, GameObjectPool<StoreItemUIController> pool)
      {
        this.m_owner.SetGiftStoreInfo(storeId, pool);
      }

      public void SetGiftStoreBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetGiftStoreBuyPanel(goCtrl);
      }

      public void SetGiftStoreStaticBoxBuyPanel(
        StoreItemUIController ctrl,
        ConfigDataItemInfo itemInfo)
      {
        this.m_owner.SetGiftStoreStaticBoxBuyPanel(ctrl, itemInfo);
      }

      public void SetMothCardBuyPanel(StoreItemUIController goCtrl)
      {
        this.m_owner.SetMothCardBuyPanel(goCtrl);
      }

      public void OnPresentPresentToggleValueChanged(bool isOn)
      {
        this.m_owner.OnPresentPresentToggleValueChanged(isOn);
      }

      public void OnPresentPreogativeToggleValueChanged(bool isOn)
      {
        this.m_owner.OnPresentPreogativeToggleValueChanged(isOn);
      }

      public void OnMonthCardBuyButtonClick()
      {
        this.m_owner.OnMonthCardBuyButtonClick();
      }

      public void OnSubscribePrivateClick()
      {
        this.m_owner.OnSubscribePrivateClick();
      }

      public void OnCancelSubscribeClick()
      {
        this.m_owner.OnCancelSubscribeClick();
      }

      public void OnMonthCardPanelBGButtonClick()
      {
        this.m_owner.OnMonthCardPanelBGButtonClick();
      }

      public void OnPresentToggleValueChanged(bool isOn)
      {
        this.m_owner.OnPresentToggleValueChanged(isOn);
      }

      public void SetGiftStoreBuyTimesLimit(GiftStoreItem item)
      {
        this.m_owner.SetGiftStoreBuyTimesLimit(item);
      }

      public void OnCloseButtonClick()
      {
        this.m_owner.OnCloseButtonClick();
      }

      public void OnRefreshButtonClick()
      {
        this.m_owner.OnRefreshButtonClick();
      }

      public void OnItemBuyPanelBGButtonClick()
      {
        this.m_owner.OnItemBuyPanelBGButtonClick();
      }

      public void OnItemBuyButtonClick()
      {
        this.m_owner.OnItemBuyButtonClick();
      }

      public void OnBoxBuyPanelBGButtonClick()
      {
        this.m_owner.OnBoxBuyPanelBGButtonClick();
      }

      public void OnRechargePanelBGButtonClick()
      {
        this.m_owner.OnRechargePanelBGButtonClick();
      }

      public void OnBoxBuyButtonClick()
      {
        this.m_owner.OnBoxBuyButtonClick();
      }

      public bool IsStoreItemIOSSubscribeItem(StoreItemUIController ctrl)
      {
        return this.m_owner.IsStoreItemIOSSubscribeItem(ctrl);
      }

      public int SortFragmentItemComparer(HeroFragmentBagItem f1, HeroFragmentBagItem f2)
      {
        return this.m_owner.SortFragmentItemComparer(f1, f2);
      }

      public void OnMemoryExtractionPanelExtractionButtonClick()
      {
        this.m_owner.OnMemoryExtractionPanelExtractionButtonClick();
      }

      public void OnMemeryExtractionHelpButtonClick()
      {
        this.m_owner.OnMemeryExtractionHelpButtonClick();
      }

      public void CloseMemoryExtractionPanel()
      {
        this.m_owner.CloseMemoryExtractionPanel();
      }

      public void FireChangeStoreEvent(StoreId storeId)
      {
        this.m_owner.FireChangeStoreEvent(storeId);
      }

      public void OnRechargeBuyButtonClick()
      {
        this.m_owner.OnRechargeBuyButtonClick();
      }

      public void OnRechargeToggleValueChanged(bool isOn)
      {
        this.m_owner.OnRechargeToggleValueChanged(isOn);
      }

      public void OnCystalToggleValueChanged(bool isOn)
      {
        this.m_owner.OnCystalToggleValueChanged(isOn);
      }

      public void OnSkinToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSkinToggleValueChanged(isOn);
      }

      public void OnSkinTab_HeroToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSkinTab_HeroToggleValueChanged(isOn);
      }

      public void OnSkinTab_SoldierToggleValueChanged(bool isOn)
      {
        this.m_owner.OnSkinTab_SoldierToggleValueChanged(isOn);
      }

      public void OnMysteriousToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousToggleValueChanged(isOn);
      }

      public void OnMysteriousBlackMarketToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousBlackMarketToggleValueChanged(isOn);
      }

      public void OnMysteriousMemoryToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousMemoryToggleValueChanged(isOn);
      }

      public void OnMysteriousEquipmentToggleValueChange(bool isOn)
      {
        this.m_owner.OnMysteriousEquipmentToggleValueChange(isOn);
      }

      public void OnMysteriousHonorToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousHonorToggleValueChanged(isOn);
      }

      public void OnMysteriousMedalToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousMedalToggleValueChanged(isOn);
      }

      public void OnMysteriousFriendshipToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousFriendshipToggleValueChanged(isOn);
      }

      public void OnMysteriousUnionToggleValueChanged(bool isOn)
      {
        this.m_owner.OnMysteriousUnionToggleValueChanged(isOn);
      }

      public void InitStoreSubType2Toggle()
      {
        this.m_owner.InitStoreSubType2Toggle();
      }

      public void SaveOriginalScrollViewLocalPosition()
      {
        this.m_owner.SaveOriginalScrollViewLocalPosition();
      }

      public bool UpdateToggles(StoreId storeId)
      {
        return this.m_owner.UpdateToggles(storeId);
      }

      public int FixedStoreSkinListSortFunc(FixedStoreItem itemA, FixedStoreItem itemB)
      {
        return this.m_owner.FixedStoreSkinListSortFunc(itemA, itemB);
      }
    }
  }
}
