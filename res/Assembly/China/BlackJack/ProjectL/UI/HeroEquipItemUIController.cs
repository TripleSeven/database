﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroEquipItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroEquipItemUIController : UIControllerBase
  {
    [AutoBind("./Content/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconBg;
    [AutoBind("./Content/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_iconSSREffect;
    [AutoBind("./Content/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImg;
    [AutoBind("./Content/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_iconButton;
    [AutoBind("./Content/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Content/LV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvText;
    [AutoBind("./Content/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starGroupg;
    [AutoBind("./RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMarkGo;
    [AutoBind("./NoEquip/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    [AutoBind("./RuneIcon", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_runeIconStateCtrl;
    [AutoBind("./RuneIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_runeIconButton;
    [AutoBind("./RuneIcon/Active", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_runeActiveIcon;
    [AutoBind("./RuneIcon/Unactive", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_runeInactiveIcon;
    public int Slot;
    public EquipmentBagItem Equipment;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroEquipmentItem(EquipmentBagItem equipment, int slot, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ShowSelectImage(bool isShow)
    {
      this.m_selectImage.SetActive(isShow);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnIconClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRuneIconButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroEquipItemUIController> EventOnAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroEquipItemUIController> EventOnIconClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroEquipItemUIController> EventOnRuneIconButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
