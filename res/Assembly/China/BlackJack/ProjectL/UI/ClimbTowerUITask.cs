﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClimbTowerUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ClimbTowerUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ClimbTowerUIController m_climbTowerUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private int m_nowSeconds;
    private ClimbTowerLevelInfoUITask m_climbTowerLevelInfoUITask;
    private ClimbTowerRaidUITask m_climbTowerRaidUITask;
    private RandomNumber m_armyRandomNumber;
    private DateTime m_nextCheckClimbTowerFlushTime;
    private bool m_isRaidClear;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClimbTowerUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool OnStart(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnStart(intent);
    }

    protected override bool OnResume(UIIntent intent)
    {
      this.InitDataFromUIIntent(intent);
      return base.OnResume(intent);
    }

    private void InitDataFromUIIntent(UIIntent intent)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectFloorAssets(int fromFloorId)
    {
      // ISSUE: unable to decompile the method.
    }

    private static int ComputeBigFloorFromFloorId(int finishedFloorId)
    {
      return finishedFloorId / 5 * 5 + 1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void InitAllUIControllers()
    {
      base.InitAllUIControllers();
      this.InitClimbTowerUIController();
    }

    protected override void ClearAllContextAndRes()
    {
      base.ClearAllContextAndRes();
      this.UninitClimbTowerUIController();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitClimbTowerUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitClimbTowerUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFloors(int fromFloorId, int finishedFloorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateClimbTowerFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerLevelInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerRaidUITask(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void ClimbTowerUIController_OnShowHelp()
    {
      CommonUIController.Instance.ShowExplanation(ExplanationId.ExplanationId_ClimbTower);
    }

    private void ClimbTowerUIController_OnShowLevelInfo()
    {
      this.StartClimbTowerLevelInfoUITask();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerUIController_OnShowRaid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerUIController_OnConfirmRaid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushClimbTower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerLevelInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerRaidUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
