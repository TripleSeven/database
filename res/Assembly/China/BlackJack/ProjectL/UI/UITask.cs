﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class UITask : UITaskBase
  {
    protected List<string> m_assets;
    private int m_loadingCustomResCorutineCount;
    protected bool m_isOpeningUI;
    protected bool m_isNeedRegisterPlayerContextEvents;
    protected bool m_isPlayerContextEventsRegistered;
    [DoNotToLua]
    private UITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_OnPause_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnNewIntentUIIntent_hotfix;
    private LuaFunction m_EnableUIInputBooleanNullable`1_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_CollectAllDynamicResForLoad_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_PostUpdateView_hotfix;
    private LuaFunction m_CollectPreloadResourceList_hotfix;
    private LuaFunction m_ClearAssetList_hotfix;
    private LuaFunction m_CollectAssetString_hotfix;
    private LuaFunction m_CollectSpriteAssetString_hotfix;
    private LuaFunction m_CollectFxAssetString_hotfix;
    private LuaFunction m_StartLoadCustomResList`1Action_hotfix;
    private LuaFunction m_StartLoadCustomAssetsAction_hotfix;
    private LuaFunction m_IsLoadingCustomAssets_hotfix;
    private LuaFunction m_IsPipeLineRunning_hotfix;
    private LuaFunction m_IsOpeningUI_hotfix;
    private LuaFunction m_ReturnPrevUITask_hotfix;
    private LuaFunction m_ClearUnusedDynamicResourceCache_hotfix;
    private LuaFunction m_ClearDynamicResourceCacheList`1_hotfix;
    private LuaFunction m_OnMemoryWarning_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public UITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void EnableUIInput(bool isEnable, bool? isGlobalEnable = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectPreloadResourceList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearAssetList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectSpriteAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectFxAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLoadCustomRes(List<string> resPathList, Action onLoadCompleted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartLoadCustomAssets(Action onLoadCompleted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsLoadingCustomAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPipeLineRunning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsOpeningUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UITaskBase ReturnPrevUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearUnusedDynamicResourceCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearDynamicResourceCache(List<string> assets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public UITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_InitlizeBeforeManagerStartIt()
    {
      this.InitlizeBeforeManagerStartIt();
    }

    private void __callBase_PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
      this.PrepareForStartOrResume(intent, onPrepareEnd);
    }

    private bool __callBase_OnStart(object param)
    {
      return this.OnStart(param);
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private UITaskPipeLineCtx __callBase_GetPipeLineCtx()
    {
      return this.GetPipeLineCtx();
    }

    private UITaskPipeLineCtx __callBase_CreatePipeLineCtx()
    {
      return this.CreatePipeLineCtx();
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      base.OnPause();
    }

    private bool __callBase_OnResume(object param)
    {
      return this.OnResume(param);
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return base.OnNewIntent(intent);
    }

    private bool __callBase_StartUpdatePipeLine(
      UIIntent intent,
      bool onlyUpdateView,
      bool canBeSkip,
      bool enableQueue)
    {
      return this.StartUpdatePipeLine(intent, onlyUpdateView, canBeSkip, enableQueue);
    }

    private bool __callBase_NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
    {
      return this.NeedSkipUpdatePipeLine(intent, onlyUpdateView);
    }

    private bool __callBase_IsNeedUpdateDataCache()
    {
      return this.IsNeedUpdateDataCache();
    }

    private void __callBase_UpdateDataCache()
    {
      this.UpdateDataCache();
    }

    private bool __callBase_IsNeedLoadStaticRes()
    {
      return this.IsNeedLoadStaticRes();
    }

    private void __callBase_StartLoadStaticRes()
    {
      this.StartLoadStaticRes();
    }

    private void __callBase_CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
    {
      this.CheckLayerDescArray(layerDescArray);
    }

    private void __callBase_OnLoadStaticResCompleted()
    {
      this.OnLoadStaticResCompleted();
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private void __callBase_StartLoadDynamicRes()
    {
      this.StartLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return base.CollectAllDynamicResForLoad();
    }

    private List<UITaskBase.LayerDesc> __callBase_CollectAllStaticResDescForLoad()
    {
      return this.CollectAllStaticResDescForLoad();
    }

    private HashSet<string> __callBase_CalculateDynamicResReallyNeedLoad(
      List<string> resPathList)
    {
      return this.CalculateDynamicResReallyNeedLoad(resPathList);
    }

    private void __callBase_OnLoadDynamicResCompleted()
    {
      this.OnLoadDynamicResCompleted();
    }

    private void __callBase_RedirectPipLineOnAllResReady(Action callBack)
    {
      this.RedirectPipLineOnAllResReady(callBack);
    }

    private void __callBase_OnLoadAllResCompleted()
    {
      this.OnLoadAllResCompleted();
    }

    private void __callBase_ReturnFromRedirectPipLineOnLoadAllResCompleted()
    {
      this.ReturnFromRedirectPipLineOnLoadAllResCompleted();
    }

    private void __callBase_InitLayerStateOnLoadAllResCompleted()
    {
      this.InitLayerStateOnLoadAllResCompleted();
    }

    private void __callBase_InitAllUIControllers()
    {
      this.InitAllUIControllers();
    }

    private void __callBase_PostOnLoadAllResCompleted()
    {
      this.PostOnLoadAllResCompleted();
    }

    private bool __callBase_IsLoadAllResCompleted()
    {
      return this.IsLoadAllResCompleted();
    }

    private void __callBase_StartUpdateView()
    {
      this.StartUpdateView();
    }

    private void __callBase_UpdateView()
    {
      this.UpdateView();
    }

    private void __callBase_RegUpdateViewPlayingEffect(
      string name,
      int timeout,
      Action<string> onTimeOut)
    {
      this.RegUpdateViewPlayingEffect(name, timeout, onTimeOut);
    }

    private void __callBase_UnregUpdateViewPlayingEffect(string name, bool isTimeOut)
    {
      this.UnregUpdateViewPlayingEffect(name, isTimeOut);
    }

    private void __callBase_PostUpdateView()
    {
      base.PostUpdateView();
    }

    private void __callBase_PostUpdateViewBeforeClearContext()
    {
      this.PostUpdateViewBeforeClearContext();
    }

    private void __callBase_HideAllView()
    {
      this.HideAllView();
    }

    private void __callBase_ClearAllContextAndRes()
    {
      this.ClearAllContextAndRes();
    }

    private void __callBase_SaveContextInIntentOnPause()
    {
      this.SaveContextInIntentOnPause();
    }

    private void __callBase_ClearContextOnPause()
    {
      this.ClearContextOnPause();
    }

    private void __callBase_ClearContextOnIntentChange(UIIntent newIntent)
    {
      this.ClearContextOnIntentChange(newIntent);
    }

    private void __callBase_ClearContextOnUpdateViewEnd()
    {
      this.ClearContextOnUpdateViewEnd();
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      base.EnableUIInput(isEnable, isGlobalEnable);
    }

    private UITaskBase.LayerDesc __callBase_GetLayerDescByName(string name)
    {
      return this.GetLayerDescByName(name);
    }

    private SceneLayerBase __callBase_GetLayerByName(string name)
    {
      return this.GetLayerByName(name);
    }

    private void __callBase_RegisterModesDefine(string defaultMode, string[] modes)
    {
      this.RegisterModesDefine(defaultMode, modes);
    }

    private bool __callBase_SetCurrentMode(string mode)
    {
      return this.SetCurrentMode(mode);
    }

    private void __callBase_SetIsNeedPauseTimeOut(bool isNeed)
    {
      this.SetIsNeedPauseTimeOut(isNeed);
    }

    private void __callBase_OnTick()
    {
      this.OnTick();
    }

    private void __callBase_PostDelayTimeExecuteAction(Action action, float delaySeconds)
    {
      this.PostDelayTimeExecuteAction(action, delaySeconds);
    }

    private void __callBase_PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
    {
      this.PostDelayTicksExecuteAction(action, delayTickCount);
    }

    private void __callBase_SetTag(string tag)
    {
      this.SetTag(tag);
    }

    private bool __callBase_HasTag(string tag)
    {
      return this.HasTag(tag);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private UITask m_owner;

      public LuaExportHelper(UITask owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_InitlizeBeforeManagerStartIt()
      {
        this.m_owner.__callBase_InitlizeBeforeManagerStartIt();
      }

      public void __callBase_PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
      {
        this.m_owner.__callBase_PrepareForStartOrResume(intent, onPrepareEnd);
      }

      public bool __callBase_OnStart(object param)
      {
        return this.m_owner.__callBase_OnStart(param);
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public UITaskPipeLineCtx __callBase_GetPipeLineCtx()
      {
        return this.m_owner.__callBase_GetPipeLineCtx();
      }

      public UITaskPipeLineCtx __callBase_CreatePipeLineCtx()
      {
        return this.m_owner.__callBase_CreatePipeLineCtx();
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(object param)
      {
        return this.m_owner.__callBase_OnResume(param);
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool __callBase_StartUpdatePipeLine(
        UIIntent intent,
        bool onlyUpdateView,
        bool canBeSkip,
        bool enableQueue)
      {
        // ISSUE: unable to decompile the method.
      }

      public bool __callBase_NeedSkipUpdatePipeLine(UIIntent intent, bool onlyUpdateView)
      {
        return this.m_owner.__callBase_NeedSkipUpdatePipeLine(intent, onlyUpdateView);
      }

      public bool __callBase_IsNeedUpdateDataCache()
      {
        return this.m_owner.__callBase_IsNeedUpdateDataCache();
      }

      public void __callBase_UpdateDataCache()
      {
        this.m_owner.__callBase_UpdateDataCache();
      }

      public bool __callBase_IsNeedLoadStaticRes()
      {
        return this.m_owner.__callBase_IsNeedLoadStaticRes();
      }

      public void __callBase_StartLoadStaticRes()
      {
        this.m_owner.__callBase_StartLoadStaticRes();
      }

      public void __callBase_CheckLayerDescArray(List<UITaskBase.LayerDesc> layerDescArray)
      {
        this.m_owner.__callBase_CheckLayerDescArray(layerDescArray);
      }

      public void __callBase_OnLoadStaticResCompleted()
      {
        this.m_owner.__callBase_OnLoadStaticResCompleted();
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public void __callBase_StartLoadDynamicRes()
      {
        this.m_owner.__callBase_StartLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public List<UITaskBase.LayerDesc> __callBase_CollectAllStaticResDescForLoad()
      {
        return this.m_owner.__callBase_CollectAllStaticResDescForLoad();
      }

      public HashSet<string> __callBase_CalculateDynamicResReallyNeedLoad(
        List<string> resPathList)
      {
        return this.m_owner.__callBase_CalculateDynamicResReallyNeedLoad(resPathList);
      }

      public void __callBase_OnLoadDynamicResCompleted()
      {
        this.m_owner.__callBase_OnLoadDynamicResCompleted();
      }

      public void __callBase_RedirectPipLineOnAllResReady(Action callBack)
      {
        this.m_owner.__callBase_RedirectPipLineOnAllResReady(callBack);
      }

      public void __callBase_OnLoadAllResCompleted()
      {
        this.m_owner.__callBase_OnLoadAllResCompleted();
      }

      public void __callBase_ReturnFromRedirectPipLineOnLoadAllResCompleted()
      {
        this.m_owner.__callBase_ReturnFromRedirectPipLineOnLoadAllResCompleted();
      }

      public void __callBase_InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.__callBase_InitLayerStateOnLoadAllResCompleted();
      }

      public void __callBase_InitAllUIControllers()
      {
        this.m_owner.__callBase_InitAllUIControllers();
      }

      public void __callBase_PostOnLoadAllResCompleted()
      {
        this.m_owner.__callBase_PostOnLoadAllResCompleted();
      }

      public bool __callBase_IsLoadAllResCompleted()
      {
        return this.m_owner.__callBase_IsLoadAllResCompleted();
      }

      public void __callBase_StartUpdateView()
      {
        this.m_owner.__callBase_StartUpdateView();
      }

      public void __callBase_UpdateView()
      {
        this.m_owner.__callBase_UpdateView();
      }

      public void __callBase_RegUpdateViewPlayingEffect(
        string name,
        int timeout,
        Action<string> onTimeOut)
      {
        this.m_owner.__callBase_RegUpdateViewPlayingEffect(name, timeout, onTimeOut);
      }

      public void __callBase_UnregUpdateViewPlayingEffect(string name, bool isTimeOut)
      {
        this.m_owner.__callBase_UnregUpdateViewPlayingEffect(name, isTimeOut);
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_PostUpdateViewBeforeClearContext()
      {
        this.m_owner.__callBase_PostUpdateViewBeforeClearContext();
      }

      public void __callBase_HideAllView()
      {
        this.m_owner.__callBase_HideAllView();
      }

      public void __callBase_ClearAllContextAndRes()
      {
        this.m_owner.__callBase_ClearAllContextAndRes();
      }

      public void __callBase_SaveContextInIntentOnPause()
      {
        this.m_owner.__callBase_SaveContextInIntentOnPause();
      }

      public void __callBase_ClearContextOnPause()
      {
        this.m_owner.__callBase_ClearContextOnPause();
      }

      public void __callBase_ClearContextOnIntentChange(UIIntent newIntent)
      {
        this.m_owner.__callBase_ClearContextOnIntentChange(newIntent);
      }

      public void __callBase_ClearContextOnUpdateViewEnd()
      {
        this.m_owner.__callBase_ClearContextOnUpdateViewEnd();
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public UITaskBase.LayerDesc __callBase_GetLayerDescByName(string name)
      {
        return this.m_owner.__callBase_GetLayerDescByName(name);
      }

      public SceneLayerBase __callBase_GetLayerByName(string name)
      {
        return this.m_owner.__callBase_GetLayerByName(name);
      }

      public void __callBase_RegisterModesDefine(string defaultMode, string[] modes)
      {
        this.m_owner.__callBase_RegisterModesDefine(defaultMode, modes);
      }

      public bool __callBase_SetCurrentMode(string mode)
      {
        return this.m_owner.__callBase_SetCurrentMode(mode);
      }

      public void __callBase_SetIsNeedPauseTimeOut(bool isNeed)
      {
        this.m_owner.__callBase_SetIsNeedPauseTimeOut(isNeed);
      }

      public void __callBase_OnTick()
      {
        this.m_owner.__callBase_OnTick();
      }

      public void __callBase_PostDelayTimeExecuteAction(Action action, float delaySeconds)
      {
        this.m_owner.__callBase_PostDelayTimeExecuteAction(action, delaySeconds);
      }

      public void __callBase_PostDelayTicksExecuteAction(Action action, ulong delayTickCount)
      {
        this.m_owner.__callBase_PostDelayTicksExecuteAction(action, delayTickCount);
      }

      public void __callBase_SetTag(string tag)
      {
        this.m_owner.__callBase_SetTag(tag);
      }

      public bool __callBase_HasTag(string tag)
      {
        return this.m_owner.__callBase_HasTag(tag);
      }

      public List<string> m_assets
      {
        get
        {
          return this.m_owner.m_assets;
        }
        set
        {
          this.m_owner.m_assets = value;
        }
      }

      public int m_loadingCustomResCorutineCount
      {
        get
        {
          return this.m_owner.m_loadingCustomResCorutineCount;
        }
        set
        {
          this.m_owner.m_loadingCustomResCorutineCount = value;
        }
      }

      public bool m_isOpeningUI
      {
        get
        {
          return this.m_owner.m_isOpeningUI;
        }
        set
        {
          this.m_owner.m_isOpeningUI = value;
        }
      }

      public bool m_isNeedRegisterPlayerContextEvents
      {
        get
        {
          return this.m_owner.m_isNeedRegisterPlayerContextEvents;
        }
        set
        {
          this.m_owner.m_isNeedRegisterPlayerContextEvents = value;
        }
      }

      public bool m_isPlayerContextEventsRegistered
      {
        get
        {
          return this.m_owner.m_isPlayerContextEventsRegistered;
        }
        set
        {
          this.m_owner.m_isPlayerContextEventsRegistered = value;
        }
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void OnPause()
      {
        this.m_owner.OnPause();
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public List<string> CollectAllDynamicResForLoad()
      {
        return this.m_owner.CollectAllDynamicResForLoad();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public void PostUpdateView()
      {
        this.m_owner.PostUpdateView();
      }

      public void CollectPreloadResourceList()
      {
        this.m_owner.CollectPreloadResourceList();
      }

      public void ClearAssetList()
      {
        this.m_owner.ClearAssetList();
      }

      public void CollectAsset(string assetName)
      {
        this.m_owner.CollectAsset(assetName);
      }

      public void CollectSpriteAsset(string assetName)
      {
        this.m_owner.CollectSpriteAsset(assetName);
      }

      public void CollectFxAsset(string assetName)
      {
        this.m_owner.CollectFxAsset(assetName);
      }

      public void StartLoadCustomRes(List<string> resPathList, Action onLoadCompleted)
      {
        this.m_owner.StartLoadCustomRes(resPathList, onLoadCompleted);
      }

      public void StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.StartLoadCustomAssets(onLoadCompleted);
      }

      public bool IsLoadingCustomAssets()
      {
        return this.m_owner.IsLoadingCustomAssets();
      }

      public bool IsPipeLineRunning()
      {
        return this.m_owner.IsPipeLineRunning();
      }

      public bool IsOpeningUI()
      {
        return this.m_owner.IsOpeningUI();
      }

      public UITaskBase ReturnPrevUITask()
      {
        return this.m_owner.ReturnPrevUITask();
      }

      public void ClearUnusedDynamicResourceCache()
      {
        this.m_owner.ClearUnusedDynamicResourceCache();
      }

      public void ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.ClearDynamicResourceCache(assets);
      }

      public void OnMemoryWarning()
      {
        this.m_owner.OnMemoryWarning();
      }
    }
  }
}
