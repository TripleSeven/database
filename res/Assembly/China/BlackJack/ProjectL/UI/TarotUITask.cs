﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TarotUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TarotUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private TarotUIController m_tarotUIController;
    private int m_poolID;
    [DoNotToLua]
    private TarotUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnNewIntentUIIntent_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_StopUITask_hotfix;
    private LuaFunction m_LoadResList`1Action_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TarotUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartTarotUITask(UIIntent lastUIIntent, int poolID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadRes(List<string> assetList, Action onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public TarotUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      this.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return base.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return this.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      this.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      this.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TarotUITask m_owner;

      public LuaExportHelper(TarotUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public TarotUIController m_tarotUIController
      {
        get
        {
          return this.m_owner.m_tarotUIController;
        }
        set
        {
          this.m_owner.m_tarotUIController = value;
        }
      }

      public int m_poolID
      {
        get
        {
          return this.m_owner.m_poolID;
        }
        set
        {
          this.m_owner.m_poolID = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }
    }
  }
}
