﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArchiveItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArchiveItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_ItemButton;
    [AutoBind("./SelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectImage;
    [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImage;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrEffect;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starGroup;
    [AutoBind("./DarkCoverImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_darkCoverImage;
    public EquipmentArchiveUIController.EquipmentInfoWrap m_equipmentInfoWrap;
    private const int SSRRank = 4;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetEquipmentInfo(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      this.m_equipmentInfoWrap = equipmentInfoWrap;
      this.Refresh();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool isSelected)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ArchiveItemUIController> OnItemClickEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
