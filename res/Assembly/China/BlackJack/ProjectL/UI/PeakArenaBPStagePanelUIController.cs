﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaBPStagePanelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class PeakArenaBPStagePanelUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./PlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerUIStateController;
    [AutoBind("./PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfo/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeadIconImage;
    [AutoBind("./PlayerInfo/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerHeadFrameTransform;
    [AutoBind("./PlayerInfo/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfo/OffLineImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerOfflineGameObject;
    [AutoBind("./PlayerInfo/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGroupGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject2;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText2;
    [AutoBind("./PlayerInfo/WinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_winGroupUIStateController;
    [AutoBind("./PlayerInfo/WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin1GameObject;
    [AutoBind("./PlayerInfo/WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin2GameObject;
    [AutoBind("./HeroList/MyHeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroScrollRect;
    [AutoBind("./HeroList/EnemyHeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroScrollRect2;
    [AutoBind("./HeroList/EnemyActiveDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activeDescUIStateController;
    [AutoBind("./HeroList/KickOutHero/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_banHeroGroupTransform;
    [AutoBind("./HeroList/KickOutHero", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_banHeroUIStateController;
    [AutoBind("./IntoBattleGroup/MyHeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_pickHeroGroupTransform;
    [AutoBind("./IntoBattleGroup/EnemyHeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_pickHeroGroupTransform2;
    [AutoBind("./IntoBattleGroup/MyText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pickHeroTitleGameObject;
    [AutoBind("./IntoBattleGroup/EnemyText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pickHeroTitleGameObject2;
    [AutoBind("./ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arrowGameObject;
    private CommonUIStateController[] m_waitPickHeroEffectUIStateControllers;
    private int m_pickHeroCount;
    private bool m_isBpPanelActive;
    private GameObjectPool<PeakArenaBpStageHeroItemUIController> m_heroItemPool;
    private GameObjectPool<PeakArenaBpStageHeroItemUIController> m_heroItemPool2;
    private GameObjectPool<PeakArenaBpStageHeroItemUIController> m_banHeroItemPool;
    [DoNotToLua]
    private PeakArenaBPStagePanelUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OnDisable_hotfix;
    private LuaFunction m_ActiveBpPanelBoolean_hotfix;
    private LuaFunction m_ShowBpPanelActiveArrowBoolean_hotfix;
    private LuaFunction m_SetPlayerStringInt32Int32_hotfix;
    private LuaFunction m_SetPlayerActiveBoolean_hotfix;
    private LuaFunction m_SetPlayerStatusPlayerBattleStatusBoolean_hotfix;
    private LuaFunction m_ClearHeros_hotfix;
    private LuaFunction m_AddHeroBPStageHeroInt32PeakArenaBPStageHeroItemStateBoolean_hotfix;
    private LuaFunction m_ShowPickHeroEffectsList`1_hotfix;
    private LuaFunction m_ShowWaitPickHeroEffectInt32_hotfix;
    private LuaFunction m_SetWaitPickHeroEffectInt32Boolean_hotfix;
    private LuaFunction m_ShowBanHeroEffectsList`1_hotfix;
    private LuaFunction m_HideBanHeroEffects_hotfix;
    private LuaFunction m_ShowEnemyActiveDescBPStageCommandType_hotfix;
    private LuaFunction m_HideEnemyActiveDesc_hotfix;
    private LuaFunction m_ShowBattleReportActiveDescBPStageCommandType_hotfix;
    private LuaFunction m_HideBattleReportActiveDesc_hotfix;
    private LuaFunction m_SetPickHeroTitleBoolean_hotfix;
    private LuaFunction m_ShowCountdownBoolean_hotfix;
    private LuaFunction m_SetCountdownTimeSpanTimeSpan_hotfix;
    private LuaFunction m_ShowWinCountInt32Boolean_hotfix;
    private LuaFunction m_PeakArenaBanPickHeroItemUIController_OnClickPeakArenaBpStageHeroItemUIController_hotfix;
    private LuaFunction m_add_EventOnHeroItemClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnHeroItemClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBPStagePanelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActiveBpPanel(bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpPanelActiveArrow(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerStatus(PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(
      BPStageHero hero,
      int actorId,
      PeakArenaBPStageHeroItemState state,
      bool showJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPickHeroEffects(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitPickHeroEffect(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetWaitPickHeroEffect(int idx, bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanHeroEffects(List<BPStageHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBanHeroEffects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnemyActiveDesc(BPStageCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideEnemyActiveDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportActiveDesc(BPStageCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleReportActiveDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPickHeroTitle(bool isEnemy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCountdown(TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWinCount(int winCount, bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBanPickHeroItemUIController_OnClick(
      PeakArenaBpStageHeroItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PeakArenaBpStageHeroItemUIController> EventOnHeroItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PeakArenaBPStagePanelUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHeroItemClick(PeakArenaBpStageHeroItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHeroItemClick(PeakArenaBpStageHeroItemUIController obj)
    {
      this.EventOnHeroItemClick = (Action<PeakArenaBpStageHeroItemUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PeakArenaBPStagePanelUIController m_owner;

      public LuaExportHelper(PeakArenaBPStagePanelUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnHeroItemClick(PeakArenaBpStageHeroItemUIController obj)
      {
        this.m_owner.__callDele_EventOnHeroItemClick(obj);
      }

      public void __clearDele_EventOnHeroItemClick(PeakArenaBpStageHeroItemUIController obj)
      {
        this.m_owner.__clearDele_EventOnHeroItemClick(obj);
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public CommonUIStateController m_playerUIStateController
      {
        get
        {
          return this.m_owner.m_playerUIStateController;
        }
        set
        {
          this.m_owner.m_playerUIStateController = value;
        }
      }

      public Text m_playerNameText
      {
        get
        {
          return this.m_owner.m_playerNameText;
        }
        set
        {
          this.m_owner.m_playerNameText = value;
        }
      }

      public Image m_playerHeadIconImage
      {
        get
        {
          return this.m_owner.m_playerHeadIconImage;
        }
        set
        {
          this.m_owner.m_playerHeadIconImage = value;
        }
      }

      public Transform m_playerHeadFrameTransform
      {
        get
        {
          return this.m_owner.m_playerHeadFrameTransform;
        }
        set
        {
          this.m_owner.m_playerHeadFrameTransform = value;
        }
      }

      public Text m_playerLevelText
      {
        get
        {
          return this.m_owner.m_playerLevelText;
        }
        set
        {
          this.m_owner.m_playerLevelText = value;
        }
      }

      public GameObject m_playerOfflineGameObject
      {
        get
        {
          return this.m_owner.m_playerOfflineGameObject;
        }
        set
        {
          this.m_owner.m_playerOfflineGameObject = value;
        }
      }

      public GameObject m_timeGroupGameObject
      {
        get
        {
          return this.m_owner.m_timeGroupGameObject;
        }
        set
        {
          this.m_owner.m_timeGroupGameObject = value;
        }
      }

      public GameObject m_timeGameObject
      {
        get
        {
          return this.m_owner.m_timeGameObject;
        }
        set
        {
          this.m_owner.m_timeGameObject = value;
        }
      }

      public Text m_timeText
      {
        get
        {
          return this.m_owner.m_timeText;
        }
        set
        {
          this.m_owner.m_timeText = value;
        }
      }

      public GameObject m_timeGameObject2
      {
        get
        {
          return this.m_owner.m_timeGameObject2;
        }
        set
        {
          this.m_owner.m_timeGameObject2 = value;
        }
      }

      public Text m_timeText2
      {
        get
        {
          return this.m_owner.m_timeText2;
        }
        set
        {
          this.m_owner.m_timeText2 = value;
        }
      }

      public CommonUIStateController m_winGroupUIStateController
      {
        get
        {
          return this.m_owner.m_winGroupUIStateController;
        }
        set
        {
          this.m_owner.m_winGroupUIStateController = value;
        }
      }

      public GameObject m_winGroupWin1GameObject
      {
        get
        {
          return this.m_owner.m_winGroupWin1GameObject;
        }
        set
        {
          this.m_owner.m_winGroupWin1GameObject = value;
        }
      }

      public GameObject m_winGroupWin2GameObject
      {
        get
        {
          return this.m_owner.m_winGroupWin2GameObject;
        }
        set
        {
          this.m_owner.m_winGroupWin2GameObject = value;
        }
      }

      public ScrollRect m_heroScrollRect
      {
        get
        {
          return this.m_owner.m_heroScrollRect;
        }
        set
        {
          this.m_owner.m_heroScrollRect = value;
        }
      }

      public ScrollRect m_heroScrollRect2
      {
        get
        {
          return this.m_owner.m_heroScrollRect2;
        }
        set
        {
          this.m_owner.m_heroScrollRect2 = value;
        }
      }

      public CommonUIStateController m_activeDescUIStateController
      {
        get
        {
          return this.m_owner.m_activeDescUIStateController;
        }
        set
        {
          this.m_owner.m_activeDescUIStateController = value;
        }
      }

      public Transform m_banHeroGroupTransform
      {
        get
        {
          return this.m_owner.m_banHeroGroupTransform;
        }
        set
        {
          this.m_owner.m_banHeroGroupTransform = value;
        }
      }

      public CommonUIStateController m_banHeroUIStateController
      {
        get
        {
          return this.m_owner.m_banHeroUIStateController;
        }
        set
        {
          this.m_owner.m_banHeroUIStateController = value;
        }
      }

      public Transform m_pickHeroGroupTransform
      {
        get
        {
          return this.m_owner.m_pickHeroGroupTransform;
        }
        set
        {
          this.m_owner.m_pickHeroGroupTransform = value;
        }
      }

      public Transform m_pickHeroGroupTransform2
      {
        get
        {
          return this.m_owner.m_pickHeroGroupTransform2;
        }
        set
        {
          this.m_owner.m_pickHeroGroupTransform2 = value;
        }
      }

      public GameObject m_pickHeroTitleGameObject
      {
        get
        {
          return this.m_owner.m_pickHeroTitleGameObject;
        }
        set
        {
          this.m_owner.m_pickHeroTitleGameObject = value;
        }
      }

      public GameObject m_pickHeroTitleGameObject2
      {
        get
        {
          return this.m_owner.m_pickHeroTitleGameObject2;
        }
        set
        {
          this.m_owner.m_pickHeroTitleGameObject2 = value;
        }
      }

      public GameObject m_arrowGameObject
      {
        get
        {
          return this.m_owner.m_arrowGameObject;
        }
        set
        {
          this.m_owner.m_arrowGameObject = value;
        }
      }

      public CommonUIStateController[] m_waitPickHeroEffectUIStateControllers
      {
        get
        {
          return this.m_owner.m_waitPickHeroEffectUIStateControllers;
        }
        set
        {
          this.m_owner.m_waitPickHeroEffectUIStateControllers = value;
        }
      }

      public int m_pickHeroCount
      {
        get
        {
          return this.m_owner.m_pickHeroCount;
        }
        set
        {
          this.m_owner.m_pickHeroCount = value;
        }
      }

      public bool m_isBpPanelActive
      {
        get
        {
          return this.m_owner.m_isBpPanelActive;
        }
        set
        {
          this.m_owner.m_isBpPanelActive = value;
        }
      }

      public GameObjectPool<PeakArenaBpStageHeroItemUIController> m_heroItemPool
      {
        get
        {
          return this.m_owner.m_heroItemPool;
        }
        set
        {
          this.m_owner.m_heroItemPool = value;
        }
      }

      public GameObjectPool<PeakArenaBpStageHeroItemUIController> m_heroItemPool2
      {
        get
        {
          return this.m_owner.m_heroItemPool2;
        }
        set
        {
          this.m_owner.m_heroItemPool2 = value;
        }
      }

      public GameObjectPool<PeakArenaBpStageHeroItemUIController> m_banHeroItemPool
      {
        get
        {
          return this.m_owner.m_banHeroItemPool;
        }
        set
        {
          this.m_owner.m_banHeroItemPool = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnDisable()
      {
        this.m_owner.OnDisable();
      }

      public void SetWaitPickHeroEffect(int idx, bool enable)
      {
        this.m_owner.SetWaitPickHeroEffect(idx, enable);
      }

      public void PeakArenaBanPickHeroItemUIController_OnClick(
        PeakArenaBpStageHeroItemUIController ctrl)
      {
        this.m_owner.PeakArenaBanPickHeroItemUIController_OnClick(ctrl);
      }
    }
  }
}
