﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GiftStoreCancelBuyNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class GiftStoreCancelBuyNetTask : UINetTask
  {
    private int m_giftStoreItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreCancelBuyNetTask(int giftStoreItemid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnGiftStoreCancelBuyStoreItemAck(int result, int goodsId)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
