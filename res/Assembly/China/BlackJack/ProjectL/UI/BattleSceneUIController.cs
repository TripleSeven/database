﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleSceneUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleSceneUIController : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
  {
    [AutoBind("./ActionMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionMarkGameObject;
    [AutoBind("./ActionMark2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionMark2GameObject;
    [AutoBind("./WinCondition", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_winConditionUIStateController;
    [AutoBind("./WinCondition/Defeat", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionKillGameObject;
    [AutoBind("./WinCondition/Protect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionProtectGameObject;
    [AutoBind("./SelectionMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectionMarkGameObject;
    [AutoBind("./PassiveSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_passiveSkillUIStateController;
    [AutoBind("./PassiveSkill/Panel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_passiveSkillIconImage;
    [AutoBind("./PassiveSkill/Panel/TalentIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_passiveSkillTalentIconImage;
    [AutoBind("./PassiveSkill/Panel/EquipIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_passiveSkillEquipIconGameObject;
    [AutoBind("./PassiveSkill/Panel/EquipIconImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_passiveSkillEquipIconImage;
    [AutoBind("./PassiveSkill/Panel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_passiveSkillNameText;
    [AutoBind("./Guard", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guardUIStateController;
    [AutoBind("./ActionArrowGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_protectBanGroupUIStateController;
    [AutoBind("./ActionArrowGroup/Protect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_protectUIStateController;
    [AutoBind("./ActionArrowGroup/KickOut", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_banUIStateController;
    [AutoBind("./DamageNumbers", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumbersGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/DamageNumber", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberPrefab;
    [AutoBind("./Prefabs/DamageNumberWeak", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberWeakPrefab;
    [AutoBind("./Prefabs/DamageNumberStrong", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberStrongPrefab;
    [AutoBind("./Prefabs/DamageNumberCritical", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberCriticalPrefab;
    [AutoBind("./Prefabs/DamageNumberSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberSoldierPrefab;
    [AutoBind("./Prefabs/HealNumber", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_healNumberPrefab;
    [AutoBind("./Prefabs/Immune", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_immunePrefab;
    [AutoBind("./Prefabs/Target1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target1Prefab;
    [AutoBind("./Prefabs/Target2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target2Prefab;
    [AutoBind("./Prefabs/Target3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target3Prefab;
    [AutoBind("./Prefabs/Target4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target4Prefab;
    [AutoBind("./Prefabs/Target5", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target5Prefab;
    [AutoBind("./Prefabs/Target6", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target6Prefab;
    [AutoBind("./Prefabs/Target7", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target7Prefab;
    [AutoBind("./Prefabs/CanAction", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_canActionPrefab;
    private ClientBattle m_clientBattle;
    private GameObjectPool<DamageNumber> m_damageNumberPool;
    private GameObjectPool<DamageNumber> m_damageNumberWeakPool;
    private GameObjectPool<DamageNumber> m_damageNumberStrongPool;
    private GameObjectPool<DamageNumber> m_damageNumberCriticalPool;
    private GameObjectPool<DamageNumber> m_damageNumberSoldierPool;
    private GameObjectPool<DamageNumber> m_healNumberPool;
    private GameObjectPool<DamageNumber> m_immunePool;
    private GameObjectPool[] m_targetIconPools;
    private GameObjectPool m_canActionIconPool;
    private float m_hideTargetIconTime;
    private const int InvalidPointerID = -1000;
    private int m_downPointerId;
    private int m_clickPointerId;
    private int m_dragPointerId;
    private bool m_is3DTouchTriggered;
    [DoNotToLua]
    private BattleSceneUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitializeClientBattleBattleMapUIController_hotfix;
    private LuaFunction m_PrepareBattle_hotfix;
    private LuaFunction m_StartBattle_hotfix;
    private LuaFunction m_StopBattle_hotfix;
    private LuaFunction m_ShowBattleWinConditionConfigDataBattleWinConditionInfoGridPosition_hotfix;
    private LuaFunction m_HideBattleWinConditionConfigDataBattleWinConditionInfoGridPosition_hotfix;
    private LuaFunction m_OnActorSkillHitClientBattleActorConfigDataSkillInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnActorBuffHitClientBattleActorConfigDataBuffInfoInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnActorTerrainHitClientBattleActorInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_ShowDamageNumbersClientBattleActorInt32Int32DamageNumberType_hotfix;
    private LuaFunction m_OnActorAttachImmuneClientBattleActor_hotfix;
    private LuaFunction m_OnActorPassiveSkillClientBattleActorBuffState_hotfix;
    private LuaFunction m_OnActorGuardClientBattleActorClientBattleActor_hotfix;
    private LuaFunction m_OnActorCombatDamageClientBattleActorInt32Int32_hotfix;
    private LuaFunction m_ShowDamangeNumberClientBattleActorInt32DamageNumberTypeBoolean_hotfix;
    private LuaFunction m_SetActiveActorClientBattleActor_hotfix;
    private LuaFunction m_ShowSelectionMarkGridPosition_hotfix;
    private LuaFunction m_HideSelectionMark_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_Update3DTouch_hotfix;
    private LuaFunction m_GetActorGraphicCenterPositionClientBattleActor_hotfix;
    private LuaFunction m_ShowAttackTargetIconsList`1List`1GridPosition_hotfix;
    private LuaFunction m_ShowSkillTargetIconsList`1ConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_ShowSkillTargetIconGameObjectGridPositionConfigDataSkillInfoGridPosition_hotfix;
    private LuaFunction m_ShowAttackTargetIconGridPositionInt32Single_hotfix;
    private LuaFunction m_ShowSkillTargetIconGridPositionConfigDataSkillInfoGridPositionSingle_hotfix;
    private LuaFunction m_HideTargetIcons_hotfix;
    private LuaFunction m_AllocateAttackTargetIconInt32_hotfix;
    private LuaFunction m_AllocateSkillTargetIconConfigDataSkillInfo_hotfix;
    private LuaFunction m_ShowCanActionIconsList`1_hotfix;
    private LuaFunction m_HideCanActionIcons_hotfix;
    private LuaFunction m_ShowProtectIndicatorGridPosition_hotfix;
    private LuaFunction m_ShowBanIndicatorGridPosition_hotfix;
    private LuaFunction m_HideBanProtectIndicator_hotfix;
    private LuaFunction m_GridPositionToMapPositionGridPositionSingle_hotfix;
    private LuaFunction m_OnPointerDownPointerEventData_hotfix;
    private LuaFunction m_OnPointerUpPointerEventData_hotfix;
    private LuaFunction m_OnPointerClickPointerEventData_hotfix;
    private LuaFunction m_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_OnDragPointerEventData_hotfix;
    private LuaFunction m_add_EventOnPointerDownAction`1_hotfix;
    private LuaFunction m_remove_EventOnPointerDownAction`1_hotfix;
    private LuaFunction m_add_EventOnPointerUpAction`1_hotfix;
    private LuaFunction m_remove_EventOnPointerUpAction`1_hotfix;
    private LuaFunction m_add_EventOnPointerClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnPointerClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBeginDragAction`1_hotfix;
    private LuaFunction m_remove_EventOnBeginDragAction`1_hotfix;
    private LuaFunction m_add_EventOnEndDragAction`1_hotfix;
    private LuaFunction m_remove_EventOnEndDragAction`1_hotfix;
    private LuaFunction m_add_EventOnDragAction`1_hotfix;
    private LuaFunction m_remove_EventOnDragAction`1_hotfix;
    private LuaFunction m_add_EventOn3DTouchAction`1_hotfix;
    private LuaFunction m_remove_EventOn3DTouchAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle, BattleMapUIController battleMapUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorSkillHit(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorBuffHit(
      ClientBattleActor a,
      ConfigDataBuffInfo buffInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorTerrainHit(
      ClientBattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDamageNumbers(
      ClientBattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorAttachImmune(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorPassiveSkill(ClientBattleActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorGuard(ClientBattleActor a, ClientBattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCombatDamage(ClientBattleActor a, int heroDamage, int soldierDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDamangeNumber(
      ClientBattleActor a,
      int hpModify,
      DamageNumberType damageNumberType,
      bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActiveActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectionMark(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSelectionMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update3DTouch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GetActorGraphicCenterPosition(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAttackTargetIcons(
      List<GridPosition> positions,
      List<int> armyRelationValues,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkillTargetIcons(
      List<GridPosition> positions,
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargetIcon(
      GameObject go,
      GridPosition pos,
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAttackTargetIcon(GridPosition pos, int armyRelationValue, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkillTargetIcon(
      GridPosition pos,
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos,
      float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideTargetIcons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject AllocateAttackTargetIcon(int armyRelationValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject AllocateSkillTargetIcon(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCanActionIcons(List<GridPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideCanActionIcons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowProtectIndicator(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanIndicator(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBanProtectIndicator()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToMapPosition(GridPosition p, float z)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PointerEventData> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Vector2> EventOn3DTouch
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleSceneUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerDown(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerDown(PointerEventData obj)
    {
      this.EventOnPointerDown = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerUp(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerUp(PointerEventData obj)
    {
      this.EventOnPointerUp = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPointerClick(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPointerClick(PointerEventData obj)
    {
      this.EventOnPointerClick = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBeginDrag(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBeginDrag(PointerEventData obj)
    {
      this.EventOnBeginDrag = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndDrag(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndDrag(PointerEventData obj)
    {
      this.EventOnEndDrag = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDrag(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnDrag(PointerEventData obj)
    {
      this.EventOnDrag = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOn3DTouch(Vector2 obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOn3DTouch(Vector2 obj)
    {
      this.EventOn3DTouch = (Action<Vector2>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleSceneUIController m_owner;

      public LuaExportHelper(BattleSceneUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnPointerDown(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnPointerDown(obj);
      }

      public void __clearDele_EventOnPointerDown(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnPointerDown(obj);
      }

      public void __callDele_EventOnPointerUp(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnPointerUp(obj);
      }

      public void __clearDele_EventOnPointerUp(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnPointerUp(obj);
      }

      public void __callDele_EventOnPointerClick(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnPointerClick(obj);
      }

      public void __clearDele_EventOnPointerClick(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnPointerClick(obj);
      }

      public void __callDele_EventOnBeginDrag(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnBeginDrag(obj);
      }

      public void __clearDele_EventOnBeginDrag(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnBeginDrag(obj);
      }

      public void __callDele_EventOnEndDrag(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnEndDrag(obj);
      }

      public void __clearDele_EventOnEndDrag(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnEndDrag(obj);
      }

      public void __callDele_EventOnDrag(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnDrag(obj);
      }

      public void __clearDele_EventOnDrag(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnDrag(obj);
      }

      public void __callDele_EventOn3DTouch(Vector2 obj)
      {
        this.m_owner.__callDele_EventOn3DTouch(obj);
      }

      public void __clearDele_EventOn3DTouch(Vector2 obj)
      {
        this.m_owner.__clearDele_EventOn3DTouch(obj);
      }

      public GameObject m_actionMarkGameObject
      {
        get
        {
          return this.m_owner.m_actionMarkGameObject;
        }
        set
        {
          this.m_owner.m_actionMarkGameObject = value;
        }
      }

      public GameObject m_actionMark2GameObject
      {
        get
        {
          return this.m_owner.m_actionMark2GameObject;
        }
        set
        {
          this.m_owner.m_actionMark2GameObject = value;
        }
      }

      public CommonUIStateController m_winConditionUIStateController
      {
        get
        {
          return this.m_owner.m_winConditionUIStateController;
        }
        set
        {
          this.m_owner.m_winConditionUIStateController = value;
        }
      }

      public GameObject m_winConditionKillGameObject
      {
        get
        {
          return this.m_owner.m_winConditionKillGameObject;
        }
        set
        {
          this.m_owner.m_winConditionKillGameObject = value;
        }
      }

      public GameObject m_winConditionProtectGameObject
      {
        get
        {
          return this.m_owner.m_winConditionProtectGameObject;
        }
        set
        {
          this.m_owner.m_winConditionProtectGameObject = value;
        }
      }

      public GameObject m_selectionMarkGameObject
      {
        get
        {
          return this.m_owner.m_selectionMarkGameObject;
        }
        set
        {
          this.m_owner.m_selectionMarkGameObject = value;
        }
      }

      public CommonUIStateController m_passiveSkillUIStateController
      {
        get
        {
          return this.m_owner.m_passiveSkillUIStateController;
        }
        set
        {
          this.m_owner.m_passiveSkillUIStateController = value;
        }
      }

      public Image m_passiveSkillIconImage
      {
        get
        {
          return this.m_owner.m_passiveSkillIconImage;
        }
        set
        {
          this.m_owner.m_passiveSkillIconImage = value;
        }
      }

      public Image m_passiveSkillTalentIconImage
      {
        get
        {
          return this.m_owner.m_passiveSkillTalentIconImage;
        }
        set
        {
          this.m_owner.m_passiveSkillTalentIconImage = value;
        }
      }

      public GameObject m_passiveSkillEquipIconGameObject
      {
        get
        {
          return this.m_owner.m_passiveSkillEquipIconGameObject;
        }
        set
        {
          this.m_owner.m_passiveSkillEquipIconGameObject = value;
        }
      }

      public Image m_passiveSkillEquipIconImage
      {
        get
        {
          return this.m_owner.m_passiveSkillEquipIconImage;
        }
        set
        {
          this.m_owner.m_passiveSkillEquipIconImage = value;
        }
      }

      public Text m_passiveSkillNameText
      {
        get
        {
          return this.m_owner.m_passiveSkillNameText;
        }
        set
        {
          this.m_owner.m_passiveSkillNameText = value;
        }
      }

      public CommonUIStateController m_guardUIStateController
      {
        get
        {
          return this.m_owner.m_guardUIStateController;
        }
        set
        {
          this.m_owner.m_guardUIStateController = value;
        }
      }

      public CommonUIStateController m_protectBanGroupUIStateController
      {
        get
        {
          return this.m_owner.m_protectBanGroupUIStateController;
        }
        set
        {
          this.m_owner.m_protectBanGroupUIStateController = value;
        }
      }

      public CommonUIStateController m_protectUIStateController
      {
        get
        {
          return this.m_owner.m_protectUIStateController;
        }
        set
        {
          this.m_owner.m_protectUIStateController = value;
        }
      }

      public CommonUIStateController m_banUIStateController
      {
        get
        {
          return this.m_owner.m_banUIStateController;
        }
        set
        {
          this.m_owner.m_banUIStateController = value;
        }
      }

      public GameObject m_damageNumbersGameObject
      {
        get
        {
          return this.m_owner.m_damageNumbersGameObject;
        }
        set
        {
          this.m_owner.m_damageNumbersGameObject = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_damageNumberPrefab
      {
        get
        {
          return this.m_owner.m_damageNumberPrefab;
        }
        set
        {
          this.m_owner.m_damageNumberPrefab = value;
        }
      }

      public GameObject m_damageNumberWeakPrefab
      {
        get
        {
          return this.m_owner.m_damageNumberWeakPrefab;
        }
        set
        {
          this.m_owner.m_damageNumberWeakPrefab = value;
        }
      }

      public GameObject m_damageNumberStrongPrefab
      {
        get
        {
          return this.m_owner.m_damageNumberStrongPrefab;
        }
        set
        {
          this.m_owner.m_damageNumberStrongPrefab = value;
        }
      }

      public GameObject m_damageNumberCriticalPrefab
      {
        get
        {
          return this.m_owner.m_damageNumberCriticalPrefab;
        }
        set
        {
          this.m_owner.m_damageNumberCriticalPrefab = value;
        }
      }

      public GameObject m_damageNumberSoldierPrefab
      {
        get
        {
          return this.m_owner.m_damageNumberSoldierPrefab;
        }
        set
        {
          this.m_owner.m_damageNumberSoldierPrefab = value;
        }
      }

      public GameObject m_healNumberPrefab
      {
        get
        {
          return this.m_owner.m_healNumberPrefab;
        }
        set
        {
          this.m_owner.m_healNumberPrefab = value;
        }
      }

      public GameObject m_immunePrefab
      {
        get
        {
          return this.m_owner.m_immunePrefab;
        }
        set
        {
          this.m_owner.m_immunePrefab = value;
        }
      }

      public GameObject m_target1Prefab
      {
        get
        {
          return this.m_owner.m_target1Prefab;
        }
        set
        {
          this.m_owner.m_target1Prefab = value;
        }
      }

      public GameObject m_target2Prefab
      {
        get
        {
          return this.m_owner.m_target2Prefab;
        }
        set
        {
          this.m_owner.m_target2Prefab = value;
        }
      }

      public GameObject m_target3Prefab
      {
        get
        {
          return this.m_owner.m_target3Prefab;
        }
        set
        {
          this.m_owner.m_target3Prefab = value;
        }
      }

      public GameObject m_target4Prefab
      {
        get
        {
          return this.m_owner.m_target4Prefab;
        }
        set
        {
          this.m_owner.m_target4Prefab = value;
        }
      }

      public GameObject m_target5Prefab
      {
        get
        {
          return this.m_owner.m_target5Prefab;
        }
        set
        {
          this.m_owner.m_target5Prefab = value;
        }
      }

      public GameObject m_target6Prefab
      {
        get
        {
          return this.m_owner.m_target6Prefab;
        }
        set
        {
          this.m_owner.m_target6Prefab = value;
        }
      }

      public GameObject m_target7Prefab
      {
        get
        {
          return this.m_owner.m_target7Prefab;
        }
        set
        {
          this.m_owner.m_target7Prefab = value;
        }
      }

      public GameObject m_canActionPrefab
      {
        get
        {
          return this.m_owner.m_canActionPrefab;
        }
        set
        {
          this.m_owner.m_canActionPrefab = value;
        }
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public GameObjectPool<DamageNumber> m_damageNumberPool
      {
        get
        {
          return this.m_owner.m_damageNumberPool;
        }
        set
        {
          this.m_owner.m_damageNumberPool = value;
        }
      }

      public GameObjectPool<DamageNumber> m_damageNumberWeakPool
      {
        get
        {
          return this.m_owner.m_damageNumberWeakPool;
        }
        set
        {
          this.m_owner.m_damageNumberWeakPool = value;
        }
      }

      public GameObjectPool<DamageNumber> m_damageNumberStrongPool
      {
        get
        {
          return this.m_owner.m_damageNumberStrongPool;
        }
        set
        {
          this.m_owner.m_damageNumberStrongPool = value;
        }
      }

      public GameObjectPool<DamageNumber> m_damageNumberCriticalPool
      {
        get
        {
          return this.m_owner.m_damageNumberCriticalPool;
        }
        set
        {
          this.m_owner.m_damageNumberCriticalPool = value;
        }
      }

      public GameObjectPool<DamageNumber> m_damageNumberSoldierPool
      {
        get
        {
          return this.m_owner.m_damageNumberSoldierPool;
        }
        set
        {
          this.m_owner.m_damageNumberSoldierPool = value;
        }
      }

      public GameObjectPool<DamageNumber> m_healNumberPool
      {
        get
        {
          return this.m_owner.m_healNumberPool;
        }
        set
        {
          this.m_owner.m_healNumberPool = value;
        }
      }

      public GameObjectPool<DamageNumber> m_immunePool
      {
        get
        {
          return this.m_owner.m_immunePool;
        }
        set
        {
          this.m_owner.m_immunePool = value;
        }
      }

      public GameObjectPool[] m_targetIconPools
      {
        get
        {
          return this.m_owner.m_targetIconPools;
        }
        set
        {
          this.m_owner.m_targetIconPools = value;
        }
      }

      public GameObjectPool m_canActionIconPool
      {
        get
        {
          return this.m_owner.m_canActionIconPool;
        }
        set
        {
          this.m_owner.m_canActionIconPool = value;
        }
      }

      public float m_hideTargetIconTime
      {
        get
        {
          return this.m_owner.m_hideTargetIconTime;
        }
        set
        {
          this.m_owner.m_hideTargetIconTime = value;
        }
      }

      public static int InvalidPointerID
      {
        get
        {
          return -1000;
        }
      }

      public int m_downPointerId
      {
        get
        {
          return this.m_owner.m_downPointerId;
        }
        set
        {
          this.m_owner.m_downPointerId = value;
        }
      }

      public int m_clickPointerId
      {
        get
        {
          return this.m_owner.m_clickPointerId;
        }
        set
        {
          this.m_owner.m_clickPointerId = value;
        }
      }

      public int m_dragPointerId
      {
        get
        {
          return this.m_owner.m_dragPointerId;
        }
        set
        {
          this.m_owner.m_dragPointerId = value;
        }
      }

      public bool m_is3DTouchTriggered
      {
        get
        {
          return this.m_owner.m_is3DTouchTriggered;
        }
        set
        {
          this.m_owner.m_is3DTouchTriggered = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void ShowDamageNumbers(
        ClientBattleActor a,
        int heroHpModify,
        int soldierHpModify,
        DamageNumberType damageNumberType)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void ShowDamangeNumber(
        ClientBattleActor a,
        int hpModify,
        DamageNumberType damageNumberType,
        bool isHero)
      {
        // ISSUE: unable to decompile the method.
      }

      public void Update()
      {
        this.m_owner.Update();
      }

      public void Update3DTouch()
      {
        this.m_owner.Update3DTouch();
      }

      public Vector3 GetActorGraphicCenterPosition(ClientBattleActor a)
      {
        return this.m_owner.GetActorGraphicCenterPosition(a);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void ShowSkillTargetIcon(
        GameObject go,
        GridPosition pos,
        ConfigDataSkillInfo skillInfo,
        GridPosition startPos)
      {
        // ISSUE: unable to decompile the method.
      }

      public GameObject AllocateAttackTargetIcon(int armyRelationValue)
      {
        return this.m_owner.AllocateAttackTargetIcon(armyRelationValue);
      }

      public GameObject AllocateSkillTargetIcon(ConfigDataSkillInfo skillInfo)
      {
        return this.m_owner.AllocateSkillTargetIcon(skillInfo);
      }

      public Vector3 GridPositionToMapPosition(GridPosition p, float z)
      {
        return this.m_owner.GridPositionToMapPosition(p, z);
      }
    }
  }
}
