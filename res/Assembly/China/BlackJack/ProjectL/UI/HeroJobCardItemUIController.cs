﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobCardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class HeroJobCardItemUIController : UIControllerBase
  {
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./CurJobImg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobImg;
    [AutoBind("./MagicStone", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_magicStone;
    [AutoBind("./LockImg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockImg;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Frame", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_frameImg;
    [AutoBind("./RedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redIcon;
    [AutoBind("./Master", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterIcon;
    private Hero m_hero;
    private int m_rank;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroJobCardItem(
      Hero hero,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInfoByState()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetFrameImage(bool isShow)
    {
      this.m_frameImg.SetActive(isShow);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobMaster()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLight()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobCardItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroJobCardItemUIController> EventOnJobCardItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo { private set; get; }

    public bool IsNeedMagicStone { private set; get; }

    public bool IsJobLocked { private set; get; }

    public bool HaveRedIcon { private set; get; }
  }
}
