﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RaffleRewardItemUIController : UIControllerBase
  {
    protected List<RewardGoodsUIController> m_goodsCtrlList;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./Detail/TitleGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text LevelText;
    [AutoBind("./Detail/ItemContent", AutoBindAttribute.InitState.NotInit, false)]
    public Transform GoodsItemRoot;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleRewardItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRaffleRewardItemInfo(
      int level,
      List<RaffleItem> itemList,
      HashSet<int> drawedRaffleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRewardItemGotState(List<RaffleItem> itemList, HashSet<int> drawedRaffleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLvNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected StringTableId GetLevelStrByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetLvStateNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
