﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSimpleItemInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroSimpleItemInfoUIController : UIControllerBase
  {
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Job/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroJobText;
    [AutoBind("./Army", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyImage;
    [AutoBind("./Army/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_armyText;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStars;
    [AutoBind("./Property/HP/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHPImage;
    [AutoBind("./Property/DF/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroDFImage;
    [AutoBind("./Property/AT/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroATImage;
    [AutoBind("./Property/MagicDF/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroMagicDFImage;
    [AutoBind("./Property/Magic/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroMagicImage;
    [AutoBind("./Property/DEX/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroDEXImage;
    [AutoBind("./Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_talentIconImg;
    [AutoBind("./Talent/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talentNameText;
    [AutoBind("./Talent/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talentDescText;
    [AutoBind("./FinalJobs/Job1/NameText_CH", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob1CHNameText;
    [AutoBind("./FinalJobs/Job1/NameText_EN", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob1ENNameText;
    [AutoBind("./FinalJobs/Job1/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finalJob1GraphicObj;
    [AutoBind("./FinalJobs/Job2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finalJob2Obj;
    [AutoBind("./FinalJobs/Job2/NameText_CH", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob2CHNameText;
    [AutoBind("./FinalJobs/Job2/NameText_EN", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob2ENNameText;
    [AutoBind("./FinalJobs/Job2/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finalJob2GraphicObj;
    public Hero m_hero;
    private UISpineGraphic m_heroJobGraphic1;
    private UISpineGraphic m_heroJobGraphic2;
    [DoNotToLua]
    private HeroSimpleItemInfoUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitHeroSimpleItemInfoHero_hotfix;
    private LuaFunction m_SetValues_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataJobConnectionInfoUISpineGraphic_GameObject_hotfix;
    private LuaFunction m_DestroySpineGraphicUISpineGraphic__hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroSimpleItemInfo(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetValues()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ref UISpineGraphic m_graphic,
      GameObject m_grapgicObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(ref UISpineGraphic m_graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public HeroSimpleItemInfoUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroSimpleItemInfoUIController m_owner;

      public LuaExportHelper(HeroSimpleItemInfoUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public Text m_heroNameText
      {
        get
        {
          return this.m_owner.m_heroNameText;
        }
        set
        {
          this.m_owner.m_heroNameText = value;
        }
      }

      public Text m_heroJobText
      {
        get
        {
          return this.m_owner.m_heroJobText;
        }
        set
        {
          this.m_owner.m_heroJobText = value;
        }
      }

      public Image m_armyImage
      {
        get
        {
          return this.m_owner.m_armyImage;
        }
        set
        {
          this.m_owner.m_armyImage = value;
        }
      }

      public Text m_armyText
      {
        get
        {
          return this.m_owner.m_armyText;
        }
        set
        {
          this.m_owner.m_armyText = value;
        }
      }

      public GameObject m_heroStars
      {
        get
        {
          return this.m_owner.m_heroStars;
        }
        set
        {
          this.m_owner.m_heroStars = value;
        }
      }

      public Image m_heroHPImage
      {
        get
        {
          return this.m_owner.m_heroHPImage;
        }
        set
        {
          this.m_owner.m_heroHPImage = value;
        }
      }

      public Image m_heroDFImage
      {
        get
        {
          return this.m_owner.m_heroDFImage;
        }
        set
        {
          this.m_owner.m_heroDFImage = value;
        }
      }

      public Image m_heroATImage
      {
        get
        {
          return this.m_owner.m_heroATImage;
        }
        set
        {
          this.m_owner.m_heroATImage = value;
        }
      }

      public Image m_heroMagicDFImage
      {
        get
        {
          return this.m_owner.m_heroMagicDFImage;
        }
        set
        {
          this.m_owner.m_heroMagicDFImage = value;
        }
      }

      public Image m_heroMagicImage
      {
        get
        {
          return this.m_owner.m_heroMagicImage;
        }
        set
        {
          this.m_owner.m_heroMagicImage = value;
        }
      }

      public Image m_heroDEXImage
      {
        get
        {
          return this.m_owner.m_heroDEXImage;
        }
        set
        {
          this.m_owner.m_heroDEXImage = value;
        }
      }

      public Image m_talentIconImg
      {
        get
        {
          return this.m_owner.m_talentIconImg;
        }
        set
        {
          this.m_owner.m_talentIconImg = value;
        }
      }

      public Text m_talentNameText
      {
        get
        {
          return this.m_owner.m_talentNameText;
        }
        set
        {
          this.m_owner.m_talentNameText = value;
        }
      }

      public Text m_talentDescText
      {
        get
        {
          return this.m_owner.m_talentDescText;
        }
        set
        {
          this.m_owner.m_talentDescText = value;
        }
      }

      public Text m_finalJob1CHNameText
      {
        get
        {
          return this.m_owner.m_finalJob1CHNameText;
        }
        set
        {
          this.m_owner.m_finalJob1CHNameText = value;
        }
      }

      public Text m_finalJob1ENNameText
      {
        get
        {
          return this.m_owner.m_finalJob1ENNameText;
        }
        set
        {
          this.m_owner.m_finalJob1ENNameText = value;
        }
      }

      public GameObject m_finalJob1GraphicObj
      {
        get
        {
          return this.m_owner.m_finalJob1GraphicObj;
        }
        set
        {
          this.m_owner.m_finalJob1GraphicObj = value;
        }
      }

      public GameObject m_finalJob2Obj
      {
        get
        {
          return this.m_owner.m_finalJob2Obj;
        }
        set
        {
          this.m_owner.m_finalJob2Obj = value;
        }
      }

      public Text m_finalJob2CHNameText
      {
        get
        {
          return this.m_owner.m_finalJob2CHNameText;
        }
        set
        {
          this.m_owner.m_finalJob2CHNameText = value;
        }
      }

      public Text m_finalJob2ENNameText
      {
        get
        {
          return this.m_owner.m_finalJob2ENNameText;
        }
        set
        {
          this.m_owner.m_finalJob2ENNameText = value;
        }
      }

      public GameObject m_finalJob2GraphicObj
      {
        get
        {
          return this.m_owner.m_finalJob2GraphicObj;
        }
        set
        {
          this.m_owner.m_finalJob2GraphicObj = value;
        }
      }

      public UISpineGraphic m_heroJobGraphic1
      {
        get
        {
          return this.m_owner.m_heroJobGraphic1;
        }
        set
        {
          this.m_owner.m_heroJobGraphic1 = value;
        }
      }

      public UISpineGraphic m_heroJobGraphic2
      {
        get
        {
          return this.m_owner.m_heroJobGraphic2;
        }
        set
        {
          this.m_owner.m_heroJobGraphic2 = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetValues()
      {
        this.m_owner.SetValues();
      }

      public void CreateSpineGraphic(
        ConfigDataJobConnectionInfo jobConnectionInfo,
        ref UISpineGraphic m_graphic,
        GameObject m_grapgicObj)
      {
        this.m_owner.CreateSpineGraphic(jobConnectionInfo, ref m_graphic, m_grapgicObj);
      }

      public void DestroySpineGraphic(ref UISpineGraphic m_graphic)
      {
        this.m_owner.DestroySpineGraphic(ref m_graphic);
      }
    }
  }
}
