﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleReportEndUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using SLua;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class BattleReportEndUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_leftPlayerIcon;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerLV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerLevel;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerName;
    [AutoBind("./Panel/RightPlyer/Player/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rightPlayerIcon;
    [AutoBind("./Panel/RightPlyer/Player/PlayerLV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerLevel;
    [AutoBind("./Panel/RightPlyer/Player/PlayerName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerName;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention/Win", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftWinGameObject;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention/Lose", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftLoseGameObject;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftAbstentionGameObject;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention/Win", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightWinGameObject;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention/Lose", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightLoseGameObject;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightAbstentionGameObject;
    [AutoBind("./Panel/LeftMatchGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftMatchGameObject;
    [AutoBind("./Panel/LeftMatchGroup/WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftMatchWin1GameObject;
    [AutoBind("./Panel/LeftMatchGroup/WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftMatchWin2GameObject;
    [AutoBind("./Panel/RightMatchGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightMatchGameObject;
    [AutoBind("./Panel/RightMatchGroup/WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightMatchWin1GameObject;
    [AutoBind("./Panel/RightMatchGroup/WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightMatchWin2GameObject;
    [AutoBind("./Panel/PeakArenaPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaPanelGameObject;
    [AutoBind("./Panel/PeakArenaPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaConfirmButton;
    [AutoBind("./Panel/PeakArenaPanel/ContinueFightButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaContinueButton;
    [AutoBind("./Panel/LivePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaLivePanelGameObject;
    [AutoBind("./Panel/LivePanel/Wait", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaLiveWaitGameObject;
    [AutoBind("./Panel/LivePanel/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaLiveExitButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/PlayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playButton;
    [AutoBind("./Panel/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exitButton;
    [AutoBind("./Panel/TestCountdownText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_testCountdownText;
    [DoNotToLua]
    private BattleReportEndUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_OpenArenaBattleReport_hotfix;
    private LuaFunction m_OpenRealTimePVPBattleReport_hotfix;
    private LuaFunction m_OpenPeakArenaLadderBattleReportInt32_hotfix;
    private LuaFunction m_OpenBattleRoom_hotfix;
    private LuaFunction m_Co_WaitAndNextRoundSingle_hotfix;
    private LuaFunction m_SetLeftPlayerStringInt32Int32_hotfix;
    private LuaFunction m_SetRightPlayerStringInt32Int32_hotfix;
    private LuaFunction m_SetLeftWinOrLoseOrAbstentionInt32_hotfix;
    private LuaFunction m_SetRightWinOrLoseOrAbstentionInt32_hotfix;
    private LuaFunction m_SetLeftWinCountInt32_hotfix;
    private LuaFunction m_SetRightWinCountInt32_hotfix;
    private LuaFunction m_SetTestCountdownTimeSpan_hotfix;
    private LuaFunction m_CloseAction_hotfix;
    private LuaFunction m_OnBackgroundButtonClick_hotfix;
    private LuaFunction m_OnPlayButtonClick_hotfix;
    private LuaFunction m_OnExitButtonClick_hotfix;
    private LuaFunction m_OnPeakArenaConfirmButtonClick_hotfix;
    private LuaFunction m_OnPeakArenaContinueButtonClick_hotfix;
    private LuaFunction m_OnPeakArenaLiveExitButtonClick_hotfix;
    private LuaFunction m_add_EventOnPlayAgainAction_hotfix;
    private LuaFunction m_remove_EventOnPlayAgainAction_hotfix;
    private LuaFunction m_add_EventOnNextRoundAction_hotfix;
    private LuaFunction m_remove_EventOnNextRoundAction_hotfix;
    private LuaFunction m_add_EventOnCloseAction_hotfix;
    private LuaFunction m_remove_EventOnCloseAction_hotfix;
    private LuaFunction m_add_EventOnPeakArenaConfirmAction_hotfix;
    private LuaFunction m_remove_EventOnPeakArenaConfirmAction_hotfix;
    private LuaFunction m_add_EventOnPeakArenaContinueAction_hotfix;
    private LuaFunction m_remove_EventOnPeakArenaContinueAction_hotfix;
    private LuaFunction m_add_EventOnPeakArenaLiveExitAction_hotfix;
    private LuaFunction m_remove_EventOnPeakArenaLiveExitAction_hotfix;

    private BattleReportEndUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(PeakArenaLadderBattleReport battleReport, int battleReportBoRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(BattleRoom battleRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitAndNextRound(float waitTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRightPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftWinOrLoseOrAbstention(int i)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRightWinOrLoseOrAbstention(int i)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftWinCount(int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRightWinCount(int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaLiveExitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPlayAgain
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNextRound
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaContinue
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaLiveExit
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleReportEndUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPlayAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPlayAgain()
    {
      this.EventOnPlayAgain = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnNextRound()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnNextRound()
    {
      this.EventOnNextRound = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClose()
    {
      this.EventOnClose = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPeakArenaConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPeakArenaConfirm()
    {
      this.EventOnPeakArenaConfirm = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPeakArenaContinue()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPeakArenaContinue()
    {
      this.EventOnPeakArenaContinue = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPeakArenaLiveExit()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPeakArenaLiveExit()
    {
      this.EventOnPeakArenaLiveExit = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleReportEndUIController m_owner;

      public LuaExportHelper(BattleReportEndUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnPlayAgain()
      {
        this.m_owner.__callDele_EventOnPlayAgain();
      }

      public void __clearDele_EventOnPlayAgain()
      {
        this.m_owner.__clearDele_EventOnPlayAgain();
      }

      public void __callDele_EventOnNextRound()
      {
        this.m_owner.__callDele_EventOnNextRound();
      }

      public void __clearDele_EventOnNextRound()
      {
        this.m_owner.__clearDele_EventOnNextRound();
      }

      public void __callDele_EventOnClose()
      {
        this.m_owner.__callDele_EventOnClose();
      }

      public void __clearDele_EventOnClose()
      {
        this.m_owner.__clearDele_EventOnClose();
      }

      public void __callDele_EventOnPeakArenaConfirm()
      {
        this.m_owner.__callDele_EventOnPeakArenaConfirm();
      }

      public void __clearDele_EventOnPeakArenaConfirm()
      {
        this.m_owner.__clearDele_EventOnPeakArenaConfirm();
      }

      public void __callDele_EventOnPeakArenaContinue()
      {
        this.m_owner.__callDele_EventOnPeakArenaContinue();
      }

      public void __clearDele_EventOnPeakArenaContinue()
      {
        this.m_owner.__clearDele_EventOnPeakArenaContinue();
      }

      public void __callDele_EventOnPeakArenaLiveExit()
      {
        this.m_owner.__callDele_EventOnPeakArenaLiveExit();
      }

      public void __clearDele_EventOnPeakArenaLiveExit()
      {
        this.m_owner.__clearDele_EventOnPeakArenaLiveExit();
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Image m_leftPlayerIcon
      {
        get
        {
          return this.m_owner.m_leftPlayerIcon;
        }
        set
        {
          this.m_owner.m_leftPlayerIcon = value;
        }
      }

      public Text m_leftPlayerLevel
      {
        get
        {
          return this.m_owner.m_leftPlayerLevel;
        }
        set
        {
          this.m_owner.m_leftPlayerLevel = value;
        }
      }

      public Text m_leftPlayerName
      {
        get
        {
          return this.m_owner.m_leftPlayerName;
        }
        set
        {
          this.m_owner.m_leftPlayerName = value;
        }
      }

      public Image m_rightPlayerIcon
      {
        get
        {
          return this.m_owner.m_rightPlayerIcon;
        }
        set
        {
          this.m_owner.m_rightPlayerIcon = value;
        }
      }

      public Text m_rightPlayerLevel
      {
        get
        {
          return this.m_owner.m_rightPlayerLevel;
        }
        set
        {
          this.m_owner.m_rightPlayerLevel = value;
        }
      }

      public Text m_rightPlayerName
      {
        get
        {
          return this.m_owner.m_rightPlayerName;
        }
        set
        {
          this.m_owner.m_rightPlayerName = value;
        }
      }

      public GameObject m_leftWinGameObject
      {
        get
        {
          return this.m_owner.m_leftWinGameObject;
        }
        set
        {
          this.m_owner.m_leftWinGameObject = value;
        }
      }

      public GameObject m_leftLoseGameObject
      {
        get
        {
          return this.m_owner.m_leftLoseGameObject;
        }
        set
        {
          this.m_owner.m_leftLoseGameObject = value;
        }
      }

      public GameObject m_leftAbstentionGameObject
      {
        get
        {
          return this.m_owner.m_leftAbstentionGameObject;
        }
        set
        {
          this.m_owner.m_leftAbstentionGameObject = value;
        }
      }

      public GameObject m_rightWinGameObject
      {
        get
        {
          return this.m_owner.m_rightWinGameObject;
        }
        set
        {
          this.m_owner.m_rightWinGameObject = value;
        }
      }

      public GameObject m_rightLoseGameObject
      {
        get
        {
          return this.m_owner.m_rightLoseGameObject;
        }
        set
        {
          this.m_owner.m_rightLoseGameObject = value;
        }
      }

      public GameObject m_rightAbstentionGameObject
      {
        get
        {
          return this.m_owner.m_rightAbstentionGameObject;
        }
        set
        {
          this.m_owner.m_rightAbstentionGameObject = value;
        }
      }

      public GameObject m_leftMatchGameObject
      {
        get
        {
          return this.m_owner.m_leftMatchGameObject;
        }
        set
        {
          this.m_owner.m_leftMatchGameObject = value;
        }
      }

      public GameObject m_leftMatchWin1GameObject
      {
        get
        {
          return this.m_owner.m_leftMatchWin1GameObject;
        }
        set
        {
          this.m_owner.m_leftMatchWin1GameObject = value;
        }
      }

      public GameObject m_leftMatchWin2GameObject
      {
        get
        {
          return this.m_owner.m_leftMatchWin2GameObject;
        }
        set
        {
          this.m_owner.m_leftMatchWin2GameObject = value;
        }
      }

      public GameObject m_rightMatchGameObject
      {
        get
        {
          return this.m_owner.m_rightMatchGameObject;
        }
        set
        {
          this.m_owner.m_rightMatchGameObject = value;
        }
      }

      public GameObject m_rightMatchWin1GameObject
      {
        get
        {
          return this.m_owner.m_rightMatchWin1GameObject;
        }
        set
        {
          this.m_owner.m_rightMatchWin1GameObject = value;
        }
      }

      public GameObject m_rightMatchWin2GameObject
      {
        get
        {
          return this.m_owner.m_rightMatchWin2GameObject;
        }
        set
        {
          this.m_owner.m_rightMatchWin2GameObject = value;
        }
      }

      public GameObject m_peakArenaPanelGameObject
      {
        get
        {
          return this.m_owner.m_peakArenaPanelGameObject;
        }
        set
        {
          this.m_owner.m_peakArenaPanelGameObject = value;
        }
      }

      public Button m_peakArenaConfirmButton
      {
        get
        {
          return this.m_owner.m_peakArenaConfirmButton;
        }
        set
        {
          this.m_owner.m_peakArenaConfirmButton = value;
        }
      }

      public Button m_peakArenaContinueButton
      {
        get
        {
          return this.m_owner.m_peakArenaContinueButton;
        }
        set
        {
          this.m_owner.m_peakArenaContinueButton = value;
        }
      }

      public GameObject m_peakArenaLivePanelGameObject
      {
        get
        {
          return this.m_owner.m_peakArenaLivePanelGameObject;
        }
        set
        {
          this.m_owner.m_peakArenaLivePanelGameObject = value;
        }
      }

      public GameObject m_peakArenaLiveWaitGameObject
      {
        get
        {
          return this.m_owner.m_peakArenaLiveWaitGameObject;
        }
        set
        {
          this.m_owner.m_peakArenaLiveWaitGameObject = value;
        }
      }

      public Button m_peakArenaLiveExitButton
      {
        get
        {
          return this.m_owner.m_peakArenaLiveExitButton;
        }
        set
        {
          this.m_owner.m_peakArenaLiveExitButton = value;
        }
      }

      public Button m_backgroundButton
      {
        get
        {
          return this.m_owner.m_backgroundButton;
        }
        set
        {
          this.m_owner.m_backgroundButton = value;
        }
      }

      public Button m_playButton
      {
        get
        {
          return this.m_owner.m_playButton;
        }
        set
        {
          this.m_owner.m_playButton = value;
        }
      }

      public Button m_exitButton
      {
        get
        {
          return this.m_owner.m_exitButton;
        }
        set
        {
          this.m_owner.m_exitButton = value;
        }
      }

      public Text m_testCountdownText
      {
        get
        {
          return this.m_owner.m_testCountdownText;
        }
        set
        {
          this.m_owner.m_testCountdownText = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public IEnumerator Co_WaitAndNextRound(float waitTime)
      {
        return this.m_owner.Co_WaitAndNextRound(waitTime);
      }

      public void SetLeftPlayer(string name, int level, int headIcon)
      {
        this.m_owner.SetLeftPlayer(name, level, headIcon);
      }

      public void SetRightPlayer(string name, int level, int headIcon)
      {
        this.m_owner.SetRightPlayer(name, level, headIcon);
      }

      public void SetLeftWinOrLoseOrAbstention(int i)
      {
        this.m_owner.SetLeftWinOrLoseOrAbstention(i);
      }

      public void SetRightWinOrLoseOrAbstention(int i)
      {
        this.m_owner.SetRightWinOrLoseOrAbstention(i);
      }

      public void SetLeftWinCount(int winCount)
      {
        this.m_owner.SetLeftWinCount(winCount);
      }

      public void SetRightWinCount(int winCount)
      {
        this.m_owner.SetRightWinCount(winCount);
      }

      public void OnBackgroundButtonClick()
      {
        this.m_owner.OnBackgroundButtonClick();
      }

      public void OnPlayButtonClick()
      {
        this.m_owner.OnPlayButtonClick();
      }

      public void OnExitButtonClick()
      {
        this.m_owner.OnExitButtonClick();
      }

      public void OnPeakArenaConfirmButtonClick()
      {
        this.m_owner.OnPeakArenaConfirmButtonClick();
      }

      public void OnPeakArenaContinueButtonClick()
      {
        this.m_owner.OnPeakArenaContinueButtonClick();
      }

      public void OnPeakArenaLiveExitButtonClick()
      {
        this.m_owner.OnPeakArenaLiveExitButtonClick();
      }
    }
  }
}
