﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroPhantomUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroPhantomUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./Background/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGo;
    [AutoBind("./Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementUIStateController;
    [AutoBind("./Achievement/Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementScrollRect;
    [AutoBind("./Achievement/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementBackgroundButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/PhantomButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroPhantomLevelListItemPrefab;
    private List<HeroPhantomLevelListItemUIController> m_heroPhantomLevelListItems;
    private List<BattleAchievementItemUIController> m_achievementItems;
    private UISpineGraphic m_graphic;
    [DoNotToLua]
    private HeroPhantomUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Open_hotfix;
    private LuaFunction m_ResetScrollViewToTop_hotfix;
    private LuaFunction m_SetHeroPhantomConfigDataHeroPhantomInfo_hotfix;
    private LuaFunction m_AddAllHeroPhantomLevelListItemsList`1_hotfix;
    private LuaFunction m_AddHeroPhantomLevelListItemConfigDataHeroPhantomLevelInfoBoolean_hotfix;
    private LuaFunction m_ClearHeroPhantomLevelListItems_hotfix;
    private LuaFunction m_CreateSpineGraphicConfigDataHeroPhantomInfo_hotfix;
    private LuaFunction m_DestroySpineGraphic_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_HeroPhantomLevelListItem_OnStartButtonClickHeroPhantomLevelListItemUIController_hotfix;
    private LuaFunction m_OnAchievementBackgroundButtonClick_hotfix;
    private LuaFunction m_HeroPhantomLevelListItem_OnAchievementButtonClickHeroPhantomLevelListItemUIController_hotfix;
    private LuaFunction m_ShowAchievementBattleLevelAchievementbeConfigDataHeroPhantomLevelInfo_hotfix;
    private LuaFunction m_AddAchievementItemConfigDataBattleAchievementInfoList`1GameObjectBoolean_hotfix;
    private LuaFunction m_ClearAchievementItems_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnHelpAction_hotfix;
    private LuaFunction m_remove_EventOnHelpAction_hotfix;
    private LuaFunction m_add_EventOnStartHeroPhantomLevelAction`1_hotfix;
    private LuaFunction m_remove_EventOnStartHeroPhantomLevelAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroPhantomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroPhantom(ConfigDataHeroPhantomInfo heroPhantomInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAllHeroPhantomLevelListItems(List<ConfigDataHeroPhantomLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroPhantomLevelListItem(ConfigDataHeroPhantomLevelInfo levelnfo, bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeroPhantomLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(ConfigDataHeroPhantomInfo heroPhantomInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroPhantomLevelListItem_OnStartButtonClick(
      HeroPhantomLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroPhantomLevelListItem_OnAchievementButtonClick(
      HeroPhantomLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAchievement(
      BattleLevelAchievement[] achievements,
      ConfigDataHeroPhantomLevelInfo heroPhantomLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddAchievementItem(
      ConfigDataBattleAchievementInfo achievementInfo,
      List<Goods> rewards,
      GameObject prefab,
      bool complete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAchievementItems()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroPhantomLevelInfo> EventOnStartHeroPhantomLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroPhantomUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnHelp()
    {
      this.EventOnHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStartHeroPhantomLevel(ConfigDataHeroPhantomLevelInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStartHeroPhantomLevel(ConfigDataHeroPhantomLevelInfo obj)
    {
      this.EventOnStartHeroPhantomLevel = (Action<ConfigDataHeroPhantomLevelInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroPhantomUIController m_owner;

      public LuaExportHelper(HeroPhantomUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnHelp()
      {
        this.m_owner.__callDele_EventOnHelp();
      }

      public void __clearDele_EventOnHelp()
      {
        this.m_owner.__clearDele_EventOnHelp();
      }

      public void __callDele_EventOnStartHeroPhantomLevel(ConfigDataHeroPhantomLevelInfo obj)
      {
        this.m_owner.__callDele_EventOnStartHeroPhantomLevel(obj);
      }

      public void __clearDele_EventOnStartHeroPhantomLevel(ConfigDataHeroPhantomLevelInfo obj)
      {
        this.m_owner.__clearDele_EventOnStartHeroPhantomLevel(obj);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public ScrollRect m_levelListScrollRect
      {
        get
        {
          return this.m_owner.m_levelListScrollRect;
        }
        set
        {
          this.m_owner.m_levelListScrollRect = value;
        }
      }

      public GameObject m_charGo
      {
        get
        {
          return this.m_owner.m_charGo;
        }
        set
        {
          this.m_owner.m_charGo = value;
        }
      }

      public CommonUIStateController m_achievementUIStateController
      {
        get
        {
          return this.m_owner.m_achievementUIStateController;
        }
        set
        {
          this.m_owner.m_achievementUIStateController = value;
        }
      }

      public ScrollRect m_achievementScrollRect
      {
        get
        {
          return this.m_owner.m_achievementScrollRect;
        }
        set
        {
          this.m_owner.m_achievementScrollRect = value;
        }
      }

      public Button m_achievementBackgroundButton
      {
        get
        {
          return this.m_owner.m_achievementBackgroundButton;
        }
        set
        {
          this.m_owner.m_achievementBackgroundButton = value;
        }
      }

      public GameObject m_prefabsGameObject
      {
        get
        {
          return this.m_owner.m_prefabsGameObject;
        }
        set
        {
          this.m_owner.m_prefabsGameObject = value;
        }
      }

      public GameObject m_heroPhantomLevelListItemPrefab
      {
        get
        {
          return this.m_owner.m_heroPhantomLevelListItemPrefab;
        }
        set
        {
          this.m_owner.m_heroPhantomLevelListItemPrefab = value;
        }
      }

      public List<HeroPhantomLevelListItemUIController> m_heroPhantomLevelListItems
      {
        get
        {
          return this.m_owner.m_heroPhantomLevelListItems;
        }
        set
        {
          this.m_owner.m_heroPhantomLevelListItems = value;
        }
      }

      public List<BattleAchievementItemUIController> m_achievementItems
      {
        get
        {
          return this.m_owner.m_achievementItems;
        }
        set
        {
          this.m_owner.m_achievementItems = value;
        }
      }

      public UISpineGraphic m_graphic
      {
        get
        {
          return this.m_owner.m_graphic;
        }
        set
        {
          this.m_owner.m_graphic = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void ResetScrollViewToTop()
      {
        this.m_owner.ResetScrollViewToTop();
      }

      public void AddHeroPhantomLevelListItem(ConfigDataHeroPhantomLevelInfo levelnfo, bool opened)
      {
        this.m_owner.AddHeroPhantomLevelListItem(levelnfo, opened);
      }

      public void CreateSpineGraphic(ConfigDataHeroPhantomInfo heroPhantomInfo)
      {
        this.m_owner.CreateSpineGraphic(heroPhantomInfo);
      }

      public void DestroySpineGraphic()
      {
        this.m_owner.DestroySpineGraphic();
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void HeroPhantomLevelListItem_OnStartButtonClick(
        HeroPhantomLevelListItemUIController ctrl)
      {
        this.m_owner.HeroPhantomLevelListItem_OnStartButtonClick(ctrl);
      }

      public void OnAchievementBackgroundButtonClick()
      {
        this.m_owner.OnAchievementBackgroundButtonClick();
      }

      public void HeroPhantomLevelListItem_OnAchievementButtonClick(
        HeroPhantomLevelListItemUIController ctrl)
      {
        this.m_owner.HeroPhantomLevelListItem_OnAchievementButtonClick(ctrl);
      }

      public void ShowAchievement(
        BattleLevelAchievement[] achievements,
        ConfigDataHeroPhantomLevelInfo heroPhantomLevelInfo)
      {
        this.m_owner.ShowAchievement(achievements, heroPhantomLevelInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void AddAchievementItem(
        ConfigDataBattleAchievementInfo achievementInfo,
        List<Goods> rewards,
        GameObject prefab,
        bool complete)
      {
        // ISSUE: unable to decompile the method.
      }

      public void ClearAchievementItems()
      {
        this.m_owner.ClearAchievementItems();
      }
    }
  }
}
