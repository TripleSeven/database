﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogDetailCharDialogInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public sealed class DialogDetailCharDialogInfoUIController : UIControllerBase
  {
    [AutoBind("./LogText", AutoBindAttribute.InitState.NotInit, false)]
    private Text LogText;
    [AutoBind("./NameGroup/NameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text NameText;

    public void SetName(string name)
    {
      this.NameText.text = name;
    }

    public void setDesc(string desc)
    {
      this.LogText.text = desc;
    }

    protected override void OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }
  }
}
