﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginCommonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class LoginCommonUIController : UIControllerBase
  {
    [AutoBind("./Fade", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fadeImage;
    [AutoBind("./DisableInput", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableInputGameObject;
    private ScreenFade m_screenFade;
    private TouchFx m_touchFx;

    private LoginCommonUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~LoginCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisposeTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      this.m_screenFade.FadeIn(time, color, onEnd);
    }

    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      this.m_screenFade.FadeOut(time, color, onEnd);
    }
  }
}
