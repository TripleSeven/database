﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClientGMUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ClientGMUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Panel/Clear", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearButton;
    [AutoBind("./Panel/Show", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_showButton;
    [AutoBind("./Panel/Confirm", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Panel/SaveToFile", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_saveToFileToggle;
    [AutoBind("./Panel/Description/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_gmInputField;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnCloseClick()
    {
      this.gameObject.SetActive(false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
