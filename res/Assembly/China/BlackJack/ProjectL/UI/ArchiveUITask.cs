﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArchiveUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class ArchiveUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public ManualUIController m_manualUIController;
    public HeroArchiveUIController m_heroArchiveUIController;
    public EquipmentArchiveUIController m_equipmentArchiveUIController;
    public HeroShowUIController m_heroShowUIController;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArchiveUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnStop()
    {
      base.OnStop();
      Input.multiTouchEnabled = false;
    }

    protected override void OnMemoryWarning()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    protected override void UpdateView()
    {
    }

    public void CustomLoadAsset(Action collectionCallback, Action finishCallback)
    {
      collectionCallback();
      this.StartLoadCustomAssets(finishCallback);
    }

    public void CollectAssetWrap(string assetName)
    {
      this.CollectAsset(assetName);
    }

    public void CollectSpriteAssetWrap(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    public void ReturnLastTask()
    {
      this.ReturnPrevUITask();
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      get
      {
        return this.m_layerDescArray;
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      get
      {
        return this.m_uiCtrlDescArray;
      }
    }
  }
}
