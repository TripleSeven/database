﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AssistanceTeamUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class AssistanceTeamUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./TrainingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_trainingButton;
    [AutoBind("./StopButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopButton;
    [AutoBind("./GetRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getRewardButton;
    [AutoBind("./DifficultyText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_difficultyText;
    [AutoBind("./TimeStateText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeStateText;
    private UISpineGraphic m_playerHeroGraphic;
    public int m_slot;
    public HeroAssistantsTaskAssignment m_heroAssistantsTask;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private AssistanceTeamUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_InitAssistanceTeamInfoHeroAssistantsTaskAssignmentInt32_hotfix;
    private LuaFunction m_SetTimeTextTimeSpan_hotfix;
    private LuaFunction m_SetSoldierSpine_hotfix;
    private LuaFunction m_OnTrainingButtonClick_hotfix;
    private LuaFunction m_OnStopButtonClick_hotfix;
    private LuaFunction m_OnGetRewardButtonClick_hotfix;
    private LuaFunction m_add_EventOnTrainingButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnTrainingButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnStopButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnStopButtonClickAction`1_hotfix;
    private LuaFunction m_add_EventOnGetRewardButtonClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetRewardButtonClickAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAssistanceTeamInfo(HeroAssistantsTaskAssignment task, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTimeText(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierSpine()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStopButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnTrainingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<AssistanceTeamUIController> EventOnStopButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<AssistanceTeamUIController> EventOnGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public AssistanceTeamUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnTrainingButtonClick(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnTrainingButtonClick(int obj)
    {
      this.EventOnTrainingButtonClick = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnStopButtonClick(AssistanceTeamUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnStopButtonClick(AssistanceTeamUIController obj)
    {
      this.EventOnStopButtonClick = (Action<AssistanceTeamUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetRewardButtonClick(AssistanceTeamUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetRewardButtonClick(AssistanceTeamUIController obj)
    {
      this.EventOnGetRewardButtonClick = (Action<AssistanceTeamUIController>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private AssistanceTeamUIController m_owner;

      public LuaExportHelper(AssistanceTeamUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnTrainingButtonClick(int obj)
      {
        this.m_owner.__callDele_EventOnTrainingButtonClick(obj);
      }

      public void __clearDele_EventOnTrainingButtonClick(int obj)
      {
        this.m_owner.__clearDele_EventOnTrainingButtonClick(obj);
      }

      public void __callDele_EventOnStopButtonClick(AssistanceTeamUIController obj)
      {
        this.m_owner.__callDele_EventOnStopButtonClick(obj);
      }

      public void __clearDele_EventOnStopButtonClick(AssistanceTeamUIController obj)
      {
        this.m_owner.__clearDele_EventOnStopButtonClick(obj);
      }

      public void __callDele_EventOnGetRewardButtonClick(AssistanceTeamUIController obj)
      {
        this.m_owner.__callDele_EventOnGetRewardButtonClick(obj);
      }

      public void __clearDele_EventOnGetRewardButtonClick(AssistanceTeamUIController obj)
      {
        this.m_owner.__clearDele_EventOnGetRewardButtonClick(obj);
      }

      public CommonUIStateController m_stateCtrl
      {
        get
        {
          return this.m_owner.m_stateCtrl;
        }
        set
        {
          this.m_owner.m_stateCtrl = value;
        }
      }

      public GameObject m_charGameObject
      {
        get
        {
          return this.m_owner.m_charGameObject;
        }
        set
        {
          this.m_owner.m_charGameObject = value;
        }
      }

      public Button m_trainingButton
      {
        get
        {
          return this.m_owner.m_trainingButton;
        }
        set
        {
          this.m_owner.m_trainingButton = value;
        }
      }

      public Button m_stopButton
      {
        get
        {
          return this.m_owner.m_stopButton;
        }
        set
        {
          this.m_owner.m_stopButton = value;
        }
      }

      public Button m_getRewardButton
      {
        get
        {
          return this.m_owner.m_getRewardButton;
        }
        set
        {
          this.m_owner.m_getRewardButton = value;
        }
      }

      public Text m_difficultyText
      {
        get
        {
          return this.m_owner.m_difficultyText;
        }
        set
        {
          this.m_owner.m_difficultyText = value;
        }
      }

      public Text m_timeStateText
      {
        get
        {
          return this.m_owner.m_timeStateText;
        }
        set
        {
          this.m_owner.m_timeStateText = value;
        }
      }

      public UISpineGraphic m_playerHeroGraphic
      {
        get
        {
          return this.m_owner.m_playerHeroGraphic;
        }
        set
        {
          this.m_owner.m_playerHeroGraphic = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetSoldierSpine()
      {
        this.m_owner.SetSoldierSpine();
      }

      public void OnTrainingButtonClick()
      {
        this.m_owner.OnTrainingButtonClick();
      }

      public void OnStopButtonClick()
      {
        this.m_owner.OnStopButtonClick();
      }

      public void OnGetRewardButtonClick()
      {
        this.m_owner.OnGetRewardButtonClick();
      }
    }
  }
}
