﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class TestListItemUIController : UIControllerBase
  {
    [AutoBind("./Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Selected", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectedGameObject;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    private int m_id;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(Action<UIControllerBase> clickEvent, Action<UIControllerBase> fillEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetId(int id)
    {
      this.m_id = id;
    }

    public int GetId()
    {
      return this.m_id;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetSelected(bool selected)
    {
      this.m_selectedGameObject.SetActive(selected);
    }

    public bool IsSelected()
    {
      return this.m_selectedGameObject.activeSelf;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClick(UnityAction action)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
