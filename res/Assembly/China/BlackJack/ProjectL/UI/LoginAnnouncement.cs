﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginAnnouncement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class LoginAnnouncement
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public LoginAnnouncement(LoginAnnouncement.AnnounceType type, string title, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Title { get; set; }

    public string Content { get; set; }

    public LoginAnnouncement.AnnounceType CurrentType { get; set; }

    public enum AnnounceType
    {
      Notice = 1,
      Activity = 2,
      None = 3,
    }
  }
}
