﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class WorldUITask : UITask, IClientWorldListener
  {
    private static bool s_isShowWorldUI = true;
    private static bool s_showMainButtonBar = true;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private static WorldUITask s_instance;
    private WorldUIController m_worldUIController;
    private WorldMapUIController m_worldMapUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private RiftUITask m_riftUITask;
    private UnchartedUITask m_unchartedUITask;
    private ArenaSelectUITask m_arenaSelectUITask;
    private ClientWorld m_clientWorld;
    private IConfigDataLoader m_configDataLoader;
    private int m_nowSeconds;
    private bool m_isResuming;
    private bool m_needUpdateEventActors;
    private ResumeNeedNextWorldStepType m_resumeNeedNextWorldStep;
    private List<int> m_movePath;
    [DoNotToLua]
    private WorldUITask.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorString_hotfix;
    private LuaFunction m_Finalize_hotfix;
    private LuaFunction m_IsNeedLoadDynamicRes_hotfix;
    private LuaFunction m_CollectPlayerAssets_hotfix;
    private LuaFunction m_CollectEventAssets_hotfix;
    private LuaFunction m_CollectMonthCardAssets_hotfix;
    private LuaFunction m_InitLayerStateOnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAllUIControllers_hotfix;
    private LuaFunction m_ClearAllContextAndRes_hotfix;
    private LuaFunction m_RegisterPlayerContextEvents_hotfix;
    private LuaFunction m_UnregisterPlayerContextEvents_hotfix;
    private LuaFunction m_OnStartUIIntent_hotfix;
    private LuaFunction m_OnResumeUIIntent_hotfix;
    private LuaFunction m_OnStop_hotfix;
    private LuaFunction m_HideAllView_hotfix;
    private LuaFunction m_OnTick_hotfix;
    private LuaFunction m_InitWorldMapUIController_hotfix;
    private LuaFunction m_UninitWorldMapUIController_hotfix;
    private LuaFunction m_InitWorldUIController_hotfix;
    private LuaFunction m_UninitWorldUIController_hotfix;
    private LuaFunction m_UpdateView_hotfix;
    private LuaFunction m_UpdateRedMarks_hotfix;
    private LuaFunction m_UpdatePlayerInfo_hotfix;
    private LuaFunction m_UpdateActiveScenario_hotfix;
    private LuaFunction m_UpdateHeroRedMark_hotfix;
    private LuaFunction m_UpdateMissionRedMark_hotfix;
    private LuaFunction m_UpdateActivityRedMark_hotfix;
    private LuaFunction m_UpdateFriendRedMark_hotfix;
    private LuaFunction m_UpdateDrillRedMark_hotfix;
    private LuaFunction m_UpdateFetterRedMark_hotfix;
    private LuaFunction m_UpdateGuildRedMark_hotfix;
    private LuaFunction m_UpdateWorldEventRedMark_hotfix;
    private LuaFunction m_UpdateInvestigationButton_hotfix;
    private LuaFunction m_UpdateOpenServiceActivityButton_hotfix;
    private LuaFunction m_UpdateBackFlowActivityButton_hotfix;
    private LuaFunction m_UpdateCooperateBattleButton_hotfix;
    private LuaFunction m_UpdateMonthCardInfo_hotfix;
    private LuaFunction m_UpdatePeakArenaButton_hotfix;
    private LuaFunction m_LoadAndUpdateEventActorsAction_hotfix;
    private LuaFunction m_OnSlowTick_hotfix;
    private LuaFunction m_ShowEventList_hotfix;
    private LuaFunction m_HasRandomEvent_hotfix;
    private LuaFunction m_UpdateNewChatCount_hotfix;
    private LuaFunction m_UpdateNewMailCount_hotfix;
    private LuaFunction m_CreateClientWorld_hotfix;
    private LuaFunction m_DestroyClientWorld_hotfix;
    private LuaFunction m_StartClientWorld_hotfix;
    private LuaFunction m_NextWorldStepBoolean_hotfix;
    private LuaFunction m_WorldUIGetReady_hotfix;
    private LuaFunction m_ClickFirstWaypoint_hotfix;
    private LuaFunction m_StartSetProtagonistNetTaskInt32_hotfix;
    private LuaFunction m_WorldUIController_OnShowMainButtonBarBoolean_hotfix;
    private LuaFunction m_WorldUIController_OnShowPlayerInfo_hotfix;
    private LuaFunction m_WorldUIController_OnCompass_hotfix;
    private LuaFunction m_WorldUIController_OnCurrentScenario_hotfix;
    private LuaFunction m_WorldUIController_OnUnlockScenarioGotoRiftLevelConfigDataRiftLevelInfo_hotfix;
    private LuaFunction m_WorldUIController_OnGotoEventConfigDataWaypointInfoConfigDataEventInfo_hotfix;
    private LuaFunction m_WorldUIController_OnStartPastScenarioConfigDataScenarioInfo_hotfix;
    private LuaFunction m_WorldUIController_OnClosePastScenarioList_hotfix;
    private LuaFunction m_WorldUIController_OnShowEvent_hotfix;
    private LuaFunction m_WorldUIController_OnShowHero_hotfix;
    private LuaFunction m_WorldUIController_OnShowBag_hotfix;
    private LuaFunction m_WorldUIController_OnShowMission_hotfix;
    private LuaFunction m_WorldUIController_OnShowFetter_hotfix;
    private LuaFunction m_WorldUIController_OnShowStore_hotfix;
    private LuaFunction m_WorldUIController_OnShowSelectCard_hotfix;
    private LuaFunction m_WorldUIController_OnShowDrill_hotfix;
    private LuaFunction m_WorldUIController_OnShowFriend_hotfix;
    private LuaFunction m_WorldUIController_OnShowGuild_hotfix;
    private LuaFunction m_WorldUIController_OnShowUncharted_hotfix;
    private LuaFunction m_WorldUIController_OnShowArena_hotfix;
    private LuaFunction m_WorldUIController_OnShowRift_hotfix;
    private LuaFunction m_WorldUIController_OnShowTest_hotfix;
    private LuaFunction m_WorldUIController_OnShowTest2_hotfix;
    private LuaFunction m_WorldUIController_OnShowCooperateBattle_hotfix;
    private LuaFunction m_WorldUIController_OnShowRecommendGiftBox_hotfix;
    private LuaFunction m_RecommendGiftBoxUITask_CloseTask_hotfix;
    private LuaFunction m_WorldUIController_OnShowMail_hotfix;
    private LuaFunction m_WorldUIController_OnShowChat_hotfix;
    private LuaFunction m_WorldUIController_OnShowRanking_hotfix;
    private LuaFunction m_WorldUIController_OnShowActivity_hotfix;
    private LuaFunction m_WorldUIController_OnShowOpenServiceActivity_hotfix;
    private LuaFunction m_WorldUIController_OnShowBackFlowActivity_hotfix;
    private LuaFunction m_WorldUIController_OnPeakArenaButtonClick_hotfix;
    private LuaFunction m_WorldUIController_OnMonthCardButtonClick_hotfix;
    private LuaFunction m_WorldUIController_OnRefreshMonthCardPanelBoolean_hotfix;
    private LuaFunction m_WorldUIController_OnMonthCardItemClickInt32_hotfix;
    private LuaFunction m_WorldUIController_OnMonthCardItemBuyClickInt32_hotfix;
    private LuaFunction m_WorldUIController_OnOpenWebInvestigation_hotfix;
    private LuaFunction m_WorldUIController_OnYYBButtonClick_hotfix;
    private LuaFunction m_WorldUIController_OnOppoButtonClick_hotfix;
    private LuaFunction m_PDSDK_OnDoQuestionSucceed_hotfix;
    private LuaFunction m_PDSDK_OnDoQuestionFailed_hotfix;
    private LuaFunction m_WorldUIController_OnShowSign_hotfix;
    private LuaFunction m_WorldUIController_OnShowActivityNotice_hotfix;
    private LuaFunction m_WorldMapUIController_OnPointerDown_hotfix;
    private LuaFunction m_WorldMapUIController_OnPointerUp_hotfix;
    private LuaFunction m_WorldMapUIController_OnPointerClick_hotfix;
    private LuaFunction m_PlayerResourceUIController_OnAddGold_hotfix;
    private LuaFunction m_PlayerResourceUIController_OnAddCrystal_hotfix;
    private LuaFunction m_PlayerContext_OnMailsChangedNtf_hotfix;
    private LuaFunction m_PlayerContext_OnFriendInviteNtf_hotfix;
    private LuaFunction m_PlayerContext_OnFriendInviteAcceptNtfUserSummary_hotfix;
    private LuaFunction m_PlayerContext_OnFriendshipPointsReceivedNtf_hotfix;
    private LuaFunction m_PlayerContext_EventOnGiftStoreBuyGoodsNtfString_hotfix;
    private LuaFunction m_PlayerContext_OnChatMessageNtfChatMessage_hotfix;
    private LuaFunction m_PlayerContext_OnMailReadAckInt32_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoChangeNtf_hotfix;
    private LuaFunction m_LogBattleRandomSeed_hotfix;
    private LuaFunction m_PlayerContext_OnTeamRoomInviteNtfTeamRoomInviteInfo_hotfix;
    private LuaFunction m_PlayerContext_OnBattlePracticeInvitedNtfPVPInviteInfo_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomTeamBattleJoinNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPVPBattleJoinNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomRealTimePVPBattleJoinNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf_hotfix;
    private LuaFunction m_PlayerContext_OnBattleRoomGuildMassiveCombatBattleJoinNtf_hotfix;
    private LuaFunction m_PlayerContext_EventOnRandomEventUpdate_hotfix;
    private LuaFunction m_PlayerContext_OnPlayerInfoInitEnd_hotfix;
    private LuaFunction m_get_LayerDescArray_hotfix;
    private LuaFunction m_get_UICtrlDescArray_hotfix;
    private LuaFunction m_GetClientWorld_hotfix;
    private LuaFunction m_StartScenarioConfigDataScenarioInfoConfigDataWaypointInfo_hotfix;
    private LuaFunction m_StartWaypointEventConfigDataWaypointInfoConfigDataWaypointInfoConfigDataEventInfo_hotfix;
    private LuaFunction m_StartBattleRoomBattle_hotfix;
    private LuaFunction m__StartBattleLiveRoomBattleInt32_hotfix;
    private LuaFunction m__StartBattleHappeningBattleTypeInt32UInt64Action_hotfix;
    private LuaFunction m_StartHappeningBattleTypeConfigDataBattleInfoInt32ConfigDataDialogInfoConfigDataDialogInfo_hotfix;
    private LuaFunction m_NextHappeningStep_hotfix;
    private LuaFunction m_StartLevelWayPointMoveNetTaskInt32Action`1_hotfix;
    private LuaFunction m_StartLevelScenarioHandleNetTaskInt32Action`1_hotfix;
    private LuaFunction m_OnLevelWayPointMoveNetTaskResultInt32BattleRewardBooleanAction`1_hotfix;
    private LuaFunction m_ShowWaypointRewardBattleRewardBoolean_hotfix;
    private LuaFunction m_GetRewardGoodsUITask_OnClose_hotfix;
    private LuaFunction m_ChestUITask_OnClose_hotfix;
    private LuaFunction m_CheckChangeActiveScenarioBoolean_hotfix;
    private LuaFunction m_Co_ChangeActiveScenarioConfigDataScenarioInfoBoolean_hotfix;
    private LuaFunction m_Co_ChangeActiveScenario35_hotfix;
    private LuaFunction m_Co_PlayWaypointEffectClientWorldWaypoint_hotfix;
    private LuaFunction m_CheckOpenRiftChapter_hotfix;
    private LuaFunction m_CheckOrderReward_hotfix;
    private LuaFunction m_CheckReturnToBeforeBattleUI_hotfix;
    private LuaFunction m_CheckTeamRoomInviteAgain_hotfix;
    private LuaFunction m_CheckOpenTeamRoomInfoUI_hotfix;
    private LuaFunction m_StartTeamRoomInfoUITask_hotfix;
    private LuaFunction m_CheckInBattleRoom_hotfix;
    private LuaFunction m_CheckInPeakArenaMatch_hotfix;
    private LuaFunction m_ShowPeakArenaMatch_hotfix;
    private LuaFunction m_BattleReportEndUITask_OnPeakArenaContinue_hotfix;
    private LuaFunction m_ShowMatchingNowUITask_hotfix;
    private LuaFunction m_CheckInPeakArenaKnockOutUI_hotfix;
    private LuaFunction m_MoveToPrevWaypointBoolean_hotfix;
    private LuaFunction m_OnWaypointClickConfigDataWaypointInfo_hotfix;
    private LuaFunction m_PlayerArriveWaypointConfigDataWaypointInfoConfigDataWaypointInfo_hotfix;
    private LuaFunction m_ShowPastScenarioListConfigDataWaypointInfo_hotfix;
    private LuaFunction m_OnScreenEffectString_hotfix;
    private LuaFunction m_StartShowFadeOutLoadingFadeInActionFadeStyle_hotfix;
    private LuaFunction m_HideFadeOutLoadingFadeIn_hotfix;
    private LuaFunction m_StartPlayerInfoUITask_hotfix;
    private LuaFunction m_PlayerInfoUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_PlayerInfoUITask_OnClose_hotfix;
    private LuaFunction m_StartAppScoreUITask_hotfix;
    private LuaFunction m_AppScoreUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_AppScoreUITask_OnClose_hotfix;
    private LuaFunction m_StartSignUITask_hotfix;
    private LuaFunction m_SignUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartActivityNoticeUITask_hotfix;
    private LuaFunction m_ActivityNoticeUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartHeroUITaskUIIntent_hotfix;
    private LuaFunction m_HeroUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartGuildStoreUITaskUIIntent_hotfix;
    private LuaFunction m_GuildStoreUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartBagUITaskDisplayTypeUIIntent_hotfix;
    private LuaFunction m_BagUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartMissionUITaskUIIntent_hotfix;
    private LuaFunction m_MissionUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartPlayerInfoUITaskByPathUIIntent_hotfix;
    private LuaFunction m_PlayerInfoUITask_OnLoadAllResCompletedByPath_hotfix;
    private LuaFunction m_StartOpenServiceActivityUITask_hotfix;
    private LuaFunction m_OpenServiceActivityUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartBackFlowActivityUITask_hotfix;
    private LuaFunction m_BackFlowActivityUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartFettersUITaskConfigDataHeroDungeonLevelInfoUIIntent_hotfix;
    private LuaFunction m_FettersUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_FetterUITask_StartHeroDungeonHeroUIIntent_hotfix;
    private LuaFunction m_StartArenaSelectUITaskArenaUITypeInt32UIIntent_hotfix;
    private LuaFunction m_ArenaSelectUITask_OnStopTask_hotfix;
    private LuaFunction m_ArenaSelectUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_ArenaSelectUITask_OnShowArenaArenaUITypeInt32UIIntent_hotfix;
    private LuaFunction m_ArenaSelectUITask_OnShowPeakArenaPeakArenaPanelTypeUIIntent_hotfix;
    private LuaFunction m_StartArenaUITaskArenaUITypeInt32UIIntent_hotfix;
    private LuaFunction m_ArenaUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_InitAndStartArenaUITaskArenaUITypeInt32UIIntent_hotfix;
    private LuaFunction m_SendPeakArenaSeasonSimpleInfoNetTask_hotfix;
    private LuaFunction m_SendGetSeasonInfoNetTaskAction_hotfix;
    private LuaFunction m_StartPeakArenaUITaskUIIntentPeakArenaPanelTypeInt32_hotfix;
    private LuaFunction m_PeakArenaUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartArenaBattleReportBasicDataGetNetTaskArenaUITypeInt32UIIntent_hotfix;
    private LuaFunction m_StartStoreUITaskStoreIdUIIntent_hotfix;
    private LuaFunction m_StoreUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartSelectCardUITaskInt32UIIntent_hotfix;
    private LuaFunction m_SelectCardUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartDrillUITaskInt32UIIntent_hotfix;
    private LuaFunction m_DrillUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartFriendUITaskUIIntentBoolean_hotfix;
    private LuaFunction m_StartGuildUITaskUInt64UIIntent_hotfix;
    private LuaFunction m_StartGuildSubUITaskGetPathTypeUIIntent_hotfix;
    private LuaFunction m_GuildUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_GuildManagementUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_FriendUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartMailUITask_hotfix;
    private LuaFunction m_MailUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartRankingUITask_hotfix;
    private LuaFunction m_StartActivityUITaskInt32UIIntent_hotfix;
    private LuaFunction m_ActivityUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartTeamUITaskGameFunctionTypeInt32Int32Int32UIIntent_hotfix;
    private LuaFunction m_TeamUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartTestUITask_hotfix;
    private LuaFunction m_TestUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_TestUITask_OnPauseOrStopTask_hotfix;
    private LuaFunction m_TestUITask_OnStartBattleConfigDataBattleInfo_hotfix;
    private LuaFunction m_StartUnchartedUITaskBattleTypeInt32Int32BooleanUIIntent_hotfix;
    private LuaFunction m_UnchartedUITask_OnStopTask_hotfix;
    private LuaFunction m_UnchartedUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m__StartUnchartedSubUITaskBattleTypeInt32BooleanUIIntent_hotfix;
    private LuaFunction m_StartRiftUITaskConfigDataRiftLevelInfoBooleanBooleanUIIntentNeedGoods_hotfix;
    private LuaFunction m_RiftUITask_OnStopTask_hotfix;
    private LuaFunction m_RiftUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_RiftUITask_GoToScenarioInt32_hotfix;
    private LuaFunction m_StartHeroDungeonUITaskConfigDataHeroDungeonLevelInfoBooleanHeroUIIntent_hotfix;
    private LuaFunction m_HeroDungeonUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartHeroPhantomUITaskConfigDataHeroPhantomInfoUIIntent_hotfix;
    private LuaFunction m_HeroPhantomUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartThearchyUITaskConfigDataThearchyTrialInfoUIIntent_hotfix;
    private LuaFunction m_ThearchyUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartAnikiUITaskConfigDataAnikiGymInfoUIIntent_hotfix;
    private LuaFunction m_AnikiUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartTreasureMapUITaskUIIntent_hotfix;
    private LuaFunction m_TreasureMapUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartMemoryCorridorUITaskConfigDataMemoryCorridorInfoUIIntent_hotfix;
    private LuaFunction m_MemoryCorridorUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartHeroTrainningUITaskConfigDataHeroTrainningInfoUIIntent_hotfix;
    private LuaFunction m_HeroTrainningUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartCooperateBattleUITaskConfigDataCooperateBattleInfoUIIntent_hotfix;
    private LuaFunction m_CooperateBattleUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartUnchartedScoreUITaskConfigDataUnchartedScoreInfoBattleTypeUIIntent_hotfix;
    private LuaFunction m_UnchartedScoreUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartClimbTowerUITaskUIIntent_hotfix;
    private LuaFunction m_ClimbTowerUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartEternalShrineUITaskConfigDataEternalShrineInfoUIIntent_hotfix;
    private LuaFunction m_EternalShrineUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartHeroAnthemUITaskConfigDataHeroAnthemInfoUIIntent_hotfix;
    private LuaFunction m_HeroAnthemUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartCollectionActivityUITaskConfigDataCollectionActivityInfoBooleanUIIntent_hotfix;
    private LuaFunction m_CollectionActivityUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_StartWorldEventMissionUITaskConfigDataEventInfo_hotfix;
    private LuaFunction m_WorldEventMissionUITask_OnClose_hotfix;
    private LuaFunction m_RemoveWorldEventMissionUITaskEvents_hotfix;
    private LuaFunction m_WorldEventMissionUITask_OnCheckEnterMission_hotfix;
    private LuaFunction m_WorldEventMissionUITask_OnEnterMission_hotfix;
    private LuaFunction m_StartGoddessDialogUITask_hotfix;
    private LuaFunction m_GoddessDialogUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_GoddessDialogUITask_OnCloseInt32_hotfix;
    private LuaFunction m_StartDialogUITaskConfigDataDialogInfo_hotfix;
    private LuaFunction m_DialogUITask_OnLoadAllResCompleted_hotfix;
    private LuaFunction m_DialogUITask_OnCloseBoolean_hotfix;
    private LuaFunction m_StartBattleUITaskConfigDataBattleInfoBattleType_hotfix;
    private LuaFunction m__StartArenaBattleUITaskBoolean_hotfix;
    private LuaFunction m_StartArenaReplayBattleUITaskArenaBattleReport_hotfix;
    private LuaFunction m_StartRealTimePVPReplayBattleUITaskRealTimePVPBattleReport_hotfix;
    private LuaFunction m_StartPeakArenaReplayBattleUITaskPeakArenaLadderBattleReportStringPeakArenaBattleReportSourceTypeInt32_hotfix;
    private LuaFunction m_FadeOutAndStartBattleUITask_hotfix;
    private LuaFunction m__StartGetPathTargetUITaskGetPathDataUIIntentNeedGoods_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~WorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPlayerAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEventAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectMonthCardAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWorldMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitWorldMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWorldUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitWorldUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRedMarks()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActivityRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFriendRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDrillRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFetterRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGuildRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWorldEventRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateInvestigationButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOpenServiceActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBackFlowActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCooperateBattleButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadAndUpdateEventActors(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSlowTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEventList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNewChatCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNewMailCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextWorldStep(bool checkWorldUIGetReady = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIGetReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickFirstWaypoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSetProtagonistNetTask(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowMainButtonBar(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnCompass()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnCurrentScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnUnlockScenarioGotoRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnGotoEvent(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnStartPastScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnClosePastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowBag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowFetter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowSelectCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowDrill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowUncharted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowRift()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowTest()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowTest2()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowCooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowRecommendGiftBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RecommendGiftBoxUITask_Close(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowRanking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowOpenServiceActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowBackFlowActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnPeakArenaButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnMonthCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnRefreshMonthCardPanel(bool curIsOpenState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnMonthCardItemClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnMonthCardItemBuyClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnOpenWebInvestigation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnYYBButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnOppoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnDoQuestionSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnDoQuestionFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WorldUIController_OnShowSign()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WorldUIController_OnShowActivityNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void WorldUIController_ShowHide(bool isShow, bool saveShowState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldMapUIController_OnPointerDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldMapUIController_OnPointerUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldMapUIController_OnPointerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnMailsChangedNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnFriendInviteNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnFriendInviteAcceptNtf(UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnFriendshipPointsReceivedNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGiftStoreBuyGoodsNtf(string OrderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatMessageNtf(ChatMessage msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnMailReadAck(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LogBattleRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInviteNtf(TeamRoomInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattlePracticeInvitedNtf(PVPInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomTeamBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomRealTimePVPBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomGuildMassiveCombatBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnRandomEventUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorld GetClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartScenario(
      ConfigDataScenarioInfo scenarioInfo,
      ConfigDataWaypointInfo prevWaypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWaypointEvent(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataWaypointInfo prevWaypointInfo,
      ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartBattleLiveRoomBattle(int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartBattleLiveRoomBattle(int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartBattleHappening(
      BattleType battleType,
      int locationId,
      ulong instanceId = 0,
      Action onStepEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool _StartBattleHappening(
      BattleType battleType,
      int levelId,
      ulong instanceId = 0,
      Action onStepEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHappening(
      BattleType battleType,
      ConfigDataBattleInfo battleInfo,
      int monsterLevel,
      ConfigDataDialogInfo dialogBefore = null,
      ConfigDataDialogInfo dialogAfter = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextHappeningStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HandleAttackFailResult(int result, UIIntent currIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelWayPointMoveNetTask(int waypointId, Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelScenarioHandleNetTask(int scenarioId, Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelWayPointMoveNetTaskResult(
      int result,
      BattleReward reward,
      bool isScenario,
      Action<int> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowWaypointReward(BattleReward reward, bool isChest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckChangeActiveScenario(bool checkWorldUIGetReady = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeActiveScenario(
      ConfigDataScenarioInfo scenarioInfo,
      bool checkWorldUIGetReady)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeActiveScenario35()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayWaypointEffect(ClientWorldWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenRiftChapter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckOrderReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckReturnToBeforeBattleUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckTeamRoomInviteAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenTeamRoomInfoUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckInBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckInPeakArenaMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPeakArenaMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPeakArenaContinue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMatchingNowUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckInPeakArenaKnockOutUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveToPrevWaypoint(bool moveCamera = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartBattleArmyRandomSeedGetNetTask(int battleId, Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartDanmakuGetNetTaskNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerArriveWaypoint(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataWaypointInfo prevWaypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ShowPastScenarioList(ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartShowFadeOutLoadingFadeIn(Action fadeOutEnd, FadeStyle style = FadeStyle.Black)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideFadeOutLoadingFadeIn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPlayerInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAppScoreUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppScoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppScoreUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSignUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SignUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartActivityNoticeUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityNoticeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildStoreUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildStoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBagUITask(BagListUIController.DisplayType displayType = BagListUIController.DisplayType.None, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMissionUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MissionUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPlayerInfoUITaskByPath(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUITask_OnLoadAllResCompletedByPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartOpenServiceActivityUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenServiceActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBackFlowActivityUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BackFlowActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartFettersUITask(
      ConfigDataHeroDungeonLevelInfo heroDungeonLevelInfo = null,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FetterUITask_StartHeroDungeon(Hero hero, UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaSelectUITask(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnShowArena(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnShowPeakArena(
      PeakArenaPanelType peakArenaPanelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaUITask(ArenaUIType arenaUIType, int panelType, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitAndStartArenaUITask(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendPeakArenaSeasonSimpleInfoNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGetSeasonInfoNetTask(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaUITask(
      UIIntent fromIntent = null,
      PeakArenaPanelType tabType = PeakArenaPanelType.Clash,
      int matchGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleReportBasicDataGetNetTask(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartStoreUITask(StoreId storeId = StoreId.StoreId_None, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSelectCardUITask(int cardPoolId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDrillUITask(int drillState = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartFriendUITask(UIIntent fromIntent = null, bool openAddFriend = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildUITask(ulong guildMassiveCombatInstanceId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildSubUITask(GetPathType UIName, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMailUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MailUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRankingUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartActivityUITask(int activityId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamUITask(
      GameFunctionType gameFunctionType = GameFunctionType.GameFunctionType_None,
      int chapterId = 0,
      int locationId = 0,
      int collectionActivityId = 0,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTestUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUITask_OnPauseOrStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUITask_OnStartBattle(ConfigDataBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedUITask(
      BattleType battleType = BattleType.None,
      int chapterId = 0,
      int tabIndex = 0,
      bool isAfterBattle = false,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUnchartedSubUITask(
      BattleType battleType,
      int chapterId,
      bool isAfterBattle,
      UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartUnchartedSubUITask(
      BattleType battleType,
      int chapterId,
      bool isAfterBattle,
      UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftUITask(
      ConfigDataRiftLevelInfo levelInfo,
      bool openLevelInfo,
      bool needReturnToChapter = true,
      UIIntent fromIntent = null,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftUITask_GoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonUITask(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      bool openLevelInfo,
      Hero hero = null,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDungeonUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroPhantomUITask(
      ConfigDataHeroPhantomInfo heroPhantomInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroPhantomUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartThearchyUITask(
      ConfigDataThearchyTrialInfo thearchyTrialInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ThearchyUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAnikiUITask(ConfigDataAnikiGymInfo anikiGymInfo, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTreasureMapUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TreasureMapUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMemoryCorridorUITask(
      ConfigDataMemoryCorridorInfo memoryCorridorInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroTrainningUITask(
      ConfigDataHeroTrainningInfo heroTrainningInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroTrainningUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCooperateBattleUITask(
      ConfigDataCooperateBattleInfo cooperateBattleInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CooperateBattleUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedScoreUITask(
      ConfigDataUnchartedScoreInfo unchartedScoreInfo,
      BattleType battleType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedScoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartEternalShrineUITask(
      ConfigDataEternalShrineInfo eternalShrineInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroAnthemUITask(ConfigDataHeroAnthemInfo heroAnthemInfo, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionActivityUITask(
      ConfigDataCollectionActivityInfo collectionActivityInfo,
      bool isAfterBattle = false,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWorldEventMissionUITask(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveWorldEventMissionUITaskEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool WorldEventMissionUITask_OnCheckEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGoddessDialogUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUITask_OnClose(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDialogUITask(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUITask_OnClose(bool isSkip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleUITask(ConfigDataBattleInfo battleInfo, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void HideInviteAndChatUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void UnloadAssetsAndStartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void BattleUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartArenaBattleUITask(bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartArenaBattleUITask(bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsBattleReportVersionValid(int battleReportVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartArenaBattleReport(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaReplayBattleUITask(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartRealTimePVPBattleReport(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPReplayBattleUITask(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaBattleReport(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType,
      int matchGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void _StartPeakArenaBattleReport(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType,
      int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaReplayBattleUITask(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType,
      int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeOutAndStartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static int CanGotoGetPath(GetPathData getPathInfo, ref FadeStyle fadeStyle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartGetPathTargetUITask(
      GetPathData getPath,
      UIIntent fromIntent,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartGetPathTargetUITask(
      GetPathData getPath,
      UIIntent fromIntent,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public WorldUITask.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private bool __callBase_OnStart(UIIntent intent)
    {
      return base.OnStart(intent);
    }

    private void __callBase_OnStop()
    {
      base.OnStop();
    }

    private void __callBase_OnPause()
    {
      this.OnPause();
    }

    private bool __callBase_OnResume(UIIntent intent)
    {
      return base.OnResume(intent);
    }

    private bool __callBase_OnNewIntent(UIIntent intent)
    {
      return this.OnNewIntent(intent);
    }

    private void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
    {
      this.EnableUIInput(isEnable, isGlobalEnable);
    }

    private bool __callBase_IsNeedLoadDynamicRes()
    {
      return base.IsNeedLoadDynamicRes();
    }

    private List<string> __callBase_CollectAllDynamicResForLoad()
    {
      return this.CollectAllDynamicResForLoad();
    }

    private void __callBase_RegisterPlayerContextEvents()
    {
      base.RegisterPlayerContextEvents();
    }

    private void __callBase_UnregisterPlayerContextEvents()
    {
      base.UnregisterPlayerContextEvents();
    }

    private void __callBase_PostUpdateView()
    {
      this.PostUpdateView();
    }

    private void __callBase_CollectPreloadResourceList()
    {
      this.CollectPreloadResourceList();
    }

    private void __callBase_ClearAssetList()
    {
      this.ClearAssetList();
    }

    private void __callBase_CollectAsset(string assetName)
    {
      this.CollectAsset(assetName);
    }

    private void __callBase_CollectSpriteAsset(string assetName)
    {
      this.CollectSpriteAsset(assetName);
    }

    private void __callBase_CollectFxAsset(string assetName)
    {
      this.CollectFxAsset(assetName);
    }

    private void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
    {
      this.StartLoadCustomAssets(onLoadCompleted);
    }

    private bool __callBase_IsLoadingCustomAssets()
    {
      return this.IsLoadingCustomAssets();
    }

    private bool __callBase_IsPipeLineRunning()
    {
      return this.IsPipeLineRunning();
    }

    private bool __callBase_IsOpeningUI()
    {
      return this.IsOpeningUI();
    }

    private UITaskBase __callBase_ReturnPrevUITask()
    {
      return this.ReturnPrevUITask();
    }

    private void __callBase_ClearUnusedDynamicResourceCache()
    {
      this.ClearUnusedDynamicResourceCache();
    }

    private void __callBase_ClearDynamicResourceCache(List<string> assets)
    {
      this.ClearDynamicResourceCache(assets);
    }

    private void __callBase_OnMemoryWarning()
    {
      this.OnMemoryWarning();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private WorldUITask m_owner;

      public LuaExportHelper(WorldUITask owner)
      {
        this.m_owner = owner;
      }

      public bool __callBase_OnStart(UIIntent intent)
      {
        return this.m_owner.__callBase_OnStart(intent);
      }

      public void __callBase_OnStop()
      {
        this.m_owner.__callBase_OnStop();
      }

      public void __callBase_OnPause()
      {
        this.m_owner.__callBase_OnPause();
      }

      public bool __callBase_OnResume(UIIntent intent)
      {
        return this.m_owner.__callBase_OnResume(intent);
      }

      public bool __callBase_OnNewIntent(UIIntent intent)
      {
        return this.m_owner.__callBase_OnNewIntent(intent);
      }

      public void __callBase_EnableUIInput(bool isEnable, bool? isGlobalEnable)
      {
        this.m_owner.__callBase_EnableUIInput(isEnable, isGlobalEnable);
      }

      public bool __callBase_IsNeedLoadDynamicRes()
      {
        return this.m_owner.__callBase_IsNeedLoadDynamicRes();
      }

      public List<string> __callBase_CollectAllDynamicResForLoad()
      {
        return this.m_owner.__callBase_CollectAllDynamicResForLoad();
      }

      public void __callBase_RegisterPlayerContextEvents()
      {
        this.m_owner.__callBase_RegisterPlayerContextEvents();
      }

      public void __callBase_UnregisterPlayerContextEvents()
      {
        this.m_owner.__callBase_UnregisterPlayerContextEvents();
      }

      public void __callBase_PostUpdateView()
      {
        this.m_owner.__callBase_PostUpdateView();
      }

      public void __callBase_CollectPreloadResourceList()
      {
        this.m_owner.__callBase_CollectPreloadResourceList();
      }

      public void __callBase_ClearAssetList()
      {
        this.m_owner.__callBase_ClearAssetList();
      }

      public void __callBase_CollectAsset(string assetName)
      {
        this.m_owner.__callBase_CollectAsset(assetName);
      }

      public void __callBase_CollectSpriteAsset(string assetName)
      {
        this.m_owner.__callBase_CollectSpriteAsset(assetName);
      }

      public void __callBase_CollectFxAsset(string assetName)
      {
        this.m_owner.__callBase_CollectFxAsset(assetName);
      }

      public void __callBase_StartLoadCustomAssets(Action onLoadCompleted)
      {
        this.m_owner.__callBase_StartLoadCustomAssets(onLoadCompleted);
      }

      public bool __callBase_IsLoadingCustomAssets()
      {
        return this.m_owner.__callBase_IsLoadingCustomAssets();
      }

      public bool __callBase_IsPipeLineRunning()
      {
        return this.m_owner.__callBase_IsPipeLineRunning();
      }

      public bool __callBase_IsOpeningUI()
      {
        return this.m_owner.__callBase_IsOpeningUI();
      }

      public UITaskBase __callBase_ReturnPrevUITask()
      {
        return this.m_owner.__callBase_ReturnPrevUITask();
      }

      public void __callBase_ClearUnusedDynamicResourceCache()
      {
        this.m_owner.__callBase_ClearUnusedDynamicResourceCache();
      }

      public void __callBase_ClearDynamicResourceCache(List<string> assets)
      {
        this.m_owner.__callBase_ClearDynamicResourceCache(assets);
      }

      public void __callBase_OnMemoryWarning()
      {
        this.m_owner.__callBase_OnMemoryWarning();
      }

      public UITaskBase.LayerDesc[] m_layerDescArray
      {
        get
        {
          return this.m_owner.m_layerDescArray;
        }
        set
        {
          this.m_owner.m_layerDescArray = value;
        }
      }

      public UITaskBase.UIControllerDesc[] m_uiCtrlDescArray
      {
        get
        {
          return this.m_owner.m_uiCtrlDescArray;
        }
        set
        {
          this.m_owner.m_uiCtrlDescArray = value;
        }
      }

      public static WorldUITask s_instance
      {
        get
        {
          return WorldUITask.s_instance;
        }
        set
        {
          WorldUITask.s_instance = value;
        }
      }

      public static bool s_isShowWorldUI
      {
        get
        {
          return WorldUITask.s_isShowWorldUI;
        }
        set
        {
          WorldUITask.s_isShowWorldUI = value;
        }
      }

      public WorldUIController m_worldUIController
      {
        get
        {
          return this.m_owner.m_worldUIController;
        }
        set
        {
          this.m_owner.m_worldUIController = value;
        }
      }

      public WorldMapUIController m_worldMapUIController
      {
        get
        {
          return this.m_owner.m_worldMapUIController;
        }
        set
        {
          this.m_owner.m_worldMapUIController = value;
        }
      }

      public PlayerResourceUIController m_playerResourceUIController
      {
        get
        {
          return this.m_owner.m_playerResourceUIController;
        }
        set
        {
          this.m_owner.m_playerResourceUIController = value;
        }
      }

      public RiftUITask m_riftUITask
      {
        get
        {
          return this.m_owner.m_riftUITask;
        }
        set
        {
          this.m_owner.m_riftUITask = value;
        }
      }

      public UnchartedUITask m_unchartedUITask
      {
        get
        {
          return this.m_owner.m_unchartedUITask;
        }
        set
        {
          this.m_owner.m_unchartedUITask = value;
        }
      }

      public ArenaSelectUITask m_arenaSelectUITask
      {
        get
        {
          return this.m_owner.m_arenaSelectUITask;
        }
        set
        {
          this.m_owner.m_arenaSelectUITask = value;
        }
      }

      public ClientWorld m_clientWorld
      {
        get
        {
          return this.m_owner.m_clientWorld;
        }
        set
        {
          this.m_owner.m_clientWorld = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public int m_nowSeconds
      {
        get
        {
          return this.m_owner.m_nowSeconds;
        }
        set
        {
          this.m_owner.m_nowSeconds = value;
        }
      }

      public bool m_isResuming
      {
        get
        {
          return this.m_owner.m_isResuming;
        }
        set
        {
          this.m_owner.m_isResuming = value;
        }
      }

      public bool m_needUpdateEventActors
      {
        get
        {
          return this.m_owner.m_needUpdateEventActors;
        }
        set
        {
          this.m_owner.m_needUpdateEventActors = value;
        }
      }

      public ResumeNeedNextWorldStepType m_resumeNeedNextWorldStep
      {
        get
        {
          return this.m_owner.m_resumeNeedNextWorldStep;
        }
        set
        {
          this.m_owner.m_resumeNeedNextWorldStep = value;
        }
      }

      public static bool s_showMainButtonBar
      {
        get
        {
          return WorldUITask.s_showMainButtonBar;
        }
        set
        {
          WorldUITask.s_showMainButtonBar = value;
        }
      }

      public List<int> m_movePath
      {
        get
        {
          return this.m_owner.m_movePath;
        }
        set
        {
          this.m_owner.m_movePath = value;
        }
      }

      public UITaskBase.LayerDesc[] LayerDescArray
      {
        get
        {
          return this.m_owner.LayerDescArray;
        }
      }

      public UITaskBase.UIControllerDesc[] UICtrlDescArray
      {
        get
        {
          return this.m_owner.UICtrlDescArray;
        }
      }

      public bool IsNeedLoadDynamicRes()
      {
        return this.m_owner.IsNeedLoadDynamicRes();
      }

      public void CollectPlayerAssets()
      {
        this.m_owner.CollectPlayerAssets();
      }

      public void CollectEventAssets()
      {
        this.m_owner.CollectEventAssets();
      }

      public void CollectMonthCardAssets()
      {
        this.m_owner.CollectMonthCardAssets();
      }

      public void InitLayerStateOnLoadAllResCompleted()
      {
        this.m_owner.InitLayerStateOnLoadAllResCompleted();
      }

      public void InitAllUIControllers()
      {
        this.m_owner.InitAllUIControllers();
      }

      public void ClearAllContextAndRes()
      {
        this.m_owner.ClearAllContextAndRes();
      }

      public void RegisterPlayerContextEvents()
      {
        this.m_owner.RegisterPlayerContextEvents();
      }

      public void UnregisterPlayerContextEvents()
      {
        this.m_owner.UnregisterPlayerContextEvents();
      }

      public bool OnStart(UIIntent intent)
      {
        return this.m_owner.OnStart(intent);
      }

      public bool OnResume(UIIntent intent)
      {
        return this.m_owner.OnResume(intent);
      }

      public void OnStop()
      {
        this.m_owner.OnStop();
      }

      public void HideAllView()
      {
        this.m_owner.HideAllView();
      }

      public void OnTick()
      {
        this.m_owner.OnTick();
      }

      public void InitWorldMapUIController()
      {
        this.m_owner.InitWorldMapUIController();
      }

      public void UninitWorldMapUIController()
      {
        this.m_owner.UninitWorldMapUIController();
      }

      public void InitWorldUIController()
      {
        this.m_owner.InitWorldUIController();
      }

      public void UninitWorldUIController()
      {
        this.m_owner.UninitWorldUIController();
      }

      public void UpdateView()
      {
        this.m_owner.UpdateView();
      }

      public void UpdateRedMarks()
      {
        this.m_owner.UpdateRedMarks();
      }

      public void UpdatePlayerInfo()
      {
        this.m_owner.UpdatePlayerInfo();
      }

      public void UpdateActiveScenario()
      {
        this.m_owner.UpdateActiveScenario();
      }

      public void UpdateHeroRedMark()
      {
        this.m_owner.UpdateHeroRedMark();
      }

      public void UpdateMissionRedMark()
      {
        this.m_owner.UpdateMissionRedMark();
      }

      public void UpdateActivityRedMark()
      {
        this.m_owner.UpdateActivityRedMark();
      }

      public void UpdateFriendRedMark()
      {
        this.m_owner.UpdateFriendRedMark();
      }

      public void UpdateDrillRedMark()
      {
        this.m_owner.UpdateDrillRedMark();
      }

      public void UpdateFetterRedMark()
      {
        this.m_owner.UpdateFetterRedMark();
      }

      public void UpdateGuildRedMark()
      {
        this.m_owner.UpdateGuildRedMark();
      }

      public void UpdateWorldEventRedMark()
      {
        this.m_owner.UpdateWorldEventRedMark();
      }

      public void UpdateInvestigationButton()
      {
        this.m_owner.UpdateInvestigationButton();
      }

      public void UpdateOpenServiceActivityButton()
      {
        this.m_owner.UpdateOpenServiceActivityButton();
      }

      public void UpdateBackFlowActivityButton()
      {
        this.m_owner.UpdateBackFlowActivityButton();
      }

      public void UpdateCooperateBattleButton()
      {
        this.m_owner.UpdateCooperateBattleButton();
      }

      public void UpdateMonthCardInfo()
      {
        this.m_owner.UpdateMonthCardInfo();
      }

      public void UpdatePeakArenaButton()
      {
        this.m_owner.UpdatePeakArenaButton();
      }

      public void LoadAndUpdateEventActors(Action onEnd)
      {
        this.m_owner.LoadAndUpdateEventActors(onEnd);
      }

      public void OnSlowTick()
      {
        this.m_owner.OnSlowTick();
      }

      public void ShowEventList()
      {
        this.m_owner.ShowEventList();
      }

      public bool HasRandomEvent()
      {
        return this.m_owner.HasRandomEvent();
      }

      public void UpdateNewChatCount()
      {
        this.m_owner.UpdateNewChatCount();
      }

      public void UpdateNewMailCount()
      {
        this.m_owner.UpdateNewMailCount();
      }

      public void CreateClientWorld()
      {
        this.m_owner.CreateClientWorld();
      }

      public void DestroyClientWorld()
      {
        this.m_owner.DestroyClientWorld();
      }

      public void StartClientWorld()
      {
        this.m_owner.StartClientWorld();
      }

      public void NextWorldStep(bool checkWorldUIGetReady)
      {
        this.m_owner.NextWorldStep(checkWorldUIGetReady);
      }

      public void WorldUIGetReady()
      {
        this.m_owner.WorldUIGetReady();
      }

      public void ClickFirstWaypoint()
      {
        this.m_owner.ClickFirstWaypoint();
      }

      public void StartSetProtagonistNetTask(int protagonistId)
      {
        this.m_owner.StartSetProtagonistNetTask(protagonistId);
      }

      public void WorldUIController_OnShowMainButtonBar(bool show)
      {
        this.m_owner.WorldUIController_OnShowMainButtonBar(show);
      }

      public void WorldUIController_OnShowPlayerInfo()
      {
        this.m_owner.WorldUIController_OnShowPlayerInfo();
      }

      public void WorldUIController_OnCompass()
      {
        this.m_owner.WorldUIController_OnCompass();
      }

      public void WorldUIController_OnCurrentScenario()
      {
        this.m_owner.WorldUIController_OnCurrentScenario();
      }

      public void WorldUIController_OnUnlockScenarioGotoRiftLevel(
        ConfigDataRiftLevelInfo riftLevelInfo)
      {
        this.m_owner.WorldUIController_OnUnlockScenarioGotoRiftLevel(riftLevelInfo);
      }

      public void WorldUIController_OnGotoEvent(
        ConfigDataWaypointInfo waypointInfo,
        ConfigDataEventInfo eventInfo)
      {
        this.m_owner.WorldUIController_OnGotoEvent(waypointInfo, eventInfo);
      }

      public void WorldUIController_OnStartPastScenario(ConfigDataScenarioInfo scenarioInfo)
      {
        this.m_owner.WorldUIController_OnStartPastScenario(scenarioInfo);
      }

      public void WorldUIController_OnClosePastScenarioList()
      {
        this.m_owner.WorldUIController_OnClosePastScenarioList();
      }

      public void WorldUIController_OnShowEvent()
      {
        this.m_owner.WorldUIController_OnShowEvent();
      }

      public void WorldUIController_OnShowHero()
      {
        this.m_owner.WorldUIController_OnShowHero();
      }

      public void WorldUIController_OnShowBag()
      {
        this.m_owner.WorldUIController_OnShowBag();
      }

      public void WorldUIController_OnShowMission()
      {
        this.m_owner.WorldUIController_OnShowMission();
      }

      public void WorldUIController_OnShowFetter()
      {
        this.m_owner.WorldUIController_OnShowFetter();
      }

      public void WorldUIController_OnShowStore()
      {
        this.m_owner.WorldUIController_OnShowStore();
      }

      public void WorldUIController_OnShowSelectCard()
      {
        this.m_owner.WorldUIController_OnShowSelectCard();
      }

      public void WorldUIController_OnShowDrill()
      {
        this.m_owner.WorldUIController_OnShowDrill();
      }

      public void WorldUIController_OnShowFriend()
      {
        this.m_owner.WorldUIController_OnShowFriend();
      }

      public void WorldUIController_OnShowGuild()
      {
        this.m_owner.WorldUIController_OnShowGuild();
      }

      public void WorldUIController_OnShowUncharted()
      {
        this.m_owner.WorldUIController_OnShowUncharted();
      }

      public void WorldUIController_OnShowArena()
      {
        this.m_owner.WorldUIController_OnShowArena();
      }

      public void WorldUIController_OnShowRift()
      {
        this.m_owner.WorldUIController_OnShowRift();
      }

      public void WorldUIController_OnShowTest()
      {
        this.m_owner.WorldUIController_OnShowTest();
      }

      public void WorldUIController_OnShowTest2()
      {
        this.m_owner.WorldUIController_OnShowTest2();
      }

      public void WorldUIController_OnShowCooperateBattle()
      {
        this.m_owner.WorldUIController_OnShowCooperateBattle();
      }

      public void WorldUIController_OnShowRecommendGiftBox()
      {
        this.m_owner.WorldUIController_OnShowRecommendGiftBox();
      }

      public void RecommendGiftBoxUITask_Close(Task task)
      {
        this.m_owner.RecommendGiftBoxUITask_Close(task);
      }

      public void WorldUIController_OnShowMail()
      {
        this.m_owner.WorldUIController_OnShowMail();
      }

      public void WorldUIController_OnShowChat()
      {
        this.m_owner.WorldUIController_OnShowChat();
      }

      public void WorldUIController_OnShowRanking()
      {
        this.m_owner.WorldUIController_OnShowRanking();
      }

      public void WorldUIController_OnShowActivity()
      {
        this.m_owner.WorldUIController_OnShowActivity();
      }

      public void WorldUIController_OnShowOpenServiceActivity()
      {
        this.m_owner.WorldUIController_OnShowOpenServiceActivity();
      }

      public void WorldUIController_OnShowBackFlowActivity()
      {
        this.m_owner.WorldUIController_OnShowBackFlowActivity();
      }

      public void WorldUIController_OnPeakArenaButtonClick()
      {
        this.m_owner.WorldUIController_OnPeakArenaButtonClick();
      }

      public void WorldUIController_OnMonthCardButtonClick()
      {
        this.m_owner.WorldUIController_OnMonthCardButtonClick();
      }

      public void WorldUIController_OnRefreshMonthCardPanel(bool curIsOpenState)
      {
        this.m_owner.WorldUIController_OnRefreshMonthCardPanel(curIsOpenState);
      }

      public void WorldUIController_OnMonthCardItemClick(int cardId)
      {
        this.m_owner.WorldUIController_OnMonthCardItemClick(cardId);
      }

      public void WorldUIController_OnMonthCardItemBuyClick(int cardId)
      {
        this.m_owner.WorldUIController_OnMonthCardItemBuyClick(cardId);
      }

      public void WorldUIController_OnOpenWebInvestigation()
      {
        this.m_owner.WorldUIController_OnOpenWebInvestigation();
      }

      public void WorldUIController_OnYYBButtonClick()
      {
        this.m_owner.WorldUIController_OnYYBButtonClick();
      }

      public void WorldUIController_OnOppoButtonClick()
      {
        this.m_owner.WorldUIController_OnOppoButtonClick();
      }

      public void PDSDK_OnDoQuestionSucceed()
      {
        this.m_owner.PDSDK_OnDoQuestionSucceed();
      }

      public void PDSDK_OnDoQuestionFailed()
      {
        this.m_owner.PDSDK_OnDoQuestionFailed();
      }

      public void WorldMapUIController_OnPointerDown()
      {
        this.m_owner.WorldMapUIController_OnPointerDown();
      }

      public void WorldMapUIController_OnPointerUp()
      {
        this.m_owner.WorldMapUIController_OnPointerUp();
      }

      public void WorldMapUIController_OnPointerClick()
      {
        this.m_owner.WorldMapUIController_OnPointerClick();
      }

      public void PlayerResourceUIController_OnAddGold()
      {
        this.m_owner.PlayerResourceUIController_OnAddGold();
      }

      public void PlayerResourceUIController_OnAddCrystal()
      {
        this.m_owner.PlayerResourceUIController_OnAddCrystal();
      }

      public void PlayerContext_OnMailsChangedNtf()
      {
        this.m_owner.PlayerContext_OnMailsChangedNtf();
      }

      public void PlayerContext_OnFriendInviteNtf()
      {
        this.m_owner.PlayerContext_OnFriendInviteNtf();
      }

      public void PlayerContext_OnFriendInviteAcceptNtf(UserSummary userSummy)
      {
        this.m_owner.PlayerContext_OnFriendInviteAcceptNtf(userSummy);
      }

      public void PlayerContext_OnFriendshipPointsReceivedNtf()
      {
        this.m_owner.PlayerContext_OnFriendshipPointsReceivedNtf();
      }

      public void PlayerContext_EventOnGiftStoreBuyGoodsNtf(string OrderId)
      {
        this.m_owner.PlayerContext_EventOnGiftStoreBuyGoodsNtf(OrderId);
      }

      public void PlayerContext_OnChatMessageNtf(ChatMessage msg)
      {
        this.m_owner.PlayerContext_OnChatMessageNtf(msg);
      }

      public void PlayerContext_OnMailReadAck(int obj)
      {
        this.m_owner.PlayerContext_OnMailReadAck(obj);
      }

      public void PlayerContext_OnPlayerInfoChangeNtf()
      {
        this.m_owner.PlayerContext_OnPlayerInfoChangeNtf();
      }

      public void LogBattleRandomSeed()
      {
        this.m_owner.LogBattleRandomSeed();
      }

      public void PlayerContext_OnTeamRoomInviteNtf(TeamRoomInviteInfo inviteInfo)
      {
        this.m_owner.PlayerContext_OnTeamRoomInviteNtf(inviteInfo);
      }

      public void PlayerContext_OnBattlePracticeInvitedNtf(PVPInviteInfo inviteInfo)
      {
        this.m_owner.PlayerContext_OnBattlePracticeInvitedNtf(inviteInfo);
      }

      public void PlayerContext_OnBattleRoomTeamBattleJoinNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomTeamBattleJoinNtf();
      }

      public void PlayerContext_OnBattleRoomPVPBattleJoinNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomPVPBattleJoinNtf();
      }

      public void PlayerContext_OnBattleRoomRealTimePVPBattleJoinNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomRealTimePVPBattleJoinNtf();
      }

      public void PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf();
      }

      public void PlayerContext_OnBattleRoomGuildMassiveCombatBattleJoinNtf()
      {
        this.m_owner.PlayerContext_OnBattleRoomGuildMassiveCombatBattleJoinNtf();
      }

      public void PlayerContext_EventOnRandomEventUpdate()
      {
        this.m_owner.PlayerContext_EventOnRandomEventUpdate();
      }

      public void PlayerContext_OnPlayerInfoInitEnd()
      {
        this.m_owner.PlayerContext_OnPlayerInfoInitEnd();
      }

      public void StartScenario(
        ConfigDataScenarioInfo scenarioInfo,
        ConfigDataWaypointInfo prevWaypointInfo)
      {
        this.m_owner.StartScenario(scenarioInfo, prevWaypointInfo);
      }

      public void StartWaypointEvent(
        ConfigDataWaypointInfo waypointInfo,
        ConfigDataWaypointInfo prevWaypointInfo,
        ConfigDataEventInfo eventInfo)
      {
        this.m_owner.StartWaypointEvent(waypointInfo, prevWaypointInfo, eventInfo);
      }

      public void StartBattleRoomBattle()
      {
        this.m_owner.StartBattleRoomBattle();
      }

      public void _StartBattleLiveRoomBattle(int matchGroupId)
      {
        this.m_owner._StartBattleLiveRoomBattle(matchGroupId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool _StartBattleHappening(
        BattleType battleType,
        int levelId,
        ulong instanceId,
        Action onStepEnd)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartHappening(
        BattleType battleType,
        ConfigDataBattleInfo battleInfo,
        int monsterLevel,
        ConfigDataDialogInfo dialogBefore,
        ConfigDataDialogInfo dialogAfter)
      {
        // ISSUE: unable to decompile the method.
      }

      public void NextHappeningStep()
      {
        this.m_owner.NextHappeningStep();
      }

      public void StartLevelWayPointMoveNetTask(int waypointId, Action<int> onEnd)
      {
        this.m_owner.StartLevelWayPointMoveNetTask(waypointId, onEnd);
      }

      public void StartLevelScenarioHandleNetTask(int scenarioId, Action<int> onEnd)
      {
        this.m_owner.StartLevelScenarioHandleNetTask(scenarioId, onEnd);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void OnLevelWayPointMoveNetTaskResult(
        int result,
        BattleReward reward,
        bool isScenario,
        Action<int> onEnd)
      {
        // ISSUE: unable to decompile the method.
      }

      public void ShowWaypointReward(BattleReward reward, bool isChest)
      {
        this.m_owner.ShowWaypointReward(reward, isChest);
      }

      public void GetRewardGoodsUITask_OnClose()
      {
        this.m_owner.GetRewardGoodsUITask_OnClose();
      }

      public void ChestUITask_OnClose()
      {
        this.m_owner.ChestUITask_OnClose();
      }

      public bool CheckChangeActiveScenario(bool checkWorldUIGetReady)
      {
        return this.m_owner.CheckChangeActiveScenario(checkWorldUIGetReady);
      }

      public IEnumerator Co_ChangeActiveScenario(
        ConfigDataScenarioInfo scenarioInfo,
        bool checkWorldUIGetReady)
      {
        return this.m_owner.Co_ChangeActiveScenario(scenarioInfo, checkWorldUIGetReady);
      }

      public IEnumerator Co_ChangeActiveScenario35()
      {
        return this.m_owner.Co_ChangeActiveScenario35();
      }

      public IEnumerator Co_PlayWaypointEffect(ClientWorldWaypoint wp)
      {
        return this.m_owner.Co_PlayWaypointEffect(wp);
      }

      public bool CheckOpenRiftChapter()
      {
        return this.m_owner.CheckOpenRiftChapter();
      }

      public void CheckOrderReward()
      {
        this.m_owner.CheckOrderReward();
      }

      public bool CheckReturnToBeforeBattleUI()
      {
        return this.m_owner.CheckReturnToBeforeBattleUI();
      }

      public bool CheckTeamRoomInviteAgain()
      {
        return this.m_owner.CheckTeamRoomInviteAgain();
      }

      public bool CheckOpenTeamRoomInfoUI()
      {
        return this.m_owner.CheckOpenTeamRoomInfoUI();
      }

      public void StartTeamRoomInfoUITask()
      {
        this.m_owner.StartTeamRoomInfoUITask();
      }

      public bool CheckInBattleRoom()
      {
        return this.m_owner.CheckInBattleRoom();
      }

      public bool CheckInPeakArenaMatch()
      {
        return this.m_owner.CheckInPeakArenaMatch();
      }

      public void ShowPeakArenaMatch()
      {
        this.m_owner.ShowPeakArenaMatch();
      }

      public void BattleReportEndUITask_OnPeakArenaContinue()
      {
        this.m_owner.BattleReportEndUITask_OnPeakArenaContinue();
      }

      public void ShowMatchingNowUITask()
      {
        this.m_owner.ShowMatchingNowUITask();
      }

      public bool CheckInPeakArenaKnockOutUI()
      {
        return this.m_owner.CheckInPeakArenaKnockOutUI();
      }

      public void MoveToPrevWaypoint(bool moveCamera)
      {
        this.m_owner.MoveToPrevWaypoint(moveCamera);
      }

      public static void StartBattleArmyRandomSeedGetNetTask(int battleId, Action<int> onEnd)
      {
        WorldUITask.StartBattleArmyRandomSeedGetNetTask(battleId, onEnd);
      }

      public static void StartDanmakuGetNetTaskNetTask(Action<int> onEnd)
      {
        WorldUITask.StartDanmakuGetNetTaskNetTask(onEnd);
      }

      public void PlayerArriveWaypoint(
        ConfigDataWaypointInfo waypointInfo,
        ConfigDataWaypointInfo prevWaypointInfo)
      {
        this.m_owner.PlayerArriveWaypoint(waypointInfo, prevWaypointInfo);
      }

      public bool ShowPastScenarioList(ConfigDataWaypointInfo waypointInfo)
      {
        return this.m_owner.ShowPastScenarioList(waypointInfo);
      }

      public void StartShowFadeOutLoadingFadeIn(Action fadeOutEnd, FadeStyle style)
      {
        this.m_owner.StartShowFadeOutLoadingFadeIn(fadeOutEnd, style);
      }

      public void HideFadeOutLoadingFadeIn()
      {
        this.m_owner.HideFadeOutLoadingFadeIn();
      }

      public void StartPlayerInfoUITask()
      {
        this.m_owner.StartPlayerInfoUITask();
      }

      public void PlayerInfoUITask_OnLoadAllResCompleted()
      {
        this.m_owner.PlayerInfoUITask_OnLoadAllResCompleted();
      }

      public void PlayerInfoUITask_OnClose()
      {
        this.m_owner.PlayerInfoUITask_OnClose();
      }

      public void StartAppScoreUITask()
      {
        this.m_owner.StartAppScoreUITask();
      }

      public void AppScoreUITask_OnLoadAllResCompleted()
      {
        this.m_owner.AppScoreUITask_OnLoadAllResCompleted();
      }

      public void AppScoreUITask_OnClose()
      {
        this.m_owner.AppScoreUITask_OnClose();
      }

      public void StartSignUITask()
      {
        this.m_owner.StartSignUITask();
      }

      public void SignUITask_OnLoadAllResCompleted()
      {
        this.m_owner.SignUITask_OnLoadAllResCompleted();
      }

      public void StartActivityNoticeUITask()
      {
        this.m_owner.StartActivityNoticeUITask();
      }

      public void ActivityNoticeUITask_OnLoadAllResCompleted()
      {
        this.m_owner.ActivityNoticeUITask_OnLoadAllResCompleted();
      }

      public void StartHeroUITask(UIIntent fromIntent)
      {
        this.m_owner.StartHeroUITask(fromIntent);
      }

      public void HeroUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroUITask_OnLoadAllResCompleted();
      }

      public void StartGuildStoreUITask(UIIntent fromIntent)
      {
        this.m_owner.StartGuildStoreUITask(fromIntent);
      }

      public void GuildStoreUITask_OnLoadAllResCompleted()
      {
        this.m_owner.GuildStoreUITask_OnLoadAllResCompleted();
      }

      public void StartBagUITask(BagListUIController.DisplayType displayType, UIIntent fromIntent)
      {
        this.m_owner.StartBagUITask(displayType, fromIntent);
      }

      public void BagUITask_OnLoadAllResCompleted()
      {
        this.m_owner.BagUITask_OnLoadAllResCompleted();
      }

      public void StartMissionUITask(UIIntent fromIntent)
      {
        this.m_owner.StartMissionUITask(fromIntent);
      }

      public void MissionUITask_OnLoadAllResCompleted()
      {
        this.m_owner.MissionUITask_OnLoadAllResCompleted();
      }

      public void StartPlayerInfoUITaskByPath(UIIntent fromIntent)
      {
        this.m_owner.StartPlayerInfoUITaskByPath(fromIntent);
      }

      public void PlayerInfoUITask_OnLoadAllResCompletedByPath()
      {
        this.m_owner.PlayerInfoUITask_OnLoadAllResCompletedByPath();
      }

      public void StartOpenServiceActivityUITask()
      {
        this.m_owner.StartOpenServiceActivityUITask();
      }

      public void OpenServiceActivityUITask_OnLoadAllResCompleted()
      {
        this.m_owner.OpenServiceActivityUITask_OnLoadAllResCompleted();
      }

      public void StartBackFlowActivityUITask()
      {
        this.m_owner.StartBackFlowActivityUITask();
      }

      public void BackFlowActivityUITask_OnLoadAllResCompleted()
      {
        this.m_owner.BackFlowActivityUITask_OnLoadAllResCompleted();
      }

      public void StartFettersUITask(
        ConfigDataHeroDungeonLevelInfo heroDungeonLevelInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartFettersUITask(heroDungeonLevelInfo, fromIntent);
      }

      public void FettersUITask_OnLoadAllResCompleted()
      {
        this.m_owner.FettersUITask_OnLoadAllResCompleted();
      }

      public void FetterUITask_StartHeroDungeon(Hero hero, UIIntent fromIntent)
      {
        this.m_owner.FetterUITask_StartHeroDungeon(hero, fromIntent);
      }

      public void StartArenaSelectUITask(
        ArenaUIType arenaUIType,
        int panelType,
        UIIntent fromIntent)
      {
        this.m_owner.StartArenaSelectUITask(arenaUIType, panelType, fromIntent);
      }

      public void ArenaSelectUITask_OnStop(Task task)
      {
        this.m_owner.ArenaSelectUITask_OnStop(task);
      }

      public void ArenaSelectUITask_OnLoadAllResCompleted()
      {
        this.m_owner.ArenaSelectUITask_OnLoadAllResCompleted();
      }

      public void ArenaSelectUITask_OnShowArena(
        ArenaUIType arenaUIType,
        int panelType,
        UIIntent fromIntent)
      {
        this.m_owner.ArenaSelectUITask_OnShowArena(arenaUIType, panelType, fromIntent);
      }

      public void ArenaSelectUITask_OnShowPeakArena(
        PeakArenaPanelType peakArenaPanelType,
        UIIntent fromIntent)
      {
        this.m_owner.ArenaSelectUITask_OnShowPeakArena(peakArenaPanelType, fromIntent);
      }

      public void StartArenaUITask(ArenaUIType arenaUIType, int panelType, UIIntent fromIntent)
      {
        this.m_owner.StartArenaUITask(arenaUIType, panelType, fromIntent);
      }

      public void ArenaUITask_OnLoadAllResCompleted()
      {
        this.m_owner.ArenaUITask_OnLoadAllResCompleted();
      }

      public void InitAndStartArenaUITask(
        ArenaUIType arenaUIType,
        int panelType,
        UIIntent fromIntent)
      {
        this.m_owner.InitAndStartArenaUITask(arenaUIType, panelType, fromIntent);
      }

      public void SendPeakArenaSeasonSimpleInfoNetTask()
      {
        this.m_owner.SendPeakArenaSeasonSimpleInfoNetTask();
      }

      public void SendGetSeasonInfoNetTask(Action onEnd)
      {
        this.m_owner.SendGetSeasonInfoNetTask(onEnd);
      }

      public void StartPeakArenaUITask(
        UIIntent fromIntent,
        PeakArenaPanelType tabType,
        int matchGroupId)
      {
        this.m_owner.StartPeakArenaUITask(fromIntent, tabType, matchGroupId);
      }

      public void PeakArenaUITask_OnLoadAllResCompleted()
      {
        this.m_owner.PeakArenaUITask_OnLoadAllResCompleted();
      }

      public void StartArenaBattleReportBasicDataGetNetTask(
        ArenaUIType arenaUIType,
        int panelType,
        UIIntent fromIntent)
      {
        this.m_owner.StartArenaBattleReportBasicDataGetNetTask(arenaUIType, panelType, fromIntent);
      }

      public void StartStoreUITask(StoreId storeId, UIIntent fromIntent)
      {
        this.m_owner.StartStoreUITask(storeId, fromIntent);
      }

      public void StoreUITask_OnLoadAllResCompleted()
      {
        this.m_owner.StoreUITask_OnLoadAllResCompleted();
      }

      public void StartSelectCardUITask(int cardPoolId, UIIntent fromIntent)
      {
        this.m_owner.StartSelectCardUITask(cardPoolId, fromIntent);
      }

      public void SelectCardUITask_OnLoadAllResCompleted()
      {
        this.m_owner.SelectCardUITask_OnLoadAllResCompleted();
      }

      public void StartDrillUITask(int drillState, UIIntent fromIntent)
      {
        this.m_owner.StartDrillUITask(drillState, fromIntent);
      }

      public void DrillUITask_OnLoadAllResCompleted()
      {
        this.m_owner.DrillUITask_OnLoadAllResCompleted();
      }

      public void StartFriendUITask(UIIntent fromIntent, bool openAddFriend)
      {
        this.m_owner.StartFriendUITask(fromIntent, openAddFriend);
      }

      public void StartGuildUITask(ulong guildMassiveCombatInstanceId, UIIntent fromIntent)
      {
        this.m_owner.StartGuildUITask(guildMassiveCombatInstanceId, fromIntent);
      }

      public void StartGuildSubUITask(GetPathType UIName, UIIntent fromIntent)
      {
        this.m_owner.StartGuildSubUITask(UIName, fromIntent);
      }

      public void GuildUITask_OnLoadAllResCompleted()
      {
        this.m_owner.GuildUITask_OnLoadAllResCompleted();
      }

      public void GuildManagementUITask_OnLoadAllResCompleted()
      {
        this.m_owner.GuildManagementUITask_OnLoadAllResCompleted();
      }

      public void FriendUITask_OnLoadAllResCompleted()
      {
        this.m_owner.FriendUITask_OnLoadAllResCompleted();
      }

      public void StartMailUITask()
      {
        this.m_owner.StartMailUITask();
      }

      public void MailUITask_OnLoadAllResCompleted()
      {
        this.m_owner.MailUITask_OnLoadAllResCompleted();
      }

      public void StartRankingUITask()
      {
        this.m_owner.StartRankingUITask();
      }

      public void StartActivityUITask(int activityId, UIIntent fromIntent)
      {
        this.m_owner.StartActivityUITask(activityId, fromIntent);
      }

      public void ActivityUITask_OnLoadAllResCompleted()
      {
        this.m_owner.ActivityUITask_OnLoadAllResCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartTeamUITask(
        GameFunctionType gameFunctionType,
        int chapterId,
        int locationId,
        int collectionActivityId,
        UIIntent fromIntent)
      {
        // ISSUE: unable to decompile the method.
      }

      public void TeamUITask_OnLoadAllResCompleted()
      {
        this.m_owner.TeamUITask_OnLoadAllResCompleted();
      }

      public void StartTestUITask()
      {
        this.m_owner.StartTestUITask();
      }

      public void TestUITask_OnLoadAllResCompleted()
      {
        this.m_owner.TestUITask_OnLoadAllResCompleted();
      }

      public void TestUITask_OnPauseOrStop(Task task)
      {
        this.m_owner.TestUITask_OnPauseOrStop(task);
      }

      public void TestUITask_OnStartBattle(ConfigDataBattleInfo battleInfo)
      {
        this.m_owner.TestUITask_OnStartBattle(battleInfo);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartUnchartedUITask(
        BattleType battleType,
        int chapterId,
        int tabIndex,
        bool isAfterBattle,
        UIIntent fromIntent)
      {
        // ISSUE: unable to decompile the method.
      }

      public void UnchartedUITask_OnStop(Task task)
      {
        this.m_owner.UnchartedUITask_OnStop(task);
      }

      public void UnchartedUITask_OnLoadAllResCompleted()
      {
        this.m_owner.UnchartedUITask_OnLoadAllResCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void _StartUnchartedSubUITask(
        BattleType battleType,
        int chapterId,
        bool isAfterBattle,
        UIIntent fromIntent)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartRiftUITask(
        ConfigDataRiftLevelInfo levelInfo,
        bool openLevelInfo,
        bool needReturnToChapter,
        UIIntent fromIntent,
        NeedGoods needGoods)
      {
        // ISSUE: unable to decompile the method.
      }

      public void RiftUITask_OnStop(Task task)
      {
        this.m_owner.RiftUITask_OnStop(task);
      }

      public void RiftUITask_OnLoadAllResCompleted()
      {
        this.m_owner.RiftUITask_OnLoadAllResCompleted();
      }

      public void RiftUITask_GoToScenario(int scenarioID)
      {
        this.m_owner.RiftUITask_GoToScenario(scenarioID);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartHeroDungeonUITask(
        ConfigDataHeroDungeonLevelInfo levelInfo,
        bool openLevelInfo,
        Hero hero,
        UIIntent fromIntent)
      {
        // ISSUE: unable to decompile the method.
      }

      public void HeroDungeonUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroDungeonUITask_OnLoadAllResCompleted();
      }

      public void StartHeroPhantomUITask(
        ConfigDataHeroPhantomInfo heroPhantomInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartHeroPhantomUITask(heroPhantomInfo, fromIntent);
      }

      public void HeroPhantomUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroPhantomUITask_OnLoadAllResCompleted();
      }

      public void StartThearchyUITask(
        ConfigDataThearchyTrialInfo thearchyTrialInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartThearchyUITask(thearchyTrialInfo, fromIntent);
      }

      public void ThearchyUITask_OnLoadAllResCompleted()
      {
        this.m_owner.ThearchyUITask_OnLoadAllResCompleted();
      }

      public void StartAnikiUITask(ConfigDataAnikiGymInfo anikiGymInfo, UIIntent fromIntent)
      {
        this.m_owner.StartAnikiUITask(anikiGymInfo, fromIntent);
      }

      public void AnikiUITask_OnLoadAllResCompleted()
      {
        this.m_owner.AnikiUITask_OnLoadAllResCompleted();
      }

      public void StartTreasureMapUITask(UIIntent fromIntent)
      {
        this.m_owner.StartTreasureMapUITask(fromIntent);
      }

      public void TreasureMapUITask_OnLoadAllResCompleted()
      {
        this.m_owner.TreasureMapUITask_OnLoadAllResCompleted();
      }

      public void StartMemoryCorridorUITask(
        ConfigDataMemoryCorridorInfo memoryCorridorInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartMemoryCorridorUITask(memoryCorridorInfo, fromIntent);
      }

      public void MemoryCorridorUITask_OnLoadAllResCompleted()
      {
        this.m_owner.MemoryCorridorUITask_OnLoadAllResCompleted();
      }

      public void StartHeroTrainningUITask(
        ConfigDataHeroTrainningInfo heroTrainningInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartHeroTrainningUITask(heroTrainningInfo, fromIntent);
      }

      public void HeroTrainningUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroTrainningUITask_OnLoadAllResCompleted();
      }

      public void StartCooperateBattleUITask(
        ConfigDataCooperateBattleInfo cooperateBattleInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartCooperateBattleUITask(cooperateBattleInfo, fromIntent);
      }

      public void CooperateBattleUITask_OnLoadAllResCompleted()
      {
        this.m_owner.CooperateBattleUITask_OnLoadAllResCompleted();
      }

      public void StartUnchartedScoreUITask(
        ConfigDataUnchartedScoreInfo unchartedScoreInfo,
        BattleType battleType,
        UIIntent fromIntent)
      {
        this.m_owner.StartUnchartedScoreUITask(unchartedScoreInfo, battleType, fromIntent);
      }

      public void UnchartedScoreUITask_OnLoadAllResCompleted()
      {
        this.m_owner.UnchartedScoreUITask_OnLoadAllResCompleted();
      }

      public void StartClimbTowerUITask(UIIntent fromIntent)
      {
        this.m_owner.StartClimbTowerUITask(fromIntent);
      }

      public void ClimbTowerUITask_OnLoadAllResCompleted()
      {
        this.m_owner.ClimbTowerUITask_OnLoadAllResCompleted();
      }

      public void StartEternalShrineUITask(
        ConfigDataEternalShrineInfo eternalShrineInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartEternalShrineUITask(eternalShrineInfo, fromIntent);
      }

      public void EternalShrineUITask_OnLoadAllResCompleted()
      {
        this.m_owner.EternalShrineUITask_OnLoadAllResCompleted();
      }

      public void StartHeroAnthemUITask(
        ConfigDataHeroAnthemInfo heroAnthemInfo,
        UIIntent fromIntent)
      {
        this.m_owner.StartHeroAnthemUITask(heroAnthemInfo, fromIntent);
      }

      public void HeroAnthemUITask_OnLoadAllResCompleted()
      {
        this.m_owner.HeroAnthemUITask_OnLoadAllResCompleted();
      }

      public void StartCollectionActivityUITask(
        ConfigDataCollectionActivityInfo collectionActivityInfo,
        bool isAfterBattle,
        UIIntent fromIntent)
      {
        this.m_owner.StartCollectionActivityUITask(collectionActivityInfo, isAfterBattle, fromIntent);
      }

      public void CollectionActivityUITask_OnLoadAllResCompleted()
      {
        this.m_owner.CollectionActivityUITask_OnLoadAllResCompleted();
      }

      public void StartWorldEventMissionUITask(ConfigDataEventInfo eventInfo)
      {
        this.m_owner.StartWorldEventMissionUITask(eventInfo);
      }

      public void WorldEventMissionUITask_OnClose()
      {
        this.m_owner.WorldEventMissionUITask_OnClose();
      }

      public void RemoveWorldEventMissionUITaskEvents()
      {
        this.m_owner.RemoveWorldEventMissionUITaskEvents();
      }

      public bool WorldEventMissionUITask_OnCheckEnterMission()
      {
        return this.m_owner.WorldEventMissionUITask_OnCheckEnterMission();
      }

      public void WorldEventMissionUITask_OnEnterMission()
      {
        this.m_owner.WorldEventMissionUITask_OnEnterMission();
      }

      public void StartGoddessDialogUITask()
      {
        this.m_owner.StartGoddessDialogUITask();
      }

      public void GoddessDialogUITask_OnLoadAllResCompleted()
      {
        this.m_owner.GoddessDialogUITask_OnLoadAllResCompleted();
      }

      public void GoddessDialogUITask_OnClose(int protagonistId)
      {
        this.m_owner.GoddessDialogUITask_OnClose(protagonistId);
      }

      public void StartDialogUITask(ConfigDataDialogInfo dialogInfo)
      {
        this.m_owner.StartDialogUITask(dialogInfo);
      }

      public void DialogUITask_OnLoadAllResCompleted()
      {
        this.m_owner.DialogUITask_OnLoadAllResCompleted();
      }

      public void DialogUITask_OnClose(bool isSkip)
      {
        this.m_owner.DialogUITask_OnClose(isSkip);
      }

      public void StartBattleUITask(ConfigDataBattleInfo battleInfo, BattleType battleType)
      {
        this.m_owner.StartBattleUITask(battleInfo, battleType);
      }

      public static void HideInviteAndChatUITask()
      {
        WorldUITask.HideInviteAndChatUITask();
      }

      public static void UnloadAssetsAndStartBattleUITask()
      {
        WorldUITask.UnloadAssetsAndStartBattleUITask();
      }

      public static IEnumerator Co_UnloadAssetsAndStartBattleUITask()
      {
        return WorldUITask.Co_UnloadAssetsAndStartBattleUITask();
      }

      public static void BattleUITask_OnLoadAllResCompleted()
      {
        WorldUITask.BattleUITask_OnLoadAllResCompleted();
      }

      public void _StartArenaBattleUITask(bool isRevenge)
      {
        this.m_owner._StartArenaBattleUITask(isRevenge);
      }

      public static bool IsBattleReportVersionValid(int battleReportVersion)
      {
        return WorldUITask.IsBattleReportVersionValid(battleReportVersion);
      }

      public void StartArenaReplayBattleUITask(ArenaBattleReport battleReport)
      {
        this.m_owner.StartArenaReplayBattleUITask(battleReport);
      }

      public void StartRealTimePVPReplayBattleUITask(RealTimePVPBattleReport battleReport)
      {
        this.m_owner.StartRealTimePVPReplayBattleUITask(battleReport);
      }

      public static void _StartPeakArenaBattleReport(
        PeakArenaLadderBattleReport battleReport,
        string userId,
        PeakArenaBattleReportSourceType sourceType,
        int matchGroupId)
      {
        WorldUITask._StartPeakArenaBattleReport(battleReport, userId, sourceType, matchGroupId);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void StartPeakArenaReplayBattleUITask(
        PeakArenaLadderBattleReport battleReport,
        string userId,
        PeakArenaBattleReportSourceType sourceType,
        int matchGroupId)
      {
        // ISSUE: unable to decompile the method.
      }

      public void FadeOutAndStartBattleUITask()
      {
        this.m_owner.FadeOutAndStartBattleUITask();
      }

      public void _StartGetPathTargetUITask(
        GetPathData getPath,
        UIIntent fromIntent,
        NeedGoods needGoods)
      {
        this.m_owner._StartGetPathTargetUITask(getPath, fromIntent, needGoods);
      }
    }
  }
}
