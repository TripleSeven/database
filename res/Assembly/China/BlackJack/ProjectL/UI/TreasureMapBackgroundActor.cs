﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TreasureMapBackgroundActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class TreasureMapBackgroundActor : IFxEventListener
  {
    private UISpineGraphic m_spineGraphic;
    [DoNotToLua]
    private TreasureMapBackgroundActor.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_CreateStringGameObject_hotfix;
    private LuaFunction m_Destroy_hotfix;
    private LuaFunction m_OnSoundFxEventString_hotfix;
    private LuaFunction m_OnCameraEffectFxEventString_hotfix;
    private LuaFunction m_OnScreenEffectFxEventString_hotfix;
    private LuaFunction m_OnGeneralFxEventString_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TreasureMapBackgroundActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Create(string name, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSound(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGeneral(FxEvent e, string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public TreasureMapBackgroundActor.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TreasureMapBackgroundActor m_owner;

      public LuaExportHelper(TreasureMapBackgroundActor owner)
      {
        this.m_owner = owner;
      }

      public UISpineGraphic m_spineGraphic
      {
        get
        {
          return this.m_owner.m_spineGraphic;
        }
        set
        {
          this.m_owner.m_spineGraphic = value;
        }
      }
    }
  }
}
