﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePracticeInviteNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class BattlePracticeInviteNetTask : UINetTask
  {
    private string m_targetUserId;
    private int m_battleInfoId;
    private PracticeMode m_practiceMode;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePracticeInviteNetTask(
      string targetUserId,
      int battleInfoId,
      PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void OnBattlePracticeInviteAck(int result)
    {
      this.Result = result;
      this.OnTransactionComplete();
    }
  }
}
