﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftChapterButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class RiftChapterButton : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sizeUIStateController;
    [AutoBind("./Item", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Item/ChapterImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Item/ChapterImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./Item/Generation", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_titleObj;
    private ConfigDataRiftChapterInfo m_chapterInfo;
    private int m_index;
    private bool m_isLocked;
    private bool m_isNew;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetIndex(int index)
    {
      this.m_index = index;
    }

    public int GetIndex()
    {
      return this.m_index;
    }

    public ConfigDataRiftChapterInfo GetChapter()
    {
      return this.m_chapterInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(bool isLocked, bool isNew, bool isCenter)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsLocked()
    {
      return this.m_isLocked;
    }

    public bool IsNew()
    {
      return this.m_isNew;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RiftChapterButton> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
