﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class FriendUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Panel/ShieldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shieldButton;
    [AutoBind("./Panel/AddFriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendButton;
    [AutoBind("./Panel/AddFriendButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendRedMark;
    [AutoBind("./Panel/CreateNewGroupButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createNewGroupButton;
    [AutoBind("./FriendPanelTypeCount", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendCountUIStateController;
    [AutoBind("./FriendPanelTypeCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendTypeCount;
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_panelUIStateController;
    [AutoBind("./FriendShipPointTip", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendShipPointTipStateCtrl;
    [AutoBind("./FriendShipPointTip/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendShipPointTipText;
    [AutoBind("./Panel/GetAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getAllFriendshipPointButton;
    [AutoBind("./Panel/GetAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_getAllFriendshipPointButtonUIStateCtrl;
    [AutoBind("./Panel/SendAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sendAllFriendshipPointButton;
    [AutoBind("./Panel/SendAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sendAllFriendshipPointButtonUIStateCtrl;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_friendToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Server", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_acrossServerToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Recent", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Group", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_groupToggle;
    [AutoBind("./AddFriendPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_addFriendPanelUIStateController;
    [AutoBind("./AddFriendPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelCloseButton;
    [AutoBind("./AddFriendPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelBackgroundButton;
    [AutoBind("./AddFriendPanel/Detail/ServerSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelSelectServerButton;
    [AutoBind("./AddFriendPanel/Detail/ServerSelectButton/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addFriendPanelServerNameText;
    [AutoBind("./AddFriendPanel/Detail/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelSearchButton;
    [AutoBind("./AddFriendPanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_addFriendPanelInputText;
    [AutoBind("./AddFriendPanel/Detail/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelChangeButton;
    [AutoBind("./AddFriendPanel/Detail/AllRefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelRefuseAllButton;
    [AutoBind("./AddFriendPanel/Detail/RecommendListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelRecommendScrollView;
    [AutoBind("./AddFriendPanel/Detail/RecommendListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelRecommendScrollViewContent;
    [AutoBind("./AddFriendPanel/Detail/ApplyListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelApplyScrollView;
    [AutoBind("./AddFriendPanel/Detail/ApplyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelApplyScrollViewContent;
    [AutoBind("/AddFriendPanel/Detail/RecommendText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelRecommendTextGameObject;
    [AutoBind("/AddFriendPanel/Detail/FindFriendText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelFindFriendTextGameObject;
    [AutoBind("./AddFriendPanel/Detail/NotFound", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelNotFindFriendGameObject;
    [AutoBind("./AddFriendPanel/ServerListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_serverListPanelUIStateController;
    [AutoBind("./AddFriendPanel/ServerListPanel/Prefab/ServerItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_serverItemGameObject;
    [AutoBind("./AddFriendPanel/ServerListPanel/Prefab/ServerItem/ServerNameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverItemName;
    [AutoBind("./AddFriendPanel/ServerListPanel/Detail/ServerScrollView/Viewport/Content/ServerListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_serverListGroup;
    [AutoBind("./AddFriendPanel/ServerListPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_serverListPanelBGButton;
    [AutoBind("./WatchGroupStaffPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_watchGroupStaffPanelUIStateController;
    [AutoBind("./WatchGroupStaffPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelBackgroundButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelCloseButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/ChangeNameButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelChangeGroupNameButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/QuitGroupButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelQuitGroupButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/InviteFriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelInviteFriendButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/PlayerListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_watchGroupStaffPanelScrollView;
    [AutoBind("./WatchGroupStaffPanel/Detail/PlayerListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_watchGroupStaffPanelScrollViewContent;
    [AutoBind("./WatchGroupStaffPanel/Detail/GroupStaffCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_watchGroupStaffPanelCountText;
    [AutoBind("./WatchGroupStaffPanel/Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_groupTitleName;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_changeGroupNameUIStateController;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeGroupNameBackgroundButton;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeGroupNameConfirmButton;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_changeNamePanelInputField;
    [AutoBind("./InviteFriendPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_inviteFriendToGroupPanelUIStateController;
    [AutoBind("./InviteFriendPanel/Detail/PlayerListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inviteFriendToGroupPanelScrollView;
    [AutoBind("./InviteFriendPanel/Detail/PlayerListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inviteFriendToGroupPanelScrollViewContent;
    [AutoBind("./InviteFriendPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteFriendToGroupPanelConfirmButton;
    [AutoBind("./InviteFriendPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteFriendToGroupPanelCancelButton;
    [AutoBind("./InviteFriendPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteFriendToGroupPanelBackgroundButton;
    [AutoBind("./InviteFriendPanel/Detail/InvitedCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteFriendToGroupPanelCountText;
    [AutoBind("./InviteFriendPanel/Detail/ToggleGroup/ServerFriend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_inviteFriendToGroupPanelServerFriendToggle;
    [AutoBind("./InviteFriendPanel/Detail/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_inviteFriendToGroupPanelFriendToggle;
    [AutoBind("./ShieldPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shieldPanelUIStateController;
    [AutoBind("./ShieldPanel/Detail/PlayListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shieldPanelScrollView;
    [AutoBind("./ShieldPanel/Detail/PlayListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shieldPanelScrollViewContent;
    [AutoBind("./ShieldPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shieldPanelBackgroundButton;
    [AutoBind("./ShieldPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shieldPanelCloseButton;
    [AutoBind("./ShieldPanel/Detail/ShieldCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_shieldPanelShieldCount;
    [AutoBind("./Panel/FriendListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendScrollView;
    [AutoBind("./Panel/FriendListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendScrollViewContent;
    [AutoBind("./Panel/GroupListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_groupScrollView;
    [AutoBind("./Panel/GroupListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_groupScrollViewContent;
    [AutoBind("./Prefab/PlayerBigListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendBigItemPrefab;
    [AutoBind("./Prefab/PlayerSmallListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendSmallItemPrefab;
    [AutoBind("./Prefab/GroupListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_groupItemPrefab;
    private int m_currentServerID;
    private ProChatGroupInfo m_currentChatGroupInfo;
    private bool m_isCreateNewGroup;
    private const int m_maxInviteToGroupCount = 10;
    public bool m_isFindFriend;
    public bool m_isRecommendFriend;
    private List<UserSummary> m_recommendFriendList;
    private List<UserSummary> m_applyFriendList;
    private List<string> m_inviteToGroupList;
    private List<UserSummary> m_groupStaffList;
    private List<string> m_friendApplyAcceptList;
    private List<string> m_friendApplyDeclineList;
    private List<string> m_friendAddList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    [DoNotToLua]
    private FriendUIController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_SetFriendPanelList`1_hotfix;
    private LuaFunction m_SetAcrossServerFriendPanelList`1_hotfix;
    private LuaFunction m_SetRecentPanelList`1_hotfix;
    private LuaFunction m_SetGroupPanelList`1_hotfix;
    private LuaFunction m_SetAddFriendPanel_hotfix;
    private LuaFunction m_SetFirstToggleOn_hotfix;
    private LuaFunction m_SetPanelScrollViewReset_hotfix;
    private LuaFunction m_IsAddFriendRedMarkShowBoolean_hotfix;
    private LuaFunction m_ShowFriendShipPointTipString_hotfix;
    private LuaFunction m_SetShieldPanelList`1_hotfix;
    private LuaFunction m_SetFindFriendListList`1_hotfix;
    private LuaFunction m_SetRecommendFriendsListList`1_hotfix;
    private LuaFunction m_SetApplyFriendsListList`1_hotfix;
    private LuaFunction m_SetInviteFriendToGroupPanel_hotfix;
    private LuaFunction m_SetInviteFriendToGroupList_hotfix;
    private LuaFunction m_SetInviteAcrossServerFriendToGroupList_hotfix;
    private LuaFunction m_SetWatchGroupStaffPanelProChatGroupInfoBoolean_hotfix;
    private LuaFunction m_SetBasicBigFriendInfoGameObjectList`1FriendInfoTypeBoolean_hotfix;
    private LuaFunction m_SetBasicSmallFriendInfoGameObjectList`1FriendInfoType_hotfix;
    private LuaFunction m_SetServerListInfo_hotfix;
    private LuaFunction m_OnFriendToggleBoolean_hotfix;
    private LuaFunction m_OnAcrossServerToggleBoolean_hotfix;
    private LuaFunction m_OnRecentToggleBoolean_hotfix;
    private LuaFunction m_OnGroupToggleBoolean_hotfix;
    private LuaFunction m_OnReturnButtonClick_hotfix;
    private LuaFunction m_OnHelpButtonClick_hotfix;
    private LuaFunction m_OnShieldButtonClick_hotfix;
    private LuaFunction m_OnShieldPanelCloseButtonClick_hotfix;
    private LuaFunction m_OnSendAllFriendshipPointButtonClick_hotfix;
    private LuaFunction m_OnGetAllFriendshipPointButtonClick_hotfix;
    private LuaFunction m_OnAddFriendButtonClick_hotfix;
    private LuaFunction m_OnAddFriendPanelCloseButtonClick_hotfix;
    private LuaFunction m_OnSelectServerButtonClick_hotfix;
    private LuaFunction m_OnServerListPanelBGButtonClick_hotfix;
    private LuaFunction m_SetSendAllFriendshipPointButtonState_hotfix;
    private LuaFunction m_SetGetAllFriendshipPointButtonState_hotfix;
    private LuaFunction m_OnAddFriendPanelSearchButtonClick_hotfix;
    private LuaFunction m_OnAddFriendPanelChangeRecommendFriendButtonClick_hotfix;
    private LuaFunction m_OnAddFriendPanelAddAllButtonClick_hotfix;
    private LuaFunction m_OnAddFriendPanelAcceptAllButtonClick_hotfix;
    private LuaFunction m_OnAddFriendPanelRefuseAllButtonClick_hotfix;
    private LuaFunction m_OnWatchGroupStaffPanelCloseButtonClick_hotfix;
    private LuaFunction m_WatchGroupStaffPanelClose_hotfix;
    private LuaFunction m_GroupDissolvedProChatGroupInfo_hotfix;
    private LuaFunction m_OnWatchGroupStaffPanelChangeNameButtonClick_hotfix;
    private LuaFunction m_OnWatchGroupStaffPanelChangeNamePanelBGButtonClick_hotfix;
    private LuaFunction m_OnWatchGroupStaffPanelChangeNameConfirmButtonClick_hotfix;
    private LuaFunction m_OnQuitGroupButtonClick_hotfix;
    private LuaFunction m_OnCreateNewGroupButtonClick_hotfix;
    private LuaFunction m_OnInviteFriendToGroupButtonClick_hotfix;
    private LuaFunction m_InviteFriendToGoupPanelClose_hotfix;
    private LuaFunction m_OnInviteFriendToGoupPanelConfirmButtonClick_hotfix;
    private LuaFunction m_OnInviteFriendToGroupPanelServerFriendToggleBoolean_hotfix;
    private LuaFunction m_OnInviteFriendToGroupPanelFriendToggleBoolean_hotfix;
    private LuaFunction m_OnPlayerItemButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnPlayerItemButtonClickFriendSmallItemUIController_hotfix;
    private LuaFunction m_OnItemAddFriendButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnItemKickFromGroupButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnUnblockPlayerButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnChatButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnSendFriendshipPointButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnGetFriendshipPointButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnFriendshipPointDoneButtonClickFriendBigItemUIController_hotfix;
    private LuaFunction m_OnItemAddFriendButtonClickFriendSmallItemUIController_hotfix;
    private LuaFunction m_OnChatButtonClickFriendSmallItemUIController_hotfix;
    private LuaFunction m_OnFriendApplyAceeptButtonClickFriendSmallItemUIController_hotfix;
    private LuaFunction m_OnFriendApplyDeclineButtonClickFriendSmallItemUIController_hotfix;
    private LuaFunction m_OnInviteFriendToGroupToggleValueChangedBooleanFriendSmallItemUIController_hotfix;
    private LuaFunction m_OnGroupChatButtonClickFriendGroupUIController_hotfix;
    private LuaFunction m_OnWatchGroupStaffButtonClickFriendGroupUIController_hotfix;
    private LuaFunction m_add_EventOnReturnAction_hotfix;
    private LuaFunction m_remove_EventOnReturnAction_hotfix;
    private LuaFunction m_add_EventOnShowHelpAction_hotfix;
    private LuaFunction m_remove_EventOnShowHelpAction_hotfix;
    private LuaFunction m_add_EventOnShowPanelAction`1_hotfix;
    private LuaFunction m_remove_EventOnShowPanelAction`1_hotfix;
    private LuaFunction m_add_EventOnAddFriendAction`1_hotfix;
    private LuaFunction m_remove_EventOnAddFriendAction`1_hotfix;
    private LuaFunction m_add_EventOnKickFromGroupAction`2_hotfix;
    private LuaFunction m_remove_EventOnKickFromGroupAction`2_hotfix;
    private LuaFunction m_add_EventOnUnblockPlayerAction`1_hotfix;
    private LuaFunction m_remove_EventOnUnblockPlayerAction`1_hotfix;
    private LuaFunction m_add_EventOnChatAction`1_hotfix;
    private LuaFunction m_remove_EventOnChatAction`1_hotfix;
    private LuaFunction m_add_EventOnSendFriendshipPointAction`1_hotfix;
    private LuaFunction m_remove_EventOnSendFriendshipPointAction`1_hotfix;
    private LuaFunction m_add_EventOnGetFriendshipPointAction`1_hotfix;
    private LuaFunction m_remove_EventOnGetFriendshipPointAction`1_hotfix;
    private LuaFunction m_add_EventOnFindFriendAction`2_hotfix;
    private LuaFunction m_remove_EventOnFindFriendAction`2_hotfix;
    private LuaFunction m_add_EventOnSendAllFriendshipPointAction_hotfix;
    private LuaFunction m_remove_EventOnSendAllFriendshipPointAction_hotfix;
    private LuaFunction m_add_EventOnGetAllFriendshipPointAction_hotfix;
    private LuaFunction m_remove_EventOnGetAllFriendshipPointAction_hotfix;
    private LuaFunction m_add_EventOnGetRecommendFriendsListAction_hotfix;
    private LuaFunction m_remove_EventOnGetRecommendFriendsListAction_hotfix;
    private LuaFunction m_add_EventOnFriendApplyAceeptAction`1_hotfix;
    private LuaFunction m_remove_EventOnFriendApplyAceeptAction`1_hotfix;
    private LuaFunction m_add_EventOnFriendApplyDeclineAction`1_hotfix;
    private LuaFunction m_remove_EventOnFriendApplyDeclineAction`1_hotfix;
    private LuaFunction m_add_EventOnShowPlayerSimpleInfoAction`3_hotfix;
    private LuaFunction m_remove_EventOnShowPlayerSimpleInfoAction`3_hotfix;
    private LuaFunction m_add_EventOnGroupChatAction`1_hotfix;
    private LuaFunction m_remove_EventOnGroupChatAction`1_hotfix;
    private LuaFunction m_add_EventOnWatchGroupStaffAction`2_hotfix;
    private LuaFunction m_remove_EventOnWatchGroupStaffAction`2_hotfix;
    private LuaFunction m_add_EventOnCreateNewGroupAction`1_hotfix;
    private LuaFunction m_remove_EventOnCreateNewGroupAction`1_hotfix;
    private LuaFunction m_add_EventOnInviteFriendToGroupAction`2_hotfix;
    private LuaFunction m_remove_EventOnInviteFriendToGroupAction`2_hotfix;
    private LuaFunction m_add_EventOnChangeGroupNameAction`2_hotfix;
    private LuaFunction m_remove_EventOnChangeGroupNameAction`2_hotfix;
    private LuaFunction m_add_EventOnLeaveGroupAction`1_hotfix;
    private LuaFunction m_remove_EventOnLeaveGroupAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    private FriendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendPanel(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAcrossServerFriendPanel(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecentPanel(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupPanel(List<ProChatGroupCompactInfo> chatGroupList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAddFriendPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFirstToggleOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPanelScrollViewReset()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IsAddFriendRedMarkShow(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFriendShipPointTip(string s)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetShieldPanel(List<UserSummary> userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFindFriendList(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecommendFriendsList(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetApplyFriendsList(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetInviteFriendToGroupPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteFriendToGroupList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteAcrossServerFriendToGroupList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWatchGroupStaffPanel(ProChatGroupInfo chatGroupInfo, bool needOpenTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBasicBigFriendInfo(
      GameObject parentScrollViewContent,
      List<UserSummary> userList,
      FriendInfoType friendInfoType,
      bool isChatGroupOwner = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBasicSmallFriendInfo(
      GameObject parentScrollViewContent,
      List<UserSummary> userList,
      FriendInfoType friendInfoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetServerListInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAcrossServerToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecentToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShieldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShieldPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendAllFriendshipPointButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAllFriendshipPointButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAddFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectServerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnServerListPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSendAllFriendshipPointButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGetAllFriendshipPointButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelSearchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelChangeRecommendFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelAddAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelAcceptAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelRefuseAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WatchGroupStaffPanelClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GroupDissolved(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffPanelChangeNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWatchGroupStaffPanelChangeNamePanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffPanelChangeNameConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuitGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateNewGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InviteFriendToGoupPanelClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGoupPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupPanelServerFriendToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupPanelFriendToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerItemButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerItemButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddFriendButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemKickFromGroupButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnblockPlayerButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendFriendshipPointButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetFriendshipPointButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendshipPointDoneButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddFriendButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendApplyAceeptButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendApplyDeclineButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupToggleValueChanged(
      bool isOn,
      FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupChatButtonClick(FriendGroupUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffButtonClick(FriendGroupUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendPanelType> EventOnShowPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnAddFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProChatGroupInfo, UserSummary> EventOnKickFromGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnUnblockPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSendFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnGetFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string> EventOnFindFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSendAllFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetAllFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetRecommendFriendsList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnFriendApplyAceept
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnFriendApplyDecline
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary, Vector3, PlayerSimpleInfoUITask.PostionType> EventOnShowPlayerSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnGroupChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, bool> EventOnWatchGroupStaff
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnCreateNewGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, List<string>> EventOnInviteFriendToGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, string> EventOnChangeGroupName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProChatGroupInfo> EventOnLeaveGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public FriendUIController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnReturn()
    {
      this.EventOnReturn = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowHelp()
    {
      this.EventOnShowHelp = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPanel(FriendPanelType obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPanel(FriendPanelType obj)
    {
      this.EventOnShowPanel = (Action<FriendPanelType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnAddFriend(List<string> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnAddFriend(List<string> obj)
    {
      this.EventOnAddFriend = (Action<List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnKickFromGroup(ProChatGroupInfo arg1, UserSummary arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnKickFromGroup(ProChatGroupInfo arg1, UserSummary arg2)
    {
      this.EventOnKickFromGroup = (Action<ProChatGroupInfo, UserSummary>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnUnblockPlayer(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnUnblockPlayer(string obj)
    {
      this.EventOnUnblockPlayer = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChat(UserSummary obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChat(UserSummary obj)
    {
      this.EventOnChat = (Action<UserSummary>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSendFriendshipPoint(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSendFriendshipPoint(string obj)
    {
      this.EventOnSendFriendshipPoint = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetFriendshipPoint(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetFriendshipPoint(string obj)
    {
      this.EventOnGetFriendshipPoint = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFindFriend(int arg1, string arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFindFriend(int arg1, string arg2)
    {
      this.EventOnFindFriend = (Action<int, string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnSendAllFriendshipPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnSendAllFriendshipPoint()
    {
      this.EventOnSendAllFriendshipPoint = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetAllFriendshipPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetAllFriendshipPoint()
    {
      this.EventOnGetAllFriendshipPoint = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGetRecommendFriendsList()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGetRecommendFriendsList()
    {
      this.EventOnGetRecommendFriendsList = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFriendApplyAceept(List<string> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFriendApplyAceept(List<string> obj)
    {
      this.EventOnFriendApplyAceept = (Action<List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnFriendApplyDecline(List<string> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnFriendApplyDecline(List<string> obj)
    {
      this.EventOnFriendApplyDecline = (Action<List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnShowPlayerSimpleInfo(
      UserSummary arg1,
      Vector3 arg2,
      PlayerSimpleInfoUITask.PostionType arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnShowPlayerSimpleInfo(
      UserSummary arg1,
      Vector3 arg2,
      PlayerSimpleInfoUITask.PostionType arg3)
    {
      this.EventOnShowPlayerSimpleInfo = (Action<UserSummary, Vector3, PlayerSimpleInfoUITask.PostionType>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGroupChat(string obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGroupChat(string obj)
    {
      this.EventOnGroupChat = (Action<string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnWatchGroupStaff(string arg1, bool arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnWatchGroupStaff(string arg1, bool arg2)
    {
      this.EventOnWatchGroupStaff = (Action<string, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnCreateNewGroup(List<string> obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnCreateNewGroup(List<string> obj)
    {
      this.EventOnCreateNewGroup = (Action<List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnInviteFriendToGroup(string arg1, List<string> arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnInviteFriendToGroup(string arg1, List<string> arg2)
    {
      this.EventOnInviteFriendToGroup = (Action<string, List<string>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnChangeGroupName(string arg1, string arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnChangeGroupName(string arg1, string arg2)
    {
      this.EventOnChangeGroupName = (Action<string, string>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnLeaveGroup(ProChatGroupInfo obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLeaveGroup(ProChatGroupInfo obj)
    {
      this.EventOnLeaveGroup = (Action<ProChatGroupInfo>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private FriendUIController m_owner;

      public LuaExportHelper(FriendUIController owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnReturn()
      {
        this.m_owner.__callDele_EventOnReturn();
      }

      public void __clearDele_EventOnReturn()
      {
        this.m_owner.__clearDele_EventOnReturn();
      }

      public void __callDele_EventOnShowHelp()
      {
        this.m_owner.__callDele_EventOnShowHelp();
      }

      public void __clearDele_EventOnShowHelp()
      {
        this.m_owner.__clearDele_EventOnShowHelp();
      }

      public void __callDele_EventOnShowPanel(FriendPanelType obj)
      {
        this.m_owner.__callDele_EventOnShowPanel(obj);
      }

      public void __clearDele_EventOnShowPanel(FriendPanelType obj)
      {
        this.m_owner.__clearDele_EventOnShowPanel(obj);
      }

      public void __callDele_EventOnAddFriend(List<string> obj)
      {
        this.m_owner.__callDele_EventOnAddFriend(obj);
      }

      public void __clearDele_EventOnAddFriend(List<string> obj)
      {
        this.m_owner.__clearDele_EventOnAddFriend(obj);
      }

      public void __callDele_EventOnKickFromGroup(ProChatGroupInfo arg1, UserSummary arg2)
      {
        this.m_owner.__callDele_EventOnKickFromGroup(arg1, arg2);
      }

      public void __clearDele_EventOnKickFromGroup(ProChatGroupInfo arg1, UserSummary arg2)
      {
        this.m_owner.__clearDele_EventOnKickFromGroup(arg1, arg2);
      }

      public void __callDele_EventOnUnblockPlayer(string obj)
      {
        this.m_owner.__callDele_EventOnUnblockPlayer(obj);
      }

      public void __clearDele_EventOnUnblockPlayer(string obj)
      {
        this.m_owner.__clearDele_EventOnUnblockPlayer(obj);
      }

      public void __callDele_EventOnChat(UserSummary obj)
      {
        this.m_owner.__callDele_EventOnChat(obj);
      }

      public void __clearDele_EventOnChat(UserSummary obj)
      {
        this.m_owner.__clearDele_EventOnChat(obj);
      }

      public void __callDele_EventOnSendFriendshipPoint(string obj)
      {
        this.m_owner.__callDele_EventOnSendFriendshipPoint(obj);
      }

      public void __clearDele_EventOnSendFriendshipPoint(string obj)
      {
        this.m_owner.__clearDele_EventOnSendFriendshipPoint(obj);
      }

      public void __callDele_EventOnGetFriendshipPoint(string obj)
      {
        this.m_owner.__callDele_EventOnGetFriendshipPoint(obj);
      }

      public void __clearDele_EventOnGetFriendshipPoint(string obj)
      {
        this.m_owner.__clearDele_EventOnGetFriendshipPoint(obj);
      }

      public void __callDele_EventOnFindFriend(int arg1, string arg2)
      {
        this.m_owner.__callDele_EventOnFindFriend(arg1, arg2);
      }

      public void __clearDele_EventOnFindFriend(int arg1, string arg2)
      {
        this.m_owner.__clearDele_EventOnFindFriend(arg1, arg2);
      }

      public void __callDele_EventOnSendAllFriendshipPoint()
      {
        this.m_owner.__callDele_EventOnSendAllFriendshipPoint();
      }

      public void __clearDele_EventOnSendAllFriendshipPoint()
      {
        this.m_owner.__clearDele_EventOnSendAllFriendshipPoint();
      }

      public void __callDele_EventOnGetAllFriendshipPoint()
      {
        this.m_owner.__callDele_EventOnGetAllFriendshipPoint();
      }

      public void __clearDele_EventOnGetAllFriendshipPoint()
      {
        this.m_owner.__clearDele_EventOnGetAllFriendshipPoint();
      }

      public void __callDele_EventOnGetRecommendFriendsList()
      {
        this.m_owner.__callDele_EventOnGetRecommendFriendsList();
      }

      public void __clearDele_EventOnGetRecommendFriendsList()
      {
        this.m_owner.__clearDele_EventOnGetRecommendFriendsList();
      }

      public void __callDele_EventOnFriendApplyAceept(List<string> obj)
      {
        this.m_owner.__callDele_EventOnFriendApplyAceept(obj);
      }

      public void __clearDele_EventOnFriendApplyAceept(List<string> obj)
      {
        this.m_owner.__clearDele_EventOnFriendApplyAceept(obj);
      }

      public void __callDele_EventOnFriendApplyDecline(List<string> obj)
      {
        this.m_owner.__callDele_EventOnFriendApplyDecline(obj);
      }

      public void __clearDele_EventOnFriendApplyDecline(List<string> obj)
      {
        this.m_owner.__clearDele_EventOnFriendApplyDecline(obj);
      }

      public void __callDele_EventOnShowPlayerSimpleInfo(
        UserSummary arg1,
        Vector3 arg2,
        PlayerSimpleInfoUITask.PostionType arg3)
      {
        this.m_owner.__callDele_EventOnShowPlayerSimpleInfo(arg1, arg2, arg3);
      }

      public void __clearDele_EventOnShowPlayerSimpleInfo(
        UserSummary arg1,
        Vector3 arg2,
        PlayerSimpleInfoUITask.PostionType arg3)
      {
        this.m_owner.__clearDele_EventOnShowPlayerSimpleInfo(arg1, arg2, arg3);
      }

      public void __callDele_EventOnGroupChat(string obj)
      {
        this.m_owner.__callDele_EventOnGroupChat(obj);
      }

      public void __clearDele_EventOnGroupChat(string obj)
      {
        this.m_owner.__clearDele_EventOnGroupChat(obj);
      }

      public void __callDele_EventOnWatchGroupStaff(string arg1, bool arg2)
      {
        this.m_owner.__callDele_EventOnWatchGroupStaff(arg1, arg2);
      }

      public void __clearDele_EventOnWatchGroupStaff(string arg1, bool arg2)
      {
        this.m_owner.__clearDele_EventOnWatchGroupStaff(arg1, arg2);
      }

      public void __callDele_EventOnCreateNewGroup(List<string> obj)
      {
        this.m_owner.__callDele_EventOnCreateNewGroup(obj);
      }

      public void __clearDele_EventOnCreateNewGroup(List<string> obj)
      {
        this.m_owner.__clearDele_EventOnCreateNewGroup(obj);
      }

      public void __callDele_EventOnInviteFriendToGroup(string arg1, List<string> arg2)
      {
        this.m_owner.__callDele_EventOnInviteFriendToGroup(arg1, arg2);
      }

      public void __clearDele_EventOnInviteFriendToGroup(string arg1, List<string> arg2)
      {
        this.m_owner.__clearDele_EventOnInviteFriendToGroup(arg1, arg2);
      }

      public void __callDele_EventOnChangeGroupName(string arg1, string arg2)
      {
        this.m_owner.__callDele_EventOnChangeGroupName(arg1, arg2);
      }

      public void __clearDele_EventOnChangeGroupName(string arg1, string arg2)
      {
        this.m_owner.__clearDele_EventOnChangeGroupName(arg1, arg2);
      }

      public void __callDele_EventOnLeaveGroup(ProChatGroupInfo obj)
      {
        this.m_owner.__callDele_EventOnLeaveGroup(obj);
      }

      public void __clearDele_EventOnLeaveGroup(ProChatGroupInfo obj)
      {
        this.m_owner.__clearDele_EventOnLeaveGroup(obj);
      }

      public RectTransform m_marginTransform
      {
        get
        {
          return this.m_owner.m_marginTransform;
        }
        set
        {
          this.m_owner.m_marginTransform = value;
        }
      }

      public Button m_returnButton
      {
        get
        {
          return this.m_owner.m_returnButton;
        }
        set
        {
          this.m_owner.m_returnButton = value;
        }
      }

      public Button m_helpButton
      {
        get
        {
          return this.m_owner.m_helpButton;
        }
        set
        {
          this.m_owner.m_helpButton = value;
        }
      }

      public Button m_shieldButton
      {
        get
        {
          return this.m_owner.m_shieldButton;
        }
        set
        {
          this.m_owner.m_shieldButton = value;
        }
      }

      public Button m_addFriendButton
      {
        get
        {
          return this.m_owner.m_addFriendButton;
        }
        set
        {
          this.m_owner.m_addFriendButton = value;
        }
      }

      public GameObject m_addFriendRedMark
      {
        get
        {
          return this.m_owner.m_addFriendRedMark;
        }
        set
        {
          this.m_owner.m_addFriendRedMark = value;
        }
      }

      public Button m_createNewGroupButton
      {
        get
        {
          return this.m_owner.m_createNewGroupButton;
        }
        set
        {
          this.m_owner.m_createNewGroupButton = value;
        }
      }

      public CommonUIStateController m_friendCountUIStateController
      {
        get
        {
          return this.m_owner.m_friendCountUIStateController;
        }
        set
        {
          this.m_owner.m_friendCountUIStateController = value;
        }
      }

      public Text m_friendTypeCount
      {
        get
        {
          return this.m_owner.m_friendTypeCount;
        }
        set
        {
          this.m_owner.m_friendTypeCount = value;
        }
      }

      public CommonUIStateController m_panelUIStateController
      {
        get
        {
          return this.m_owner.m_panelUIStateController;
        }
        set
        {
          this.m_owner.m_panelUIStateController = value;
        }
      }

      public CommonUIStateController m_friendShipPointTipStateCtrl
      {
        get
        {
          return this.m_owner.m_friendShipPointTipStateCtrl;
        }
        set
        {
          this.m_owner.m_friendShipPointTipStateCtrl = value;
        }
      }

      public Text m_friendShipPointTipText
      {
        get
        {
          return this.m_owner.m_friendShipPointTipText;
        }
        set
        {
          this.m_owner.m_friendShipPointTipText = value;
        }
      }

      public Button m_getAllFriendshipPointButton
      {
        get
        {
          return this.m_owner.m_getAllFriendshipPointButton;
        }
        set
        {
          this.m_owner.m_getAllFriendshipPointButton = value;
        }
      }

      public CommonUIStateController m_getAllFriendshipPointButtonUIStateCtrl
      {
        get
        {
          return this.m_owner.m_getAllFriendshipPointButtonUIStateCtrl;
        }
        set
        {
          this.m_owner.m_getAllFriendshipPointButtonUIStateCtrl = value;
        }
      }

      public Button m_sendAllFriendshipPointButton
      {
        get
        {
          return this.m_owner.m_sendAllFriendshipPointButton;
        }
        set
        {
          this.m_owner.m_sendAllFriendshipPointButton = value;
        }
      }

      public CommonUIStateController m_sendAllFriendshipPointButtonUIStateCtrl
      {
        get
        {
          return this.m_owner.m_sendAllFriendshipPointButtonUIStateCtrl;
        }
        set
        {
          this.m_owner.m_sendAllFriendshipPointButtonUIStateCtrl = value;
        }
      }

      public Toggle m_friendToggle
      {
        get
        {
          return this.m_owner.m_friendToggle;
        }
        set
        {
          this.m_owner.m_friendToggle = value;
        }
      }

      public Toggle m_acrossServerToggle
      {
        get
        {
          return this.m_owner.m_acrossServerToggle;
        }
        set
        {
          this.m_owner.m_acrossServerToggle = value;
        }
      }

      public Toggle m_recentToggle
      {
        get
        {
          return this.m_owner.m_recentToggle;
        }
        set
        {
          this.m_owner.m_recentToggle = value;
        }
      }

      public Toggle m_groupToggle
      {
        get
        {
          return this.m_owner.m_groupToggle;
        }
        set
        {
          this.m_owner.m_groupToggle = value;
        }
      }

      public CommonUIStateController m_addFriendPanelUIStateController
      {
        get
        {
          return this.m_owner.m_addFriendPanelUIStateController;
        }
        set
        {
          this.m_owner.m_addFriendPanelUIStateController = value;
        }
      }

      public Button m_addFriendPanelCloseButton
      {
        get
        {
          return this.m_owner.m_addFriendPanelCloseButton;
        }
        set
        {
          this.m_owner.m_addFriendPanelCloseButton = value;
        }
      }

      public Button m_addFriendPanelBackgroundButton
      {
        get
        {
          return this.m_owner.m_addFriendPanelBackgroundButton;
        }
        set
        {
          this.m_owner.m_addFriendPanelBackgroundButton = value;
        }
      }

      public Button m_addFriendPanelSelectServerButton
      {
        get
        {
          return this.m_owner.m_addFriendPanelSelectServerButton;
        }
        set
        {
          this.m_owner.m_addFriendPanelSelectServerButton = value;
        }
      }

      public Text m_addFriendPanelServerNameText
      {
        get
        {
          return this.m_owner.m_addFriendPanelServerNameText;
        }
        set
        {
          this.m_owner.m_addFriendPanelServerNameText = value;
        }
      }

      public Button m_addFriendPanelSearchButton
      {
        get
        {
          return this.m_owner.m_addFriendPanelSearchButton;
        }
        set
        {
          this.m_owner.m_addFriendPanelSearchButton = value;
        }
      }

      public InputField m_addFriendPanelInputText
      {
        get
        {
          return this.m_owner.m_addFriendPanelInputText;
        }
        set
        {
          this.m_owner.m_addFriendPanelInputText = value;
        }
      }

      public Button m_addFriendPanelChangeButton
      {
        get
        {
          return this.m_owner.m_addFriendPanelChangeButton;
        }
        set
        {
          this.m_owner.m_addFriendPanelChangeButton = value;
        }
      }

      public Button m_addFriendPanelRefuseAllButton
      {
        get
        {
          return this.m_owner.m_addFriendPanelRefuseAllButton;
        }
        set
        {
          this.m_owner.m_addFriendPanelRefuseAllButton = value;
        }
      }

      public GameObject m_addFriendPanelRecommendScrollView
      {
        get
        {
          return this.m_owner.m_addFriendPanelRecommendScrollView;
        }
        set
        {
          this.m_owner.m_addFriendPanelRecommendScrollView = value;
        }
      }

      public GameObject m_addFriendPanelRecommendScrollViewContent
      {
        get
        {
          return this.m_owner.m_addFriendPanelRecommendScrollViewContent;
        }
        set
        {
          this.m_owner.m_addFriendPanelRecommendScrollViewContent = value;
        }
      }

      public GameObject m_addFriendPanelApplyScrollView
      {
        get
        {
          return this.m_owner.m_addFriendPanelApplyScrollView;
        }
        set
        {
          this.m_owner.m_addFriendPanelApplyScrollView = value;
        }
      }

      public GameObject m_addFriendPanelApplyScrollViewContent
      {
        get
        {
          return this.m_owner.m_addFriendPanelApplyScrollViewContent;
        }
        set
        {
          this.m_owner.m_addFriendPanelApplyScrollViewContent = value;
        }
      }

      public GameObject m_addFriendPanelRecommendTextGameObject
      {
        get
        {
          return this.m_owner.m_addFriendPanelRecommendTextGameObject;
        }
        set
        {
          this.m_owner.m_addFriendPanelRecommendTextGameObject = value;
        }
      }

      public GameObject m_addFriendPanelFindFriendTextGameObject
      {
        get
        {
          return this.m_owner.m_addFriendPanelFindFriendTextGameObject;
        }
        set
        {
          this.m_owner.m_addFriendPanelFindFriendTextGameObject = value;
        }
      }

      public GameObject m_addFriendPanelNotFindFriendGameObject
      {
        get
        {
          return this.m_owner.m_addFriendPanelNotFindFriendGameObject;
        }
        set
        {
          this.m_owner.m_addFriendPanelNotFindFriendGameObject = value;
        }
      }

      public CommonUIStateController m_serverListPanelUIStateController
      {
        get
        {
          return this.m_owner.m_serverListPanelUIStateController;
        }
        set
        {
          this.m_owner.m_serverListPanelUIStateController = value;
        }
      }

      public GameObject m_serverItemGameObject
      {
        get
        {
          return this.m_owner.m_serverItemGameObject;
        }
        set
        {
          this.m_owner.m_serverItemGameObject = value;
        }
      }

      public Text m_serverItemName
      {
        get
        {
          return this.m_owner.m_serverItemName;
        }
        set
        {
          this.m_owner.m_serverItemName = value;
        }
      }

      public GameObject m_serverListGroup
      {
        get
        {
          return this.m_owner.m_serverListGroup;
        }
        set
        {
          this.m_owner.m_serverListGroup = value;
        }
      }

      public Button m_serverListPanelBGButton
      {
        get
        {
          return this.m_owner.m_serverListPanelBGButton;
        }
        set
        {
          this.m_owner.m_serverListPanelBGButton = value;
        }
      }

      public CommonUIStateController m_watchGroupStaffPanelUIStateController
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelUIStateController;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelUIStateController = value;
        }
      }

      public Button m_watchGroupStaffPanelBackgroundButton
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelBackgroundButton;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelBackgroundButton = value;
        }
      }

      public Button m_watchGroupStaffPanelCloseButton
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelCloseButton;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelCloseButton = value;
        }
      }

      public Button m_watchGroupStaffPanelChangeGroupNameButton
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelChangeGroupNameButton;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelChangeGroupNameButton = value;
        }
      }

      public Button m_watchGroupStaffPanelQuitGroupButton
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelQuitGroupButton;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelQuitGroupButton = value;
        }
      }

      public Button m_watchGroupStaffPanelInviteFriendButton
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelInviteFriendButton;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelInviteFriendButton = value;
        }
      }

      public GameObject m_watchGroupStaffPanelScrollView
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelScrollView;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelScrollView = value;
        }
      }

      public GameObject m_watchGroupStaffPanelScrollViewContent
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelScrollViewContent;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelScrollViewContent = value;
        }
      }

      public Text m_watchGroupStaffPanelCountText
      {
        get
        {
          return this.m_owner.m_watchGroupStaffPanelCountText;
        }
        set
        {
          this.m_owner.m_watchGroupStaffPanelCountText = value;
        }
      }

      public Text m_groupTitleName
      {
        get
        {
          return this.m_owner.m_groupTitleName;
        }
        set
        {
          this.m_owner.m_groupTitleName = value;
        }
      }

      public CommonUIStateController m_changeGroupNameUIStateController
      {
        get
        {
          return this.m_owner.m_changeGroupNameUIStateController;
        }
        set
        {
          this.m_owner.m_changeGroupNameUIStateController = value;
        }
      }

      public Button m_changeGroupNameBackgroundButton
      {
        get
        {
          return this.m_owner.m_changeGroupNameBackgroundButton;
        }
        set
        {
          this.m_owner.m_changeGroupNameBackgroundButton = value;
        }
      }

      public Button m_changeGroupNameConfirmButton
      {
        get
        {
          return this.m_owner.m_changeGroupNameConfirmButton;
        }
        set
        {
          this.m_owner.m_changeGroupNameConfirmButton = value;
        }
      }

      public InputField m_changeNamePanelInputField
      {
        get
        {
          return this.m_owner.m_changeNamePanelInputField;
        }
        set
        {
          this.m_owner.m_changeNamePanelInputField = value;
        }
      }

      public CommonUIStateController m_inviteFriendToGroupPanelUIStateController
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelUIStateController;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelUIStateController = value;
        }
      }

      public GameObject m_inviteFriendToGroupPanelScrollView
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelScrollView;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelScrollView = value;
        }
      }

      public GameObject m_inviteFriendToGroupPanelScrollViewContent
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelScrollViewContent;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelScrollViewContent = value;
        }
      }

      public Button m_inviteFriendToGroupPanelConfirmButton
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelConfirmButton;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelConfirmButton = value;
        }
      }

      public Button m_inviteFriendToGroupPanelCancelButton
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelCancelButton;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelCancelButton = value;
        }
      }

      public Button m_inviteFriendToGroupPanelBackgroundButton
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelBackgroundButton;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelBackgroundButton = value;
        }
      }

      public Text m_inviteFriendToGroupPanelCountText
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelCountText;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelCountText = value;
        }
      }

      public Toggle m_inviteFriendToGroupPanelServerFriendToggle
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelServerFriendToggle;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelServerFriendToggle = value;
        }
      }

      public Toggle m_inviteFriendToGroupPanelFriendToggle
      {
        get
        {
          return this.m_owner.m_inviteFriendToGroupPanelFriendToggle;
        }
        set
        {
          this.m_owner.m_inviteFriendToGroupPanelFriendToggle = value;
        }
      }

      public CommonUIStateController m_shieldPanelUIStateController
      {
        get
        {
          return this.m_owner.m_shieldPanelUIStateController;
        }
        set
        {
          this.m_owner.m_shieldPanelUIStateController = value;
        }
      }

      public GameObject m_shieldPanelScrollView
      {
        get
        {
          return this.m_owner.m_shieldPanelScrollView;
        }
        set
        {
          this.m_owner.m_shieldPanelScrollView = value;
        }
      }

      public GameObject m_shieldPanelScrollViewContent
      {
        get
        {
          return this.m_owner.m_shieldPanelScrollViewContent;
        }
        set
        {
          this.m_owner.m_shieldPanelScrollViewContent = value;
        }
      }

      public Button m_shieldPanelBackgroundButton
      {
        get
        {
          return this.m_owner.m_shieldPanelBackgroundButton;
        }
        set
        {
          this.m_owner.m_shieldPanelBackgroundButton = value;
        }
      }

      public Button m_shieldPanelCloseButton
      {
        get
        {
          return this.m_owner.m_shieldPanelCloseButton;
        }
        set
        {
          this.m_owner.m_shieldPanelCloseButton = value;
        }
      }

      public Text m_shieldPanelShieldCount
      {
        get
        {
          return this.m_owner.m_shieldPanelShieldCount;
        }
        set
        {
          this.m_owner.m_shieldPanelShieldCount = value;
        }
      }

      public GameObject m_friendScrollView
      {
        get
        {
          return this.m_owner.m_friendScrollView;
        }
        set
        {
          this.m_owner.m_friendScrollView = value;
        }
      }

      public GameObject m_friendScrollViewContent
      {
        get
        {
          return this.m_owner.m_friendScrollViewContent;
        }
        set
        {
          this.m_owner.m_friendScrollViewContent = value;
        }
      }

      public GameObject m_groupScrollView
      {
        get
        {
          return this.m_owner.m_groupScrollView;
        }
        set
        {
          this.m_owner.m_groupScrollView = value;
        }
      }

      public GameObject m_groupScrollViewContent
      {
        get
        {
          return this.m_owner.m_groupScrollViewContent;
        }
        set
        {
          this.m_owner.m_groupScrollViewContent = value;
        }
      }

      public GameObject m_friendBigItemPrefab
      {
        get
        {
          return this.m_owner.m_friendBigItemPrefab;
        }
        set
        {
          this.m_owner.m_friendBigItemPrefab = value;
        }
      }

      public GameObject m_friendSmallItemPrefab
      {
        get
        {
          return this.m_owner.m_friendSmallItemPrefab;
        }
        set
        {
          this.m_owner.m_friendSmallItemPrefab = value;
        }
      }

      public GameObject m_groupItemPrefab
      {
        get
        {
          return this.m_owner.m_groupItemPrefab;
        }
        set
        {
          this.m_owner.m_groupItemPrefab = value;
        }
      }

      public int m_currentServerID
      {
        get
        {
          return this.m_owner.m_currentServerID;
        }
        set
        {
          this.m_owner.m_currentServerID = value;
        }
      }

      public ProChatGroupInfo m_currentChatGroupInfo
      {
        get
        {
          return this.m_owner.m_currentChatGroupInfo;
        }
        set
        {
          this.m_owner.m_currentChatGroupInfo = value;
        }
      }

      public bool m_isCreateNewGroup
      {
        get
        {
          return this.m_owner.m_isCreateNewGroup;
        }
        set
        {
          this.m_owner.m_isCreateNewGroup = value;
        }
      }

      public static int m_maxInviteToGroupCount
      {
        get
        {
          return 10;
        }
      }

      public List<UserSummary> m_recommendFriendList
      {
        get
        {
          return this.m_owner.m_recommendFriendList;
        }
        set
        {
          this.m_owner.m_recommendFriendList = value;
        }
      }

      public List<UserSummary> m_applyFriendList
      {
        get
        {
          return this.m_owner.m_applyFriendList;
        }
        set
        {
          this.m_owner.m_applyFriendList = value;
        }
      }

      public List<string> m_inviteToGroupList
      {
        get
        {
          return this.m_owner.m_inviteToGroupList;
        }
        set
        {
          this.m_owner.m_inviteToGroupList = value;
        }
      }

      public List<UserSummary> m_groupStaffList
      {
        get
        {
          return this.m_owner.m_groupStaffList;
        }
        set
        {
          this.m_owner.m_groupStaffList = value;
        }
      }

      public List<string> m_friendApplyAcceptList
      {
        get
        {
          return this.m_owner.m_friendApplyAcceptList;
        }
        set
        {
          this.m_owner.m_friendApplyAcceptList = value;
        }
      }

      public List<string> m_friendApplyDeclineList
      {
        get
        {
          return this.m_owner.m_friendApplyDeclineList;
        }
        set
        {
          this.m_owner.m_friendApplyDeclineList = value;
        }
      }

      public List<string> m_friendAddList
      {
        get
        {
          return this.m_owner.m_friendAddList;
        }
        set
        {
          this.m_owner.m_friendAddList = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void SetAddFriendPanel()
      {
        this.m_owner.SetAddFriendPanel();
      }

      public void SetInviteFriendToGroupList()
      {
        this.m_owner.SetInviteFriendToGroupList();
      }

      public void SetInviteAcrossServerFriendToGroupList()
      {
        this.m_owner.SetInviteAcrossServerFriendToGroupList();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetBasicBigFriendInfo(
        GameObject parentScrollViewContent,
        List<UserSummary> userList,
        FriendInfoType friendInfoType,
        bool isChatGroupOwner)
      {
        // ISSUE: unable to decompile the method.
      }

      public void SetBasicSmallFriendInfo(
        GameObject parentScrollViewContent,
        List<UserSummary> userList,
        FriendInfoType friendInfoType)
      {
        this.m_owner.SetBasicSmallFriendInfo(parentScrollViewContent, userList, friendInfoType);
      }

      public void SetServerListInfo()
      {
        this.m_owner.SetServerListInfo();
      }

      public void OnFriendToggle(bool isOn)
      {
        this.m_owner.OnFriendToggle(isOn);
      }

      public void OnAcrossServerToggle(bool isOn)
      {
        this.m_owner.OnAcrossServerToggle(isOn);
      }

      public void OnRecentToggle(bool isOn)
      {
        this.m_owner.OnRecentToggle(isOn);
      }

      public void OnGroupToggle(bool isOn)
      {
        this.m_owner.OnGroupToggle(isOn);
      }

      public void OnReturnButtonClick()
      {
        this.m_owner.OnReturnButtonClick();
      }

      public void OnHelpButtonClick()
      {
        this.m_owner.OnHelpButtonClick();
      }

      public void OnShieldButtonClick()
      {
        this.m_owner.OnShieldButtonClick();
      }

      public void OnShieldPanelCloseButtonClick()
      {
        this.m_owner.OnShieldPanelCloseButtonClick();
      }

      public void OnSendAllFriendshipPointButtonClick()
      {
        this.m_owner.OnSendAllFriendshipPointButtonClick();
      }

      public void OnGetAllFriendshipPointButtonClick()
      {
        this.m_owner.OnGetAllFriendshipPointButtonClick();
      }

      public void OnAddFriendPanelCloseButtonClick()
      {
        this.m_owner.OnAddFriendPanelCloseButtonClick();
      }

      public void OnSelectServerButtonClick()
      {
        this.m_owner.OnSelectServerButtonClick();
      }

      public void OnServerListPanelBGButtonClick()
      {
        this.m_owner.OnServerListPanelBGButtonClick();
      }

      public void SetSendAllFriendshipPointButtonState()
      {
        this.m_owner.SetSendAllFriendshipPointButtonState();
      }

      public void SetGetAllFriendshipPointButtonState()
      {
        this.m_owner.SetGetAllFriendshipPointButtonState();
      }

      public void OnAddFriendPanelSearchButtonClick()
      {
        this.m_owner.OnAddFriendPanelSearchButtonClick();
      }

      public void OnAddFriendPanelChangeRecommendFriendButtonClick()
      {
        this.m_owner.OnAddFriendPanelChangeRecommendFriendButtonClick();
      }

      public void OnAddFriendPanelAddAllButtonClick()
      {
        this.m_owner.OnAddFriendPanelAddAllButtonClick();
      }

      public void OnAddFriendPanelAcceptAllButtonClick()
      {
        this.m_owner.OnAddFriendPanelAcceptAllButtonClick();
      }

      public void OnAddFriendPanelRefuseAllButtonClick()
      {
        this.m_owner.OnAddFriendPanelRefuseAllButtonClick();
      }

      public void OnWatchGroupStaffPanelCloseButtonClick()
      {
        this.m_owner.OnWatchGroupStaffPanelCloseButtonClick();
      }

      public void OnWatchGroupStaffPanelChangeNameButtonClick()
      {
        this.m_owner.OnWatchGroupStaffPanelChangeNameButtonClick();
      }

      public void OnWatchGroupStaffPanelChangeNameConfirmButtonClick()
      {
        this.m_owner.OnWatchGroupStaffPanelChangeNameConfirmButtonClick();
      }

      public void OnQuitGroupButtonClick()
      {
        this.m_owner.OnQuitGroupButtonClick();
      }

      public void OnCreateNewGroupButtonClick()
      {
        this.m_owner.OnCreateNewGroupButtonClick();
      }

      public void OnInviteFriendToGroupButtonClick()
      {
        this.m_owner.OnInviteFriendToGroupButtonClick();
      }

      public void OnInviteFriendToGoupPanelConfirmButtonClick()
      {
        this.m_owner.OnInviteFriendToGoupPanelConfirmButtonClick();
      }

      public void OnInviteFriendToGroupPanelServerFriendToggle(bool isOn)
      {
        this.m_owner.OnInviteFriendToGroupPanelServerFriendToggle(isOn);
      }

      public void OnInviteFriendToGroupPanelFriendToggle(bool isOn)
      {
        this.m_owner.OnInviteFriendToGroupPanelFriendToggle(isOn);
      }

      public void OnPlayerItemButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnPlayerItemButtonClick(ctrl);
      }

      public void OnPlayerItemButtonClick(FriendSmallItemUIController ctrl)
      {
        this.m_owner.OnPlayerItemButtonClick(ctrl);
      }

      public void OnItemAddFriendButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnItemAddFriendButtonClick(ctrl);
      }

      public void OnItemKickFromGroupButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnItemKickFromGroupButtonClick(ctrl);
      }

      public void OnUnblockPlayerButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnUnblockPlayerButtonClick(ctrl);
      }

      public void OnChatButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnChatButtonClick(ctrl);
      }

      public void OnSendFriendshipPointButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnSendFriendshipPointButtonClick(ctrl);
      }

      public void OnGetFriendshipPointButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnGetFriendshipPointButtonClick(ctrl);
      }

      public void OnFriendshipPointDoneButtonClick(FriendBigItemUIController ctrl)
      {
        this.m_owner.OnFriendshipPointDoneButtonClick(ctrl);
      }

      public void OnItemAddFriendButtonClick(FriendSmallItemUIController ctrl)
      {
        this.m_owner.OnItemAddFriendButtonClick(ctrl);
      }

      public void OnChatButtonClick(FriendSmallItemUIController ctrl)
      {
        this.m_owner.OnChatButtonClick(ctrl);
      }

      public void OnFriendApplyAceeptButtonClick(FriendSmallItemUIController ctrl)
      {
        this.m_owner.OnFriendApplyAceeptButtonClick(ctrl);
      }

      public void OnFriendApplyDeclineButtonClick(FriendSmallItemUIController ctrl)
      {
        this.m_owner.OnFriendApplyDeclineButtonClick(ctrl);
      }

      public void OnInviteFriendToGroupToggleValueChanged(
        bool isOn,
        FriendSmallItemUIController ctrl)
      {
        this.m_owner.OnInviteFriendToGroupToggleValueChanged(isOn, ctrl);
      }

      public void OnGroupChatButtonClick(FriendGroupUIController ctrl)
      {
        this.m_owner.OnGroupChatButtonClick(ctrl);
      }

      public void OnWatchGroupStaffButtonClick(FriendGroupUIController ctrl)
      {
        this.m_owner.OnWatchGroupStaffButtonClick(ctrl);
      }
    }
  }
}
