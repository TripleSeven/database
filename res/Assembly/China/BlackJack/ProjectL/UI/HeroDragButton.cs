﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDragButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class HeroDragButton : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headImage;
    [AutoBind("./ArmyIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyImage;
    [AutoBind("./LvGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    private GameObject m_scoreBonusTagGameObject;
    private GameObject m_powerUpTagGameObject;
    private GameObject m_dropUpTagGameObject;
    private CommonUIStateController m_uiStateController;
    private GridPosition m_position;
    private BattleHero m_hero;
    private bool m_isEnabled;
    private const int InvalidPointerDownID = -1000;
    private int m_pointerDownId;
    private bool m_isDetectingDrag;
    private bool m_ignoreClick;
    private Vector2 m_pointerDownPosition;
    private PointerEventData m_dragEventData;
    [DoNotToLua]
    private HeroDragButton.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_OnBindFiledsCompleted_hotfix;
    private LuaFunction m_Destroy_hotfix;
    private LuaFunction m_SetHeroBattleHero_hotfix;
    private LuaFunction m_GetHero_hotfix;
    private LuaFunction m_SetPositionGridPosition_hotfix;
    private LuaFunction m_GetPosition_hotfix;
    private LuaFunction m_DisableRaycastTarget_hotfix;
    private LuaFunction m_SetEnabledBoolean_hotfix;
    private LuaFunction m_IsEnabled_hotfix;
    private LuaFunction m_SetTagTypeStageActorTagType_hotfix;
    private LuaFunction m_OnPointerDownPointerEventData_hotfix;
    private LuaFunction m_OnPointerUpPointerEventData_hotfix;
    private LuaFunction m_OnPointerClickPointerEventData_hotfix;
    private LuaFunction m_CancelDrag_hotfix;
    private LuaFunction m_OnBeginDragPointerEventData_hotfix;
    private LuaFunction m_OnEndDragPointerEventData_hotfix;
    private LuaFunction m_OnDragPointerEventData_hotfix;
    private LuaFunction m_OnDropPointerEventData_hotfix;
    private LuaFunction m_Update_hotfix;
    private LuaFunction m_add_EventOnClickAction`1_hotfix;
    private LuaFunction m_remove_EventOnClickAction`1_hotfix;
    private LuaFunction m_add_EventOnBeginDragAction`2_hotfix;
    private LuaFunction m_remove_EventOnBeginDragAction`2_hotfix;
    private LuaFunction m_add_EventOnEndDragAction`2_hotfix;
    private LuaFunction m_remove_EventOnEndDragAction`2_hotfix;
    private LuaFunction m_add_EventOnDragAction`1_hotfix;
    private LuaFunction m_remove_EventOnDragAction`1_hotfix;
    private LuaFunction m_add_EventOnDropAction`1_hotfix;
    private LuaFunction m_remove_EventOnDropAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDragButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisableRaycastTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnabled(bool enabled)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnabled()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTagType(StageActorTagType tagType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroDragButton> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroDragButton, PointerEventData> EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroDragButton, PointerEventData> EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroDragButton.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initlize(string ctrlName, bool bindNow)
    {
      this.Initlize(ctrlName, bindNow);
    }

    private void __callBase_BindFields()
    {
      this.BindFields();
    }

    private void __callBase_OnBindFiledsCompleted()
    {
      base.OnBindFiledsCompleted();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object __callBase_BindFieldImpl(
      System.Type fieldType,
      string path,
      AutoBindAttribute.InitState initState,
      string fieldName,
      string ctrlName,
      bool optional)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __callBase_Clear()
    {
      this.Clear();
    }

    private void __callBase_OnButtonClick(Button button, string fieldName)
    {
      this.OnButtonClick(button, fieldName);
    }

    private void __callBase_SetButtonClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldName, action);
    }

    private void __callBase_SetButtonClickListener(
      string[] fieldNames,
      Action<UIControllerBase> action)
    {
      this.SetButtonClickListener(fieldNames, action);
    }

    private void __callBase_SetButtonDoubleClickListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonDoubleClickListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressStartListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressStartListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressingListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressingListener(fieldName, action);
    }

    private void __callBase_SetButtonLongPressEndListener(
      string fieldName,
      Action<UIControllerBase> action)
    {
      this.SetButtonLongPressEndListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string fieldName,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldName, action);
    }

    private void __callBase_SetToggleValueChangedListener(
      string[] fieldNames,
      Action<UIControllerBase, bool> action)
    {
      this.SetToggleValueChangedListener(fieldNames, action);
    }

    private void __callBase_OnDestroy()
    {
      this.OnDestroy();
    }

    private string __callBase_ToString()
    {
      return this.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnClick(HeroDragButton obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnClick(HeroDragButton obj)
    {
      this.EventOnClick = (Action<HeroDragButton>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnBeginDrag(HeroDragButton arg1, PointerEventData arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnBeginDrag(HeroDragButton arg1, PointerEventData arg2)
    {
      this.EventOnBeginDrag = (Action<HeroDragButton, PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnEndDrag(HeroDragButton arg1, PointerEventData arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnEndDrag(HeroDragButton arg1, PointerEventData arg2)
    {
      this.EventOnEndDrag = (Action<HeroDragButton, PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDrag(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnDrag(PointerEventData obj)
    {
      this.EventOnDrag = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnDrop(PointerEventData obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnDrop(PointerEventData obj)
    {
      this.EventOnDrop = (Action<PointerEventData>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroDragButton m_owner;

      public LuaExportHelper(HeroDragButton owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initlize(string ctrlName, bool bindNow)
      {
        this.m_owner.__callBase_Initlize(ctrlName, bindNow);
      }

      public void __callBase_BindFields()
      {
        this.m_owner.__callBase_BindFields();
      }

      public void __callBase_OnBindFiledsCompleted()
      {
        this.m_owner.__callBase_OnBindFiledsCompleted();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public UnityEngine.Object __callBase_BindFieldImpl(
        System.Type fieldType,
        string path,
        AutoBindAttribute.InitState initState,
        string fieldName,
        string ctrlName,
        bool optional)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __callBase_Clear()
      {
        this.m_owner.__callBase_Clear();
      }

      public void __callBase_OnButtonClick(Button button, string fieldName)
      {
        this.m_owner.__callBase_OnButtonClick(button, fieldName);
      }

      public void __callBase_SetButtonClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldName, action);
      }

      public void __callBase_SetButtonClickListener(
        string[] fieldNames,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonClickListener(fieldNames, action);
      }

      public void __callBase_SetButtonDoubleClickListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonDoubleClickListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressStartListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressStartListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressingListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressingListener(fieldName, action);
      }

      public void __callBase_SetButtonLongPressEndListener(
        string fieldName,
        Action<UIControllerBase> action)
      {
        this.m_owner.__callBase_SetButtonLongPressEndListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string fieldName,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldName, action);
      }

      public void __callBase_SetToggleValueChangedListener(
        string[] fieldNames,
        Action<UIControllerBase, bool> action)
      {
        this.m_owner.__callBase_SetToggleValueChangedListener(fieldNames, action);
      }

      public void __callBase_OnDestroy()
      {
        this.m_owner.__callBase_OnDestroy();
      }

      public string __callBase_ToString()
      {
        return this.m_owner.__callBase_ToString();
      }

      public void __callDele_EventOnClick(HeroDragButton obj)
      {
        this.m_owner.__callDele_EventOnClick(obj);
      }

      public void __clearDele_EventOnClick(HeroDragButton obj)
      {
        this.m_owner.__clearDele_EventOnClick(obj);
      }

      public void __callDele_EventOnBeginDrag(HeroDragButton arg1, PointerEventData arg2)
      {
        this.m_owner.__callDele_EventOnBeginDrag(arg1, arg2);
      }

      public void __clearDele_EventOnBeginDrag(HeroDragButton arg1, PointerEventData arg2)
      {
        this.m_owner.__clearDele_EventOnBeginDrag(arg1, arg2);
      }

      public void __callDele_EventOnEndDrag(HeroDragButton arg1, PointerEventData arg2)
      {
        this.m_owner.__callDele_EventOnEndDrag(arg1, arg2);
      }

      public void __clearDele_EventOnEndDrag(HeroDragButton arg1, PointerEventData arg2)
      {
        this.m_owner.__clearDele_EventOnEndDrag(arg1, arg2);
      }

      public void __callDele_EventOnDrag(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnDrag(obj);
      }

      public void __clearDele_EventOnDrag(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnDrag(obj);
      }

      public void __callDele_EventOnDrop(PointerEventData obj)
      {
        this.m_owner.__callDele_EventOnDrop(obj);
      }

      public void __clearDele_EventOnDrop(PointerEventData obj)
      {
        this.m_owner.__clearDele_EventOnDrop(obj);
      }

      public Button m_button
      {
        get
        {
          return this.m_owner.m_button;
        }
        set
        {
          this.m_owner.m_button = value;
        }
      }

      public Image m_headImage
      {
        get
        {
          return this.m_owner.m_headImage;
        }
        set
        {
          this.m_owner.m_headImage = value;
        }
      }

      public Image m_armyImage
      {
        get
        {
          return this.m_owner.m_armyImage;
        }
        set
        {
          this.m_owner.m_armyImage = value;
        }
      }

      public Text m_levelText
      {
        get
        {
          return this.m_owner.m_levelText;
        }
        set
        {
          this.m_owner.m_levelText = value;
        }
      }

      public GameObject m_scoreBonusTagGameObject
      {
        get
        {
          return this.m_owner.m_scoreBonusTagGameObject;
        }
        set
        {
          this.m_owner.m_scoreBonusTagGameObject = value;
        }
      }

      public GameObject m_powerUpTagGameObject
      {
        get
        {
          return this.m_owner.m_powerUpTagGameObject;
        }
        set
        {
          this.m_owner.m_powerUpTagGameObject = value;
        }
      }

      public GameObject m_dropUpTagGameObject
      {
        get
        {
          return this.m_owner.m_dropUpTagGameObject;
        }
        set
        {
          this.m_owner.m_dropUpTagGameObject = value;
        }
      }

      public CommonUIStateController m_uiStateController
      {
        get
        {
          return this.m_owner.m_uiStateController;
        }
        set
        {
          this.m_owner.m_uiStateController = value;
        }
      }

      public GridPosition m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public BattleHero m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public bool m_isEnabled
      {
        get
        {
          return this.m_owner.m_isEnabled;
        }
        set
        {
          this.m_owner.m_isEnabled = value;
        }
      }

      public static int InvalidPointerDownID
      {
        get
        {
          return -1000;
        }
      }

      public int m_pointerDownId
      {
        get
        {
          return this.m_owner.m_pointerDownId;
        }
        set
        {
          this.m_owner.m_pointerDownId = value;
        }
      }

      public bool m_isDetectingDrag
      {
        get
        {
          return this.m_owner.m_isDetectingDrag;
        }
        set
        {
          this.m_owner.m_isDetectingDrag = value;
        }
      }

      public bool m_ignoreClick
      {
        get
        {
          return this.m_owner.m_ignoreClick;
        }
        set
        {
          this.m_owner.m_ignoreClick = value;
        }
      }

      public Vector2 m_pointerDownPosition
      {
        get
        {
          return this.m_owner.m_pointerDownPosition;
        }
        set
        {
          this.m_owner.m_pointerDownPosition = value;
        }
      }

      public PointerEventData m_dragEventData
      {
        get
        {
          return this.m_owner.m_dragEventData;
        }
        set
        {
          this.m_owner.m_dragEventData = value;
        }
      }

      public void OnBindFiledsCompleted()
      {
        this.m_owner.OnBindFiledsCompleted();
      }

      public void OnBeginDrag(PointerEventData eventData)
      {
        this.m_owner.OnBeginDrag(eventData);
      }

      public void OnEndDrag(PointerEventData eventData)
      {
        this.m_owner.OnEndDrag(eventData);
      }

      public void OnDrag(PointerEventData eventData)
      {
        this.m_owner.OnDrag(eventData);
      }

      public void OnDrop(PointerEventData eventData)
      {
        this.m_owner.OnDrop(eventData);
      }

      public void Update()
      {
        this.m_owner.Update();
      }
    }
  }
}
