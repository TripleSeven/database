﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierShowItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Scene;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [HotFix]
  public class SoldierShowItemController
  {
    public UISpineGraphic m_soldierSkinGraphic;
    public Text m_nameText;
    public GameObject m_spineObj;
    public CommonUIStateController m_itemStateCtrl;
    private GameObject m_item;
    [DoNotToLua]
    private SoldierShowItemController.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctorGameObject_hotfix;
    private LuaFunction m_SetActiveBoolean_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public SoldierShowItemController(GameObject soldierItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActive(bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public SoldierShowItemController.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private SoldierShowItemController m_owner;

      public LuaExportHelper(SoldierShowItemController owner)
      {
        this.m_owner = owner;
      }

      public GameObject m_item
      {
        get
        {
          return this.m_owner.m_item;
        }
        set
        {
          this.m_owner.m_item = value;
        }
      }
    }
  }
}
