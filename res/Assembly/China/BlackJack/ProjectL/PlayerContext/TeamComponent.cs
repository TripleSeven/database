﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.TeamComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class TeamComponent : TeamComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSTeamNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateTeam(TeamRoom room)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoMatchTeamRoom(
      GameFunctionType gameFunctionTypeId,
      int locationId,
      TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeTeamRoomAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamRoom(TeamRoom room)
    {
    }

    public override void QuitTeamRoom()
    {
      this.Room = (TeamRoom) null;
      base.QuitTeamRoom();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerJoinTeamRoom(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerQuitTeamRoom(
      TeamRoomPlayer quitPlayer,
      ulong leaderSessionId,
      long leaderKickOutTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LeaveWaitingListAndJoinRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeductTeamPveBattleEnergyByClient()
    {
    }

    public TeamRoom Room { get; set; }

    public bool IsTeamRoomInviteAgain { get; set; }
  }
}
