﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GlobalRankingListComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class GlobalRankingListComponent : GlobalRankingListComponentCommon
  {
    protected Dictionary<RankingListType, GlobalRankingListComponent.CachedRankingListInfo> m_cachedRankingListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_cachedSingleHeroRankListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_cachedSingleAncientCallBossRankListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_friendAndGuildSingleAncientCallBossRankListDict;
    protected const float OutDateTime = 15f;

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalRankingListComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRankingListInfoToCache(
      RankingListType rankingType,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetRankingListInfoByType(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRankingListInfoValid(RankingListType rankingListType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSingleHeroRankingListInfoToCache(int heroId, RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSingleAncientCallBossRankingListInfoToCache(
      int bossId,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendAndGuildSingleAncientCallBossRankingListInfoToCache(
      int bossId,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleBossRankingListInfoByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetFriendAndGuildSingleBossRankingListInfoByBossId(
      int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleHeroRankingListInfoByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAbleQueryRankingListInfo(RankingListType rankingListType, out int errorCode)
    {
      return this.CheckRankingListInfoQuery(rankingListType, out errorCode);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSingleHeroRankingListInfoValid(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class CachedRankingListInfo
    {
      public DateTime m_outDateTime;
      public RankingListInfo m_rankingListInfo;
    }
  }
}
