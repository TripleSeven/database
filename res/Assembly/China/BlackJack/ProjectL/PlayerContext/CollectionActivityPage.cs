﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class CollectionActivityPage
  {
    public int id;
    public string name;
    public int coverHeroId;
    public int startHour;
    public bool isLockedByTime;
    public bool isLockedByLevel;
    public string lockedLevelName;
    public bool isReserved;
    public List<CollectionActivityMission> missionList;
    [DoNotToLua]
    private CollectionActivityPage.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Sort(CollectionActivityPage x, CollectionActivityPage y)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public CollectionActivityPage.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityPage m_owner;

      public LuaExportHelper(CollectionActivityPage owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
