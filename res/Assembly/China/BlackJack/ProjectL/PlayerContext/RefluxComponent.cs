﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.RefluxComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class RefluxComponent : RefluxComponentCommon
  {
    [DoNotToLua]
    private RefluxComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSRefluxNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_IsRefluxActivityOpen_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public RefluxComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSRefluxNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRefluxActivityOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public RefluxComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private int __callBase_ClaimReward(int Slot, bool NoCheck)
    {
      return this.ClaimReward(Slot, NoCheck);
    }

    private int __callBase_CanClaimReward(int Slot)
    {
      return this.CanClaimReward(Slot);
    }

    private int __callBase_GetMissionPoints()
    {
      return this.GetMissionPoints();
    }

    private bool __callBase_IsRewardClaimed(int Slot)
    {
      return this.IsRewardClaimed(Slot);
    }

    private int __callBase_GetDaysAfterOpening()
    {
      return this.GetDaysAfterOpening();
    }

    private bool __callBase_IsTimeOut()
    {
      return this.IsTimeOut();
    }

    private void __callBase_AddMissionPoints(int Delta)
    {
      this.AddMissionPoints(Delta);
    }

    private List<Mission> __callBase_GetProcessingMissions()
    {
      return this.GetProcessingMissions();
    }

    private List<Mission> __callBase_GetFinishedMissions()
    {
      return this.GetFinishedMissions();
    }

    private List<ConfigDataRefluxRewardInfo> __callBase_GetRefluxPointsRewardsConfig()
    {
      return this.GetRefluxPointsRewardsConfig();
    }

    private long __callBase_GetMissionDay(ConfigDataMissionInfo Mission)
    {
      return this.GetMissionDay(Mission);
    }

    private Dictionary<long, List<long>> __callBase_GetMissions()
    {
      return this.GetMissions();
    }

    private TimeSpan __callBase_GetMissionsEndTime()
    {
      return this.GetMissionsEndTime();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private RefluxComponent m_owner;

      public LuaExportHelper(RefluxComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public int __callBase_ClaimReward(int Slot, bool NoCheck)
      {
        return this.m_owner.__callBase_ClaimReward(Slot, NoCheck);
      }

      public int __callBase_CanClaimReward(int Slot)
      {
        return this.m_owner.__callBase_CanClaimReward(Slot);
      }

      public int __callBase_GetMissionPoints()
      {
        return this.m_owner.__callBase_GetMissionPoints();
      }

      public bool __callBase_IsRewardClaimed(int Slot)
      {
        return this.m_owner.__callBase_IsRewardClaimed(Slot);
      }

      public int __callBase_GetDaysAfterOpening()
      {
        return this.m_owner.__callBase_GetDaysAfterOpening();
      }

      public bool __callBase_IsTimeOut()
      {
        return this.m_owner.__callBase_IsTimeOut();
      }

      public void __callBase_AddMissionPoints(int Delta)
      {
        this.m_owner.__callBase_AddMissionPoints(Delta);
      }

      public List<Mission> __callBase_GetProcessingMissions()
      {
        return this.m_owner.__callBase_GetProcessingMissions();
      }

      public List<Mission> __callBase_GetFinishedMissions()
      {
        return this.m_owner.__callBase_GetFinishedMissions();
      }

      public List<ConfigDataRefluxRewardInfo> __callBase_GetRefluxPointsRewardsConfig()
      {
        return this.m_owner.__callBase_GetRefluxPointsRewardsConfig();
      }

      public long __callBase_GetMissionDay(ConfigDataMissionInfo Mission)
      {
        return this.m_owner.__callBase_GetMissionDay(Mission);
      }

      public Dictionary<long, List<long>> __callBase_GetMissions()
      {
        return this.m_owner.__callBase_GetMissions();
      }

      public TimeSpan __callBase_GetMissionsEndTime()
      {
        return this.m_owner.__callBase_GetMissionsEndTime();
      }
    }
  }
}
