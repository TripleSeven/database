﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.NetWorkClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ProjectL.LibClient;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class NetWorkClient : IPlayerContextNetworkClient
  {
    private IClient m_client;

    public NetWorkClient(IClient realClient)
    {
      this.m_client = realClient;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization,
      int loginChannelId,
      int bornChannelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Disconnect()
    {
      return this.m_client.Disconnect();
    }

    public bool SendMessage(object msg)
    {
      return this.m_client.SendMessage(msg);
    }

    public void Tick()
    {
      this.m_client.Tick();
    }

    public void Close()
    {
      this.m_client.Close();
    }

    public void BlockProcessMsg(bool isBlock)
    {
      this.m_client.BlockProcessMsg(isBlock);
    }
  }
}
