﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaPlayOffMatchupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaPlayOffMatchupInfo
  {
    public int Round;
    public int MatchupId;
    public ulong RoomId;
    public int GroupId;
    public PeakArenaPlayOffPlayerInfo PlayerOnLeft;
    public PeakArenaPlayOffPlayerInfo PlayerOnRight;
    public string WinnerId;
    public List<PeakArenaPlayOffBattleInfo> BattleInfos;
    public ulong BattleReportId;
    public int NextMatchupId;
    public int PrevLeftMatchupId;
    public int PrevRightMatchupId;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.MatchState GetMatchupSate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult GetMatchResultByUserId(
      string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult GetLeftUserMatchResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult GetRightUserMatchResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasLeftPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasRightPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLeftPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLeftUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRightPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetRightUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerInThisMatch(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffPlayerInfo GetPlayerByID(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GatBattleCount()
    {
      return this.BattleInfos.Count;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLeftPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRightPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum MatchState
    {
      None,
      WaitMatch,
      InMatch,
      FinishMatch,
    }

    public enum PlayerMatchResult
    {
      None,
      Win,
      Lose,
      Abstention,
    }
  }
}
