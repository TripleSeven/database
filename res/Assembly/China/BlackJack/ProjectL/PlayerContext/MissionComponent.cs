﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.MissionComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class MissionComponent : MissionComponentCommon
  {
    [DoNotToLua]
    private MissionComponent.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_DeSerializeDSMissionNtf_hotfix;
    private LuaFunction m_GetDSVersion_hotfix;
    private LuaFunction m_AddMissionByIdInt32_hotfix;
    private LuaFunction m_CanUnlockMissionInLogicFromInitConfigDataMissionInfo_hotfix;
    private LuaFunction m_CanUnlockMissionInUIInterfaceMission_hotfix;
    private LuaFunction m_GetMissionRewardInt32_hotfix;
    private LuaFunction m_GetAllProcessingMissionList_hotfix;
    private LuaFunction m_GetAllCompletedMissionList_hotfix;
    private LuaFunction m_GetAllFinishedMissionList_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSMissionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMissionById(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool CanUnlockMissionInUIInterface(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllCompletedMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public MissionComponent.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private string __callBase_GetName()
    {
      return this.GetName();
    }

    private void __callBase_Init()
    {
      base.Init();
    }

    private void __callBase_PostInit()
    {
      base.PostInit();
    }

    private void __callBase_DeInit()
    {
      base.DeInit();
    }

    private void __callBase_Tick(uint deltaMillisecond)
    {
      this.Tick(deltaMillisecond);
    }

    private void __callBase_PostDeSerialize()
    {
      this.PostDeSerialize();
    }

    private void __callBase_RemoveProcessingMissionById(int missionId)
    {
      this.RemoveProcessingMissionById(missionId);
    }

    private void __callBase_InitProcessingMission(List<Mission> processingMissionList)
    {
      this.InitProcessingMission(processingMissionList);
    }

    private List<Mission> __callBase_GetProcessingMissionByMissionType(
      MissionType missionType)
    {
      return this.GetProcessingMissionByMissionType(missionType);
    }

    private void __callBase_InitExistMissions()
    {
      this.InitExistMissions();
    }

    private void __callBase_ResetEverydayMissions()
    {
      this.ResetEverydayMissions();
    }

    private void __callBase_InitMissionListByMissionPeriodType(MissionPeriodType missionPeriod)
    {
      this.InitMissionListByMissionPeriodType(missionPeriod);
    }

    private bool __callBase_CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
    {
      return base.CanUnlockMissionInLogicFromInit(missionInfo);
    }

    private void __callBase_OnDirectActivatedMissionLocked(ConfigDataMissionInfo missionConfig)
    {
      this.OnDirectActivatedMissionLocked(missionConfig);
    }

    private bool __callBase_IsNoviceMission(Mission mission)
    {
      return this.IsNoviceMission(mission);
    }

    private bool __callBase_IsRefluxMission(Mission mission)
    {
      return this.IsRefluxMission(mission);
    }

    private bool __callBase_IsRefluxMission(ConfigDataMissionInfo mission)
    {
      return this.IsRefluxMission(mission);
    }

    private bool __callBase_IsNoviceMission(ConfigDataMissionInfo mission)
    {
      return this.IsNoviceMission(mission);
    }

    private bool __callBase_IsNoviceMissionActivated(ConfigDataMissionInfo mission)
    {
      return this.IsNoviceMissionActivated(mission);
    }

    private bool __callBase_IsNoviceMissionActivatedForRewarding(ConfigDataMissionInfo mission)
    {
      return this.IsNoviceMissionActivatedForRewarding(mission);
    }

    private bool __callBase_IsNoviceMissionActivated(Mission mission)
    {
      return this.IsNoviceMissionActivated(mission);
    }

    private bool __callBase_IsRefluxMissionActivated(Mission mission)
    {
      return this.IsRefluxMissionActivated(mission);
    }

    private bool __callBase_IsRefluxMissionActivated(ConfigDataMissionInfo mission)
    {
      return this.IsRefluxMissionActivated(mission);
    }

    private bool __callBase_IsRefluxMissionActivatedForRewarding(ConfigDataMissionInfo mission)
    {
      return this.IsRefluxMissionActivatedForRewarding(mission);
    }

    private bool __callBase_CanGetRewarding(Mission mission)
    {
      return this.CanGetRewarding(mission);
    }

    private bool __callBase_CanUnlockMissionInLogic(ConfigDataMissionInfo missionConfigInfo)
    {
      return this.CanUnlockMissionInLogic(missionConfigInfo);
    }

    private bool __callBase_CanUnlockMission(ConfigDataMissionInfo missionConfigInfo)
    {
      return this.CanUnlockMission(missionConfigInfo);
    }

    private DirectlyActivatedMissionSatatus __callBase_CaculateDirectlyActivationMissionStatus(
      ConfigDataMissionInfo missionConfigInfo)
    {
      return this.CaculateDirectlyActivationMissionStatus(missionConfigInfo);
    }

    private bool __callBase_AddMission(ConfigDataMissionInfo missionInfo)
    {
      return this.AddMission(missionInfo);
    }

    private void __callBase_OnAddProcessingDirectelyActivitedMission(Mission mission)
    {
      this.OnAddProcessingDirectelyActivitedMission(mission);
    }

    private Mission __callBase_GetProcessingMissionByMissionPeriod(
      MissionPeriodType missionPeriodType,
      int missionId)
    {
      return this.GetProcessingMissionByMissionPeriod(missionPeriodType, missionId);
    }

    private void __callBase_OnGetProcessingDirectActivationMissionFail(Mission mission)
    {
      this.OnGetProcessingDirectActivationMissionFail(mission);
    }

    private void __callBase_AddOneOffMission(Mission mission)
    {
      this.AddOneOffMission(mission);
    }

    private bool __callBase_IsMissionFinished(int missionId)
    {
      return this.IsMissionFinished(missionId);
    }

    private void __callBase_InitMissionsFromConfig(List<Mission> missions)
    {
      this.InitMissionsFromConfig(missions);
    }

    private void __callBase_FinishMission(Mission mission)
    {
      this.FinishMission(mission);
    }

    private List<Mission> __callBase_GetAllProcessingRefluxMissionList()
    {
      return this.GetAllProcessingRefluxMissionList();
    }

    private List<Mission> __callBase_GetAllFinishedRefluxMissionList()
    {
      return this.GetAllFinishedRefluxMissionList();
    }

    private List<Mission> __callBase_GetAllProcessingNoviceMissionList()
    {
      return this.GetAllProcessingNoviceMissionList();
    }

    private List<Mission> __callBase_GetAllFinishedNoviceMissionList()
    {
      return this.GetAllFinishedNoviceMissionList();
    }

    private void __callBase_AddMissionProcess(Mission mission, long process)
    {
      this.AddMissionProcess(mission, process);
    }

    private int __callBase_GetMissionMaxProcess(Mission mission)
    {
      return this.GetMissionMaxProcess(mission);
    }

    private bool __callBase_IsMissionProcessFinished(int missionId)
    {
      return this.IsMissionProcessFinished(missionId);
    }

    private int __callBase_CanGainMissionReward(int missionId)
    {
      return this.CanGainMissionReward(missionId);
    }

    private bool __callBase_IsCompleted(int missionId)
    {
      return this.IsCompleted(missionId);
    }

    private void __callBase_GuildMassiveCombatAttack()
    {
      this.GuildMassiveCombatAttack();
    }

    private void __callBase_SetStatisticalData(StatisticalDataType typeId, long nums)
    {
      this.SetStatisticalData(typeId, nums);
    }

    private void __callBase_AddStatisticalData(StatisticalDataType typeId, int nums)
    {
      this.AddStatisticalData(typeId, nums);
    }

    private long __callBase_GetMissionCompletedProcess(Mission mission)
    {
      return this.GetMissionCompletedProcess(mission);
    }

    private void __callBase_RollbackConsumeEnergy(GameFunctionType gameFuncTypeId, int nums)
    {
      this.RollbackConsumeEnergy(gameFuncTypeId, nums);
    }

    private void __callBase_OnUseHeroExpItemCallBack(int itemCount)
    {
      this.OnUseHeroExpItemCallBack(itemCount);
    }

    private void __callBase_OnLoginGameCallBack()
    {
      this.OnLoginGameCallBack();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private MissionComponent m_owner;

      public LuaExportHelper(MissionComponent owner)
      {
        this.m_owner = owner;
      }

      public string __callBase_GetName()
      {
        return this.m_owner.__callBase_GetName();
      }

      public void __callBase_Init()
      {
        this.m_owner.__callBase_Init();
      }

      public void __callBase_PostInit()
      {
        this.m_owner.__callBase_PostInit();
      }

      public void __callBase_DeInit()
      {
        this.m_owner.__callBase_DeInit();
      }

      public void __callBase_Tick(uint deltaMillisecond)
      {
        this.m_owner.__callBase_Tick(deltaMillisecond);
      }

      public void __callBase_PostDeSerialize()
      {
        this.m_owner.__callBase_PostDeSerialize();
      }

      public void __callBase_RemoveProcessingMissionById(int missionId)
      {
        this.m_owner.__callBase_RemoveProcessingMissionById(missionId);
      }

      public void __callBase_InitProcessingMission(List<Mission> processingMissionList)
      {
        this.m_owner.__callBase_InitProcessingMission(processingMissionList);
      }

      public List<Mission> __callBase_GetProcessingMissionByMissionType(
        MissionType missionType)
      {
        return this.m_owner.__callBase_GetProcessingMissionByMissionType(missionType);
      }

      public void __callBase_InitExistMissions()
      {
        this.m_owner.__callBase_InitExistMissions();
      }

      public void __callBase_ResetEverydayMissions()
      {
        this.m_owner.__callBase_ResetEverydayMissions();
      }

      public void __callBase_InitMissionListByMissionPeriodType(MissionPeriodType missionPeriod)
      {
        this.m_owner.__callBase_InitMissionListByMissionPeriodType(missionPeriod);
      }

      public bool __callBase_CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
      {
        return this.m_owner.__callBase_CanUnlockMissionInLogicFromInit(missionInfo);
      }

      public void __callBase_OnDirectActivatedMissionLocked(ConfigDataMissionInfo missionConfig)
      {
        this.m_owner.__callBase_OnDirectActivatedMissionLocked(missionConfig);
      }

      public bool __callBase_IsNoviceMission(Mission mission)
      {
        return this.m_owner.__callBase_IsNoviceMission(mission);
      }

      public bool __callBase_IsRefluxMission(Mission mission)
      {
        return this.m_owner.__callBase_IsRefluxMission(mission);
      }

      public bool __callBase_IsRefluxMission(ConfigDataMissionInfo mission)
      {
        return this.m_owner.__callBase_IsRefluxMission(mission);
      }

      public bool __callBase_IsNoviceMission(ConfigDataMissionInfo mission)
      {
        return this.m_owner.__callBase_IsNoviceMission(mission);
      }

      public bool __callBase_IsNoviceMissionActivated(ConfigDataMissionInfo mission)
      {
        return this.m_owner.__callBase_IsNoviceMissionActivated(mission);
      }

      public bool __callBase_IsNoviceMissionActivatedForRewarding(ConfigDataMissionInfo mission)
      {
        return this.m_owner.__callBase_IsNoviceMissionActivatedForRewarding(mission);
      }

      public bool __callBase_IsNoviceMissionActivated(Mission mission)
      {
        return this.m_owner.__callBase_IsNoviceMissionActivated(mission);
      }

      public bool __callBase_IsRefluxMissionActivated(Mission mission)
      {
        return this.m_owner.__callBase_IsRefluxMissionActivated(mission);
      }

      public bool __callBase_IsRefluxMissionActivated(ConfigDataMissionInfo mission)
      {
        return this.m_owner.__callBase_IsRefluxMissionActivated(mission);
      }

      public bool __callBase_IsRefluxMissionActivatedForRewarding(ConfigDataMissionInfo mission)
      {
        return this.m_owner.__callBase_IsRefluxMissionActivatedForRewarding(mission);
      }

      public bool __callBase_CanGetRewarding(Mission mission)
      {
        return this.m_owner.__callBase_CanGetRewarding(mission);
      }

      public bool __callBase_CanUnlockMissionInLogic(ConfigDataMissionInfo missionConfigInfo)
      {
        return this.m_owner.__callBase_CanUnlockMissionInLogic(missionConfigInfo);
      }

      public bool __callBase_CanUnlockMission(ConfigDataMissionInfo missionConfigInfo)
      {
        return this.m_owner.__callBase_CanUnlockMission(missionConfigInfo);
      }

      public DirectlyActivatedMissionSatatus __callBase_CaculateDirectlyActivationMissionStatus(
        ConfigDataMissionInfo missionConfigInfo)
      {
        return this.m_owner.__callBase_CaculateDirectlyActivationMissionStatus(missionConfigInfo);
      }

      public bool __callBase_AddMission(ConfigDataMissionInfo missionInfo)
      {
        return this.m_owner.__callBase_AddMission(missionInfo);
      }

      public void __callBase_OnAddProcessingDirectelyActivitedMission(Mission mission)
      {
        this.m_owner.__callBase_OnAddProcessingDirectelyActivitedMission(mission);
      }

      public Mission __callBase_GetProcessingMissionByMissionPeriod(
        MissionPeriodType missionPeriodType,
        int missionId)
      {
        return this.m_owner.__callBase_GetProcessingMissionByMissionPeriod(missionPeriodType, missionId);
      }

      public void __callBase_OnGetProcessingDirectActivationMissionFail(Mission mission)
      {
        this.m_owner.__callBase_OnGetProcessingDirectActivationMissionFail(mission);
      }

      public void __callBase_AddOneOffMission(Mission mission)
      {
        this.m_owner.__callBase_AddOneOffMission(mission);
      }

      public bool __callBase_IsMissionFinished(int missionId)
      {
        return this.m_owner.__callBase_IsMissionFinished(missionId);
      }

      public void __callBase_InitMissionsFromConfig(List<Mission> missions)
      {
        this.m_owner.__callBase_InitMissionsFromConfig(missions);
      }

      public void __callBase_FinishMission(Mission mission)
      {
        this.m_owner.__callBase_FinishMission(mission);
      }

      public List<Mission> __callBase_GetAllProcessingRefluxMissionList()
      {
        return this.m_owner.__callBase_GetAllProcessingRefluxMissionList();
      }

      public List<Mission> __callBase_GetAllFinishedRefluxMissionList()
      {
        return this.m_owner.__callBase_GetAllFinishedRefluxMissionList();
      }

      public List<Mission> __callBase_GetAllProcessingNoviceMissionList()
      {
        return this.m_owner.__callBase_GetAllProcessingNoviceMissionList();
      }

      public List<Mission> __callBase_GetAllFinishedNoviceMissionList()
      {
        return this.m_owner.__callBase_GetAllFinishedNoviceMissionList();
      }

      public void __callBase_AddMissionProcess(Mission mission, long process)
      {
        this.m_owner.__callBase_AddMissionProcess(mission, process);
      }

      public int __callBase_GetMissionMaxProcess(Mission mission)
      {
        return this.m_owner.__callBase_GetMissionMaxProcess(mission);
      }

      public bool __callBase_IsMissionProcessFinished(int missionId)
      {
        return this.m_owner.__callBase_IsMissionProcessFinished(missionId);
      }

      public int __callBase_CanGainMissionReward(int missionId)
      {
        return this.m_owner.__callBase_CanGainMissionReward(missionId);
      }

      public bool __callBase_IsCompleted(int missionId)
      {
        return this.m_owner.__callBase_IsCompleted(missionId);
      }

      public void __callBase_GuildMassiveCombatAttack()
      {
        this.m_owner.__callBase_GuildMassiveCombatAttack();
      }

      public void __callBase_SetStatisticalData(StatisticalDataType typeId, long nums)
      {
        this.m_owner.__callBase_SetStatisticalData(typeId, nums);
      }

      public void __callBase_AddStatisticalData(StatisticalDataType typeId, int nums)
      {
        this.m_owner.__callBase_AddStatisticalData(typeId, nums);
      }

      public long __callBase_GetMissionCompletedProcess(Mission mission)
      {
        return this.m_owner.__callBase_GetMissionCompletedProcess(mission);
      }

      public void __callBase_RollbackConsumeEnergy(GameFunctionType gameFuncTypeId, int nums)
      {
        this.m_owner.__callBase_RollbackConsumeEnergy(gameFuncTypeId, nums);
      }

      public void __callBase_OnUseHeroExpItemCallBack(int itemCount)
      {
        this.m_owner.__callBase_OnUseHeroExpItemCallBack(itemCount);
      }

      public void __callBase_OnLoginGameCallBack()
      {
        this.m_owner.__callBase_OnLoginGameCallBack();
      }

      public bool CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
      {
        return this.m_owner.CanUnlockMissionInLogicFromInit(missionInfo);
      }

      public bool CanUnlockMissionInUIInterface(Mission mission)
      {
        return this.m_owner.CanUnlockMissionInUIInterface(mission);
      }
    }
  }
}
