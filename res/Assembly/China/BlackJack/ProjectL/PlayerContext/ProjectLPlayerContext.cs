﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ProjectLPlayerContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.UI;
using BlackJack.ServerFramework.Protocol;
using BlackJack.UtilityTools;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class ProjectLPlayerContext : PlayerContextBase, IComponentOwner
  {
    private float m_testMarqueeTime;
    private int m_testMarqueeCount;
    private float m_testChatTime;
    private int m_testChatCount;
    protected long m_serverTime;
    private IConfigDataLoader m_configDataLoader;
    private ComponenetManager<IComponentBase> m_components;
    public PlayerBasicInfoComponent m_playerBasicInfoComponent;
    private HeroComponent m_heroComponent;
    private BagComponent m_bagComponent;
    private BattleComponent m_battleComponent;
    private LevelComponent m_levelComponent;
    private RiftComponent m_riftComponent;
    private HeroDungeonComponent m_heroDungeonComponent;
    private ThearchyTrialComponent m_thearchyTrialComponent;
    private AnikiGymComponent m_anikiGymComponent;
    private TreasureMapComponent m_treasureMapComponent;
    private MemoryCorridorCompoment m_memoryCorridorComponent;
    private MailComponent m_mailComponent;
    private FixedStoreComponent m_fixedStoreComponent;
    private RandomStoreComponent m_randomStoreComponent;
    private SelectCardComponent m_selectCardComponent;
    private RaffleComponent m_raffleComponent;
    private ChatComponent m_chatComponent;
    private ChatGroupComponent m_chatGroupComponent;
    private MissionComponent m_missionComponent;
    private CommentComponent m_commentComponent;
    private TrainingGroundCompoment m_trainingGroundComponent;
    private ArenaComponent m_arenaComponent;
    private OperationalActivityCompoment m_operationalActivityComponent;
    private SurveyComponent m_surveyComponent;
    private DanmakuComponent m_danmakuComponent;
    private HeroTrainningComponent m_heroTrainningComponent;
    private FriendComponent m_friendComponent;
    private GlobalRankingListComponent m_globalRankingListComponent;
    private TeamComponent m_teamComponent;
    private HeroAssistantsCompoment m_heroAssistantsComponent;
    private HeroPhantomCompoment m_heroPhantomComponent;
    private CooperateBattleCompoment m_cooperateBattleComponent;
    private NoviceComponent m_noviceComponent;
    private RefluxComponent m_refluxComponent;
    private RechargeStoreComponent m_rechargeStoreComponent;
    private ResourceComponent m_resourceComponent;
    private RealTimePVPComponent m_realTimePVPComponent;
    private PeakArenaComponent m_peakArenaComponent;
    private GiftStoreComponent m_giftStoreComponent;
    private GuildComponent m_guildComponent;
    private UnchartedScoreComponent m_unchartedScoreComponent;
    private ClimbTowerComponent m_climbTowerComponent;
    private EternalShrineComponent m_eternalShrineComponent;
    private HeroAnthemComponent m_heroAnthemComponent;
    private CollectionActivityComponent m_collectionActivityComponent;
    private AncientCallComponent m_ancientCallComponent;
    private TimeSpan m_diffToServerTime;
    private TimeSpan m_diffToAssignTime;
    private DateTime m_lastReceiveMessageTime;
    private bool m_isPlayerInfoInitEnd;
    private bool m_isDataError;
    private bool m_initOpenStates;
    private Happening m_happening;
    private CurrentBattle m_currentBattle;
    private int m_playerArenaRank;
    private bool m_isNeedGetArenaPlayerInfo;
    private List<ProArenaTopRankPlayer> m_arenaTopRankPlayers;
    private bool ShowEnchantCancelConfirmPanel;
    private bool ShowEnchantSaveConfirmPanel;
    private RewardPlayerStatus m_beforeRewardPlayerStatus;
    private List<RewardHeroStatus> m_beforeRewardHeroStatus;
    private List<int> m_battleHeroIDs;
    private bool m_isNeedRebuildBattle;
    private int m_battleRegretCount;
    private ConfigDataBattleInfo m_firstBattleInfo;
    private List<NoticeText> m_waitingNoticeStringList;
    private bool m_isShowingNotice;
    public Action<int, List<UserSummary>>[] EventsOnGetUserSummaryAck;
    private int summaryToken;
    public Dictionary<int, ProjectLPlayerContext.CollectionActivityState> m_collectionActivityStates;
    private bool m_needGetFriendSocialRelation;
    private List<UserSummary> m_friendList;
    private List<UserSummary> m_acrossServerFriendList;
    private List<UserSummary> m_findFriendList;
    private List<UserSummary> m_recommendFriendList;
    private Dictionary<int, string> m_monthCardStatePrefNameMap;
    private bool isHideEquipMaster;
    private Dictionary<int, Hero> m_allUseableDefaultHeros;
    private ConfigDataScenarioInfo m_activeNextScenarioInfo;
    private ConfigDataScenarioInfo m_activeLastFinishedScenarioInfo;
    private Dictionary<int, ProjectLPlayerContext.CurrentWaypointEvent> m_activeWaypointEvents;
    private int m_enterWorldUITaskCount;
    private List<ulong> ReadAnnounceActivityList;
    private bool m_isBuyGuideActivityViewed;
    private const string OpenActivityNoticeUIFlag = "OpenActivityNoticeUIFlag";
    private RedeemInfoAck m_RedeemInfo;
    private const int RedeemActivityConfigId = 17;
    private const int FansRewardsFromPBTCBTActivityId = 30;
    private List<ProUserSummary> m_peakArenaTopRankPlayerSummaries;
    private List<ProPeakArenaLeaderboardPlayerInfo> m_peakArenaTopRankPlayerInfos;
    private ProPeakArenaLeaderboardPlayerInfo m_peakArenaPlayerInfo;
    private int m_peakArenaSeasonId;
    private PeakArenaSeasonInfo m_peakAreanaSeasonInfo;
    public bool AlreadyGotPeakArenaPlayoffMatchupInfo;
    private string m_heroErrMsg;
    private ProRealTimePVPLeaderboardPlayerInfo m_realtimePVPPlayerInfo;
    private ProRealTimePVPUserInfo m_realtimePVPOpponentPlayerInfo;
    private List<ProRealTimePVPLeaderboardPlayerInfo> m_realtimePVPLocalLeaderboardPlayerInfos;
    private List<ProUserSummary> m_realtimePVPLocalLeaderboardUserSummarys;
    private List<ProRealTimePVPLeaderboardPlayerInfo> m_realtimePVPGlobalLeaderboardPlayerInfos;
    private List<ProUserSummary> m_realtimePVPGlobalLeaderboardUserSummarys;
    private bool m_isRealTimePVPGetTopPlayersReqGlobal;
    private List<int> m_opendRiftChapterIds;
    private ConfigDataRiftChapterInfo m_lastOpenedRiftChapterInfo;
    private bool[] m_rewardFlags;
    private Dictionary<int, bool> m_isSelectHeroRewardTransferToFragmentDict;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLPlayerContext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFullMessageLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SendMessage(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnGameServerMessage(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(ServerHeartBeatNtf msg)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ServerDisconnectNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClientCheatNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public T AddOwnerComponent<T>() where T : class, IComponentBase, new()
    {
      return this.m_components.Add<T>();
    }

    public void RemoveOwnerComponent<T>() where T : class, IComponentBase
    {
      this.m_components.Remove<T>();
    }

    public T GetOwnerComponent<T>() where T : class, IComponentBase
    {
      return this.m_components.GetComponent<T>();
    }

    public IComponentBase GetOwnerComponent(string componentName)
    {
      return this.m_components.GetComponent(componentName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestMarquee()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestChat()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetSessionToken()
    {
      return this.m_sessionToken;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override string GetDeviceId()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override string GetClientVersion()
    {
      return this.m_configDataLoader.UtilityGetVersionString(VersionInfoId.VersionInfoId_ClientProgram);
    }

    public override bool IsDataCacheDirtyByPlayerInfoInitAck(
      object msg,
      out bool raiseCriticalDataCacheDirty)
    {
      raiseCriticalDataCacheDirty = false;
      return false;
    }

    public override bool IsPlayerInfoInitAck4CheckOnly(object msg)
    {
      return false;
    }

    public bool IsLocalGame()
    {
      return this.m_networkClient == null;
    }

    public DateTime GetLocalTime()
    {
      return DateTime.Now;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetServerTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime LocalTimeToServerTime(DateTime t)
    {
      return t + this.m_diffToServerTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAssignTime(DateTime t)
    {
    }

    public DateTime ServerTimeToLocalTime(DateTime t)
    {
      return t - this.m_diffToServerTime;
    }

    public DateTime GetLastReceiveMessageTime()
    {
      return this.m_lastReceiveMessageTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsDataError()
    {
      return this.m_isDataError;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TestWriteMessageToFile(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T TestReadMessageFromFile<T>() where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    public Happening Happening
    {
      get
      {
        return this.m_happening;
      }
    }

    public CurrentBattle CurrentBattle
    {
      get
      {
        return this.m_currentBattle;
      }
    }

    public event Action EventOnClientCheatNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnServerDisconnectNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAncientCallBossAttackReq(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAncientCallBossBattleFinishedReq(int bossId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSAncientCallNtf msg)
    {
      this.m_ancientCallComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AncientCallBossAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AncientCallBossBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(AncientCallInfoUpdateNtf msg)
    {
      this.m_ancientCallComponent.DeSerialize(msg);
    }

    public AncientCall GetAncientCallInfo()
    {
      return this.m_ancientCallComponent.GetAncientCallInfo();
    }

    public AncientCallPeriod GetCurrentAncientCallPeriod()
    {
      return this.m_ancientCallComponent.GetCurrentAncientCallPeriod();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallPeriodInfo GetRecentlyAncientCallPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss GetAncientCallBossByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossHistory GetAncientCallBossHistoryByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossAllMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossCompletedMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossProcessingMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAncientCallBossCompleteMissionList(
      ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAncientCallCurrentPeriodExist()
    {
      return this.m_ancientCallComponent.IsCurrentPeriodExist();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAncientCallBossIdList()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAncientCallBossOpen(int bossId)
    {
      return this.m_ancientCallComponent.GetCurrentBossId() == bossId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallBossOpenTime(int selectBossId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackAncientCallBoss(int levelId)
    {
      return this.m_ancientCallComponent.CanAttackAncientCallBoss(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentBossStartTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentPeriodStartTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetAncientCallCurrentPeriodEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnAncientCallBossAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.AncientCallBossBattleFinishedAckCallback EventOnAncientCallBossBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAnikiGymLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAnikiGymLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSAnikiGymNtf msg)
    {
      this.m_anikiGymComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AnikiGymLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AnikiGymLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAnikiGymOpened(int anikiGymId)
    {
      return this.m_anikiGymComponent.IsAnikiGymOpened(anikiGymId);
    }

    public bool IsAnikiLevelOpened(int levelId)
    {
      return this.m_anikiGymComponent.IsAnikiLevelOpened(levelId);
    }

    public int CanAttackAnikiLevel(int levelId)
    {
      return this.m_anikiGymComponent.CanAttackAnikiGymLevel(levelId, false);
    }

    public int GetAnikiTicketCount()
    {
      return this.m_anikiGymComponent.GetCurrentTicketNums();
    }

    public bool IsAnikiLevelFinished(int levelId)
    {
      return this.m_anikiGymComponent.IsLevelFinished(levelId);
    }

    public int GetAnikiGymMaxFinishedLevelId(int chapterId)
    {
      return this.m_anikiGymComponent.GetMaxFinishedLevelId(chapterId);
    }

    public int GetAnikiDailyRewardRestCount()
    {
      return this.m_anikiGymComponent.GetDailyChallengNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAnikiDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAnikiLevelBlessing(int levelId)
    {
      return this.m_anikiGymComponent.IsBlessing(levelId);
    }

    public event Action<int> EventOnAnikiGymLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnAnikiGymLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaPlayerInfoGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaFlushOpponentsReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentViewReq(int opponentIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentAttackReq(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaRevengeOpponentGetReq(ulong arenaBattleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentRevengeReq(ulong arenaBattleReportInstanceId, bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaOpponentAttackFightingReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleFinishedReq(ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaPlayerSetDefensiveTeamReq(
      int arenaDefenderRuleId,
      int battleId,
      List<ArenaPlayerDefensiveHero> defensiveHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaVictoryPointsRewardGainReq(int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleReportBaiscDataGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleReportPlayBackDataGetReq(ulong arenaBattleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaTopRankPlayersGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendArenaBattleReconnectReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSArenaNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaPlayerInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaFlushOpponentsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentViewAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaRevengeOpponentGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentRevengeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaOpponentAttackFightingAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaPlayerSetDefensiveTeamAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaVictoryPointsRewardGainAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReportBasicDataGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReportPlayBackDataGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaTopRankPlayersGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReconnectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaBattleReportNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaFlushOpponentsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsEmptyArenaPlayerInfo()
    {
      return this.m_arenaComponent.IsEmptyArenaPlayerInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedGetArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsNeedFlushArenaOpponents()
    {
      return this.m_arenaComponent.CanFlushOpponents() == 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaLevelId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerArenaVictoryPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaThisWeekVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaThisWeekTotalBattleNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaAutoBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaTicketCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaHonour()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanGetArenaVictoryPointsReward(int id)
    {
      return this.m_arenaComponent.CanGetVictoryPointsReward(id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetArenaVictoryPointsRewardStatus(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetArenaThisWeekDefendBattleIds()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackArenaOpponent()
    {
      return this.m_arenaComponent.CanAttackOpponentByClient();
    }

    public int CanRevengeArenaOpponent(ulong battleReportInstanceId)
    {
      return this.m_arenaComponent.CanRevengeOpponent(battleReportInstanceId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaOpponent> GetArenaOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetArenaOpponentNextFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo GetArenaOpponentDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveTeam GetArenaPlayerDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsArenaInSettleTime()
    {
      return this.m_arenaComponent.IsInSettleCacheTime();
    }

    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      return this.m_arenaComponent.GetAllArenaBattleReports();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetArenaPointChangeValue(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetPlayerArenaRank()
    {
      return this.m_playerArenaRank;
    }

    public List<ProArenaTopRankPlayer> GetArenaTopRankPlayers()
    {
      return this.m_arenaTopRankPlayers;
    }

    public int CanReconnectArenaBattle()
    {
      return this.m_arenaComponent.CanReconnectArenaBattle();
    }

    public event Action<int> EventOnArenaPlayerInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaFlushOpponentsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<ProBattleHero>, int> EventOnArenaOpponentViewAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaOpponentAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProArenaOpponent, List<ProBattleHero>, int> EventOnArenaRevengeOpponentGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaOpponentRevengeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaOpponentAttackFightingAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward, bool> EventOnArenaBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaPlayerSetDefensiveTeamAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnArenaVictoryPointsRewardGainAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaBattleReportBasicDataGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaBattleReportPlayBackDataGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaTopRankPlayersGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaBattleReconnectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnArenaBattleReportNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnArenaFlushOpponentsNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool SendHeroMaxLevelUseExpItemReq()
    {
      return this.SendMessage((object) new HeroMaxLevelUseExpItemReq());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenHeroSkinSelfSelectedBoxReq(List<int> indexes, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenSelfSelectedBoxReq(
      int index,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUseBagItemReq(GoodsType goodsType, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSellBagItemReq(ulong instanceId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLockAndUnLockEquipmentReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentEnhanceReq(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentStarLevelUpReq(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBagItemDecomposeReq(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBagItemComposeReq(int itemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentEnchantReq(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentEnchantSaveReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroMaxLevelUseExpItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSBagNtf msg)
    {
      this.m_bagComponent.DeSerialize(msg);
    }

    private void OnMessage(DSBagExtraNtf msg)
    {
      this.m_bagComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UseBagItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(OpenHeroSkinSelfSelectedBoxAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(OpenSelfSelectedBoxAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SellBagItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentLockAndUnLockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentEnhanceAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentStarLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BagItemDecomposeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BagItemComposeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentEnchantAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentEnchantSaveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanDecomposeBagItems(List<ProGoods> goods)
    {
      return this.m_bagComponent.CanDecomposeBagItems(goods);
    }

    public int CanEnchantEquipment(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      return this.m_bagComponent.CanEnchantEquipment(equipmentInstanceId, enchantStoneInstanceId);
    }

    public List<BagItemBase> GetBagItems()
    {
      return this.m_bagComponent.GetAllBagItems();
    }

    public int GetBagSize()
    {
      return this.m_bagComponent.GetBagSize();
    }

    public BagItemBase GetBagItem(GoodsType goodsType, int id, ulong instanceId)
    {
      return this.m_bagComponent.FindBagItem(goodsType, id, instanceId);
    }

    public BagItemBase GetBagItemByType(GoodsType goodsType, int id)
    {
      return this.m_bagComponent.FindBagItemByType(goodsType, id);
    }

    public bool IsExistBagItemIgnoreInstanceDifference(GoodsType goodsType, int id)
    {
      return this.m_bagComponent.IsExistBagItemIgnoreInstanceDifference(goodsType, id);
    }

    public BagItemBase GetBagItemByInstanceId(ulong instanceId)
    {
      return this.m_bagComponent.FindBagItemByInstanceId(instanceId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCount(GoodsType goodsType, int id, ulong instanceId)
    {
      BagItemBase bagItemBase;
      throw bagItemBase;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCountByType(GoodsType goodsType, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetBagItemCountByTypeAndID(GoodsType goodsType, int goodsID)
    {
      return this.m_bagComponent.GetBagItemNums(goodsType, goodsID);
    }

    public List<ulong> GetInstanceBagItemInstanceIdsByContentId(int contentId)
    {
      return this.m_bagComponent.GetInstanceBagItemInstanceIdsByContentId(contentId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagItemCountByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanUseEnergyMedicine(int itemId, int useCount)
    {
      return this.m_bagComponent.CanUseEnergyMedicine(itemId, useCount);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentExp(List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CalculateEnhanceEquipmentGold(int exp)
    {
      return this.m_bagComponent.CalculateEnhanceEquipmentGold(exp);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateEquipmentNextLevelExp(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CaculateEquipmentNextLevelExp(int contentID, int Level)
    {
      return this.m_bagComponent.CaculateEquipmentNextLevelExp(contentID, Level);
    }

    public int CanEnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      return this.m_bagComponent.CanEnhanceEquipment(instanceId, materialIds);
    }

    public int CanLevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      return this.m_bagComponent.CanLevelUpEquipmentStar(instanceId, materialId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelTicketsMaxInMission(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CalcEquipmentDecomposeReturnGold(EquipmentBagItem equipment)
    {
      return this.m_bagComponent.CalculateDecomposeEquipmentBackGold(equipment);
    }

    public int GetEquipmentResonanceNums(ulong equipmentInstanceId)
    {
      return this.m_bagComponent.GetEquipmentResonanceNums(equipmentInstanceId);
    }

    public int GetEquipmentResonanceNumsByHeroAndEquipment(
      ulong equipmentInstanceId,
      Hero hero,
      List<EquipmentBagItem> equipmentList)
    {
      return this.m_bagComponent.GetEquipmentResonanceNumsByHeroAndEquipment(equipmentInstanceId, hero, equipmentList);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEquipmentResonanceNumsByHeroId(ulong equipmentInstanceId, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItemByType(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsShowEnchantCancelConfirmPanel()
    {
      return this.ShowEnchantCancelConfirmPanel;
    }

    public void SetEnchantCancelConfirmFlag(bool isShow)
    {
      this.ShowEnchantCancelConfirmPanel = isShow;
    }

    public bool IsShowEnchantSaveConfirmPanel()
    {
      return this.ShowEnchantSaveConfirmPanel;
    }

    public void SetEnchantSaveConfirmFlag(bool isShow)
    {
      this.ShowEnchantSaveConfirmPanel = isShow;
    }

    public bool HasOwn(GoodsType goodsType, int id)
    {
      return this.m_bagComponent.HasOwn(goodsType, id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetLevelUpEquipmentStarItems(
      int star,
      ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool NeedUpdatePlayerResource(GoodsType goodsType, int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBagItemChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnUseBagItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroMaxLevelUseExpItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSellBagItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentLockAndUnLockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentEnhanceAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentStarLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnBagItemDecomposeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, List<CommonBattleProperty>> EventOnEquipmentEnchantAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, EquipmentBagItem> EventOnEquipmentEnchantSaveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnOpenHeroSkinSelfSelectedBoxAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnOpenSelfSelectedBoxAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnBagItemComposeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleCancelReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleTeamSetReq(int battleId, int gamePlayTeamTypeId, List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleArmyRandomSeedGetReq(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSBattleNtf msg)
    {
      this.m_battleComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleCancelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleTeamSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleArmyRandomSeedGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsGameFunctionOpenByMonthCard(GameFunctionType gameFuncTypeId)
    {
      return this.m_battleComponent.IsGameFunctionOpenByMonthCard(gameFuncTypeId);
    }

    public void SetBattleBase(BattleBase battleBase)
    {
      this.m_battleComponent.SetBattleBase(battleBase);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentBattle(ConfigDataBattleInfo battleInfo, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentArenaBattle(
      ConfigDataArenaBattleInfo battleInfo,
      ConfigDataArenaDefendRuleInfo arenaDefendRule,
      int defenderPlayerLevel,
      List<BattleHero> defenderHeros,
      List<TrainingTech> defenderTechs,
      bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCurrentArenaBattleReport(ArenaBattleReport battleReport)
    {
      this.m_currentBattle.ArenaBattleReport = battleReport;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPVPBattle(ConfigDataPVPBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentRealTimePVPBattle(ConfigDataRealTimePVPBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCurrentRealTimePVPBattleReport(RealTimePVPBattleReport battleReport)
    {
      this.m_currentBattle.RealTimePVPBattleReport = battleReport;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeakArenaBattle(ConfigDataPeakArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeakArenaBattleReport(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetCurrentPeakArenaMatchGroupId(int matchGroupId)
    {
      this.m_currentBattle.PeakArenaMatchGroupId = matchGroupId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentBattleFirstBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBattleTeam(BattleType battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindGamePlayTeamTypeId(BattleType battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero CreateBattleHeroFromMyHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero CreateDefaultBattleHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveBeforeRewardStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveBattleHeroIds(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public RewardPlayerStatus GetBeforeRewardPlayerStatus()
    {
      return this.m_beforeRewardPlayerStatus;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RewardHeroStatus GetBeforeRewardHeroStatus(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ProcessingBattle GetProcessingBattle()
    {
      return this.m_battleComponent.GetProcessingBattle();
    }

    public ArenaBattleStatus GetArenaBattleStatus()
    {
      return this.m_battleComponent.GetArenaBattleStatus();
    }

    public int GetArenaBattleId()
    {
      return this.m_battleComponent.GetArenaBattleId();
    }

    public void SetNeedRebuildBattle(bool need)
    {
      this.m_isNeedRebuildBattle = need;
    }

    public bool IsNeedRebuildBattle()
    {
      return this.m_isNeedRebuildBattle;
    }

    public List<int> GetGainBattleTreasrueIds()
    {
      return this.m_battleComponent.GetGotBattleTreasureIds();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGainBattleTreasureCount(ConfigDataBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ComputeBattlePower(Hero hero)
    {
      return this.m_battleComponent.ComputeBattlePower(hero);
    }

    public int ComputeBattlePower(BattleHero hero, List<TrainingTech> techs)
    {
      return this.m_battleComponent.ComputeBattlePower(hero, techs);
    }

    public void SetBattleRandomSeed(int seed)
    {
      this.m_battleComponent.SetBattleRandomSeed(seed);
    }

    public int GetBattleRandomSeed()
    {
      return this.m_battleComponent.BattleRandomSeed;
    }

    public void SetArenaBattleRandomSeed(int seed)
    {
      this.m_battleComponent.SetArenaBattleRandomSeed(seed);
    }

    public int GetArenaBattleRandomSeed()
    {
      return this.m_battleComponent.ArenaBattleRandomSeed;
    }

    public void SetBattleArmyRandomSeed(int seed)
    {
      this.m_battleComponent.SetBattleArmyRandomSeed(seed);
    }

    public int GetBattleArmyRandomSeed()
    {
      return this.m_battleComponent.ArmyRandomSeed;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleFinishedError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetDeadEnemyBattleActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGeneralInviteInfos(List<GeneralInviteInfo> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRemainBattleRegretCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetRemainBattleRegretCount()
    {
      return this.m_battleRegretCount;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UseBattleRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentBattleFailEnergyCost()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetChapterIdFromLevelId(GameFunctionType gameFunctionType, int levelId)
    {
      return this.m_battleComponent.GetChapterIdFromLevelId(gameFunctionType, levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareInviteTimeout(GeneralInviteInfo i1, GeneralInviteInfo i2)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnBattleCancelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleTeamSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleArmyRandomSeedGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamBattleRoomCreateReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomPlayerStatusChangeReq(PlayerBattleStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomQuitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroSetupReq(int heroId, int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroSwapReq(int fromPosition, int toPosition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroPutdownReq(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroProtectReq(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomHeroBanReq(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomEndCurrentBPTurnReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomBattleCommandExecuteReq(List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomPlayerActionBeginReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomBattleLogReq(string log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleRoomBPStageCommandExecuteReq(BPStageCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomCreateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerStatusChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroSetupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroSwapAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroPutdownAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroProtectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroBanAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomEndCurrentBPTurnAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBattleCommandExecuteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerActionBeginAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBPStageCommandExecuteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomTeamBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomGuildMassiveCombatBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPVPBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomRealTimePVPBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPeakArenaBattleJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerStatusChangedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomHeroSetupNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomTeamBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPVPBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomRealTimePVPBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPeakArenaBattleStartNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBattleCommandExecuteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomBPStageCommandExecuteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomTeamBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleGuildMassiveCombatBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPVPBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomRealTimePVPBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPeakArenaBattleFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerBattleInfoInitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomPlayerBPStageInfoInitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleRoomDataChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsInBattleRoom()
    {
      return this.m_battleComponent.IsInBattleRoom();
    }

    public bool IsInBattleLiveRoom()
    {
      return this.m_peakArenaComponent.IsOnLiveRoom();
    }

    public ulong GetBattleLiveRoomId()
    {
      return this.m_peakArenaComponent.GetLiveRoomId();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckCanJoinBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleRoom GetBattleRoom()
    {
      return this.m_battleComponent.GetBattleRoom();
    }

    public void SetQuitBattleRoom()
    {
      this.m_battleComponent.QuitBattleRoom();
    }

    public event Action<int> EventOnBattleRoomCreateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomPlayerStatusChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroSetupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroSwapAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroPutdownAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroProtectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomHeroBanAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomEndCurrentBPTurnAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomBattleCommandExecuteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomPlayerActionBeginAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomBPStageCommandExecuteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomTeamBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomGuildMassiveCombatBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPVPBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomRealTimePVPBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPeakArenaBattleJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomPlayerStatusChangedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleRoomQuitReason> EventOnBattleRoomQuitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<int>> EventOnBattleRoomHeroSetupNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomTeamBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPVPBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomRealTimePVPBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPeakArenaBattleStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattleRoomBattleCommandExecuteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProBPStageCommand> EventOnBattleRoomBPStageCommandExecuteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomTeamBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomGuildMassiveCombatBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPVPBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomRealTimePVPBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPeakArenaBattleFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPlayerBattleInfoInitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomPlayerBPStageInfoInitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleRoomDataChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatTextMessage(ChatChannel channel, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatExpressionMessage(ChatChannel channel, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatVoiceMessage(
      ChatChannel channel,
      byte[] voiceBytes,
      int length,
      int frequency,
      int samples,
      string translateText,
      string destId = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetVoiceContentMessage(ChatChannel channel, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendWorldEnterNewRoomMessage(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetChatContactUserSummaryInfoMessage(List<string> playerIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatHeroEquipmentMessage(
      ChatChannel channel,
      string content,
      List<ulong> instanceIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChatBattleReportMessage(
      ChatChannel channel,
      string content,
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatContactUserSummaryInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSChatNtf msg)
    {
      this.m_chatComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatMessageNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatMessageSendAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatBannedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatVoiceContentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatWorldChannelRoomEnterAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGuildHistoryMessageNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetChatGroupName(string groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPrivateChatPlayeName(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ChatComponent GetChatComponent()
    {
      return this.m_chatComponent;
    }

    public int CanSendChatMessage(int channelId)
    {
      return this.m_chatComponent.CanSendChatMessage(channelId);
    }

    public int CanSendDanmaku(DateTime lastDanmakuSendTime)
    {
      return this.m_chatComponent.CanSendDanmaku(lastDanmakuSendTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatComponent.ChatMessageClient> GetChatMessageList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetUnReadChatMsgCount4PointPlayerOrGroup(ChatChannel channel, string id)
    {
      return this.m_chatComponent.GetUnReadChatMsgCount4PointPlayerOrGroup(channel, id);
    }

    public string GetGroupChatId()
    {
      return this.m_chatComponent.GetGroupChatTarget();
    }

    public void SetGroupChatTarget(string groupId)
    {
      this.m_chatComponent.SetGroupChatTarget(groupId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGroupChatTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetPrivateChatTarget(string gameUserId)
    {
      this.m_chatComponent.SetPrivateChatTarget(gameUserId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanPrivateChatTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetPrivateChatPlayerId()
    {
      return this.m_chatComponent.m_currPrivateChatPlayerGameUserId;
    }

    public List<string> GetRecentPrivateChatIdList()
    {
      return this.m_chatComponent.m_recentPrivateChatPlayerIdList;
    }

    public int GetNewChatMsgCount()
    {
      return this.m_chatComponent.GetAllUnReadChatMsgCount();
    }

    public int GetGroupUnreadChatMsgCount()
    {
      return this.m_chatComponent.GetGroupUnreadChatMsgCount();
    }

    public int GetAssignGroupUnreadChatMsgCount(string groupID)
    {
      return this.m_chatComponent.GetAssignGroupUnreadChatMsgCount(groupID);
    }

    public int GetAssignPrivateUnreadChatMsgCount(string userID)
    {
      return this.m_chatComponent.GetAssignPrivateUnreadChatMsgCount(userID);
    }

    public int GetPrivateUnreadChatMsgCount()
    {
      return this.m_chatComponent.GetPrivateUnreadChatMsgCount();
    }

    public int GetGuildUnreadChatMsgCount()
    {
      return this.m_chatComponent.GetGuildUnreadChatMsgCount();
    }

    public int GetRoomIndex()
    {
      return this.m_chatComponent.RoomIndex;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadChat(ChatChannel type)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ReadChatBeforeDate(ChatChannel type, DateTime date)
    {
      this.m_chatComponent.ReadChatBeforeDate(type, date);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshGuildChatLastReadTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime GetGuildChatLastReadTime()
    {
      return this.m_chatComponent.m_guildChatLastReadTime;
    }

    public KeyValuePair<List<string>, List<string>> GetRecentChatTargetList(
      ChatChannel channel)
    {
      return this.m_chatComponent.GetRecentChatTargetList(channel);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGuildChatContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public NoticeText GetWaitingNoticeStringListFirstNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetWaitingNoticeStringListCount()
    {
      return this.m_waitingNoticeStringList.Count;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNoticeToWaitingNoticeStringList(NoticeText noticeText)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetIsShowingNoticeFlag(bool value)
    {
      this.m_isShowingNotice = value;
    }

    public bool GetIsShowingNoticeFlag()
    {
      return this.m_isShowingNotice;
    }

    public event Action<ChatMessage> EventOnChatMessageNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChatContactUserSummaryInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChatMessageAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ChatVoiceMessage> EventOnChatGetVoiceContentAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnChatEnterRoomAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChangeOwnerReq(string chatGroupId, string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChangeNameReq(string chatGroupId, string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCreateChatGroupReq(string groupName, List<string> targetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetChatGroupMemberReq(string chatGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetAllChatGroupReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendInviteToChatGroupReq(string chatGroupId, List<string> targetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLeaveChatGroupReq(string chatGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendKickUserReq(string chatGroupId, string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetUserSummaryReq(
      List<string> targetUserIds,
      Action<int, List<UserSummary>> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupChangeOwnerAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupChangeNameAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupCreateChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupGetChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupGetChatGroupMemberAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupInviteToChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupLeaveChatGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupKickUserAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendGetUserSummaryAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ChatGroupUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetGuildMemberSummary(
      List<string> targetUserIds,
      Action<int, List<UserSummary>> action)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, ProChatGroupInfo, ProChatUserInfo> EventOnCreateChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<ProChatGroupCompactInfo>> EventOnGetAllChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProChatGroupInfo> EventOnGetChatGroupMemberAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChatGroupChangeOwnerAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChatGroupChangeNameAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ProChatGroupInfo, ProChatUserInfo> EventOnInviteToChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLeaveChatGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnKickUserFromGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProChatGroupInfo> EventOnChatGroupUpdateNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerLevelAttackReq(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerLevelBattleFinishedReq(int floorId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClimbTowerRaidReq()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSClimbTowerNtf msg)
    {
      this.m_climbTowerComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ClimbTowerRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackClimbTowerFloor(int floorId)
    {
      return this.m_climbTowerComponent.CanAttackClimbTowerLevel(floorId);
    }

    public DateTime GetClimbTowerNextFlushTime()
    {
      return this.m_climbTowerComponent.GetNextFlushTime();
    }

    public int GetClimbTowerMaxFloorId()
    {
      return this.m_climbTowerComponent.GetClimbTowerFloorMax();
    }

    public int GetClimbTowerFinishedFloorId()
    {
      return this.m_climbTowerComponent.GetFinishedFloorId();
    }

    public int GetClimbTowerRaidMaxFloorId()
    {
      return this.m_climbTowerComponent.GetAutoRaidClimbTowerMax();
    }

    public int GetClimbTowerHistoryMaxFloorId()
    {
      return this.m_climbTowerComponent.GetHistoryMaxFloorId();
    }

    public int ClimbTowerTryRaid(out int reachFloorId, out int costEnergy)
    {
      return this.m_climbTowerComponent.TryRaid(out reachFloorId, out costEnergy);
    }

    public GlobalClimbTowerFloor GetClimbTowerFloor(int floorId)
    {
      return this.m_climbTowerComponent.GetFloor(floorId);
    }

    public bool IsNeedFlush()
    {
      return this.m_climbTowerComponent.IsNeedFlush();
    }

    public event Action<int> EventOnClimbTowerGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnClimbTowerLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnClimbTowerLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnClimbTowerRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionWayPointMoveReq(int collectionWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionScenarioHandleReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionLevelAttackReq(GameFunctionType levelType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionLevelFinishReq(
      GameFunctionType levelType,
      int levelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionExchangeReq(ulong activityInstanceId, int exchangeItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionEventHandleReq(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionEventAttackReq(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCollectionEventBattleFinishedReq(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSCollectionNtf msg)
    {
      this.m_collectionActivityComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionWayPointMoveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionScenarioHandleAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionLevelFinishAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionExchangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AddCollectionEventNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCollectionEventAdd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DeleteCollectionEventNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCollectionEventDelete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionEventHandleAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionEventAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CollectionEventBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsCollectionActivityOpened(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.IsCollectionActivityOpened(collectionActivityId);
    }

    public bool IsCollectionActivityDisplay(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.IsCollectionActivityDisplay(collectionActivityId);
    }

    public bool IsCollectionActivityExchangeTime(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.IsCollectionActivityExchangeTime(collectionActivityId);
    }

    public void GetCollectionActivityOpenTime(
      int collectionActivityId,
      out DateTime startTime,
      out DateTime endTime)
    {
      this.m_collectionActivityComponent.GetCollectionActivityOpenTime(collectionActivityId, out startTime, out endTime);
    }

    public DateTime GetCollectionActivityExchangeEndTime(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.GetCollectionActivityExchangeEndTime(collectionActivityId);
    }

    public int CanMoveToCollectionActivityWaypoint(int collectionWaypointId)
    {
      return this.m_collectionActivityComponent.CanMoveToWayPoint(collectionWaypointId);
    }

    public void GetCollectionActivityWaypointState(
      int collectionWaypointId,
      out CollectionActivityWaypointStateType lockState,
      out string graphicState)
    {
      this.m_collectionActivityComponent.GetWaypointState(collectionWaypointId, out lockState, out graphicState);
    }

    public int GetCollectionActivityWaypointRedPointCount(int collectionWaypointId)
    {
      return this.m_collectionActivityComponent.GetWaypointRedPointCount(collectionWaypointId);
    }

    public string GetCollectionActivityPlayerGraphicResource(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.GetPlayerGraphicResource(collectionActivityId);
    }

    public int CanHandleCollectionActivityScenario(int levelId)
    {
      return this.m_collectionActivityComponent.CanHandleScenario(levelId);
    }

    public int CanAttackCollectionActivityLevel(GameFunctionType typeId, int levelId)
    {
      return this.m_collectionActivityComponent.CanAttackLevel(typeId, levelId, false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCollectionActivityEvent(int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHandleCollectionActivityEvent(int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetNextCollectionActivityScenarioLevelInfo(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo GetCollectionActivityCurrentWaypointInfo(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsCollectionActivityScenarioLevelFinished(int levelId)
    {
      return this.m_collectionActivityComponent.IsScenarioLevelFinished(levelId);
    }

    public bool IsCollectionActivityChallengeLevelFinished(int levelId)
    {
      return this.m_collectionActivityComponent.IsChallengeLevelFinished(levelId);
    }

    public bool IsCollectionActivityLootLevelFinished(int levelId)
    {
      return this.m_collectionActivityComponent.IsLootLevelFinished(levelId);
    }

    public int GetCollectionActivityMaxFinishedLootLevelId(int collectionWaypointId)
    {
      return this.m_collectionActivityComponent.GetMaxFinishedLootLevelId(collectionWaypointId);
    }

    public bool IsCollectionActivityChallengePreLevelFinished(int levelId)
    {
      return this.m_collectionActivityComponent.IsChallengePreLevelFinished(levelId);
    }

    public bool IsCollectionActivityLootPreLevelFinished(int levelId)
    {
      return this.m_collectionActivityComponent.IsLootPreLevelFinished(levelId);
    }

    public int GetCollectionActivityScenarioLevelUnlockDay(int levelId)
    {
      return this.m_collectionActivityComponent.GetScenarioLevelUnlockDay(levelId);
    }

    public int GetCollectionActivityChallengeLevelUnlockDay(int levelId)
    {
      return this.m_collectionActivityComponent.GetChallengeLevelUnlockDay(levelId);
    }

    public bool IsCollectionActivityChallengeLevelPlayerLevelVaild(
      int levelId,
      out int playerLevelRequired)
    {
      return this.m_collectionActivityComponent.IsChallengeLevelPlayerLevelVaild(levelId, out playerLevelRequired);
    }

    public int GetCollectionActivityLootLevelUnlockDay(int levelId)
    {
      return this.m_collectionActivityComponent.GetLootLevelUnlockDay(levelId);
    }

    public bool IsCollectionActivityLootLevelPlayerLevelVaild(
      int levelId,
      out int playerLevelRequired)
    {
      return this.m_collectionActivityComponent.IsLootLevelPlayerLevelVaild(levelId, out playerLevelRequired);
    }

    public bool IsCollectionActivityLootLevelUnlock(int levelId)
    {
      return this.m_collectionActivityComponent.IsLootLevelUnlock(levelId);
    }

    public int CanCollectionActivityExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      return this.m_collectionActivityComponent.CanExchangeItem(activityInstanceId, exchangeItemID);
    }

    public ulong FindActivityInstanceIdByCollectionActivityId(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.FindActivityInstanceIdByCollectionActivityId(collectionActivityId);
    }

    public bool IsCollectionActivityScoreRewardGot(
      int collectionActivityId,
      int collectionActvityScoreRewardGroupId)
    {
      return this.m_collectionActivityComponent.IsRewardGot(collectionActivityId, collectionActvityScoreRewardGroupId);
    }

    public int GetCollectionActivityScore(int collectionActivityId)
    {
      return this.m_collectionActivityComponent.GetScore(collectionActivityId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeActiveNextCollectionActivityScenarioLevelInfo(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetActiveNextCollectionActivityScenarioLevelInfo(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateActiveCollectionActivityWaypointEvents(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> GetActiveCollectionActivityWaypointEvents(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo GetActiveCollectionActivityEventInfo(
      int collectionWaypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnCollectionWayPointMoveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnCollectionScenarioHandleAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCollectionLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnCollectionLevelFinishAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnCollectionExchangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCollectionEventAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCollectionEventDelete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnCollectionEventHandleAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCollectionEventAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnCollectionEventBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetHeroCommentReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCommentHeroReq(int heroId, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPraiseHeroCommentEntryReq(int heroId, ulong entryInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendViewCommenterHeroReq(int heroId, string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroCommentGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroCommentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroCommentEntryPraiseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CommenterHeroViewAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroCommentGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroCommentSendAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroCommentPraisedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Hero> EventOnCommenterHeroViewAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroComment GetHeroComments(int heroId)
    {
      return this.m_commentComponent.GetHeroComment(heroId);
    }

    public int CanPraiseHeroCommentEntry(int heroId, ulong entryInstanceId)
    {
      return this.m_commentComponent.CanPraiseHeroCommentEntry(heroId, entryInstanceId);
    }

    private void OnMessage(DSCooperateBattleNtf ntf)
    {
      this.m_cooperateBattleComponent.DeSerialize(ntf);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCooperateBattleOpened(int cooperateBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsCooperateBattleDisplayable(int cooperateBattleId)
    {
      return this.m_cooperateBattleComponent.CheckCooperateBattleDisplayable(cooperateBattleId);
    }

    public int GetCooperateBattleDailyRewardRestCount(int cooperateBattleId)
    {
      return this.m_cooperateBattleComponent.GetDailyChallengeNums(cooperateBattleId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCooperateBattleDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo GetFirstOpenedCooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsCooperateBattleLevelOpened(int levelId)
    {
      return this.m_cooperateBattleComponent.IsLevelUnlocked(levelId);
    }

    public int CanAttackCooperateBattleLevel(int levelId)
    {
      return this.m_cooperateBattleComponent.CanAttackCooperateBattleLevel(levelId);
    }

    public bool IsCooperateBattleLevelFinished(int levelId)
    {
      return this.m_cooperateBattleComponent.IsLevelFinished(levelId);
    }

    public int GetCooperateBattleMaxFinishedLevelId(int cooperateBattleId)
    {
      return this.m_cooperateBattleComponent.GetMaxFinishedLevelId(cooperateBattleId);
    }

    public int GetCooperateBattleMaxOpenedLevelId(int cooperateBattleId)
    {
      return this.m_cooperateBattleComponent.GetMaxUnlockedLevelId(cooperateBattleId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelDanmakuGetReq(int gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelDanmakuPostReq(
      int gameFunctionTypeId,
      int locationId,
      List<PostDanmakuEntry> entries)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelDanmakuGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelDanmakuPostAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public LevelDanmaku GetLevelDanmaku()
    {
      return this.m_danmakuComponent.GetLevelDanmaku();
    }

    public List<PostDanmakuEntry> GetPostedLevelDanmaku()
    {
      return this.m_danmakuComponent.GetPostedLevelDanmaku();
    }

    public void PostLevelDanmaku(PostDanmakuEntry entry)
    {
      this.m_danmakuComponent.PostLevelDanmaku(entry);
    }

    public int CanGetLevelDanmaku(int gameFunctionTypeId, int locationId)
    {
      return this.m_danmakuComponent.CanGetLevelDanmaku(gameFunctionTypeId, locationId);
    }

    public int CanPostLevelDanmaku(
      int gameFunctionTypeId,
      int locationId,
      List<PostDanmakuEntry> entries)
    {
      return this.m_danmakuComponent.CanPostLevelDanmaku(gameFunctionTypeId, locationId, entries);
    }

    public void ClearLevelDanmaku()
    {
      this.m_danmakuComponent.ClearLevelDanmaku();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanUseDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGetLevelDanmakuAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSendLevelDanmakuAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEternalShrineLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEternalShrineLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSEternalShrineNtf msg)
    {
      this.m_eternalShrineComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EternalShrineLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EternalShrineLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEternalShrineOpened(int eternalShrineId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEternalShrineLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackEternalShrineLevel(int levelId)
    {
      return this.m_eternalShrineComponent.CanAttackEternalShrineLevel(levelId, false);
    }

    public bool IsEternalShrineLevelFinished(int levelId)
    {
      return this.m_eternalShrineComponent.IsLevelFinished(levelId);
    }

    public int GetEternalShrineMaxFinishedLevelId(int chapterId)
    {
      return this.m_eternalShrineComponent.GetMaxFinishedLevelId(chapterId);
    }

    public int GetEternalShrineDailyChallengeRestCount()
    {
      return this.m_eternalShrineComponent.GetDailyChallengNums();
    }

    public int GetEternalShrineDailyChallengeCountMax()
    {
      return this.m_eternalShrineComponent.GetDailyChallengeMaxNums();
    }

    public bool IsEternalShrineLevelBlessing(int levelId)
    {
      return this.m_eternalShrineComponent.IsBlessing(levelId);
    }

    public event Action<int> EventOnEternalShrineLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnEternalShrineLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFixedStoreBuyStoreItemReq(int storeId, int goodsId, int selectedIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSFixedStoreNtf msg)
    {
      this.m_fixedStoreComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FixedStoreBuyStoreItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkinTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryEssence()
    {
      // ISSUE: unable to decompile the method.
    }

    public FixedStore FindFixedStoreByID(int storeID)
    {
      return this.m_fixedStoreComponent.FindStoreById(storeID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<FixedStoreItem> GetFixedStoreItemList(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanBuyFixedStoreGoods(int storeId, int goodsId, int selfChooseItemIndex = -1)
    {
      return this.m_fixedStoreComponent.CanBuyGoods(storeId, goodsId, selfChooseItemIndex);
    }

    public bool IsOnDiscount(ConfigDataFixedStoreItemInfo itemInfo)
    {
      return this.m_fixedStoreComponent.IsOnDiscountPeriod(itemInfo);
    }

    public event Action<int> EventOnFixedStoreBuyStoreItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroicMomentBattleReportRemoveReq(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroicMomentBattleReportAddReq(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroRandomActionSetReq(bool isActionRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLikesSendReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBusinessCardDescUpdateReq(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBusinessCardHeroSetUpdateReq(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBusinessCardSelectReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetSocialRelationReq(FriendSocialRelationFlag Flag = FriendSocialRelationFlag.All)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendInivteReq(List<string> targetUserIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendAcceptReq(List<string> inviterUserIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendDeclineReq(List<string> inviterUserIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendDeleteReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPlayerBlockReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPlayerUnblockReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPlayerFindReq(int targetBornChannelId, string partialName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSuggestedUserReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeInviteReq(
      string targetUserId,
      int battleId,
      PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeAcceptReq(string targetUserId, PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeDeclineReq(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattlePracticeCancelReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendshipPointsSendReq(List<string> TargetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFriendshipPointsClaimReq(List<string> TargetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroicMomentBattleReportAddAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroicMomentBattleReportRemoveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroRandomActionSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LikesSendAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BusinessCardSelectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BusinessCardDescUpdateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BusinessCardHeroSetUpdateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMineBusinessCard(
      bool isDescUpdate,
      bool updateHero,
      bool updateHeroicMomentBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSFriendNtf ntf)
    {
      this.m_friendComponent.Deserialize(ntf);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInfoUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendSocialRelationUpdateNtf Ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(FriendInviteNtf msg)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteAcceptAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(FriendInviteAcceptNtf msg)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteDeclineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendInviteDeclineNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendDeleteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendBlockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendUnblockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendFindAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendSuggestedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendSummaryUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeAcceptAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeDeclineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeCancelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeInvitedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeFailNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattlePracticeDeclinedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendshipPointsSendAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FriendshipPointsClaimAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBattleReportInHeroMoment(ulong id)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ulong> GetHeroMomentBattleReportIdList()
    {
      return this.m_friendComponent.GetHeroicMomentBattleReports();
    }

    public int CanSendLikes(string targetUserId)
    {
      return this.m_friendComponent.CanSendLikes(targetUserId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSendFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGetFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasSentFriendShipPoints(string userID)
    {
      return this.m_friendComponent.HasSentFriendShipPoints(userID);
    }

    public bool HasReceivedFriendShipPoints(string userID)
    {
      return this.m_friendComponent.HasReceivedFriendShipPoints(userID);
    }

    public int GetSendFriendShipPointsCountToday()
    {
      return this.m_friendComponent.GetSendFriendShipPointsCount();
    }

    public int GetClaimedFriendShipPointsToday()
    {
      return this.m_friendComponent.GetClaimedFriendShipPointsCount();
    }

    public int CanUpdateBusinessCardDesc(string desc)
    {
      return this.m_friendComponent.CanUpdateBusinessCardDesc(desc);
    }

    public int CanUpdateBusinessCardHeroSets(List<BusinessCardHeroSet> heroSets)
    {
      return this.m_friendComponent.CanUpdateBusinessCardHeroSets(heroSets);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UserSummary> GetAcrossServerFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UserSummary> GetRecentChatPlayerList()
    {
      return this.m_friendComponent.RecentContactsChat;
    }

    public List<UserSummary> GetRecentTeamBattlePlayerList()
    {
      return this.m_friendComponent.RecentContactsTeamBattle;
    }

    public List<UserSummary> GetGuildPlayerList()
    {
      return this.m_friendComponent.GuildPlayers;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAcrossServerFriend(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UserSummary> GetBlackList()
    {
      return this.m_friendComponent.Blacklist;
    }

    public List<UserSummary> GetFriendInvitedList()
    {
      return this.m_friendComponent.Invited;
    }

    public List<UserSummary> GetFriendInviteList()
    {
      return this.m_friendComponent.Invite;
    }

    public List<UserSummary> GetRecommendFriendList()
    {
      return this.m_recommendFriendList;
    }

    public List<UserSummary> GetFindFriendList()
    {
      return this.m_findFriendList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SortUserSummaryList(List<UserSummary> Summaries)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFriend(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetFriendShipPoints()
    {
      return this.m_playerBasicInfoComponent.GetFriendshipPoints();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFriendRedMarkShow()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<PVPInviteInfo> GetPVPInviteInfos()
    {
      return this.m_friendComponent.PVPInviteInfos;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemovePVPInviteInfo(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsNeedGetFriendSocialRelation()
    {
      return this.m_needGetFriendSocialRelation;
    }

    public void SetNeedGetFriendSocialRelation(bool isNeed)
    {
      this.m_needGetFriendSocialRelation = isNeed;
    }

    public BusinessCard BusinessCard { get; set; }

    public event Action<int> EventOnBusinessCardRandomShowChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLikeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardDescUpdateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBusinessCardHeroSetUpdateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendGetSocialRelationAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, UserSummary> EventOnFriendInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFriendInviteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, UserSummary> EventOnFriendInviteAcceptAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnFriendInviteAcceptNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendInviteDeclineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnFriendInviteDeclineNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendDeleteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPlayerBlockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPlayerUnblockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendFindAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendSuggestedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnFriendSummaryUpdateNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeAcceptAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeDeclineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBattlePracticeCancelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PVPInviteInfo> EventOnBattlePracticeInvitedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PracticeMode> EventOnBattlePracticeFailNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattlePracticeDeclinedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFriendshipPointsSendAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnFriendshipPointsClaimAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFriendshipPointsReceivedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroMomentBattleReportAddAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroMomentBattleReportRemoveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPullOrderRewardReq(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGiftStoreCancelBuyReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGiftStoreItemBuyReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGiftStoreAppleSubscribeReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PullOrderRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSGiftStoreNtf msg)
    {
      this.m_giftStoreComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreCancelBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreBuyGoodsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreOperationalGoodsUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreItemBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GiftStoreAppleSubscribeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<OrderReward> GetAllOrderRewards()
    {
      return this.m_giftStoreComponent.GetAllOrderRewards();
    }

    public List<GiftStoreItem> GetGiftStoreItemList()
    {
      return this.m_giftStoreComponent.GetOfferedStoreItems();
    }

    public GiftStoreItem GetGiftStoreItem(int goodsId)
    {
      return this.m_giftStoreComponent.GetBoughtItem(goodsId);
    }

    public bool IsGiftStoreItemSellOut(GiftStoreItem item)
    {
      return this.m_giftStoreComponent.IsGiftStoreItemSellOut(item);
    }

    public int CanBuyGiftStoreGoods(int goodsId)
    {
      return this.m_giftStoreComponent.CanBuyGoods(goodsId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetMonthCardLeftTime(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardValid(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetCachedMonthCardState(int cardId, bool defaultState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCachedMonthCardState(int cardId, bool state)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetMonthCardPrefName(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnGiftStoreItemBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGiftStoreAppleSubscribeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnGiftStoreBuyGoodsNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGiftStoreOperationalGoodsUpdateNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGiftStoreItemCancelBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string, OrderReward> EventOnPullOrderRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGmCommandReq(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GenerateAddItemGmCommand(GoodsType goodsType, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GenerateRemoveItemGmCommand(GoodsType goodsType, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GmCommandAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGmCommandAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildCreateReq(
      string guildName,
      string hiringDeclaration,
      string announcement,
      bool autoJoin,
      int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinApplyReq(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinAdminConfirmReq(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinApplyRefuseReq(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildJoinPlayerConfirmReq(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildSearchReq(string searchText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildRandomListReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GuildQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildInvitePlayerListReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildHiringDeclarationSetReq(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinInvitationGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinInvitationRefuseReq(string guildID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAllGuildJoinInvitationRefuseReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildGetReq(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildBasicSetReq(bool autoJoin, int joinLevel, string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildAnnouncementSetReq(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinApplyGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildNameChangeReq(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildLogGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildJoinInviteReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildVicePresidentAppointReq(string userId, bool appoint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildKickOutReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildPresidentRelieveReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildPresidentAppointReq(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAllGuildJoinApplyRefuseReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildUpdateAnnouncementNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSGuildNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildCreateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyConfirmAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationConfirmAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildSearchAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildRandomListAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildInvitePlayerListAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildHiringDeclarationSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AllGuildJoinInvitationRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildBasicSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildAnnouncementSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildNameChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildLogGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildVicePresidentAppointAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildKickOutAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildPresidentRelieveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildPresidentAppointAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AllGuildJoinApplyRefuseAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinApplyAdminNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildJoinInvitationNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CheckGuildCreateCondition(string guildName, string hiringDeclaration, int joinLevel)
    {
      return this.m_guildComponent.CanCreateGuild(guildName, hiringDeclaration, joinLevel);
    }

    public Guild GetGuildInfo()
    {
      return this.m_guildComponent.m_guild;
    }

    public void RefreshGuildListJoinState(string guildID)
    {
      this.m_guildComponent.RefreshGuildListJoinState(guildID, true);
    }

    public int CanQuitGuild()
    {
      return this.m_guildComponent.CanQuitGuild();
    }

    public int CanKickOutGuild()
    {
      return this.m_guildComponent.CanKickOutGuild();
    }

    public int CheckGuildName(string name)
    {
      return this.m_guildComponent.CheckGuildName(name);
    }

    public int CanSetGuildHiringDeclaration(string hiringDeclaration)
    {
      return this.m_guildComponent.CanSetGuildHiringDeclaration(hiringDeclaration);
    }

    public int CanSetGuildAnnouncement(string announcement)
    {
      return this.m_guildComponent.CanSetGuildAnnouncement(announcement);
    }

    public int CheckGuildInvitePlayerList()
    {
      return this.m_guildComponent.CheckGuildInvitePlayerList();
    }

    public List<GuildJoinInvitation> GetGuildJoinInvitationList()
    {
      return this.m_guildComponent.m_guildJoinInvitationList;
    }

    public List<UserSummary> GetGuildJoinApplyList()
    {
      return this.m_guildComponent.m_guildJoinApplyList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveGuildJoinApplyListById(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CheckGuildSearch(string searchText)
    {
      return this.m_guildComponent.CheckGuildSearch(searchText);
    }

    public int CheckGuildRandomList()
    {
      return this.m_guildComponent.CheckGuildRandomList();
    }

    public List<GuildSearchInfo> GetGuildRecommendList()
    {
      return this.m_guildComponent.m_guildRecommendList;
    }

    public string GetGuildId()
    {
      return this.m_guildComponent.GetGuildId();
    }

    public string GetGuildLogContent(GuildLog log)
    {
      return this.m_guildComponent.GetGuildLogContent(log);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUserIdToGuildInviteList(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan CanJoinGuildCDTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasPlayerBeenInvitedByGuild(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetComponentGuildData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGuildCreateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinApplyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinAdminConfirmAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinApplyRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinPlayerConfirmAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildSearchInfo>> EventOnGuildSearchAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildSearchInfo>> EventOnGuildRandomListAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<UserSummary>> EventOnGuildInvitePlayerListAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildHiringDeclarationSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildJoinInvitation>> EventOnGuildJoinInvitationGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinInvitationRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAllGuildJoinInvitationRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Guild> EventOnGuildGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildBasicSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildAnnouncementSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<UserSummary>> EventOnGuildJoinApplyGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildNameChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<GuildLog>> EventOnGuildLogGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildJoinInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildVicePresidentAppointAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildKickOutAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildPresidentRelieveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildPresidentAppointAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAllGuildJoinApplyRefuseAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildLog> EventOnGuildUpdateInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatDataReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatStartReq(int difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatSurrenderReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatConquerReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatAttackReq(int levelId, List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGuildMassiveCombatAttackFinishedReq(int levelId, ProBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatDataAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatPlayerNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatStartAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatSurrenderAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatConquerAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GuildMassiveCombatAttackFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildMassiveCombatStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatStrongholdTakeOver(int levelId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStartedGuildMassiveCombatThisWeek()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanStartGuildMassiveCombat(int Difficulty)
    {
      return this.m_guildComponent.CanStartMassiveCombat(Difficulty);
    }

    public int CanAttackGuildMassiveCombatSinglePVELevel(int levelId)
    {
      return this.m_guildComponent.CanAttackStronghold(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildPlayerMassiveCombatInfo GetGuildPlayerMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUIntMassiveCombatPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowGuildMassiveCombatFirstOpenRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FillGuildMassiveCombatMemberSummary(
      GuildMassiveCombatGeneral Info,
      Action OnComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFillGuildMassiveCombatNtfSummary(FriendGetUserSummaryAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFillGuildMassiveGetDataSummary(List<UserSummary> users, Action OnComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo GetGuildMassiveCombatInfo(ulong instanceId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatGeneral GetGuildMassiveCombatGeneral()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildMassiveCombatEliminateRate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold GetGuildMassiveCombatStronghold(
      int levelId,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombatConquered(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombatFinished(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatConqueredAndFinished(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatConqueredAndNotFinished(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMassiveCombatSurrender(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGuildMassiveCombatAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnGuildMassiveCombatAttackFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, GuildMassiveCombatGeneral> EventOnGuildMassiveCombatDataAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatGeneral> EventOnGuildMassiveCombatNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildPlayerMassiveCombatInfo> EventOnGuildMassiveCombatPlayerNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildMassiveCombatStartAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildMassiveCombatSurrenderAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGuildMassiveCombatConquerAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatInfo> EventOnGuildMassiveCombatStartNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatInfo> EventOnGuildMassiveCombatSurrenderNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatInfo> EventOnGuildMassiveCombatConqueredNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenHeroJobRefineryReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobRefineReq(
      int heroId,
      int jobConnectionId,
      int slotId,
      int index,
      int stoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool SendHeroJobRefineSaveReq()
    {
      return this.SendMessage((object) new HeroJobRefineSaveReq());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelUpHeroHeartFetterLevelReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnlockHeroHeartFetterReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAutoTakeOffEquipmentsReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendExchangeHeroFragmentReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroFavorabilityExpAddReq(
      int heroId,
      GoodsType goodsType,
      int goodId,
      int nums)
    {
      throw 1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroInteractReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroConfessReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSetProtagonistReq(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobTransferReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroStarLevelUpReq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobLevelUpReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroSkillsSelectReq(
      int heroId,
      List<int> skillsId,
      SkillAndSoldierSelectMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroSoldierSelectReq(int heroId, int soldierId, SkillAndSoldierSelectMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroComposeReqq(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroExpAddReq(int heroId, GoodsType goodsType, int itemId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroJobUnlockReq(int heroId, int jobRelateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentWearReq(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEquipmentTakeOffReq(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendAutoEquipReq(int heroId, List<ulong> equipmentInstanceIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroFetterUnlockReq(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroFetterLevelUp(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCharSkinWear(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCharSkinTakeOff(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendModelSkinWear(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendModelSkinTakeOff(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSoldierSkinWearReq(
      int heroId,
      int soldierId,
      int soldierSkinId,
      bool isSetToAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSoldierSkinTakeOffReq(int heroId, int soldierId, bool isAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OpenHeroJobRefineryAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobRefineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobRefineSaveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnlockHeroHeartFetterAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelUpHeroHeartFetterLevelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(AutoTakeOffEquipmentsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ExchangeHeroFragmentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSHeroNtf msg)
    {
      this.m_heroComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroConfessAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroInteractAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroFavorabilityExpAddAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SetProtagonistAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobTransferAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroStarLevelLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroSkillsSelectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroSoldierSelectAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroComposeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroExpAddAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroJobUnlockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EquipmentAutoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroFetterLevelUpAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroFetterUnlockAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CharSkinWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CharSkinTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ModelSkinWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ModelSkinTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SoldierSkinWearAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SoldierSkinTakeOffAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GmResetHeroJob(int heroId)
    {
      return this.m_heroComponent.GmResetHeroJob(heroId);
    }

    public int GmLevelUpHeroFetter2SpecificLevel(int heroId, int fetterId, int reachLevel)
    {
      return this.m_heroComponent.GmLevelUpHeroFetter2SpecificLevel(heroId, fetterId, reachLevel);
    }

    public int GmLevelUpHeroHeartFetter(int heroId, int reachLevel)
    {
      return this.m_heroComponent.GmSetHeroHeartFetter(heroId, reachLevel);
    }

    public int GetAdditiveHeroAddExp(int addExp)
    {
      return this.m_heroComponent.GetAdditiveHeroAddExp(addExp);
    }

    public int GetAdditiveHeroFavourabilityAddExp(int addExp)
    {
      return this.m_heroComponent.GetAdditiveHeroFavourabilityAddExp(addExp);
    }

    public int GetHeroFavorabilityUpLevel(
      Hero hero,
      ConfigDataHeroInformationInfo heroInformationInfo,
      int addExp)
    {
      return this.m_heroComponent.GetHeroFavorabilityUpLevel(hero, heroInformationInfo, addExp);
    }

    public ConfigDataHeroInteractionInfo GetHeroInteractionInfo(
      int heroId)
    {
      return this.m_heroComponent.GetHeroInteractionInfo(heroId);
    }

    public int GetHeroInteractHeroPerformanceId(int heroId, HeroInteractionResultType resultType)
    {
      return this.m_heroComponent.GetHeroInteractHeroPerformanceId(heroId, resultType);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitAllUseableDefaultConfigHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero CreateDefaultHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Hero GetHero(int heroId)
    {
      return this.m_heroComponent.FindHero(heroId);
    }

    public List<Hero> GetHeros()
    {
      return this.m_heroComponent.GetAllHeros();
    }

    public Dictionary<int, Hero> GetAllUseableDefaultHeros()
    {
      return this.m_allUseableDefaultHeros;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetUseableJobConnectionInfos(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroNextLevelExp(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetHeroSkillPointMax(int heroLevel)
    {
      return this.m_heroComponent.GetSkillEnergyFromConfig(heroLevel);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroShowRedMark(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanLevelUpHeroStarLevel(int heroId)
    {
      return this.m_heroComponent.CanLevelUpHeroStarLevel(heroId);
    }

    public bool IfWasteAddExp(Hero hero, int addExp)
    {
      return this.m_heroComponent.IsWastefulAddExp(hero, addExp);
    }

    public int CanLevelUpHeroJobLevel(int heroId, int jobConnectionid)
    {
      return this.m_heroComponent.CanLevelUpHeroJobLevel(heroId, jobConnectionid);
    }

    public int CanHeroJobTransfer(int heroId, int jobConnectionId)
    {
      return this.m_heroComponent.CanTransferHeroJob(heroId, jobConnectionId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWorldHeroTabShowRedIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroHaveNewJobCanTransfer(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroJobNeedMagicStone(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanComposeHero(int heroId)
    {
      return this.m_heroComponent.CanComposeHero(heroId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkillLimitToHeroJob(int jobRelatedId, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetProtagonistId()
    {
      return this.m_heroComponent.GetProtagonistID();
    }

    public bool IsProtagonistHero(int heroId)
    {
      return this.m_heroComponent.IsProtagonistHero(heroId);
    }

    public bool IsProtagonistExist()
    {
      return this.m_heroComponent.IsProtagonistExist();
    }

    public bool IsCurrentLevelMaxHeroLevel(int level)
    {
      return this.m_heroComponent.IsCurrentLevelMaxHeroLevel(level);
    }

    public int CanHeroSelectSolider(Hero hero, int soliderId)
    {
      return HeroComponentCommon.CanHeroSelectSolider(hero, soliderId);
    }

    public int GetHeroInteractNums()
    {
      return this.m_heroComponent.GetHeroInteractNums();
    }

    public DateTime GetHeroInteractNumsFlushTime()
    {
      return this.m_heroComponent.GetHeroInteractNumsFlushTime();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroCanComposed(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsExistSoliderId(int soliderId)
    {
      return this.m_heroComponent.IsExistSoliderId(soliderId);
    }

    public bool IsHeroAssigned(int heroId)
    {
      return this.m_heroAssistantsComponent.IsHeroAssigned(heroId);
    }

    public List<HeroFragmentBagItem> GetAllStarLvlMaxHeroFragements()
    {
      return this.m_heroComponent.GetAllStarLvlMaxHeroFragments();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowJobLevelCanUpTip(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSelectSkillHero(Hero hero, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasIgnoreCostEquipment(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsEquipmentWeared(ulong instanceId)
    {
      return this.m_heroComponent.IsEquipmentWeared(instanceId);
    }

    public int GetWearedEquipmentHeroIdByEquipmentId(ulong equipmentId)
    {
      return this.m_heroComponent.GetWearedEquipmentHeroIdByEquipmentId(equipmentId);
    }

    public int CanWearEquipment(int heroId, ulong instanceId)
    {
      return this.m_heroComponent.CanWearEquipment(heroId, instanceId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBetterEquipmentByHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBetterEquipmentBySlotId(int heroId, int slotId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong[] GetBestEquipments(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, List<EquipmentBagItem> quipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAutoEquipment(int heroId)
    {
      return this.m_heroComponent.CanAutoEquipmentByClient(heroId);
    }

    public int CanTakeOffEquipment(int heroId, int slot)
    {
      return this.m_heroComponent.CanTakeOffEquipment(heroId, slot);
    }

    public int CanUnlockHeroHeartFetter(int heroId)
    {
      return this.m_heroComponent.CanUnlockHeroHeartFetter(heroId);
    }

    public int CanLevelUpHeroHeartFetter(int heroId)
    {
      return this.m_heroComponent.CanLevelUpHeroHeartFetter(heroId);
    }

    public int CanUnlockHeroPerformance(int heroPerformanceId)
    {
      return this.m_heroComponent.CanUnlockHeroPerformance(heroPerformanceId);
    }

    public int CanUnlockHeroBiography(int biographyId)
    {
      return this.m_heroComponent.CanUnlockHeroBiography(biographyId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanReachFetterUnlockCondition(HeroFetterCompletionCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanInteractHero(int heroId)
    {
      return this.m_heroComponent.CanInteractHero(heroId);
    }

    public int CanConfessHero(int heroId)
    {
      return this.m_heroComponent.CanConfessHero(heroId);
    }

    public int CanLevelUpHeroFetter(int heroId, int fetterId)
    {
      return this.m_heroComponent.CanLevelUpHeroFetter(heroId, fetterId);
    }

    public int CanUnlockHeroFetter(int heroId, int fetterId)
    {
      return this.m_heroComponent.CanUnlockHeroFetter(heroId, fetterId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ImitateUseHeroFavorabilityExpItem(
      int heroId,
      int itemId,
      int nums,
      GoodsType goodsType = GoodsType.GoodsType_Item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullFavorabilityExp(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowFetterRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroFetterHasNewOrLevelUp(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanWearCharSkin(int heroId, int charSkinId)
    {
      return this.m_heroComponent.CanWearCharSkin(heroId, charSkinId);
    }

    public int CanTakeOffCharSkin(int heroId)
    {
      return this.m_heroComponent.CanTakeOffCharSkin(heroId);
    }

    public int CanWearModelSkin(int heroId, int jobRelatedId, int modelSkinId)
    {
      return this.m_heroComponent.CanWearModelSkin(heroId, jobRelatedId, modelSkinId);
    }

    public int CanTakeOffModelSkin(int heroId, int jobRelatedId)
    {
      return this.m_heroComponent.CanTakeOffModelSkin(heroId, jobRelatedId);
    }

    public int CanWearSoldierSkin(int heroId, int soldierId, int soldierSkinId)
    {
      return this.m_heroComponent.CanWearSoldierSkin(heroId, soldierId, soldierSkinId);
    }

    public int CanTakeOffSoldierSkin(int heroId, int soldierId)
    {
      return this.m_heroComponent.CanTakeOffSoldierSkin(heroId, soldierId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowSkinBeforeOnSale(int fixedStoreItemId, bool isHeroSkin)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanOpenHeroJobRefineryReq(int heroId, int jobConnectId)
    {
      return this.m_heroComponent.CanOpenHeroJobRefineryReq(heroId, jobConnectId);
    }

    public bool IsJobLevelMax(HeroJob heroJob)
    {
      return this.m_heroComponent.IsJobLevelMax(heroJob);
    }

    public bool IsHideEquipMaster
    {
      get
      {
        return this.isHideEquipMaster;
      }
    }

    public bool DonotShowEquipMasterCloseConfirmPanelThisLogin { get; set; }

    public bool DonotShowEquipMasterSaveConfirmPanelThisLogin { get; set; }

    public event Action<int> EventOnSetProtagonistAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobTransferAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroStarLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroSkillsSelectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroSoldierSelectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroComposeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroExpAddAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobUnlockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnEquipmentTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAutoEquipAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroFetterLevelUpAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnHeroConfessAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnHeroFavorabilityExpAddAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnFetterUnlockAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroInteractAck, List<Goods>, int> EventOnHeroInteractAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCharSkinWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCharSkinTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnModelSkinWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnModelSkinTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSoldierSkinWearAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSoldierSkinTakeOffAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnExchangeHeroFragementAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAutoTakeOffEquipmentsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnUnlockHeroHeartFetterAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLevelUpHeroHeartFetterLevelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnOpenHeroJobRefineryAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, HeroJobSlotRefineryProperty> EventOnHeroJobRefineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroJobRefineSaveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAnthemLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAnthemLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSHeroAnthemNtf msg)
    {
      this.m_heroAnthemComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAnthemLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAnthemLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackHeroAnthemLevel(int levelId)
    {
      return this.m_heroAnthemComponent.CanAttackLevel(levelId);
    }

    public bool IsHeroAnthemLevelFinished(int levelId)
    {
      return this.m_heroAnthemComponent.IsLevelFinished(levelId);
    }

    public int GetHeroAnthemMaxFinishedLevelId(int chapterId)
    {
      return this.m_heroAnthemComponent.GetMaxFinishedLevelId(chapterId);
    }

    public bool HasHeroAnthemLevelAchievement(int achievementRelatedId)
    {
      return this.m_heroAnthemComponent.HasGotAchievementRelationId(achievementRelatedId);
    }

    public int GetHeroAnthemLevelAchievementCount(int levelId)
    {
      return this.m_heroAnthemComponent.GetHeroAnthemLevelAchievements(levelId);
    }

    public int GetHeroAnthemChapterAchievementCount(int chapterId)
    {
      return this.m_heroAnthemComponent.GetChapterHeroAnthemAchievements(chapterId);
    }

    public event Action<int> EventOnHeroAnthemLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.HeroAnthemLevelBattleFinishedAckCallback EventOnHeroAnthemLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAssistantAssignToTaskReq(
      int TaskId,
      int Slot,
      List<int> HeroIds,
      int WorkSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAssistantCancelTaskReq(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroAssistantClaimRewardReq(int TaskId, int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSHeroAssistantNtf ntf)
    {
      this.m_heroAssistantsComponent.DeSerialize(ntf);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAssistantAssignToTaskAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAssistantCancelTaskAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroAssistantClaimRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<HeroAssistantsTask> GetAllTodayHeroAssistantsTask()
    {
      return this.m_heroAssistantsComponent.GetTodayHeroAssistantsTask();
    }

    public List<HeroAssistantsTask> GetHeroAssistantsTasksByWeekDay(
      int weekDay)
    {
      return this.m_heroAssistantsComponent.GetHeroAssistantsTaskByWeekDay(weekDay);
    }

    public List<HeroAssistantsTaskAssignment> GetAssignedHeroAssistantsTask()
    {
      return this.m_heroAssistantsComponent.GetAssignedHeroAssistantsTask();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedAssistantTask()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetDropIdByTaskCompleteRate(int taskId, int completeRate)
    {
      return this.m_heroAssistantsComponent.GetDropIdByTaskCompleteRate(taskId, completeRate);
    }

    public int GetDropCountByTaskWorkSeconds(int taskId, int workSeconds)
    {
      return this.m_heroAssistantsComponent.GetDropCountByTaskWorkSeconds(taskId, workSeconds);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAssignHero(List<int> canAssignHero, int taskId, int slot, int workSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanCancelTask(int taskId, int slot)
    {
      return this.m_heroAssistantsComponent.CanCancelTask(taskId, slot);
    }

    public int CanClaimRewards(int taskId, int slot)
    {
      return this.m_heroAssistantsComponent.CanClaimRewards(taskId, slot);
    }

    public TimeSpan GetTaskRemainingTime(int taskId, int slot)
    {
      return this.m_heroAssistantsComponent.GetTaskRemainingTime(taskId, slot);
    }

    public event Action<int> EventOnHeroAssistantAssignToTaskAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroAssistantCancelTaskAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnHeroAssistantClaimRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonLevelBattleFinishedReq(
      int HeroDungeonLevelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonLevelRaidReq(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroDungeonChapterStarRewardGainReq(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSHeroDungeonNtf msg)
    {
      this.m_heroDungeonComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonLevelRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroDungeonChapterStarRewardGainAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanUnLockHeroDungeonLevel(int levelID)
    {
      return this.m_heroDungeonComponent.CanUnlockHeroDungeonLevel(levelID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsHeroDungeonLevelAttachUnlockLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackHeroDungeonLevel(int levelID)
    {
      return this.m_heroDungeonComponent.CanAttackHeroDungeonLevel(levelID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelChallengeNum(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelCanChallengeNum(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelCanChallengeMaxNum(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidHeroDungeonLevel(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelStars(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonLevelAchievementCount(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasHeroDungeonLevelAchievement(int achievementID)
    {
      return this.m_heroDungeonComponent.HasGotAchievementRelationId(achievementID);
    }

    public int GetHeroDungeonChapterGainStars(int chapterID)
    {
      return this.m_heroDungeonComponent.GetHeroDungeonChapterStar(chapterID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonChapterAllStars(int chapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetHeroDungeonStarRewardStatus(int chapterID, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanGainHeroDungeonStarReward(int chapterID, int index)
    {
      return this.m_heroDungeonComponent.CanGainHeroDungeonChapterStarRewards(chapterID, index);
    }

    public bool IsLevelChallenged(int levelID)
    {
      return this.m_heroDungeonComponent.IsLevelChallenged(levelID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroDungeonNewMarkShow(int heroDungeonChapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HeroDungeonProgress(int heroDungeonChapterID)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetHeroDungeonDailyChallengeMaxNums()
    {
      return this.m_heroDungeonComponent.GetDailyChallengeMaxNums();
    }

    public bool IsHeroDungeonLevelFinihed(int levelId)
    {
      return this.m_heroDungeonComponent.IsFinishedLevel(levelId);
    }

    public int GetHeroDungeonMaxFinishedLevelId(int chapterId)
    {
      return this.m_heroDungeonComponent.GetMaxFinishedLevelId(chapterId);
    }

    public event Action<int> EventOnHeroDungeonLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward, List<Goods>> EventOnHeroDungeonLevelRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnHeroDungeonRewardGainAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.HeroDungeonLevelBattleFinishedAckCallback EventOnHeroDungeonLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroPhantomAttackReq(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroPhantomBattleFinishedReq(int LevelId, ProBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSHeroPhantomNtf ntf)
    {
      this.m_heroPhantomComponent.DeSerialize(ntf);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroPhantomAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroPhantomBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomDisplay(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomOpened(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomLevelOpened(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackHeroPhantomLevel(int levelID)
    {
      return this.m_heroPhantomComponent.CheckChallengeHeroPhantomLevel(levelID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomLevelFinihed(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetHeroPhantomMaxFinishedLevelId(int heroPhantomID)
    {
      return this.m_heroPhantomComponent.GetMaxFinishedLevelId(heroPhantomID);
    }

    public bool IsCompleteHeroPhantomLevelAchievement(int achievementRelatedInfoID)
    {
      return this.m_heroPhantomComponent.IsCompleteHeroPhantomLevelAchievement(achievementRelatedInfoID);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPhantomAchievementCount(int heroPhantomID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPhantomLevelAchievementCount(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroPhantomLevelFirstCleanComplete(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnHeroPhantomAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.HeroPhantomBattleFinishedAckCallback EventOnHeroPhantomBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroTrainningLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeroTrainningLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSHeroTrainningNtf msg)
    {
      this.m_heroTrainningComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroTrainningLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroTrainningLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsHeroTrainningOpened(int heroTrainningId)
    {
      return this.m_heroTrainningComponent.IsHeroTrainningOpened(heroTrainningId);
    }

    public bool IsHeroTrainningLevelOpened(int levelId)
    {
      return this.m_heroTrainningComponent.IsHeroTrainningLevelOpened(levelId);
    }

    public int CanAttackHeroTrainningLevel(int levelId)
    {
      return this.m_heroTrainningComponent.CanAttackHeroTrainningLevel(levelId, false);
    }

    public int GetHeroTrainningTicketCount()
    {
      return this.m_heroTrainningComponent.GetCurrentTicketNums();
    }

    public bool IsHeroTrainningLevelFinished(int levelId)
    {
      return this.m_heroTrainningComponent.IsLevelFinished(levelId);
    }

    public int GetHeroTrainningMaxFinishedLevelId(int chapterId)
    {
      return this.m_heroTrainningComponent.GetMaxFinishedLevelId(chapterId);
    }

    public int GetHeroTrainingDailyRewardRestCount()
    {
      return this.m_heroTrainningComponent.GetDailyChallengNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroTrainingDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsHeroTrainingLevelBlessing(int levelId)
    {
      return this.m_heroTrainningComponent.IsBlessing(levelId);
    }

    public event Action<int> EventOnHeroTrainningLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnHeroTrainningLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelWayPointMoveReq(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelScenarioHandleReq(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendLevelWayPointBattleFinishReq(int waypointId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSLevelNtf msg)
    {
      this.m_levelComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelWayPointMoveAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelScenarioHandleAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(LevelWayPointBattleFinishAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RandomEventNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRandomEventUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendChangedGoodsToRewards(List<ProGoods> changedGoods, List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendGoodsToRewards(
      GoodsType goodsType,
      int id,
      int count,
      List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetNotBagItemCount(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRegionOpen(int regionId)
    {
      return this.m_levelComponent.IsRegionOpen(regionId);
    }

    public ConfigDataWorldMapInfo GetCurrentWorldMapInfo()
    {
      return this.m_configDataLoader.GetConfigDataWorldMapInfo(1);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo GetPlayerCurrentWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public WayPointStatus GetWaypointStatus(int waypointId)
    {
      return this.m_levelComponent.GetWaypointStatus(waypointId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetWaypointEvent(
      int waypointId,
      out ConfigDataEventInfo eventInfo,
      out RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackScenario(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    private ConfigDataScenarioInfo GetLastFinishedScenarioInfo()
    {
      return this.m_levelComponent.GetLastFinishedScenarioInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataScenarioInfo GetNextScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockScenario(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitActiveScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo GetActiveNextScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataScenarioInfo GetActiveLastFinsishedScenarioInfo()
    {
      return this.m_activeLastFinishedScenarioInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveWaypointEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, ProjectLPlayerContext.CurrentWaypointEvent> GetActiveWaypointEvents()
    {
      return this.m_activeWaypointEvents;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo GetActiveWaypointEventInfo(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent GetActiveWaypointRandomEvent(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public void EnterWorldUITask()
    {
      ++this.m_enterWorldUITaskCount;
    }

    public bool IsFirstEnterWorldUITask()
    {
      return this.m_enterWorldUITaskCount == 1;
    }

    public event Action<int, BattleReward, bool> EventOnLevelWayPointMoveAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnLevelScenarioHandleAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnLevelWayPointBattleFinishAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRandomEventUpdate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailsGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailReadReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailAttachmentsGetReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMailAttachmentAutoGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSMailNtf msg)
    {
      this.m_mailComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailsGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailsChangedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailReadAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailAttachmentsGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MailAttachmentAutoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Mail> GetMails()
    {
      return this.m_mailComponent.GetMails();
    }

    public bool IsMailReaded(Mail mail)
    {
      return this.m_mailComponent.IsMailReaded(mail);
    }

    public bool IsExistMailAttacments(Mail mail)
    {
      return this.m_mailComponent.IsExistMailAttacments(mail);
    }

    public bool HasGotMailAttachments(Mail mail)
    {
      return this.m_mailComponent.HasGotMailAttachments(mail);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double GetMailExpiredTime(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnReadMailCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanGetMailAttachment(ulong mailId)
    {
      return this.m_mailComponent.CanGetMailAttachment(mailId, true);
    }

    public int CanAutoGetMailAttachment()
    {
      return this.m_mailComponent.CanAutoGetMailAttachment();
    }

    public bool IsOfficialMail(Mail mail)
    {
      return this.m_mailComponent.IsOfficialMail(mail);
    }

    public bool IsHaveMailList { get; set; }

    public event Action EventOnMailsGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMailsChangedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnMailReadAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnMailAttachmentsGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnMailAttachmentsAutoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMemoryCorridorLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMemoryCorridorLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSMemoryCorridorNtf msg)
    {
      this.m_memoryCorridorComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MemoryCorridorLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MemoryCorridorLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMemoryCorridorOpened(int MemoryCorridorId)
    {
      return this.m_memoryCorridorComponent.IsMemoryCorridorOpened(MemoryCorridorId);
    }

    public bool IsMemoryCorridorLevelOpened(int levelId)
    {
      return this.m_memoryCorridorComponent.IsMemoryCorridorLevelOpened(levelId);
    }

    public int CanAttackMemoryCorridorLevel(int levelId)
    {
      return this.m_memoryCorridorComponent.CanAttackMemoryCorridorLevel(levelId, false);
    }

    public int GetMemoryCorridorTicketCount()
    {
      return this.m_memoryCorridorComponent.GetCurrentTicketNums();
    }

    public bool IsMemoryCorridorLevelFinished(int levelId)
    {
      return this.m_memoryCorridorComponent.IsLevelFinished(levelId);
    }

    public int GetMemoryCorridorMaxFinishedLevelId(int chapterId)
    {
      return this.m_memoryCorridorComponent.GetMaxFinishedLevelId(chapterId);
    }

    public int GetMemoryCorridorDailyRewardRestCount()
    {
      return this.m_memoryCorridorComponent.GetDailyChallengNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMemoryCorridorDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMemoryCorridorLevelBlessing(int levelId)
    {
      return this.m_memoryCorridorComponent.IsBlessing(levelId);
    }

    public event Action<int> EventOnMemoryCorridorLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnMemoryCorridorLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetMissionRewardReq(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DirectelyActivitedMissionUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSMissionNtf msg)
    {
      this.m_missionComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GetMissionRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnMissionRewardGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<Mission> GetAllProcessingMissionList()
    {
      return this.m_missionComponent.GetAllProcessingMissionList();
    }

    public List<Mission> GetAllCompletedMissionList()
    {
      return this.m_missionComponent.GetAllCompletedMissionList();
    }

    public List<Mission> GetAllFinishedMissionList()
    {
      return this.m_missionComponent.GetAllFinishedMissionList();
    }

    public int GetMissionMaxProcess(Mission mission)
    {
      return this.m_missionComponent.GetMissionMaxProcess(mission);
    }

    public long GetMissionCompleteProcess(Mission mission)
    {
      return this.m_missionComponent.GetMissionCompletedProcess(mission);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistMissionCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanGainMissionReward(int missionId)
    {
      return this.m_missionComponent.CanGainMissionReward(missionId);
    }

    public bool CanGetMissionRewarding(Mission mission)
    {
      return this.m_missionComponent.CanGetRewarding(mission);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendNoviceClaimRewardReq(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSNoviceNtf msg)
    {
      this.m_noviceComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NoviceClaimRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetDaysAfterCreation()
    {
      return this.m_noviceComponent.GetDaysAfterCreation();
    }

    public Dictionary<int, List<int>> GetNoviceMissions()
    {
      return this.m_noviceComponent.GetMissions();
    }

    public List<Mission> GetNoviceProcessingMissions()
    {
      return this.m_noviceComponent.GetProcessingMissions();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowRedMarkOnOpenServiceButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetNoviceMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetNoiviceMissionsByDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Mission> GetFinishedAndGetRewardNoviceMissions()
    {
      return this.m_noviceComponent.GetFinishedMissions();
    }

    public int GetNoviceMissionPoints()
    {
      return this.m_noviceComponent.GetMissionPoints();
    }

    public List<ConfigDataNoviceRewardInfo> GetNovicePointsRewardsConfig()
    {
      return this.m_noviceComponent.GetNovicePointsRewardsConfig();
    }

    public int CanClaimNoviceReward(int slot)
    {
      return this.m_noviceComponent.CanClaimReward(slot);
    }

    public TimeSpan GetNoviceMissionsEndTime()
    {
      return this.m_noviceComponent.GetMissionsEndTime();
    }

    public event Action<int, List<Goods>> EventOnNoviceClaimRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOperationalActivityGainRewardReq(
      ulong operationalActivityInstanceId,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOperationalActivityExchangeItemGroupReq(
      ulong operationalActivityInstanceId,
      int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRedeemInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRedeemClaimReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFansRewardsFromPBTCBTInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFansRewardsFromPBTCBTClaimReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OperationalActivityGainRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OperationalActivityExchangeItemGroupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(AdvertisementFlowLayoutUpdateNtf msg)
    {
      this.m_operationalActivityComponent.Deserialize(msg);
    }

    private void OnMessage(DSOperationalActivityNtf msg)
    {
      this.m_operationalActivityComponent.Deserialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RemoveAnnouncementNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSAnnouncementNtf msg)
    {
      this.m_operationalActivityComponent.Deserialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewOperationalActivityNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewAnnouncementNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UpdateOperationalActivityNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewMarqueeNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RedeemInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RedeemClaimAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FansRewardsFromPBTCBTInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(FansRewardsFromPBTCBTClaimAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Announcement> GetAllCurrentAnnouncements()
    {
      return this.m_operationalActivityComponent.GetValidAnnouncements();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRedeemInfo(RedeemInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRedeemClaimed()
    {
      // ISSUE: unable to decompile the method.
    }

    public RedeemInfoAck GetRedeemInfo()
    {
      return this.m_RedeemInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRedeemClaimed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> GetAllCurrentActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanShowActivity(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanActivityGainReward(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanActivityGainRewardResult(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      return this.m_operationalActivityComponent.CanGainOperactionalActivityReward(operationalActivityBase, rewardIndex);
    }

    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      return this.m_operationalActivityComponent.FindOperationalActivityById(operationalActivityInstanceId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeGoodsResult(
      OperationalActivityBase operationalActivityBase,
      int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTabRedPointShow(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActivityRedPointShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstTryToOpenActivityNoticeUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveActivityNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetBuyGuideActivityViewed()
    {
      this.m_isBuyGuideActivityViewed = true;
    }

    public event Action<int> EventOnActivityGainRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnActivityExchangeItemGroupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Marquee> EventOnNewMarqueeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnActivityRedeemClaimAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnActivityFansRewardsFromPBTCBT
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendNoticeEnterPlayOffRaceReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleLiveRoomMatchInfoGetReq(ulong roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleLiveRoomJoinReq(int matchId, ulong roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleLiveRoomQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBetReq(int matchupId, string userid, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleReportShare2FriendsReq(
      ulong battleReportId,
      List<string> friendUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleReportChangeNameReq(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPublicHeroesPoolReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleTeamSetupReq(PeakArenaBattleTeam battleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleTeamGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaGetTopPlayersReq(int n)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaGetInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffCurrentSeasonMatchInfoGetReq(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaAcquireWinsBonusReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffInfoGetReq(int sequenceId, int matchIndex, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffLiveRoomGetReq(int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaMyPlayOffInfoGetReq(int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaBattleReportsGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBattleReportBattleInfoGetReq(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffMatchSetReadyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaPlayOffBattleSetReadyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaDanmakuReq(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendPeakArenaSeasonSimpleInfoReq(int seasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NoticeEnterPlayOffRaceAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleLiveRoomMatchInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleNameBanTimeUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleLiveRoomQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleLiveRoomFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleLiveRoomJoinAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleReportShare2FriendsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleReportChangeNameAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSPeakArenaNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaDataUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleTeamGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleTeamSetupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPublicHeroesPoolAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaGetTopPlayersAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaGetInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffCurrentSeasonMatchInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaAcquireWinsBonusAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaAcquireRegularRewardsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffLiveRoomInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaMyPlayOffInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaBattleReportsGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BattleReportBattleInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffMatchSetReadyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayOffBattleSetReadyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaDanmakuAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaDanmakuNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaMatchFinishNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaShakeHandsNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaPlayerForfeitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PeakArenaSeasonSimpleInfoAck ack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyJettonUseCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanBuyJetton()
    {
      return this.m_playerBasicInfoComponent.CanBuyJetton();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBetPeakArenaCare4Round(int round, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<PeakArenaHeroWithEquipments> GetPeakArenaPublicHeroesPool()
    {
      return this.m_peakArenaComponent.PeakArenaPublicHeroesPool;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHeroWithEquipments GetPeakArenaPublicHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroEquipment> GetPeakArenaPublicBattleHeroEquipments(
      int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero PeakArenaMercenaryToBattleHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMercenaryInBattleTeam(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMercenarySoldierChange(int heroID, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMercenarySkillChange(int heroId, List<int> skillIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMyHeroInPeakArenaBattleTeam(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaBattleTeamAllHeroCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaHiredHero GetPeakArenaMercenaryInBattleTeam(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaTeamManagementComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ProUserSummary> GetPeakArenaTopRankPlayerSummaries()
    {
      return this.m_peakArenaTopRankPlayerSummaries;
    }

    public List<ProPeakArenaLeaderboardPlayerInfo> GetPeakArenaTopRankPlayerInfos()
    {
      return this.m_peakArenaTopRankPlayerInfos;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerDan()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaPlayerQuitCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo GetPeakArenaSeasonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaSeasonId()
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime GetPeakArenaSeasonEndTime(int Season)
    {
      return this.m_peakArenaComponent.GetPeakArenaSeasonEndTime(Season);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinalRoundKnockOutMatchsFinish()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CalcPeakArenaSeasonId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EPeakArenaDurringStateType GetCurPeakArenaStateType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaPlayOffOverDayEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleTimeIsOverEndTimeInSameDay(
      DateTime srcTime,
      DateTime tarTime,
      int minutes,
      ref DateTime cachedDate,
      ref bool result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDuringPeakArenaSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan CalcTimeBeforePeakArenaSeasonBegin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaSeasonBeginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInPeakAreanPreRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPlayerFinishPreRankingState()
    {
      return !this.m_peakArenaComponent.IsInPreRankingState();
    }

    public int GetPeakArenaConsecutiveWins(RealTimePVPMode mode)
    {
      return this.m_peakArenaComponent.GetConsecutiveWins(mode);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats GetPeakArenaMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAlreadyGainPeakArenaWeekFirstWinReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaPointRaceEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaInKnockout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaKnockoutRound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaKnockoutRoundAboutToStart(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDuringPeakArenaKnockoutRoundMatch(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaKnockoutRoundStart(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaPlayOffRoundStartTime(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetPeakArenaPlayOffRoundEndTime(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsKnockOutMatchRoundEnd(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerKnockoutGroupId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo GetPeakArenaGroupRootKnockoutInfo(
      int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaPlayOffMatchupInfo> GetAllPeakArenaKnockoutInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaPlayOffMatchupInfo> GetPeakArenaMatchupInfosByRound(
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    public PeakArenaPlayOffMatchupInfo GetPeakArenaNextMatchInfo()
    {
      return this.m_peakArenaComponent.MyNextPlayoffMatch;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMineReadyAtKnockOutMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaGroupId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaKnockOutGroupName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool DoesPlayerHaveNextKnockOutMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerHaveBeenInPeakArenaKnockOutMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerFinalRound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerFinalRoundTitleName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime GetCurPeakArenaSeasonKnockOutMatchTime(
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime GetPeakArenaSeasonKnockOutMatchTime(
      int round,
      int seasonID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataPeakArenaKnockoutMatchTime> GetPeakArenaSeasonKnockoutTimeTable(
      int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMatchmakingAvailable(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaMatchmakingAvailableWithOutGameFunction(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaDanMaxDisplayDan()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<BattleReportHead> GetAllPeakArenaBattleReports()
    {
      return this.m_peakArenaComponent.GetBattleReport();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportHead GetPeakArenaBattleReport(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsNeedGetPeakArenaBattleReports()
    {
      return !this.m_peakArenaComponent.BattleReportInited;
    }

    public List<PeakArenaBattleReportNameRecord> GetPeakArenaBattleReportNameRecords()
    {
      return this.m_peakArenaComponent.GetBattleReportNameRecords();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportNameRecord GetPeakArenaBattleReportNameRecord(
      ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaBattleReportStateName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaPlayOffRoundText(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportDefaultName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportStateTypeName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportStateLevelName(
      PeakArenaLadderBattleReport battleReport,
      string sourceUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetNoticeEnterPlayOffRaceTag()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, PeakArenaBattleTeam> EventOnPeakArenaBattleTeamGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleTeamSetupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<PeakArenaHeroWithEquipments>> EventOnPeakArenaPublicHeroesPoolAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaGetTopPlayersAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaGetInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, PeakArenaPlayOffMatchupInfo> EventOnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnPeakArenaAcquireWinsBonusAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaAcquireRegularRewardsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int, long> EventOnPeakArenaPlayOffInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<ProPeakArenaLiveRoomInfo>> EventOnPeakArenaPlayOffLiveRoomInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaMyPlayOffInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleReportShare2FriendsAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleReportsGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReportHead> EventOnBattleReportBattleInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaPlayOffMatchSetReadyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaPlayOffBattleSetReadyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleReportChangeNameAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleLiveRoomJoinAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBattleLiveRoomQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaBetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleLiveRoomFinishReason> EventOnBattleLiveRoomFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPeakArenaDanmakuAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, PeakArenaBattleLiveRoomInfo> EventOnPeakArenaBattleLiveRoomInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, string> EventOnPeakArenaDanmakuNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventNoticeEnterPlayOffRaceAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaMatchFinishNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaShakeHandsNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnPeakArenaPlayerForfeitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaSeasonSimpleInfoAck> EventOnPeakArenaSeasonSimpleInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGainDialogRewardReq(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool SendJettonBuyReq()
    {
      return this.SendMessage((object) new JettonBuyReq());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenMemoryStoreReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendShareReq(int heroId, int archiveId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCheckOnlineReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool SendPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool SendPlayerInfoReqOnReloginBySession(bool isOnlyCheck)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerInfoInitReq BuildPlayerInfoInitReq(bool sendDSVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendCreateCharactorReq(string nickName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBuyEnergyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBuyArenaTicketsReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeadPortraitSetReq(int headPortraitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeadFrameSetReq(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendHeadPortraitAndHeadFrameSetReq(int headPortraitId, int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendChangePlayerNameReq(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendExchangeGiftCDKeyReq(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClientHeartBeatNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool SendClientAppPauseNtf()
    {
      return this.SendMessage((object) new ClientAppPauseNtf());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendClientAppResumeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool SendOpenGameRatingReq()
    {
      return this.SendMessage((object) new GameRatingOpenReq());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GainDialogRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(JettonBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeroErrorMsgNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WriteHeroErrorFileLog(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OpenMemoryStoreAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DailyFlushNtf msg)
    {
      this.m_playerBasicInfoComponent.FlushPlayerAction();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ShareAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(HeadPortraitAndHeadFrameSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RMBUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(FlushConfigNtf ntf)
    {
      Console.WriteLine("need flush config");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ExchangeGiftCDKeyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PlayerInfoInitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(CreateCharactorAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSPlayerBasicNtf msg)
    {
      this.InitPlayerBasic(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PlayerInfoInitEndNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(ProChangedGoodsNtf msg)
    {
      this.SetChangedGoodsListStatus(msg.Changed);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(BuyEnergyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ArenaTicketsBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(PlayerNameChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GameRatingOpenAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(CheckOnlineAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetChangedGoodsListStatus(List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetChangedGoodsStatus(
      ProGoods goods,
      ref bool bagChange,
      ref bool playerInfoChange,
      ref bool heroChange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase SetBagItemPropertyChanges(ProGoods bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMemoryStoreOpen()
    {
      return this.m_playerBasicInfoComponent.IsMemoryStoreOpen();
    }

    public DateTime GetEnergyReachMaxTime()
    {
      return this.m_playerBasicInfoComponent.GetEnergyReachMaxTime();
    }

    public DateTime GetArenaTicketNextGivenTime()
    {
      return this.m_arenaComponent.GetArenaTicketNextGivenTime();
    }

    public DateTime GetBlackMarketNextFlushTime()
    {
      return this.m_randomStoreComponent.GetStoreNextFlushTime(1);
    }

    private void OnMessage(UpdateServerTimeNtf msg)
    {
      this.UpdateServerTime(msg.ServerTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayerInfo(PlayerInfoInitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateServerTime(long serverTimeTicks)
    {
      // ISSUE: unable to decompile the method.
    }

    private void InitPlayerBasic(DSPlayerBasicNtf msg)
    {
      this.m_playerBasicInfoComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyJettonBuyCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetRechargeRMB()
    {
      return this.m_playerBasicInfoComponent.GetRechargeRMB();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildMedal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMithralStone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChallengePoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFashionPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBrillianceMithralStone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetEnergyMax()
    {
      return this.m_configDataLoader.ConfigableConstId_EnergyMax;
    }

    public DateTime GetCreateTime()
    {
      return this.m_playerBasicInfoComponent.GetCreateTime();
    }

    public DateTime GetCreateTimeUtc()
    {
      return this.m_playerBasicInfoComponent.GetCreateTimeUtc();
    }

    public int CanBuyEnergy()
    {
      return this.m_playerBasicInfoComponent.CanBuyEnergy();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuyEnergyNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEnergyFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanBuyArenaTickets()
    {
      return this.m_playerBasicInfoComponent.CanBuyArenaTickets();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuyArenaTicketsNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPlayerLevelMax()
    {
      return this.m_playerBasicInfoComponent.IsPlayerLevelMax();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerExp()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetPlayerNextLevelExp()
    {
      return this.m_playerBasicInfoComponent.GetNextLevelExpFromConfig();
    }

    public int GetPlayerVip()
    {
      return this.m_playerBasicInfoComponent.m_vipLevel;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerHeadIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetPlayerHeadPortrait()
    {
      return HeadIconTools.GetHeadPortrait(this.GetPlayerHeadIcon());
    }

    public int GetPlayerHeadFrame()
    {
      return HeadIconTools.GetHeadFrame(this.GetPlayerHeadIcon());
    }

    public int SetUserGuide(List<int> completeStepIds)
    {
      return this.m_playerBasicInfoComponent.SetUserGuide(completeStepIds);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerName()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CheckPlayerName(string name)
    {
      return this.m_playerBasicInfoComponent.CheckPlayerName(name);
    }

    public int CanChangePlayerName(string name)
    {
      return this.m_playerBasicInfoComponent.CanChangePlayerName(name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMe(string userId)
    {
      return this.m_playerBasicInfoComponent.IsMe(userId);
    }

    public void SetNeedBattleReportLog(bool isNeed)
    {
      this.IsNeedBattleReportLog = isNeed;
    }

    public bool HasHeadFrameId(int headFameId)
    {
      return this.m_resourceComponent.HasHeadFrameId(headFameId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetHeadFrameSource(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<MonthCard> GetAllValidMonthCards()
    {
      return this.m_resourceComponent.GetAllValidMonthCards();
    }

    public List<MonthCard> GetAllMonthCards()
    {
      return this.m_resourceComponent.GetAllMonthCards();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardNeverBuy(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasValidMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanGainDialogReward(int dialogId)
    {
      return this.m_playerBasicInfoComponent.CanGainDialogReward(dialogId);
    }

    public bool IsNeedBattleReportLog { get; private set; }

    public string HeroErrMsg
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnExchangeGiftCDKeyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override event Action<object> EventOnPlayerInfoInitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override event Action EventOnPlayerInfoInitEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnCreateCharactorAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuyEnergyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeadPortraitAndHeadFrameSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnPlayerNameChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPlayerInfoChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnArenaTicketsBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOpenGameRatingAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShareAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCheckOnlineAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSendClientHeartBeatNtfFail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<Goods>> EventOnGainDialogRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSendClientAppResumeNtfFail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOpenMemoryStoreAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnJettonBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnResetPushNotification
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRaffleDrawReq(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSRaffleNtf msg)
    {
      this.m_raffleComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RaffleDrawAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public RafflePool GetRafflePool(int poolId)
    {
      return this.m_raffleComponent.GetRafflePool(poolId);
    }

    public RaffleItem GetRaffleItem(RafflePool pool, int raffleId)
    {
      return this.m_raffleComponent.GetRaffleItem(pool, raffleId);
    }

    public int GetDrawItemCost(RafflePool pool)
    {
      return this.m_raffleComponent.GetDrawItemCost(pool);
    }

    public int CanDraw(int poolId)
    {
      return this.m_raffleComponent.CanDraw(poolId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRafflePoolOnActivityTime(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int, Goods> EventOnRaffleDrawAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetRandomStoreReq(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendFlushRandomStoreReq(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendBuyRandomStoreItemReq(int storeId, int index, int selectedIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSRandomStoreNtf msg)
    {
      this.m_randomStoreComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RandomStoreGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RandomStoreFlushAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RandomStoreBuyStoreItemAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<RandomStoreItem> GetRandomStoreItemList(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetNextFlushTime(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanManualRefreshRandomStore(int storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    public RandomStore FindRandomStoreByID(int storeID)
    {
      return this.m_randomStoreComponent.GetStore(storeID);
    }

    public int CanBuyRandomStoreGoods(int randomStoreID, int index, int selectedIndex = -1)
    {
      return this.m_randomStoreComponent.CanBuyRandomStoreItem(randomStoreID, index, selectedIndex);
    }

    public int GetManualFlushNums(int storeId)
    {
      return this.m_randomStoreComponent.GetManualFlushNums(storeId);
    }

    public event Action<int> EventOnGetRandomStoreListAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnFlushRandomStoreAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRandomStoreBuyStoreItemAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRankingListInfoQueryReq(
      RankingListType type,
      int heroId,
      int bossId = 0,
      int seasonId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RankingListInfoQueryAck ack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaSeasonRankTitleIdBySeasonAndRank(int rank, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPeakArenaRankingListTitle(int rank, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAbleQueryRankingListInfo(RankingListType rankingListType, out int errorCode)
    {
      return this.m_globalRankingListComponent.IsAbleQueryRankingListInfo(rankingListType, out errorCode);
    }

    public bool CheckRankingListInfoQueryCare4AncientCall(
      RankingListType rankingListType,
      int bossId,
      out int errCode)
    {
      return this.m_globalRankingListComponent.CheckRankingListInfoQueryCare4AncientCall(rankingListType, bossId, out errCode);
    }

    public RankingListInfo GetRankingListInfoByType(RankingListType rankingType)
    {
      return this.m_globalRankingListComponent.GetRankingListInfoByType(rankingType);
    }

    public RankingListInfo GetSingleBossRankingListInfoByBossId(int bossId)
    {
      return this.m_globalRankingListComponent.GetSingleBossRankingListInfoByBossId(bossId);
    }

    public RankingListInfo GetFriendAndGuildSingleBossRankingListInfoByBossId(
      int bossId)
    {
      return this.m_globalRankingListComponent.GetFriendAndGuildSingleBossRankingListInfoByBossId(bossId);
    }

    public bool IsRankingListInfoValid(RankingListType rankingListType)
    {
      return this.m_globalRankingListComponent.IsRankingListInfoValid(rankingListType);
    }

    public bool IsSingleHeroRankingListInfoValid(int heroId)
    {
      return this.m_globalRankingListComponent.IsSingleHeroRankingListInfoValid(heroId);
    }

    public RankingListInfo GetSingleHeroRankingListInfoByHeroId(int heroId)
    {
      return this.m_globalRankingListComponent.GetSingleHeroRankingListInfoByHeroId(heroId);
    }

    public event Action<int> EventOnRankingListInfoQueryAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPWaitingForOpponentReq(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPCancelWaitingForOpponentReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPGetInfoReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPGetTopPlayersReq(int topN, bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRealTimePVPAcquireWinsBonusReq(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSRealTimePVPNtf msg)
    {
      this.m_realTimePVPComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRealTimePVPPromotionBattleReportNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSRealTimePVPBattleReportNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPWaitingForOpponentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPMatchupNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPCancelWaitingForOpponentAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPGetInfoAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPGetTopPlayersAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPAcquireWinsBonusAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RealTimePVPLeaderboardInfoNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsEmptyRealTimePVPPlayerInfo()
    {
      return this.m_realtimePVPPlayerInfo == null;
    }

    public bool IsNeedGetRealTimePVPPlayerInfo()
    {
      return this.m_realtimePVPPlayerInfo == null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerRealTimePVPScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerRealTimePVPDan()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerRealTimePVPRank(bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProRealTimePVPLeaderboardPlayerInfo> GetRealTimePVPLeaderboardPlayerInfos(
      bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ProUserSummary> GetRealTimePVPLeaderboardUserSummarys(
      bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    public LinkedList<RealTimePVPBattleReport> GetRealTimePVPBattleReports()
    {
      return this.m_realTimePVPComponent.GetBattleReports();
    }

    public LinkedList<RealTimePVPBattleReport> GetRealTimePVPPromotionBattleReports()
    {
      return this.m_realTimePVPComponent.GetPromotionBattleReports();
    }

    public bool IsRealTimePVPPromotion()
    {
      return this.m_realTimePVPComponent.IsPromotion();
    }

    public int GetRealTimePVPConsecutiveWins(RealTimePVPMode mode)
    {
      return this.m_realTimePVPComponent.GetConsecutiveWins(mode);
    }

    public int CheckRealTimePVPAcquireWinsBonus(int bonusId)
    {
      return this.m_realTimePVPComponent.CheckAcquireWinsBonus(bonusId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetRealTimePVPWinsBonusStatus(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    public RealTimePVPMatchStats GetRealTimePVPLadderMatchStats()
    {
      return this.m_realTimePVPComponent.GetLadderMatchStats();
    }

    public RealTimePVPMatchStats GetRealTimePVPFriendlyMatchStats()
    {
      return this.m_realTimePVPComponent.GetFriendlyMatchStats();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRealTimePVPHonour()
    {
      // ISSUE: unable to decompile the method.
    }

    public int IsRealTimePVPArenaAvailable(RealTimePVPMode mode)
    {
      return this.m_realTimePVPComponent.IsRealTimePVPArenaAvailable(mode);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPWillMatchBot(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnRealTimePVPWaitingForOpponentAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, RealTimePVPMode> EventOnRealTimePVPMatchupNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRealTimePVPCancelWaitingForOpponentAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRealTimePVPGetInfoAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRealTimePVPGetTopPlayersAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnRealTimePVPAcquireWinsBonusAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProRealTimePVPUserInfo> EventOnRealTimePVPLeaderboardInfoNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRechargeStoreCancelBuyReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRechargeStoreBuyGoodsReq(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RechargeStoreCancelBuyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(BoughtRechargeStoreItemNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(RechargeStoreBuyGoodsAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnMessage(DSRechargeStoreNtf msg)
    {
      this.m_rechargeStoreComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMessage(UpdateRechargeStoreStatusNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRechargeGoodsBought(int goodsId)
    {
      return this.m_rechargeStoreComponent.IsGoodsBought(goodsId);
    }

    public int CaculateGotCrystalNums(int goodsId)
    {
      return this.m_rechargeStoreComponent.CaculateGotCrystalNums(goodsId);
    }

    public int GetRechargeCrystalOriginalNums(int goodsId)
    {
      return this.m_rechargeStoreComponent.GetCrystalOriginalNums(goodsId);
    }

    public int GetRechargeCrystalFirstBoughtRewardNums(int goodsId)
    {
      return this.m_rechargeStoreComponent.GetCrystalFirstBoughtRewardNums(goodsId);
    }

    public int GetRechargeCrystalRepeatlyBoughtRewardNums(int goodsId)
    {
      return this.m_rechargeStoreComponent.GetCrystalRepeatlyBoughtRewardNums(goodsId);
    }

    public string GetRechargeGoodsIcon(int goodsId)
    {
      return this.m_rechargeStoreComponent.GetGoodsIcon(goodsId);
    }

    public bool GetRechargeGoodsIsBestValue(int goodsId)
    {
      return this.m_rechargeStoreComponent.GetGoodsIsBestValue(goodsId);
    }

    public event Action<string> EventOnRechargeBoughtSuccessNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnRechargeStoreItemBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnRechargeStoreCancelBuyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRefluxClaimRewardReq(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSRefluxNtf msg)
    {
      this.m_refluxComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RefluxClaimRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetMissionDay(ConfigDataMissionInfo Mission)
    {
      return this.m_refluxComponent.GetMissionDay(Mission);
    }

    public Dictionary<long, List<long>> GetRefluxMissions()
    {
      return this.m_refluxComponent.GetMissions();
    }

    public List<Mission> GetRefluxProcessingMissions()
    {
      return this.m_refluxComponent.GetProcessingMissions();
    }

    public List<Mission> GetFinishedAndGetRewardRefluxMissions()
    {
      return this.m_refluxComponent.GetFinishedMissions();
    }

    public int GetRefluxMissionPoints()
    {
      return this.m_refluxComponent.GetMissionPoints();
    }

    public List<ConfigDataRefluxRewardInfo> GetRefluxPointsRewardsConfig()
    {
      return this.m_refluxComponent.GetRefluxPointsRewardsConfig();
    }

    public int CanClaimRefluxReward(int slot)
    {
      return this.m_refluxComponent.CanClaimReward(slot);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBackFlowActivityOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    public TimeSpan GetRefluxMissionsEndTime()
    {
      return this.m_refluxComponent.GetMissionsEndTime();
    }

    public int GetDaysAfterBackFlowActivityOpening()
    {
      return this.m_refluxComponent.GetDaysAfterOpening();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowRedMarkOnBackFlowButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetRefluxMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveFinishedNotGetRefluxMissionsByDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnRefluxClaimRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private void OnMessage(DSResourceNtf msg)
    {
      this.m_resourceComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(MonthCardUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftLevelBattleFinishedReq(int riftLevelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftLevelRaidReq(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendRiftChapterRewardGainReq(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSRiftNtf msg)
    {
      this.m_riftComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftLevelRaidAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(RiftChapterRewardGainAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanUnlockRiftChapter(int chapterId)
    {
      return this.m_riftComponent.CanUnlockChapter(chapterId);
    }

    public int GetRiftChapterTotalStars(int chapterId)
    {
      return this.m_riftComponent.GetChapterTotalStars(chapterId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalStarsMax(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalAchievementCount(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalAchievementCountMax(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalBattleTreasureCount(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftChapterTotalBattleTreasureCountMax(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRiftChapterUnlockConditionCompleted(
      int chapterId,
      RiftChapterUnlockConditionType condition,
      int param)
    {
      return this.m_riftComponent.IsRiftChapterUnlockConditionCompleted(chapterId, condition, param);
    }

    public int CanUnlockRiftLevel(int levelId)
    {
      return this.m_riftComponent.CanUnLockLevel(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackRiftLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidRiftLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus GetRiftLevelStatus(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus GetEventRiftLevelStatus(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsRiftLevelUnlockConditionCompleted(
      int levelId,
      RiftLevelUnlockConditionType condition,
      int param)
    {
      return this.m_riftComponent.IsRiftLevelUnlockConditionCompleted(levelId, condition, param);
    }

    public int GetRiftLevelChallengeNum(int levelId)
    {
      return this.m_riftComponent.GetRiftLevelChallengeNum(levelId);
    }

    public int GetRiftLevelCanChallengeNums(ConfigDataRiftLevelInfo levelInfo)
    {
      return this.m_riftComponent.GetRiftLevelCanChallengeNums(levelInfo);
    }

    public int GetRiftLevelCanChallengeMaxNums(ConfigDataRiftLevelInfo levelInfo)
    {
      return this.m_riftComponent.GetRiftLevelCanChallengeMaxNums(levelInfo);
    }

    public bool IsRiftLevelChallenged(int levelId)
    {
      return this.m_riftComponent.IsRiftLevelChallenged(levelId);
    }

    public int GetRiftLevelStars(int levelId)
    {
      return this.m_riftComponent.GetRiftLevelStars(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRiftLevelAchievementCount(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasRiftLevelAchievement(int achievementRelatedId)
    {
      return this.m_riftComponent.HasGotAchievementRelationId(achievementRelatedId);
    }

    public int CanGainRiftChapterStarReward(int chapterId, int index)
    {
      return this.m_riftComponent.CanGainChapterStarReward(chapterId, index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GainRewardStatus GetRiftChapterStarRewardStatus(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetRiftLevelFinished(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitOpenedRiftChapters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeOpenedRiftChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChangeLastOpenedRiftChapter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo GetLastOpenedRiftChapterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnRiftLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event ProjectLPlayerContext.RiftLevelBattleFinishedAckCallback EventOnRiftLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward, List<Goods>> EventOnRiftLevelRaidAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BattleReward> EventOnRiftChapterRewardGainAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendSelectCardReq(int cardPoolId, bool isSingleSlect, bool isUsingTicket)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSSelectCardNtf msg)
    {
      this.m_selectCardComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(SelectCardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>, List<Goods>> EventOnCardSelectAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool[] GetSelectRewardsIsFirstGetArray()
    {
      return this.m_rewardFlags;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardFlags(List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, bool> GetIsSelectHeroRewardTransferToFragmentDict()
    {
      return this.m_isSelectHeroRewardTransferToFragmentDict;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroRewardToFragmentDict(List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public CardPool GetCardPoolById(int id)
    {
      return this.m_selectCardComponent.GetCardPoolById(id, true);
    }

    public List<CardPool> GetActivityCardPoolList()
    {
      return this.m_selectCardComponent.GetActivityCardPool();
    }

    public string GetActivityTimeByPoolId(int id)
    {
      return this.m_selectCardComponent.GetActivityTimeByPoolId(id);
    }

    public OperationalActivityBase FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      return this.m_selectCardComponent.FindOperationalActivityByActivityCardPoolId(activityCardPoolId);
    }

    public int IsActivityCardPoolOnActivityTime(int activityCardPoolId)
    {
      return this.m_selectCardComponent.IsActivityCardPoolOnActivityTime(activityCardPoolId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActivityCardPoolLastDays(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanSelectCard(int cardPoolId, bool isSingleSelect, bool isUsingTickets = true)
    {
      return this.m_selectCardComponent.CanSelectCard(cardPoolId, isSingleSelect, isUsingTickets);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendEverydaySignReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(EverydaySignAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanSignToday()
    {
      return this.m_playerBasicInfoComponent.CanSignToday();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSignDaysMonth()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<List<Goods>> GetMonthRewardList(int month)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetSignRewardMonthList(int month)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool TodayIsSigned()
    {
      return this.m_playerBasicInfoComponent.IsSigned();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods GetTodaySignReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods GetOneDaySignReward(int month, int day)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<Goods>> EventOnEverydaySignAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendOpenSurveyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendGetSurveyRewardReq()
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSSurveyNtf msg)
    {
      this.m_surveyComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(NewSurveyNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UpdateSurveyNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(GetSurveyRewardAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(OpenSurveyAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public SurveyStatus GetCurrentSurveyStatus()
    {
      return this.m_surveyComponent.GetCurrentSurveyStatus();
    }

    public event Action<int> EventOnOpenSurveyAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetSurveyRewardAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CanGainSurveyReward()
    {
      return this.m_surveyComponent.CanGainSurveyReward();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomCreateReq(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomViewReq(
      GameFunctionType gameFunctionTypeId,
      int chapterId,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomAutoMatchReq(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomAutoMatchCancelReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomAuthorityChangeReq(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomJoinReq(
      int roomId,
      GameFunctionType gameFunctionTypeId,
      int locationId,
      ulong inviterSessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomQuitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomGetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomInviteReq(List<string> inviteeUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomInvitationRefusedReq(
      ulong inviterSessionId,
      int inviterChannelId,
      int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomInviteeInfoGetReq(
      List<string> inviteeUserIds,
      TeamRoomInviteeInfoType infoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTeamRoomPlayerPositionChangeReq(List<ProTeamRoomPlayerPositionInfo> positionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSTeamNtf msg)
    {
      this.m_teamComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomCreateAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomViewAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAutoMatchAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAutoMatchCancelAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAuthorityChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomJoinAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomQuitAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInvitationRefusedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteeInfoGetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerPositionChangeAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerJoinNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerQuitNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerLeaveWaitingListAndJoinRoomNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAuthorityChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomAutoMatchInfoNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInvitationRefusedNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomSelfKickOutNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomInviteeLevelInfoNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TeamRoomPlayerPositionChangeNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public TeamRoom GetTeamRoom()
    {
      return this.m_teamComponent.Room;
    }

    public bool IsInTeamRoom()
    {
      return this.m_teamComponent.IsInRoom();
    }

    public bool IsInTeamRoomAutoMatchWaitingList()
    {
      return this.m_teamComponent.IsInWaitingList();
    }

    public List<TeamRoomInviteInfo> GetTeamRoomInviteInfos()
    {
      return this.m_teamComponent.GetInviteInfos();
    }

    public void RemoveTeamRoomAInviteInfo(ulong sessionId, int roomId)
    {
      this.m_teamComponent.ClearAInviteInfo(sessionId, roomId);
    }

    public int CanUnlockTeamBattle(GameFunctionType gameFunctionType, int locationId)
    {
      return this.m_teamComponent.IsLevelUnlocked(gameFunctionType, locationId);
    }

    public void SetTeamRoomInviteAgain(bool on)
    {
      this.m_teamComponent.IsTeamRoomInviteAgain = on;
    }

    public bool IsTeamRoomInviteAgain()
    {
      return this.m_teamComponent.IsTeamRoomInviteAgain;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateTeam(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanViewTeamRoom(GameFunctionType gameFunctionTypeId, int chapterId, int locationId)
    {
      return this.m_teamComponent.CanViewTeamRoom(gameFunctionTypeId, chapterId, locationId);
    }

    public int CanJoinTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      return this.m_teamComponent.CanJoinTeamRoom(gameFunctionTypeId, locationId);
    }

    public int CanAutoMatchTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      return this.m_teamComponent.CanAutoMatchTeamRoom(gameFunctionTypeId, locationId);
    }

    public int CanCancelAutoMatchTeamRoom()
    {
      return this.m_teamComponent.CanCancelAutoMatchTeamRoom();
    }

    public event Action<int> EventOnTeamRoomCreateAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, List<TeamRoom>> EventOnTeamRoomViewAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, bool> EventOnTeamRoomAutoMatchAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomAutoMatchCancelAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomAuthorityChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomJoinAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomQuitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnTeamRoomGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomInviteAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomInvitationRefusedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomInviteeInfoGetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayer> EventOnTeamRoomPlayerJoinNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayer> EventOnTeamRoomPlayerQuitNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomAuthority> EventOnTeamRoomAuthorityChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomAutoMatchInfoNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomInviteInfo> EventOnTeamRoomInviteNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string> EventOnTeamRoomInvitationRefusedNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomSelfKickOutNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int, int> EventOnTeamRoomInviteeInfoNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTeamRoomPlayerPositionChangeAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTeamRoomPlayerPositionChangeNtf
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendThearchyTrialLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendThearchyTrialLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSThearchyTrialNtf msg)
    {
      this.m_thearchyTrialComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ThearchyTrialLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(ThearchyTrialLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThearchyOpened(int thearchyId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThearchyLevelOpened(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackThearchyLevel(int levelId)
    {
      return this.m_thearchyTrialComponent.CanAttackThearchyTrialLevel(levelId, false);
    }

    public int GetThearchyTicketCount()
    {
      return this.m_thearchyTrialComponent.GetCurrentTicketNums();
    }

    public bool IsThearchyTrialLevelFinished(int levelId)
    {
      return this.m_thearchyTrialComponent.IsLevelFinished(levelId);
    }

    public int GetThearchyTrialMaxFinishedLevelId(int chapterId)
    {
      return this.m_thearchyTrialComponent.GetMaxFinishedLevelId(chapterId);
    }

    public int GetThearchyDailyRewardRestCount()
    {
      return this.m_thearchyTrialComponent.GetDailyChallengNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetThearchyDailyRewardCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsThearchyLevelBlessing(int levelId)
    {
      return this.m_thearchyTrialComponent.IsBlessing(levelId);
    }

    public event Action<int> EventOnThearchyTrialLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnThearchyTrialLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTechLevelupReq(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(DSTrainingGroundNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TrainingGroundTechLevelupAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public TrainingRoom GetTrainingRoomById(int roomId)
    {
      return this.m_trainingGroundComponent.GetRoom(roomId);
    }

    public int CanTechLevelup(int techId)
    {
      return this.m_trainingGroundComponent.CheckTechLevelup(techId, 1);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanTrainingCourseLevelup(TrainingCourse course)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanTrainingRoomLevelup(TrainingRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<TrainingTech> GetTrainingGroundAvailableTechs()
    {
      return this.m_trainingGroundComponent.GetAvailableTechs();
    }

    public TrainingTech GetTrainingTechById(int techId)
    {
      return this.m_trainingGroundComponent.GetTech(techId);
    }

    public int GetTechMaxLevel(int TechId)
    {
      return this.m_trainingGroundComponent.GetTechMaxLevel(TechId);
    }

    public bool IsTechLocked(int TechId)
    {
      return this.m_trainingGroundComponent.IsTechLocked(TechId);
    }

    public bool CanLevelup(int TechId, int DeltaLevel = 1)
    {
      return this.m_trainingGroundComponent.CanLevelup(TechId, DeltaLevel);
    }

    public TrainingTechResourceRequirements GetResourceRequirementsByLevel(
      int TechId,
      int Level)
    {
      return this.m_trainingGroundComponent.GetResourceRequirementsByLevel(TechId, Level);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTrainingCourseIdByTechId(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTrainingRoomIdByTechId(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGoToGetSoldierTech(int getSoliderTechId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnTrainingGroundTechLevelupAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTreasureLevelAttackReq(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendTreasureLevelBattleFinishedReq(int levelId, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSTreasureMapNtf msg)
    {
      this.m_treasureMapComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TreasureLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(TreasureLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTreasureLevelOpened(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CanAttackTreasureLevel(int levelID)
    {
      return this.m_treasureMapComponent.CanAttackTreasureLevel(levelID);
    }

    public int GetTreasureTicketCount()
    {
      return this.m_treasureMapComponent.GetCurrentTicketNums();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTreasureTicketCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsTreasureLevelFinished(int levelID)
    {
      return this.m_treasureMapComponent.IsLevelFinished(levelID);
    }

    public int GetTreasureMapMaxFinishedLevelId()
    {
      return this.m_treasureMapComponent.GetMaxFinishedLevelId();
    }

    public event Action<int> EventOnTreasureLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnTreasureLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreChallengeLevelAttackReq(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreChallengeLevelBattleFinishedReq(
      int unchartedScoreId,
      int levelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreScoreLevelAttackReq(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUnchartedScoreScoreLevelBattleFinishedReq(
      int unchartedScoreId,
      int levelId,
      ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnMessage(DSUnchartedScoreNtf msg)
    {
      this.m_unchartedScoreComponent.DeSerialize(msg);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreChallengeLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreChallengeLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreScoreLevelAttackAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UnchartedScoreScoreLevelBattleFinishedAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsUnchartedScoreOpened(int unchartedScoreId)
    {
      return this.m_unchartedScoreComponent.IsUnchartedScoreOpened(unchartedScoreId);
    }

    public bool IsUnchartedScoreDisplay(int unchartedScoreId)
    {
      return this.m_unchartedScoreComponent.IsUnchartedScoreDisplay(unchartedScoreId);
    }

    public void GetUnchartedScoreOpenTime(
      int unchartedScoreId,
      out DateTime startTime,
      out DateTime endTime)
    {
      this.m_unchartedScoreComponent.GetUnchartedScoreOpenTime(unchartedScoreId, out startTime, out endTime);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreLevelTimeUnlock(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnchartedScoreLevelPlayerLevelVaild(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsUnchartedChallengePrevLevelComplete(int unchartedScoreId, int levelId)
    {
      return this.m_unchartedScoreComponent.IsChallengePrevLevelComplete(unchartedScoreId, levelId);
    }

    public int GetUnchartedChallengeLevelTimeUnlockDay(int unchartedScoreId, int levelId)
    {
      return this.m_unchartedScoreComponent.GetChallengeLevelTimeUnlockDay(unchartedScoreId, levelId);
    }

    public int CanAttackUnchartedScoreLevel(int unchartedScoreId, int levelId)
    {
      return this.m_unchartedScoreComponent.CanAttackScoreLevel(unchartedScoreId, levelId, false);
    }

    public int CanAttackUnchartedChallengeLevel(int unchartedScoreId, int levelId)
    {
      return this.m_unchartedScoreComponent.CanAttackChallengeLevel(unchartedScoreId, levelId);
    }

    public bool IsUnchartedScoreLevelFinished(int unchartedScoreId, int levelId)
    {
      return this.m_unchartedScoreComponent.IsScoreLevelComplete(unchartedScoreId, levelId);
    }

    public bool IsUnchartedChallengeLevelFinished(int unchartedScoreId, int levelId)
    {
      return this.m_unchartedScoreComponent.IsChallengeLevelComplete(unchartedScoreId, levelId);
    }

    public int GetUnchartedScoreMaxFinishedScoreLevelId(int unchartedScoreId)
    {
      return this.m_unchartedScoreComponent.GetMaxFinishedScoreLevelId(unchartedScoreId);
    }

    public int GetUnchartedScoreMaxFinishedChallengeLevelId(int unchartedScoreId)
    {
      return this.m_unchartedScoreComponent.GetMaxFinishedChallengeLevelId(unchartedScoreId);
    }

    public bool IsUnchartedScoreRewardGot(int unchartedScoreId, int unchartedScoreRewardGroupId)
    {
      return this.m_unchartedScoreComponent.IsRewardGot(unchartedScoreId, unchartedScoreRewardGroupId);
    }

    public int GetUnchartedScoreScore(int unchartedScoreId)
    {
      return this.m_unchartedScoreComponent.GetScore(unchartedScoreId);
    }

    public int GetUnchartedScoreDailyRewardRestCount(int unchartedScoreId)
    {
      return this.m_unchartedScoreComponent.GetDailyRewardRestCount(unchartedScoreId);
    }

    public event Action<int> EventOnUnchartedScoreChallengeLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnUnchartedScoreChallengeLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnUnchartedScoreScoreLevelAttackAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool, BattleReward> EventOnUnchartedScoreScoreLevelBattleFinishedAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUserGuideSetReq(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUserGuideSetReq(int[] steps)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendUserGuideClearReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessage(UserGuideSetAck msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsGameFunctionOpened(GameFunctionType gameFunctionType)
    {
      return this.m_playerBasicInfoComponent.IsGameFunctionOpened(gameFunctionType);
    }

    public bool IsUserGuideComplete(int step)
    {
      return this.m_playerBasicInfoComponent.IsUserGuideCompleted(step);
    }

    public bool IsRiftChapterEverOpened(int riftChapterId)
    {
      return this.IsUserGuideComplete(100 + riftChapterId);
    }

    public event Action<int> EventOnUserGuideSetAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void AncientCallBossBattleFinishedAckCallback(
      int result,
      bool isWin,
      BattleReward reward);

    public class CollectionActivityState
    {
      public List<CollectionEvent> m_activeWaypointEvents = new List<CollectionEvent>();
      public ConfigDataCollectionActivityScenarioLevelInfo m_activeNextScenarioLevelInfo;
    }

    public delegate void HeroAnthemLevelBattleFinishedAckCallback(
      int result,
      bool isWin,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);

    public delegate void HeroDungeonLevelBattleFinishedAckCallback(
      int result,
      int stars,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);

    public delegate void HeroPhantomBattleFinishedAckCallback(
      int result,
      bool isWin,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);

    public class CurrentWaypointEvent
    {
      public int WaypointId;
      public ConfigDataEventInfo EventInfo;
      public RandomEvent RandomEvent;
    }

    public delegate void RiftLevelBattleFinishedAckCallback(
      int result,
      int stars,
      BattleReward reward,
      bool isFirstWin,
      List<int> achievements);
  }
}
