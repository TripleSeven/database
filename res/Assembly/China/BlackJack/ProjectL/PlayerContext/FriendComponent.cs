﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.FriendComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class FriendComponent : FriendComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_friendDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSFriendNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadFromPBData(DSFriendNtf pbFriendInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(FriendInfoUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHero> GetBusinessCardHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    public BusinessCardInfoSet GetBusinessCardInfoSet()
    {
      return this.m_friendDS.BusinessCardSetInfo;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSendFriendShipPointsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetClaimedFriendShipPointsCount()
    {
      return this.m_friendDS.FriendshipPointsClaimedToday;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReceivedFriendShipPointsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UserSummary> Friends { get; set; }

    public List<UserSummary> Blacklist { get; set; }

    public List<UserSummary> Invite { get; set; }

    public List<UserSummary> Invited { get; set; }

    public List<UserSummary> GuildPlayers { get; set; }

    public List<UserSummary> RecentContactsChat { get; set; }

    public List<UserSummary> RecentContactsTeamBattle { get; set; }

    public List<PVPInviteInfo> PVPInviteInfos { get; set; }
  }
}
