﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.Happening
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [HotFix]
  public class Happening
  {
    public HappeningStep Step;
    public ConfigDataScenarioInfo ScenarioInfo;
    public ConfigDataEventInfo EventInfo;
    public ConfigDataRiftLevelInfo RiftLevelInfo;
    public ConfigDataHeroDungeonLevelInfo HeroDungeonLevelInfo;
    public ConfigDataHeroPhantomLevelInfo HeroPhantomLevelInfo;
    public ConfigDataAnikiLevelInfo AnikiLevelInfo;
    public ConfigDataThearchyTrialLevelInfo ThearchyLevelInfo;
    public ConfigDataTreasureLevelInfo TreasureLevelInfo;
    public ConfigDataMemoryCorridorLevelInfo MemoryCorridorLevelInfo;
    public ConfigDataHeroTrainningLevelInfo HeroTrainningLevelInfo;
    public ConfigDataCooperateBattleLevelInfo CooperateBattleLevelInfo;
    public ConfigDataScoreLevelInfo UnchartedScoreLevelInfo;
    public ConfigDataChallengeLevelInfo UnchartedChallengeLevelInfo;
    public ConfigDataTowerFloorInfo TowerFloorInfo;
    public ConfigDataEternalShrineLevelInfo EternalShrineLevelInfo;
    public ConfigDataHeroAnthemLevelInfo HeroAnthemLevelInfo;
    public ConfigDataAncientCallBossInfo AncientCallBossLevelInfo;
    public ConfigDataCollectionActivityScenarioLevelInfo CollectionActivityScenarioLevelInfo;
    public ConfigDataCollectionActivityChallengeLevelInfo CollectionActivityChallengeLevelInfo;
    public ConfigDataCollectionActivityLootLevelInfo CollectionActivityLootLevelInfo;
    public ConfigDataCollectionEventInfo CollectionActivityEventInfo;
    public ConfigDataGuildMassiveCombatLevelInfo GuildMassiveCombatLevelInfo;
    public ulong GuildMassiveCombatInstanceId;
    public ConfigDataWaypointInfo WaypointInfo;
    public ConfigDataWaypointInfo PrevWaypointInfo;
    public ConfigDataDialogInfo DialogInfoBefore;
    public ConfigDataDialogInfo DialogInfoAfter;
    public ConfigDataBattleInfo BattleInfo;
    public int MonsterLevel;
    public BattleType BattleType;
    public Action OnStepEnd;
    [DoNotToLua]
    private Happening.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitBattleAndDialogBattleTypeConfigDataBattleInfoInt32ConfigDataDialogInfoConfigDataDialogInfo_hotfix;
    private LuaFunction m_NextStep_hotfix;
    private LuaFunction m_StepEnd_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_IsLevelInfoNull_hotfix;
    private LuaFunction m_GetGameFunctionTypeAndLocationIdGameFunctionType_Int32__hotfix;
    private LuaFunction m_GetStrategy_hotfix;
    private LuaFunction m_GetLevelId_hotfix;
    private LuaFunction m_GetStarConditionInt32_Int32__hotfix;
    private LuaFunction m_GetBattleLevelAchievements_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public Happening()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleAndDialog(
      BattleType battleType,
      ConfigDataBattleInfo battleInfo,
      int monsterLevel,
      ConfigDataDialogInfo dialogBefore = null,
      ConfigDataDialogInfo dialogAfter = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StepEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLevelInfoNull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetGameFunctionTypeAndLocationId(
      out GameFunctionType gameFunctionType,
      out int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetStrategy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetStarCondition(out int starTurnMax, out int starDeadMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleLevelAchievement[] GetBattleLevelAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public Happening.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private Happening m_owner;

      public LuaExportHelper(Happening owner)
      {
        this.m_owner = owner;
      }

      public void StepEnd()
      {
        this.m_owner.StepEnd();
      }

      public bool IsLevelInfoNull()
      {
        return this.m_owner.IsLevelInfoNull();
      }
    }
  }
}
