﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.HeroComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  [CustomLuaClass]
  public class HeroComponent : HeroComponentCommon
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void PostInit()
    {
      base.PostInit();
    }

    public override void DeInit()
    {
      base.DeInit();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSHeroNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InteractHero(int heroId, int addFavorabilityExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo GetHeroInteractionInfo(
      int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroInteractHeroPerformanceId(int heroId, HeroInteractionResultType resultType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroBiography(int biographyId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort GetDSVersion()
    {
      return this.m_heroDS.ClientVersion;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetWearedEquipmentHeroIdByEquipmentId(ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetHeroInteractNums()
    {
      return this.m_heroDS.HeroInteractNums;
    }

    public DateTime GetHeroInteractNumsFlushTime()
    {
      return this.m_heroDS.HeroInteractNumsFlushTime;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ImitateUseHeroFavorabilityExpItem(
      int heroId,
      int itemId,
      int nums,
      GoodsType goodsType = GoodsType.GoodsType_Item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AutoEquipment(int heroId, List<ulong> equipmentInstanceIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasIgnoreCostEquipment(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
