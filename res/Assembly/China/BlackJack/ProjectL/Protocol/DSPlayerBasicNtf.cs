﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSPlayerBasicNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSPlayerBasicNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class DSPlayerBasicNtf : IExtensible
  {
    private uint _Version;
    private int _PlayerLevel;
    private long _RechargeCsystal;
    private int _ExpCurr;
    private int _Energy;
    private long _Gold;
    private long _Crystal;
    private long _LastSignTime;
    private int _SignDays;
    private int _HeadIcon;
    private int _BuyEngryNums;
    private long _PlayerActionNextFlushTime;
    private readonly List<int> _GuideCompleteFlags;
    private int _ArenaTickets;
    private int _ArenaHonour;
    private int _BuyArenaTicketsNums;
    private long _EnergyFlushTime;
    private long _CreateTime;
    private int _FriendshipPoints;
    private bool _OpenGameRating;
    private long _RechargeRMB;
    private int _SkinTickets;
    private int _RealTimePVPHonor;
    private int _MemoryEssence;
    private int _MithralStone;
    private int _BrillianceMithralStone;
    private int _VipLevel;
    private bool _MemoryStoreOpen;
    private int _GuildMedal;
    private long _RefluxBeginTime;
    private int _ChallengePoint;
    private int _FashionPoint;
    private int _BuyJettonNums;
    private readonly List<int> _RewardedDialogIds;
    private int _Title;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSPlayerBasicNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerLevel")]
    public int PlayerLevel
    {
      get
      {
        return this._PlayerLevel;
      }
      set
      {
        this._PlayerLevel = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RechargeCsystal")]
    public long RechargeCsystal
    {
      get
      {
        return this._RechargeCsystal;
      }
      set
      {
        this._RechargeCsystal = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExpCurr")]
    public int ExpCurr
    {
      get
      {
        return this._ExpCurr;
      }
      set
      {
        this._ExpCurr = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Energy")]
    public int Energy
    {
      get
      {
        return this._Energy;
      }
      set
      {
        this._Energy = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Gold")]
    public long Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Crystal")]
    public long Crystal
    {
      get
      {
        return this._Crystal;
      }
      set
      {
        this._Crystal = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastSignTime")]
    public long LastSignTime
    {
      get
      {
        return this._LastSignTime;
      }
      set
      {
        this._LastSignTime = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SignDays")]
    public int SignDays
    {
      get
      {
        return this._SignDays;
      }
      set
      {
        this._SignDays = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      get
      {
        return this._HeadIcon;
      }
      set
      {
        this._HeadIcon = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyEngryNums")]
    public int BuyEngryNums
    {
      get
      {
        return this._BuyEngryNums;
      }
      set
      {
        this._BuyEngryNums = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerActionNextFlushTime")]
    public long PlayerActionNextFlushTime
    {
      get
      {
        return this._PlayerActionNextFlushTime;
      }
      set
      {
        this._PlayerActionNextFlushTime = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "GuideCompleteFlags")]
    public List<int> GuideCompleteFlags
    {
      get
      {
        return this._GuideCompleteFlags;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaTickets")]
    public int ArenaTickets
    {
      get
      {
        return this._ArenaTickets;
      }
      set
      {
        this._ArenaTickets = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaHonour")]
    public int ArenaHonour
    {
      get
      {
        return this._ArenaHonour;
      }
      set
      {
        this._ArenaHonour = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyArenaTicketsNums")]
    public int BuyArenaTicketsNums
    {
      get
      {
        return this._BuyArenaTicketsNums;
      }
      set
      {
        this._BuyArenaTicketsNums = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFlushTime")]
    public long EnergyFlushTime
    {
      get
      {
        return this._EnergyFlushTime;
      }
      set
      {
        this._EnergyFlushTime = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CreateTime")]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPoints")]
    public int FriendshipPoints
    {
      get
      {
        return this._FriendshipPoints;
      }
      set
      {
        this._FriendshipPoints = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenGameRating")]
    public bool OpenGameRating
    {
      get
      {
        return this._OpenGameRating;
      }
      set
      {
        this._OpenGameRating = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RechargeRMB")]
    public long RechargeRMB
    {
      get
      {
        return this._RechargeRMB;
      }
      set
      {
        this._RechargeRMB = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkinTickets")]
    public int SkinTickets
    {
      get
      {
        return this._SkinTickets;
      }
      set
      {
        this._SkinTickets = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RealTimePVPHonor")]
    public int RealTimePVPHonor
    {
      get
      {
        return this._RealTimePVPHonor;
      }
      set
      {
        this._RealTimePVPHonor = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MemoryEssence")]
    public int MemoryEssence
    {
      get
      {
        return this._MemoryEssence;
      }
      set
      {
        this._MemoryEssence = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MithralStone")]
    public int MithralStone
    {
      get
      {
        return this._MithralStone;
      }
      set
      {
        this._MithralStone = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BrillianceMithralStone")]
    public int BrillianceMithralStone
    {
      get
      {
        return this._BrillianceMithralStone;
      }
      set
      {
        this._BrillianceMithralStone = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "VipLevel")]
    public int VipLevel
    {
      get
      {
        return this._VipLevel;
      }
      set
      {
        this._VipLevel = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.Default, IsRequired = true, Name = "MemoryStoreOpen")]
    public bool MemoryStoreOpen
    {
      get
      {
        return this._MemoryStoreOpen;
      }
      set
      {
        this._MemoryStoreOpen = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuildMedal")]
    public int GuildMedal
    {
      get
      {
        return this._GuildMedal;
      }
      set
      {
        this._GuildMedal = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RefluxBeginTime")]
    public long RefluxBeginTime
    {
      get
      {
        return this._RefluxBeginTime;
      }
      set
      {
        this._RefluxBeginTime = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChallengePoint")]
    public int ChallengePoint
    {
      get
      {
        return this._ChallengePoint;
      }
      set
      {
        this._ChallengePoint = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FashionPoint")]
    public int FashionPoint
    {
      get
      {
        return this._FashionPoint;
      }
      set
      {
        this._FashionPoint = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyJettonNums")]
    public int BuyJettonNums
    {
      get
      {
        return this._BuyJettonNums;
      }
      set
      {
        this._BuyJettonNums = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, Name = "RewardedDialogIds")]
    public List<int> RewardedDialogIds
    {
      get
      {
        return this._RewardedDialogIds;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Title")]
    public int Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
