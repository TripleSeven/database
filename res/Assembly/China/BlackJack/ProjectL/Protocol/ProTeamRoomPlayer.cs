﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProTeamRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProTeamRoomPlayer")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProTeamRoomPlayer : IExtensible
  {
    private string _UserId;
    private int _ChannelId;
    private string _Name;
    private int _HeadIcon;
    private int _ActiveHeroJobRelatedId;
    private int _Level;
    private int _Position;
    private ulong _SessionId;
    private int _ModenSkinId;
    private bool _Blessing;
    private int _Title;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoomPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "UserId")]
    public string UserId
    {
      get
      {
        return this._UserId;
      }
      set
      {
        this._UserId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      get
      {
        return this._ChannelId;
      }
      set
      {
        this._ChannelId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      get
      {
        return this._HeadIcon;
      }
      set
      {
        this._HeadIcon = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActiveHeroJobRelatedId")]
    public int ActiveHeroJobRelatedId
    {
      get
      {
        return this._ActiveHeroJobRelatedId;
      }
      set
      {
        this._ActiveHeroJobRelatedId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Position")]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SessionId")]
    public ulong SessionId
    {
      get
      {
        return this._SessionId;
      }
      set
      {
        this._SessionId = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModenSkinId")]
    public int ModenSkinId
    {
      get
      {
        return this._ModenSkinId;
      }
      set
      {
        this._ModenSkinId = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Blessing")]
    public bool Blessing
    {
      get
      {
        return this._Blessing;
      }
      set
      {
        this._Blessing = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Title")]
    public int Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
