﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRankingPlayerAncientCallBossInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProRankingPlayerAncientCallBossInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProRankingPlayerAncientCallBossInfo : IExtensible
  {
    private int _BossId;
    private readonly List<int> _TeamList;
    private int _TeamListBattlePower;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRankingPlayerAncientCallBossInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BossId")]
    public int BossId
    {
      get
      {
        return this._BossId;
      }
      set
      {
        this._BossId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "TeamList")]
    public List<int> TeamList
    {
      get
      {
        return this._TeamList;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TeamListBattlePower")]
    public int TeamListBattlePower
    {
      get
      {
        return this._TeamListBattlePower;
      }
      set
      {
        this._TeamListBattlePower = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
