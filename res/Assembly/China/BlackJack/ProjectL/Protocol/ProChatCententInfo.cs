﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProChatCententInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProChatCententInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProChatCententInfo : IExtensible
  {
    private ProChatContentText _ChatContentText;
    private readonly List<ProGoodsWithInstanceId> _GoodWithInstanceIdList;
    private ProPeakArenaBattleReportShareInfo _PeakArenaBattleReportInfo;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatCententInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatContentText")]
    [DefaultValue(null)]
    public ProChatContentText ChatContentText
    {
      get
      {
        return this._ChatContentText;
      }
      set
      {
        this._ChatContentText = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "GoodWithInstanceIdList")]
    public List<ProGoodsWithInstanceId> GoodWithInstanceIdList
    {
      get
      {
        return this._GoodWithInstanceIdList;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "PeakArenaBattleReportInfo")]
    public ProPeakArenaBattleReportShareInfo PeakArenaBattleReportInfo
    {
      get
      {
        return this._PeakArenaBattleReportInfo;
      }
      set
      {
        this._PeakArenaBattleReportInfo = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
