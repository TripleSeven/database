﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.CollectionLevelFinishAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "CollectionLevelFinishAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class CollectionLevelFinishAck : IExtensible
  {
    private int _Result;
    private int _GameFunctionTypeId;
    private int _LevelId;
    private bool _Win;
    private ProChangedGoodsNtf _Ntf;
    private readonly List<ProGoods> _NormalRewards;
    private readonly List<ProGoods> _AdditionalRewards;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionLevelFinishAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      get
      {
        return this._GameFunctionTypeId;
      }
      set
      {
        this._GameFunctionTypeId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelId")]
    public int LevelId
    {
      get
      {
        return this._LevelId;
      }
      set
      {
        this._LevelId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Win")]
    public bool Win
    {
      get
      {
        return this._Win;
      }
      set
      {
        this._Win = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    [DefaultValue(null)]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "NormalRewards")]
    public List<ProGoods> NormalRewards
    {
      get
      {
        return this._NormalRewards;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "AdditionalRewards")]
    public List<ProGoods> AdditionalRewards
    {
      get
      {
        return this._AdditionalRewards;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
