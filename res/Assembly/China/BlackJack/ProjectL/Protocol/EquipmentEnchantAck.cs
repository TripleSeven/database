﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.EquipmentEnchantAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "EquipmentEnchantAck")]
  [Serializable]
  public class EquipmentEnchantAck : IExtensible
  {
    private int _Result;
    private ulong _EquipmentInstanceId;
    private ulong _EnchantStoneInstanceId;
    private int _NewResonanceId;
    private readonly List<ProCommonBattleProperty> _NewEnchantProperties;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentEnchantAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EquipmentInstanceId")]
    public ulong EquipmentInstanceId
    {
      get
      {
        return this._EquipmentInstanceId;
      }
      set
      {
        this._EquipmentInstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnchantStoneInstanceId")]
    public ulong EnchantStoneInstanceId
    {
      get
      {
        return this._EnchantStoneInstanceId;
      }
      set
      {
        this._EnchantStoneInstanceId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NewResonanceId")]
    public int NewResonanceId
    {
      get
      {
        return this._NewResonanceId;
      }
      set
      {
        this._NewResonanceId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "NewEnchantProperties")]
    public List<ProCommonBattleProperty> NewEnchantProperties
    {
      get
      {
        return this._NewEnchantProperties;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
