﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProHeroPhantomLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProHeroPhantomLevel")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProHeroPhantomLevel : IExtensible
  {
    private int _ConfigId;
    private long _FirstClear;
    private readonly List<int> _AchievementsFinished;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHeroPhantomLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConfigId")]
    public int ConfigId
    {
      get
      {
        return this._ConfigId;
      }
      set
      {
        this._ConfigId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstClear")]
    public long FirstClear
    {
      get
      {
        return this._FirstClear;
      }
      set
      {
        this._FirstClear = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "AchievementsFinished")]
    public List<int> AchievementsFinished
    {
      get
      {
        return this._AchievementsFinished;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
