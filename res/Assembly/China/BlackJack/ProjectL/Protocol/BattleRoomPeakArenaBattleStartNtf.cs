﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomPeakArenaBattleStartNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomPeakArenaBattleStartNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class BattleRoomPeakArenaBattleStartNtf : IExtensible
  {
    private readonly List<ProBattleHeroSetupInfo> _All;
    private long _TurnActionTimeSpan;
    private readonly List<long> _PublicTimeSpan;
    private readonly List<ProBattleRoomPlayerBattleInfoInitInfo> _InitInfos;
    private long _CreateTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPeakArenaBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, Name = "All")]
    public List<ProBattleHeroSetupInfo> All
    {
      get
      {
        return this._All;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TurnActionTimeSpan")]
    public long TurnActionTimeSpan
    {
      get
      {
        return this._TurnActionTimeSpan;
      }
      set
      {
        this._TurnActionTimeSpan = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "PublicTimeSpan")]
    public List<long> PublicTimeSpan
    {
      get
      {
        return this._PublicTimeSpan;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "InitInfos")]
    public List<ProBattleRoomPlayerBattleInfoInitInfo> InitInfos
    {
      get
      {
        return this._InitInfos;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CreateTime")]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
