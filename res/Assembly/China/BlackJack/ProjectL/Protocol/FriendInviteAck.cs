﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FriendInviteAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "FriendInviteAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class FriendInviteAck : IExtensible
  {
    private int _Result;
    private FriendInviteReq _Req;
    private readonly List<ProUserSummary> _InvitedUsers;
    private ProUserSummary _FailedUser;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInviteAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Req")]
    public FriendInviteReq Req
    {
      get
      {
        return this._Req;
      }
      set
      {
        this._Req = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "InvitedUsers")]
    public List<ProUserSummary> InvitedUsers
    {
      get
      {
        return this._InvitedUsers;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "FailedUser")]
    public ProUserSummary FailedUser
    {
      get
      {
        return this._FailedUser;
      }
      set
      {
        this._FailedUser = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
