﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ChatMessageSendReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ChatMessageSendReq")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ChatMessageSendReq : IExtensible
  {
    private int _ChannelId;
    private ProChatInfo _ChatInfo;
    private ProChatCententInfo _ChatContentInfo;
    private ProChatContentVoice _VoiceInfo;
    private ProChatEnterRoomInfo _EnterRoomInfo;
    private string _DestGameUserId;
    private string _ChatGroupId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageSendReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      get
      {
        return this._ChannelId;
      }
      set
      {
        this._ChannelId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatInfo")]
    public ProChatInfo ChatInfo
    {
      get
      {
        return this._ChatInfo;
      }
      set
      {
        this._ChatInfo = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatContentInfo")]
    public ProChatCententInfo ChatContentInfo
    {
      get
      {
        return this._ChatContentInfo;
      }
      set
      {
        this._ChatContentInfo = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceInfo")]
    [DefaultValue(null)]
    public ProChatContentVoice VoiceInfo
    {
      get
      {
        return this._VoiceInfo;
      }
      set
      {
        this._VoiceInfo = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "EnterRoomInfo")]
    [DefaultValue(null)]
    public ProChatEnterRoomInfo EnterRoomInfo
    {
      get
      {
        return this._EnterRoomInfo;
      }
      set
      {
        this._EnterRoomInfo = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "DestGameUserId")]
    [DefaultValue("")]
    public string DestGameUserId
    {
      get
      {
        return this._DestGameUserId;
      }
      set
      {
        this._DestGameUserId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatGroupId")]
    [DefaultValue("")]
    public string ChatGroupId
    {
      get
      {
        return this._ChatGroupId;
      }
      set
      {
        this._ChatGroupId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
