﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.TeamRoomInviteeLevelInfoNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "TeamRoomInviteeLevelInfoNtf")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class TeamRoomInviteeLevelInfoNtf : IExtensible
  {
    private string _InviteeUserId;
    private int _LevelUnlocked;
    private int _GuildMassiveCombatAvailable;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteeLevelInfoNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "InviteeUserId")]
    public string InviteeUserId
    {
      get
      {
        return this._InviteeUserId;
      }
      set
      {
        this._InviteeUserId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelUnlocked")]
    public int LevelUnlocked
    {
      get
      {
        return this._LevelUnlocked;
      }
      set
      {
        this._LevelUnlocked = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuildMassiveCombatAvailable")]
    public int GuildMassiveCombatAvailable
    {
      get
      {
        return this._GuildMassiveCombatAvailable;
      }
      set
      {
        this._GuildMassiveCombatAvailable = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
