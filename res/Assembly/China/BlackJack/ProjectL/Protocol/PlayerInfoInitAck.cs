﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PlayerInfoInitAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PlayerInfoInitAck")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class PlayerInfoInitAck : IExtensible
  {
    private int _Result;
    private long _ServerCurrTime;
    private bool _NeedCreateCharactor;
    private string _CharactorNickName;
    private string _GameUserId;
    private bool _IsNeedBattleReportLog;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoInitAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ServerCurrTime")]
    public long ServerCurrTime
    {
      get
      {
        return this._ServerCurrTime;
      }
      set
      {
        this._ServerCurrTime = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "NeedCreateCharactor")]
    public bool NeedCreateCharactor
    {
      get
      {
        return this._NeedCreateCharactor;
      }
      set
      {
        this._NeedCreateCharactor = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "CharactorNickName")]
    [DefaultValue("")]
    public string CharactorNickName
    {
      get
      {
        return this._CharactorNickName;
      }
      set
      {
        this._CharactorNickName = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "GameUserId")]
    public string GameUserId
    {
      get
      {
        return this._GameUserId;
      }
      set
      {
        this._GameUserId = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "IsNeedBattleReportLog")]
    [DefaultValue(false)]
    public bool IsNeedBattleReportLog
    {
      get
      {
        return this._IsNeedBattleReportLog;
      }
      set
      {
        this._IsNeedBattleReportLog = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
