﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProArenaBattleReport")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProArenaBattleReport : IExtensible
  {
    private int _Version;
    private ulong _InstanceId;
    private int _BattleType;
    private int _BattleId;
    private int _RandomSeed;
    private readonly List<ProBattleCommand> _Commands;
    private int _Status;
    private int _ArenaDefenderRuleId;
    private string _DefenderUserId;
    private string _DefenderName;
    private int _DefenderLevel;
    private readonly List<ProBattleHero> _DefenderHeroes;
    private string _AttackerUserId;
    private string _AttackerName;
    private int _AttackerLevel;
    private readonly List<ProBattleHero> _AttackerHeroes;
    private int _AttackerGotArenaPoints;
    private int _DefenderGotArenaPoints;
    private long _CreateTime;
    private int _OpponentHeadIcon;
    private readonly List<ProTrainingTech> _DefenderTechs;
    private readonly List<ProTrainingTech> _AttackerTechs;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "Version")]
    public int Version
    {
      get
      {
        return this._Version;
      }
      set
      {
        this._Version = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    [DefaultValue(0.0f)]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleType")]
    [DefaultValue(0)]
    public int BattleType
    {
      get
      {
        return this._BattleType;
      }
      set
      {
        this._BattleType = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RandomSeed")]
    [DefaultValue(0)]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Commands")]
    public List<ProBattleCommand> Commands
    {
      get
      {
        return this._Commands;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "Status")]
    public int Status
    {
      get
      {
        return this._Status;
      }
      set
      {
        this._Status = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaDefenderRuleId")]
    public int ArenaDefenderRuleId
    {
      get
      {
        return this._ArenaDefenderRuleId;
      }
      set
      {
        this._ArenaDefenderRuleId = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = false, Name = "DefenderUserId")]
    public string DefenderUserId
    {
      get
      {
        return this._DefenderUserId;
      }
      set
      {
        this._DefenderUserId = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = false, Name = "DefenderName")]
    public string DefenderName
    {
      get
      {
        return this._DefenderName;
      }
      set
      {
        this._DefenderName = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "DefenderLevel")]
    [DefaultValue(0)]
    public int DefenderLevel
    {
      get
      {
        return this._DefenderLevel;
      }
      set
      {
        this._DefenderLevel = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "DefenderHeroes")]
    public List<ProBattleHero> DefenderHeroes
    {
      get
      {
        return this._DefenderHeroes;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = false, Name = "AttackerUserId")]
    [DefaultValue("")]
    public string AttackerUserId
    {
      get
      {
        return this._AttackerUserId;
      }
      set
      {
        this._AttackerUserId = value;
      }
    }

    [DefaultValue("")]
    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = false, Name = "AttackerName")]
    public string AttackerName
    {
      get
      {
        return this._AttackerName;
      }
      set
      {
        this._AttackerName = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AttackerLevel")]
    public int AttackerLevel
    {
      get
      {
        return this._AttackerLevel;
      }
      set
      {
        this._AttackerLevel = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, Name = "AttackerHeroes")]
    public List<ProBattleHero> AttackerHeroes
    {
      get
      {
        return this._AttackerHeroes;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AttackerGotArenaPoints")]
    public int AttackerGotArenaPoints
    {
      get
      {
        return this._AttackerGotArenaPoints;
      }
      set
      {
        this._AttackerGotArenaPoints = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "DefenderGotArenaPoints")]
    public int DefenderGotArenaPoints
    {
      get
      {
        return this._DefenderGotArenaPoints;
      }
      set
      {
        this._DefenderGotArenaPoints = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CreateTime")]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "OpponentHeadIcon")]
    public int OpponentHeadIcon
    {
      get
      {
        return this._OpponentHeadIcon;
      }
      set
      {
        this._OpponentHeadIcon = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, Name = "DefenderTechs")]
    public List<ProTrainingTech> DefenderTechs
    {
      get
      {
        return this._DefenderTechs;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, Name = "AttackerTechs")]
    public List<ProTrainingTech> AttackerTechs
    {
      get
      {
        return this._AttackerTechs;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
