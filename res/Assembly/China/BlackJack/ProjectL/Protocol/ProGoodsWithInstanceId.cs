﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGoodsWithInstanceId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProGoodsWithInstanceId")]
  [Serializable]
  public class ProGoodsWithInstanceId : IExtensible
  {
    private ulong _InstanceId;
    private ProGoods _GoodInfo;
    private int _resonanceCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGoodsWithInstanceId()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0.0f)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "GoodInfo")]
    [DefaultValue(null)]
    public ProGoods GoodInfo
    {
      get
      {
        return this._GoodInfo;
      }
      set
      {
        this._GoodInfo = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "resonanceCount")]
    [DefaultValue(0)]
    public int ResonanceCount
    {
      get
      {
        return this._resonanceCount;
      }
      set
      {
        this._resonanceCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
