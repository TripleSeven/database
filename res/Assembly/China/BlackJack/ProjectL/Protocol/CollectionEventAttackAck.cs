﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.CollectionEventAttackAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "CollectionEventAttackAck")]
  [Serializable]
  public class CollectionEventAttackAck : IExtensible
  {
    private int _Result;
    private int _CollectionEventId;
    private ProBattleBeginInfo _BattleBeginInfo;
    private ulong _ActivityInstanceId;
    private int _WapPointId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionEventAttackAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CollectionEventId")]
    public int CollectionEventId
    {
      get
      {
        return this._CollectionEventId;
      }
      set
      {
        this._CollectionEventId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleBeginInfo")]
    public ProBattleBeginInfo BattleBeginInfo
    {
      get
      {
        return this._BattleBeginInfo;
      }
      set
      {
        this._BattleBeginInfo = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivityInstanceId")]
    public ulong ActivityInstanceId
    {
      get
      {
        return this._ActivityInstanceId;
      }
      set
      {
        this._ActivityInstanceId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WapPointId")]
    public int WapPointId
    {
      get
      {
        return this._WapPointId;
      }
      set
      {
        this._WapPointId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
