﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.GiftStoreBuyGoodsNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "GiftStoreBuyGoodsNtf")]
  [Serializable]
  public class GiftStoreBuyGoodsNtf : IExtensible
  {
    private int _GoodsId;
    private string _GoodsRegisterId;
    private int _BoughtNums;
    private long _NextFlushTime;
    private ProChangedGoodsNtf _Ntf;
    private ProOrderReward _Reward;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreBuyGoodsNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoodsId")]
    public int GoodsId
    {
      get
      {
        return this._GoodsId;
      }
      set
      {
        this._GoodsId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "GoodsRegisterId")]
    public string GoodsRegisterId
    {
      get
      {
        return this._GoodsRegisterId;
      }
      set
      {
        this._GoodsRegisterId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BoughtNums")]
    public int BoughtNums
    {
      get
      {
        return this._BoughtNums;
      }
      set
      {
        this._BoughtNums = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextFlushTime")]
    public long NextFlushTime
    {
      get
      {
        return this._NextFlushTime;
      }
      set
      {
        this._NextFlushTime = value;
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      get
      {
        return this._Ntf;
      }
      set
      {
        this._Ntf = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Reward")]
    public ProOrderReward Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
