﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.AncientCallBossBattleFinishedAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "AncientCallBossBattleFinishedAck")]
  [Serializable]
  public class AncientCallBossBattleFinishedAck : IExtensible
  {
    private int _Result;
    private int _AncientCallBossId;
    private bool _Win;
    private int _AncientCallBossTotalDamage;
    private int _AncientCallBossCurrentRanking;
    private int _AncientCallBossLastRankingTotalDamage;
    private int _AncientCallBossOldMaxDamage;
    private readonly List<int> _AchievementIds;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossBattleFinishedAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossId")]
    public int AncientCallBossId
    {
      get
      {
        return this._AncientCallBossId;
      }
      set
      {
        this._AncientCallBossId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Win")]
    public bool Win
    {
      get
      {
        return this._Win;
      }
      set
      {
        this._Win = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossTotalDamage")]
    public int AncientCallBossTotalDamage
    {
      get
      {
        return this._AncientCallBossTotalDamage;
      }
      set
      {
        this._AncientCallBossTotalDamage = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossCurrentRanking")]
    public int AncientCallBossCurrentRanking
    {
      get
      {
        return this._AncientCallBossCurrentRanking;
      }
      set
      {
        this._AncientCallBossCurrentRanking = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossLastRankingTotalDamage")]
    public int AncientCallBossLastRankingTotalDamage
    {
      get
      {
        return this._AncientCallBossLastRankingTotalDamage;
      }
      set
      {
        this._AncientCallBossLastRankingTotalDamage = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossOldMaxDamage")]
    public int AncientCallBossOldMaxDamage
    {
      get
      {
        return this._AncientCallBossOldMaxDamage;
      }
      set
      {
        this._AncientCallBossOldMaxDamage = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "AchievementIds")]
    public List<int> AchievementIds
    {
      get
      {
        return this._AchievementIds;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
