﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PeakArenaBattleLiveRoomMatchInfoGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "PeakArenaBattleLiveRoomMatchInfoGetAck")]
  [Serializable]
  public class PeakArenaBattleLiveRoomMatchInfoGetAck : IExtensible
  {
    private int _Result;
    private long _RoomCreateTime;
    private readonly List<int> _WinPlayerIndexes;
    private readonly List<string> _FirstHandUserIds;
    private readonly List<ProUserSummary> _Players;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleLiveRoomMatchInfoGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      get
      {
        return this._Result;
      }
      set
      {
        this._Result = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomCreateTime")]
    public long RoomCreateTime
    {
      get
      {
        return this._RoomCreateTime;
      }
      set
      {
        this._RoomCreateTime = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "WinPlayerIndexes")]
    public List<int> WinPlayerIndexes
    {
      get
      {
        return this._WinPlayerIndexes;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "FirstHandUserIds")]
    public List<string> FirstHandUserIds
    {
      get
      {
        return this._FirstHandUserIds;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProUserSummary> Players
    {
      get
      {
        return this._Players;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
