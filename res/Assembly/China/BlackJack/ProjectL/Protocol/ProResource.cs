﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProResource")]
  [Serializable]
  public class ProResource : IExtensible
  {
    private readonly List<int> _HeadFrames;
    private readonly List<int> _HeroSkinIds;
    private readonly List<int> _SoldierSkinIds;
    private readonly List<ProMonthCard> _MonthCards;
    private readonly List<int> _EquipmentIds;
    private readonly List<int> _TitleIds;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, Name = "HeadFrames")]
    public List<int> HeadFrames
    {
      get
      {
        return this._HeadFrames;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "HeroSkinIds")]
    public List<int> HeroSkinIds
    {
      get
      {
        return this._HeroSkinIds;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "SoldierSkinIds")]
    public List<int> SoldierSkinIds
    {
      get
      {
        return this._SoldierSkinIds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "MonthCards")]
    public List<ProMonthCard> MonthCards
    {
      get
      {
        return this._MonthCards;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "EquipmentIds")]
    public List<int> EquipmentIds
    {
      get
      {
        return this._EquipmentIds;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "TitleIds")]
    public List<int> TitleIds
    {
      get
      {
        return this._TitleIds;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
