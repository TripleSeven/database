﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaPlayOffBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArenaPlayOffBattleInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProPeakArenaPlayOffBattleInfo : IExtensible
  {
    private bool _PlayerOnLeftReady;
    private bool _PlayerOnRightReady;
    private string _WinnerId;
    private long _BattleEndTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaPlayOffBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayerOnLeftReady")]
    public bool PlayerOnLeftReady
    {
      get
      {
        return this._PlayerOnLeftReady;
      }
      set
      {
        this._PlayerOnLeftReady = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayerOnRightReady")]
    public bool PlayerOnRightReady
    {
      get
      {
        return this._PlayerOnRightReady;
      }
      set
      {
        this._PlayerOnRightReady = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "WinnerId")]
    public string WinnerId
    {
      get
      {
        return this._WinnerId;
      }
      set
      {
        this._WinnerId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleEndTime")]
    public long BattleEndTime
    {
      get
      {
        return this._BattleEndTime;
      }
      set
      {
        this._BattleEndTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
