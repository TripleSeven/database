﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProArenaPlayerDefensiveTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProArenaPlayerDefensiveTeam")]
  [Serializable]
  public class ProArenaPlayerDefensiveTeam : IExtensible
  {
    private int _BattleId;
    private int _ArenaDefenderRuleId;
    private readonly List<ProArenaPlayerDefensiveHero> _DefenderHeroes;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaPlayerDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaDefenderRuleId")]
    public int ArenaDefenderRuleId
    {
      get
      {
        return this._ArenaDefenderRuleId;
      }
      set
      {
        this._ArenaDefenderRuleId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "DefenderHeroes")]
    public List<ProArenaPlayerDefensiveHero> DefenderHeroes
    {
      get
      {
        return this._DefenderHeroes;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
