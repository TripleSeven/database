﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProArenaDefensiveBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProArenaDefensiveBattleInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ProArenaDefensiveBattleInfo : IExtensible
  {
    private int _BattleId;
    private int _ArenaDefenderRuleId;
    private int _RandomSeed;
    private long _BattleExpiredTime;
    private readonly List<ProBattleHero> _DefenderHeroes;
    private int _ArenaOpponentPointZoneId;
    private readonly List<ProTrainingTech> _Techs;
    private int _PlayerLevel;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleId")]
    public int BattleId
    {
      get
      {
        return this._BattleId;
      }
      set
      {
        this._BattleId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaDefenderRuleId")]
    public int ArenaDefenderRuleId
    {
      get
      {
        return this._ArenaDefenderRuleId;
      }
      set
      {
        this._ArenaDefenderRuleId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomSeed")]
    public int RandomSeed
    {
      get
      {
        return this._RandomSeed;
      }
      set
      {
        this._RandomSeed = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleExpiredTime")]
    public long BattleExpiredTime
    {
      get
      {
        return this._BattleExpiredTime;
      }
      set
      {
        this._BattleExpiredTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "DefenderHeroes")]
    public List<ProBattleHero> DefenderHeroes
    {
      get
      {
        return this._DefenderHeroes;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaOpponentPointZoneId")]
    public int ArenaOpponentPointZoneId
    {
      get
      {
        return this._ArenaOpponentPointZoneId;
      }
      set
      {
        this._ArenaOpponentPointZoneId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Techs")]
    public List<ProTrainingTech> Techs
    {
      get
      {
        return this._Techs;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerLevel")]
    public int PlayerLevel
    {
      get
      {
        return this._PlayerLevel;
      }
      set
      {
        this._PlayerLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
