﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuildMassiveCombatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "ProGuildMassiveCombatInfo")]
  [Serializable]
  public class ProGuildMassiveCombatInfo : IExtensible
  {
    private ulong _InstanceId;
    private int _Difficulty;
    private readonly List<ProGuildMassiveCombatStrongholdInfo> _Strongholds;
    private readonly List<ProGuildMassiveCombatMemberInfo> _Members;
    private long _CreateTime;
    private long _FinishTime;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InstanceId")]
    public ulong InstanceId
    {
      get
      {
        return this._InstanceId;
      }
      set
      {
        this._InstanceId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Difficulty")]
    public int Difficulty
    {
      get
      {
        return this._Difficulty;
      }
      set
      {
        this._Difficulty = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Strongholds")]
    public List<ProGuildMassiveCombatStrongholdInfo> Strongholds
    {
      get
      {
        return this._Strongholds;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Members")]
    public List<ProGuildMassiveCombatMemberInfo> Members
    {
      get
      {
        return this._Members;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CreateTime")]
    public long CreateTime
    {
      get
      {
        return this._CreateTime;
      }
      set
      {
        this._CreateTime = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FinishTime")]
    public long FinishTime
    {
      get
      {
        return this._FinishTime;
      }
      set
      {
        this._FinishTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
