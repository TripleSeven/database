﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.NullBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class NullBattleListener : IBattleListener
  {
    public virtual void OnBattleStart()
    {
    }

    public virtual void OnBattleNextTurn(int turn)
    {
    }

    public virtual void OnBattleNextTeam(int team, bool isNpc)
    {
    }

    public virtual void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
    }

    public virtual void OnBattleNextActor(BattleActor actor)
    {
    }

    public virtual void OnBattleActorCreate(BattleActor a, bool visible)
    {
    }

    public virtual void OnBattleActorCreateEnd(BattleActor a)
    {
    }

    public virtual void OnBattleActorActive(BattleActor a, bool newStep)
    {
    }

    public virtual void OnBattleActorActionBegin(BattleActor a)
    {
    }

    public virtual void OnBattleActorActionEnd(BattleActor a)
    {
    }

    public virtual void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
    }

    public virtual void OnBattleActorMoveEnd(BattleActor a)
    {
    }

    public virtual void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
    }

    public virtual void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
    }

    public virtual void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
    }

    public virtual void OnBattleActorSetDir(BattleActor a, int dir)
    {
    }

    public virtual void OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
    {
    }

    public virtual void OnBattleActorPlayAnimation(
      BattleActor a,
      string animationName,
      int animationTime)
    {
    }

    public virtual void OnBattleActorChangeIdleAnimation(BattleActor a, string idleAnimationName)
    {
    }

    public virtual void OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActor)
    {
    }

    public virtual void OnBattleActorSkillHitBegin(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
    }

    public virtual void OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
    }

    public virtual void OnBattleActorSkillHitEnd(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
    }

    public virtual void OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
    {
    }

    public virtual void OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
    {
    }

    public virtual void OnBattleActorImmune(BattleActor a)
    {
    }

    public virtual void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
    }

    public virtual void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
    }

    public virtual void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
    }

    public virtual void OnBattleActorTeleport(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
    }

    public virtual void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
    }

    public virtual void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
    }

    public virtual void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
    }

    public virtual void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
    }

    public virtual void OnBattleActorChangeTeam(BattleActor a)
    {
    }

    public virtual void OnBattleActorChangeArmy(BattleActor a)
    {
    }

    public virtual void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
    }

    public virtual void OnBattleActorCameraFocus(BattleActor a)
    {
    }

    public virtual void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
    }

    public virtual void OnStartGuard(BattleActor a, BattleActor target)
    {
    }

    public virtual void OnStopGuard(BattleActor a, BattleActor target)
    {
    }

    public virtual void OnBeforeStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
    }

    public virtual void OnCancelCombat()
    {
    }

    public virtual void OnStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
    }

    public virtual void OnPreStopCombat()
    {
    }

    public virtual void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
    }

    public virtual void OnCombatActorSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
    }

    public virtual void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
    }

    public virtual void OnCombatActorDie(CombatActor a)
    {
    }

    public virtual void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      ConfigDataCutsceneInfo cutsceneInfo2,
      int team)
    {
    }

    public virtual void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
    }

    public virtual void OnStopSkillCutscene()
    {
    }

    public virtual void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
    }

    public virtual void OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
    }

    public virtual void OnStopBattlePerform()
    {
    }

    public virtual void OnChangeMapTerrain(
      List<GridPosition> positions,
      ConfigDataTerrainInfo terrainInfo)
    {
    }

    public virtual void OnChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
    }

    public virtual void OnCameraFocus(GridPosition p)
    {
    }

    public virtual void OnPlayMusic(string musicName)
    {
    }

    public virtual void OnPlaySound(string soundName)
    {
    }

    public virtual void OnPlayFx(string fxName, GridPosition p)
    {
    }

    public virtual void OnWaitTime(int timeInMs)
    {
    }

    public virtual void OnBattleTreasureCreate(
      ConfigDataBattleTreasureInfo treasureInfo,
      bool isOpened)
    {
    }

    public virtual IBattleGraphic CreateCombatGraphic(string assetName, float scale)
    {
      return (IBattleGraphic) null;
    }

    public virtual void DestroyCombatGraphic(IBattleGraphic model)
    {
    }

    public virtual IBattleGraphic PlayCombatFx(string assetName, float scale)
    {
      return (IBattleGraphic) null;
    }

    public virtual void PlaySound(string name)
    {
    }

    public virtual void DrawLine(Vector2i p0, Vector2i p1, Colori color)
    {
    }

    public virtual void DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
    {
    }

    public virtual void LogBattleStart()
    {
    }

    public virtual void LogBattleStop(bool isWin)
    {
    }

    public virtual void LogBattleTeam(BattleTeam team0, BattleTeam team1)
    {
    }

    public virtual void LogActorMove(BattleActor actor, GridPosition fromPos, GridPosition toPos)
    {
    }

    public virtual void LogActorStandby(BattleActor actor)
    {
    }

    public virtual void LogActorAttack(BattleActor actor, BattleActor targetActor)
    {
    }

    public virtual void LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos)
    {
    }

    public virtual void LogActorDie(BattleActor actor, BattleActor killerActor)
    {
    }
  }
}
