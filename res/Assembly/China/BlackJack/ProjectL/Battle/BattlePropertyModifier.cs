﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattlePropertyModifier
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Battle
{
  [HotFix]
  public class BattlePropertyModifier
  {
    private int[] m_values;
    public int ExchangeAttack;
    public int ExchangeDefense;
    public int ExchangeMagic;
    public int ExchangeMagicDefense;
    public int ExchangeDexterity;
    public int ExchangeHealthPointMax;
    [DoNotToLua]
    private BattlePropertyModifier.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetPropertyModifyType_hotfix;
    private LuaFunction m_CollectPropertyModifyTypeInt32_hotfix;
    private LuaFunction m_Clear_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePropertyModifier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Get(PropertyModifyType t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Collect(PropertyModifyType t, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattlePropertyModifier.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattlePropertyModifier m_owner;

      public LuaExportHelper(BattlePropertyModifier owner)
      {
        this.m_owner = owner;
      }

      public int[] m_values
      {
        get
        {
          return this.m_owner.m_values;
        }
        set
        {
          this.m_owner.m_values = value;
        }
      }
    }
  }
}
