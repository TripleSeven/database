﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Pathfinder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class Pathfinder
  {
    private LinkedList<Pathfinder.Node> m_OpenList;
    private List<Pathfinder.Node> m_ClosedList;
    private List<Pathfinder.Node> m_Successors;
    private Pathfinder.SearchState m_State;
    private int m_Steps;
    private Pathfinder.Node m_Start;
    private Pathfinder.Node m_Goal;
    private Pathfinder.Node m_CurrentSolutionNode;
    private bool m_CancelRequest;
    private List<Pathfinder.Node> m_NodePool;
    private int m_AllocateNodeCount;
    private List<PathNode> m_PathNodePool;
    private int m_AllocatedPathNodeCount;
    private const int kPreallocatedNodes = 64;
    private BattleMap m_Map;
    private int m_MovePoint;
    private MoveType m_MoveType;
    private FindPathIgnoreTeamType m_IgnoreTeamType;
    private int m_InRegion;
    private int m_OverrideMovePointCost;
    private SpecialMoveCostType m_SpecialMoveCostType;
    private int m_SpecialMovePoint;

    [MethodImpl((MethodImplOptions) 32768)]
    public Pathfinder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortedAddToOpenList(Pathfinder.Node node)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Pathfinder.Node AllocateNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode AllocatePathNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitiatePathfind()
    {
      // ISSUE: unable to decompile the method.
    }

    public void CancelSearch()
    {
      this.m_CancelRequest = true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStartAndGoalStates(PathNode start, PathNode goal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Pathfinder.SearchState SearchStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRegionStartState(PathNode start)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Pathfinder.SearchState SearchRegionStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSuccessor(PathNode state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode GetSolutionStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode GetSolutionNext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeSolutionNodes()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleMap Map
    {
      get
      {
        return this.m_Map;
      }
    }

    public MoveType MoveType
    {
      get
      {
        return this.m_MoveType;
      }
    }

    public FindPathIgnoreTeamType IgnoreTeamType
    {
      get
      {
        return this.m_IgnoreTeamType;
      }
    }

    public int OverrideMovePointCost
    {
      get
      {
        return this.m_OverrideMovePointCost;
      }
    }

    public int InRegion
    {
      get
      {
        return this.m_InRegion;
      }
    }

    public PathNode StartNode
    {
      get
      {
        return this.m_Start.m_UserState;
      }
    }

    public PathNode GoalNode
    {
      get
      {
        return this.m_Goal.m_UserState;
      }
    }

    public bool HasStartNode()
    {
      return this.m_Start != null;
    }

    public bool HasGoalNode()
    {
      return this.m_Goal != null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      BattleMap map,
      GridPosition start,
      GridPosition goal,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindRegion(
      BattleMap map,
      GridPosition start,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum SearchState
    {
      NotInitialized,
      Searching,
      Succeeded,
      Failed,
    }

    [CustomLuaClass]
    public class Node
    {
      public Pathfinder.Node parent;
      public Pathfinder.Node child;
      public int g;
      public int g2;
      public int h;
      public int f;
      public PathNode m_UserState;

      public Node()
      {
        this.Reinitialize();
      }

      public void Reinitialize()
      {
        this.parent = (Pathfinder.Node) null;
        this.child = (Pathfinder.Node) null;
        this.g = 0;
        this.g2 = 0;
        this.h = 0;
        this.f = 0;
      }
    }
  }
}
