﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatFlyObjectList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  [CustomLuaClass]
  public class CombatFlyObjectList
  {
    public static void RemoveAll(List<CombatFlyObject> list)
    {
      EntityList.RemoveAll<CombatFlyObject>(list);
    }

    public static void RemoveDeleted(List<CombatFlyObject> list)
    {
      EntityList.RemoveDeleted<CombatFlyObject>(list);
    }

    public static void Tick(List<CombatFlyObject> list)
    {
      EntityList.Tick<CombatFlyObject>(list);
    }

    public static void TickGraphic(List<CombatFlyObject> list, float dt)
    {
      EntityList.TickGraphic<CombatFlyObject>(list, dt);
    }

    public static void Draw(List<CombatFlyObject> list)
    {
      EntityList.Draw<CombatFlyObject>(list);
    }

    public static void Pause(List<CombatFlyObject> list, bool pause)
    {
      EntityList.Pause<CombatFlyObject>(list, pause);
    }
  }
}
