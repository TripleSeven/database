﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.StateMachine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.LibClient
{
  [CustomLuaClass]
  public class StateMachine
  {
    public virtual int SetStateCheck(int commingEvent, int newState = -1, bool testOnly = false)
    {
      this.State = newState;
      return this.State;
    }

    public virtual void SetStateWithoutCheck(int newState)
    {
      this.State = newState;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool EventCheck(int commingEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    public int State { get; protected set; }
  }
}
