﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityWorld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.UI;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class CollectionActivityWorld
  {
    private int m_entityIdCounter;
    private int m_frameCount;
    private float m_worldTickTime;
    private bool m_isPaused;
    private bool m_enableDebugDraw;
    private RandomNumber m_randomNumber;
    private ProjectLPlayerContext m_playerContext;
    private List<CollectionActivityPlayerActor> m_playerActors;
    private List<CollectionActivityEventActor> m_eventActors;
    private List<CollectionActivityWaypoint> m_waypoints;
    private IConfigDataLoader m_configDataLoader;
    private ICollectionActivityWorldListener m_collectionActivityWorldListener;
    private WorldCamera m_worldCamera;
    private WorldPathfinder m_pathfinder;
    private GameObject m_worldRoot;
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_background;
    private GameObject m_backgroundWaypointsRoot;
    private GameObject m_worldUIRoot;
    private GameObject m_waypointUIRoot;
    private GameObject m_eventUIRoot;
    private GameObject m_playerUIRoot;
    private GameObject m_waypointUIPrefab;
    private GameObject m_waypoint2UIPrefab;
    private GameObject m_eventUIPrefab;
    private GameObject m_playerUIPrefab;
    private WorldScenarioUIController m_worldScenarioUIController;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;
    private Quaternion m_faceCameraRotation;
    private bool m_isCameraFollowPlayer;
    private ConfigDataCollectionActivityInfo m_collectionActivityInfo;
    [DoNotToLua]
    private CollectionActivityWorld.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_InitializeICollectionActivityWorldListenerGameObject_hotfix;
    private LuaFunction m_Is2D_hotfix;
    private LuaFunction m_TickSingle_hotfix;
    private LuaFunction m_TickCollectionActivityWorld_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_Clear_hotfix;
    private LuaFunction m_StartConfigDataCollectionActivityInfo_hotfix;
    private LuaFunction m_PauseBoolean_hotfix;
    private LuaFunction m_ShowWorldBoolean_hotfix;
    private LuaFunction m_CreateWaypointConfigDataCollectionActivityWaypointInfo_hotfix;
    private LuaFunction m_CreatePlayerActorConfigDataCollectionActivityWaypointInfoInt32_hotfix;
    private LuaFunction m_CreateEventActorConfigDataCollectionEventInfoConfigDataCollectionActivityWaypointInfoInt32_hotfix;
    private LuaFunction m_UpdateWaypointState_hotfix;
    private LuaFunction m_UpdatePlayerGraphicAndLocation_hotfix;
    private LuaFunction m_UpdateActiveScenarioAndWaypointState_hotfix;
    private LuaFunction m_UpdateEventActorState_hotfix;
    private LuaFunction m_GetWaypointInt32_hotfix;
    private LuaFunction m_GetEventActorAtInt32_hotfix;
    private LuaFunction m_GetEventActoryByEventIdInt32_hotfix;
    private LuaFunction m_GetEventActors_hotfix;
    private LuaFunction m_GetPlayerActor_hotfix;
    private LuaFunction m_OnWaypointClickCollectionActivityWaypoint_hotfix;
    private LuaFunction m_OnEventActorClickCollectionActivityEventActor_hotfix;
    private LuaFunction m_CreateGraphicStringSingle_hotfix;
    private LuaFunction m_DestroyGraphicGenericGraphic_hotfix;
    private LuaFunction m_GetCamera_hotfix;
    private LuaFunction m_GetCameraPosition_hotfix;
    private LuaFunction m_ComputeActorPositionVector2Single_hotfix;
    private LuaFunction m_IsCulledVector2Boolean_hotfix;
    private LuaFunction m_IsCulledVector2Vector2Boolean_hotfix;
    private LuaFunction m_GetNextEntityId_hotfix;
    private LuaFunction m_CreateBackgroundConfigDataCollectionActivityMapInfoGameObject_hotfix;
    private LuaFunction m_ClearBackground_hotfix;
    private LuaFunction m_ShowBackgroundChildStringBoolean_hotfix;
    private LuaFunction m_PlaySoundString_hotfix;
    private LuaFunction m_PlaySoundSoundTableId_hotfix;
    private LuaFunction m_FindPathInt32Int32List`1Boolean_hotfix;
    private LuaFunction m_get_WorldCamera_hotfix;
    private LuaFunction m_set_EnableDebugDrawBoolean_hotfix;
    private LuaFunction m_get_EnableDebugDraw_hotfix;
    private LuaFunction m_get_GraphicRoot_hotfix;
    private LuaFunction m_get_WorldUIRoot_hotfix;
    private LuaFunction m_get_WaypointUIRoot_hotfix;
    private LuaFunction m_get_EventUIRoot_hotfix;
    private LuaFunction m_get_PlayerUIRoot_hotfix;
    private LuaFunction m_get_WaypointUIPrefab_hotfix;
    private LuaFunction m_get_Waypoint2UIPrefab_hotfix;
    private LuaFunction m_get_EventUIPrefab_hotfix;
    private LuaFunction m_get_PlayerUIPrefab_hotfix;
    private LuaFunction m_get_FaceCameraRotation_hotfix;
    private LuaFunction m_get_IsCameraFollowPlayer_hotfix;
    private LuaFunction m_get_FxPlayer_hotfix;
    private LuaFunction m_get_ConfigDataLoader_hotfix;
    private LuaFunction m_get_ClientWorldListener_hotfix;
    private LuaFunction m_get_PlayerContext_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      ICollectionActivityWorldListener collectionActivityWorldListener,
      GameObject worldRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Is2D()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityWaypoint CreateWaypoint(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityPlayerActor CreatePlayerActor(
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityEventActor CreateEventActor(
      ConfigDataCollectionEventInfo collectionEventInfo,
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerGraphicAndLocation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveScenarioAndWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEventActorState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypoint GetWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor GetEventActorAt(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor GetEventActoryByEventId(
      int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivityEventActor> GetEventActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPlayerActor GetPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(CollectionActivityWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventActorClick(CollectionActivityEventActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CameraBase GetCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 ComputeActorPosition(Vector2 pos, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 p, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackground(ConfigDataCollectionActivityMapInfo mapInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBackgroundChild(string childName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path, bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int FrameToMillisecond(int frame)
    {
      return frame * 1000 / 30;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldCamera WorldCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool EnableDebugDraw
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject GraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WorldUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject Waypoint2UIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Quaternion FaceCameraRotation
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCameraFollowPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer FxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ICollectionActivityWorldListener ClientWorldListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ProjectLPlayerContext PlayerContext
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CollectionActivityWorld.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CollectionActivityWorld m_owner;

      public LuaExportHelper(CollectionActivityWorld owner)
      {
        this.m_owner = owner;
      }

      public int m_entityIdCounter
      {
        get
        {
          return this.m_owner.m_entityIdCounter;
        }
        set
        {
          this.m_owner.m_entityIdCounter = value;
        }
      }

      public int m_frameCount
      {
        get
        {
          return this.m_owner.m_frameCount;
        }
        set
        {
          this.m_owner.m_frameCount = value;
        }
      }

      public float m_worldTickTime
      {
        get
        {
          return this.m_owner.m_worldTickTime;
        }
        set
        {
          this.m_owner.m_worldTickTime = value;
        }
      }

      public bool m_isPaused
      {
        get
        {
          return this.m_owner.m_isPaused;
        }
        set
        {
          this.m_owner.m_isPaused = value;
        }
      }

      public bool m_enableDebugDraw
      {
        get
        {
          return this.m_owner.m_enableDebugDraw;
        }
        set
        {
          this.m_owner.m_enableDebugDraw = value;
        }
      }

      public RandomNumber m_randomNumber
      {
        get
        {
          return this.m_owner.m_randomNumber;
        }
        set
        {
          this.m_owner.m_randomNumber = value;
        }
      }

      public ProjectLPlayerContext m_playerContext
      {
        get
        {
          return this.m_owner.m_playerContext;
        }
        set
        {
          this.m_owner.m_playerContext = value;
        }
      }

      public List<CollectionActivityPlayerActor> m_playerActors
      {
        get
        {
          return this.m_owner.m_playerActors;
        }
        set
        {
          this.m_owner.m_playerActors = value;
        }
      }

      public List<CollectionActivityEventActor> m_eventActors
      {
        get
        {
          return this.m_owner.m_eventActors;
        }
        set
        {
          this.m_owner.m_eventActors = value;
        }
      }

      public List<CollectionActivityWaypoint> m_waypoints
      {
        get
        {
          return this.m_owner.m_waypoints;
        }
        set
        {
          this.m_owner.m_waypoints = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public ICollectionActivityWorldListener m_collectionActivityWorldListener
      {
        get
        {
          return this.m_owner.m_collectionActivityWorldListener;
        }
        set
        {
          this.m_owner.m_collectionActivityWorldListener = value;
        }
      }

      public WorldCamera m_worldCamera
      {
        get
        {
          return this.m_owner.m_worldCamera;
        }
        set
        {
          this.m_owner.m_worldCamera = value;
        }
      }

      public WorldPathfinder m_pathfinder
      {
        get
        {
          return this.m_owner.m_pathfinder;
        }
        set
        {
          this.m_owner.m_pathfinder = value;
        }
      }

      public GameObject m_worldRoot
      {
        get
        {
          return this.m_owner.m_worldRoot;
        }
        set
        {
          this.m_owner.m_worldRoot = value;
        }
      }

      public GameObject m_graphicRoot
      {
        get
        {
          return this.m_owner.m_graphicRoot;
        }
        set
        {
          this.m_owner.m_graphicRoot = value;
        }
      }

      public GameObject m_mapRoot
      {
        get
        {
          return this.m_owner.m_mapRoot;
        }
        set
        {
          this.m_owner.m_mapRoot = value;
        }
      }

      public GameObject m_background
      {
        get
        {
          return this.m_owner.m_background;
        }
        set
        {
          this.m_owner.m_background = value;
        }
      }

      public GameObject m_backgroundWaypointsRoot
      {
        get
        {
          return this.m_owner.m_backgroundWaypointsRoot;
        }
        set
        {
          this.m_owner.m_backgroundWaypointsRoot = value;
        }
      }

      public GameObject m_worldUIRoot
      {
        get
        {
          return this.m_owner.m_worldUIRoot;
        }
        set
        {
          this.m_owner.m_worldUIRoot = value;
        }
      }

      public GameObject m_waypointUIRoot
      {
        get
        {
          return this.m_owner.m_waypointUIRoot;
        }
        set
        {
          this.m_owner.m_waypointUIRoot = value;
        }
      }

      public GameObject m_eventUIRoot
      {
        get
        {
          return this.m_owner.m_eventUIRoot;
        }
        set
        {
          this.m_owner.m_eventUIRoot = value;
        }
      }

      public GameObject m_playerUIRoot
      {
        get
        {
          return this.m_owner.m_playerUIRoot;
        }
        set
        {
          this.m_owner.m_playerUIRoot = value;
        }
      }

      public GameObject m_waypointUIPrefab
      {
        get
        {
          return this.m_owner.m_waypointUIPrefab;
        }
        set
        {
          this.m_owner.m_waypointUIPrefab = value;
        }
      }

      public GameObject m_waypoint2UIPrefab
      {
        get
        {
          return this.m_owner.m_waypoint2UIPrefab;
        }
        set
        {
          this.m_owner.m_waypoint2UIPrefab = value;
        }
      }

      public GameObject m_eventUIPrefab
      {
        get
        {
          return this.m_owner.m_eventUIPrefab;
        }
        set
        {
          this.m_owner.m_eventUIPrefab = value;
        }
      }

      public GameObject m_playerUIPrefab
      {
        get
        {
          return this.m_owner.m_playerUIPrefab;
        }
        set
        {
          this.m_owner.m_playerUIPrefab = value;
        }
      }

      public WorldScenarioUIController m_worldScenarioUIController
      {
        get
        {
          return this.m_owner.m_worldScenarioUIController;
        }
        set
        {
          this.m_owner.m_worldScenarioUIController = value;
        }
      }

      public GraphicPool m_graphicPool
      {
        get
        {
          return this.m_owner.m_graphicPool;
        }
        set
        {
          this.m_owner.m_graphicPool = value;
        }
      }

      public GraphicPool m_fxPool
      {
        get
        {
          return this.m_owner.m_fxPool;
        }
        set
        {
          this.m_owner.m_fxPool = value;
        }
      }

      public FxPlayer m_fxPlayer
      {
        get
        {
          return this.m_owner.m_fxPlayer;
        }
        set
        {
          this.m_owner.m_fxPlayer = value;
        }
      }

      public Quaternion m_faceCameraRotation
      {
        get
        {
          return this.m_owner.m_faceCameraRotation;
        }
        set
        {
          this.m_owner.m_faceCameraRotation = value;
        }
      }

      public bool m_isCameraFollowPlayer
      {
        get
        {
          return this.m_owner.m_isCameraFollowPlayer;
        }
        set
        {
          this.m_owner.m_isCameraFollowPlayer = value;
        }
      }

      public ConfigDataCollectionActivityInfo m_collectionActivityInfo
      {
        get
        {
          return this.m_owner.m_collectionActivityInfo;
        }
        set
        {
          this.m_owner.m_collectionActivityInfo = value;
        }
      }

      public void TickCollectionActivityWorld()
      {
        this.m_owner.TickCollectionActivityWorld();
      }

      public void TickGraphic(float dt)
      {
        this.m_owner.TickGraphic(dt);
      }

      public void Draw()
      {
        this.m_owner.Draw();
      }

      public void Clear()
      {
        this.m_owner.Clear();
      }

      public CollectionActivityWaypoint CreateWaypoint(
        ConfigDataCollectionActivityWaypointInfo waypointInfo)
      {
        return this.m_owner.CreateWaypoint(waypointInfo);
      }

      public CollectionActivityPlayerActor CreatePlayerActor(
        ConfigDataCollectionActivityWaypointInfo waypointInfo,
        int dir)
      {
        return this.m_owner.CreatePlayerActor(waypointInfo, dir);
      }

      public CollectionActivityEventActor CreateEventActor(
        ConfigDataCollectionEventInfo collectionEventInfo,
        ConfigDataCollectionActivityWaypointInfo waypointInfo,
        int dir)
      {
        return this.m_owner.CreateEventActor(collectionEventInfo, waypointInfo, dir);
      }

      public void UpdateWaypointState()
      {
        this.m_owner.UpdateWaypointState();
      }

      public CameraBase GetCamera()
      {
        return this.m_owner.GetCamera();
      }

      public void CreateBackground(ConfigDataCollectionActivityMapInfo mapInfo, GameObject parent)
      {
        this.m_owner.CreateBackground(mapInfo, parent);
      }

      public void ClearBackground()
      {
        this.m_owner.ClearBackground();
      }
    }
  }
}
