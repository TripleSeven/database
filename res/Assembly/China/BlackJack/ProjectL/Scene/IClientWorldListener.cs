﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.IClientWorldListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;

namespace BlackJack.ProjectL.Scene
{
  public interface IClientWorldListener
  {
    void OnWaypointClick(ConfigDataWaypointInfo waypointInfo);

    void OnScreenEffect(string name);
  }
}
