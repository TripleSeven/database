﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActDetachBuff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Battle;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientActorActDetachBuff : ClientActorAct
  {
    public BuffState m_buffState;
    public uint m_fightTags;
    public int m_heroHpMax;
    public int m_soldierHpMax;
  }
}
