﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActStopCombat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientActorActStopCombat : ClientActorAct
  {
    public int m_heroHp;
    public int m_soldierHp;
    public int m_heroDamage;
    public int m_soldierDamage;
    public List<int> m_disabledBuffStateIdList;
  }
}
