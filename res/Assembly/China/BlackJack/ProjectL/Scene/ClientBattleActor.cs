﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.UI;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class ClientBattleActor : Entity
  {
    private ClientBattle m_clientBattle;
    private BattleActor m_battleActor;
    private int m_heroHealthPoint;
    private int m_soldierTotalHealthPoint;
    private int m_heroHealthPointMax;
    private int m_soldierTotalHealthPointMax;
    private uint m_fightTags;
    private ClientBattleActor.GraphicState[] m_graphicStates;
    private int m_soldierGraphicCount;
    private Vector2 m_graphicPosition;
    private bool m_isGraphicSkillFade;
    private bool m_isActionFinishedGray;
    private BattleActorUIController m_uiController;
    private List<ClientActorBuff> m_buffs;
    private List<GenericGraphic> m_attachFxs;
    private GridPosition m_position;
    private int m_direction;
    private bool m_isVisible;
    private bool m_isIdleRun;
    private bool m_isGuarding;
    private bool m_isTeleportDisappear;
    private string m_idleAnimationName;
    private List<ClientActorAct> m_acts;
    private int m_actFrame;
    private int m_tempActFrame;
    private int m_deleteMeCountdown;
    [DoNotToLua]
    private ClientBattleActor.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeClientBattleBattleActor_hotfix;
    private LuaFunction m_InitializeEnd_hotfix;
    private LuaFunction m_PostRebuildBattle_hotfix;
    private LuaFunction m_CreateHeroGraphic_hotfix;
    private LuaFunction m_CreateSoldierGraphics_hotfix;
    private LuaFunction m_Dispose_hotfix;
    private LuaFunction m_Tick_hotfix;
    private LuaFunction m_TickGraphicSingle_hotfix;
    private LuaFunction m_ComputeGraphicPositionVector2Single_hotfix;
    private LuaFunction m_Draw_hotfix;
    private LuaFunction m_DoPauseBoolean_hotfix;
    private LuaFunction m_IsDead_hotfix;
    private LuaFunction m_GetTotalHealthPoint_hotfix;
    private LuaFunction m_GetTotalHealthPointMax_hotfix;
    private LuaFunction m_HasFightTagFightTag_hotfix;
    private LuaFunction m_IsStun_hotfix;
    private LuaFunction m_LocateGridPositionInt32_hotfix;
    private LuaFunction m_FaceToGridPosition_hotfix;
    private LuaFunction m_AddActClientActorAct_hotfix;
    private LuaFunction m_HasAct_hotfix;
    private LuaFunction m_PlayAnimationStringBooleanBoolean_hotfix;
    private LuaFunction m_GetHeroAnimationDurationString_hotfix;
    private LuaFunction m_IsHeroPlayAnimation_hotfix;
    private LuaFunction m_PlayDeathFxInt32_hotfix;
    private LuaFunction m_PlayIdleAnimation_hotfix;
    private LuaFunction m_PlayHeroFxString_hotfix;
    private LuaFunction m_PlayFxStringGridPositionSingleBoolean_hotfix;
    private LuaFunction m_PlayAttachFxString_hotfix;
    private LuaFunction m_ClearAttachFx_hotfix;
    private LuaFunction m_SetGraphicEffectGraphicEffectSingleSingle_hotfix;
    private LuaFunction m_ClearGraphicEffectGraphicEffect_hotfix;
    private LuaFunction m_PlaySoundString_hotfix;
    private LuaFunction m_PlaySoundSoundTableId_hotfix;
    private LuaFunction m_SetGraphicActionFinishedEffectBoolean_hotfix;
    private LuaFunction m_SetGraphicColorColori_hotfix;
    private LuaFunction m_AttachBuffBuffStateList`1_hotfix;
    private LuaFunction m_DetachBuffBuffState_hotfix;
    private LuaFunction m_SetHpBarTypeInt32Boolean_hotfix;
    private LuaFunction m_StartIdleRun_hotfix;
    private LuaFunction m_StopIdleRun_hotfix;
    private LuaFunction m_CancelTeleportDisappear_hotfix;
    private LuaFunction m_SetVisibleBoolean_hotfix;
    private LuaFunction m_IsVisible_hotfix;
    private LuaFunction m_ComputeSoldierGraphicCount_hotfix;
    private LuaFunction m_UpdateSoldierGraphicCount_hotfix;
    private LuaFunction m_GetSoldierGraphicCount_hotfix;
    private LuaFunction m_UpdateGraphicVisible_hotfix;
    private LuaFunction m_ComputeGraphicOffsetInt32_hotfix;
    private LuaFunction m_GetGraphicInt32_hotfix;
    private LuaFunction m_GetGraphicPosition_hotfix;
    private LuaFunction m_SetGraphicSkillFadeBoolean_hotfix;
    private LuaFunction m_IsGraphicSkillFade_hotfix;
    private LuaFunction m_UpdateHpAndBuffUIList`1_hotfix;
    private LuaFunction m_ShowPlayerIndexBoolean_hotfix;
    private LuaFunction m_get_Position_hotfix;
    private LuaFunction m_get_Direction_hotfix;
    private LuaFunction m_get_BattleActor_hotfix;
    private LuaFunction m_get_TeamNumber_hotfix;
    private LuaFunction m_get_HeroHealthPoint_hotfix;
    private LuaFunction m_get_SoldierTotalHealthPoint_hotfix;
    private LuaFunction m_TickActDispatchClientActorAct_hotfix;
    private LuaFunction m_TickActClientActorActActive_hotfix;
    private LuaFunction m_TickActClientActorActActionBegin_hotfix;
    private LuaFunction m_TickActClientActorActActionEnd_hotfix;
    private LuaFunction m_InitMoveActGridPositionInt32BooleanBoolean_hotfix;
    private LuaFunction m_ComputeMoveTimeGridPositionGridPosition_hotfix;
    private LuaFunction m_TickActClientActorActMove_hotfix;
    private LuaFunction m_TickActClientActorActMoveEnd_hotfix;
    private LuaFunction m_TickActClientActorActTryMove_hotfix;
    private LuaFunction m_TickActClientActorActPerformMove_hotfix;
    private LuaFunction m_TickActClientActorActPunchMove_hotfix;
    private LuaFunction m_TickActClientActorActExchangeMove_hotfix;
    private LuaFunction m_TickActClientActorActSetDir_hotfix;
    private LuaFunction m_TickActClientActorActPlayFx_hotfix;
    private LuaFunction m_TickActClientActorActPlayAnimation_hotfix;
    private LuaFunction m_TickActClientActorActChangeIdleAnimation_hotfix;
    private LuaFunction m_TickActClientActorActTarget_hotfix;
    private LuaFunction m_TickActClientActorActSkill_hotfix;
    private LuaFunction m_TickActClientActorActCombineSkill_hotfix;
    private LuaFunction m_TickActClientActorActSkillHit_hotfix;
    private LuaFunction m_TickSkillHitsConfigDataSkillInfoList`1_hotfix;
    private LuaFunction m_TickActClientActorActAttachBuff_hotfix;
    private LuaFunction m_TickActClientActorActDetachBuff_hotfix;
    private LuaFunction m_TickActClientActorActImmune_hotfix;
    private LuaFunction m_TickActClientActorActPassiveSkill_hotfix;
    private LuaFunction m_TickActClientActorActPassiveSkillHit_hotfix;
    private LuaFunction m_TickActClientActorActBuffHit_hotfix;
    private LuaFunction m_TickActClientActorActTerrainHit_hotfix;
    private LuaFunction m_TickActClientActorActTeleportDisappear_hotfix;
    private LuaFunction m_TickActClientActorActTeleportAppear_hotfix;
    private LuaFunction m_TickActClientActorActSummon_hotfix;
    private LuaFunction m_TickActClientActorActDie_hotfix;
    private LuaFunction m_TickActClientActorActAppear_hotfix;
    private LuaFunction m_TickActClientActorActDisappear_hotfix;
    private LuaFunction m_TickActClientActorActChangeTeam_hotfix;
    private LuaFunction m_TickActClientActorActChangeArmy_hotfix;
    private LuaFunction m_TickActClientActorActReplace_hotfix;
    private LuaFunction m_TickActClientActorActCameraFocus_hotfix;
    private LuaFunction m_TickActClientActorActGainBattleTreasure_hotfix;
    private LuaFunction m_TickActClientActorActStartGuard_hotfix;
    private LuaFunction m_TickActClientActorActStopGuard_hotfix;
    private LuaFunction m_TickActClientActorActBeforeStartCombat_hotfix;
    private LuaFunction m_TickActClientNullActorActCancelCombat_hotfix;
    private LuaFunction m_TickActClientActorActStartCombat_hotfix;
    private LuaFunction m_TickActClientActorActStopCombat_hotfix;
    private LuaFunction m_TickActClientNullActorActStartBattle_hotfix;
    private LuaFunction m_TickActClientNullActorActBattleWinCondition_hotfix;
    private LuaFunction m_TickActClientNullActorActBattleLoseCondition_hotfix;
    private LuaFunction m_TickActClientNullActorActNextTurn_hotfix;
    private LuaFunction m_TickActClientNullActorActNextTeam_hotfix;
    private LuaFunction m_TickActClientNullActorActNextPlayer_hotfix;
    private LuaFunction m_TickActClientNullActorActNextActor_hotfix;
    private LuaFunction m_TickActClientNullActorActFastCombat_hotfix;
    private LuaFunction m_TickActClientNullActorActBattleDialog_hotfix;
    private LuaFunction m_TickActClientNullActorActStartBattlePerform_hotfix;
    private LuaFunction m_TickActClientNullActorActStopBattlePerform_hotfix;
    private LuaFunction m_TickActClientNullActorActChangeMapTerrain_hotfix;
    private LuaFunction m_TickActClientNullActorActChangeMapTerrainEffect_hotfix;
    private LuaFunction m_TickActClientNullActorActCameraFocus_hotfix;
    private LuaFunction m_TickActClientNullActorActPlayMusic_hotfix;
    private LuaFunction m_TickActClientNullActorActPlaySound_hotfix;
    private LuaFunction m_TickActClientNullActorActPlayFx_hotfix;
    private LuaFunction m_TickActClientNullActorActWaitTime_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle battle, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostRebuildBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateHeroGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSoldierGraphics()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPointMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasFightTag(FightTag ft)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsStun()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FaceTo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAct(ClientActorAct act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAct()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop, bool onAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetHeroAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeroPlayAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayDeathFx(int deathType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayIdleAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayHeroFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, GridPosition p, float rotation = 0.0f, bool canLoop = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAttachFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAttachFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicActionFinishedEffect(bool finished)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGraphicColor(Colori c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AttachBuff(BuffState buffState, List<int> disabledBuffStateIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareBuffDisplayOrder(ClientActorBuff b0, ClientActorBuff b1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DetachBuff(BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHpBarType(int team, bool isAINpcOrTeamMate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartIdleRun()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopIdleRun()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelTeleportDisappear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeSoldierGraphicCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierGraphicCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierGraphicCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGraphicVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeGraphicOffset(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2 ComputeGraphicOffset(
      int idx,
      int dir,
      int soldierCount,
      bool hasHeroAndSoldier)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic GetGraphic(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GetGraphicPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGraphicSkillFade()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHpAndBuffUI(List<int> disabledBuffStateIdList = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndex(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleActor BattleActor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierTotalHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickActDispatch(ClientActorAct act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActActive act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActActionBegin act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActActionEnd act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitMoveAct(GridPosition pos, int dir, bool sound, bool endDelay = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeMoveTime(GridPosition p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActMoveEnd act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTryMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPerformMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPunchMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActExchangeMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSetDir act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPlayFx act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPlayAnimation act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActChangeIdleAnimation act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTarget act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSkill act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActCombineSkill act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSkillHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeSkillCastTime(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeSkillHitTime(
      ConfigDataSkillInfo skillInfo,
      List<ClientActorHitInfo> hits)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickSkillHits(ConfigDataSkillInfo skillInfo, List<ClientActorHitInfo> hits)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActAttachBuff act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActDetachBuff act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActImmune act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPassiveSkill act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPassiveSkillHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActBuffHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTerrainHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTeleportDisappear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTeleportAppear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSummon act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActDie act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActAppear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActDisappear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActChangeTeam act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActChangeArmy act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActReplace act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActCameraFocus act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActGainBattleTreasure act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStartGuard act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStopGuard act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActBeforeStartCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActCancelCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStartCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStopCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActStartBattle act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBattleWinCondition act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBattleLoseCondition act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextTurn act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextTeam act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextPlayer act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextActor act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActFastCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBattleDialog act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActStartBattlePerform act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActStopBattlePerform act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActChangeMapTerrain act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActChangeMapTerrainEffect act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActCameraFocus act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActPlayMusic act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActPlaySound act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActPlayFx act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActWaitTime act)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public ClientBattleActor.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Dispose()
    {
      base.Dispose();
    }

    private void __callBase_Tick()
    {
      base.Tick();
    }

    private void __callBase_TickGraphic(float dt)
    {
      base.TickGraphic(dt);
    }

    private void __callBase_Draw()
    {
      base.Draw();
    }

    private void __callBase_Pause(bool pause)
    {
      this.Pause(pause);
    }

    private void __callBase_DoPause(bool pause)
    {
      base.DoPause(pause);
    }

    private void __callBase_DeleteMe()
    {
      this.DeleteMe();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [CustomLuaClass]
    public class GraphicState
    {
      public GenericGraphic m_graphic;
      public float m_hideCountdown;
    }

    public class LuaExportHelper
    {
      private ClientBattleActor m_owner;

      public LuaExportHelper(ClientBattleActor owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Dispose()
      {
        this.m_owner.__callBase_Dispose();
      }

      public void __callBase_Tick()
      {
        this.m_owner.__callBase_Tick();
      }

      public void __callBase_TickGraphic(float dt)
      {
        this.m_owner.__callBase_TickGraphic(dt);
      }

      public void __callBase_Draw()
      {
        this.m_owner.__callBase_Draw();
      }

      public void __callBase_Pause(bool pause)
      {
        this.m_owner.__callBase_Pause(pause);
      }

      public void __callBase_DoPause(bool pause)
      {
        this.m_owner.__callBase_DoPause(pause);
      }

      public void __callBase_DeleteMe()
      {
        this.m_owner.__callBase_DeleteMe();
      }

      public ClientBattle m_clientBattle
      {
        get
        {
          return this.m_owner.m_clientBattle;
        }
        set
        {
          this.m_owner.m_clientBattle = value;
        }
      }

      public BattleActor m_battleActor
      {
        get
        {
          return this.m_owner.m_battleActor;
        }
        set
        {
          this.m_owner.m_battleActor = value;
        }
      }

      public int m_heroHealthPoint
      {
        get
        {
          return this.m_owner.m_heroHealthPoint;
        }
        set
        {
          this.m_owner.m_heroHealthPoint = value;
        }
      }

      public int m_soldierTotalHealthPoint
      {
        get
        {
          return this.m_owner.m_soldierTotalHealthPoint;
        }
        set
        {
          this.m_owner.m_soldierTotalHealthPoint = value;
        }
      }

      public int m_heroHealthPointMax
      {
        get
        {
          return this.m_owner.m_heroHealthPointMax;
        }
        set
        {
          this.m_owner.m_heroHealthPointMax = value;
        }
      }

      public int m_soldierTotalHealthPointMax
      {
        get
        {
          return this.m_owner.m_soldierTotalHealthPointMax;
        }
        set
        {
          this.m_owner.m_soldierTotalHealthPointMax = value;
        }
      }

      public uint m_fightTags
      {
        get
        {
          return this.m_owner.m_fightTags;
        }
        set
        {
          this.m_owner.m_fightTags = value;
        }
      }

      public ClientBattleActor.GraphicState[] m_graphicStates
      {
        get
        {
          return this.m_owner.m_graphicStates;
        }
        set
        {
          this.m_owner.m_graphicStates = value;
        }
      }

      public int m_soldierGraphicCount
      {
        get
        {
          return this.m_owner.m_soldierGraphicCount;
        }
        set
        {
          this.m_owner.m_soldierGraphicCount = value;
        }
      }

      public Vector2 m_graphicPosition
      {
        get
        {
          return this.m_owner.m_graphicPosition;
        }
        set
        {
          this.m_owner.m_graphicPosition = value;
        }
      }

      public bool m_isGraphicSkillFade
      {
        get
        {
          return this.m_owner.m_isGraphicSkillFade;
        }
        set
        {
          this.m_owner.m_isGraphicSkillFade = value;
        }
      }

      public bool m_isActionFinishedGray
      {
        get
        {
          return this.m_owner.m_isActionFinishedGray;
        }
        set
        {
          this.m_owner.m_isActionFinishedGray = value;
        }
      }

      public BattleActorUIController m_uiController
      {
        get
        {
          return this.m_owner.m_uiController;
        }
        set
        {
          this.m_owner.m_uiController = value;
        }
      }

      public List<ClientActorBuff> m_buffs
      {
        get
        {
          return this.m_owner.m_buffs;
        }
        set
        {
          this.m_owner.m_buffs = value;
        }
      }

      public List<GenericGraphic> m_attachFxs
      {
        get
        {
          return this.m_owner.m_attachFxs;
        }
        set
        {
          this.m_owner.m_attachFxs = value;
        }
      }

      public GridPosition m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public int m_direction
      {
        get
        {
          return this.m_owner.m_direction;
        }
        set
        {
          this.m_owner.m_direction = value;
        }
      }

      public bool m_isVisible
      {
        get
        {
          return this.m_owner.m_isVisible;
        }
        set
        {
          this.m_owner.m_isVisible = value;
        }
      }

      public bool m_isIdleRun
      {
        get
        {
          return this.m_owner.m_isIdleRun;
        }
        set
        {
          this.m_owner.m_isIdleRun = value;
        }
      }

      public bool m_isGuarding
      {
        get
        {
          return this.m_owner.m_isGuarding;
        }
        set
        {
          this.m_owner.m_isGuarding = value;
        }
      }

      public bool m_isTeleportDisappear
      {
        get
        {
          return this.m_owner.m_isTeleportDisappear;
        }
        set
        {
          this.m_owner.m_isTeleportDisappear = value;
        }
      }

      public string m_idleAnimationName
      {
        get
        {
          return this.m_owner.m_idleAnimationName;
        }
        set
        {
          this.m_owner.m_idleAnimationName = value;
        }
      }

      public List<ClientActorAct> m_acts
      {
        get
        {
          return this.m_owner.m_acts;
        }
        set
        {
          this.m_owner.m_acts = value;
        }
      }

      public int m_actFrame
      {
        get
        {
          return this.m_owner.m_actFrame;
        }
        set
        {
          this.m_owner.m_actFrame = value;
        }
      }

      public int m_tempActFrame
      {
        get
        {
          return this.m_owner.m_tempActFrame;
        }
        set
        {
          this.m_owner.m_tempActFrame = value;
        }
      }

      public int m_deleteMeCountdown
      {
        get
        {
          return this.m_owner.m_deleteMeCountdown;
        }
        set
        {
          this.m_owner.m_deleteMeCountdown = value;
        }
      }

      public void CreateHeroGraphic()
      {
        this.m_owner.CreateHeroGraphic();
      }

      public void CreateSoldierGraphics()
      {
        this.m_owner.CreateSoldierGraphics();
      }

      public Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
      {
        return this.m_owner.ComputeGraphicPosition(p, zoffset);
      }

      public bool HasFightTag(FightTag ft)
      {
        return this.m_owner.HasFightTag(ft);
      }

      public void FaceTo(GridPosition p)
      {
        this.m_owner.FaceTo(p);
      }

      public void PlayAnimation(string name, bool loop, bool onAll)
      {
        this.m_owner.PlayAnimation(name, loop, onAll);
      }

      public int GetHeroAnimationDuration(string name)
      {
        return this.m_owner.GetHeroAnimationDuration(name);
      }

      public bool IsHeroPlayAnimation()
      {
        return this.m_owner.IsHeroPlayAnimation();
      }

      public void PlayDeathFx(int deathType)
      {
        this.m_owner.PlayDeathFx(deathType);
      }

      public void PlayIdleAnimation()
      {
        this.m_owner.PlayIdleAnimation();
      }

      public void PlayHeroFx(string name)
      {
        this.m_owner.PlayHeroFx(name);
      }

      public void PlayFx(string name, GridPosition p, float rotation, bool canLoop)
      {
        this.m_owner.PlayFx(name, p, rotation, canLoop);
      }

      public void PlayAttachFx(string name)
      {
        this.m_owner.PlayAttachFx(name);
      }

      public void PlaySound(string name)
      {
        this.m_owner.PlaySound(name);
      }

      public void PlaySound(SoundTableId id)
      {
        this.m_owner.PlaySound(id);
      }

      public void SetGraphicColor(Colori c)
      {
        this.m_owner.SetGraphicColor(c);
      }

      public void AttachBuff(BuffState buffState, List<int> disabledBuffStateIdList)
      {
        this.m_owner.AttachBuff(buffState, disabledBuffStateIdList);
      }

      public static int CompareBuffDisplayOrder(ClientActorBuff b0, ClientActorBuff b1)
      {
        return ClientBattleActor.CompareBuffDisplayOrder(b0, b1);
      }

      public void DetachBuff(BuffState buffState)
      {
        this.m_owner.DetachBuff(buffState);
      }

      public void SetHpBarType(int team, bool isAINpcOrTeamMate)
      {
        this.m_owner.SetHpBarType(team, isAINpcOrTeamMate);
      }

      public int ComputeSoldierGraphicCount()
      {
        return this.m_owner.ComputeSoldierGraphicCount();
      }

      public void UpdateSoldierGraphicCount()
      {
        this.m_owner.UpdateSoldierGraphicCount();
      }

      public void UpdateGraphicVisible()
      {
        this.m_owner.UpdateGraphicVisible();
      }

      public Vector2 ComputeGraphicOffset(int idx)
      {
        return this.m_owner.ComputeGraphicOffset(idx);
      }

      public void UpdateHpAndBuffUI(List<int> disabledBuffStateIdList)
      {
        this.m_owner.UpdateHpAndBuffUI(disabledBuffStateIdList);
      }

      public bool TickActDispatch(ClientActorAct act)
      {
        return this.m_owner.TickActDispatch(act);
      }

      public bool TickAct(ClientActorActActive act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActActionBegin act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActActionEnd act)
      {
        return this.m_owner.TickAct(act);
      }

      public void InitMoveAct(GridPosition pos, int dir, bool sound, bool endDelay)
      {
        this.m_owner.InitMoveAct(pos, dir, sound, endDelay);
      }

      public int ComputeMoveTime(GridPosition p0, GridPosition p1)
      {
        return this.m_owner.ComputeMoveTime(p0, p1);
      }

      public bool TickAct(ClientActorActMove act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActMoveEnd act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActTryMove act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActPerformMove act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActPunchMove act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActExchangeMove act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActSetDir act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActPlayFx act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActPlayAnimation act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActChangeIdleAnimation act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActTarget act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActSkill act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActCombineSkill act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActSkillHit act)
      {
        return this.m_owner.TickAct(act);
      }

      public static int ComputeSkillCastTime(ConfigDataSkillInfo skillInfo)
      {
        return ClientBattleActor.ComputeSkillCastTime(skillInfo);
      }

      public static int ComputeSkillHitTime(
        ConfigDataSkillInfo skillInfo,
        List<ClientActorHitInfo> hits)
      {
        return ClientBattleActor.ComputeSkillHitTime(skillInfo, hits);
      }

      public bool TickSkillHits(ConfigDataSkillInfo skillInfo, List<ClientActorHitInfo> hits)
      {
        return this.m_owner.TickSkillHits(skillInfo, hits);
      }

      public bool TickAct(ClientActorActAttachBuff act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActDetachBuff act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActImmune act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActPassiveSkill act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActPassiveSkillHit act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActBuffHit act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActTerrainHit act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActTeleportDisappear act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActTeleportAppear act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActSummon act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActDie act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActAppear act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActDisappear act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActChangeTeam act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActChangeArmy act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActReplace act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActCameraFocus act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActGainBattleTreasure act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActStartGuard act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActStopGuard act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActBeforeStartCombat act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActCancelCombat act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActStartCombat act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientActorActStopCombat act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActStartBattle act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActBattleWinCondition act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActBattleLoseCondition act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActNextTurn act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActNextTeam act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActNextPlayer act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActNextActor act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActFastCombat act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActBattleDialog act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActStartBattlePerform act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActStopBattlePerform act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActChangeMapTerrain act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActChangeMapTerrainEffect act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActCameraFocus act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActPlayMusic act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActPlaySound act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActPlayFx act)
      {
        return this.m_owner.TickAct(act);
      }

      public bool TickAct(ClientNullActorActWaitTime act)
      {
        return this.m_owner.TickAct(act);
      }
    }
  }
}
