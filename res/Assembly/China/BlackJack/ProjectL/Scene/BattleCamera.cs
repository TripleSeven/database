﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.BattleCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class BattleCamera : CameraBase
  {
    private Vector2 m_position;
    private Vector2 m_smoothMoveStartPosition;
    private Vector2 m_smoothMoveEndPosition;
    private Vector2 m_touchMoveVelocity;
    private bool m_enableTouchMove;
    private bool m_enableTouchMove2;
    private float m_smoothMoveTime;
    private float m_smoothMoveTotalTime;
    private EventSystem m_eventSystem;
    private PointerEventData m_pointData;
    private List<RaycastResult> m_raycastResults;
    private TouchStatus[] m_touchStatus;
    private Vector3 m_initPosition;
    private Vector2 m_mapSizeHalf;
    private float m_viewSizeMin;
    private float m_viewSizeMax;
    [DoNotToLua]
    private BattleCamera.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_InitializeGameObject_hotfix;
    private LuaFunction m_PrepareBattleConfigDataBattlefieldInfo_hotfix;
    private LuaFunction m_StartBattle_hotfix;
    private LuaFunction m_SetOrthographicSizeSingle_hotfix;
    private LuaFunction m_EnableTouchMoveBoolean_hotfix;
    private LuaFunction m_EnableTouchMove2Boolean_hotfix;
    private LuaFunction m_UpdateEnableTouchMove_hotfix;
    private LuaFunction m_UpdateSingle_hotfix;
    private LuaFunction m_IsTouchUIVector2_hotfix;
    private LuaFunction m_UpdateTouchSingle_hotfix;
    private LuaFunction m_ClampCameraPositionVector2_hotfix;
    private LuaFunction m_ComputeBoundPositionVector2_hotfix;
    private LuaFunction m_LookVector2_hotfix;
    private LuaFunction m_SmoothLookVector2_hotfix;
    private LuaFunction m_IsSmoothMoving_hotfix;
    private LuaFunction m_BoundVector2SingleInt32_hotfix;
    private LuaFunction m_GetPosition_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOrthographicSize(float size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTouchMove(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTouchMove2(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEnableTouchMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Vector2 ScreenPositionToViewPosition(
      Vector2 p,
      float viewWidth,
      float viewHeight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTouchUI(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTouch(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ClampCameraPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeBoundPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Look(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SmoothLook(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSmoothMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Bound(Vector2 p, float dt, int speed = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public BattleCamera.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    private void __callBase_Initialize(GameObject cameraGo, GameObject animatorGo)
    {
      this.Initialize(cameraGo, animatorGo);
    }

    private void __callBase_GetViewSize(out float width, out float height)
    {
      this.GetViewSize(out width, out height);
    }

    private void __callBase_PlayAnimation(string name)
    {
      this.PlayAnimation(name);
    }

    private Vector3 __callBase_GetAnimationOffset()
    {
      return this.GetAnimationOffset();
    }

    private bool __callBase_IsCulled(Vector2 p)
    {
      return this.IsCulled(p);
    }

    private bool __callBase_IsCulled(Vector2 bmin, Vector2 bmax)
    {
      return this.IsCulled(bmin, bmax);
    }

    private void __callBase_SetActive(bool a)
    {
      this.SetActive(a);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleCamera m_owner;

      public LuaExportHelper(BattleCamera owner)
      {
        this.m_owner = owner;
      }

      public void __callBase_Initialize(GameObject cameraGo, GameObject animatorGo)
      {
        this.m_owner.__callBase_Initialize(cameraGo, animatorGo);
      }

      public void __callBase_GetViewSize(out float width, out float height)
      {
        this.m_owner.__callBase_GetViewSize(out width, out height);
      }

      public void __callBase_PlayAnimation(string name)
      {
        this.m_owner.__callBase_PlayAnimation(name);
      }

      public Vector3 __callBase_GetAnimationOffset()
      {
        return this.m_owner.__callBase_GetAnimationOffset();
      }

      public bool __callBase_IsCulled(Vector2 p)
      {
        return this.m_owner.__callBase_IsCulled(p);
      }

      public bool __callBase_IsCulled(Vector2 bmin, Vector2 bmax)
      {
        return this.m_owner.__callBase_IsCulled(bmin, bmax);
      }

      public void __callBase_SetActive(bool a)
      {
        this.m_owner.__callBase_SetActive(a);
      }

      public Vector2 m_position
      {
        get
        {
          return this.m_owner.m_position;
        }
        set
        {
          this.m_owner.m_position = value;
        }
      }

      public Vector2 m_smoothMoveStartPosition
      {
        get
        {
          return this.m_owner.m_smoothMoveStartPosition;
        }
        set
        {
          this.m_owner.m_smoothMoveStartPosition = value;
        }
      }

      public Vector2 m_smoothMoveEndPosition
      {
        get
        {
          return this.m_owner.m_smoothMoveEndPosition;
        }
        set
        {
          this.m_owner.m_smoothMoveEndPosition = value;
        }
      }

      public Vector2 m_touchMoveVelocity
      {
        get
        {
          return this.m_owner.m_touchMoveVelocity;
        }
        set
        {
          this.m_owner.m_touchMoveVelocity = value;
        }
      }

      public bool m_enableTouchMove
      {
        get
        {
          return this.m_owner.m_enableTouchMove;
        }
        set
        {
          this.m_owner.m_enableTouchMove = value;
        }
      }

      public bool m_enableTouchMove2
      {
        get
        {
          return this.m_owner.m_enableTouchMove2;
        }
        set
        {
          this.m_owner.m_enableTouchMove2 = value;
        }
      }

      public float m_smoothMoveTime
      {
        get
        {
          return this.m_owner.m_smoothMoveTime;
        }
        set
        {
          this.m_owner.m_smoothMoveTime = value;
        }
      }

      public float m_smoothMoveTotalTime
      {
        get
        {
          return this.m_owner.m_smoothMoveTotalTime;
        }
        set
        {
          this.m_owner.m_smoothMoveTotalTime = value;
        }
      }

      public EventSystem m_eventSystem
      {
        get
        {
          return this.m_owner.m_eventSystem;
        }
        set
        {
          this.m_owner.m_eventSystem = value;
        }
      }

      public PointerEventData m_pointData
      {
        get
        {
          return this.m_owner.m_pointData;
        }
        set
        {
          this.m_owner.m_pointData = value;
        }
      }

      public List<RaycastResult> m_raycastResults
      {
        get
        {
          return this.m_owner.m_raycastResults;
        }
        set
        {
          this.m_owner.m_raycastResults = value;
        }
      }

      public TouchStatus[] m_touchStatus
      {
        get
        {
          return this.m_owner.m_touchStatus;
        }
        set
        {
          this.m_owner.m_touchStatus = value;
        }
      }

      public Vector3 m_initPosition
      {
        get
        {
          return this.m_owner.m_initPosition;
        }
        set
        {
          this.m_owner.m_initPosition = value;
        }
      }

      public Vector2 m_mapSizeHalf
      {
        get
        {
          return this.m_owner.m_mapSizeHalf;
        }
        set
        {
          this.m_owner.m_mapSizeHalf = value;
        }
      }

      public float m_viewSizeMin
      {
        get
        {
          return this.m_owner.m_viewSizeMin;
        }
        set
        {
          this.m_owner.m_viewSizeMin = value;
        }
      }

      public float m_viewSizeMax
      {
        get
        {
          return this.m_owner.m_viewSizeMax;
        }
        set
        {
          this.m_owner.m_viewSizeMax = value;
        }
      }

      public void UpdateEnableTouchMove()
      {
        this.m_owner.UpdateEnableTouchMove();
      }

      public static Vector2 ScreenPositionToViewPosition(
        Vector2 p,
        float viewWidth,
        float viewHeight)
      {
        return BattleCamera.ScreenPositionToViewPosition(p, viewWidth, viewHeight);
      }

      public bool IsTouchUI(Vector2 pos)
      {
        return this.m_owner.IsTouchUI(pos);
      }

      public void UpdateTouch(float dt)
      {
        this.m_owner.UpdateTouch(dt);
      }

      public Vector2 ClampCameraPosition(Vector2 p)
      {
        return this.m_owner.ClampCameraPosition(p);
      }

      public Vector2 ComputeBoundPosition(Vector2 p)
      {
        return this.m_owner.ComputeBoundPosition(p);
      }
    }
  }
}
