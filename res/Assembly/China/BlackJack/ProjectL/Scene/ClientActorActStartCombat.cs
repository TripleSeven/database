﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActStartCombat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using SLua;

namespace BlackJack.ProjectL.Scene
{
  [CustomLuaClass]
  public class ClientActorActStartCombat : ClientActorAct
  {
    public BattleActor m_targetActor;
    public ConfigDataSkillInfo m_attackerSkillInfo;
    public int m_direction;
    public bool m_isAttacker;
  }
}
