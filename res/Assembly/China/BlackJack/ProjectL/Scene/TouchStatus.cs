﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.TouchStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  [HotFix]
  public class TouchStatus
  {
    public Vector2 m_beginPosition;
    public Vector2 m_currentPosition;
    public Vector2 m_prevPosition;
    public bool m_pressed;
    [DoNotToLua]
    private TouchStatus.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public TouchStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public TouchStatus.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private TouchStatus m_owner;

      public LuaExportHelper(TouchStatus owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
