﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class BattleComponentCommon : IComponentBase
  {
    public List<int> m_gotBattleTreasureIds;
    protected IConfigDataLoader m_configDataLoader;
    protected HeroComponentCommon m_hero;
    protected LevelComponentCommon m_level;
    protected ArenaComponentCommon m_arena;
    protected GuildComponentCommon m_guild;
    protected CollectionComponentCommon m_collection;
    protected UnchartedScoreComponentCommon m_unchartedScore;
    protected RealTimePVPComponentCommon m_realtimePVP;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected TeamComponentCommon m_team;
    protected TrainingGroundCompomentCommon m_trainingGround;
    protected ResourceComponentCommon m_resource;
    protected DataSectionBattle m_battleDS;
    protected BattleBase m_battle;
    private BattlePropertyModifier m_propertyModifier;
    private BattleProperty m_battleProperty;
    [DoNotToLua]
    private BattleComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_IsGameFunctionOpenByMonthCardGameFunctionType_hotfix;
    private LuaFunction m_GetDailyBonusAddNumsByMonthCardGameFunctionType_hotfix;
    private LuaFunction m_GetDailyBonusMaxNumsGameFunctionType_hotfix;
    private LuaFunction m_GetSinglePveBattleFightEnergyCost_hotfix;
    private LuaFunction m_GetSinglePveBattleFailEnergyCostByMonthCard_hotfix;
    private LuaFunction m_GetTeamPveBattleFailEnergyCostGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetTeamPveBattleFailEnergyCostByMonthCardGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetFightFailEnergyCostByMonthcardInt32_hotfix;
    private LuaFunction m_AddFightFailCompensationEnergyByMonthCardGameFunctionTypeInt32_hotfix;
    private LuaFunction m_AddTeamPveFightFailCompensationEnergyByMonthCardGameFunctionTypeInt32_hotfix;
    private LuaFunction m_AddSinglePveFightFailCompensationEnergyByMonthCard_hotfix;
    private LuaFunction m_IsWaypointBattling_hotfix;
    private LuaFunction m_IsCollectionWaypointBattling_hotfix;
    private LuaFunction m_OnFlushBattle_hotfix;
    private LuaFunction m_IsBattleWin_hotfix;
    private LuaFunction m_CancelBattle_hotfix;
    private LuaFunction m_IsFighting_hotfix;
    private LuaFunction m_CanSetupMineTeam_hotfix;
    private LuaFunction m_FightFinishedGameFunctionStatusBooleanBoolean_hotfix;
    private LuaFunction m_IsWayPointFightExistInt32_hotfix;
    private LuaFunction m_WinPveBattleInt32_hotfix;
    private LuaFunction m_FinishedArenaFight_hotfix;
    private LuaFunction m_GetProcessingBattle_hotfix;
    private LuaFunction m_SetProcessingBattleInfoBattleTypeInt32_hotfix;
    private LuaFunction m_SetBattleArmyRandomSeedInt32_hotfix;
    private LuaFunction m_GetBattleArmyRandomSeed_hotfix;
    private LuaFunction m_SetArenaBattleInfoInt32_hotfix;
    private LuaFunction m_IsAttackingPveLevelBattleTypeInt32_hotfix;
    private LuaFunction m_IsAttackingArenaOpponent_hotfix;
    private LuaFunction m_IsArenaBattleInReady_hotfix;
    private LuaFunction m_IsAttackingInBattleServer_hotfix;
    private LuaFunction m_FinishBattleInBattleServer_hotfix;
    private LuaFunction m_CanCreateBattleRoomBattleRoomType_hotfix;
    private LuaFunction m_CanCreateTeamBattleRoom_hotfix;
    private LuaFunction m_CanChangePlayerBattleStatusPlayerBattleStatus_hotfix;
    private LuaFunction m_SetArenaBattleFighting_hotfix;
    private LuaFunction m_GetBattleId_hotfix;
    private LuaFunction m_GetBattleIdGameFunctionTypeInt32_hotfix;
    private LuaFunction m_GetMonsterLevel_hotfix;
    private LuaFunction m_AddFightHeroFightNumsAndExpList`1Int32_hotfix;
    private LuaFunction m_GetPveTeam_hotfix;
    private LuaFunction m_GetSubGameTypeIdBattleTypeInt32_hotfix;
    private LuaFunction m_IsMineTeamSetValidInt32Int32List`1_hotfix;
    private LuaFunction m_IsArenaDefensiveTeamSetValidInt32Int32_hotfix;
    private LuaFunction m_IsActionPositionIndexValidConfigDataArenaBattleInfoInt32_hotfix;
    private LuaFunction m_IsActionValueValidConfigDataArenaBattleInfoInt32_hotfix;
    private LuaFunction m_IsTeamEmptyList`1_hotfix;
    private LuaFunction m_SetMineTeamInt32Int32List`1_hotfix;
    private LuaFunction m_SetTeamByIdInt32List`1_hotfix;
    private LuaFunction m_GetGamePlayTeamTypeIdBattleTypeInt32_hotfix;
    private LuaFunction m_FindGamePlayTeamTypeInfoBattleTypeInt32_hotfix;
    private LuaFunction m_GetTeamBattleTypeInt32_hotfix;
    private LuaFunction m_GetTeamByIdInt32_hotfix;
    private LuaFunction m_CreateMineBattleHeroEquipmentsUInt64be_hotfix;
    private LuaFunction m_MineHeroToBattleHeroHero_hotfix;
    private LuaFunction m_ComputeBattlePowerFromBattleHeroesList`1List`1_hotfix;
    private LuaFunction m_ComputeBattlePowerHero_hotfix;
    private LuaFunction m_ComputeBattlePowerHeroUInt64be_hotfix;
    private LuaFunction m_ComputeBattlePowerHeroList`1_hotfix;
    private LuaFunction m_ComputeBattlePowerBattleHeroList`1_hotfix;
    private LuaFunction m_ComputeEquipiemntBattlePowerHeroUInt64be_hotfix;
    private LuaFunction m_CollectJobMasterPropertyModifierList`1BattlePropertyModifier_hotfix;
    private LuaFunction m_CollectEquipmentPropertyModifierAndSkillBattlePowerInt32List`1BattlePropertyModifier_hotfix;
    private LuaFunction m_CollectPassiveSkillStaticPropertyModifierConfigDataSkillInfoBattlePropertyModifier_hotfix;
    private LuaFunction m_CollectStaticPropertyModifierPropertyModifyTypeInt32BattlePropertyModifier_hotfix;
    private LuaFunction m_InitGainBattleTreasures_hotfix;
    private LuaFunction m_AddGotBattleTreasuresInThisBattle_hotfix;
    private LuaFunction m_AddBattleTreasuresList`1_hotfix;
    private LuaFunction m_GetGainBattleTreasuresInThisBattle_hotfix;
    private LuaFunction m_get_BattleRandomSeed_hotfix;
    private LuaFunction m_get_ArmyRandomSeed_hotfix;
    private LuaFunction m_get_ProcessingScenarioId_hotfix;
    private LuaFunction m_get_ArenaBattleRandomSeed_hotfix;
    private LuaFunction m_get_BattleRoomId_hotfix;
    private LuaFunction m_set_BattleRoomIdUInt64_hotfix;
    private LuaFunction m_OnBattlePracticeMissionEvent_hotfix;
    private LuaFunction m_add_BattlePracticeMissionEventAction_hotfix;
    private LuaFunction m_remove_BattlePracticeMissionEventAction_hotfix;
    private LuaFunction m_add_CancelBattleEventAction`2_hotfix;
    private LuaFunction m_remove_CancelBattleEventAction`2_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGameFunctionOpenByMonthCard(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetDailyBonusAddNumsByMonthCard(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyBonusMaxNums(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSinglePveBattleFightEnergyCost()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSinglePveBattleFailEnergyCostByMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeamPveBattleFailEnergyCost(GameFunctionType typeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeamPveBattleFailEnergyCostByMonthCard(GameFunctionType typeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetFightFailEnergyCostByMonthcard(int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddFightFailCompensationEnergyByMonthCard(GameFunctionType typeId, int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTeamPveFightFailCompensationEnergyByMonthCard(
      GameFunctionType typeId,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSinglePveFightFailCompensationEnergyByMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameFunctionType BattleType2GameFunctionType(BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleType GameFunctionType2BattleType(GameFunctionType typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaypointBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionWaypointBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattleWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CancelBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSetupMineTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void FightFinished(GameFunctionStatus status, bool win = false, bool needBatlleLog = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWayPointFightExist(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WinPveBattle(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishedArenaFight()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle GetProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetProcessingBattleInfo(BattleType type, int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleArmyRandomSeed(int armyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleArmyRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetArenaBattleInfo(int arenaBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackingPveLevel(BattleType battleType, int levelId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackingArenaOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaBattleInReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackingInBattleServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishBattleInBattleServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateBattleRoom(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateTeamBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangePlayerBattleStatus(PlayerBattleStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleId(GameFunctionType typeId, int loctionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFightHeroFightNumsAndExp(List<int> heroes, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetPveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSubGameTypeId(BattleType battleType, int processingBattleInfoTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsMineTeamSetValid(int battleId, int gamePlayTeamTypeId, List<int> teamHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsArenaDefensiveTeamSetValid(int battleId, int teamCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActionPositionIndexValid(
      ConfigDataArenaBattleInfo battleInfo,
      int actionPositionIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActionValueValid(ConfigDataArenaBattleInfo battleInfo, int actionValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTeamEmpty(List<int> team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetMineTeam(int battleId, int gamePlayTeamTypeId, List<int> team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamById(int gamePlayTeamTypeId, List<int> team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGamePlayTeamTypeId(BattleType battleType, int subGameTypeId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGamePlayTeamTypeInfo FindGamePlayTeamTypeInfo(
      BattleType battleType,
      int subGameTypeId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetTeam(BattleType battleType, int subGameTypeId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetTeamById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroEquipment> CreateMineBattleHeroEquipments(
      ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero MineHeroToBattleHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePowerFromBattleHeroes(List<BattleHero> heroes, List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, List<EquipmentBagItem> equipments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(BattleHero hero, List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeEquipiemntBattlePower(Hero hero, ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(
      List<BattleHeroJob> jobs,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CollectJobPropertyModifier(
      ConfigDataJobInfo jobInfo,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CollectEquipmentPropertyModifierAndSkillBattlePower(
      int heroId,
      List<BattleHeroEquipment> equipments,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPassiveSkillStaticPropertyModifier(
      ConfigDataSkillInfo skillInfo,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectStaticPropertyModifier(
      PropertyModifyType modifyType,
      int value,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitGainBattleTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGotBattleTreasuresInThisBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddBattleTreasures(List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGainBattleTreasuresInThisBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public int BattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArmyRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ProcessingScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaBattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong BattleRoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattlePracticeMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action BattlePracticeMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType, int> CancelBattleEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public BattleComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_BattlePracticeMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_BattlePracticeMissionEvent()
    {
      this.BattlePracticeMissionEvent = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CancelBattleEvent(BattleType arg1, int arg2)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CancelBattleEvent(BattleType arg1, int arg2)
    {
      this.CancelBattleEvent = (Action<BattleType, int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private BattleComponentCommon m_owner;

      public LuaExportHelper(BattleComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_BattlePracticeMissionEvent()
      {
        this.m_owner.__callDele_BattlePracticeMissionEvent();
      }

      public void __clearDele_BattlePracticeMissionEvent()
      {
        this.m_owner.__clearDele_BattlePracticeMissionEvent();
      }

      public void __callDele_CancelBattleEvent(BattleType arg1, int arg2)
      {
        this.m_owner.__callDele_CancelBattleEvent(arg1, arg2);
      }

      public void __clearDele_CancelBattleEvent(BattleType arg1, int arg2)
      {
        this.m_owner.__clearDele_CancelBattleEvent(arg1, arg2);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public LevelComponentCommon m_level
      {
        get
        {
          return this.m_owner.m_level;
        }
        set
        {
          this.m_owner.m_level = value;
        }
      }

      public ArenaComponentCommon m_arena
      {
        get
        {
          return this.m_owner.m_arena;
        }
        set
        {
          this.m_owner.m_arena = value;
        }
      }

      public GuildComponentCommon m_guild
      {
        get
        {
          return this.m_owner.m_guild;
        }
        set
        {
          this.m_owner.m_guild = value;
        }
      }

      public CollectionComponentCommon m_collection
      {
        get
        {
          return this.m_owner.m_collection;
        }
        set
        {
          this.m_owner.m_collection = value;
        }
      }

      public UnchartedScoreComponentCommon m_unchartedScore
      {
        get
        {
          return this.m_owner.m_unchartedScore;
        }
        set
        {
          this.m_owner.m_unchartedScore = value;
        }
      }

      public RealTimePVPComponentCommon m_realtimePVP
      {
        get
        {
          return this.m_owner.m_realtimePVP;
        }
        set
        {
          this.m_owner.m_realtimePVP = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public TeamComponentCommon m_team
      {
        get
        {
          return this.m_owner.m_team;
        }
        set
        {
          this.m_owner.m_team = value;
        }
      }

      public TrainingGroundCompomentCommon m_trainingGround
      {
        get
        {
          return this.m_owner.m_trainingGround;
        }
        set
        {
          this.m_owner.m_trainingGround = value;
        }
      }

      public ResourceComponentCommon m_resource
      {
        get
        {
          return this.m_owner.m_resource;
        }
        set
        {
          this.m_owner.m_resource = value;
        }
      }

      public DataSectionBattle m_battleDS
      {
        get
        {
          return this.m_owner.m_battleDS;
        }
        set
        {
          this.m_owner.m_battleDS = value;
        }
      }

      public BattleBase m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BattlePropertyModifier m_propertyModifier
      {
        get
        {
          return this.m_owner.m_propertyModifier;
        }
        set
        {
          this.m_owner.m_propertyModifier = value;
        }
      }

      public BattleProperty m_battleProperty
      {
        get
        {
          return this.m_owner.m_battleProperty;
        }
        set
        {
          this.m_owner.m_battleProperty = value;
        }
      }

      public int GetDailyBonusAddNumsByMonthCard(GameFunctionType gameFuncTypeId)
      {
        return this.m_owner.GetDailyBonusAddNumsByMonthCard(gameFuncTypeId);
      }

      public int GetFightFailEnergyCostByMonthcard(int energyCost)
      {
        return this.m_owner.GetFightFailEnergyCostByMonthcard(energyCost);
      }

      public void AddFightFailCompensationEnergyByMonthCard(GameFunctionType typeId, int energyCost)
      {
        this.m_owner.AddFightFailCompensationEnergyByMonthCard(typeId, energyCost);
      }

      public void AddSinglePveFightFailCompensationEnergyByMonthCard()
      {
        this.m_owner.AddSinglePveFightFailCompensationEnergyByMonthCard();
      }

      public void OnFlushBattle()
      {
        this.m_owner.OnFlushBattle();
      }

      public bool IsTeamEmpty(List<int> team)
      {
        return this.m_owner.IsTeamEmpty(team);
      }

      public void CollectJobMasterPropertyModifier(
        List<BattleHeroJob> jobs,
        BattlePropertyModifier pm)
      {
        this.m_owner.CollectJobMasterPropertyModifier(jobs, pm);
      }

      public static void CollectJobPropertyModifier(
        ConfigDataJobInfo jobInfo,
        BattlePropertyModifier pm)
      {
        BattleComponentCommon.CollectJobPropertyModifier(jobInfo, pm);
      }

      public int CollectEquipmentPropertyModifierAndSkillBattlePower(
        int heroId,
        List<BattleHeroEquipment> equipments,
        BattlePropertyModifier pm)
      {
        return this.m_owner.CollectEquipmentPropertyModifierAndSkillBattlePower(heroId, equipments, pm);
      }

      public void CollectPassiveSkillStaticPropertyModifier(
        ConfigDataSkillInfo skillInfo,
        BattlePropertyModifier pm)
      {
        this.m_owner.CollectPassiveSkillStaticPropertyModifier(skillInfo, pm);
      }

      public void CollectStaticPropertyModifier(
        PropertyModifyType modifyType,
        int value,
        BattlePropertyModifier pm)
      {
        this.m_owner.CollectStaticPropertyModifier(modifyType, value, pm);
      }

      public void InitGainBattleTreasures()
      {
        this.m_owner.InitGainBattleTreasures();
      }
    }
  }
}
