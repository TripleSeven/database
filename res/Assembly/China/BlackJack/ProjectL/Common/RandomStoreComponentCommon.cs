﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RandomStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected DataSectionRandomStore m_randomStoreDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "RandomStore";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushManualFlushNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAutoFlushStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsBoughtStoreItem(RandomStoreItem storeItem)
    {
      return storeItem.Bought;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanManualFlushStore(RandomStore store, ConfigDataRandomStoreInfo storeInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyRandomStoreItem(int storeId, int index, int selectedIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBuyStoreItem(int storeId, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
