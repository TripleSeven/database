﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CooperateBattleCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class CooperateBattleCompomentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected DataSectionCooperateBattle m_cooperateBattleDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected RiftComponentCommon m_rift;
    [DoNotToLua]
    private CooperateBattleCompomentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_CheckCooperateBattleAvailableInt32Int32__hotfix;
    private LuaFunction m_CheckCooperateBattleDisplayableInt32_hotfix;
    private LuaFunction m_IsLevelUnlockedInt32_hotfix;
    private LuaFunction m_IsLevelFinishedInt32_hotfix;
    private LuaFunction m_GetAllUnlockedLevels_hotfix;
    private LuaFunction m_GetAllFinishedLevels_hotfix;
    private LuaFunction m_get_OperationalActivityDailyRewardNums_hotfix;
    private LuaFunction m_set_OperationalActivityDailyRewardNumsInt32_hotfix;
    private LuaFunction m_get_HasRewardAddRelativeOperationalActivity_hotfix;
    private LuaFunction m_set_HasRewardAddRelativeOperationalActivityBoolean_hotfix;
    private LuaFunction m_get_HasDailyExtraReward_hotfix;
    private LuaFunction m_set_HasDailyExtraRewardBoolean_hotfix;
    private LuaFunction m_FlushChallengNums_hotfix;
    private LuaFunction m_GetDailyChallengeNumsInt32_hotfix;
    private LuaFunction m_CheckCooperateBattleLevelAvailableInt32Int32Int32__hotfix;
    private LuaFunction m_CheckPlayerOutOfBattleInt32__hotfix;
    private LuaFunction m_CheckEnergyInt32Int32Int32__hotfix;
    private LuaFunction m_CheckBagInt32Int32Int32__hotfix;
    private LuaFunction m_CanAttackCooperateBattleLevelInt32Int32_hotfix;
    private LuaFunction m_IsGameFunctionOpened_hotfix;
    private LuaFunction m_CanAttackCooperateBattleLevelInt32_hotfix;
    private LuaFunction m_SetCommonSuccessCooperateBattleLevelConfigDataCooperateBattleLevelInfoList`1List`1_hotfix;
    private LuaFunction m_FinishedCooperateBattleLevelCooperateBattleLevelList`1_hotfix;
    private LuaFunction m_get_m_configDataLoader_hotfix;
    private LuaFunction m_set_m_configDataLoaderIConfigDataLoader_hotfix;
    private LuaFunction m_add_CompleteCooperateBattleMissionEventAction`3_hotfix;
    private LuaFunction m_remove_CompleteCooperateBattleMissionEventAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckCooperateBattleAvailable(int CooperateBattleID, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckCooperateBattleDisplayable(int CooperateBattleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelUnlocked(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllFinishedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    public int OperationalActivityDailyRewardNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasRewardAddRelativeOperationalActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDailyExtraReward
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushChallengNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengeNums(int BattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckCooperateBattleLevelAvailable(int BattleId, int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckPlayerOutOfBattle(ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckEnergy(int BattleId, int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckBag(int CooperateBattleId, int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCooperateBattleLevel(int BattleId, int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGameFunctionOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackCooperateBattleLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessCooperateBattleLevel(
      ConfigDataCooperateBattleLevelInfo Level,
      List<int> Heroes,
      List<int> BattleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void FinishedCooperateBattleLevel(
      CooperateBattleLevel Level,
      List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType, int, List<int>> CompleteCooperateBattleMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public CooperateBattleCompomentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteCooperateBattleMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteCooperateBattleMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      this.CompleteCooperateBattleMissionEvent = (Action<BattleType, int, List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private CooperateBattleCompomentCommon m_owner;

      public LuaExportHelper(CooperateBattleCompomentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteCooperateBattleMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__callDele_CompleteCooperateBattleMissionEvent(arg1, arg2, arg3);
      }

      public void __clearDele_CompleteCooperateBattleMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__clearDele_CompleteCooperateBattleMissionEvent(arg1, arg2, arg3);
      }

      public IConfigDataLoader _configDataLoader
      {
        get
        {
          return this.m_owner._configDataLoader;
        }
        set
        {
          this.m_owner._configDataLoader = value;
        }
      }

      public DataSectionCooperateBattle m_cooperateBattleDS
      {
        get
        {
          return this.m_owner.m_cooperateBattleDS;
        }
        set
        {
          this.m_owner.m_cooperateBattleDS = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public void FlushChallengNums()
      {
        this.m_owner.FlushChallengNums();
      }

      public bool IsGameFunctionOpened()
      {
        return this.m_owner.IsGameFunctionOpened();
      }

      public void FinishedCooperateBattleLevel(CooperateBattleLevel Level, List<int> heroes)
      {
        this.m_owner.FinishedCooperateBattleLevel(Level, heroes);
      }
    }
  }
}
