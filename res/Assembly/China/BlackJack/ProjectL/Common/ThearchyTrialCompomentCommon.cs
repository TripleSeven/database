﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ThearchyTrialCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class ThearchyTrialCompomentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionThearchyTrail m_thearchyTrial;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    [DoNotToLua]
    private ThearchyTrialCompomentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_AttackThearchyTrialLevelInt32_hotfix;
    private LuaFunction m_IsLevelFinishedInt32_hotfix;
    private LuaFunction m_IsGameFunctionOpened_hotfix;
    private LuaFunction m_CanAttackThearchyTrialLevelInt32Boolean_hotfix;
    private LuaFunction m_CheckThearchyTrialOpenedInt32_hotfix;
    private LuaFunction m_CanOpenLevelInt32_hotfix;
    private LuaFunction m_CanAttackLevelByEnergyAndSoOnConfigDataThearchyTrialLevelInfoBoolean_hotfix;
    private LuaFunction m_SetCommonSuccessThearchyTrialLevelConfigDataThearchyTrialLevelInfoList`1List`1Int32Boolean_hotfix;
    private LuaFunction m_GetAllFinishedLevels_hotfix;
    private LuaFunction m_IsBlessingInt32_hotfix;
    private LuaFunction m_get_HasRewardAddRelativeOperationalActivity_hotfix;
    private LuaFunction m_set_HasRewardAddRelativeOperationalActivityBoolean_hotfix;
    private LuaFunction m_get_OperationalActivityDailyRewardNums_hotfix;
    private LuaFunction m_set_OperationalActivityDailyRewardNumsInt32_hotfix;
    private LuaFunction m_get_HasDailyExtraReward_hotfix;
    private LuaFunction m_set_HasDailyExtraRewardBoolean_hotfix;
    private LuaFunction m_Flush_hotfix;
    private LuaFunction m_IsDailyChallenge_hotfix;
    private LuaFunction m_GetDailyChallengNums_hotfix;
    private LuaFunction m_AddChallengedNumsInt32_hotfix;
    private LuaFunction m_GetAllUnlockedLevels_hotfix;
    private LuaFunction m_IsLevelUnlockedInt32_hotfix;
    private LuaFunction m_add_CompleteThearchyTrialMissionEventAction`3_hotfix;
    private LuaFunction m_remove_CompleteThearchyTrialMissionEventAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ThearchyTrialCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackThearchyTrialLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGameFunctionOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackThearchyTrialLevel(int levelId, bool isTeamBattle = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckThearchyTrialOpened(int thearchyTrialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanOpenLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanAttackLevelByEnergyAndSoOn(
      ConfigDataThearchyTrialLevelInfo levelInfo,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetCommonSuccessThearchyTrialLevel(
      ConfigDataThearchyTrialLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetAllFinishedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasRewardAddRelativeOperationalActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OperationalActivityDailyRewardNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDailyExtraReward
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Flush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDailyChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddChallengedNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelUnlocked(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteThearchyTrialMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ThearchyTrialCompomentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteThearchyTrialMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteThearchyTrialMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      this.CompleteThearchyTrialMissionEvent = (Action<BattleType, int, List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ThearchyTrialCompomentCommon m_owner;

      public LuaExportHelper(ThearchyTrialCompomentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteThearchyTrialMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__callDele_CompleteThearchyTrialMissionEvent(arg1, arg2, arg3);
      }

      public void __clearDele_CompleteThearchyTrialMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__clearDele_CompleteThearchyTrialMissionEvent(arg1, arg2, arg3);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public DataSectionThearchyTrail m_thearchyTrial
      {
        get
        {
          return this.m_owner.m_thearchyTrial;
        }
        set
        {
          this.m_owner.m_thearchyTrial = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public RiftComponentCommon m_rift
      {
        get
        {
          return this.m_owner.m_rift;
        }
        set
        {
          this.m_owner.m_rift = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public bool IsGameFunctionOpened()
      {
        return this.m_owner.IsGameFunctionOpened();
      }

      public int CanAttackLevelByEnergyAndSoOn(
        ConfigDataThearchyTrialLevelInfo levelInfo,
        bool isTeamBattle)
      {
        return this.m_owner.CanAttackLevelByEnergyAndSoOn(levelInfo, isTeamBattle);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetCommonSuccessThearchyTrialLevel(
        ConfigDataThearchyTrialLevelInfo levelInfo,
        List<int> battleTreasures,
        List<int> heroes,
        int energyCost,
        bool isBattleTeam)
      {
        // ISSUE: unable to decompile the method.
      }

      public void Flush()
      {
        this.m_owner.Flush();
      }

      public void AddChallengedNums(int nums)
      {
        this.m_owner.AddChallengedNums(nums);
      }
    }
  }
}
