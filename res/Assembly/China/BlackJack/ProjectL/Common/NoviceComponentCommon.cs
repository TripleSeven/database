﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.NoviceComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class NoviceComponentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected List<ConfigDataNoviceRewardInfo> m_novicePointsRewards;
    public TimeSpan m_noviceMissionDuration;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected MissionComponentCommon m_mission;
    protected DataSectionNovice m_noviceDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public NoviceComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Novice";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    public virtual void Tick(uint deltaMillisecond)
    {
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    public int GetMissionPoints()
    {
      return this.m_noviceDS.MissionPoints;
    }

    public void AddMissionPoints(int Delta)
    {
      this.m_noviceDS.AddMissionPoints(Delta);
    }

    public List<int> GetRewardClaimedSlots()
    {
      return this.m_noviceDS.RewardClaimedSlots;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardClaimed(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanClaimReward(int Slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ClaimReward(int Slot, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ConfigDataNoviceRewardInfo> GetNovicePointsRewardsConfig()
    {
      return this.m_novicePointsRewards;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDaysAfterCreation()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Mission> GetProcessingMissions()
    {
      return this.m_mission.GetAllProcessingNoviceMissionList();
    }

    public List<Mission> GetFinishedMissions()
    {
      return this.m_mission.GetAllFinishedNoviceMissionList();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionDay(ConfigDataMissionInfo Mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<int>> GetMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TimeSpan GetMissionsEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      get
      {
        return this._configDataLoader;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
