﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaComponentCommon : IComponentBase
  {
    protected DataSectionArena m_arenaDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected RiftComponentCommon m_rift;
    protected IConfigDataLoader m_configDataLoader;
    public Action<int> ArenaConsecutiveVictoryEvent;
    public Action<BattleType, int, List<int>> ArenaAttackMissionEvent;
    public Action ArenaFightEvent;

    public string GetName()
    {
      return "Arena";
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnBattleTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFinishedArenaBattle(
      ArenaOpponentDefensiveBattleInfo defensivBattleInfo,
      bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetThisWeekBattleInfo(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetThisWeekBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    protected void InitArenaPlayerInfo(ArenaPlayerInfo info)
    {
      this.m_arenaDS.InitArenaPlayerInfo(info);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddReceivedVictoryPointsRewardIndex(int victoryPointsIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFlushVictoryPointsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnArenaGiveTicketsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SystemGiveArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaOpponentDefensiveBattleInfo GetArenaDefensiveBattleInfo()
    {
      return this.m_arenaDS.GetArenaDefensiveBattleInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSuccessArenaBattle(int arenaOpponentPointZoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaBattleReport(ArenaBattleReport arenaBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaLevelIdAndPoints(int arenaLevelId, ushort arenaPoints)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static byte GetNextBattleReportIndex(
      byte currentNextBattleReportIndex,
      int arenaBattleReportMaxNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetVictoryPointsReward(int victoryPointsRewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFullBattleReportNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanViewOpponent(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanAttackOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackOpponentByClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRevengeOpponent(ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetBattleReportRevenged(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanFlushOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInSettleCacheTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsInRealWeekSettleTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInSettleTime(
      IConfigDataLoader configDataLoader,
      int offsetSeconds,
      DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool CanFlushArenaByWeek(
      DateTime current,
      DateTime lastFlushTime,
      int relativeSeconds)
    {
      return current >= ArenaComponentCommon.CalculateWeekNextFlushTime(lastFlushTime, relativeSeconds);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static DateTime CalculateWeekNextFlushTime(
      DateTime lastFlushTime,
      int relativeSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsArenaOpen()
    {
      return this.m_basicInfo.IsGameFunctionOpened(GameFunctionType.GameFunctionType_ArenaBattle);
    }

    public bool IsEmptyArenaPlayerInfo()
    {
      return this.m_arenaDS.IsEmptyArenaPlayerInfo();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaInited()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsVictoryPointsRewardGot(int victoryPointsRewardIndex)
    {
      return this.m_arenaDS.HasReceivedVictoryPointsRewardedIndex(victoryPointsRewardIndex);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackOpponentFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ArenaBattleFinish(GameFunctionStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsFirstFindOpponents
    {
      get
      {
        return this.m_arenaDS.IsFirstFindOpponents();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetUpFirstDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetMineRank(int rank)
    {
      this.ArenaPlayerInfo.MineRank = rank;
    }

    public int GetMineRank()
    {
      return this.ArenaPlayerInfo.MineRank;
    }

    public ArenaPlayerInfo ArenaPlayerInfo
    {
      get
      {
        return this.m_arenaDS.ArenaPlayerInfo;
      }
    }

    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      return this.m_arenaBattleReportDS.GetAllArenaBattleReports();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ArenaConsecutiveSuccess(int consecutiveVictoryNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetArenaTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReconnectArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReconnectArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public DataSectionArenaBattleReport m_arenaBattleReportDS { get; private set; }
  }
}
