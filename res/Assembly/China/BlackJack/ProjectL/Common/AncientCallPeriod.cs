﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCallPeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AncientCallPeriod
  {
    public List<AncientCallBoss> Bosses;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallPeriod()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CurrentPeriodId { get; set; }

    public int CurrentBossIndex { get; set; }

    public int Score { get; set; }

    public DateTime UpdateTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCallPeriod FromPB(ProAncientCallPeriod pbPeriod)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCallPeriod ToPB(AncientCallPeriod period)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossCurrentPeriodMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateScore(int score, DateTime updateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateBossDamage(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss FindBossById(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentBoss(int currentBossIndex)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
