﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionAncientCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionAncientCall : DataSection
  {
    public AncientCall AncientCallInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionAncientCall()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.AncientCallInfo = new AncientCall();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAncientCallAchievement(List<int> newAchievementRelationIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBossDamage(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRealTimePeriodRank(int bossId, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateScore(int score, DateTime updateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public AncientCallBoss FindBossById(int bossId)
    {
      return this.AncientCallInfo.FindBossById(bossId);
    }

    public bool IsCurrentPeriodExist()
    {
      return this.AncientCallInfo.IsCurrentPeriodExist();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossCurrentPeriodMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentPeriodScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
