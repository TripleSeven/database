﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAnthemComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class HeroAnthemComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected DataSectionHeroAnthem m_heroAnthemDS;
    [DoNotToLua]
    private HeroAnthemComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_GetAllHeroAnthemAchievements_hotfix;
    private LuaFunction m_GetAllHeroAnthemLevelAchievements_hotfix;
    private LuaFunction m_GetChapterHeroAnthemAchievementsInt32_hotfix;
    private LuaFunction m_GetHeroAnthemLevelAchievementsInt32_hotfix;
    private LuaFunction m_HasGotAchievementRelationIdInt32_hotfix;
    private LuaFunction m_IsFirstPassLevelInt32_hotfix;
    private LuaFunction m_IsLevelFinishedInt32_hotfix;
    private LuaFunction m_SetSuccessHeroAnthemLevelConfigDataHeroAnthemLevelInfoList`1List`1_hotfix;
    private LuaFunction m_AddHasCompleteLevelConfigDataHeroAnthemLevelInfo_hotfix;
    private LuaFunction m_CompleteAchievementConfigDataHeroAnthemLevelInfoInt32_hotfix;
    private LuaFunction m_AttackLevelInt32_hotfix;
    private LuaFunction m_IsGameFunctionOpened_hotfix;
    private LuaFunction m_CanAttackLevelInt32_hotfix;
    private LuaFunction m_CanAttackLevelByEnergyAndSoOnConfigDataHeroAnthemLevelInfo_hotfix;
    private LuaFunction m_CanUnLockLevelInt32_hotfix;
    private LuaFunction m_OnHeroAnthemAchivementAddInt32_hotfix;
    private LuaFunction m_add_CompleteHeroAnthemMissionEventAction`3_hotfix;
    private LuaFunction m_remove_CompleteHeroAnthemMissionEventAction`3_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAnthemComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllHeroAnthemAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetAllHeroAnthemLevelAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterHeroAnthemAchievements(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAnthemLevelAchievements(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotAchievementRelationId(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstPassLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetSuccessHeroAnthemLevel(
      ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo,
      List<int> newAchievementIds,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddHasCompleteLevel(ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void CompleteAchievement(
      ConfigDataHeroAnthemLevelInfo heroAnthemLevelInfo,
      int achievementRelationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGameFunctionOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(ConfigDataHeroAnthemLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnLockLevel(int riftLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroAnthemAchivementAdd(int addAchivement)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteHeroAnthemMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public HeroAnthemComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteHeroAnthemMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteHeroAnthemMissionEvent(
      BattleType arg1,
      int arg2,
      List<int> arg3)
    {
      this.CompleteHeroAnthemMissionEvent = (Action<BattleType, int, List<int>>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private HeroAnthemComponentCommon m_owner;

      public LuaExportHelper(HeroAnthemComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteHeroAnthemMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__callDele_CompleteHeroAnthemMissionEvent(arg1, arg2, arg3);
      }

      public void __clearDele_CompleteHeroAnthemMissionEvent(
        BattleType arg1,
        int arg2,
        List<int> arg3)
      {
        this.m_owner.__clearDele_CompleteHeroAnthemMissionEvent(arg1, arg2, arg3);
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public DataSectionHeroAnthem m_heroAnthemDS
      {
        get
        {
          return this.m_owner.m_heroAnthemDS;
        }
        set
        {
          this.m_owner.m_heroAnthemDS = value;
        }
      }

      public bool IsGameFunctionOpened()
      {
        return this.m_owner.IsGameFunctionOpened();
      }

      public int CanAttackLevelByEnergyAndSoOn(ConfigDataHeroAnthemLevelInfo levelInfo)
      {
        return this.m_owner.CanAttackLevelByEnergyAndSoOn(levelInfo);
      }

      public void OnHeroAnthemAchivementAdd(int addAchivement)
      {
        this.m_owner.OnHeroAnthemAchivementAdd(addAchivement);
      }
    }
  }
}
