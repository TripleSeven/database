﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroCommentEntry
  {
    public HeroCommentEntry()
    {
      this.CommentTime = 0L;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentEntry(HeroCommentEntry other)
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId { get; set; }

    public string Content { get; set; }

    public string CommenterUserId { get; set; }

    public string CommenterName { get; set; }

    public int CommenterLevel { get; set; }

    public int PraiseNums { get; set; }

    public long CommentTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroCommentEntry HeroCommentEntryToPBHeroCommentEntry(
      HeroCommentEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroCommentEntry PBHeroCommentEntryToHeroCommentEntry(
      ProHeroCommentEntry pbEntry)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
