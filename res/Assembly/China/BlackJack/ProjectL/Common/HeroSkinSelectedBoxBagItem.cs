﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroSkinSelectedBoxBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroSkinSelectedBoxBagItem : UseableBagItem
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSkinSelectedBoxBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    public override int HaveEffect(IComponentOwner owner, params object[] param)
    {
      return this.Result;
    }

    private int Result { get; set; }
  }
}
