﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RankingPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RankingPlayerInfo
  {
    public List<RankingPlayerAncientCallBossInfo> AncientCallBossInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    public string Name { get; set; }

    public int ChampionHeroId { get; set; }

    public int HeroActivatedJobId { get; set; }

    public int HeroActivatedJobLevel { get; set; }

    public void ClearAncientCallBossInfos()
    {
      this.AncientCallBossInfos.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOrUpdateBossInfo(int bossId, List<int> teamList, int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRankingPlayerInfo RankingPlayerToPBRankingPlayer(
      RankingPlayerInfo playerInfo,
      int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RankingPlayerInfo PBRankingPlayerToRankingPlayer(
      ProRankingPlayerInfo proPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
