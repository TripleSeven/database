﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public static class PeakArenaExtensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaMatchStats ToPro(this PeakArenaMatchStats Stats)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaMatchStats ToMemory(this ProPeakArenaMatchStats Stats)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaHiredHero ToPro(this PeakArenaHiredHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaHiredHero ToMemory(this ProPeakArenaHiredHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaHiredHero> ToMemory(
      this List<ProPeakArenaHiredHero> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleTeam ToPro(this PeakArenaBattleTeam bt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBattleTeam ToMemory(this ProPeakArenaBattleTeam bt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaHeroWithEquipments ToMemory(
      this ProHeroWithEquipments hero,
      BagItemFactory factory)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaHeroWithEquipments> ToMemory(
      this List<ProHeroWithEquipments> heroes,
      BagItemFactory factory)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArena ToPro(this DataSectionPeakArena ds)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsWinnerDecided(this string winnerId)
    {
      return winnerId != (string) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsHumanPlayer(this string userId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
