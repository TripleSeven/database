﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerExpUseableBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class PlayerExpUseableBagItem : UseableBagItem
  {
    public PlayerExpUseableBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
      : base(goodsTypeId, contentId, nums, instanceId)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int HaveEffect(IComponentOwner owner, params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    public int PlayerExp { get; set; }
  }
}
