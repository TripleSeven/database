﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Mission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class Mission
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public Mission(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int MissionId { get; set; }

    public long CompletedProcess { get; set; }

    public ConfigDataMissionInfo Config { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Mission PBMissionToMission(ProMission pbMission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<Mission> PBMissionListToMissionList(List<ProMission> pbMissionList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProMission MissionToPBMission(Mission misson)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProMission> MissionListToPBMissionList(
      List<Mission> missionList)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
