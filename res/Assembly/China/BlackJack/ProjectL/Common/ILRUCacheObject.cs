﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ILRUCacheObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.ProjectL.Common
{
  public interface ILRUCacheObject
  {
    void SetLastUpdateTime();

    long GetLastUpdateTime();

    void SetLastReadTime();

    long GetLastReadTime();

    long GetNewestTime();
  }
}
