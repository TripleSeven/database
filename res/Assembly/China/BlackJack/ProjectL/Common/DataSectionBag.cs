﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionBag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionBag : DataSection
  {
    private Dictionary<ulong, int> m_instanceId2CacheIndex;
    private const int MaxBagNumsPerNtf = 1000;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionBag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<object> SerializeMultipleToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBagItem(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBagItem(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase SetBagItemNums(BagItemBase bagItem, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase RemoveBagItem(BagItemBase bagItem, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItem(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBagItemDirty(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItem(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindBagItemIndex(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UpdateCache<BagItemBase> FindCache(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Size()
    {
      return this.Bag.ValidCount;
    }

    public List<BagItemBase> GetAllBagItems()
    {
      return this.Bag.GetAllVaildDatas();
    }

    public IEnumerable<BagItemBase> IterateAllBagItems()
    {
      return this.Bag.IterateAllBagItems();
    }

    public Dictionary<ulong, int> InstanceId2CacheIndex
    {
      get
      {
        return this.m_instanceId2CacheIndex;
      }
    }

    public BagItemUpdateCache Bag { get; set; }
  }
}
