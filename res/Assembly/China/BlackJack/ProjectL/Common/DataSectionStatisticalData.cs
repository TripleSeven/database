﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionStatisticalData : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionStatisticalData()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsExistStatisticalData(int typeId)
    {
      return this.StatisticalData.ContainsKey(typeId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStatisticalData(int typeId, long nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewStatisticalData(int typeId, long nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetStatisticalDataByTypeId(int typeId)
    {
      return this.StatisticalData[typeId];
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStatisticalData(Dictionary<int, long> dataDict)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, long> StatisticalData { get; set; }
  }
}
