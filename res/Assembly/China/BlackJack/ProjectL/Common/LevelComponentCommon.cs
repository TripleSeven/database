﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.LevelComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class LevelComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    protected DataSectionLevel m_levelDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GetName()
    {
      return "Level";
    }

    public virtual void Init()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void DeInit()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool Serialize<T>(T dest)
    {
      return false;
    }

    public virtual void DeSerialize<T>(T source)
    {
    }

    public virtual void PostDeSerialize()
    {
    }

    public IComponentOwner Owner { get; set; }

    public virtual void TryGenerateActivityEvent(OperationalActivityBase activity)
    {
    }

    public bool IsWayPointArrived(int wayPointId)
    {
      return this.m_levelDS.IsWayPointArrived(wayPointId);
    }

    protected virtual void OutPutRandomEventOperateLog(
      int eventId,
      RandomEventStatus status,
      List<Goods> rewards = null)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLevelInfo(List<int> arrivedWayPoints)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetArrivedWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ArriveWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool HasFirstWayPointWithScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCanMovePublicWayPoint(int newId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetFinishedScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetLastFinishedScenarioId()
    {
      return this.m_levelDS.LastFinishedScenarioId;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioCompleted(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScenarioId()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void SetBattleWayPointSuccessful(
      ConfigDataWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
    }

    private bool IsExistRandomEvent()
    {
      return this.m_levelDS.IsExistRandomEvent();
    }

    public bool IsSetRandomEvent(int wayPointId)
    {
      return this.m_levelDS.IsSetRandomEvent(wayPointId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitRandomEvents(List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreInitRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected long GetEventExpiredTime(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEventLives(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRandomEventTimeOut(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRandomEventDead(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void RemoveRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddRandomEvent(RandomEvent randomEvent, bool isActivityEvent = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnLockScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleScenario(int scenarioId, bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsScenarioFinished(ConfigDataScenarioInfo secenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioInWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public RandomEvent GetRandomEvent(int wayPointId)
    {
      return this.m_levelDS.GetRandomEvent(wayPointId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEventId(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual int CheckGameFunctionOpenByGM(GameFunctionType gameFunctionTypeId)
    {
      return 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HandleEventWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventComplete(int wayPointId, RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual int HandleDialogEvent(
      ConfigDataWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int expReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      return 0;
    }

    protected virtual int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      return 0;
    }

    protected virtual List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
    {
      return eventInfo.Reward;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AttackScenario(
      ConfigDataScenarioInfo secenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackScenario(
      ConfigDataScenarioInfo scenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HandleAttackWayPointEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AttackEventWayPoint(int wayPointId, ConfigDataEventInfo eventyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWayPoint(int wayPointdId, ConfigDataEventInfo eventyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanMoveToWayPoint(int destWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanMoveToWayPointExistRandomEvent(int destWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void OnCompleteWayPointEvent()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRegionOpen(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnOpenRegion(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWayPointStatus(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> CompleteEventMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteScenarioMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteNewScenarioMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
