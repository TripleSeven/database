﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMassiveCombatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class GuildMassiveCombatInfo
  {
    public List<GuildMassiveCombatStronghold> Strongholds;
    public List<GuildMassiveCombatMemberInfo> Members;
    public DateTime CreateTime;
    public DateTime FinishTime;
    public DateTime RewardSendTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId { get; set; }

    public int Difficulty { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardSent(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold GetStronghold(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConquered(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
