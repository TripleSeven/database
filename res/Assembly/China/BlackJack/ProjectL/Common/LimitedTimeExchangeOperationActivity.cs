﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.LimitedTimeExchangeOperationActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class LimitedTimeExchangeOperationActivity : OperationalActivityBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public LimitedTimeExchangeOperationActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LimitedTimeExchangeOperationActivity(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeserializeFromPB(
      ProLimitedTimeExchangeOperationActivity pbOperationalActivity,
      ConfigDataOperationalActivityInfo config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLimitedTimeExchangeOperationActivity SerializeToPB()
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, int> ExchangedItemGroups { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeOperationalActivityItem(int itemGroupIndex, int exchangeTimes = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddOperationalActivityItemExchangeTimes(int itemGroupIndex, int exchangeTimes = 1)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
