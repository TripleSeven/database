﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionMemoryCorridor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionMemoryCorridor : DataSection
  {
    private int m_challengedNums;
    private HashSet<int> m_finishedLevels;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionMemoryCorridor()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.m_finishedLevels.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedLevels(List<int> levels)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsLevelFinished(int levelId)
    {
      return this.m_finishedLevels.Contains(levelId);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFinishedLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<int> GetAllFinishedLevels()
    {
      return this.m_finishedLevels;
    }

    public void InitChallengedNums(int nums)
    {
      this.m_challengedNums = nums;
    }

    public void SetChallengedNums(int nums)
    {
      this.m_challengedNums = nums;
      this.SetDirty(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChallengedNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ChallengedNums
    {
      get
      {
        return this.m_challengedNums;
      }
    }
  }
}
