﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroFragmentBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class HeroFragmentBagItem : BagItemBase
  {
    public int ExchangeMemoryEssence;

    public HeroFragmentBagItem(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
      : base(goodsTypeId, contentId, nums, instanceId)
    {
    }

    public int GetAllExchangeMemoryEssence()
    {
      return this.Nums * this.ExchangeMemoryEssence;
    }
  }
}
