﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHeroAnthem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionHeroAnthem : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHeroAnthem()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ClearInitedData()
    {
      this.HasAttackHeroAnthemLevelInfo.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitNeedInfo(
      List<ProHeroAnthemSuccessLevel> HasSuccessLevel,
      DateTime update,
      int lastHeroAnthemAchievment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroAnthemAchievement(
      int heroAnthemLevelId,
      int achievementRelationId,
      DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroAnthemAchievement(int heroAnthemLevelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievementRelationIdsRank(
      DateTime updateTime,
      int lastHeroAnthemAchievment,
      DateTime lastHeroAnthemRankUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public int LastHeroAnthemAchievementRank { get; set; }

    public DateTime AchievementUpdateTime { get; set; }

    public DateTime LastHeroAnthemRankUpdateTime { get; set; }

    public List<HeroAnthemHasAttackLevelInfo> HasAttackHeroAnthemLevelInfo { get; set; }
  }
}
