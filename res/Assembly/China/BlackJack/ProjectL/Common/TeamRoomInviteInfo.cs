﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoomInviteInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class TeamRoomInviteInfo
  {
    public ulong SessionId { get; set; }

    public int ChannelId { get; set; }

    public string Name { get; set; }

    public int Level { get; set; }

    public int RoomId { get; set; }

    public int GameFunctionTypeId { get; set; }

    public int LocationId { get; set; }

    public DateTime TimeOut { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoomInviteInfo PbTeamRoomInviteInfoToTeamRoomInviteInfo(
      ProTeamRoomInviteInfo pbInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoomInviteInfo TeamRoomInviteInfoToPbTeamRoomInviteInfo(
      TeamRoomInviteInfo info)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
