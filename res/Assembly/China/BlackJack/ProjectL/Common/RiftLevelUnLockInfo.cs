﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RiftLevelUnLockInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class RiftLevelUnLockInfo
  {
    public HashSet<int> FinishedRiftLevelIds { get; set; }

    public HashSet<int> GainRiftAchievementRelationIds { get; set; }

    public HashSet<int> GainHeroIds { get; set; }
  }
}
