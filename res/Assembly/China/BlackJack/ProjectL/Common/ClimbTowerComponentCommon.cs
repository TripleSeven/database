﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ClimbTowerComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix]
  public class ClimbTowerComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected IConfigDataLoader m_configDataLoader;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected DataSectionClimbTower m_climbTowerDS;
    [DoNotToLua]
    private ClimbTowerComponentCommon.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_GetName_hotfix;
    private LuaFunction m_Init_hotfix;
    private LuaFunction m_PostInit_hotfix;
    private LuaFunction m_DeInit_hotfix;
    private LuaFunction m_TickUInt32_hotfix;
    private LuaFunction m_SerializeT_hotfix;
    private LuaFunction m_DeSerializeT_hotfix;
    private LuaFunction m_PostDeSerialize_hotfix;
    private LuaFunction m_IsNeedFlush_hotfix;
    private LuaFunction m_GetAutoRaidClimbTowerMax_hotfix;
    private LuaFunction m_IsReachTopTower_hotfix;
    private LuaFunction m_GetClimbTower_hotfix;
    private LuaFunction m_SetCompleteSuccessFloorInt32Boolean_hotfix;
    private LuaFunction m_GetGlobalClimbTowerInfo_hotfix;
    private LuaFunction m_AttackClimbTowerLevelInt32_hotfix;
    private LuaFunction m_CanAttackClimbTowerLevelInt32_hotfix;
    private LuaFunction m_GetFloorInt32_hotfix;
    private LuaFunction m_NewClimbTowerInt32_hotfix;
    private LuaFunction m_SetCommonSuccessClimbTowerLevelConfigDataTowerFloorInfoConfigDataTowerLevelInfoList`1Int32_hotfix;
    private LuaFunction m_OnCompleteTowerFloorInt32_hotfix;
    private LuaFunction m_CanRaidLevelConfigDataTowerFloorInfo_hotfix;
    private LuaFunction m_SetRaidSuccessClimbTowerLevelConfigDataTowerFloorInfo_hotfix;
    private LuaFunction m_CanAttackLevelByEnergyAndSoOnConfigDataTowerFloorInfoBoolean_hotfix;
    private LuaFunction m_get_ClimbTowerFloorMax_hotfix;
    private LuaFunction m_get_AutoClimbFactor_hotfix;
    private LuaFunction m_set_AutoClimbFactorInt32_hotfix;
    private LuaFunction m_get_AutoClimbMax_hotfix;
    private LuaFunction m_set_AutoClimbMaxInt32_hotfix;
    private LuaFunction m_get_Owner_hotfix;
    private LuaFunction m_set_OwnerIComponentOwner_hotfix;
    private LuaFunction m_add_CompleteTowerFloorEventAction`1_hotfix;
    private LuaFunction m_remove_CompleteTowerFloorEventAction`1_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClimbTowerComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAutoRaidClimbTowerMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachTopTower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClimbTower GetClimbTower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCompleteSuccessFloor(int floorId, bool raid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerInfo GetGlobalClimbTowerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackClimbTowerLevel(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackClimbTowerLevel(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerFloor GetFloor(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalClimbTowerInfo NewClimbTower(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetCommonSuccessClimbTowerLevel(
      ConfigDataTowerFloorInfo floorInfo,
      ConfigDataTowerLevelInfo levelInfo,
      List<int> battleTreasures,
      int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCompleteTowerFloor(int floorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidLevel(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaidSuccessClimbTowerLevel(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanAttackLevelByEnergyAndSoOn(
      ConfigDataTowerFloorInfo floorInfo,
      bool checkBagFull = true)
    {
      // ISSUE: unable to decompile the method.
    }

    protected int ClimbTowerFloorMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected int AutoClimbFactor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected int AutoClimbMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteTowerFloorEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public ClimbTowerComponentCommon.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_CompleteTowerFloorEvent(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_CompleteTowerFloorEvent(int obj)
    {
      this.CompleteTowerFloorEvent = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private ClimbTowerComponentCommon m_owner;

      public LuaExportHelper(ClimbTowerComponentCommon owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_CompleteTowerFloorEvent(int obj)
      {
        this.m_owner.__callDele_CompleteTowerFloorEvent(obj);
      }

      public void __clearDele_CompleteTowerFloorEvent(int obj)
      {
        this.m_owner.__clearDele_CompleteTowerFloorEvent(obj);
      }

      public PlayerBasicInfoComponentCommon m_basicInfo
      {
        get
        {
          return this.m_owner.m_basicInfo;
        }
        set
        {
          this.m_owner.m_basicInfo = value;
        }
      }

      public IConfigDataLoader m_configDataLoader
      {
        get
        {
          return this.m_owner.m_configDataLoader;
        }
        set
        {
          this.m_owner.m_configDataLoader = value;
        }
      }

      public BattleComponentCommon m_battle
      {
        get
        {
          return this.m_owner.m_battle;
        }
        set
        {
          this.m_owner.m_battle = value;
        }
      }

      public BagComponentCommon m_bag
      {
        get
        {
          return this.m_owner.m_bag;
        }
        set
        {
          this.m_owner.m_bag = value;
        }
      }

      public HeroComponentCommon m_hero
      {
        get
        {
          return this.m_owner.m_hero;
        }
        set
        {
          this.m_owner.m_hero = value;
        }
      }

      public DataSectionClimbTower m_climbTowerDS
      {
        get
        {
          return this.m_owner.m_climbTowerDS;
        }
        set
        {
          this.m_owner.m_climbTowerDS = value;
        }
      }

      public int ClimbTowerFloorMax
      {
        get
        {
          return this.m_owner.ClimbTowerFloorMax;
        }
      }

      public int AutoClimbFactor
      {
        get
        {
          return this.m_owner.AutoClimbFactor;
        }
        set
        {
          this.m_owner.AutoClimbFactor = value;
        }
      }

      public int AutoClimbMax
      {
        get
        {
          return this.m_owner.AutoClimbMax;
        }
        set
        {
          this.m_owner.AutoClimbMax = value;
        }
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetCommonSuccessClimbTowerLevel(
        ConfigDataTowerFloorInfo floorInfo,
        ConfigDataTowerLevelInfo levelInfo,
        List<int> battleTreasures,
        int energyCost)
      {
        // ISSUE: unable to decompile the method.
      }

      public void OnCompleteTowerFloor(int floorId)
      {
        this.m_owner.OnCompleteTowerFloor(floorId);
      }

      public void SetRaidSuccessClimbTowerLevel(ConfigDataTowerFloorInfo floorInfo)
      {
        this.m_owner.SetRaidSuccessClimbTowerLevel(floorInfo);
      }

      public int CanAttackLevelByEnergyAndSoOn(
        ConfigDataTowerFloorInfo floorInfo,
        bool checkBagFull)
      {
        return this.m_owner.CanAttackLevelByEnergyAndSoOn(floorInfo, checkBagFull);
      }
    }
  }
}
