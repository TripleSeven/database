﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AwardOperationalActivityBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ConfigData;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class AwardOperationalActivityBase : OperationalActivityBase
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public AwardOperationalActivityBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AwardOperationalActivityBase(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanGainRewardByIndex(int rewardIndex, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public void GainReward(int rewardIndex)
    {
      this.GainedRewardIndexs.Add(rewardIndex);
    }

    public List<int> GainedRewardIndexs { get; set; }
  }
}
