﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UpdateCache`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UpdateCache<T> : CacheObject where T : class
  {
    public UpdateCache(T data)
    {
      this.Data = data;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsValid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetInvalid()
    {
      // ISSUE: unable to decompile the method.
    }

    public T Data { get; set; }
  }
}
