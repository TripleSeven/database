﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class DataSectionMission : DataSection
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public void InitEverydayMissions(List<Mission> missions)
    {
      this.ProcessingEverydayMissions.AddRange((IEnumerable<Mission>) missions);
    }

    public void InitOneOffMissions(List<Mission> missions)
    {
      this.ProcessingOneOffMissions.AddRange((IEnumerable<Mission>) missions);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedEverydayMissions(List<int> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedOneOffMissions(List<int> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetEverydayMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOneOffMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEverydayMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishOneOffMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishEverydayMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMissionProcess(Mission mission, long count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMissionProcess(Mission mission, int process)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveEveryDayMission(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetExistMissionIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProcessingMission(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Mission> ProcessingEverydayMissions { get; set; }

    public HashSet<int> FinishedEverydayMissions { get; set; }

    public List<Mission> ProcessingOneOffMissions { get; set; }

    public HashSet<int> FinishedOneOffMissions { get; set; }
  }
}
