﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBLinkInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "GDBLinkInfo")]
  [Serializable]
  public class GDBLinkInfo : IExtensible
  {
    private int _FromStarfieldID;
    private int _FromStargroupID;
    private int _FromSolarSystemID;
    private int _FromStargateID;
    private int _ToStarfieldID;
    private int _ToStargroupID;
    private int _ToSolarSystemID;
    private int _ToStargateID;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBLinkInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "FromStarfieldID")]
    public int FromStarfieldID
    {
      get
      {
        return this._FromStarfieldID;
      }
      set
      {
        this._FromStarfieldID = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "FromStargroupID")]
    public int FromStargroupID
    {
      get
      {
        return this._FromStargroupID;
      }
      set
      {
        this._FromStargroupID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FromSolarSystemID")]
    public int FromSolarSystemID
    {
      get
      {
        return this._FromSolarSystemID;
      }
      set
      {
        this._FromSolarSystemID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FromStargateID")]
    public int FromStargateID
    {
      get
      {
        return this._FromStargateID;
      }
      set
      {
        this._FromStargateID = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ToStarfieldID")]
    public int ToStarfieldID
    {
      get
      {
        return this._ToStarfieldID;
      }
      set
      {
        this._ToStarfieldID = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ToStargroupID")]
    public int ToStargroupID
    {
      get
      {
        return this._ToStargroupID;
      }
      set
      {
        this._ToStargroupID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ToSolarSystemID")]
    public int ToSolarSystemID
    {
      get
      {
        return this._ToSolarSystemID;
      }
      set
      {
        this._ToSolarSystemID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ToStargateID")]
    public int ToStargateID
    {
      get
      {
        return this._ToStargateID;
      }
      set
      {
        this._ToStargateID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
