﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaTopRankPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.ProjectL.Protocol;
using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [CustomLuaClass]
  public class ArenaTopRankPlayer
  {
    public string Name { get; set; }

    public ushort ArenaPoints { get; set; }

    public byte LevelId { get; set; }

    public int HeadIcon { get; set; }

    public int Level { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaTopRankPlayer ArenaTopRankPlayerToPBArenaTopRankPlayer(
      ArenaTopRankPlayer topRankPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaTopRankPlayer PBArenaTopRankPlayerToArenaTopRankPlayer(
      ProArenaTopRankPlayer pbTopRankPlayer)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
