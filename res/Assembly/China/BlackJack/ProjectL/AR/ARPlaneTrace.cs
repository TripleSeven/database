﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.AR.ARPlaneTrace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.AR
{
  [CustomLuaClass]
  public abstract class ARPlaneTrace
  {
    private const float m_findingSquareDist = 0.5f;
    protected Vector3 m_centerPos;
    protected Quaternion m_centerRotation;
    protected Camera m_camera;
    private bool m_isInitialized;
    private ARPlaneTrace.EFocusState m_focusState;

    public Vector3 CenterPos
    {
      get
      {
        return this.m_centerPos;
      }
    }

    public Quaternion CenterRotation
    {
      get
      {
        return this.m_centerRotation;
      }
    }

    public ARPlaneTrace.EFocusState FocusState
    {
      get
      {
        return this.m_focusState;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(Camera camera)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Quaternion CalculateLookVector(Vector3 srcPos, Vector3 destPos)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnInit()
    {
      this.m_isInitialized = true;
      this.OnStart();
    }

    public virtual void OnStart()
    {
    }

    public virtual void OnStop()
    {
    }

    public virtual void OnPause()
    {
    }

    public virtual void OnResume()
    {
    }

    protected abstract bool Raycast(Vector3 center, out Vector3 pos);

    public virtual void AlwaysUpdate()
    {
    }

    public enum EFocusState
    {
      Initializing,
      Finding,
      Found,
    }
  }
}
