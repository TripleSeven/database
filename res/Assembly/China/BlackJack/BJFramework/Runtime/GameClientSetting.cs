﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.GameClientSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  public class GameClientSetting : ScriptableObject
  {
    public static string defaultClientPrefPath = "Assets/GameProject/Resources/GameClientSetting";
    public string UITaskRegisterTypeDNName = "Assembly-CSharp@BlackJack.ProjectSample.SampleGameUITaskRegister";
    public bool DisableUnityEngineLog;
    public bool Log2Persistent;
    public ResourcesSettings ResourcesSetting;
    public ResolutionSettings ResolutionSetting;
    public SceneSettings SceneSetting;
    public DynamicAssemblySettings DynamicAssemblySetting;
    public LuaSettings LuaSetting;
    public ConfigDataSettings ConfigDataSetting;
    public AudioSettings AudioSetting;
    public StringTableSettings StringTableSetting;
    public bool DisableUserGuide;
    public LoginSettings LoginSetting;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameClientSetting()
    {
    }
  }
}
