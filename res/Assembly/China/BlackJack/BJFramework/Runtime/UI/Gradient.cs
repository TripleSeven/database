﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.Gradient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UI/Effects/Gradient")]
  public class Gradient : BaseMeshEffect
  {
    public GradientDirection m_gradientDir;
    public Color m_color1;
    public Color m_color2;

    [MethodImpl((MethodImplOptions) 32768)]
    public Gradient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ModifyMesh(VertexHelper vh)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
