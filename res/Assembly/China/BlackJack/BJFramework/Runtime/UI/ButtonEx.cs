﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.UI.ButtonEx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace BlackJack.BJFramework.Runtime.UI
{
  [AddComponentMenu("UIExtend/ButtonEx", 16)]
  public class ButtonEx : Button
  {
    [FormerlySerializedAs("onDoubleClick")]
    [SerializeField]
    private ButtonEx.ButtonDoubleClickedEvent m_OnDoubleClick = new ButtonEx.ButtonDoubleClickedEvent();
    [SerializeField]
    private ButtonEx.ButtonLongPressStartEvent m_OnLongPressStart = new ButtonEx.ButtonLongPressStartEvent();
    [SerializeField]
    private ButtonEx.ButtonLongPressingEvent m_OnLongPressing = new ButtonEx.ButtonLongPressingEvent();
    [SerializeField]
    private ButtonEx.ButtonLongPressEndEvent m_OnLongPressEnd = new ButtonEx.ButtonLongPressEndEvent();
    public float m_doubleClickDelayTime = 0.3f;
    public float m_longPressThreshhold = 0.5f;
    public float m_longPressingTriggerTimeGap = -1f;
    private bool m_isPointerInside;
    private bool m_isPointerDown;
    private int m_continueClickCount;
    private float m_continueClickDelayLeftTime;
    private float m_timeFromPressStarted;
    private bool m_isLongPressTriggered;
    private float m_timeFromLastPressingTrigger;
    [SerializeField]
    [FormerlySerializedAs("ButtonComponent")]
    [Header("[按钮组件]")]
    protected List<GameObject> m_buttonComponent;
    [FormerlySerializedAs("NormalStateColorList")]
    [SerializeField]
    protected List<Color> m_normalStateColorList;
    [FormerlySerializedAs("PressedStateColorList")]
    [SerializeField]
    protected List<Color> m_pressedStateColorList;
    [SerializeField]
    [FormerlySerializedAs("DisableStateColorList")]
    protected List<Color> m_disableStateColorList;
    protected Dictionary<GameObject, Color> m_baseColorList;
    [Header("[Unity按下音效]")]
    [FormerlySerializedAs("PressedAudioClip")]
    [SerializeField]
    protected AudioClip m_pressedAudioClip;
    [SerializeField]
    [Header("[CRI按下音效]")]
    [FormerlySerializedAs("PressedAudioCRI")]
    protected string m_pressedAudioCRI;
    [Header("[是否禁止音效]")]
    [SerializeField]
    protected bool m_prohibitAudio;
    [SerializeField]
    [FormerlySerializedAs("PointerDownTweenList")]
    [Header("按下Tween列表")]
    protected List<TweenMain> m_pointerDownTweenList;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ButtonEx()
    {
      this.onClick.AddListener(new UnityAction(this.OnButtonClicked));
    }

    public ButtonEx.ButtonDoubleClickedEvent onDoubleClick
    {
      get
      {
        return this.m_OnDoubleClick;
      }
      set
      {
        this.m_OnDoubleClick = value;
      }
    }

    public ButtonEx.ButtonLongPressStartEvent onLongPressStart
    {
      get
      {
        return this.m_OnLongPressStart;
      }
      set
      {
        this.m_OnLongPressStart = value;
      }
    }

    public ButtonEx.ButtonLongPressingEvent onLongPressing
    {
      get
      {
        return this.m_OnLongPressing;
      }
      set
      {
        this.m_OnLongPressing = value;
      }
    }

    public ButtonEx.ButtonLongPressEndEvent onLongPressEnd
    {
      get
      {
        return this.m_OnLongPressEnd;
      }
      set
      {
        this.m_OnLongPressEnd = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void Update()
    {
      if ((double) this.m_continueClickDelayLeftTime > 0.0)
      {
        this.m_continueClickDelayLeftTime -= Time.deltaTime;
        if ((double) this.m_continueClickDelayLeftTime < 0.0)
        {
          this.m_continueClickDelayLeftTime = 0.0f;
          --this.m_continueClickCount;
        }
      }
      if (!this.m_isPointerDown)
        return;
      if (!this.m_isLongPressTriggered)
      {
        if ((double) Time.time - (double) this.m_timeFromPressStarted <= (double) this.m_longPressThreshhold)
          return;
        this.m_isLongPressTriggered = true;
        this.m_timeFromLastPressingTrigger = 0.0f;
        this.m_OnLongPressStart.Invoke();
      }
      else if ((double) this.m_longPressingTriggerTimeGap <= 0.0)
      {
        this.m_OnLongPressing.Invoke();
      }
      else
      {
        this.m_timeFromLastPressingTrigger += Time.deltaTime;
        if ((double) this.m_timeFromLastPressingTrigger < (double) this.m_longPressingTriggerTimeGap)
          return;
        this.m_OnLongPressing.Invoke();
        this.m_timeFromLastPressingTrigger -= this.m_longPressingTriggerTimeGap;
      }
    }

    public void SetNormalState()
    {
      this.DoStateTransition(Selectable.SelectionState.Normal, false);
    }

    public void SetPressedState()
    {
      this.DoStateTransition(Selectable.SelectionState.Pressed, false);
    }

    public void SetDisableState()
    {
      this.DoStateTransition(Selectable.SelectionState.Disabled, false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
      this.m_isPointerInside = true;
      base.OnPointerEnter(eventData);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPointerExit(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void SetBaseColorList(Dictionary<GameObject, Color> baseColorList)
    {
      this.m_baseColorList = baseColorList;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void DoStateTransition(Selectable.SelectionState state, bool instant)
    {
      base.DoStateTransition(state, instant);
      if (!this.gameObject.activeInHierarchy)
        return;
      switch (state)
      {
        case Selectable.SelectionState.Normal:
        case Selectable.SelectionState.Highlighted:
          if (this.m_baseColorList == null)
          {
            this.SetButtonComponentColor(this.m_normalStateColorList);
            break;
          }
          this.ResetButtonCompontentToBaseColor();
          break;
        case Selectable.SelectionState.Pressed:
          this.SetButtonComponentColor(this.m_pressedStateColorList);
          break;
        case Selectable.SelectionState.Disabled:
          this.SetButtonComponentColor(this.m_disableStateColorList);
          break;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InstantClearState()
    {
      base.InstantClearState();
      this.SetButtonComponentColor(this.m_disableStateColorList);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetButtonComponentColor(List<Color> stateColorList)
    {
      if (stateColorList == null)
        return;
      for (int index = 0; index < this.m_buttonComponent.Count && index < stateColorList.Count; ++index)
      {
        GameObject go = this.m_buttonComponent[index];
        if (!((UnityEngine.Object) go == (UnityEngine.Object) null))
          this.SetGameObjectColor(go, stateColorList[index]);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetButtonCompontentToBaseColor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Color GetBaseStateColor(GameObject go, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void PlayPointerDownTween(bool isforward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGameObjectColor(GameObject go, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetDoubleClickState()
    {
      // ISSUE: unable to decompile the method.
    }

    public AudioClip PressedAudioClip
    {
      get
      {
        return this.m_pressedAudioClip;
      }
    }

    public string PressedAudioCRI
    {
      get
      {
        return this.m_pressedAudioCRI;
      }
    }

    public bool ProhibitAudio
    {
      get
      {
        return this.m_prohibitAudio;
      }
    }

    [Serializable]
    public class ButtonDoubleClickedEvent : UnityEvent
    {
    }

    [Serializable]
    public class ButtonLongPressStartEvent : UnityEvent
    {
    }

    [Serializable]
    public class ButtonLongPressingEvent : UnityEvent
    {
    }

    [Serializable]
    public class ButtonLongPressEndEvent : UnityEvent
    {
    }
  }
}
