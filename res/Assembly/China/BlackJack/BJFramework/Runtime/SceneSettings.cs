﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.SceneSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class SceneSettings
  {
    public int DesignScreenWidth = 1920;
    public int DesignScreenHeight = 1080;
    public Vector3 SceneLayerOffset = new Vector3(1000f, 0.0f, 0.0f);
    public bool UseOrthographicForUILayer;
    public int TrigerWidth2ShrinkScale;
    public int TrigerHeight2ShrinkScale;

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneSettings()
    {
    }
  }
}
