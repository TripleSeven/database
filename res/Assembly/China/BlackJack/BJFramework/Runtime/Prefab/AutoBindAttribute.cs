﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Prefab.AutoBindAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.Prefab
{
  [AttributeUsage(AttributeTargets.Field)]
  public class AutoBindAttribute : Attribute
  {
    public readonly string m_path;
    public readonly AutoBindAttribute.InitState m_initState;
    public readonly bool m_optional;

    [MethodImpl((MethodImplOptions) 32768)]
    public AutoBindAttribute(string path, AutoBindAttribute.InitState initState = AutoBindAttribute.InitState.NotInit, bool optional = false)
    {
      this.m_path = path;
      this.m_initState = initState;
      this.m_optional = optional;
    }

    public enum InitState
    {
      NotInit,
      Active,
      Inactive,
    }
  }
}
