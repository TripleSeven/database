﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.IAudioPlayback
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.BJFramework.Runtime
{
  public interface IAudioPlayback
  {
    float Seconds { get; }

    bool IsStoped();

    string CueName { get; }

    void Stop();
  }
}
