﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.BundleData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.BJFramework.Runtime.Resource
{
  public class BundleData : ScriptableObject
  {
    public int m_version = 1;
    public int m_basicVersion = 1;
    public List<BundleData.SingleBundleData> m_bundleList = new List<BundleData.SingleBundleData>();

    [MethodImpl((MethodImplOptions) 32768)]
    public BundleData()
    {
    }

    [Serializable]
    public class SingleBundleData
    {
      public int m_version = 1;
      public List<string> m_assetList = new List<string>();
      public string m_bundleName;
      public string m_bundleHash;
      public uint m_bundleCRC;
      public long m_size;
      public bool m_isInStreamingAssets;
      public bool m_isNeedPreUpdateByDefault;
      public bool m_isResaveFileBundle;
      public bool m_isLazyUpdate;
      public string m_localizationKey;

      [MethodImpl((MethodImplOptions) 32768)]
      public SingleBundleData()
      {
      }
    }
  }
}
