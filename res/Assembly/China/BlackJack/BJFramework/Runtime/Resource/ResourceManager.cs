﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.Resource.ResourceManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.Utils;
using Boo.Lang;
using SLua;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BlackJack.BJFramework.Runtime.Resource
{
  [CustomLuaClass]
  public class ResourceManager : ITickable
  {
    public static string m_preUpdateVersion4BundleDataKey = "PreUpdateVersion4BundleData";
    protected TinyCorutineHelper m_corutineHelper = new TinyCorutineHelper();
    protected Dictionary<string, ResourceManager.AssetCacheItem> m_assetsCacheDict = new Dictionary<string, ResourceManager.AssetCacheItem>();
    protected List<ResourceManager.AssetCacheItem> m_assetsCacheToRemove = new List<ResourceManager.AssetCacheItem>();
    protected HashSet<string> m_loadedAssetPathSet = new HashSet<string>();
    public HashSet<string> m_loadFilePaths = new HashSet<string>();
    private Stopwatch _stopWatch = new Stopwatch();
    protected DateTime m_nextTickBundleTime = DateTime.Now.AddSeconds(120.0);
    protected Dictionary<string, ResourceManager.BundleCacheItem> m_bundleCacheDict = new Dictionary<string, ResourceManager.BundleCacheItem>();
    protected List<ResourceManager.BundleCacheItem> m_bundleCachetoUnload = new List<ResourceManager.BundleCacheItem>();
    protected Dictionary<string, ResourceManager.BundleLoadingCtx> m_bundleLoadingCtxDict = new Dictionary<string, ResourceManager.BundleLoadingCtx>();
    protected Dictionary<string, ResourceManager.ReserveItem> m_assetReserveDict = new Dictionary<string, ResourceManager.ReserveItem>();
    protected Dictionary<string, ResourceManager.ReserveItem> m_assetReserveDict4Keep = new Dictionary<string, ResourceManager.ReserveItem>();
    protected List<string> m_assetReserve4Remove = new List<string>();
    private List<BundleData.SingleBundleData> m_streamingAssetsMissingBundles = new List<BundleData.SingleBundleData>();
    protected static ResourceManager m_instance;
    protected string m_currLocalization;
    protected bool m_needGC;
    protected int m_unloadUnusedAssetTimeInterval;
    protected DateTime m_nextUnloadUnusedAssetTime;
    protected DateTime m_nextCheckMemUseageTime;
    protected bool m_assetPathIgnoreCase;
    protected bool m_enableDetailResourceManagerLog;
    protected AsyncOperation m_currUnloadUnusedAssetsOperation;
    protected bool m_disableAssetBundle;
    protected bool m_loadAssetFromBundleInEditor;
    protected static string m_bundleDownloadUrlRoot;
    protected static string m_downloadUrlRootWithPlatform;
    protected string m_downloadBundleUrlPrefix;
    protected bool m_needUnloadAllUnusedBundles;
    protected bool m_needUnloadUnusedAssets;
    protected int m_loadingOpCount;
    protected const string m_streamingAssetsFileListVersionKey = "StreamingAssetsFileListVersion";
    protected const string m_bundleDataVersionKey = "BundleDataVersion";
    protected const string m_touchedBundleSetFileName = "TouchedBundleSet";
    public float m_loadingProgress;
    protected BundleData m_bundleData;
    protected BundleDataHelper m_bundleDataHelper;
    protected AssetBundleManifest m_assetBundleManifest;
    protected HashSet<string> m_touchedBundleSet;
    protected bool m_touchedBundleSetDirty;
    protected bool m_skipAssetBundlePreUpdateing;
    protected int m_updateingWorkerCount;
    public Func<List<string>> GetCustomPreUpdateBundleListFunc;
    public Func<List<string>> GetCustomSkipPreUpdateBundleListFunc;
    protected List<BundleData.SingleBundleData> m_updateingBundleList;
    protected List<string> m_customSkipPreUpdateBundleList;
    protected List<string> m_customPreUpdateBundleList;
    protected int m_updateWorkerAliveCount;
    protected bool m_isAllUpdateWorkerSuccess;
    protected long m_assetBundleUpdateingTotalByte;
    protected long m_assetBundleUpdateingDownloadedByte;
    private bool m_disableAssetBundleDownload;
    private int m_currBundleDataVersion;
    private BundleData m_lastPreUpdateBundleData;
    private const string m_signalFileName = "dontmove.tmp";
    protected DateTime m_reserveTickDelayOutTime;
    protected const float ReserveTickDelayTime = 5f;
    private bool m_skipStreamingAssetsFileProcessing;
    private int m_currStreamingAssetsFileListVersion;
    private StreamingAssetsFileList m_streamingAssetsFileList;
    private BundleData m_streamingAssetsBundleData;
    private BundleDataHelper m_streamingAssetsBundleDataHelper;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ResourceManager()
    {
      this.State = ResourceManager.RMState.None;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public static ResourceManager CreateResourceManager()
    {
      if (ResourceManager.m_instance == null)
        ResourceManager.m_instance = new ResourceManager();
      return ResourceManager.m_instance;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initlize(ResourcesSettings settings, string currLocalization)
    {
      Debug.Log("ResourceManager.Initlize start");
      this.State = ResourceManager.RMState.Inited;
      this.m_disableAssetBundle = settings.DisableAssetBundle;
      this.m_skipStreamingAssetsFileProcessing = this.m_disableAssetBundle && settings.SkipStreamingAssetsFileProcessing;
      this.m_disableAssetBundleDownload = this.m_disableAssetBundle || settings.DisableAssetBundleDownload;
      this.m_skipAssetBundlePreUpdateing = this.m_disableAssetBundle || this.m_disableAssetBundleDownload || settings.SkipAssetBundlePreUpdateing;
      ResourceManager.m_bundleDownloadUrlRoot = settings.AssetBundleDownloadUrlRoot;
      this.m_updateingWorkerCount = settings.PreUpdateWorkerCount >= 1 ? settings.PreUpdateWorkerCount : 1;
      this.m_loadAssetFromBundleInEditor = !settings.DisableAssetBundle && settings.LoadAssetFromBundleInEditor;
      this.m_unloadUnusedAssetTimeInterval = settings.UnloadUnusedAssetTimeInterval;
      this.m_assetPathIgnoreCase = settings.AssetPathIgnoreCase;
      this.m_enableDetailResourceManagerLog = settings.EnableDetailResourceManagerLog;
      this.m_currLocalization = currLocalization;
      return this.LoadTouchedBundleSet();
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void Uninitlize()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator LoadAsset<T>(
      string path,
      Action<string, T> onCompleted,
      bool noErrlog = false,
      bool loadAync = false)
      where T : UnityEngine.Object
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ResourceManager.\u003CLoadAsset\u003Ec__Iterator0<T>()
      {
        path = path,
        onCompleted = onCompleted,
        noErrlog = noErrlog,
        loadAync = loadAync,
        \u0024this = this
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNeedLoadAllForAssetPath(string assetPath)
    {
      if (!assetPath.EndsWith(".png", StringComparison.OrdinalIgnoreCase))
        return assetPath.EndsWith(".fbx", StringComparison.OrdinalIgnoreCase);
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartLoadAssetsCorutine(
      HashSet<string> pathList,
      IDictionary<string, UnityEngine.Object> assetDict,
      Action onComplete,
      bool loadAsync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartLoadAssetCorutine(string path, Action<string, UnityEngine.Object> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadAssetsCorutine(
      HashSet<string> pathList,
      int corutineId,
      IDictionary<string, UnityEngine.Object> assetDict,
      Action onComplete,
      bool loadAsync)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void LoadAssetByResourcesLoad<T>(
      string path,
      Action<string, UnityEngine.Object[]> onCompleted,
      bool noErrlog = false,
      bool isNeedLoadAllRes = false)
      where T : UnityEngine.Object
    {
      int length = path.LastIndexOf(".");
      string str = length != -1 ? path.Substring(0, length) : path;
      int num = str.IndexOf("Resources/");
      string path1 = num != -1 ? str.Substring(num + "Resources/".Length) : str;
      UnityEngine.Object[] objectArray;
      if (isNeedLoadAllRes)
        objectArray = Resources.LoadAll(path1);
      else
        objectArray = new UnityEngine.Object[1]
        {
          (UnityEngine.Object) Resources.Load<T>(path1)
        };
      if (objectArray == null || objectArray.Length == 0)
      {
        if (!noErrlog)
          Debug.LogError(string.Format("LoadAssetByResourcesLoad fail {0}", (object) path1));
        else
          Debug.Log(string.Format("LoadAssetByResourcesLoad fail {0}", (object) path1));
      }
      else
        Debug.Log(string.Format("LoadAssetByResourcesLoad ok {0}", (object) path1));
      onCompleted(path1, objectArray);
    }

    protected virtual void LoadAssetByAssetDatabase<T>(
      string path,
      Action<string, UnityEngine.Object[]> onCompleted,
      bool noErrlog = false,
      bool isNeedLoadAllRes = false)
      where T : UnityEngine.Object
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UnityEngine.Object GetAssetFromCache(string key)
    {
      ResourceManager.AssetCacheItem assetCacheItem;
      if (this.m_assetsCacheDict.TryGetValue(key, out assetCacheItem))
      {
        WeakReference weakRefrence = assetCacheItem.m_weakRefrence;
        if (weakRefrence.IsAlive)
        {
          if (this.m_enableDetailResourceManagerLog)
            Debug.Log("GetAssetFromCache OK " + key);
          if (assetCacheItem.m_removeReserveOnHit)
            this.RemoveReserve4CacheHit(key);
          return weakRefrence.Target as UnityEngine.Object;
        }
        if (this.m_enableDetailResourceManagerLog)
          Debug.Log("GetAssetFromCache Dead " + key);
        this.m_assetsCacheDict.Remove(key);
      }
      else if (this.m_enableDetailResourceManagerLog)
        Debug.Log("GetAssetFromCache NULL " + key);
      return (UnityEngine.Object) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void PushAssetToCache(
      string key,
      UnityEngine.Object asset,
      ResourceManager.BundleCacheItem bundleCacheItem = null)
    {
      ResourceManager.AssetCacheItem assetCacheItem;
      if (this.m_assetsCacheDict.TryGetValue(key, out assetCacheItem) && object.ReferenceEquals(assetCacheItem.m_weakRefrence.Target, (object) asset))
        return;
      this.m_assetsCacheDict[key] = new ResourceManager.AssetCacheItem()
      {
        m_weakRefrence = new WeakReference((object) asset),
        m_cacheKey = key
      };
      if (bundleCacheItem == null)
        return;
      this.OnBundleCacheHit(bundleCacheItem);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDownloadUrlRoot(string url)
    {
      if (ResourceManager.m_bundleDownloadUrlRoot == url)
        return;
      ResourceManager.m_bundleDownloadUrlRoot = url;
      ResourceManager.m_downloadUrlRootWithPlatform = (string) null;
    }

    public static string GetDownloadUrlRoot()
    {
      return ResourceManager.m_bundleDownloadUrlRoot;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocalization(string localization, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      this.m_corutineHelper.Tick();
      this.TickReserve();
      this.TickAsset();
      this.TickBundle();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickAsset()
    {
      if (this.m_needUnloadUnusedAssets && this.m_loadingOpCount == 0)
      {
        this.m_needUnloadUnusedAssets = false;
        this.UnloadUnusedAssets();
      }
      if (this.m_currUnloadUnusedAssetsOperation != null && this.m_currUnloadUnusedAssetsOperation.isDone)
      {
        this.m_currUnloadUnusedAssetsOperation = (AsyncOperation) null;
        if (LuaSvr.mainState != null)
          LuaDLL.lua_gc(LuaSvr.mainState.L, LuaGCOptions.LUA_GCCOLLECT, 0);
        GC.Collect();
        this.CleanUnusedAssetsCache();
        if (this.m_needUnloadAllUnusedBundles)
        {
          if (this.UnloadAllUnusedBundles(false))
          {
            this.m_needUnloadAllUnusedBundles = false;
            this.m_needGC = true;
          }
          if (this.m_eventOnUnloadUnusedResourceAllCompleted != null)
          {
            this.m_eventOnUnloadUnusedResourceAllCompleted();
            this.m_eventOnUnloadUnusedResourceAllCompleted = (Action) null;
          }
        }
      }
      if (!(this.m_nextUnloadUnusedAssetTime <= BlackJack.BJFramework.Runtime.Timer.m_currTime) || this.m_loadingOpCount != 0 || this.m_currUnloadUnusedAssetsOperation != null)
        return;
      this.m_nextUnloadUnusedAssetTime = BlackJack.BJFramework.Runtime.Timer.m_currTime.AddSeconds((double) this.m_unloadUnusedAssetTimeInterval);
      this.UnloadUnusedAssets();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickGC()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickMemUseage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OutputMemUseage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UnloadUnusedAssets()
    {
      if (this.m_loadingOpCount != 0)
        return false;
      if (this.m_currUnloadUnusedAssetsOperation == null)
        this.m_currUnloadUnusedAssetsOperation = Resources.UnloadUnusedAssets();
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UnloadUnusedResourceAll(Action onComplete = null, HashSet<string> keepReserve = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnloadAllBundle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CleanUnusedAssetsCache()
    {
      foreach (ResourceManager.AssetCacheItem assetCacheItem in this.m_assetsCacheDict.Values)
      {
        if (!assetCacheItem.m_weakRefrence.IsAlive)
          this.m_assetsCacheToRemove.Add(assetCacheItem);
      }
      foreach (ResourceManager.AssetCacheItem assetCacheItem in this.m_assetsCacheToRemove)
        this.m_assetsCacheDict.Remove(assetCacheItem.m_cacheKey);
      this.m_assetsCacheToRemove.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetDownloadBundleUrl(string path)
    {
      if (ResourceManager.m_downloadUrlRootWithPlatform == null)
        ResourceManager.m_downloadUrlRootWithPlatform = !string.IsNullOrEmpty(this.m_downloadBundleUrlPrefix) ? string.Format("{0}/{1}/{2}", (object) ResourceManager.m_bundleDownloadUrlRoot, (object) this.m_downloadBundleUrlPrefix, (object) Util.GetCurrentTargetPlatform()) : string.Format("{0}/{1}", (object) ResourceManager.m_bundleDownloadUrlRoot, (object) Util.GetCurrentTargetPlatform());
      return string.Format("{0}/{1}", (object) ResourceManager.m_downloadUrlRootWithPlatform, (object) path);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetStreamingAssetBundleUrl(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetStreamingAssetBundlePath(string bundleName)
    {
      return string.Format("{0}/{1}", (object) PathHelper.StreamingAssetsBundlePath, (object) bundleName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetBundleLoadingUrl(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBundleNeedLoadFromStreamingAssetFile(string bundleName, int version)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundleFromCacheOrDownload(
      string url,
      int bundleVersion,
      uint bundleCRC,
      Action<AssetBundle> onEnd)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ResourceManager.\u003CLoadBundleFromCacheOrDownload\u003Ec__Iterator2()
      {
        url = url,
        bundleVersion = bundleVersion,
        bundleCRC = bundleCRC,
        onEnd = onEnd
      };
    }

    public HashSet<string> GetLoadedAssetPathSet()
    {
      return this.m_loadedAssetPathSet;
    }

    public event Action EventOnAssetLoaded
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static ResourceManager Instance
    {
      get
      {
        return ResourceManager.m_instance;
      }
    }

    public ResourceManager.RMState State { get; protected set; }

    protected event Action m_eventOnUnloadUnusedResourceAllCompleted
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public void SetDownloadBundleUrlPrefix(string prefix)
    {
      this.m_downloadBundleUrlPrefix = prefix;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadAssetFromBundle(
      string path,
      Action<string, UnityEngine.Object[], ResourceManager.BundleCacheItem> onCompleted,
      bool noErrlog = false,
      bool loadAync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundle4UnityScene(
      string scenePath,
      Action<string, ResourceManager.BundleCacheItem> onComplete,
      bool noErrlog,
      bool loadAync)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundle(
      BundleData.SingleBundleData bundleData,
      bool loadAync,
      Action<AssetBundle, ResourceManager.BundleCacheItem> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundle(
      string bundleName,
      bool loadAync,
      Action<AssetBundle, ResourceManager.BundleCacheItem> onComplete,
      bool noErrlog = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator LoadBundleFromWWWOrStreamingImpl(
      BundleData.SingleBundleData bundleData,
      bool loadAync,
      bool ignoreCRC,
      Action<AssetBundle> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetBundleNameByCurrLocalization(BundleData.SingleBundleData bundleData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool MakeAssetBundleDontUnload(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected ResourceManager.BundleCacheItem GetAssetBundleFromCache(
      string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected ResourceManager.BundleCacheItem PushAssetBundleToCache(
      string bundleName,
      AssetBundle bundle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBundleCacheHit(ResourceManager.BundleCacheItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool RegBundleLoadingCtx(string bundleName, out ResourceManager.BundleLoadingCtx ctx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UnregBundleLoadingCtx(ResourceManager.BundleLoadingCtx bundleLoadingCtx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected BundleData.SingleBundleData GetBundleDataByAssetPath(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected BundleData.SingleBundleData GetResaveFileBundleDataByPath(
      string relativePath)
    {
      // ISSUE: unable to decompile the method.
    }

    protected string GetAssetNameByPath(string path)
    {
      return Path.GetFileNameWithoutExtension(path);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickBundle()
    {
      if (this.m_touchedBundleSetDirty)
        this.SaveTouchedBundleSet(false);
      if (this.m_nextTickBundleTime > BlackJack.BJFramework.Runtime.Timer.m_currTime)
        return;
      this.m_nextTickBundleTime = BlackJack.BJFramework.Runtime.Timer.m_currTime.AddSeconds(120.0);
      this.UnloadAllUnusedBundles(true);
    }

    protected bool UnloadAllUnusedBundles(bool onlyTimeOut = false)
    {
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void DealWithResaveFileInBundle(
      AssetBundle assetBundle,
      BundleDataHelper bundleHelper)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ConvertRelativePathFile2OrginalPathFile(string relativePathFile)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetResaveRuntimePath(string srcDir, string relativePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string TryGetResaveFile(string srcDir, string relativePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator DownloadResaveFile(
      string srcDir,
      HashSet<string> relativePaths,
      Action<string, bool> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartDownloadResaveFile(
      string srcDir,
      HashSet<string> relativePaths,
      Action<string, bool> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool LoadTouchedBundleSet()
    {
      string filePersistentPath = Util.GetFilePersistentPath("TouchedBundleSet");
      if (!File.Exists(filePersistentPath))
      {
        this.m_touchedBundleSet = new HashSet<string>();
        this.SaveTouchedBundleSet(true);
        return true;
      }
      try
      {
        List<string> stringList = new List<string>();
        this.m_touchedBundleSet = new HashSet<string>();
        FileStream fileStream;
        using (fileStream = File.OpenRead(filePersistentPath))
          stringList = new BinaryFormatter().Deserialize((Stream) fileStream) as List<string>;
        foreach (string str in stringList)
          this.m_touchedBundleSet.Add(str);
      }
      catch (Exception ex)
      {
        Debug.LogError(string.Format("LoadTouchedBundleSet failed! error = {0}", (object) ex.Message));
        this.m_touchedBundleSet = new HashSet<string>();
        this.SaveTouchedBundleSet(true);
        return false;
      }
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveTouchedBundleSet(bool force = false)
    {
      if (this.m_touchedBundleSet == null)
        return;
      if (!force)
      {
        if (!this.m_touchedBundleSetDirty)
          return;
      }
      try
      {
        string filePersistentPath = Util.GetFilePersistentPath("TouchedBundleSet");
        List<string> stringList = new List<string>();
        foreach (string touchedBundle in this.m_touchedBundleSet)
          stringList.Add(touchedBundle);
        FileStream fileStream;
        using (fileStream = File.Open(filePersistentPath, FileMode.OpenOrCreate, FileAccess.Write))
          new BinaryFormatter().Serialize((Stream) fileStream, (object) stringList);
        this.m_touchedBundleSetDirty = false;
      }
      catch (Exception ex)
      {
        Debug.LogError((object) "SaveTouchedBundleSet Error: {0}", (object) ex.Message);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddBundle2TouchedBundleSet(string bundleName, bool checkBundleData = true)
    {
      if (checkBundleData && this.m_bundleDataHelper.GetBundleDataByName(bundleName) == null || !this.m_touchedBundleSet.Add(bundleName))
        return false;
      this.m_touchedBundleSetDirty = true;
      return true;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAssetInTouchedBundleSet(string assetPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartAssetBundleManifestLoading(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator AssetBundleManifestLoadingWorker()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnAssetBundleManifestLoadingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    protected string GetAssetBundleManifestBundleName()
    {
      return Util.GetCurrentTargetPlatform();
    }

    protected event Action<bool> m_eventAssetBundleManifestLoading
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartAssetBundlePreUpdateing(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartAssetBundleUpdateingWorkers(
      Action<bool> onEnd,
      Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator AssetBundleUpdateingWorker(
      Action<bool> onEnd,
      Action<BundleData.SingleBundleData> onSingleBundleDownloadSuccess = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator WaitForTimeSecond(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CalcPreUpdateBundleListByBundleData()
    {
      if (this.State < ResourceManager.RMState.BundleDataLoadEnd || this.m_skipAssetBundlePreUpdateing)
        return;
      if (this.GetCustomPreUpdateBundleListFunc != null)
        this.m_customPreUpdateBundleList = this.GetCustomPreUpdateBundleListFunc();
      if (PlayerPrefs.GetInt(ResourceManager.m_preUpdateVersion4BundleDataKey) == this.m_bundleData.m_version && !this.IsForceCustomPreUpdate() || this.m_updateingBundleList != null)
        return;
      this.m_updateingBundleList = new List<BundleData.SingleBundleData>();
      if (this.GetCustomSkipPreUpdateBundleListFunc != null)
        this.m_customSkipPreUpdateBundleList = this.GetCustomSkipPreUpdateBundleListFunc();
      foreach (BundleData.SingleBundleData bundle1 in this.m_bundleData.m_bundleList)
      {
        if (this.IsBundleInCustomPreUpdateList(bundle1.m_bundleName))
          this.m_updateingBundleList.Add(bundle1);
        else if (this.m_touchedBundleSet.Contains(bundle1.m_bundleName))
        {
          BundleData.SingleBundleData singleBundleData1 = (BundleData.SingleBundleData) null;
          BundleData.SingleBundleData singleBundleData2 = (BundleData.SingleBundleData) null;
          bool flag = false;
          if ((UnityEngine.Object) this.m_lastPreUpdateBundleData != (UnityEngine.Object) null)
          {
            foreach (BundleData.SingleBundleData bundle2 in this.m_lastPreUpdateBundleData.m_bundleList)
            {
              if (bundle2.m_bundleName == bundle1.m_bundleName)
              {
                singleBundleData1 = bundle2;
                break;
              }
            }
          }
          if ((UnityEngine.Object) this.m_streamingAssetsBundleData != (UnityEngine.Object) null)
          {
            foreach (BundleData.SingleBundleData bundle2 in this.m_streamingAssetsBundleData.m_bundleList)
            {
              if (bundle2.m_bundleName == bundle1.m_bundleName)
              {
                singleBundleData2 = !bundle2.m_isInStreamingAssets ? (BundleData.SingleBundleData) null : bundle2;
                break;
              }
            }
          }
          BundleData.SingleBundleData singleBundleData3;
          if (singleBundleData1 == null || singleBundleData2 == null)
          {
            singleBundleData3 = singleBundleData1 ?? singleBundleData2;
            if (singleBundleData2 != null)
              flag = this.m_streamingAssetsMissingBundles.Contains(singleBundleData2);
          }
          else
          {
            singleBundleData3 = singleBundleData1.m_version <= singleBundleData2.m_version ? singleBundleData2 : singleBundleData1;
            if (singleBundleData2.m_version > singleBundleData1.m_version)
              flag = this.m_streamingAssetsMissingBundles.Contains(singleBundleData2);
          }
          if (singleBundleData3 == null || singleBundleData3.m_version != bundle1.m_version || flag)
          {
            if (!this.IsBundleInSkipPreUpdateList(bundle1.m_bundleName))
              this.m_updateingBundleList.Add(bundle1);
            else
              Debug.Log("Skip preUpdate BundleName:" + bundle1.m_bundleName);
          }
        }
      }
      this.m_assetBundleUpdateingTotalByte = 0L;
      foreach (BundleData.SingleBundleData updateingBundle in this.m_updateingBundleList)
        this.m_assetBundleUpdateingTotalByte += updateingBundle.m_size;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected BundleData.SingleBundleData GetSingleBundleData4PreUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnAssetBundlePreUpdateingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAssetBundleUpdateingPercent()
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetPreUpdateingDownloadedBytes()
    {
      return this.m_assetBundleUpdateingDownloadedByte;
    }

    public long GetTotalPreUpdateingDownloadBytes()
    {
      this.CalcPreUpdateBundleListByBundleData();
      return this.m_assetBundleUpdateingTotalByte;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartDownloadUntouchedAssets(HashSet<string> assets, Action<bool> OnEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnUntouchedAssetBundleUpdateingWorkerEnd(bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    public long GetTotalUntouchedAssetBundleDownloadBytes()
    {
      return this.m_assetBundleUpdateingTotalByte;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsForceCustomPreUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBundleInCustomPreUpdateList(string bundleName)
    {
      if (this.m_customPreUpdateBundleList != null)
        return this.m_customPreUpdateBundleList.Contains(bundleName);
      return false;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBundleInSkipPreUpdateList(string bundleName)
    {
      if (this.m_customSkipPreUpdateBundleList != null)
        return this.m_customSkipPreUpdateBundleList.Contains(bundleName);
      return false;
    }

    protected event Action<bool> m_eventOnAssetBundleUpdateingEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBundleDataLoading(Action<bool> onEnd)
    {
      Debug.Log("ResourceManager.StartBundleDataLoading");
      if (this.State != ResourceManager.RMState.StreamingAssetsFilesProcessEnd)
      {
        Debug.LogError("StartBundleDataLoading in wrong state");
        onEnd(false);
      }
      else
      {
        this.State = ResourceManager.RMState.BundleDataLoading;
        if (this.m_disableAssetBundle)
        {
          Debug.Log("ResourceManager.StartBundleDataLoading skip");
          this.State = ResourceManager.RMState.BundleDataLoadEnd;
          onEnd(true);
        }
        else
        {
          this.EventOnBundleDataLoadingEnd += onEnd;
          this.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.BundleDataLoadingWorker));
        }
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator BundleDataLoadingWorker()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ResourceManager.\u003CBundleDataLoadingWorker\u003Ec__IteratorC()
      {
        \u0024this = this
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator CheckBundleDataVersionFromUrl(Action<bool, int, uint, bool> onEnd)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ResourceManager.\u003CCheckBundleDataVersionFromUrl\u003Ec__IteratorD()
      {
        onEnd = onEnd,
        \u0024this = this
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBundleDataLoadingWorkerEnd(bool success)
    {
      Debug.Log("ResourceManager.OnBundleDataLoadingWorkerEnd");
      if (success)
      {
        this.State = ResourceManager.RMState.BundleDataLoadEnd;
        foreach (BundleData.SingleBundleData bundle in this.m_bundleData.m_bundleList)
        {
          if (bundle.m_isNeedPreUpdateByDefault)
            this.AddBundle2TouchedBundleSet(bundle.m_bundleName, true);
        }
      }
      if (this.EventOnBundleDataLoadingEnd == null)
        return;
      this.EventOnBundleDataLoadingEnd(success);
      this.EventOnBundleDataLoadingEnd = (Action<bool>) null;
    }

    public BundleData GetBundleData()
    {
      return this.m_bundleData;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBundleDataBasicVersion()
    {
      if ((UnityEngine.Object) this.m_bundleData != (UnityEngine.Object) null)
        return this.m_bundleData.m_basicVersion;
      return 0;
    }

    private event Action<bool> EventOnBundleDataLoadingEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        Action<bool> comparand = this.EventOnBundleDataLoadingEnd;
        Action<bool> action;
        do
        {
          action = comparand;
          comparand = Interlocked.CompareExchange<Action<bool>>(ref this.EventOnBundleDataLoadingEnd, action + value, comparand);
        }
        while (comparand != action);
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckBundleCache()
    {
      if (this.IsBundleCacheSignalFileExist())
        return;
      this.ClearForCacheLost();
      this.CreateBundleCacheSignalFile();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBundleCacheSignalFileExist()
    {
      return File.Exists(string.Format("{0}/{1}", (object) Application.persistentDataPath, (object) "dontmove.tmp").Replace('\\', '/'));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBundleCacheSignalFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearForCacheLost()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator ReserveAsset(
      string path,
      Action<string, UnityEngine.Object> onCompleted,
      bool noErrlog = false,
      int reserveTime = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartReserveAssetsCorutine(
      HashSet<string> pathList,
      IDictionary<string, UnityEngine.Object> assetDict,
      Action onComplete,
      bool loadAsync = false,
      int reserveTime = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ResourceManager.ReserveItem AddAsset2Reserve(
      string path,
      int reserveTime,
      UnityEngine.Object asset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupReserveTimeOut(ResourceManager.ReserveItem reserveItem, int ReserveTime)
    {
      // ISSUE: unable to decompile the method.
    }

    protected void RemoveReserve(string path)
    {
      this.m_assetReserveDict.Remove(path);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveReserve4CacheHit(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearAllReserve(HashSet<string> keepReserve = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TickReserve()
    {
      DateTime currTime = BlackJack.BJFramework.Runtime.Timer.m_currTime;
      if (!(this.m_reserveTickDelayOutTime < currTime))
        return;
      this.m_reserveTickDelayOutTime = currTime.AddSeconds(5.0);
      foreach (KeyValuePair<string, ResourceManager.ReserveItem> keyValuePair in this.m_assetReserveDict)
      {
        if (keyValuePair.Value.m_timeOut != DateTime.MinValue && keyValuePair.Value.m_timeOut < currTime)
          this.m_assetReserve4Remove.Add(keyValuePair.Key);
      }
      if (this.m_assetReserve4Remove.Count == 0)
        return;
      foreach (string path in this.m_assetReserve4Remove)
        this.RemoveReserve(path);
      this.m_assetReserve4Remove.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartStreamingAssetsFilesProcessing(Action<bool> onEnd)
    {
      Debug.Log("ResourceManager.StartStreamingAssetsFilesProcessing");
      if (this.State != ResourceManager.RMState.Inited)
      {
        Debug.LogError("StartProcessStreamingAssetsFiles in wrong state");
        onEnd(false);
      }
      else
      {
        this.State = ResourceManager.RMState.StreamingAssetsFilesProcessing;
        if (this.m_skipStreamingAssetsFileProcessing)
        {
          Debug.Log("ResourceManager.StartStreamingAssetsFilesProcessing skip processing");
          this.State = ResourceManager.RMState.StreamingAssetsFilesProcessEnd;
          onEnd(true);
        }
        else
        {
          int num = PlayerPrefs.GetInt("StreamingAssetsFileListVersion");
          this.m_streamingAssetsBundleData = Resources.Load<BundleData>(PathHelper.StreamAssetBundleDataResourcesPath);
          if ((UnityEngine.Object) this.m_streamingAssetsBundleData == (UnityEngine.Object) null)
          {
            Debug.LogError("Load streamingAssetsBundleData fail");
            onEnd(false);
          }
          else
          {
            this.m_streamingAssetsBundleDataHelper = new BundleDataHelper(this.m_streamingAssetsBundleData, this.m_assetPathIgnoreCase);
            Debug.Log(string.Format("ResourceManager.StartStreamingAssetsFilesProcessing m_streamingAssetsBundleData.version = {0}", (object) this.m_streamingAssetsBundleData.m_version));
            this.m_streamingAssetsFileList = Resources.Load<StreamingAssetsFileList>(PathHelper.StreamingAssetsFileListResourcesName);
            if ((UnityEngine.Object) this.m_streamingAssetsFileList == (UnityEngine.Object) null)
            {
              Debug.LogError("Load StreamingAssetsFileList fail");
              onEnd(false);
            }
            else
            {
              Debug.Log(string.Format("ResourceManager.StartStreamingAssetsFilesProcessing StreamingAssetsFileList: curVer = {0}, oldVer = {1}", (object) this.m_streamingAssetsFileList.m_version, (object) num));
              foreach (BundleData.SingleBundleData bundle in this.m_streamingAssetsBundleData.m_bundleList)
              {
                BundleData.SingleBundleData singleBundleData = bundle;
                if (singleBundleData.m_isInStreamingAssets && this.m_streamingAssetsFileList.m_fileList.Find((Predicate<StreamingAssetsFileList.ListItem>) (item => item.m_bundleName == singleBundleData.m_bundleName)) == null)
                  this.m_streamingAssetsMissingBundles.Add(singleBundleData);
              }
              if (this.m_streamingAssetsFileList.m_version <= num)
              {
                this.m_currStreamingAssetsFileListVersion = num;
                this.State = ResourceManager.RMState.StreamingAssetsFilesProcessEnd;
                Debug.LogWarning("ResourceManager.StartStreamingAssetsFilesProcessing m_streamingAssetsFileList.m_version <= oldVersion, may skip processing");
                onEnd(true);
              }
              else
              {
                this.m_currStreamingAssetsFileListVersion = this.m_streamingAssetsFileList.m_version;
                this.m_eventOnStreamingAssetsFilesProcessingEnd += onEnd;
                this.m_corutineHelper.StartCorutine(new Func<IEnumerator>(this.StreamingAssetsFileProcessor));
              }
            }
          }
        }
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator StreamingAssetsFileProcessor()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ResourceManager.\u003CStreamingAssetsFileProcessor\u003Ec__IteratorF()
      {
        \u0024this = this
      };
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DealWithResaveFileInBundleAfterStreamingAssetProcessing()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ResourceManager.\u003CDealWithResaveFileInBundleAfterStreamingAssetProcessing\u003Ec__Iterator10()
      {
        \u0024this = this
      };
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStreamingAssetsFileProcessorEnd(bool success)
    {
      Debug.Log("ResourceManager.OnStreamingAssetsFileProcessorEnd");
      if (success)
      {
        this.State = ResourceManager.RMState.StreamingAssetsFilesProcessEnd;
        PlayerPrefs.SetInt("StreamingAssetsFileListVersion", this.m_currStreamingAssetsFileListVersion);
        PlayerPrefs.Save();
      }
      this.SaveTouchedBundleSet(false);
      if (this.m_eventOnStreamingAssetsFilesProcessingEnd == null)
        return;
      this.m_eventOnStreamingAssetsFilesProcessingEnd(success);
      this.m_eventOnStreamingAssetsFilesProcessingEnd = (Action<bool>) null;
    }

    private event Action<bool> m_eventOnStreamingAssetsFilesProcessingEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        Action<bool> comparand = this.m_eventOnStreamingAssetsFilesProcessingEnd;
        Action<bool> action;
        do
        {
          action = comparand;
          comparand = Interlocked.CompareExchange<Action<bool>>(ref this.m_eventOnStreamingAssetsFilesProcessingEnd, action + value, comparand);
        }
        while (comparand != action);
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator LoadUnityScene(
      string path,
      Action<string, Scene?> onCompleted,
      bool noErrlog = false,
      bool loadAync = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum RMState
    {
      None = 0,
      Inited = 1,
      StreamingAssetsFilesProcessing = 2,
      StreamingAssetsFilesProcessEnd = 3,
      BundleDataLoading = 4,
      BundleDataLoadEnd = 5,
      AssetBundlePreUpdateing = 6,
      AssetBundlePreUpdateEnd = 7,
      AssetBundleManifestLoading = 8,
      AssetBundleManifestLoadEnd = 9,
      Ready = 9,
    }

    protected class AssetCacheItem
    {
      public string m_cacheKey;
      public WeakReference m_weakRefrence;
      public bool m_removeReserveOnHit;
    }

    protected class BundleCacheItem
    {
      public static float s_validedHitInterval = 1f;
      public static int m_firstTimeOut = 600;
      public static int m_OnHitTimeOutDelay = 60;
      public string m_bundleName;
      public DateTime m_lastTouchTime;
      public DateTime m_timeOutTime;
      public int m_hitCount;
      public int m_refCount;
      public AssetBundle m_bundle;
      public bool m_dontUnload;
      public List<ResourceManager.BundleCacheItem> m_dependBundleCacheList;

      public void AddRefrence()
      {
        ++this.m_refCount;
      }

      public void RemoveRefrence()
      {
        --this.m_refCount;
      }
    }

    public class BundleLoadingCtx
    {
      public string m_bundleName;
      public AssetBundle m_bundle;
      public bool m_isEnd;
      public int m_ref;
    }

    protected class ReserveItem
    {
      public DateTime m_timeOut = DateTime.MinValue;
      public UnityEngine.Object m_asset;
    }
  }
}
