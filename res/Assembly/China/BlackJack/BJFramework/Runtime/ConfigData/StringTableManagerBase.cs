﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.ConfigData.StringTableManagerBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime.ConfigData
{
  public abstract class StringTableManagerBase
  {
    protected ClientConfigDataLoaderBase m_configLoader;
    protected string m_currLocalization;
    protected List<string> m_localizationList;

    [MethodImpl((MethodImplOptions) 32768)]
    public StringTableManagerBase(ClientConfigDataLoaderBase configLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerator InitLocalization(string localization, Action<bool> onEnd)
    {
      return this.SetLocalization(localization, onEnd);
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual IEnumerator SetLocalization(string localization, Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual IEnumerator InitDefaultStringTable(Action<bool> onEnd)
    {
      throw new NotImplementedException();
    }

    protected virtual void ClearCurrentLocalizeion()
    {
      throw new NotImplementedException();
    }

    public virtual string GetStringInDefaultStringTable(string key)
    {
      throw new NotImplementedException();
    }
  }
}
