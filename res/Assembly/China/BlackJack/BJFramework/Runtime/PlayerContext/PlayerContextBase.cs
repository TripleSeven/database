﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.PlayerContextBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.Utils;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  [HotFix]
  public abstract class PlayerContextBase : ITickable, IPlayerContextNetworkEventHandler
  {
    protected IPlayerContextNetworkClient m_networkClient;
    protected TinyCorutineHelper m_tinyCorutineHelper;
    protected PlayerContextStateMachine m_fsm;
    protected string m_deviceId;
    protected string m_clientVersion;
    private int m_loginChannelId;
    private int m_bornChannelId;
    private string m_localization;
    protected string m_sessionToken;
    protected bool m_inited;
    private DateTime? m_serverTimeSynced;
    private DateTime m_localTimeAtServerTimeSynced;
    private DateTime m_currTickServerTime;
    [DoNotToLua]
    private PlayerContextBase.LuaExportHelper luaExportHelper;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;
    private LuaFunction m_IsInited_hotfix;
    private LuaFunction m_OnPlayerInfoInitEndNtf_hotfix;
    private LuaFunction m_SyncServerTimeDateTime_hotfix;
    private LuaFunction m_GetCurrServerTime_hotfix;
    private LuaFunction m_GetCurrTickServerTime_hotfix;
    private LuaFunction m_Disconnect_hotfix;
    private LuaFunction m_StartGameAuthLoginStringInt32StringStringInt32Int32String_hotfix;
    private LuaFunction m_SetSessionTokenStringInt32Int32_hotfix;
    private LuaFunction m_StartGameSessionLogin_hotfix;
    private LuaFunction m_SendPlayerInfoInitReq_hotfix;
    private LuaFunction m_SendPlayerInfoReqOnReloginBySessionBoolean_hotfix;
    private LuaFunction m_SendWorldEnterReqOnReloginBySession_hotfix;
    private LuaFunction m_CheckForSessionLogin_hotfix;
    private LuaFunction m_GetDeviceId_hotfix;
    private LuaFunction m_GetClientVersion_hotfix;
    private LuaFunction m_OnTimeJumped_hotfix;
    private LuaFunction m_add_EventOnGameServerConnectedAction_hotfix;
    private LuaFunction m_remove_EventOnGameServerConnectedAction_hotfix;
    private LuaFunction m_add_EventOnGameServerNetworkErrorAction`1_hotfix;
    private LuaFunction m_remove_EventOnGameServerNetworkErrorAction`1_hotfix;
    private LuaFunction m_add_EventOnGameServerDisconnectedAction_hotfix;
    private LuaFunction m_remove_EventOnGameServerDisconnectedAction_hotfix;
    private LuaFunction m_add_EventOnLoginByAuthTokenAckFunc`4_hotfix;
    private LuaFunction m_remove_EventOnLoginByAuthTokenAckFunc`4_hotfix;
    private LuaFunction m_add_EventOnLoginBySessionTokenAckFunc`2_hotfix;
    private LuaFunction m_remove_EventOnLoginBySessionTokenAckFunc`2_hotfix;
    private LuaFunction m_add_EventOnGameServerDataUnsyncNotifyAction_hotfix;
    private LuaFunction m_remove_EventOnGameServerDataUnsyncNotifyAction_hotfix;
    private LuaFunction m_add_EventOnPlayerInfoInitAckAction`1_hotfix;
    private LuaFunction m_remove_EventOnPlayerInfoInitAckAction`1_hotfix;
    private LuaFunction m_add_EventOnPlayerInfoInitEndAction_hotfix;
    private LuaFunction m_remove_EventOnPlayerInfoInitEndAction_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    protected PlayerContextBase()
    {
      // ISSUE: unable to decompile the method.
    }

    public static PlayerContextBase GetInstance()
    {
      return GameManager.Instance.PlayerContext;
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void InitWithNetworkClient(IPlayerContextNetworkClient networkClient)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInited()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    public void BlockNetworkClient(bool isBlock)
    {
      this.m_networkClient.BlockProcessMsg(isBlock);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPlayerInfoInitEndNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract bool IsDataCacheDirtyByPlayerInfoInitAck(
      object msg,
      out bool raiseCriticalDataCacheDirty);

    public abstract bool IsPlayerInfoInitAck4CheckOnly(object msg);

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SyncServerTime(DateTime serverTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCurrServerTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCurrTickServerTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Disconnect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartGameAuthLogin(
      string serverAddress,
      int serverPort,
      string authToken,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSessionToken(string sessionToken, int channelId, int bornChannelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool StartGameSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool SendPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool SendPlayerInfoReqOnReloginBySession(bool checkOnly)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool SendWorldEnterReqOnReloginBySession()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool CheckForSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    public static string CreateFakeAuthToken(string platformUserId, string password)
    {
      return string.Format("1|{0}|{1}|{2}|Self|0|0|0", (object) platformUserId, (object) platformUserId, (object) password);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual string GetDeviceId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual string GetClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerError(int err, string excepionInfo = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnLoginBySessionTokenAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnGameServerMessage(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGameServerMessageExtend(object msg, int msgId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DoNotToLua]
    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnTimeJumped()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnGameServerConnected
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGameServerNetworkError
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGameServerDisconnected
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<int, string, bool, bool> EventOnLoginByAuthTokenAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<int, bool> EventOnLoginBySessionTokenAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual event Action EventOnGameServerDataUnsyncNotify
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual event Action<object> EventOnPlayerInfoInitAck
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual event Action EventOnPlayerInfoInitEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DoNotToLua]
    public PlayerContextBase.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGameServerConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGameServerConnected()
    {
      this.EventOnGameServerConnected = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGameServerNetworkError(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGameServerNetworkError(int obj)
    {
      this.EventOnGameServerNetworkError = (Action<int>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGameServerDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGameServerDisconnected()
    {
      this.EventOnGameServerDisconnected = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool __callDele_EventOnLoginByAuthTokenAck(int arg1, string arg2, bool arg3)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLoginByAuthTokenAck(int arg1, string arg2, bool arg3)
    {
      this.EventOnLoginByAuthTokenAck = (Func<int, string, bool, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool __callDele_EventOnLoginBySessionTokenAck(int arg1)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnLoginBySessionTokenAck(int arg1)
    {
      this.EventOnLoginBySessionTokenAck = (Func<int, bool>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnGameServerDataUnsyncNotify()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnGameServerDataUnsyncNotify()
    {
      this.EventOnGameServerDataUnsyncNotify = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPlayerInfoInitAck(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPlayerInfoInitAck(object obj)
    {
      this.EventOnPlayerInfoInitAck = (Action<object>) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void __callDele_EventOnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    private void __clearDele_EventOnPlayerInfoInitEnd()
    {
      this.EventOnPlayerInfoInitEnd = (Action) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LuaExportHelper
    {
      private PlayerContextBase m_owner;

      public LuaExportHelper(PlayerContextBase owner)
      {
        this.m_owner = owner;
      }

      public void __callDele_EventOnGameServerConnected()
      {
        this.m_owner.__callDele_EventOnGameServerConnected();
      }

      public void __clearDele_EventOnGameServerConnected()
      {
        this.m_owner.__clearDele_EventOnGameServerConnected();
      }

      public void __callDele_EventOnGameServerNetworkError(int obj)
      {
        this.m_owner.__callDele_EventOnGameServerNetworkError(obj);
      }

      public void __clearDele_EventOnGameServerNetworkError(int obj)
      {
        this.m_owner.__clearDele_EventOnGameServerNetworkError(obj);
      }

      public void __callDele_EventOnGameServerDisconnected()
      {
        this.m_owner.__callDele_EventOnGameServerDisconnected();
      }

      public void __clearDele_EventOnGameServerDisconnected()
      {
        this.m_owner.__clearDele_EventOnGameServerDisconnected();
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool __callDele_EventOnLoginByAuthTokenAck(int arg1, string arg2, bool arg3)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __clearDele_EventOnLoginByAuthTokenAck(int arg1, string arg2, bool arg3)
      {
        this.m_owner.__clearDele_EventOnLoginByAuthTokenAck(arg1, arg2, arg3);
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool __callDele_EventOnLoginBySessionTokenAck(int arg1)
      {
        // ISSUE: unable to decompile the method.
      }

      public void __clearDele_EventOnLoginBySessionTokenAck(int arg1)
      {
        this.m_owner.__clearDele_EventOnLoginBySessionTokenAck(arg1);
      }

      public void __callDele_EventOnGameServerDataUnsyncNotify()
      {
        this.m_owner.__callDele_EventOnGameServerDataUnsyncNotify();
      }

      public void __clearDele_EventOnGameServerDataUnsyncNotify()
      {
        this.m_owner.__clearDele_EventOnGameServerDataUnsyncNotify();
      }

      public void __callDele_EventOnPlayerInfoInitAck(object obj)
      {
        this.m_owner.__callDele_EventOnPlayerInfoInitAck(obj);
      }

      public void __clearDele_EventOnPlayerInfoInitAck(object obj)
      {
        this.m_owner.__clearDele_EventOnPlayerInfoInitAck(obj);
      }

      public void __callDele_EventOnPlayerInfoInitEnd()
      {
        this.m_owner.__callDele_EventOnPlayerInfoInitEnd();
      }

      public void __clearDele_EventOnPlayerInfoInitEnd()
      {
        this.m_owner.__clearDele_EventOnPlayerInfoInitEnd();
      }

      public IPlayerContextNetworkClient m_networkClient
      {
        get
        {
          return this.m_owner.m_networkClient;
        }
        set
        {
          this.m_owner.m_networkClient = value;
        }
      }

      public TinyCorutineHelper m_tinyCorutineHelper
      {
        get
        {
          return this.m_owner.m_tinyCorutineHelper;
        }
        set
        {
          this.m_owner.m_tinyCorutineHelper = value;
        }
      }

      public PlayerContextStateMachine m_fsm
      {
        get
        {
          return this.m_owner.m_fsm;
        }
        set
        {
          this.m_owner.m_fsm = value;
        }
      }

      public string m_deviceId
      {
        get
        {
          return this.m_owner.m_deviceId;
        }
        set
        {
          this.m_owner.m_deviceId = value;
        }
      }

      public string m_clientVersion
      {
        get
        {
          return this.m_owner.m_clientVersion;
        }
        set
        {
          this.m_owner.m_clientVersion = value;
        }
      }

      public int m_loginChannelId
      {
        get
        {
          return this.m_owner.m_loginChannelId;
        }
        set
        {
          this.m_owner.m_loginChannelId = value;
        }
      }

      public int m_bornChannelId
      {
        get
        {
          return this.m_owner.m_bornChannelId;
        }
        set
        {
          this.m_owner.m_bornChannelId = value;
        }
      }

      public string m_localization
      {
        get
        {
          return this.m_owner.m_localization;
        }
        set
        {
          this.m_owner.m_localization = value;
        }
      }

      public string m_sessionToken
      {
        get
        {
          return this.m_owner.m_sessionToken;
        }
        set
        {
          this.m_owner.m_sessionToken = value;
        }
      }

      public bool m_inited
      {
        get
        {
          return this.m_owner.m_inited;
        }
        set
        {
          this.m_owner.m_inited = value;
        }
      }

      public DateTime? m_serverTimeSynced
      {
        get
        {
          return this.m_owner.m_serverTimeSynced;
        }
        set
        {
          this.m_owner.m_serverTimeSynced = value;
        }
      }

      public DateTime m_localTimeAtServerTimeSynced
      {
        get
        {
          return this.m_owner.m_localTimeAtServerTimeSynced;
        }
        set
        {
          this.m_owner.m_localTimeAtServerTimeSynced = value;
        }
      }

      public DateTime m_currTickServerTime
      {
        get
        {
          return this.m_owner.m_currTickServerTime;
        }
        set
        {
          this.m_owner.m_currTickServerTime = value;
        }
      }

      public void OnPlayerInfoInitEndNtf()
      {
        this.m_owner.OnPlayerInfoInitEndNtf();
      }

      public void SyncServerTime(DateTime serverTime)
      {
        this.m_owner.SyncServerTime(serverTime);
      }

      public string GetDeviceId()
      {
        return this.m_owner.GetDeviceId();
      }

      public string GetClientVersion()
      {
        return this.m_owner.GetClientVersion();
      }

      public void OnTimeJumped()
      {
        this.m_owner.OnTimeJumped();
      }
    }
  }
}
