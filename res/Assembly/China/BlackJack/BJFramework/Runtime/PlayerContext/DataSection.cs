﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.PlayerContext.DataSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace BlackJack.BJFramework.Runtime.PlayerContext
{
  [Serializable]
  public class DataSection
  {
    [NonSerialized]
    protected ushort m_version;
    protected ushort m_persistentVersion;

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitWithPersistentVersion(ushort version)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetVersion(ushort version)
    {
      this.m_version = version;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedFlushPersistent()
    {
      // ISSUE: unable to decompile the method.
    }

    public void OnFlushed()
    {
      this.m_persistentVersion = this.m_version;
    }

    public ushort Version
    {
      get
      {
        return this.m_version;
      }
    }

    public ushort PersistentVersion
    {
      get
      {
        return this.m_persistentVersion;
      }
    }

    [OnDeserialized]
    private void OnDeserialized(StreamingContext context)
    {
      this.InitWithPersistentVersion(this.m_persistentVersion);
    }
  }
}
