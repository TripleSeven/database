﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.BJFramework.Runtime.DynamicAssemblySettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.BJFramework.Runtime
{
  [Serializable]
  public class DynamicAssemblySettings
  {
    public List<string> DynamicAssemblyList = new List<string>();
    public bool EnableDynamicAssembly;

    [MethodImpl((MethodImplOptions) 32768)]
    public DynamicAssemblySettings()
    {
    }
  }
}
