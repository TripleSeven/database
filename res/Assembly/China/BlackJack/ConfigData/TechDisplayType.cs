﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TechDisplayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TechDisplayType")]
  public enum TechDisplayType
  {
    [ProtoEnum(Name = "TechDisplayType_None", Value = 0)] TechDisplayType_None,
    [ProtoEnum(Name = "TechDisplayType_SoldierTypeLevelUp", Value = 1)] TechDisplayType_SoldierTypeLevelUp,
    [ProtoEnum(Name = "TechDisplayType_SoldierLevelUp", Value = 2)] TechDisplayType_SoldierLevelUp,
    [ProtoEnum(Name = "TechDisplayType_SkillLevelUp", Value = 3)] TechDisplayType_SkillLevelUp,
  }
}
