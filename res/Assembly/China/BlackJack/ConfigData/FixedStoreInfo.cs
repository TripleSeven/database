﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FixedStoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  public class FixedStoreInfo
  {
    public Dictionary<int, ConfigDataFixedStoreItemInfo> StoreItems;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int StoreId { get; set; }
  }
}
