﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRefineryStoneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRefineryStoneInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataRefineryStoneInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Description;
    private int _Rank;
    private string _IconLocation;
    private List<int> _LimitArmyType;
    private int _ConsumeGold;
    private List<int> _RefineryTemplateIds;
    private PropertyModifyType _RefineryPropertyType;
    private List<int> _SlotIds;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Description")]
    public string Description
    {
      get
      {
        return this._Description;
      }
      set
      {
        this._Description = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IconLocation")]
    public string IconLocation
    {
      get
      {
        return this._IconLocation;
      }
      set
      {
        this._IconLocation = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "LimitArmyType")]
    public List<int> LimitArmyType
    {
      get
      {
        return this._LimitArmyType;
      }
      set
      {
        this._LimitArmyType = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsumeGold")]
    public int ConsumeGold
    {
      get
      {
        return this._ConsumeGold;
      }
      set
      {
        this._ConsumeGold = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "RefineryTemplateIds")]
    public List<int> RefineryTemplateIds
    {
      get
      {
        return this._RefineryTemplateIds;
      }
      set
      {
        this._RefineryTemplateIds = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RefineryPropertyType")]
    public PropertyModifyType RefineryPropertyType
    {
      get
      {
        return this._RefineryPropertyType;
      }
      set
      {
        this._RefineryPropertyType = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "SlotIds")]
    public List<int> SlotIds
    {
      get
      {
        return this._SlotIds;
      }
      set
      {
        this._SlotIds = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
