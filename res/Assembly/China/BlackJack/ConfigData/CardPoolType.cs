﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardPoolType")]
  public enum CardPoolType
  {
    [ProtoEnum(Name = "CardPoolType_FreeCardPool", Value = 1)] CardPoolType_FreeCardPool = 1,
    [ProtoEnum(Name = "CardPoolType_CrystalCardPool", Value = 2)] CardPoolType_CrystalCardPool = 2,
    [ProtoEnum(Name = "CardPoolType_ActivityCardPool", Value = 3)] CardPoolType_ActivityCardPool = 3,
  }
}
