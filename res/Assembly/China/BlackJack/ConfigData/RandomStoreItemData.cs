﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomStoreItemData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using SLua;

namespace BlackJack.ConfigData
{
  [CustomLuaClass]
  public class RandomStoreItemData
  {
    public int Id { get; set; }

    public int Weight { get; set; }
  }
}
