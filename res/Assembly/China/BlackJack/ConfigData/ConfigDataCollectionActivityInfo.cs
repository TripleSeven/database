﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [CustomLuaClass]
  [Serializable]
  public class ConfigDataCollectionActivityInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TabImage;
    private string _BackgroundImage;
    private string _StartPlayerResource;
    private int _MapId;
    private List<int> _ExchangeTable;
    private List<CurrencyItemInfo> _CurrencyItemTable;
    private CollectionActivityRewardType _RewardType;
    private int _ScoreRewardGroupId;
    private int _ScoreItemId;
    private List<Int32Pair> _BonusHeroIdList;
    private string _ExchangePanelBg;
    private string _ExchangePanelStateName;
    private IExtension extensionObject;
    public ConfigDataCollectionActivityMapInfo MapInfo;
    public List<CollectionActivityScoreRewardInfo> RewardInfos;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "TabImage")]
    public string TabImage
    {
      get
      {
        return this._TabImage;
      }
      set
      {
        this._TabImage = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "BackgroundImage")]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "StartPlayerResource")]
    public string StartPlayerResource
    {
      get
      {
        return this._StartPlayerResource;
      }
      set
      {
        this._StartPlayerResource = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MapId")]
    public int MapId
    {
      get
      {
        return this._MapId;
      }
      set
      {
        this._MapId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "ExchangeTable")]
    public List<int> ExchangeTable
    {
      get
      {
        return this._ExchangeTable;
      }
      set
      {
        this._ExchangeTable = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "CurrencyItemTable")]
    public List<CurrencyItemInfo> CurrencyItemTable
    {
      get
      {
        return this._CurrencyItemTable;
      }
      set
      {
        this._CurrencyItemTable = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RewardType")]
    public CollectionActivityRewardType RewardType
    {
      get
      {
        return this._RewardType;
      }
      set
      {
        this._RewardType = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreRewardGroupId")]
    public int ScoreRewardGroupId
    {
      get
      {
        return this._ScoreRewardGroupId;
      }
      set
      {
        this._ScoreRewardGroupId = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreItemId")]
    public int ScoreItemId
    {
      get
      {
        return this._ScoreItemId;
      }
      set
      {
        this._ScoreItemId = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, Name = "BonusHeroIdList")]
    public List<Int32Pair> BonusHeroIdList
    {
      get
      {
        return this._BonusHeroIdList;
      }
      set
      {
        this._BonusHeroIdList = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "ExchangePanelBg")]
    public string ExchangePanelBg
    {
      get
      {
        return this._ExchangePanelBg;
      }
      set
      {
        this._ExchangePanelBg = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "ExchangePanelStateName")]
    public string ExchangePanelStateName
    {
      get
      {
        return this._ExchangePanelStateName;
      }
      set
      {
        this._ExchangePanelStateName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
