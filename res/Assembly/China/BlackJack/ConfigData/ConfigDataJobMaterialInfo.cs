﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobMaterialInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobMaterialInfo")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class ConfigDataJobMaterialInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rank;
    private int _SellGold;
    private int _ComposeGold;
    private List<Goods> _ComposeMaterials;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private PropertyModifyType _Property3_ID;
    private int _Property3_Value;
    private PropertyModifyType _Property4_ID;
    private int _Property4_Value;
    private PropertyModifyType _Property5_ID;
    private int _Property5_Value;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private int _AlchemyGold;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobMaterialInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SellGold")]
    public int SellGold
    {
      get
      {
        return this._SellGold;
      }
      set
      {
        this._SellGold = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ComposeGold")]
    public int ComposeGold
    {
      get
      {
        return this._ComposeGold;
      }
      set
      {
        this._ComposeGold = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "ComposeMaterials")]
    public List<Goods> ComposeMaterials
    {
      get
      {
        return this._ComposeMaterials;
      }
      set
      {
        this._ComposeMaterials = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_ID")]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_Value")]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_ID")]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_Value")]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_ID")]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_Value")]
    public int Property3_Value
    {
      get
      {
        return this._Property3_Value;
      }
      set
      {
        this._Property3_Value = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property4_ID")]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property4_Value")]
    public int Property4_Value
    {
      get
      {
        return this._Property4_Value;
      }
      set
      {
        this._Property4_Value = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property5_ID")]
    public PropertyModifyType Property5_ID
    {
      get
      {
        return this._Property5_ID;
      }
      set
      {
        this._Property5_ID = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property5_Value")]
    public int Property5_Value
    {
      get
      {
        return this._Property5_Value;
      }
      set
      {
        this._Property5_Value = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetPathDesc")]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AlchemyGold")]
    public int AlchemyGold
    {
      get
      {
        return this._AlchemyGold;
      }
      set
      {
        this._AlchemyGold = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomDropRewardID")]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayRewardCount")]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
