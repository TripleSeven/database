﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelType")]
  public enum RiftLevelType
  {
    [ProtoEnum(Name = "RiftLevelType_None", Value = 0)] RiftLevelType_None,
    [ProtoEnum(Name = "RiftLevelType_Scenario", Value = 1)] RiftLevelType_Scenario,
    [ProtoEnum(Name = "RiftLevelType_Event", Value = 2)] RiftLevelType_Event,
  }
}
