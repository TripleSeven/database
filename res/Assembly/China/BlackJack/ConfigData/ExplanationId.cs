﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ExplanationId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ExplanationId")]
  public enum ExplanationId
  {
    [ProtoEnum(Name = "ExplanationId_Aniki", Value = 1)] ExplanationId_Aniki = 1,
    [ProtoEnum(Name = "ExplanationId_Thearchy", Value = 2)] ExplanationId_Thearchy = 2,
    [ProtoEnum(Name = "ExplanationId_Arena", Value = 3)] ExplanationId_Arena = 3,
    [ProtoEnum(Name = "ExplanationId_TreasureMap", Value = 4)] ExplanationId_TreasureMap = 4,
    [ProtoEnum(Name = "ExplanationId_MemoryCorridor", Value = 5)] ExplanationId_MemoryCorridor = 5,
    [ProtoEnum(Name = "ExplanationId_Team", Value = 6)] ExplanationId_Team = 6,
    [ProtoEnum(Name = "ExplanationId_Friend", Value = 7)] ExplanationId_Friend = 7,
    [ProtoEnum(Name = "ExplanationId_HeroTraining", Value = 8)] ExplanationId_HeroTraining = 8,
    [ProtoEnum(Name = "ExplanationId_CooperateBattle", Value = 9)] ExplanationId_CooperateBattle = 9,
    [ProtoEnum(Name = "ExplanationId_HeroPhantom", Value = 10)] ExplanationId_HeroPhantom = 10, // 0x0000000A
    [ProtoEnum(Name = "ExplanationId_Rift", Value = 11)] ExplanationId_Rift = 11, // 0x0000000B
    [ProtoEnum(Name = "ExplanationId_Fetters", Value = 12)] ExplanationId_Fetters = 12, // 0x0000000C
    [ProtoEnum(Name = "ExplanationId_FettersFavorability", Value = 13)] ExplanationId_FettersFavorability = 13, // 0x0000000D
    [ProtoEnum(Name = "ExplanationId_FettersConfession", Value = 14)] ExplanationId_FettersConfession = 14, // 0x0000000E
    [ProtoEnum(Name = "ExplanationId_HeroDungeon", Value = 15)] ExplanationId_HeroDungeon = 15, // 0x0000000F
    [ProtoEnum(Name = "ExplanationId_Drill", Value = 16)] ExplanationId_Drill = 16, // 0x00000010
    [ProtoEnum(Name = "ExplanationId_EquipmentForge", Value = 17)] ExplanationId_EquipmentForge = 17, // 0x00000011
    [ProtoEnum(Name = "ExplanationId_Alchemy", Value = 18)] ExplanationId_Alchemy = 18, // 0x00000012
    [ProtoEnum(Name = "ExplanationId_HeroProperty", Value = 19)] ExplanationId_HeroProperty = 19, // 0x00000013
    [ProtoEnum(Name = "ExplanationId_DrillTutor", Value = 20)] ExplanationId_DrillTutor = 20, // 0x00000014
    [ProtoEnum(Name = "ExplanationId_Battle", Value = 21)] ExplanationId_Battle = 21, // 0x00000015
    [ProtoEnum(Name = "ExplanationId_RealtimeArena", Value = 22)] ExplanationId_RealtimeArena = 22, // 0x00000016
    [ProtoEnum(Name = "ExplanationId_SelectCard", Value = 23)] ExplanationId_SelectCard = 23, // 0x00000017
    [ProtoEnum(Name = "ExplanationId_Raffle", Value = 24)] ExplanationId_Raffle = 24, // 0x00000018
    [ProtoEnum(Name = "ExplanationId_SelectCard_ActivityPool4", Value = 25)] ExplanationId_SelectCard_ActivityPool4 = 25, // 0x00000019
    [ProtoEnum(Name = "ExplanationId_SelectCard_ActivityPool5", Value = 26)] ExplanationId_SelectCard_ActivityPool5 = 26, // 0x0000001A
    [ProtoEnum(Name = "ExplanationId_Archive", Value = 27)] ExplanationId_Archive = 27, // 0x0000001B
    [ProtoEnum(Name = "ExplanationId_MemeryExtraction", Value = 28)] ExplanationId_MemeryExtraction = 28, // 0x0000001C
    [ProtoEnum(Name = "ExplanationId_UnchartedScore", Value = 29)] ExplanationId_UnchartedScore = 29, // 0x0000001D
    [ProtoEnum(Name = "ExplanationId_ClimbTower", Value = 30)] ExplanationId_ClimbTower = 30, // 0x0000001E
    [ProtoEnum(Name = "ExplanationId_Guild", Value = 31)] ExplanationId_Guild = 31, // 0x0000001F
    [ProtoEnum(Name = "ExplanationId_SelectCard_ActivityPool10", Value = 32)] ExplanationId_SelectCard_ActivityPool10 = 32, // 0x00000020
    [ProtoEnum(Name = "ExplanationId_SelectCard_ActivityPool11", Value = 33)] ExplanationId_SelectCard_ActivityPool11 = 33, // 0x00000021
    [ProtoEnum(Name = "ExplanationId_SelectCard_ActivityPool12", Value = 34)] ExplanationId_SelectCard_ActivityPool12 = 34, // 0x00000022
    [ProtoEnum(Name = "ExplanationId_Guild_GameList", Value = 35)] ExplanationId_Guild_GameList = 35, // 0x00000023
    [ProtoEnum(Name = "ExplanationId_Guild_Raid", Value = 36)] ExplanationId_Guild_Raid = 36, // 0x00000024
    [ProtoEnum(Name = "ExplanationId_Guild_MassiveCombat", Value = 37)] ExplanationId_Guild_MassiveCombat = 37, // 0x00000025
    [ProtoEnum(Name = "ExplanationId_EternalShrine", Value = 38)] ExplanationId_EternalShrine = 38, // 0x00000026
    [ProtoEnum(Name = "ExplanationId_CollectionActivity", Value = 39)] ExplanationId_CollectionActivity = 39, // 0x00000027
    [ProtoEnum(Name = "ExplanationId_Tarot", Value = 40)] ExplanationId_Tarot = 40, // 0x00000028
    [ProtoEnum(Name = "ExplanationId_BackFlowActivity", Value = 41)] ExplanationId_BackFlowActivity = 41, // 0x00000029
    [ProtoEnum(Name = "ExplanationId_HeroAnthem", Value = 42)] ExplanationId_HeroAnthem = 42, // 0x0000002A
    [ProtoEnum(Name = "ExplanationId_CollectionActivity_Score", Value = 43)] ExplanationId_CollectionActivity_Score = 43, // 0x0000002B
    [ProtoEnum(Name = "ExplanationId_PeakArenaBattleTeamManagement", Value = 44)] ExplanationId_PeakArenaBattleTeamManagement = 44, // 0x0000002C
    [ProtoEnum(Name = "ExplanationId_PeakArena", Value = 45)] ExplanationId_PeakArena = 45, // 0x0000002D
    [ProtoEnum(Name = "ExplanationId_PeakArenaKnockout", Value = 46)] ExplanationId_PeakArenaKnockout = 46, // 0x0000002E
  }
}
