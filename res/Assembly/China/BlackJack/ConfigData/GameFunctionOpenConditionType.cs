﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GameFunctionOpenConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GameFunctionOpenConditionType")]
  public enum GameFunctionOpenConditionType
  {
    [ProtoEnum(Name = "GameFunctionOpenConditionType_None", Value = 0)] GameFunctionOpenConditionType_None,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_PlayerLevel", Value = 1)] GameFunctionOpenConditionType_PlayerLevel,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_Scenario", Value = 2)] GameFunctionOpenConditionType_Scenario,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_WaypointEvent", Value = 3)] GameFunctionOpenConditionType_WaypointEvent,
    [ProtoEnum(Name = "GameFunctionOpenConditionType_RiftLevel", Value = 4)] GameFunctionOpenConditionType_RiftLevel,
  }
}
