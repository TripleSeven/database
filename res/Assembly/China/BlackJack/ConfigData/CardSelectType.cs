﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardSelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardSelectType")]
  public enum CardSelectType
  {
    [ProtoEnum(Name = "CardSelectType_SingleSelect", Value = 1)] CardSelectType_SingleSelect = 1,
    [ProtoEnum(Name = "CardSelectType_TenSelect", Value = 2)] CardSelectType_TenSelect = 2,
    [ProtoEnum(Name = "CardSelectType_BothSelect", Value = 3)] CardSelectType_BothSelect = 3,
  }
}
