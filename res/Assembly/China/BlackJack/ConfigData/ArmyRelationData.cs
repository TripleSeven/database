﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ArmyRelationData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

namespace BlackJack.ConfigData
{
  public struct ArmyRelationData
  {
    public int Attack;
    public int Defend;
    public int Magic;
    public int MagicDefend;
  }
}
