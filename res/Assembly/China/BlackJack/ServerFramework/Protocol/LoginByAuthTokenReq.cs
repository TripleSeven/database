﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ServerFramework.Protocol.LoginByAuthTokenReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using ProtoBuf;
using SLua;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ServerFramework.Protocol
{
  [HotFix(true, m_isCtorOnly = true)]
  [ProtoContract(Name = "LoginByAuthTokenReq")]
  [Serializable]
  public class LoginByAuthTokenReq : IExtensible
  {
    private string _AuthToken;
    private string _ClientVersion;
    private string _ClientDeviceId;
    private string _Localization;
    private int _CurrChannelId;
    private int _BornChannelId;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginByAuthTokenReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "AuthToken")]
    public string AuthToken
    {
      get
      {
        return this._AuthToken;
      }
      set
      {
        this._AuthToken = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClientVersion")]
    public string ClientVersion
    {
      get
      {
        return this._ClientVersion;
      }
      set
      {
        this._ClientVersion = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClientDeviceId")]
    public string ClientDeviceId
    {
      get
      {
        return this._ClientDeviceId;
      }
      set
      {
        this._ClientDeviceId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Localization")]
    public string Localization
    {
      get
      {
        return this._Localization;
      }
      set
      {
        this._Localization = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CurrChannelId")]
    public int CurrChannelId
    {
      get
      {
        return this._CurrChannelId;
      }
      set
      {
        this._CurrChannelId = value;
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BornChannelId")]
    public int BornChannelId
    {
      get
      {
        return this._BornChannelId;
      }
      set
      {
        this._BornChannelId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
