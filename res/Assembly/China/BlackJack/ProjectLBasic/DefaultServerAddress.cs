﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.DefaultServerAddress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using BlackJack.BJFramework.Runtime;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [CustomLuaClass]
  public class DefaultServerAddress
  {
    private static string[] m_address;

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Load()
    {
      string str1 = string.Empty;
      WWW www1 = new WWW(Util.GetFileStreamingAssetsPath4WWW("Config/DefaultServerAddress.txt"));
      do
        ;
      while (!www1.isDone);
      if (!string.IsNullOrEmpty(www1.error))
        Debug.LogError("DefaultServerAddress.Load Config/DefaultServerAddress.txt Error: " + www1.error + " TryLoad DefaultServerAddress2");
      else
        str1 = www1.text;
      if (string.IsNullOrEmpty(str1))
      {
        WWW www2 = new WWW(Util.GetFileStreamingAssetsPath4WWW("Config/DefaultServerAddress2.txt"));
        do
          ;
        while (!www2.isDone);
        if (!string.IsNullOrEmpty(www2.error))
        {
          Debug.LogError("DefaultServerAddress.Load Config/DefaultServerAddress2.txt Error: " + www2.error);
          return false;
        }
        str1 = www2.text;
      }
      string[] strArray1 = Array.ConvertAll<string, string>(str1.Split('\n'), (Converter<string, string>) (s => s.Trim()));
      string[] strArray2 = (string[]) null;
      foreach (string str2 in strArray1)
      {
        if (!str2.StartsWith("//"))
        {
          strArray2 = Array.ConvertAll<string, string>(str2.Split(','), (Converter<string, string>) (inputString => inputString.Trim()));
          if (strArray2 == null || strArray2.Length < 6)
            Debug.LogError(string.Format("DefaultServerAddress.Load: {0} 's segments count is less than 6.", (object) str2));
          else
            break;
        }
      }
      for (int index = 0; index < strArray2.Length; ++index)
      {
        if (string.IsNullOrEmpty(strArray2[index]))
        {
          Debug.LogError(string.Format("DefaultServerAddress.Load address[{0}] is empty.", (object) index));
          return false;
        }
      }
      DefaultServerAddress.m_address = strArray2;
      return true;
    }

    public static string ServerListURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string AnnouncementsURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string URLToSaveUpdateClientServerURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        if (DefaultServerAddress.m_address == null)
          Debug.LogError("DefaultServerAddress.URLToSaveUpdateClientServerURL: m_address is null.");
        return string.Format("{0}?a={1}", (object) DefaultServerAddress.m_address[2], (object) DateTime.Now.ToString("MMddHHmm"));
      }
    }

    public static string GMUserConfigURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string ReportBugURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
