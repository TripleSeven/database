﻿// Decompiled with JetBrains decompiler
// Type: HelpCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

public class HelpCmd : IDebugCmd
{
  public const string _NAME = "help";
  public const string _DESC = "help: print all command's description";

  public void Execute(string strParams)
  {
    DebugCmdManager.instance.PringAllCmdDescription();
  }

  public string GetHelpDesc()
  {
    return "help: print all command's description";
  }

  public string GetName()
  {
    return "help";
  }
}
