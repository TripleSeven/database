﻿// Decompiled with JetBrains decompiler
// Type: JsonMapper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: D28234CD-8A19-48D3-90D3-13E5097BD5FD
// Assembly location: D:\User\Desktop\ldlls_cn\pe000a49a7e6.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

public class JsonMapper
{
  private static readonly object array_metadata_lock = new object();
  private static readonly object conv_ops_lock = new object();
  private static readonly object object_metadata_lock = new object();
  private static readonly object type_properties_lock = new object();
  private static readonly object static_writer_lock = new object();
  private static int max_nesting_depth = 100;
  private static IDictionary<System.Type, ArrayMetadata> array_metadata = (IDictionary<System.Type, ArrayMetadata>) new Dictionary<System.Type, ArrayMetadata>();
  private static IDictionary<System.Type, IDictionary<System.Type, MethodInfo>> conv_ops = (IDictionary<System.Type, IDictionary<System.Type, MethodInfo>>) new Dictionary<System.Type, IDictionary<System.Type, MethodInfo>>();
  private static IDictionary<System.Type, ObjectMetadata> object_metadata = (IDictionary<System.Type, ObjectMetadata>) new Dictionary<System.Type, ObjectMetadata>();
  private static IDictionary<System.Type, IList<PropertyMetadata>> type_properties = (IDictionary<System.Type, IList<PropertyMetadata>>) new Dictionary<System.Type, IList<PropertyMetadata>>();
  private static JsonWriter static_writer = new JsonWriter();
  private static IFormatProvider datetime_format = (IFormatProvider) DateTimeFormatInfo.InvariantInfo;
  private static IDictionary<System.Type, ExporterFunc> base_exporters_table = (IDictionary<System.Type, ExporterFunc>) new Dictionary<System.Type, ExporterFunc>();
  private static IDictionary<System.Type, ExporterFunc> custom_exporters_table = (IDictionary<System.Type, ExporterFunc>) new Dictionary<System.Type, ExporterFunc>();
  private static IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>> base_importers_table = (IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>>) new Dictionary<System.Type, IDictionary<System.Type, ImporterFunc>>();
  private static IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>> custom_importers_table = (IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>>) new Dictionary<System.Type, IDictionary<System.Type, ImporterFunc>>();

  [MethodImpl((MethodImplOptions) 32768)]
  static JsonMapper()
  {
    JsonMapper.RegisterBaseExporters();
    JsonMapper.RegisterBaseImporters();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void AddArrayMetadata(System.Type type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void AddObjectMetadata(System.Type type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void AddTypeProperties(System.Type type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static MethodInfo GetConvOp(System.Type t1, System.Type t2)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static object ReadValue(System.Type inst_type, JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static IJsonWrapper ReadValue(WrapperFactory factory, JsonReader reader)
  {
    reader.Read();
    if (reader.Token == JsonToken.ArrayEnd || reader.Token == JsonToken.Null)
      return (IJsonWrapper) null;
    IJsonWrapper jsonWrapper1 = factory();
    if (reader.Token == JsonToken.String)
    {
      jsonWrapper1.SetString((string) reader.Value);
      return jsonWrapper1;
    }
    if (reader.Token == JsonToken.Double)
    {
      jsonWrapper1.SetDouble((double) reader.Value);
      return jsonWrapper1;
    }
    if (reader.Token == JsonToken.Int)
    {
      jsonWrapper1.SetInt((int) reader.Value);
      return jsonWrapper1;
    }
    if (reader.Token == JsonToken.Long)
    {
      jsonWrapper1.SetLong((long) reader.Value);
      return jsonWrapper1;
    }
    if (reader.Token == JsonToken.Boolean)
    {
      jsonWrapper1.SetBoolean((bool) reader.Value);
      return jsonWrapper1;
    }
    if (reader.Token == JsonToken.ArrayStart)
    {
      jsonWrapper1.SetJsonType(JsonType.Array);
      while (true)
      {
        IJsonWrapper jsonWrapper2 = JsonMapper.ReadValue(factory, reader);
        if (reader.Token != JsonToken.ArrayEnd)
          jsonWrapper1.Add((object) jsonWrapper2);
        else
          break;
      }
    }
    else if (reader.Token == JsonToken.ObjectStart)
    {
      jsonWrapper1.SetJsonType(JsonType.Object);
      while (true)
      {
        reader.Read();
        if (reader.Token != JsonToken.ObjectEnd)
        {
          string str = (string) reader.Value;
          jsonWrapper1[(object) str] = (object) JsonMapper.ReadValue(factory, reader);
        }
        else
          break;
      }
    }
    return jsonWrapper1;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void RegisterBaseExporters()
  {
    JsonMapper.base_exporters_table[typeof (byte)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (char)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (DateTime)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (Decimal)] = (ExporterFunc) ((obj, writer) => writer.Write((Decimal) obj));
    JsonMapper.base_exporters_table[typeof (sbyte)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (short)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (ushort)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (uint)] = (ExporterFunc) ((obj, writer) =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.base_exporters_table[typeof (ulong)] = (ExporterFunc) ((obj, writer) => writer.Write((ulong) obj));
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void RegisterBaseImporters()
  {
    ImporterFunc importer1 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (byte), importer1);
    ImporterFunc importer2 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (ulong), importer2);
    ImporterFunc importer3 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (sbyte), importer3);
    ImporterFunc importer4 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (short), importer4);
    ImporterFunc importer5 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (ushort), importer5);
    ImporterFunc importer6 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (uint), importer6);
    ImporterFunc importer7 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (float), importer7);
    ImporterFunc importer8 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (int), typeof (double), importer8);
    ImporterFunc importer9 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (double), typeof (Decimal), importer9);
    ImporterFunc importer10 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (long), typeof (uint), importer10);
    ImporterFunc importer11 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (string), typeof (char), importer11);
    ImporterFunc importer12 = (ImporterFunc) (input =>
    {
      // ISSUE: unable to decompile the method.
    });
    JsonMapper.RegisterImporter(JsonMapper.base_importers_table, typeof (string), typeof (DateTime), importer12);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void RegisterImporter(
    IDictionary<System.Type, IDictionary<System.Type, ImporterFunc>> table,
    System.Type json_type,
    System.Type value_type,
    ImporterFunc importer)
  {
    if (!table.ContainsKey(json_type))
      table.Add(json_type, (IDictionary<System.Type, ImporterFunc>) new Dictionary<System.Type, ImporterFunc>());
    table[json_type][value_type] = importer;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void WriteValue(object obj, JsonWriter writer, bool writer_is_private, int depth)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static string ToJson(object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void ToJson(object obj, JsonWriter writer)
  {
    JsonMapper.WriteValue(obj, writer, false, 0);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static JsonData ToObject(JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static JsonData ToObject(TextReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static JsonData ToObject(string json)
  {
    return (JsonData) JsonMapper.ToWrapper((WrapperFactory) (() => (IJsonWrapper) new JsonData()), json);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static T ToObject<T>(JsonReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static T ToObject<T>(TextReader reader)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static T ToObject<T>(string json)
  {
    // ISSUE: unable to decompile the method.
  }

  public static IJsonWrapper ToWrapper(WrapperFactory factory, JsonReader reader)
  {
    return JsonMapper.ReadValue(factory, reader);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static IJsonWrapper ToWrapper(WrapperFactory factory, string json)
  {
    JsonReader reader = new JsonReader(json);
    return JsonMapper.ReadValue(factory, reader);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void RegisterExporter<T>(ExporterFunc<T> exporter)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void RegisterImporter<TJson, TValue>(ImporterFunc<TJson, TValue> importer)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void UnregisterExporters()
  {
    JsonMapper.custom_exporters_table.Clear();
  }

  public static void UnregisterImporters()
  {
    JsonMapper.custom_importers_table.Clear();
  }
}
