﻿// Decompiled with JetBrains decompiler
// Type: GoogleTransTool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SimpleGoogleTranslation;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GoogleTransTool : MonoBehaviour
{
  public static string SystemLanguage;

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Translate(string source, Action<bool, string> OnTranslateComplete)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Detect(string source, Action<bool, string> OnDetectComplete)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static string HandleCallback(List<string> str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static string HandleDetectCallback(List<string> str)
  {
    // ISSUE: unable to decompile the method.
  }

  public static string GetLanguageNameByAbbrLang(string sysLang)
  {
    return GoogleLanguageMap.GetLanguageNameByAbbrLang(sysLang);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static string GetSystemLanguageAbbr()
  {
    // ISSUE: unable to decompile the method.
  }
}
