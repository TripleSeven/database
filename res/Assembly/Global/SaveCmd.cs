﻿// Decompiled with JetBrains decompiler
// Type: SaveCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public class SaveCmd : IDebugCmd
{
  public void Execute(string strParams)
  {
    DebugConsoleMode.instance.Save();
  }

  public string GetHelpDesc()
  {
    return "save : Save the current log";
  }

  public string GetName()
  {
    return "Save";
  }
}
