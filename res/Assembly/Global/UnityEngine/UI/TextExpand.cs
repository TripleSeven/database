﻿// Decompiled with JetBrains decompiler
// Type: UnityEngine.UI.TextExpand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
  [AddComponentMenu("UI/TextExpand", 10)]
  public class TextExpand : UnityEngine.UI.Text, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler, IEventSystemHandler
  {
    protected string m_OutputText;
    protected readonly List<TextExpand.LinkInfo> m_LinkInfos;
    protected static readonly StringBuilder s_TextBuilder;
    [SerializeField]
    private TextExpand.LinkClickEvent m_OnLinkClick;
    [SerializeField]
    private TextExpand.MeshPopulateEvent m_OnMeshPopulateEnd;
    private static readonly Regex s_LinkRegex;
    private static readonly Regex s_LinkRegexDetail;

    [MethodImpl((MethodImplOptions) 32768)]
    public TextExpand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPopulateMesh(VertexHelper toFill)
    {
      // ISSUE: unable to decompile the method.
    }

    public override void SetVerticesDirty()
    {
      base.SetVerticesDirty();
      this.UpdateOutputText();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateOutputText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual string GetOutputText(string outputText)
    {
      // ISSUE: unable to decompile the method.
    }

    public TextExpand.LinkClickEvent onLinkClick
    {
      get
      {
        return this.m_OnLinkClick;
      }
      set
      {
        this.m_OnLinkClick = value;
      }
    }

    public TextExpand.MeshPopulateEvent onMeshPopulateEnd
    {
      get
      {
        return this.m_OnMeshPopulateEnd;
      }
      set
      {
        this.m_OnMeshPopulateEnd = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static TextExpand()
    {
      // ISSUE: unable to decompile the method.
    }

    [Serializable]
    public class MeshPopulateEvent : UnityEvent<List<TextExpand.LinkInfo>>
    {
    }

    [Serializable]
    public class LinkClickEvent : UnityEvent<string>
    {
    }

    public class LinkInfo
    {
      public int startIndex = -1;
      public int endIndex = -1;
      public string type = string.Empty;
      public bool isCareClick = true;
      public Color bgImageColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
      public readonly List<Rect> boxes = new List<Rect>();
    }
  }
}
