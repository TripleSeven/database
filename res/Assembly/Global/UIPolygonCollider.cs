﻿// Decompiled with JetBrains decompiler
// Type: UIPolygonCollider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (PolygonCollider2D))]
public class UIPolygonCollider : Image
{
  private PolygonCollider2D m_polygon;

  protected UIPolygonCollider()
  {
    this.useLegacyMeshGeneration = true;
  }

  private PolygonCollider2D Polygon
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  protected override void OnPopulateMesh(VertexHelper vh)
  {
    vh.Clear();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
  {
    // ISSUE: unable to decompile the method.
  }
}
