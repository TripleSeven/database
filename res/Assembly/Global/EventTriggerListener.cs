﻿// Decompiled with JetBrains decompiler
// Type: EventTriggerListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerListener : EventTrigger
{
  public EventTriggerListener.VoidDelegate onClick;
  public EventTriggerListener.VoidDelegate onDown;
  public EventTriggerListener.VoidDelegate onUp;
  public EventTriggerListener.VoidDelegate onEnter;
  public EventTriggerListener.VoidDelegate onExit;
  public EventTriggerListener.VoidDelegate onSelect;
  public EventTriggerListener.VoidDelegate onDeselect;
  public EventTriggerListener.VoidDelegate onUpdateSelect;
  public EventTriggerListener.VoidDelegate onBeginDrag;
  public EventTriggerListener.VoidDelegate onEndDrag;

  [MethodImpl((MethodImplOptions) 32768)]
  public static EventTriggerListener Get(GameObject go)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static EventTriggerListener Get(Transform transform)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerClick(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerUp(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerDown(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerEnter(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnPointerExit(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnSelect(BaseEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnDeselect(BaseEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnBeginDrag(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnEndDrag(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override void OnUpdateSelected(BaseEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  public delegate void VoidDelegate(GameObject go);
}
