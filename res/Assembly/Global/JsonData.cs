﻿// Decompiled with JetBrains decompiler
// Type: JsonData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.CompilerServices;

public class JsonData : IJsonWrapper, IEquatable<JsonData>, IList, IOrderedDictionary, ICollection, IEnumerable, IDictionary
{
  private IList<JsonData> inst_array;
  private bool inst_boolean;
  private double inst_double;
  private int inst_int;
  private long inst_long;
  private IDictionary<string, JsonData> inst_object;
  private string inst_string;
  private string json;
  private JsonType type;
  private IList<KeyValuePair<string, JsonData>> object_list;

  public JsonData()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(bool boolean)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(double number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(int number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(long number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonData(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  public int Count
  {
    get
    {
      return this.EnsureCollection().Count;
    }
  }

  public bool IsArray
  {
    get
    {
      return this.type == JsonType.Array;
    }
  }

  public bool IsBoolean
  {
    get
    {
      return this.type == JsonType.Boolean;
    }
  }

  public bool IsDouble
  {
    get
    {
      return this.type == JsonType.Double;
    }
  }

  public bool IsInt
  {
    get
    {
      return this.type == JsonType.Int;
    }
  }

  public bool IsLong
  {
    get
    {
      return this.type == JsonType.Long;
    }
  }

  public bool IsObject
  {
    get
    {
      return this.type == JsonType.Object;
    }
  }

  public bool IsString
  {
    get
    {
      return this.type == JsonType.String;
    }
  }

  int ICollection.Count
  {
    get
    {
      return this.Count;
    }
  }

  bool ICollection.IsSynchronized
  {
    get
    {
      return this.EnsureCollection().IsSynchronized;
    }
  }

  object ICollection.SyncRoot
  {
    get
    {
      return this.EnsureCollection().SyncRoot;
    }
  }

  bool IDictionary.IsFixedSize
  {
    get
    {
      return this.EnsureDictionary().IsFixedSize;
    }
  }

  bool IDictionary.IsReadOnly
  {
    get
    {
      return this.EnsureDictionary().IsReadOnly;
    }
  }

  ICollection IDictionary.Keys
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  ICollection IDictionary.Values
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  bool IJsonWrapper.IsArray
  {
    get
    {
      return this.IsArray;
    }
  }

  bool IJsonWrapper.IsBoolean
  {
    get
    {
      return this.IsBoolean;
    }
  }

  bool IJsonWrapper.IsDouble
  {
    get
    {
      return this.IsDouble;
    }
  }

  bool IJsonWrapper.IsInt
  {
    get
    {
      return this.IsInt;
    }
  }

  bool IJsonWrapper.IsLong
  {
    get
    {
      return this.IsLong;
    }
  }

  bool IJsonWrapper.IsObject
  {
    get
    {
      return this.IsObject;
    }
  }

  bool IJsonWrapper.IsString
  {
    get
    {
      return this.IsString;
    }
  }

  bool IList.IsFixedSize
  {
    get
    {
      return this.EnsureList().IsFixedSize;
    }
  }

  bool IList.IsReadOnly
  {
    get
    {
      return this.EnsureList().IsReadOnly;
    }
  }

  object IDictionary.this[object key]
  {
    get
    {
      return this.EnsureDictionary()[key];
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  object IOrderedDictionary.this[int idx]
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  object IList.this[int index]
  {
    get
    {
      return this.EnsureList()[index];
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public JsonData this[string prop_name]
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public JsonData this[int index]
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public static implicit operator JsonData(bool data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(double data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(int data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(long data)
  {
    return new JsonData(data);
  }

  public static implicit operator JsonData(string data)
  {
    return new JsonData(data);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator bool(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator double(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator int(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator long(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static explicit operator string(JsonData data)
  {
    // ISSUE: unable to decompile the method.
  }

  void ICollection.CopyTo(Array array, int index)
  {
    this.EnsureCollection().CopyTo(array, index);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IDictionary.Add(object key, object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IDictionary.Clear()
  {
    // ISSUE: unable to decompile the method.
  }

  bool IDictionary.Contains(object key)
  {
    return this.EnsureDictionary().Contains(key);
  }

  IDictionaryEnumerator IDictionary.GetEnumerator()
  {
    return ((IOrderedDictionary) this).GetEnumerator();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IDictionary.Remove(object key)
  {
    // ISSUE: unable to decompile the method.
  }

  IEnumerator IEnumerable.GetEnumerator()
  {
    return this.EnsureCollection().GetEnumerator();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  bool IJsonWrapper.GetBoolean()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  double IJsonWrapper.GetDouble()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  int IJsonWrapper.GetInt()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  long IJsonWrapper.GetLong()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  string IJsonWrapper.GetString()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetBoolean(bool val)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetDouble(double val)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetInt(int val)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetLong(long val)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IJsonWrapper.SetString(string val)
  {
    // ISSUE: unable to decompile the method.
  }

  string IJsonWrapper.ToJson()
  {
    return this.ToJson();
  }

  void IJsonWrapper.ToJson(JsonWriter writer)
  {
    this.ToJson(writer);
  }

  int IList.Add(object value)
  {
    return this.Add(value);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.Clear()
  {
    // ISSUE: unable to decompile the method.
  }

  bool IList.Contains(object value)
  {
    return this.EnsureList().Contains(value);
  }

  int IList.IndexOf(object value)
  {
    return this.EnsureList().IndexOf(value);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.Insert(int index, object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.Remove(object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IList.RemoveAt(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  IDictionaryEnumerator IOrderedDictionary.GetEnumerator()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IOrderedDictionary.Insert(int idx, object key, object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  void IOrderedDictionary.RemoveAt(int idx)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private ICollection EnsureCollection()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private IDictionary EnsureDictionary()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private IList EnsureList()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private JsonData ToJsonData(object obj)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void WriteJson(IJsonWrapper obj, JsonWriter writer)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int Add(object value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Clear()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool Equals(JsonData x)
  {
    // ISSUE: unable to decompile the method.
  }

  public JsonType GetJsonType()
  {
    return this.type;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetJsonType(JsonType type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string ToJson()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ToJson(JsonWriter writer)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override string ToString()
  {
    // ISSUE: unable to decompile the method.
  }
}
