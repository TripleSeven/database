﻿// Decompiled with JetBrains decompiler
// Type: OrderedDictionaryEnumerator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

internal class OrderedDictionaryEnumerator : IDictionaryEnumerator, IEnumerator
{
  private IEnumerator<KeyValuePair<string, JsonData>> list_enumerator;

  public OrderedDictionaryEnumerator(
    IEnumerator<KeyValuePair<string, JsonData>> enumerator)
  {
    this.list_enumerator = enumerator;
  }

  public object Current
  {
    get
    {
      return (object) this.Entry;
    }
  }

  public DictionaryEntry Entry
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public object Key
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public object Value
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool MoveNext()
  {
    return this.list_enumerator.MoveNext();
  }

  public void Reset()
  {
    this.list_enumerator.Reset();
  }
}
