﻿// Decompiled with JetBrains decompiler
// Type: WriterContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

internal class WriterContext
{
  public int Count;
  public bool InArray;
  public bool InObject;
  public bool ExpectingValue;
  public int Padding;
}
