﻿// Decompiled with JetBrains decompiler
// Type: ArgumentsParser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

public class ArgumentsParser
{
  private Dictionary<string, IArgumentMarshaler> _marshalers;
  private int _schemaElementNum;
  private const char ARG_SEPARATOR = ' ';

  [MethodImpl((MethodImplOptions) 32768)]
  public ArgumentsParser(string schema, string args)
  {
    // ISSUE: unable to decompile the method.
  }

  public int SchemaElementNum
  {
    get
    {
      return this._schemaElementNum;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private bool _ValidArgumentsNumber(string[] schemaElementArray, string[] argsArray)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ParseSchema(string[] schemaElementArray)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ParseSchemaElement(int index, string element)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ParseArguments(string[] argsArray)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public bool GetBoolean(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int GetInt32(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public long GetInt64(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public float GetFloat(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public double GetDouble(int index)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string GetString(int index)
  {
    // ISSUE: unable to decompile the method.
  }
}
