﻿// Decompiled with JetBrains decompiler
// Type: XILHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using Assets.XIL;
using BlackJack.BJFramework.Runtime.Hotfix;
using System.Reflection;
using System.Runtime.CompilerServices;

public static class XILHelper
{
  public static TDel CallBaseMethod<T, TDel>(this T obj, string methodName, ParamType[] types = null) where TDel : class
  {
    return HotfixManager.CallBaseMethod<T, TDel>(obj, methodName, types);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static TDel GetMethod<T, TDel>(this T obj, string methodName, ParamType[] types = null) where TDel : class
  {
    // ISSUE: unable to decompile the method.
  }

  public static MethodInfo GetMethod(
    this System.Type type,
    string methodName,
    ParamType[] types = null)
  {
    return HotfixManager.GetMethod(type, methodName, types);
  }
}
