﻿// Decompiled with JetBrains decompiler
// Type: SimpleGoogleTranslation.GoogleLanguageMap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SimpleGoogleTranslation
{
  public class GoogleLanguageMap
  {
    private static Dictionary<string, ConfigDataLanguageDataInfo> dicAbbrLangToData;
    private static Dictionary<string, ConfigDataLanguageDataInfo> dicSysLangToData;

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Dictionary<string, ConfigDataLanguageDataInfo> GetSysLangToDataDic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Dictionary<string, ConfigDataLanguageDataInfo> GetAbbrLangToDataDic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLanguageAbbrBySysLang(string sysLang)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLanguageNameByAbbrLang(string abbrLang)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataLanguageDataInfo GetLanguageDataInfoBySysLang(
      string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataLanguageDataInfo GetLanguageDataInfoByAbbrLang(
      string key)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
