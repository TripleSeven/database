﻿// Decompiled with JetBrains decompiler
// Type: SimpleGoogleTranslation.GoogleTranslationHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace SimpleGoogleTranslation
{
  public class GoogleTranslationHelper : MonoBehaviour
  {
    private static Dictionary<string, string> m_transCache = new Dictionary<string, string>();
    private static GoogleTranslationHelper s_instance;

    public static void Trans(string target, string source, Action<List<string>> callback)
    {
      GoogleTranslationHelper.Trans((string) null, target, source, callback);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Trans(
      string sourceLanguage,
      string target,
      string source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Trans(
      string sourceLanguage,
      string target,
      List<string> source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Translate(
      string sourceLanguage,
      string target,
      List<string> source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ProcessTrans(
      string sourceLanguage,
      string target,
      List<string> source,
      Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Detect(string source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Detect(List<string> source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DetectLanguage(List<string> source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ProcessDetect(List<string> source, Action<List<string>> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void DoCache(string source, string target, string result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetCache(string source, string target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetCacheKey(string key, string target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CheckInited()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
