﻿// Decompiled with JetBrains decompiler
// Type: EmojiLinkText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EmojiLinkText : EmojiText, IPointerClickHandler, IPointerUpHandler, IPointerDownHandler, IEventSystemHandler
{
  protected string m_ContentStr;
  protected string m_OutputText;
  protected readonly List<EmojiLinkText.LinkInfo> m_LinkInfos;
  protected static readonly StringBuilder s_TextBuilder;
  [SerializeField]
  private EmojiLinkText.LinkClickEvent m_OnLinkClick;
  [SerializeField]
  private EmojiLinkText.MeshPopulateEvent m_OnMeshPopulateEnd;
  private const string m_chatLinkMark = "<color=blue> <link type={1} click=true bgcolor=%00000000>{0}</link> </color>";
  private static readonly Regex s_LinkRegex;
  private static readonly Regex s_LinkRegexDetail;

  [MethodImpl((MethodImplOptions) 32768)]
  public EmojiLinkText()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetBattleHeroEquipmentContentText(bool isEmoji, string contentStr)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static string GetChatLink(string name, ulong iter)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected override void OnPopulateMesh(VertexHelper toFill)
  {
    // ISSUE: unable to decompile the method.
  }

  public override void SetVerticesDirty()
  {
    base.SetVerticesDirty();
    this.UpdateOutputText();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public virtual void OnPointerClick(PointerEventData eventData)
  {
    // ISSUE: unable to decompile the method.
  }

  public virtual void OnPointerUp(PointerEventData eventData)
  {
  }

  public void OnPointerDown(PointerEventData eventData)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static string ReplaceLink(string contentStr)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected void UpdateOutputText()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected virtual string GetOutputText(string outputText)
  {
    // ISSUE: unable to decompile the method.
  }

  public EmojiLinkText.LinkClickEvent onLinkClick
  {
    get
    {
      return this.m_OnLinkClick;
    }
    set
    {
      this.m_OnLinkClick = value;
    }
  }

  public EmojiLinkText.MeshPopulateEvent onMeshPopulateEnd
  {
    get
    {
      return this.m_OnMeshPopulateEnd;
    }
    set
    {
      this.m_OnMeshPopulateEnd = value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  static EmojiLinkText()
  {
    // ISSUE: unable to decompile the method.
  }

  [Serializable]
  public class MeshPopulateEvent : UnityEvent<List<EmojiLinkText.LinkInfo>>
  {
  }

  [Serializable]
  public class LinkClickEvent : UnityEvent<string>
  {
  }

  public class LinkInfo
  {
    public int startIndex = -1;
    public int endIndex = -1;
    public string type = string.Empty;
    public bool isCareClick = true;
    public Color bgImageColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    public readonly List<Rect> boxes = new List<Rect>();
  }
}
