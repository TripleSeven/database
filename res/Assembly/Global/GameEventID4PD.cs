﻿// Decompiled with JetBrains decompiler
// Type: GameEventID4PD
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public class GameEventID4PD
{
  public const string launchGame = "1";
  public const string showLoginUI = "3";
  public const string loginPDSucceed = "20";
  public const string updateClientBegin = "15";
  public const string updateClientFromServer1 = "16";
  public const string updateClientFromServer2 = "17";
  public const string updateClientAllFailed = "18";
  public const string updateAudioFilesFailed = "18.1";
  public const string updateClientFinished = "19";
  public const string confirmGameMessageBoard = "21";
  public const string clickOpenAnnounceButton = "22";
  public const string showServerList = "23";
  public const string selectServer = "24";
  public const string clickStartGameButton = "25";
  public const string showSelectCharactorUI = "25.5";
  public const string clickRandomCharName = "26";
  public const string clickCreateCharactorButton = "27";
  public const string showGoddessDialogUI = "28";
  public const string showGoddessGivingJobUI = "29";
  public const string clickAcceptJobButton = "30";
  public const string clickReselectJobButton = "31";
  public const string showWorldScene = "34";
  public const string battle_start = "40";
  public const string battle_arena_start = "41";
  public const string battle_team = "42";
  public const string battle_move = "43";
  public const string battle_attack = "44";
  public const string battle_skill = "45";
  public const string battle_standby = "46";
  public const string battle_die = "47";
  public const string battle_stop = "48";
  public const string startPreload = "50";
  public const string endPreload = "51";
  public const int completeUserGuideBaseNum = 1000;
}
