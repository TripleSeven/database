﻿// Decompiled with JetBrains decompiler
// Type: DebugCmdManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

public class DebugCmdManager
{
  private Dictionary<string, IDebugCmd> _cmdMap;
  private static DebugCmdManager _instance;
  private List<string> _oldCommands;
  private int _showOldCommandIdx;

  [MethodImpl((MethodImplOptions) 32768)]
  private DebugCmdManager()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugCmdManager Create(IConsoleMode debugConsoleMode)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void AddCmd(System.Type cmdType)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void LoadIDebugCmds(Assembly a)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _Init(IConsoleMode consoleMode)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void RunInstruct(string instruct)
  {
    // ISSUE: unable to decompile the method.
  }

  private void _PrintWrongCommandHint()
  {
    Debug.LogError("Wrong command!use \"help\" to show all command.");
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private string _GetCommandName(string instruct)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private string _GetCommandParams(string instruct)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private IDebugCmd _GetCommand(string cmdName)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void PringAllCmdDescription()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public int GetCommandNumber()
  {
    // ISSUE: unable to decompile the method.
  }

  public static DebugCmdManager instance
  {
    get
    {
      return DebugCmdManager._instance;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void SaveOldCommands()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void LoadOldCommands()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string GetNextOldCommand()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string GetPreviousOldCommand()
  {
    // ISSUE: unable to decompile the method.
  }
}
