﻿// Decompiled with JetBrains decompiler
// Type: EmojiText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

public class EmojiText : Text
{
  protected string m_content;
  private string m_emSpace;
  private string m_regexParamStr;
  private int m_richTextIndexOffSet;
  private float m_fontSize;
  private float m_offsetX;
  private float m_offsetY;
  private IConfigDataLoader m_configDataLoader;
  private Image m_imageToClone;
  private List<UIVertex> m_listUIVertex;

  [MethodImpl((MethodImplOptions) 32768)]
  public EmojiText()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Init(SmallExpressionParseDesc expressionDesc, Image image)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetContent(string content)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected override void OnPopulateMesh(VertexHelper toFill)
  {
    // ISSUE: unable to decompile the method.
  }

  [DebuggerHidden]
  [MethodImpl((MethodImplOptions) 32768)]
  private IEnumerator SetUITextWithSmallExpression(Text textToEdit, string inputString)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private List<EmojiText.PosStringTuple> ParseEmoji(
    string inputString,
    out string changedStr)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  protected string ReplaceEmoji(string inputString)
  {
    // ISSUE: unable to decompile the method.
  }

  public class PosStringTuple
  {
    public int pos;
    public int expressionKey;

    [MethodImpl((MethodImplOptions) 32768)]
    public PosStringTuple(int p, int key)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
