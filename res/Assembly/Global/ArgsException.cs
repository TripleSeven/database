﻿// Decompiled with JetBrains decompiler
// Type: ArgsException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

public class ArgsException : Exception
{
  private string _errorArgumentId;
  private string _errorParameter;
  private ArgsException.ErrorCode _errorCode;

  [MethodImpl((MethodImplOptions) 32768)]
  public ArgsException()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public ArgsException(string message)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public ArgsException(ArgsException.ErrorCode errorCode)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public ArgsException(ArgsException.ErrorCode errorCode, string errorParameter)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public ArgsException(
    ArgsException.ErrorCode errorCode,
    string errorArgumentId,
    string errorParameter)
  {
    // ISSUE: unable to decompile the method.
  }

  public string ErrorArgumentId
  {
    get
    {
      return this._errorArgumentId;
    }
    set
    {
      this._errorArgumentId = value;
    }
  }

  public string ErrorParameter
  {
    get
    {
      return this._errorParameter;
    }
    set
    {
      this._errorParameter = value;
    }
  }

  public ArgsException.ErrorCode GetErrorCode()
  {
    return this._errorCode;
  }

  public void SetErrorCode(ArgsException.ErrorCode errorCode)
  {
    this._errorCode = errorCode;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public string ErrorMessage()
  {
    // ISSUE: unable to decompile the method.
  }

  public enum ErrorCode
  {
    OK,
    INVALID_ARGUMENT_FORMAT,
    UNEXPECTED_ARGUMENT,
    INVALID_ARGUMENT_NAME,
    INVALID_ARGUMENTS_NUMBER,
    INVALID_BOOLEAN,
    MISSING_BOOLEAN,
    OVERFLOW_INTEGER32,
    INVALID_INTEGER32,
    MISSING_INTEGER32,
    OVERFLOW_INTEGER64,
    INVALID_INTEGER64,
    MISSING_INTEGER64,
    OVERFLOW_FLOAT,
    INVALID_FLOAT,
    MISSING_FLOAT,
    OVERFLOW_DOUBLE,
    INVALID_DOUBLE,
    MISSING_DOUBLE,
  }
}
