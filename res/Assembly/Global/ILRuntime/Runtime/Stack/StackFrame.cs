﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Stack.StackFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;

namespace ILRuntime.Runtime.Stack
{
  internal struct StackFrame
  {
    public ILMethod Method;
    public unsafe StackObject* LocalVarPointer;
    public unsafe StackObject* BasePointer;
    public unsafe StackObject* ValueTypeBasePointer;
    public IntegerReference Address;
    public int ManagedStackBase;
  }
}
