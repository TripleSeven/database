﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Stack.RuntimeStack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Intepreter;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Stack
{
  internal class RuntimeStack : IDisposable
  {
    private ILIntepreter intepreter;
    private unsafe StackObject* pointer;
    private unsafe StackObject* endOfMemory;
    private unsafe StackObject* valueTypePtr;
    private IntPtr nativePointer;
    private IList<object> managedStack;
    private System.Collections.Generic.Stack<StackFrame> frames;
    public const int MAXIMAL_STACK_OBJECTS = 16384;

    [MethodImpl((MethodImplOptions) 32768)]
    public RuntimeStack(ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    public System.Collections.Generic.Stack<StackFrame> Frames
    {
      get
      {
        return this.frames;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~RuntimeStack()
    {
      // ISSUE: unable to decompile the method.
    }

    public unsafe StackObject* StackBase
    {
      get
      {
        return this.pointer;
      }
    }

    public unsafe StackObject* ValueTypeStackPointer
    {
      get
      {
        return this.valueTypePtr;
      }
    }

    public unsafe StackObject* ValueTypeStackBase
    {
      get
      {
        return this.endOfMemory - 1;
      }
    }

    public IList<object> ManagedStack
    {
      get
      {
        return this.managedStack;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetValueTypePointer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeFrame(ILMethod method, StackObject* esp, out StackFrame res)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushFrame(ref StackFrame frame)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StackObject* PopFrame(ref StackFrame frame, StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RelocateValueType(StackObject* src, ref StackObject* dst, ref int mStackBase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AllocValueType(StackObject* ptr, IType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeValueTypeObject(IType type, StackObject* ptr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearValueTypeObject(IType type, StackObject* ptr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeValueTypeObject(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CountValueTypeManaged(
      StackObject* esp,
      ref int start,
      ref int end,
      StackObject** endAddr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    private unsafe StackObject* Add(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a + (ulong) (sizeof (StackObject) * b));
    }
  }
}
