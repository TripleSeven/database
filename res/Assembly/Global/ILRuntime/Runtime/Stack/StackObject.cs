﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Stack.StackObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Stack
{
  public struct StackObject
  {
    public static StackObject Null;
    public ObjectTypes ObjectType;
    public int Value;
    public int ValueLow;

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator ==(StackObject a, StackObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator !=(StackObject a, StackObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object ToObject(StackObject* esp, ILRuntime.Runtime.Enviorment.AppDomain appdomain, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Initialized(
      ref StackObject esp,
      int idx,
      Type t,
      IType fieldType,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Initialized(StackObject* esp, IType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static StackObject()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
