﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.DebugService
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;

namespace ILRuntime.Runtime.Debugger
{
  public class DebugService
  {
    private BreakPointContext curBreakpoint;
    private DebuggerServer server;
    private ILRuntime.Runtime.Enviorment.AppDomain domain;
    private Dictionary<int, LinkedList<BreakpointInfo>> activeBreakpoints;
    private Dictionary<int, BreakpointInfo> breakpointMapping;
    private Queue<KeyValuePair<int, VariableReference>> pendingReferences;
    private Queue<KeyValuePair<int, VariableReference>> pendingEnuming;
    private Queue<KeyValuePair<int, KeyValuePair<VariableReference, VariableReference>>> pendingIndexing;
    private AutoResetEvent evt;
    public Action<string> OnBreakPoint;

    [MethodImpl((MethodImplOptions) 32768)]
    public DebugService(ILRuntime.Runtime.Enviorment.AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    public ILRuntime.Runtime.Enviorment.AppDomain AppDomain
    {
      get
      {
        return this.domain;
      }
    }

    public AutoResetEvent BlockEvent
    {
      get
      {
        return this.evt;
      }
    }

    public bool IsDebuggerAttached
    {
      get
      {
        return false;
      }
    }

    public void StartDebugService(int port)
    {
    }

    public void StopDebugService()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal bool Break(ILIntepreter intpreter, Exception ex = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetStackTrace(ILIntepreter intepreper)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetThisInfo(ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLocalVariableInfo(ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static SequencePoint FindSequencePoint(
      Instruction ins,
      IDictionary<Instruction, SequencePoint> seqMapping)
    {
      // ISSUE: unable to decompile the method.
    }

    private unsafe StackObject* Add(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a + (ulong) (sizeof (StackObject) * b));
    }

    private unsafe StackObject* Minus(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a - (ulong) (sizeof (StackObject) * b));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void NotifyModuleLoaded(string moduleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SetBreakPoint(int methodHash, int bpHash, int startLine)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void DeleteBreakpoint(int bpHash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void ExecuteThread(int threadHash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void StepThread(int threadHash, StepTypes type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void CheckShouldBreak(ILMethod method, ILIntepreter intp, int ip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSequenceValid(SequencePoint sp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBreak(ILIntepreter intp, int bpHash, bool isStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StackFrameInfo[] GetStackFrameInfo(ILIntepreter intp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal VariableInfo[] EnumChildren(int threadHashCode, VariableReference parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableInfo[] EnumArray(Array arr, ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableInfo[] EnumList(IList lst, ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableInfo[] EnumDictionary(IDictionary lst, ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string SafeToString(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object[] GetArray(ICollection lst)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableInfo[] EnumILTypeInstance(
      ILTypeInstance obj,
      ILIntepreter intepreter)
    {
      // ISSUE: unable to decompile the method.
    }

    private VariableInfo[] EnumCLRObject(object obj, ILIntepreter intepreter)
    {
      return this.EnumObject(obj, obj.GetType());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableInfo[] EnumObject(object obj, Type t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal VariableInfo ResolveIndexAccess(
      int threadHashCode,
      VariableReference body,
      VariableReference idx,
      out object res)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void ResolvePendingRequests()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal VariableInfo ResolveVariable(
      int threadHashCode,
      VariableReference variable,
      out object res)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableInfo ResolveMember(object obj, string name, out object res)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool GetValueExpandable(StackObject* esp, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void ThreadStarted(ILIntepreter intp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void ThreadEnded(ILIntepreter intp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Detach()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void DumpStack(StackObject* esp, RuntimeStack stack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetStackObjectText(
      StringBuilder sb,
      StackObject* esp,
      IList<object> mStack,
      StackObject* valueTypeEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void VisitValueTypeReference(StackObject* esp, HashSet<long> leak)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
