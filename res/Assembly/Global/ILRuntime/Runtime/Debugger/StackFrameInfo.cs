﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.StackFrameInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Runtime.Debugger
{
  public class StackFrameInfo
  {
    public string MethodName { get; set; }

    public string DocumentName { get; set; }

    public int StartLine { get; set; }

    public int StartColumn { get; set; }

    public int EndLine { get; set; }

    public int EndColumn { get; set; }

    public VariableInfo[] LocalVariables { get; set; }
  }
}
