﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.BreakPointContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Debugger
{
  internal class BreakPointContext
  {
    public ILIntepreter Interpreter { get; set; }

    public Exception Exception { get; set; }

    public string DumpContext()
    {
      return (string) null;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetStackObjectValue(StackObject val, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
