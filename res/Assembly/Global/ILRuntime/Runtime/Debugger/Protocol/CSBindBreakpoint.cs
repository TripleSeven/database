﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.Protocol.CSBindBreakpoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Runtime.Debugger.Protocol
{
  public class CSBindBreakpoint
  {
    public int BreakpointHashCode { get; set; }

    public bool IsLambda { get; set; }

    public string TypeName { get; set; }

    public string MethodName { get; set; }

    public int StartLine { get; set; }

    public int EndLine { get; set; }
  }
}
