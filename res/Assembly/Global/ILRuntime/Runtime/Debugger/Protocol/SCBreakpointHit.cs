﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.Protocol.SCBreakpointHit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace ILRuntime.Runtime.Debugger.Protocol
{
  public class SCBreakpointHit
  {
    public int BreakpointHashCode { get; set; }

    public int ThreadHashCode { get; set; }

    public KeyValuePair<int, StackFrameInfo[]>[] StackFrame { get; set; }
  }
}
