﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.DebugSocket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Debugger
{
  public class DebugSocket
  {
    private Socket _socket;
    private bool _ready;
    private bool connectFailed;
    private const int MAX_BUFF_SIZE = 262144;
    private const int HEAD_SIZE = 8;
    private byte[] _headBuffer;
    private byte[] _sendBuffer;
    private MemoryStream _sendStream;
    private BinaryWriter bw;
    private const int RECV_BUFFER_SIZE = 1024;
    private MemoryStream recvBuffer;
    private int lastMsgLength;
    private byte[] socketAsyncBuffer;
    private SocketAsyncEventArgs saeArgs;
    private object socketLockObj;
    private byte[] _sendHeaderBuffer;

    [MethodImpl((MethodImplOptions) 32768)]
    public DebugSocket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DebugSocket(Socket _socket)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Disconnected
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Action OnConnect { get; set; }

    public Action OnConnectFailed { get; set; }

    public Action OnClose { get; set; }

    public Action<DebugMessageType, byte[]> OnReciveMessage { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Connect(string ip, int port)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AsyncRecv_Completed(object sender, SocketAsyncEventArgs e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReceivePayload(byte[] data, int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onConnected(IAsyncResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BeginReceive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Send(DebugMessageType type, byte[] buffer, int len)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RawSend(Socket sock, byte[] buf, int end)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
