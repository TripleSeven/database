﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.VariableReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Debugger
{
  public class VariableReference
  {
    public static VariableReference Null;
    public static VariableReference True;
    public static VariableReference False;

    public long Address { get; set; }

    public VariableTypes Type { get; set; }

    public int Offset { get; set; }

    public string Name { get; set; }

    public VariableReference Parent { get; set; }

    public VariableReference[] Parameters { get; set; }

    public string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableReference GetInteger(int val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableReference GetString(string val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableReference GetMember(
      string name,
      VariableReference parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static VariableReference()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
