﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.DebuggerServer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Debugger.Protocol;
using ILRuntime.Runtime.Enviorment;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Threading;

namespace ILRuntime.Runtime.Debugger
{
  public class DebuggerServer
  {
    public const int Version = 2;
    private TcpListener listener;
    private bool isUp;
    private int maxNewConnections;
    private int port;
    private Thread mainLoop;
    private DebugSocket clientSocket;
    private MemoryStream sendStream;
    private BinaryWriter bw;
    private DebugService ds;

    [MethodImpl((MethodImplOptions) 32768)]
    public DebuggerServer(DebugService ds)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Port
    {
      get
      {
        return this.port;
      }
      set
      {
        this.port = value;
      }
    }

    public DebugSocket Client
    {
      get
      {
        return this.clientSocket;
      }
    }

    public bool IsAttached
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NetworkLoop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateNewSession(TcpListener listener)
    {
      // ISSUE: unable to decompile the method.
    }

    private void ClientConnected()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReceive(DebugMessageType type, byte[] buffer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableReference ReadVariableReference(BinaryReader br)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendAttachResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoSend(DebugMessageType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckCompilerGeneratedStateMachine(
      ILMethod ilm,
      AppDomain domain,
      int startLine,
      out ILMethod found)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryBindBreakpoint(CSBindBreakpoint msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendSCBindBreakpointResult(SCBindBreakpointResult msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SendSCBreakpointHit(
      int intpHash,
      int bpHash,
      KeyValuePair<int, StackFrameInfo[]>[] info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SendSCStepComplete(int intpHash, KeyValuePair<int, StackFrameInfo[]>[] info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SendSCResolveVariableResult(VariableInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SendSCEnumChildrenResult(VariableInfo[] info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WriteStackFrames(KeyValuePair<int, StackFrameInfo[]>[] info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WriteVariableInfo(VariableInfo k)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SendSCThreadStarted(int threadHash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SendSCThreadEnded(int threadHash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyModuleLoaded(string modulename)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
