﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Debugger.VariableInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Debugger
{
  public class VariableInfo
  {
    public static VariableInfo NullReferenceExeption;
    public static VariableInfo RequestTimeout;
    public static VariableInfo Null;
    public static VariableInfo True;
    public static VariableInfo False;

    public long Address { get; set; }

    public VariableTypes Type { get; set; }

    public string Name { get; set; }

    public string TypeName { get; set; }

    public string Value { get; set; }

    public ValueTypes ValueType { get; set; }

    public bool Expandable { get; set; }

    public bool IsPrivate { get; set; }

    public bool IsProtected { get; set; }

    public int Offset { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableInfo FromObject(object obj, bool retriveType = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableInfo GetCannotFind(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableInfo GetInteger(int val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableInfo GetString(string val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableInfo GetException(Exception ex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static VariableInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
