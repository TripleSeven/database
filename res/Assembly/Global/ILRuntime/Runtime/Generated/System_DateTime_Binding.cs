﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Generated.System_DateTime_Binding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Generated
{
  internal class System_DateTime_Binding
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void WriteBackInstance(
      ILRuntime.Runtime.Enviorment.AppDomain __domain,
      StackObject* ptr_of_this_method,
      IList<object> __mStack,
      ref DateTime instance_of_this_method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Date_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Month_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Day_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_DayOfWeek_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_DayOfYear_4(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_TimeOfDay_5(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Hour_6(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Minute_7(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Second_8(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Millisecond_9(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Now_10(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Ticks_11(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Today_12(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_UtcNow_13(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Year_14(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Kind_15(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Add_16(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddDays_17(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddTicks_18(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddHours_19(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddMilliseconds_20(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddMinutes_21(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddMonths_22(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddSeconds_23(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* AddYears_24(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Compare_25(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CompareTo_26(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* IsDaylightSavingTime_27(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CompareTo_28(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Equals_29(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToBinary_30(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromBinary_31(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* SpecifyKind_32(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* DaysInMonth_33(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Equals_34(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Equals_35(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromFileTime_36(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromFileTimeUtc_37(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromOADate_38(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetDateTimeFormats_39(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetDateTimeFormats_40(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetDateTimeFormats_41(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetDateTimeFormats_42(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetHashCode_43(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetTypeCode_44(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* IsLeapYear_45(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Parse_46(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Parse_47(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Parse_48(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ParseExact_49(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ParseExact_50(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ParseExact_51(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* TryParse_52(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* TryParse_53(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* TryParseExact_54(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* TryParseExact_55(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Subtract_56(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Subtract_57(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToFileTime_58(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToFileTimeUtc_59(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToLongDateString_60(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToLongTimeString_61(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToOADate_62(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToShortDateString_63(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToShortTimeString_64(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToString_65(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToString_66(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToString_67(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToString_68(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToLocalTime_69(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToUniversalTime_70(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Addition_71(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Equality_72(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_GreaterThan_73(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_GreaterThanOrEqual_74(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Inequality_75(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_LessThan_76(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_LessThanOrEqual_77(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Subtraction_78(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Subtraction_79(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static object get_MaxValue_0(ref object o)
    {
      return (object) DateTime.MaxValue;
    }

    private static object get_MinValue_1(ref object o)
    {
      return (object) DateTime.MinValue;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object PerformMemberwiseClone(ref object o)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_4(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_5(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_6(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_7(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_8(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_9(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_10(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
