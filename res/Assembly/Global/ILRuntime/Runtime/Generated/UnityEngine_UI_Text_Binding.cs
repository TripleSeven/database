﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Generated.UnityEngine_UI_Text_Binding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Generated
{
  internal class UnityEngine_UI_Text_Binding
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void Register(AppDomain app)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_cachedTextGenerator_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_cachedTextGeneratorForLayout_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_mainTexture_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FontTextureChanged_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_font_4(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_font_5(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_text_6(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_text_7(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_supportRichText_8(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_supportRichText_9(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_resizeTextForBestFit_10(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_resizeTextForBestFit_11(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_resizeTextMinSize_12(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_resizeTextMinSize_13(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_resizeTextMaxSize_14(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_resizeTextMaxSize_15(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_alignment_16(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_alignment_17(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_alignByGeometry_18(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_alignByGeometry_19(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_fontSize_20(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_fontSize_21(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_horizontalOverflow_22(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_horizontalOverflow_23(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_verticalOverflow_24(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_verticalOverflow_25(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_lineSpacing_26(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_lineSpacing_27(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_fontStyle_28(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_fontStyle_29(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_pixelsPerUnit_30(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetGenerationSettings_31(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetTextAnchorPivot_32(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CalculateLayoutInputHorizontal_33(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CalculateLayoutInputVertical_34(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_minWidth_35(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_preferredWidth_36(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_flexibleWidth_37(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_minHeight_38(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_preferredHeight_39(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_flexibleHeight_40(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_layoutPriority_41(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
