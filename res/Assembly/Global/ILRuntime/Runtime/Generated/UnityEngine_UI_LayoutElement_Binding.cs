﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Generated.UnityEngine_UI_LayoutElement_Binding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Generated
{
  internal class UnityEngine_UI_LayoutElement_Binding
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void Register(AppDomain app)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_ignoreLayout_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_ignoreLayout_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CalculateLayoutInputHorizontal_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CalculateLayoutInputVertical_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_minWidth_4(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_minWidth_5(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_minHeight_6(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_minHeight_7(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_preferredWidth_8(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_preferredWidth_9(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_preferredHeight_10(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_preferredHeight_11(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_flexibleWidth_12(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_flexibleWidth_13(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_flexibleHeight_14(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_flexibleHeight_15(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_layoutPriority_16(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_layoutPriority_17(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
