﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Generated.System_TimeSpan_Binding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Generated
{
  internal class System_TimeSpan_Binding
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void WriteBackInstance(
      ILRuntime.Runtime.Enviorment.AppDomain __domain,
      StackObject* ptr_of_this_method,
      IList<object> __mStack,
      ref TimeSpan instance_of_this_method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Days_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Hours_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Milliseconds_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Minutes_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Seconds_4(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_Ticks_5(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_TotalDays_6(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_TotalHours_7(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_TotalMilliseconds_8(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_TotalMinutes_9(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_TotalSeconds_10(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Add_11(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Compare_12(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CompareTo_13(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* CompareTo_14(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Equals_15(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Duration_16(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Equals_17(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Equals_18(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromDays_19(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromHours_20(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromMinutes_21(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromSeconds_22(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromMilliseconds_23(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* FromTicks_24(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* GetHashCode_25(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Negate_26(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Parse_27(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* TryParse_28(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Subtract_29(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* ToString_30(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Addition_31(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Equality_32(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_GreaterThan_33(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_GreaterThanOrEqual_34(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Inequality_35(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_LessThan_36(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_LessThanOrEqual_37(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_Subtraction_38(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* op_UnaryNegation_39(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    private static object get_TicksPerDay_0(ref object o)
    {
      return (object) 864000000000L;
    }

    private static object get_TicksPerHour_1(ref object o)
    {
      return (object) 36000000000L;
    }

    private static object get_TicksPerMillisecond_2(ref object o)
    {
      return (object) 10000L;
    }

    private static object get_TicksPerMinute_3(ref object o)
    {
      return (object) 600000000L;
    }

    private static object get_TicksPerSecond_4(ref object o)
    {
      return (object) 10000000L;
    }

    private static object get_MaxValue_5(ref object o)
    {
      return (object) TimeSpan.MaxValue;
    }

    private static object get_MinValue_6(ref object o)
    {
      return (object) TimeSpan.MinValue;
    }

    private static object get_Zero_7(ref object o)
    {
      return (object) TimeSpan.Zero;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object PerformMemberwiseClone(ref object o)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
