﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Generated.UnityEngine_Time_Binding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Generated
{
  internal class UnityEngine_Time_Binding
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void Register(AppDomain app)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_time_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_timeSinceLevelLoad_1(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_deltaTime_2(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_fixedTime_3(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_unscaledTime_4(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_fixedUnscaledTime_5(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_unscaledDeltaTime_6(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_fixedUnscaledDeltaTime_7(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_fixedDeltaTime_8(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_fixedDeltaTime_9(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_maximumDeltaTime_10(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_maximumDeltaTime_11(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_smoothDeltaTime_12(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_maximumParticleDeltaTime_13(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_maximumParticleDeltaTime_14(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_timeScale_15(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_timeScale_16(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_frameCount_17(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_renderedFrameCount_18(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_realtimeSinceStartup_19(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_captureFramerate_20(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* set_captureFramerate_21(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* get_inFixedTimeStep_22(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static StackObject* Ctor_0(
      ILIntepreter __intp,
      StackObject* __esp,
      IList<object> __mStack,
      CLRMethod __method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
