﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Extensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime
{
  public static class Extensions
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetClassName(
      this Type type,
      out string clsName,
      out string realClsName,
      out bool isByRef,
      bool simpleClassName = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ToInt32(this object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static long ToInt64(this object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static short ToInt16(this object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float ToFloat(this object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static double ToDouble(this object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Type GetActualType(this object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MatchGenericParameters(
      this MethodInfo m,
      Type[] genericArguments,
      Type returnType,
      params Type[] parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MatchGenericParameters(
      this Type[] args,
      Type type,
      Type q,
      Type[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
