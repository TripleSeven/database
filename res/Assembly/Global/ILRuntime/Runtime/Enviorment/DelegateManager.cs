﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.DelegateManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Intepreter;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public class DelegateManager
  {
    private List<DelegateManager.DelegateMapNode> methods;
    private List<DelegateManager.DelegateMapNode> functions;
    private IDelegateAdapter zeroParamMethodAdapter;
    private IDelegateAdapter dummyAdapter;
    private Dictionary<Type, Func<Delegate, Delegate>> clrDelegates;
    private Func<Delegate, Delegate> defaultConverter;
    private AppDomain appdomain;

    [MethodImpl((MethodImplOptions) 32768)]
    public DelegateManager(AppDomain appdomain)
    {
      // ISSUE: unable to decompile the method.
    }

    private static Delegate DefaultConverterStub(Delegate dele)
    {
      return dele;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterDelegateConvertor<T>(Func<Delegate, Delegate> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterMethodDelegate<T1>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterMethodDelegate<T1, T2>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterMethodDelegate<T1, T2, T3>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterMethodDelegate<T1, T2, T3, T4>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterFunctionDelegate<TResult>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterFunctionDelegate<T1, TResult>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterFunctionDelegate<T1, T2, TResult>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterFunctionDelegate<T1, T2, T3, TResult>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterFunctionDelegate<T1, T2, T3, T4, TResult>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal Delegate ConvertToDelegate(Type clrDelegateType, IDelegateAdapter adapter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal IDelegateAdapter FindDelegateAdapter(
      ILTypeInstance instance,
      ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    private class DelegateMapNode
    {
      public IDelegateAdapter Adapter { get; set; }

      public Type[] ParameterTypes { get; set; }
    }
  }
}
