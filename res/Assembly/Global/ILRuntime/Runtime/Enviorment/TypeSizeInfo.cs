﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.TypeSizeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;

namespace ILRuntime.Runtime.Enviorment
{
  public struct TypeSizeInfo
  {
    public ILType Type;
    public int StaticFieldSize;
    public int MethodBodySize;
    public int TotalSize;
  }
}
