﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.CrossBindingAdaptor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Intepreter;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public abstract class CrossBindingAdaptor : IType
  {
    private IType type;

    public abstract Type BaseCLRType { get; }

    public virtual Type[] BaseCLRTypes
    {
      get
      {
        return (Type[]) null;
      }
    }

    public abstract Type AdaptorType { get; }

    public abstract object CreateCLRInstance(AppDomain appdomain, ILTypeInstance instance);

    internal IType RuntimeType
    {
      get
      {
        return this.type;
      }
      set
      {
        this.type = value;
      }
    }

    public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false)
    {
      return this.type.GetMethod(name, paramCount, declaredOnly);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetMethod(
      string name,
      List<IType> param,
      IType[] genericArguments,
      IType returnType = null,
      bool declaredOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<IMethod> GetMethods()
    {
      return this.type.GetMethods();
    }

    public int GetFieldIndex(object token)
    {
      return this.type.GetFieldIndex(token);
    }

    public IMethod GetConstructor(List<IType> param)
    {
      return this.type.GetConstructor(param);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAssignTo(IType type)
    {
      // ISSUE: unable to decompile the method.
    }

    public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments)
    {
      return this.type.MakeGenericInstance(genericArguments);
    }

    public IType MakeByRefType()
    {
      return this.type.MakeByRefType();
    }

    public IType MakeArrayType(int rank)
    {
      return this.type.MakeArrayType(rank);
    }

    public IType FindGenericArgument(string key)
    {
      return this.type.FindGenericArgument(key);
    }

    public IType ResolveGenericType(IType contextType)
    {
      return this.type.ResolveGenericType(contextType);
    }

    public IMethod GetVirtualMethod(IMethod method)
    {
      return this.type.GetVirtualMethod(method);
    }

    public bool IsGenericInstance
    {
      get
      {
        return this.type.IsGenericInstance;
      }
    }

    public KeyValuePair<string, IType>[] GenericArguments
    {
      get
      {
        return this.type.GenericArguments;
      }
    }

    public Type TypeForCLR
    {
      get
      {
        return this.type.TypeForCLR;
      }
    }

    public IType ByRefType
    {
      get
      {
        return this.type.ByRefType;
      }
    }

    public IType ArrayType
    {
      get
      {
        return this.type.ArrayType;
      }
    }

    public string FullName
    {
      get
      {
        return this.type.FullName;
      }
    }

    public string Name
    {
      get
      {
        return this.type.Name;
      }
    }

    public bool IsValueType
    {
      get
      {
        return this.type.IsValueType;
      }
    }

    public bool IsPrimitive
    {
      get
      {
        return this.type.IsPrimitive;
      }
    }

    public bool IsEnum
    {
      get
      {
        return this.type.IsEnum;
      }
    }

    public bool IsDelegate
    {
      get
      {
        return this.type.IsDelegate;
      }
    }

    public AppDomain AppDomain
    {
      get
      {
        return this.type.AppDomain;
      }
    }

    public Type ReflectionType
    {
      get
      {
        return this.type.ReflectionType;
      }
    }

    public IType BaseType
    {
      get
      {
        return this.type.BaseType;
      }
    }

    public IType[] Implements
    {
      get
      {
        return this.type.Implements;
      }
    }

    public bool HasGenericParameter
    {
      get
      {
        return this.type.HasGenericParameter;
      }
    }

    public bool IsGenericParameter
    {
      get
      {
        return this.type.IsGenericParameter;
      }
    }

    public bool IsArray
    {
      get
      {
        return false;
      }
    }

    public bool IsByRef
    {
      get
      {
        return this.type.IsByRef;
      }
    }

    public bool IsInterface
    {
      get
      {
        return this.type.IsInterface;
      }
    }

    public IType ElementType
    {
      get
      {
        return this.type.ElementType;
      }
    }

    public int ArrayRank
    {
      get
      {
        return this.type.ArrayRank;
      }
    }
  }
}
