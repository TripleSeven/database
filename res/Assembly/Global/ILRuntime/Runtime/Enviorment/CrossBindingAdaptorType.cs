﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.CrossBindingAdaptorType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Runtime.Intepreter;

namespace ILRuntime.Runtime.Enviorment
{
  public interface CrossBindingAdaptorType
  {
    ILTypeInstance ILInstance { get; }
  }
}
