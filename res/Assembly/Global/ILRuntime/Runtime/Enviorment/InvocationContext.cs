﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.InvocationContext
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public struct InvocationContext : IDisposable
  {
    private unsafe StackObject* esp;
    private AppDomain domain;
    private ILIntepreter intp;
    private ILMethod method;
    private IList<object> mStack;
    private bool invocated;
    private int paramCnt;
    private bool hasReturn;
    private static bool defaultConverterIntialized;

    [MethodImpl((MethodImplOptions) 32768)]
    internal InvocationContext(ILIntepreter intp, ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void InitializeDefaultConverters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushBool(bool val)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PushInteger<T>(T val)
    {
      this.PushInteger(PrimitiveConverter<T>.CheckAndInvokeToInteger(val));
    }

    public void PushLong<T>(T val)
    {
      this.PushInteger(PrimitiveConverter<T>.CheckAndInvokeToLong(val));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushInteger(int val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushInteger(long val)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PushFloat<T>(T val)
    {
      this.PushFloat(PrimitiveConverter<T>.CheckAndInvokeToFloat(val));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushFloat(float val)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PushDouble<T>(T val)
    {
      this.PushDouble(PrimitiveConverter<T>.CheckAndInvokeToDouble(val));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushDouble(double val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushObject(object obj, bool isBox = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Invoke()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckReturnValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadInteger()
    {
      // ISSUE: unable to decompile the method.
    }

    public T ReadInteger<T>()
    {
      return PrimitiveConverter<T>.CheckAndInvokeFromInteger(this.ReadInteger());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long ReadLong()
    {
      // ISSUE: unable to decompile the method.
    }

    public T ReadLong<T>()
    {
      return PrimitiveConverter<T>.CheckAndInvokeFromLong(this.ReadLong());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float ReadFloat()
    {
      // ISSUE: unable to decompile the method.
    }

    public T ReadFloat<T>()
    {
      return PrimitiveConverter<T>.CheckAndInvokeFromFloat(this.ReadFloat());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double ReadDouble()
    {
      // ISSUE: unable to decompile the method.
    }

    public T ReadDouble<T>()
    {
      return PrimitiveConverter<T>.CheckAndInvokeFromDouble(this.ReadDouble());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ReadBool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T ReadObject<T>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object ReadObject(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
