﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.PrimitiveConverter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public static class PrimitiveConverter<T>
  {
    public static Func<T, int> ToInteger;
    public static Func<int, T> FromInteger;
    public static Func<T, long> ToLong;
    public static Func<long, T> FromLong;
    public static Func<T, float> ToFloat;
    public static Func<float, T> FromFloat;
    public static Func<T, double> ToDouble;
    public static Func<double, T> FromDouble;

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CheckAndInvokeToInteger(T val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CheckAndInvokeFromInteger(int val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static long CheckAndInvokeToLong(T val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CheckAndInvokeFromLong(long val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float CheckAndInvokeToFloat(T val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CheckAndInvokeFromFloat(float val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static double CheckAndInvokeToDouble(T val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CheckAndInvokeFromDouble(double val)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
