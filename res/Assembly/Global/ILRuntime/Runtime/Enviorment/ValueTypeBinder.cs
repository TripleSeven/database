﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.ValueTypeBinder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public abstract class ValueTypeBinder
  {
    protected CLRType clrType;
    protected AppDomain domain;

    public CLRType CLRType
    {
      get
      {
        return this.clrType;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public abstract unsafe void CopyValueTypeToStack(
      object ins,
      StackObject* ptr,
      IList<object> mStack);

    public abstract unsafe object ToObject(StackObject* esp, IList<object> managedStack);

    public virtual void RegisterCLRRedirection(AppDomain appdomain)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CopyValueTypeToStack<K>(ref K ins, StackObject* esp, IList<object> mStack) where K : struct
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AssignFromStack<K>(ref K ins, StackObject* esp, IList<object> mStack) where K : struct
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
