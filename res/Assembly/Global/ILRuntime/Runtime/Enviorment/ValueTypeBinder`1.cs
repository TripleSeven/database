﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.ValueTypeBinder`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public abstract class ValueTypeBinder<T> : ValueTypeBinder where T : struct
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public override void CopyValueTypeToStack(object ins, StackObject* ptr, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract unsafe void CopyValueTypeToStack(
      ref T ins,
      StackObject* ptr,
      IList<object> mStack);

    [MethodImpl((MethodImplOptions) 32768)]
    public override object ToObject(StackObject* esp, IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract unsafe void AssignFromStack(ref T ins, StackObject* ptr, IList<object> mStack);

    [MethodImpl((MethodImplOptions) 32768)]
    public void ParseValue(
      ref T value,
      ILIntepreter intp,
      StackObject* ptr_of_this_method,
      IList<object> mStack,
      bool shouldFree = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteBackValue(
      AppDomain domain,
      StackObject* ptr_of_this_method,
      IList<object> mStack,
      ref T instance_of_this_method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PushValue(
      ref T value,
      ILIntepreter intp,
      StackObject* ptr_of_this_method,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
