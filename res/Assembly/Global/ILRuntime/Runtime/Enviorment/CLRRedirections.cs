﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.CLRRedirections
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  internal static class CLRRedirections
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* CreateInstance(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* CreateInstance2(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* CreateInstance3(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* GetType(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* TypeEquals(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* InitializeArray(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* DelegateCombine(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* DelegateRemove(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* DelegateEqulity(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* DelegateInequlity(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    public static unsafe StackObject* GetTypeFromHandle(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      return esp;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* MethodInfoInvoke(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object CheckCrossBindingAdapter(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* ObjectGetType(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* EnumParse(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* EnumGetValues(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* EnumGetNames(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* EnumGetName(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* EnumToObject(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack,
      CLRMethod method,
      bool isNewObj)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
