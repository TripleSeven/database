﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.AppDomain
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Mono.Cecil;
using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Other;
using ILRuntime.Runtime.Debugger;
using ILRuntime.Runtime.Intepreter;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Enviorment
{
  public class AppDomain
  {
    private Queue<ILIntepreter> freeIntepreters;
    private Dictionary<int, ILIntepreter> intepreters;
    private Dictionary<System.Type, CrossBindingAdaptor> crossAdaptors;
    private Dictionary<System.Type, ValueTypeBinder> valueTypeBinders;
    private ThreadSafeDictionary<string, IType> mapType;
    private Dictionary<System.Type, IType> clrTypeMapping;
    private ThreadSafeDictionary<int, IType> mapTypeToken;
    private ThreadSafeDictionary<int, IMethod> mapMethod;
    private ThreadSafeDictionary<long, string> mapString;
    private Dictionary<MethodBase, CLRRedirectionDelegate> redirectMap;
    private Dictionary<FieldInfo, CLRFieldGetterDelegate> fieldGetterMap;
    private Dictionary<FieldInfo, CLRFieldSetterDelegate> fieldSetterMap;
    private Dictionary<System.Type, CLRMemberwiseCloneDelegate> memberwiseCloneMap;
    private Dictionary<System.Type, CLRCreateDefaultInstanceDelegate> createDefaultInstanceMap;
    private Dictionary<System.Type, CLRCreateArrayInstanceDelegate> createArrayInstanceMap;
    private IType voidType;
    private IType intType;
    private IType longType;
    private IType boolType;
    private IType floatType;
    private IType doubleType;
    private IType objectType;
    private DelegateManager dMgr;
    private Assembly[] loadedAssemblies;
    private Dictionary<string, byte[]> references;
    private DebugService debugService;

    [MethodImpl((MethodImplOptions) 32768)]
    public AppDomain()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool AllowUnboundCLRMethod { get; set; }

    public IType VoidType
    {
      get
      {
        return this.voidType;
      }
    }

    public IType IntType
    {
      get
      {
        return this.intType;
      }
    }

    public IType LongType
    {
      get
      {
        return this.longType;
      }
    }

    public IType BoolType
    {
      get
      {
        return this.boolType;
      }
    }

    public IType FloatType
    {
      get
      {
        return this.floatType;
      }
    }

    public IType DoubleType
    {
      get
      {
        return this.doubleType;
      }
    }

    public IType ObjectType
    {
      get
      {
        return this.objectType;
      }
    }

    public Dictionary<string, IType> LoadedTypes
    {
      get
      {
        return this.mapType.InnerDictionary;
      }
    }

    internal Dictionary<MethodBase, CLRRedirectionDelegate> RedirectMap
    {
      get
      {
        return this.redirectMap;
      }
    }

    internal Dictionary<FieldInfo, CLRFieldGetterDelegate> FieldGetterMap
    {
      get
      {
        return this.fieldGetterMap;
      }
    }

    internal Dictionary<FieldInfo, CLRFieldSetterDelegate> FieldSetterMap
    {
      get
      {
        return this.fieldSetterMap;
      }
    }

    internal Dictionary<System.Type, CLRMemberwiseCloneDelegate> MemberwiseCloneMap
    {
      get
      {
        return this.memberwiseCloneMap;
      }
    }

    internal Dictionary<System.Type, CLRCreateDefaultInstanceDelegate> CreateDefaultInstanceMap
    {
      get
      {
        return this.createDefaultInstanceMap;
      }
    }

    internal Dictionary<System.Type, CLRCreateArrayInstanceDelegate> CreateArrayInstanceMap
    {
      get
      {
        return this.createArrayInstanceMap;
      }
    }

    internal Dictionary<System.Type, CrossBindingAdaptor> CrossBindingAdaptors
    {
      get
      {
        return this.crossAdaptors;
      }
    }

    internal Dictionary<System.Type, ValueTypeBinder> ValueTypeBinders
    {
      get
      {
        return this.valueTypeBinders;
      }
    }

    public DebugService DebugService
    {
      get
      {
        return this.debugService;
      }
    }

    internal Dictionary<int, ILIntepreter> Intepreters
    {
      get
      {
        return this.intepreters;
      }
    }

    internal Queue<ILIntepreter> FreeIntepreters
    {
      get
      {
        return this.freeIntepreters;
      }
    }

    public DelegateManager DelegateManager
    {
      get
      {
        return this.dMgr;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadAssemblyFile(string path)
    {
      // ISSUE: unable to decompile the method.
    }

    public void LoadAssembly(Stream stream)
    {
      this.LoadAssembly(stream, (Stream) null, (ISymbolReaderProvider) null);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadAssembly(Stream stream, Stream symbol, ISymbolReaderProvider symbolReader)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddReferenceBytes(string name, byte[] content)
    {
      this.references[name] = content;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCLRMethodRedirection(MethodBase mi, CLRRedirectionDelegate func)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCLRFieldGetter(FieldInfo f, CLRFieldGetterDelegate getter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCLRFieldSetter(FieldInfo f, CLRFieldSetterDelegate setter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCLRMemberwiseClone(System.Type t, CLRMemberwiseCloneDelegate memberwiseClone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCLRCreateDefaultInstance(
      System.Type t,
      CLRCreateDefaultInstanceDelegate createDefaultInstance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCLRCreateArrayInstance(System.Type t, CLRCreateArrayInstanceDelegate createArray)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterValueTypeBinder(System.Type t, ValueTypeBinder binder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType GetType(string fullname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void ParseGenericType(
      string fullname,
      out string baseType,
      out List<string> genericParams,
      out bool isArray)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetAssemblyName(IMetadataScope scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal IType GetType(object token, IType contextType, IMethod contextMethod)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType GetType(int hash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType GetType(System.Type t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Instantiate<T>(string type, object[] args = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ILTypeInstance Instantiate(string type, object[] args = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Prewarm(string type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(string type, string method, object instance, params object[] p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object InvokeGenericMethod(
      string type,
      string method,
      IType[] genericArguments,
      object instance,
      params object[] p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ILIntepreter RequestILIntepreter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void FreeILIntepreter(ILIntepreter inteptreter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(IMethod m, object instance, params object[] p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public InvocationContext BeginInvoke(IMethod m)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal IMethod GetMethod(
      object token,
      ILType contextType,
      ILMethod contextMethod,
      out bool invalidToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal IMethod GetMethod(int tokenHash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal long GetStaticFieldIndex(object token, IType contextType, IMethod contextMethod)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal long CacheString(object token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckStringCollision(long hashCode, string newStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal string GetString(long hashCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterCrossBindingAdaptor(CrossBindingAdaptor adaptor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSizeInMemory(out List<TypeSizeInfo> detail)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
