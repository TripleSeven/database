﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Enviorment.CLRRedirectionDelegate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;

namespace ILRuntime.Runtime.Enviorment
{
  public unsafe delegate StackObject* CLRRedirectionDelegate(
    ILIntepreter intp,
    StackObject* esp,
    IList<object> mStack,
    CLRMethod method,
    bool isNewObj);
}
