﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.DelegateAdapter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  internal abstract class DelegateAdapter : ILTypeInstance, IDelegateAdapter
  {
    protected ILMethod method;
    protected ILTypeInstance instance;
    protected ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private Dictionary<Type, Delegate> converters;
    private IDelegateAdapter next;
    protected bool isClone;

    protected DelegateAdapter()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected DelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract Delegate Delegate { get; }

    public IDelegateAdapter Next
    {
      get
      {
        return this.next;
      }
    }

    public ILTypeInstance Instance
    {
      get
      {
        return this.instance;
      }
    }

    public ILMethod Method
    {
      get
      {
        return this.method;
      }
    }

    public override bool IsValueType
    {
      get
      {
        return false;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected static InvocationTypes GetInvocationType<T>()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected static void PushParameter<T>(ref InvocationContext ctx, InvocationTypes type, T val)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected static T ReadResult<T>(ref InvocationContext ctx, InvocationTypes type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StackObject* ILInvoke(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StackObject* ILInvokeSub(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StackObject* ClearStack(
      ILIntepreter intp,
      StackObject* esp,
      StackObject* ebp,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract IDelegateAdapter Instantiate(
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      ILTypeInstance instance,
      ILMethod method);

    public abstract IDelegateAdapter Clone();

    public bool IsClone
    {
      get
      {
        return this.isClone;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Combine(IDelegateAdapter adapter)
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract void Combine(Delegate dele);

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Remove(IDelegateAdapter adapter)
    {
      // ISSUE: unable to decompile the method.
    }

    public abstract void Remove(Delegate dele);

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Equals(IDelegateAdapter adapter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool Equals(Delegate dele)
    {
      return this.Delegate == dele;
    }

    public override string ToString()
    {
      return this.method.ToString();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Delegate GetConvertor(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    private unsafe StackObject* Minus(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a - (ulong) (sizeof (StackObject) * b));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ThrowAdapterNotFound(IMethod method)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
