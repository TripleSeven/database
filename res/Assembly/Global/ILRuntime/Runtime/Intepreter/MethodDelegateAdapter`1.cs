﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.MethodDelegateAdapter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  internal class MethodDelegateAdapter<T1> : DelegateAdapter
  {
    private static InvocationTypes pType = DelegateAdapter.GetInvocationType<T1>();
    private Action<T1> action;

    public MethodDelegateAdapter()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    public override Delegate Delegate
    {
      get
      {
        return (Delegate) this.action;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InvokeILMethod(T1 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    public override IDelegateAdapter Instantiate(
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      ILTypeInstance instance,
      ILMethod method)
    {
      return (IDelegateAdapter) new MethodDelegateAdapter<T1>(appdomain, instance, method);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override IDelegateAdapter Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Combine(Delegate dele)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Remove(Delegate dele)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
