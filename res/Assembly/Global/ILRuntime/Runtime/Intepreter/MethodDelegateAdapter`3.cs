﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.MethodDelegateAdapter`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  internal class MethodDelegateAdapter<T1, T2, T3> : DelegateAdapter
  {
    private Action<T1, T2, T3> action;
    private static InvocationTypes[] pTypes;

    [MethodImpl((MethodImplOptions) 32768)]
    static MethodDelegateAdapter()
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodDelegateAdapter()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    public override Delegate Delegate
    {
      get
      {
        return (Delegate) this.action;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InvokeILMethod(T1 p1, T2 p2, T3 p3)
    {
      // ISSUE: unable to decompile the method.
    }

    public override IDelegateAdapter Instantiate(
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      ILTypeInstance instance,
      ILMethod method)
    {
      return (IDelegateAdapter) new MethodDelegateAdapter<T1, T2, T3>(appdomain, instance, method);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override IDelegateAdapter Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Combine(Delegate dele)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Remove(Delegate dele)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
