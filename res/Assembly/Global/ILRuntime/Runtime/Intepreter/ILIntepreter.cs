﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.ILIntepreter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Debugger;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  public class ILIntepreter
  {
    private ILRuntime.Runtime.Enviorment.AppDomain domain;
    private RuntimeStack stack;
    private object _lockObj;
    private bool allowUnboundCLRMethod;
    private unsafe StackObject* ValueTypeBasePointer;
    private bool mainthreadLock;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILIntepreter(ILRuntime.Runtime.Enviorment.AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    internal RuntimeStack Stack
    {
      get
      {
        return this.stack;
      }
    }

    public bool ShouldBreak { get; set; }

    public StepTypes CurrentStepType { get; set; }

    public unsafe StackObject* LastStepFrameBase { get; set; }

    public int LastStepInstructionIndex { get; set; }

    public ILRuntime.Runtime.Enviorment.AppDomain AppDomain
    {
      get
      {
        return this.domain;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Break()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Resume()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearDebugState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Run(ILMethod method, object instance, object[] p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal StackObject* Execute(
      ILMethod method,
      StackObject* esp,
      out bool unhandledException)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DumpStack(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloneStackValueType(StackObject* src, StackObject* dst, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanCastTo(StackObject* src, StackObject* dst)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CopyStackValueType(StackObject* src, StackObject* dst, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CopyValueTypeToStack(StackObject* dst, object ins, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CopyToValueTypeField(
      StackObject* obj,
      int idx,
      StackObject* val,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StLocSub(
      StackObject* esp,
      StackObject* v,
      StackObject* bp,
      int idx,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object RetriveObject(StackObject* esp, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RetriveInt32(StackObject* esp, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long RetriveInt64(StackObject* esp, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float RetriveFloat(StackObject* esp, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double RetriveDouble(StackObject* esp, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArraySetValue(Array arr, object obj, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreIntValueToArray(Array arr, StackObject* val, StackObject* idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ExceptionHandler GetCorrespondingExceptionHandler(
      ILMethod method,
      object obj,
      int addr,
      ExceptionHandlerType type,
      bool explicitMatch)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadFromFieldReference(
      object obj,
      int idx,
      StackObject* dst,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreValueToFieldReference(
      object obj,
      int idx,
      StackObject* val,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadFromArrayReference(
      object obj,
      int idx,
      StackObject* objRef,
      IType t,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadFromArrayReference(
      object obj,
      int idx,
      StackObject* objRef,
      Type nT,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreValueToArrayReference(
      StackObject* objRef,
      StackObject* val,
      IType t,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreValueToArrayReference(
      StackObject* objRef,
      StackObject* val,
      Type nT,
      IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckExceptionType(IType catchType, object exception, bool explicitMatch)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* ResolveReference(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* GetObjectAndResolveReference(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StackObject* PushParameters(IMethod method, StackObject* esp, object[] p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyToStack(StackObject* dst, StackObject* src, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static object CheckAndCloneValueType(object obj, ILRuntime.Runtime.Enviorment.AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* PushOne(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* PushZero(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* PushNull(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void UnboxObject(
      StackObject* esp,
      object obj,
      IList<object> mStack = null,
      ILRuntime.Runtime.Enviorment.AppDomain domain = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StackObject* PushObject(
      StackObject* esp,
      IList<object> mStack,
      object obj,
      bool isBox = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public static unsafe StackObject* Add(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a + (ulong) (sizeof (StackObject) * b));
    }

    public static unsafe StackObject* Minus(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a - (ulong) (sizeof (StackObject) * b));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Free(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeStackValueType(StackObject* esp)
    {
      // ISSUE: unable to decompile the method.
    }

    public unsafe void AllocValueType(StackObject* ptr, IType type)
    {
      this.stack.AllocValueType(ptr, type);
    }
  }
}
