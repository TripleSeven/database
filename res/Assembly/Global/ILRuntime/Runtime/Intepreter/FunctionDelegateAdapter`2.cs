﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.FunctionDelegateAdapter`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  internal class FunctionDelegateAdapter<T1, TResult> : DelegateAdapter
  {
    private Func<T1, TResult> action;
    private static InvocationTypes[] pTypes;

    [MethodImpl((MethodImplOptions) 32768)]
    static FunctionDelegateAdapter()
    {
      // ISSUE: unable to decompile the method.
    }

    public FunctionDelegateAdapter()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private FunctionDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    public override Delegate Delegate
    {
      get
      {
        return (Delegate) this.action;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TResult InvokeILMethod(T1 p1)
    {
      // ISSUE: unable to decompile the method.
    }

    public override IDelegateAdapter Instantiate(
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      ILTypeInstance instance,
      ILMethod method)
    {
      return (IDelegateAdapter) new FunctionDelegateAdapter<T1, TResult>(appdomain, instance, method);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override IDelegateAdapter Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Combine(Delegate dele)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Remove(Delegate dele)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
