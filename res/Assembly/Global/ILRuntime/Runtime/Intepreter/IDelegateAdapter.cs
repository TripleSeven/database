﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.IDelegateAdapter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;

namespace ILRuntime.Runtime.Intepreter
{
  internal interface IDelegateAdapter
  {
    Delegate Delegate { get; }

    IDelegateAdapter Next { get; }

    ILTypeInstance Instance { get; }

    ILMethod Method { get; }

    unsafe StackObject* ILInvoke(
      ILIntepreter intp,
      StackObject* esp,
      IList<object> mStack);

    IDelegateAdapter Instantiate(
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      ILTypeInstance instance,
      ILMethod method);

    bool IsClone { get; }

    IDelegateAdapter Clone();

    Delegate GetConvertor(Type type);

    void Combine(IDelegateAdapter adapter);

    void Combine(Delegate dele);

    void Remove(IDelegateAdapter adapter);

    void Remove(Delegate dele);

    bool Equals(IDelegateAdapter adapter);

    bool Equals(Delegate dele);
  }
}
