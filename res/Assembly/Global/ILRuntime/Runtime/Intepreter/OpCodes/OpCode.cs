﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.OpCodes.OpCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Runtime.Intepreter.OpCodes
{
  internal struct OpCode
  {
    public OpCodeEnum Code;
    public int TokenInteger;
    public long TokenLong;
  }
}
