﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.ILRuntimeException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  public class ILRuntimeException : Exception
  {
    private string stackTrace;
    private string thisInfo;
    private string localInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ILRuntimeException(
      string message,
      ILIntepreter intepreter,
      ILMethod method,
      Exception innerException = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public override string StackTrace
    {
      get
      {
        return this.stackTrace;
      }
    }

    public string ThisInfo
    {
      get
      {
        return this.thisInfo;
      }
    }

    public string LocalInfo
    {
      get
      {
        return this.localInfo;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
