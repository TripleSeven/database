﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.DummyDelegateAdapter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  internal class DummyDelegateAdapter : DelegateAdapter
  {
    public DummyDelegateAdapter()
    {
    }

    protected DummyDelegateAdapter(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance, ILMethod method)
      : base(appdomain, instance, method)
    {
    }

    public override Delegate Delegate
    {
      get
      {
        DelegateAdapter.ThrowAdapterNotFound((IMethod) this.method);
        return (Delegate) null;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InvokeILMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    public override IDelegateAdapter Instantiate(
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      ILTypeInstance instance,
      ILMethod method)
    {
      return (IDelegateAdapter) new DummyDelegateAdapter(appdomain, instance, method);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override IDelegateAdapter Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void Combine(Delegate dele)
    {
      DelegateAdapter.ThrowAdapterNotFound((IMethod) this.method);
    }

    public override void Remove(Delegate dele)
    {
      DelegateAdapter.ThrowAdapterNotFound((IMethod) this.method);
    }
  }
}
