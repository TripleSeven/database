﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Intepreter.ILTypeInstance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Stack;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Intepreter
{
  public class ILTypeInstance
  {
    protected ILType type;
    protected StackObject[] fields;
    protected IList<object> managedObjs;
    private object clrInstance;
    private Dictionary<ILMethod, IDelegateAdapter> delegates;
    private const int SizeOfILTypeInstance = 21;

    protected ILTypeInstance()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ILTypeInstance(ILType type, bool initializeCLRInstance = true)
    {
      // ISSUE: unable to decompile the method.
    }

    public ILType Type
    {
      get
      {
        return this.type;
      }
    }

    public StackObject[] Fields
    {
      get
      {
        return this.fields;
      }
    }

    public virtual bool IsValueType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Boxed { get; set; }

    public IList<object> ManagedObjects
    {
      get
      {
        return this.managedObjs;
      }
    }

    public object CLRInstance
    {
      get
      {
        return this.clrInstance;
      }
      set
      {
        this.clrInstance = value;
      }
    }

    public object this[int index]
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSizeInMemory(HashSet<object> traversedObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetSizeInMemory(object obj, HashSet<object> traversedObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFields(ILType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void PushFieldAddress(int fieldIdx, StackObject* esp, IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void PushToStack(
      int fieldIdx,
      StackObject* esp,
      AppDomain appdomain,
      IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushToStackSub(
      ref StackObject field,
      int fieldIdx,
      StackObject* esp,
      IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void CopyValueTypeToStack(StackObject* ptr, IList<object> mStack)
    {
      // ISSUE: unable to decompile the method.
    }

    internal void Clear()
    {
      this.InitializeFields(this.type);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void AssignFromStack(
      int fieldIdx,
      StackObject* esp,
      AppDomain appdomain,
      IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void AssignFromStack(
      StackObject* esp,
      AppDomain appdomain,
      IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AssignFromStackSub(
      ref StackObject field,
      int fieldIdx,
      StackObject* esp,
      IList<object> managedStack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool CanAssignTo(IType type)
    {
      return this.type.CanAssignTo(type);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ILTypeInstance Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal IDelegateAdapter GetDelegateAdapter(ILMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void SetDelegateAdapter(ILMethod method, IDelegateAdapter adapter)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
