﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Runtime.Adaptors.AttributeAdaptor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Runtime.Adaptors
{
  internal class AttributeAdaptor : CrossBindingAdaptor
  {
    public override Type AdaptorType
    {
      get
      {
        return typeof (AttributeAdaptor.Adaptor);
      }
    }

    public override Type BaseCLRType
    {
      get
      {
        return typeof (Attribute);
      }
    }

    public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
    {
      return (object) new AttributeAdaptor.Adaptor(appdomain, instance);
    }

    private class Adaptor : Attribute, CrossBindingAdaptorType
    {
      private ILTypeInstance instance;
      private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
      private bool isToStringGot;
      private IMethod toString;

      [MethodImpl((MethodImplOptions) 32768)]
      public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
      {
        // ISSUE: unable to decompile the method.
      }

      public ILTypeInstance ILInstance
      {
        get
        {
          return this.instance;
        }
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public override string ToString()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
