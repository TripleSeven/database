﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Other.UncheckedList`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace ILRuntime.Other
{
  [Serializable]
  public class UncheckedList<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable, IList, ICollection
  {
    private static readonly T[] _emptyArray = new T[0];
    private const int _defaultCapacity = 4;
    private T[] _items;
    private int _size;
    private int _version;
    [NonSerialized]
    private object _syncRoot;

    [MethodImpl((MethodImplOptions) 32768)]
    public UncheckedList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UncheckedList(int capacity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UncheckedList(IEnumerable<T> collection)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Capacity
    {
      get
      {
        return this._items.Length;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Count
    {
      get
      {
        return this._size;
      }
    }

    bool IList.IsFixedSize
    {
      get
      {
        return false;
      }
    }

    bool ICollection<T>.IsReadOnly
    {
      get
      {
        return false;
      }
    }

    bool IList.IsReadOnly
    {
      get
      {
        return false;
      }
    }

    bool ICollection.IsSynchronized
    {
      get
      {
        return false;
      }
    }

    object ICollection.SyncRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public T this[int index]
    {
      get
      {
        return this._items[index];
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsCompatibleObject(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    object IList.this[int index]
    {
      get
      {
        return (object) this[index];
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Add(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddWithResize(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    int IList.Add(object item)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddRange(IEnumerable<T> collection)
    {
      this.InsertRange(this._size, collection);
    }

    public ReadOnlyCollection<T> AsReadOnly()
    {
      return new ReadOnlyCollection<T>((IList<T>) this);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BinarySearch(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BinarySearch(T item, IComparer<T> comparer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Contains(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    bool IList.Contains(object item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UncheckedList<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CopyTo(T[] array)
    {
      this.CopyTo(array, 0);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void ICollection.CopyTo(Array array, int arrayIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyTo(int index, T[] array, int arrayIndex, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyTo(T[] array, int arrayIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnsureCapacity(int min)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Exists(Predicate<T> match)
    {
      return this.FindIndex(match) != -1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Find(Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UncheckedList<T> FindAll(Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    public int FindIndex(Predicate<T> match)
    {
      return this.FindIndex(0, this._size, match);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindIndex(int startIndex, Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindIndex(int startIndex, int count, Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T FindLast(Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindLastIndex(Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    public int FindLastIndex(int startIndex, Predicate<T> match)
    {
      return this.FindLastIndex(startIndex, startIndex + 1, match);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindLastIndex(int startIndex, int count, Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForEach(Action<T> action)
    {
      // ISSUE: unable to decompile the method.
    }

    public UncheckedList<T>.Enumerator GetEnumerator()
    {
      return new UncheckedList<T>.Enumerator(this);
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      return (IEnumerator<T>) new UncheckedList<T>.Enumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) new UncheckedList<T>.Enumerator(this);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UncheckedList<T> GetRange(int index, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IndexOf(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    int IList.IndexOf(object item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IndexOf(T item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public int IndexOf(T item, int index, int count)
    {
      return Array.IndexOf<T>(this._items, item, index, count);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Insert(int index, T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void IList.Insert(int index, object item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InsertRange(int index, IEnumerable<T> collection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LastIndexOf(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    public int LastIndexOf(T item, int index)
    {
      return this.LastIndexOf(item, index, index + 1);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LastIndexOf(T item, int index, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Remove(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void IList.Remove(object item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveAll(Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAt(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRange(int index, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Reverse()
    {
      this.Reverse(0, this.Count);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Reverse(int index, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Sort()
    {
      this.Sort(0, this.Count, (IComparer<T>) null);
    }

    public void Sort(IComparer<T> comparer)
    {
      this.Sort(0, this.Count, comparer);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Sort(int index, int count, IComparer<T> comparer)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Sort(Comparison<T> comparison)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T[] ToArray()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TrimExcess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TrueForAll(Predicate<T> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEnumerable(IEnumerable<T> enumerable)
    {
      // ISSUE: unable to decompile the method.
    }

    public struct Enumerator : IEnumerator<T>, IEnumerator, IDisposable
    {
      private UncheckedList<T> list;
      private int index;
      private int version;
      private T current;

      [MethodImpl((MethodImplOptions) 32768)]
      internal Enumerator(UncheckedList<T> list)
      {
        // ISSUE: unable to decompile the method.
      }

      public void Dispose()
      {
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool MoveNext()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private bool MoveNextRare()
      {
        // ISSUE: unable to decompile the method.
      }

      public T Current
      {
        get
        {
          return this.current;
        }
      }

      object IEnumerator.Current
      {
        get
        {
          return (object) this.Current;
        }
      }

      [MethodImpl((MethodImplOptions) 32768)]
      void IEnumerator.Reset()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
