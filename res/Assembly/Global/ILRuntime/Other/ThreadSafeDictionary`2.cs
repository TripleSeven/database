﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Other.ThreadSafeDictionary`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Other
{
  internal class ThreadSafeDictionary<K, V> : IDictionary<K, V>, ICollection<KeyValuePair<K, V>>, IEnumerable<KeyValuePair<K, V>>, IEnumerable
  {
    private Dictionary<K, V> dic;

    [MethodImpl((MethodImplOptions) 32768)]
    public ThreadSafeDictionary()
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<K, V> InnerDictionary
    {
      get
      {
        return this.dic;
      }
    }

    public V this[K key]
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Count
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsReadOnly
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ICollection<K> Keys
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public ICollection<V> Values
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Add(KeyValuePair<K, V> item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Add(K key, V value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Contains(KeyValuePair<K, V> item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ContainsKey(K key)
    {
      // ISSUE: unable to decompile the method.
    }

    public void CopyTo(KeyValuePair<K, V>[] array, int arrayIndex)
    {
      throw new NotImplementedException();
    }

    public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
    {
      throw new NotImplementedException();
    }

    public bool Remove(KeyValuePair<K, V> item)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Remove(K key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetValue(K key, out V value)
    {
      // ISSUE: unable to decompile the method.
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      throw new NotImplementedException();
    }
  }
}
