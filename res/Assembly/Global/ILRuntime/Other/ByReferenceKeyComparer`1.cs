﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Other.ByReferenceKeyComparer`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Other
{
  internal class ByReferenceKeyComparer<T> : IEqualityComparer<T>
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public bool Equals(T x, T y)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetHashCode(T obj)
    {
      return RuntimeHelpers.GetHashCode((object) obj);
    }
  }
}
