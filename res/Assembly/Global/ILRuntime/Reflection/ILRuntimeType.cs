﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Reflection.ILRuntimeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Intepreter;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Reflection
{
  public class ILRuntimeType : Type
  {
    private ILType type;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private object[] customAttributes;
    private Type[] attributeTypes;
    private ILRuntimeFieldInfo[] fields;
    private ILRuntimePropertyInfo[] properties;
    private ILRuntimeMethodInfo[] methods;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimeType(ILType t)
    {
      // ISSUE: unable to decompile the method.
    }

    public ILType ILType
    {
      get
      {
        return this.type;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeCustomAttribute()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeProperties()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override Type MakeGenericType(params Type[] typeArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFields()
    {
      // ISSUE: unable to decompile the method.
    }

    public override Assembly Assembly
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string AssemblyQualifiedName
    {
      get
      {
        return this.type.FullName;
      }
    }

    public override Type BaseType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string FullName
    {
      get
      {
        return this.type.FullName;
      }
    }

    public override Guid GUID
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public override Module Module
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public override string Name
    {
      get
      {
        return this.type.Name;
      }
    }

    public override string Namespace
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override Type UnderlyingSystemType
    {
      get
      {
        return typeof (ILTypeInstance);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsAssignableFrom(Type c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsInstanceOfType(object o)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override Type GetElementType()
    {
      // ISSUE: unable to decompile the method.
    }

    public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
    {
      throw new NotImplementedException();
    }

    public override EventInfo[] GetEvents(BindingFlags bindingAttr)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override FieldInfo GetField(string name, BindingFlags bindingAttr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override FieldInfo[] GetFields(BindingFlags bindingAttr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override Type GetInterface(string name, bool ignoreCase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override Type[] GetInterfaces()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
    {
      // ISSUE: unable to decompile the method.
    }

    public override Type GetNestedType(string name, BindingFlags bindingAttr)
    {
      throw new NotImplementedException();
    }

    public override Type[] GetNestedTypes(BindingFlags bindingAttr)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
    {
      // ISSUE: unable to decompile the method.
    }

    public override object InvokeMember(
      string name,
      BindingFlags invokeAttr,
      Binder binder,
      object target,
      object[] args,
      ParameterModifier[] modifiers,
      CultureInfo culture,
      string[] namedParameters)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsDefined(Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override TypeAttributes GetAttributeFlagsImpl()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override ConstructorInfo GetConstructorImpl(
      BindingFlags bindingAttr,
      Binder binder,
      CallingConventions callConvention,
      Type[] types,
      ParameterModifier[] modifiers)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override MethodInfo GetMethodImpl(
      string name,
      BindingFlags bindingAttr,
      Binder binder,
      CallingConventions callConvention,
      Type[] types,
      ParameterModifier[] modifiers)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override PropertyInfo GetPropertyImpl(
      string name,
      BindingFlags bindingAttr,
      Binder binder,
      Type returnType,
      Type[] types,
      ParameterModifier[] modifiers)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override bool HasElementTypeImpl()
    {
      return false;
    }

    protected override bool IsArrayImpl()
    {
      return this.type.IsArray;
    }

    protected override bool IsByRefImpl()
    {
      return false;
    }

    protected override bool IsCOMObjectImpl()
    {
      return false;
    }

    protected override bool IsPointerImpl()
    {
      return false;
    }

    protected override bool IsPrimitiveImpl()
    {
      return false;
    }

    public override int GetHashCode()
    {
      return this.type.GetHashCode();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object o)
    {
      // ISSUE: unable to decompile the method.
    }

    public override bool IsGenericType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override Type GetGenericTypeDefinition()
    {
      // ISSUE: unable to decompile the method.
    }

    public override bool IsGenericTypeDefinition
    {
      get
      {
        return this.type.HasGenericParameter;
      }
    }
  }
}
