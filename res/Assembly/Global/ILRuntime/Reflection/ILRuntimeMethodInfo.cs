﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Reflection.ILRuntimeMethodInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Mono.Cecil;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Reflection
{
  public class ILRuntimeMethodInfo : MethodInfo
  {
    private ILMethod method;
    private ILRuntimeParameterInfo[] parameters;
    private MethodDefinition definition;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private object[] customAttributes;
    private System.Type[] attributeTypes;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimeMethodInfo(ILMethod m)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeCustomAttribute()
    {
      // ISSUE: unable to decompile the method.
    }

    internal ILMethod ILMethod
    {
      get
      {
        return this.method;
      }
    }

    public override System.Reflection.MethodAttributes Attributes
    {
      get
      {
        return System.Reflection.MethodAttributes.Public;
      }
    }

    public override System.Type DeclaringType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override RuntimeMethodHandle MethodHandle
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public override string Name
    {
      get
      {
        return this.method.Name;
      }
    }

    public override System.Type ReflectedType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override System.Reflection.ICustomAttributeProvider ReturnTypeCustomAttributes
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public override MethodInfo GetBaseDefinition()
    {
      return (MethodInfo) this;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(System.Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    public override System.Reflection.MethodImplAttributes GetMethodImplementationFlags()
    {
      throw new NotImplementedException();
    }

    public override ParameterInfo[] GetParameters()
    {
      return (ParameterInfo[]) this.parameters;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object Invoke(
      object obj,
      BindingFlags invokeAttr,
      Binder binder,
      object[] parameters,
      CultureInfo culture)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsDefined(System.Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    public override System.Type ReturnType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
