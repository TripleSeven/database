﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Reflection.ILRuntimeFieldInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using ILRuntime.Mono.Cecil;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Reflection
{
  public class ILRuntimeFieldInfo : FieldInfo
  {
    private System.Reflection.FieldAttributes attr;
    private ILRuntimeType dType;
    private ILType ilType;
    private IType fieldType;
    private bool isStatic;
    private int fieldIdx;
    private string name;
    private FieldDefinition definition;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private object[] customAttributes;
    private System.Type[] attributeTypes;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimeFieldInfo(
      FieldDefinition def,
      ILRuntimeType declaredType,
      bool isStatic,
      int fieldIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimeFieldInfo(
      FieldDefinition def,
      ILRuntimeType declaredType,
      int fieldIdx,
      IType fieldType)
    {
      // ISSUE: unable to decompile the method.
    }

    public IType ILFieldType
    {
      get
      {
        return this.fieldType;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeCustomAttribute()
    {
      // ISSUE: unable to decompile the method.
    }

    public override System.Reflection.FieldAttributes Attributes
    {
      get
      {
        return this.attr;
      }
    }

    public override System.Type DeclaringType
    {
      get
      {
        return (System.Type) this.dType;
      }
    }

    public override RuntimeFieldHandle FieldHandle
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public override System.Type FieldType
    {
      get
      {
        return this.fieldType.ReflectionType;
      }
    }

    public override string Name
    {
      get
      {
        return this.name;
      }
    }

    public override System.Type ReflectedType
    {
      get
      {
        return this.fieldType.ReflectionType;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(System.Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object GetValue(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsDefined(System.Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetValue(
      object obj,
      object value,
      BindingFlags invokeAttr,
      Binder binder,
      CultureInfo culture)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
