﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Reflection.ILRuntimeConstructorInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Reflection
{
  public class ILRuntimeConstructorInfo : ConstructorInfo
  {
    private ILMethod method;
    private ILRuntimeParameterInfo[] parameters;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimeConstructorInfo(ILMethod m)
    {
      // ISSUE: unable to decompile the method.
    }

    internal ILMethod ILMethod
    {
      get
      {
        return this.method;
      }
    }

    public override MethodAttributes Attributes
    {
      get
      {
        return MethodAttributes.Public;
      }
    }

    public override Type DeclaringType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override RuntimeMethodHandle MethodHandle
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public override string Name
    {
      get
      {
        return this.method.Name;
      }
    }

    public override Type ReflectedType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override object[] GetCustomAttributes(bool inherit)
    {
      throw new NotImplementedException();
    }

    public override object[] GetCustomAttributes(Type attributeType, bool inherit)
    {
      throw new NotImplementedException();
    }

    public override MethodImplAttributes GetMethodImplementationFlags()
    {
      throw new NotImplementedException();
    }

    public override ParameterInfo[] GetParameters()
    {
      return (ParameterInfo[]) this.parameters;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object Invoke(
      object obj,
      BindingFlags invokeAttr,
      Binder binder,
      object[] parameters,
      CultureInfo culture)
    {
      // ISSUE: unable to decompile the method.
    }

    public override bool IsDefined(Type attributeType, bool inherit)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object Invoke(
      BindingFlags invokeAttr,
      Binder binder,
      object[] parameters,
      CultureInfo culture)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
