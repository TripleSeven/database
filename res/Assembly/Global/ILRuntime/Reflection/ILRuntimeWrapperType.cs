﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Reflection.ILRuntimeWrapperType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Reflection
{
  public class ILRuntimeWrapperType : Type
  {
    private CLRType type;
    private Type et;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimeWrapperType(CLRType t)
    {
      // ISSUE: unable to decompile the method.
    }

    public CLRType CLRType
    {
      get
      {
        return this.type;
      }
    }

    public Type RealType
    {
      get
      {
        return this.et;
      }
    }

    public override Guid GUID
    {
      get
      {
        return this.et.GUID;
      }
    }

    public override Module Module
    {
      get
      {
        return this.et.Module;
      }
    }

    public override Assembly Assembly
    {
      get
      {
        return this.et.Assembly;
      }
    }

    public override string FullName
    {
      get
      {
        return this.et.FullName;
      }
    }

    public override string Namespace
    {
      get
      {
        return this.et.Namespace;
      }
    }

    public override string AssemblyQualifiedName
    {
      get
      {
        return this.et.AssemblyQualifiedName;
      }
    }

    public override Type BaseType
    {
      get
      {
        return this.et.BaseType;
      }
    }

    public override Type UnderlyingSystemType
    {
      get
      {
        return this.et.UnderlyingSystemType;
      }
    }

    public override string Name
    {
      get
      {
        return this.et.Name;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object InvokeMember(
      string name,
      BindingFlags invokeAttr,
      Binder binder,
      object target,
      object[] args,
      ParameterModifier[] modifiers,
      CultureInfo culture,
      string[] namedParameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override ConstructorInfo GetConstructorImpl(
      BindingFlags bindingAttr,
      Binder binder,
      CallingConventions callConvention,
      Type[] types,
      ParameterModifier[] modifiers)
    {
      // ISSUE: unable to decompile the method.
    }

    public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
    {
      return this.et.GetConstructors(bindingAttr);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override MethodInfo GetMethodImpl(
      string name,
      BindingFlags bindingAttr,
      Binder binder,
      CallingConventions callConvention,
      Type[] types,
      ParameterModifier[] modifiers)
    {
      // ISSUE: unable to decompile the method.
    }

    public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
    {
      return this.et.GetMethods(bindingAttr);
    }

    public override FieldInfo GetField(string name, BindingFlags bindingAttr)
    {
      return this.et.GetField(name, bindingAttr);
    }

    public override FieldInfo[] GetFields(BindingFlags bindingAttr)
    {
      return this.et.GetFields(bindingAttr);
    }

    public override Type GetInterface(string name, bool ignoreCase)
    {
      return this.et.GetInterface(name, ignoreCase);
    }

    public override Type[] GetInterfaces()
    {
      return this.et.GetInterfaces();
    }

    public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
    {
      return this.et.GetEvent(name, bindingAttr);
    }

    public override EventInfo[] GetEvents(BindingFlags bindingAttr)
    {
      return this.et.GetEvents(bindingAttr);
    }

    protected override PropertyInfo GetPropertyImpl(
      string name,
      BindingFlags bindingAttr,
      Binder binder,
      Type returnType,
      Type[] types,
      ParameterModifier[] modifiers)
    {
      return this.et.GetProperty(name, bindingAttr);
    }

    public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
    {
      return this.et.GetProperties(bindingAttr);
    }

    public override Type[] GetNestedTypes(BindingFlags bindingAttr)
    {
      return this.et.GetNestedTypes(bindingAttr);
    }

    public override int GetHashCode()
    {
      return this.type.GetHashCode();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsAssignableFrom(Type c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsInstanceOfType(object o)
    {
      // ISSUE: unable to decompile the method.
    }

    public override Type GetNestedType(string name, BindingFlags bindingAttr)
    {
      return this.et.GetNestedType(name, bindingAttr);
    }

    public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
    {
      return this.et.GetMembers(bindingAttr);
    }

    protected override TypeAttributes GetAttributeFlagsImpl()
    {
      return this.et.Attributes;
    }

    protected override bool IsArrayImpl()
    {
      return this.et.IsArray;
    }

    protected override bool IsByRefImpl()
    {
      return this.et.IsByRef;
    }

    protected override bool IsPointerImpl()
    {
      return this.et.IsPointer;
    }

    protected override bool IsPrimitiveImpl()
    {
      return this.et.IsPrimitive;
    }

    protected override bool IsCOMObjectImpl()
    {
      return this.et.IsCOMObject;
    }

    public override bool IsGenericType
    {
      get
      {
        return this.et.IsGenericType;
      }
    }

    public override bool IsGenericTypeDefinition
    {
      get
      {
        return this.et.IsGenericTypeDefinition;
      }
    }

    public override Type GetGenericTypeDefinition()
    {
      return this.et.GetGenericTypeDefinition();
    }

    public override bool IsGenericParameter
    {
      get
      {
        return this.et.IsGenericParameter;
      }
    }

    public override Type GetElementType()
    {
      return this.et.GetElementType();
    }

    protected override bool HasElementTypeImpl()
    {
      return this.et.HasElementType;
    }

    public override object[] GetCustomAttributes(bool inherit)
    {
      return this.et.GetCustomAttributes(inherit);
    }

    public override object[] GetCustomAttributes(Type attributeType, bool inherit)
    {
      return this.et.GetCustomAttributes(attributeType, inherit);
    }

    public override bool IsDefined(Type attributeType, bool inherit)
    {
      return this.et.IsDefined(attributeType, inherit);
    }
  }
}
