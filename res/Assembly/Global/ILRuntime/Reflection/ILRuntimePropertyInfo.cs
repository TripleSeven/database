﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Reflection.ILRuntimePropertyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Mono.Cecil;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Reflection
{
  public class ILRuntimePropertyInfo : PropertyInfo
  {
    private ILMethod getter;
    private ILMethod setter;
    private ILType dType;
    private PropertyDefinition definition;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private object[] customAttributes;
    private System.Type[] attributeTypes;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILRuntimePropertyInfo(PropertyDefinition definition, ILType dType)
    {
      // ISSUE: unable to decompile the method.
    }

    public ILMethod Getter
    {
      get
      {
        return this.getter;
      }
      set
      {
        this.getter = value;
      }
    }

    public ILMethod Setter
    {
      get
      {
        return this.setter;
      }
      set
      {
        this.setter = value;
      }
    }

    public bool IsPublic
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsStatic
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeCustomAttribute()
    {
      // ISSUE: unable to decompile the method.
    }

    public override string Name
    {
      get
      {
        return this.definition.Name;
      }
    }

    public override System.Type ReflectedType
    {
      get
      {
        return this.dType.ReflectionType;
      }
    }

    public override System.Reflection.PropertyAttributes Attributes
    {
      get
      {
        return System.Reflection.PropertyAttributes.None;
      }
    }

    public override bool CanRead
    {
      get
      {
        return this.getter != null;
      }
    }

    public override bool CanWrite
    {
      get
      {
        return this.setter != null;
      }
    }

    public override System.Type PropertyType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override System.Type DeclaringType
    {
      get
      {
        return this.dType.ReflectionType;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object[] GetCustomAttributes(System.Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsDefined(System.Type attributeType, bool inherit)
    {
      // ISSUE: unable to decompile the method.
    }

    public override MethodInfo[] GetAccessors(bool nonPublic)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override MethodInfo GetGetMethod(bool nonPublic)
    {
      // ISSUE: unable to decompile the method.
    }

    public override ParameterInfo[] GetIndexParameters()
    {
      return new ParameterInfo[0];
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override MethodInfo GetSetMethod(bool nonPublic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object GetValue(
      object obj,
      BindingFlags invokeAttr,
      Binder binder,
      object[] index,
      CultureInfo culture)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void SetValue(
      object obj,
      object value,
      BindingFlags invokeAttr,
      Binder binder,
      object[] index,
      CultureInfo culture)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
