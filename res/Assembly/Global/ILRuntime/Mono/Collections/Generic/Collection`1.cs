﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Collections.Generic.Collection`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Collections.Generic
{
  public class Collection<T> : IList<T>, IList, ICollection<T>, IEnumerable<T>, IEnumerable, ICollection
  {
    internal T[] items;
    internal int size;
    private int version;

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection(int capacity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection(ICollection<T> items)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Count
    {
      get
      {
        return this.size;
      }
    }

    public T this[int index]
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Capacity
    {
      get
      {
        return this.items.Length;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    bool ICollection<T>.IsReadOnly
    {
      get
      {
        return false;
      }
    }

    bool IList.IsFixedSize
    {
      get
      {
        return false;
      }
    }

    bool IList.IsReadOnly
    {
      get
      {
        return false;
      }
    }

    object IList.this[int index]
    {
      get
      {
        return (object) this[index];
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    int ICollection.Count
    {
      get
      {
        return this.Count;
      }
    }

    bool ICollection.IsSynchronized
    {
      get
      {
        return false;
      }
    }

    object ICollection.SyncRoot
    {
      get
      {
        return (object) this;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Add(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool Contains(T item)
    {
      return this.IndexOf(item) != -1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IndexOf(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Insert(int index, T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAt(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Remove(T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyTo(T[] array, int arrayIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T[] ToArray()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Shift(int start, int delta)
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual void OnAdd(T item, int index)
    {
    }

    protected virtual void OnInsert(T item, int index)
    {
    }

    protected virtual void OnSet(T item, int index)
    {
    }

    protected virtual void OnRemove(T item, int index)
    {
    }

    protected virtual void OnClear()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal virtual void Grow(int desired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void Resize(int new_size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    int IList.Add(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    void IList.Clear()
    {
      this.Clear();
    }

    bool IList.Contains(object value)
    {
      return ((IList) this).IndexOf(value) > -1;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    int IList.IndexOf(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void IList.Insert(int index, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void IList.Remove(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    void IList.RemoveAt(int index)
    {
      this.RemoveAt(index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    void ICollection.CopyTo(Array array, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public Collection<T>.Enumerator GetEnumerator()
    {
      return new Collection<T>.Enumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) new Collection<T>.Enumerator(this);
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      return (IEnumerator<T>) new Collection<T>.Enumerator(this);
    }

    public struct Enumerator : IEnumerator<T>, IDisposable, IEnumerator
    {
      private Collection<T> collection;
      private T current;
      private int next;
      private readonly int version;

      [MethodImpl((MethodImplOptions) 32768)]
      internal Enumerator(Collection<T> collection)
      {
        // ISSUE: unable to decompile the method.
      }

      public T Current
      {
        get
        {
          return this.current;
        }
      }

      object IEnumerator.Current
      {
        [MethodImpl((MethodImplOptions) 32768)] get
        {
          // ISSUE: unable to decompile the method.
        }
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public bool MoveNext()
      {
        // ISSUE: unable to decompile the method.
      }

      public void Reset()
      {
        this.CheckState();
        this.next = 0;
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private void CheckState()
      {
        // ISSUE: unable to decompile the method.
      }

      public void Dispose()
      {
        this.collection = (Collection<T>) null;
      }
    }
  }
}
