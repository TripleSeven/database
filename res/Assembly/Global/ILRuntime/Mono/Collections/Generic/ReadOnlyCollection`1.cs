﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Collections.Generic.ReadOnlyCollection`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Collections.Generic
{
  public sealed class ReadOnlyCollection<T> : Collection<T>, ICollection<T>, IList, IEnumerable<T>, IEnumerable, ICollection
  {
    private static ReadOnlyCollection<T> empty;

    private ReadOnlyCollection()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ReadOnlyCollection(T[] array)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ReadOnlyCollection(Collection<T> collection)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ReadOnlyCollection<T> Empty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    bool ICollection<T>.IsReadOnly
    {
      get
      {
        return true;
      }
    }

    bool IList.IsFixedSize
    {
      get
      {
        return true;
      }
    }

    bool IList.IsReadOnly
    {
      get
      {
        return true;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Initialize(T[] items, int size)
    {
      // ISSUE: unable to decompile the method.
    }

    internal override void Grow(int desired)
    {
      throw new InvalidOperationException();
    }

    protected override void OnAdd(T item, int index)
    {
      throw new InvalidOperationException();
    }

    protected override void OnClear()
    {
      throw new InvalidOperationException();
    }

    protected override void OnInsert(T item, int index)
    {
      throw new InvalidOperationException();
    }

    protected override void OnRemove(T item, int index)
    {
      throw new InvalidOperationException();
    }

    protected override void OnSet(T item, int index)
    {
      throw new InvalidOperationException();
    }
  }
}
