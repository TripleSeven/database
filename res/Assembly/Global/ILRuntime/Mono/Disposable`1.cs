﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Disposable`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono
{
  internal struct Disposable<T> : IDisposable where T : class, IDisposable
  {
    internal readonly T value;
    private readonly bool owned;

    public Disposable(T value, bool owned)
    {
      this.value = value;
      this.owned = owned;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
