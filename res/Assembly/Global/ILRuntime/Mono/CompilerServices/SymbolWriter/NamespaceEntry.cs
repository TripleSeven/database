﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.NamespaceEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public struct NamespaceEntry
  {
    public readonly string Name;
    public readonly int Index;
    public readonly int Parent;
    public readonly string[] UsingClauses;

    [MethodImpl((MethodImplOptions) 32768)]
    public NamespaceEntry(string name, int index, string[] using_clauses, int parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal NamespaceEntry(MonoSymbolFile file, MyBinaryReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(MonoSymbolFile file, MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
