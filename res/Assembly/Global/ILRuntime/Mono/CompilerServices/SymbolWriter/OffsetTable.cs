﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.OffsetTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class OffsetTable
  {
    public const int MajorVersion = 50;
    public const int MinorVersion = 0;
    public const long Magic = 5037318119232611860;
    public int TotalFileSize;
    public int DataSectionOffset;
    public int DataSectionSize;
    public int CompileUnitCount;
    public int CompileUnitTableOffset;
    public int CompileUnitTableSize;
    public int SourceCount;
    public int SourceTableOffset;
    public int SourceTableSize;
    public int MethodCount;
    public int MethodTableOffset;
    public int MethodTableSize;
    public int TypeCount;
    public int AnonymousScopeCount;
    public int AnonymousScopeTableOffset;
    public int AnonymousScopeTableSize;
    public OffsetTable.Flags FileFlags;
    public int LineNumberTable_LineBase;
    public int LineNumberTable_LineRange;
    public int LineNumberTable_OpcodeBase;

    [MethodImpl((MethodImplOptions) 32768)]
    internal OffsetTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal OffsetTable(BinaryReader reader, int major_version, int minor_version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(BinaryWriter bw, int major_version, int minor_version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [System.Flags]
    public enum Flags
    {
      IsAspxSource = 1,
      WindowsFileNames = 2,
    }
  }
}
