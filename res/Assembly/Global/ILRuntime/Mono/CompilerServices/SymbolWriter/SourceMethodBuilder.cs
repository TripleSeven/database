﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.SourceMethodBuilder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class SourceMethodBuilder
  {
    private List<LocalVariableEntry> _locals;
    private List<CodeBlockEntry> _blocks;
    private List<ScopeVariable> _scope_vars;
    private Stack<CodeBlockEntry> _block_stack;
    private readonly List<LineNumberEntry> method_lines;
    private readonly ICompileUnit _comp_unit;
    private readonly int ns_id;
    private readonly IMethodDef method;

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceMethodBuilder(ICompileUnit comp_unit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceMethodBuilder(ICompileUnit comp_unit, int ns_id, IMethodDef method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MarkSequencePoint(
      int offset,
      SourceFileEntry file,
      int line,
      int column,
      bool is_hidden)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MarkSequencePoint(
      int offset,
      SourceFileEntry file,
      int line,
      int column,
      int end_line,
      int end_column,
      bool is_hidden)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBlock(CodeBlockEntry.Type type, int start_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBlock(CodeBlockEntry.Type type, int start_offset, int scopeIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EndBlock(int end_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    public CodeBlockEntry[] Blocks
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CodeBlockEntry CurrentBlock
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public LocalVariableEntry[] Locals
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ICompileUnit SourceFile
    {
      get
      {
        return this._comp_unit;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLocal(int index, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public ScopeVariable[] ScopeVariables
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddScopeVariable(int scope, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineMethod(MonoSymbolFile file)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineMethod(MonoSymbolFile file, int token)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
