﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.CompileUnitEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class CompileUnitEntry : ICompileUnit
  {
    public readonly int Index;
    private int DataOffset;
    private MonoSymbolFile file;
    private SourceFileEntry source;
    private List<SourceFileEntry> include_files;
    private List<NamespaceEntry> namespaces;
    private bool creating;

    [MethodImpl((MethodImplOptions) 32768)]
    public CompileUnitEntry(MonoSymbolFile file, SourceFileEntry source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal CompileUnitEntry(MonoSymbolFile file, MyBinaryReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int Size
    {
      get
      {
        return 8;
      }
    }

    CompileUnitEntry ICompileUnit.Entry
    {
      get
      {
        return this;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFile(SourceFileEntry file)
    {
      // ISSUE: unable to decompile the method.
    }

    public SourceFileEntry SourceFile
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int DefineNamespace(string name, string[] using_clauses, int parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void WriteData(MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(BinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ReadAll()
    {
      this.ReadData();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadData()
    {
      // ISSUE: unable to decompile the method.
    }

    public NamespaceEntry[] Namespaces
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public SourceFileEntry[] IncludeFiles
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
