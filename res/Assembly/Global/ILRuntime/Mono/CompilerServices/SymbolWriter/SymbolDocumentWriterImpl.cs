﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.SymbolDocumentWriterImpl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Diagnostics.SymbolStore;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  internal class SymbolDocumentWriterImpl : ISymbolDocumentWriter, ISourceFile, ICompileUnit
  {
    private CompileUnitEntry comp_unit;

    public SymbolDocumentWriterImpl(CompileUnitEntry comp_unit)
    {
      this.comp_unit = comp_unit;
    }

    public void SetCheckSum(Guid algorithmId, byte[] checkSum)
    {
    }

    public void SetSource(byte[] source)
    {
    }

    SourceFileEntry ISourceFile.Entry
    {
      get
      {
        return this.comp_unit.SourceFile;
      }
    }

    public CompileUnitEntry Entry
    {
      get
      {
        return this.comp_unit;
      }
    }
  }
}
