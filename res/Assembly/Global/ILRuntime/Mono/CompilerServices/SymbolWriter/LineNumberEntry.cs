﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.LineNumberEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class LineNumberEntry
  {
    public static readonly LineNumberEntry Null = new LineNumberEntry(0, 0, 0, 0);
    public readonly int Row;
    public int Column;
    public int EndRow;
    public int EndColumn;
    public readonly int File;
    public readonly int Offset;
    public readonly bool IsHidden;

    public LineNumberEntry(int file, int row, int column, int offset)
      : this(file, row, column, offset, false)
    {
    }

    public LineNumberEntry(int file, int row, int offset)
      : this(file, row, -1, offset, false)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LineNumberEntry(int file, int row, int column, int offset, bool is_hidden)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LineNumberEntry(
      int file,
      int row,
      int column,
      int end_row,
      int end_column,
      int offset,
      bool is_hidden)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    public sealed class LocationComparer : IComparer<LineNumberEntry>
    {
      public static readonly LineNumberEntry.LocationComparer Default = new LineNumberEntry.LocationComparer();

      [MethodImpl((MethodImplOptions) 32768)]
      public int Compare(LineNumberEntry l1, LineNumberEntry l2)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
