﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.MyBinaryWriter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  internal sealed class MyBinaryWriter : BinaryWriter
  {
    public MyBinaryWriter(Stream stream)
      : base(stream)
    {
    }

    public void WriteLeb128(int value)
    {
      this.Write7BitEncodedInt(value);
    }
  }
}
