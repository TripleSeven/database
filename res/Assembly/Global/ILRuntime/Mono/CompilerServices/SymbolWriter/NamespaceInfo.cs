﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.NamespaceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  internal class NamespaceInfo
  {
    public string Name;
    public int NamespaceID;
    public ArrayList UsingClauses;

    [MethodImpl((MethodImplOptions) 32768)]
    public NamespaceInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
