﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.MonoSymbolFileException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class MonoSymbolFileException : Exception
  {
    public MonoSymbolFileException()
    {
    }

    public MonoSymbolFileException(string message, params object[] args)
      : base(string.Format(message, args))
    {
    }

    public MonoSymbolFileException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
