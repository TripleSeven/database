﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.MyBinaryReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  internal class MyBinaryReader : BinaryReader
  {
    public MyBinaryReader(Stream stream)
      : base(stream)
    {
    }

    public int ReadLeb128()
    {
      return this.Read7BitEncodedInt();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string ReadString(int offset)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
