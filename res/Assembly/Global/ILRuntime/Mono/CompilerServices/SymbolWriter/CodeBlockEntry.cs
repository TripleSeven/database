﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.CodeBlockEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class CodeBlockEntry
  {
    public int Index;
    public int Parent;
    public CodeBlockEntry.Type BlockType;
    public int StartOffset;
    public int EndOffset;

    [MethodImpl((MethodImplOptions) 32768)]
    public CodeBlockEntry(int index, int parent, CodeBlockEntry.Type type, int start_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal CodeBlockEntry(int index, MyBinaryReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Close(int end_offset)
    {
      this.EndOffset = end_offset;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum Type
    {
      Lexical = 1,
      CompilerGenerated = 2,
      IteratorBody = 3,
      IteratorDispatcher = 4,
    }
  }
}
