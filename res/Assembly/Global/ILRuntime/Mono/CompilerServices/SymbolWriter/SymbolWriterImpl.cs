﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.SymbolWriterImpl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics.SymbolStore;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class SymbolWriterImpl : ISymbolWriter
  {
    private MonoSymbolWriter msw;
    private int nextLocalIndex;
    private int currentToken;
    private string methodName;
    private Stack namespaceStack;
    private bool methodOpened;
    private Hashtable documents;
    private ModuleBuilder mb;
    private SymbolWriterImpl.GetGuidFunc get_guid_func;

    [MethodImpl((MethodImplOptions) 32768)]
    public SymbolWriterImpl(ModuleBuilder mb)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseNamespace()
    {
      // ISSUE: unable to decompile the method.
    }

    public void CloseScope(int endOffset)
    {
      this.msw.CloseScope(endOffset);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ISymbolDocumentWriter DefineDocument(
      string url,
      Guid language,
      Guid languageVendor,
      Guid documentType)
    {
      // ISSUE: unable to decompile the method.
    }

    public void DefineField(
      SymbolToken parent,
      string name,
      FieldAttributes attributes,
      byte[] signature,
      SymAddressKind addrKind,
      int addr1,
      int addr2,
      int addr3)
    {
    }

    public void DefineGlobalVariable(
      string name,
      FieldAttributes attributes,
      byte[] signature,
      SymAddressKind addrKind,
      int addr1,
      int addr2,
      int addr3)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineLocalVariable(
      string name,
      FieldAttributes attributes,
      byte[] signature,
      SymAddressKind addrKind,
      int addr1,
      int addr2,
      int addr3,
      int startOffset,
      int endOffset)
    {
      // ISSUE: unable to decompile the method.
    }

    public void DefineParameter(
      string name,
      ParameterAttributes attributes,
      int sequence,
      SymAddressKind addrKind,
      int addr1,
      int addr2,
      int addr3)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineSequencePoints(
      ISymbolDocumentWriter document,
      int[] offsets,
      int[] lines,
      int[] columns,
      int[] endLines,
      int[] endColumns)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Initialize(IntPtr emitter, string filename, bool fFullBuild)
    {
      this.msw = new MonoSymbolWriter(filename);
    }

    public void OpenMethod(SymbolToken method)
    {
      this.currentToken = method.GetToken();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenNamespace(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public int OpenScope(int startOffset)
    {
      return this.msw.OpenScope(startOffset);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMethodSourceRange(
      ISymbolDocumentWriter startDoc,
      int startLine,
      int startColumn,
      ISymbolDocumentWriter endDoc,
      int endLine,
      int endColumn)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetScopeRange(int scopeID, int startOffset, int endOffset)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSymAttribute(SymbolToken parent, string name, byte[] data)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetUnderlyingWriter(IntPtr underlyingWriter)
    {
    }

    public void SetUserEntryPoint(SymbolToken entryMethod)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UsingNamespace(string fullName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetCurrentNamespace(ISymbolDocumentWriter doc)
    {
      // ISSUE: unable to decompile the method.
    }

    private delegate Guid GetGuidFunc(ModuleBuilder mb);
  }
}
