﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.SourceFileEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class SourceFileEntry
  {
    public readonly int Index;
    private int DataOffset;
    private MonoSymbolFile file;
    private string file_name;
    private byte[] guid;
    private byte[] hash;
    private bool creating;
    private bool auto_generated;
    private readonly string sourceFile;

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceFileEntry(MonoSymbolFile file, string file_name)
    {
      // ISSUE: unable to decompile the method.
    }

    public SourceFileEntry(MonoSymbolFile file, string sourceFile, byte[] guid, byte[] checksum)
      : this(file, sourceFile, sourceFile, guid, checksum)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceFileEntry(
      MonoSymbolFile file,
      string fileName,
      string sourceFile,
      byte[] guid,
      byte[] checksum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal SourceFileEntry(MonoSymbolFile file, MyBinaryReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    public static int Size
    {
      get
      {
        return 8;
      }
    }

    public byte[] Checksum
    {
      get
      {
        return this.hash;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void WriteData(MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(BinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    public string FileName
    {
      get
      {
        return this.file_name;
      }
      set
      {
        this.file_name = value;
      }
    }

    public bool AutoGenerated
    {
      get
      {
        return this.auto_generated;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoGenerated()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckChecksum()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
