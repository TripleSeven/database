﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.AnonymousScopeEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class AnonymousScopeEntry
  {
    public readonly int ID;
    private List<CapturedVariable> captured_vars;
    private List<CapturedScope> captured_scopes;

    [MethodImpl((MethodImplOptions) 32768)]
    public AnonymousScopeEntry(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal AnonymousScopeEntry(MyBinaryReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void AddCapturedVariable(
      string name,
      string captured_name,
      CapturedVariable.CapturedKind kind)
    {
      // ISSUE: unable to decompile the method.
    }

    public CapturedVariable[] CapturedVariables
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void AddCapturedScope(int scope, string captured_name)
    {
      // ISSUE: unable to decompile the method.
    }

    public CapturedScope[] CapturedScopes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
