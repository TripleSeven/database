﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.MonoSymbolWriter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class MonoSymbolWriter
  {
    private List<SourceMethodBuilder> methods;
    private List<SourceFileEntry> sources;
    private List<CompileUnitEntry> comp_units;
    protected readonly MonoSymbolFile file;
    private string filename;
    private SourceMethodBuilder current_method;
    private Stack<SourceMethodBuilder> current_method_stack;

    [MethodImpl((MethodImplOptions) 32768)]
    public MonoSymbolWriter(string filename)
    {
      // ISSUE: unable to decompile the method.
    }

    public MonoSymbolFile SymbolFile
    {
      get
      {
        return this.file;
      }
    }

    public void CloseNamespace()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineLocalVariable(int index, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineCapturedLocal(int scope_id, string name, string captured_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineCapturedParameter(int scope_id, string name, string captured_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineCapturedThis(int scope_id, string captured_name)
    {
      // ISSUE: unable to decompile the method.
    }

    public void DefineCapturedScope(int scope_id, int id, string captured_name)
    {
      this.file.DefineCapturedScope(scope_id, id, captured_name);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefineScopeVariable(int scope, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MarkSequencePoint(
      int offset,
      SourceFileEntry file,
      int line,
      int column,
      bool is_hidden)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceMethodBuilder OpenMethod(
      ICompileUnit file,
      int ns_id,
      IMethodDef method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceFileEntry DefineDocument(string url)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceFileEntry DefineDocument(string url, byte[] guid, byte[] checksum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CompileUnitEntry DefineCompilationUnit(SourceFileEntry source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int DefineNamespace(
      string name,
      CompileUnitEntry unit,
      string[] using_clauses,
      int parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int OpenScope(int start_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseScope(int end_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenCompilerGeneratedBlock(int start_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseCompilerGeneratedBlock(int end_offset)
    {
      // ISSUE: unable to decompile the method.
    }

    public void StartIteratorBody(int start_offset)
    {
      this.current_method.StartBlock(CodeBlockEntry.Type.IteratorBody, start_offset);
    }

    public void EndIteratorBody(int end_offset)
    {
      this.current_method.EndBlock(end_offset);
    }

    public void StartIteratorDispatcher(int start_offset)
    {
      this.current_method.StartBlock(CodeBlockEntry.Type.IteratorDispatcher, start_offset);
    }

    public void EndIteratorDispatcher(int end_offset)
    {
      this.current_method.EndBlock(end_offset);
    }

    public void DefineAnonymousScope(int id)
    {
      this.file.DefineAnonymousScope(id);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WriteSymbolFile(Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
