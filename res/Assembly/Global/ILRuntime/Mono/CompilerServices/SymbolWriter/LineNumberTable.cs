﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.LineNumberTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class LineNumberTable
  {
    protected LineNumberEntry[] _line_numbers;
    public readonly int LineBase;
    public readonly int LineRange;
    public readonly byte OpcodeBase;
    public readonly int MaxAddressIncrement;
    public const int Default_LineBase = -1;
    public const int Default_LineRange = 8;
    public const byte Default_OpcodeBase = 9;
    public const byte DW_LNS_copy = 1;
    public const byte DW_LNS_advance_pc = 2;
    public const byte DW_LNS_advance_line = 3;
    public const byte DW_LNS_set_file = 4;
    public const byte DW_LNS_const_add_pc = 8;
    public const byte DW_LNE_end_sequence = 1;
    public const byte DW_LNE_MONO_negate_is_hidden = 64;
    internal const byte DW_LNE_MONO__extensions_start = 64;
    internal const byte DW_LNE_MONO__extensions_end = 127;

    [MethodImpl((MethodImplOptions) 32768)]
    protected LineNumberTable(MonoSymbolFile file)
    {
      // ISSUE: unable to decompile the method.
    }

    internal LineNumberTable(MonoSymbolFile file, LineNumberEntry[] lines)
      : this(file)
    {
      this._line_numbers = lines;
    }

    public LineNumberEntry[] LineNumbers
    {
      get
      {
        return this._line_numbers;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(
      MonoSymbolFile file,
      MyBinaryWriter bw,
      bool hasColumnsInfo,
      bool hasEndInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static LineNumberTable Read(
      MonoSymbolFile file,
      MyBinaryReader br,
      bool readColumnsInfo,
      bool readEndInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoRead(
      MonoSymbolFile file,
      MyBinaryReader br,
      bool includesColumns,
      bool includesEnds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetMethodBounds(out LineNumberEntry start, out LineNumberEntry end)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
