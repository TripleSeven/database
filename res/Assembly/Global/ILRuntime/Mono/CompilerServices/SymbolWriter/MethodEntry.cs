﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.MethodEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class MethodEntry : IComparable
  {
    public readonly int CompileUnitIndex;
    public readonly int Token;
    public readonly int NamespaceID;
    private int DataOffset;
    private int LocalVariableTableOffset;
    private int LineNumberTableOffset;
    private int CodeBlockTableOffset;
    private int ScopeVariableTableOffset;
    private int RealNameOffset;
    private MethodEntry.Flags flags;
    private int index;
    public readonly CompileUnitEntry CompileUnit;
    private LocalVariableEntry[] locals;
    private CodeBlockEntry[] code_blocks;
    private ScopeVariable[] scope_vars;
    private LineNumberTable lnt;
    private string real_name;
    public readonly MonoSymbolFile SymbolFile;
    public const int Size = 12;

    [MethodImpl((MethodImplOptions) 32768)]
    internal MethodEntry(MonoSymbolFile file, MyBinaryReader reader, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal MethodEntry(
      MonoSymbolFile file,
      CompileUnitEntry comp_unit,
      int token,
      ScopeVariable[] scope_vars,
      LocalVariableEntry[] locals,
      LineNumberEntry[] lines,
      CodeBlockEntry[] code_blocks,
      string real_name,
      MethodEntry.Flags flags,
      int namespace_id)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodEntry.Flags MethodFlags
    {
      get
      {
        return this.flags;
      }
    }

    public int Index
    {
      get
      {
        return this.index;
      }
      set
      {
        this.index = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CheckLineNumberTable(LineNumberEntry[] line_numbers)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Write(MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void WriteData(MonoSymbolFile file, MyBinaryWriter bw)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LineNumberTable GetLineNumberTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalVariableEntry[] GetLocals()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CodeBlockEntry[] GetCodeBlocks()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ScopeVariable[] GetScopeVariables()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetRealName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CompareTo(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [System.Flags]
    public enum Flags
    {
      LocalNamesAmbiguous = 1,
      ColumnsInfoIncluded = 2,
      EndInfoIncluded = 4,
    }
  }
}
