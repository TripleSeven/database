﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.MonoSymbolFile
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  public class MonoSymbolFile : IDisposable
  {
    private List<MethodEntry> methods;
    private List<SourceFileEntry> sources;
    private List<CompileUnitEntry> comp_units;
    private Dictionary<int, AnonymousScopeEntry> anonymous_scopes;
    private OffsetTable ot;
    private int last_type_index;
    private int last_method_index;
    private int last_namespace_index;
    public readonly int MajorVersion;
    public readonly int MinorVersion;
    public int NumLineNumbers;
    private MyBinaryReader reader;
    private Dictionary<int, SourceFileEntry> source_file_hash;
    private Dictionary<int, CompileUnitEntry> compile_unit_hash;
    private List<MethodEntry> method_list;
    private Dictionary<int, MethodEntry> method_token_hash;
    private Dictionary<string, int> source_name_hash;
    private Guid guid;
    internal int LineNumberCount;
    internal int LocalCount;
    internal int StringSize;
    internal int LineNumberSize;
    internal int ExtendedLineNumberSize;

    [MethodImpl((MethodImplOptions) 32768)]
    public MonoSymbolFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MonoSymbolFile(Stream stream)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddSource(SourceFileEntry source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddCompileUnit(CompileUnitEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddMethod(MethodEntry entry)
    {
      this.methods.Add(entry);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodEntry DefineMethod(
      CompileUnitEntry comp_unit,
      int token,
      ScopeVariable[] scope_vars,
      LocalVariableEntry[] locals,
      LineNumberEntry[] lines,
      CodeBlockEntry[] code_blocks,
      string real_name,
      MethodEntry.Flags flags,
      int namespace_id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void DefineAnonymousScope(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void DefineCapturedVariable(
      int scope_id,
      string name,
      string captured_name,
      CapturedVariable.CapturedKind kind)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void DefineCapturedScope(int scope_id, int id, string captured_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal int GetNextTypeIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal int GetNextMethodIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal int GetNextNamespaceIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Write(MyBinaryWriter bw, Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateSymbolFile(Guid guid, FileStream fs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MonoSymbolFile ReadSymbolFile(Assembly assembly)
    {
      // ISSUE: unable to decompile the method.
    }

    public static MonoSymbolFile ReadSymbolFile(string mdbFilename)
    {
      return MonoSymbolFile.ReadSymbolFile((Stream) new FileStream(mdbFilename, FileMode.Open, FileAccess.Read));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MonoSymbolFile ReadSymbolFile(string mdbFilename, Guid assemblyGuid)
    {
      // ISSUE: unable to decompile the method.
    }

    public static MonoSymbolFile ReadSymbolFile(Stream stream)
    {
      return new MonoSymbolFile(stream);
    }

    public int CompileUnitCount
    {
      get
      {
        return this.ot.CompileUnitCount;
      }
    }

    public int SourceCount
    {
      get
      {
        return this.ot.SourceCount;
      }
    }

    public int MethodCount
    {
      get
      {
        return this.ot.MethodCount;
      }
    }

    public int TypeCount
    {
      get
      {
        return this.ot.TypeCount;
      }
    }

    public int AnonymousScopeCount
    {
      get
      {
        return this.ot.AnonymousScopeCount;
      }
    }

    public int NamespaceCount
    {
      get
      {
        return this.last_namespace_index;
      }
    }

    public Guid Guid
    {
      get
      {
        return this.guid;
      }
    }

    public OffsetTable OffsetTable
    {
      get
      {
        return this.ot;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceFileEntry GetSourceFile(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public SourceFileEntry[] Sources
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CompileUnitEntry GetCompileUnit(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public CompileUnitEntry[] CompileUnits
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void read_methods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodEntry GetMethodByToken(int token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodEntry GetMethod(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodEntry[] Methods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindSource(string file_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AnonymousScopeEntry GetAnonymousScope(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    internal MyBinaryReader BinaryReader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public void Dispose()
    {
      this.Dispose(true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void Dispose(bool disposing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
