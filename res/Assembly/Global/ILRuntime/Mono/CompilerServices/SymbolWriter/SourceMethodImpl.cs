﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.CompilerServices.SymbolWriter.SourceMethodImpl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.CompilerServices.SymbolWriter
{
  internal class SourceMethodImpl : IMethodDef
  {
    private string name;
    private int token;
    private int namespaceID;

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceMethodImpl(string name, int token, int namespaceID)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public int NamespaceID
    {
      get
      {
        return this.namespaceID;
      }
    }

    public int Token
    {
      get
      {
        return this.token;
      }
    }
  }
}
