﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PropertyDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class PropertyDefinition : PropertyReference, IMemberDefinition, IConstantProvider, ICustomAttributeProvider, IMetadataTokenProvider
  {
    private bool? has_this;
    private ushort attributes;
    private Collection<CustomAttribute> custom_attributes;
    internal MethodDefinition get_method;
    internal MethodDefinition set_method;
    internal Collection<MethodDefinition> other_methods;
    private object constant;

    [MethodImpl((MethodImplOptions) 32768)]
    public PropertyDefinition(
      string name,
      PropertyAttributes attributes,
      TypeReference propertyType)
    {
      // ISSUE: unable to decompile the method.
    }

    public PropertyAttributes Attributes
    {
      get
      {
        return (PropertyAttributes) this.attributes;
      }
      set
      {
        this.attributes = (ushort) value;
      }
    }

    public bool HasThis
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.has_this = new bool?(value);
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodDefinition GetMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.get_method = value;
      }
    }

    public MethodDefinition SetMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.set_method = value;
      }
    }

    public bool HasOtherMethods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<MethodDefinition> OtherMethods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override Collection<ParameterDefinition> Parameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Collection<ParameterDefinition> MirrorParameters(
      MethodDefinition method,
      int bound)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasConstant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public object Constant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.constant = value;
      }
    }

    public bool IsSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntimeSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDefault
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeDefinition DeclaringType
    {
      get
      {
        return (TypeDefinition) base.DeclaringType;
      }
      set
      {
        this.DeclaringType = (TypeReference) value;
      }
    }

    public override bool IsDefinition
    {
      get
      {
        return true;
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    public override PropertyDefinition Resolve()
    {
      return this;
    }
  }
}
