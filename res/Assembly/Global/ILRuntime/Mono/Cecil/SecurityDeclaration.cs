﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.SecurityDeclaration
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class SecurityDeclaration
  {
    internal readonly uint signature;
    private byte[] blob;
    private readonly ModuleDefinition module;
    internal bool resolved;
    private SecurityAction action;
    internal Collection<SecurityAttribute> security_attributes;

    [MethodImpl((MethodImplOptions) 32768)]
    internal SecurityDeclaration(SecurityAction action, uint signature, ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SecurityDeclaration(SecurityAction action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SecurityDeclaration(SecurityAction action, byte[] blob)
    {
      // ISSUE: unable to decompile the method.
    }

    public SecurityAction Action
    {
      get
      {
        return this.action;
      }
      set
      {
        this.action = value;
      }
    }

    public bool HasSecurityAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<SecurityAttribute> SecurityAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    internal bool HasImage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] GetBlob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Resolve()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
