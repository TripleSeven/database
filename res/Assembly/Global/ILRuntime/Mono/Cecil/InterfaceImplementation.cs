﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.InterfaceImplementation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class InterfaceImplementation : ICustomAttributeProvider, IMetadataTokenProvider
  {
    internal TypeDefinition type;
    internal MetadataToken token;
    private TypeReference interface_type;
    private Collection<CustomAttribute> custom_attributes;

    [MethodImpl((MethodImplOptions) 32768)]
    internal InterfaceImplementation(TypeReference interfaceType, MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public InterfaceImplementation(TypeReference interfaceType)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference InterfaceType
    {
      get
      {
        return this.interface_type;
      }
      set
      {
        this.interface_type = value;
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }
  }
}
