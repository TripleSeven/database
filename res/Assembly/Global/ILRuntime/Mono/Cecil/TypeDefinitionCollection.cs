﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeDefinitionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Collections.Generic;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class TypeDefinitionCollection : Collection<TypeDefinition>
  {
    private readonly ModuleDefinition container;
    private readonly Dictionary<Row<string, string>, TypeDefinition> name_cache;

    [MethodImpl((MethodImplOptions) 32768)]
    internal TypeDefinitionCollection(ModuleDefinition container)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal TypeDefinitionCollection(ModuleDefinition container, int capacity)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnAdd(TypeDefinition item, int index)
    {
      this.Attach(item);
    }

    protected override void OnSet(TypeDefinition item, int index)
    {
      this.Attach(item);
    }

    protected override void OnInsert(TypeDefinition item, int index)
    {
      this.Attach(item);
    }

    protected override void OnRemove(TypeDefinition item, int index)
    {
      this.Detach(item);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnClear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Attach(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Detach(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition GetType(string fullname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition GetType(string @namespace, string name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
