﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodSemanticsAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum MethodSemanticsAttributes : ushort
  {
    None = 0,
    Setter = 1,
    Getter = 2,
    Other = 4,
    AddOn = 8,
    RemoveOn = 16, // 0x0010
    Fire = 32, // 0x0020
  }
}
