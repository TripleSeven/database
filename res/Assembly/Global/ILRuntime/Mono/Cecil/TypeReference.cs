﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public class TypeReference : MemberReference, IGenericParameterProvider, IGenericContext, IMetadataTokenProvider
  {
    private string @namespace;
    private bool value_type;
    private int hashCode;
    private static int instance_id;
    internal IMetadataScope scope;
    internal ModuleDefinition module;
    internal ElementType etype;
    private string fullname;
    protected Collection<GenericParameter> generic_parameters;

    [MethodImpl((MethodImplOptions) 32768)]
    protected TypeReference(string @namespace, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference(
      string @namespace,
      string name,
      ModuleDefinition module,
      IMetadataScope scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference(
      string @namespace,
      string name,
      ModuleDefinition module,
      IMetadataScope scope,
      bool valueType)
    {
      // ISSUE: unable to decompile the method.
    }

    public override string Name
    {
      get
      {
        return base.Name;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual string Namespace
    {
      get
      {
        return this.@namespace;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual bool IsValueType
    {
      get
      {
        return this.value_type;
      }
      set
      {
        this.value_type = value;
      }
    }

    public override ModuleDefinition Module
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    internal TypeReferenceProjection WindowsRuntimeProjection
    {
      get
      {
        return (TypeReferenceProjection) this.projection;
      }
      set
      {
        this.projection = (object) value;
      }
    }

    IGenericParameterProvider IGenericContext.Type
    {
      get
      {
        return (IGenericParameterProvider) this;
      }
    }

    IGenericParameterProvider IGenericContext.Method
    {
      get
      {
        return (IGenericParameterProvider) null;
      }
    }

    GenericParameterType IGenericParameterProvider.GenericParameterType
    {
      get
      {
        return GenericParameterType.Type;
      }
    }

    public virtual bool HasGenericParameters
    {
      get
      {
        return !this.generic_parameters.IsNullOrEmpty<GenericParameter>();
      }
    }

    public virtual Collection<GenericParameter> GenericParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual IMetadataScope Scope
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNested
    {
      get
      {
        return this.DeclaringType != null;
      }
    }

    public override TypeReference DeclaringType
    {
      get
      {
        return base.DeclaringType;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual bool IsByReference
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsPointer
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsSentinel
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsArray
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsGenericParameter
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsGenericInstance
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsRequiredModifier
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsOptionalModifier
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsPinned
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsFunctionPointer
    {
      get
      {
        return false;
      }
    }

    public virtual bool IsPrimitive
    {
      get
      {
        return this.etype.IsPrimitive();
      }
    }

    public virtual MetadataType MetadataType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected virtual void ClearFullName()
    {
      this.fullname = (string) null;
    }

    public virtual TypeReference GetElementType()
    {
      return this;
    }

    protected override IMemberDefinition ResolveDefinition()
    {
      return (IMemberDefinition) this.Resolve();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual TypeDefinition Resolve()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
