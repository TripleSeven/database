﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodReturnType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class MethodReturnType : IConstantProvider, ICustomAttributeProvider, IMarshalInfoProvider, IMetadataTokenProvider
  {
    internal IMethodSignature method;
    internal ParameterDefinition parameter;
    private TypeReference return_type;

    public MethodReturnType(IMethodSignature method)
    {
      this.method = method;
    }

    public IMethodSignature Method
    {
      get
      {
        return this.method;
      }
    }

    public TypeReference ReturnType
    {
      get
      {
        return this.return_type;
      }
      set
      {
        this.return_type = value;
      }
    }

    internal ParameterDefinition Parameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.Parameter.MetadataToken;
      }
      set
      {
        this.Parameter.MetadataToken = value;
      }
    }

    public ParameterAttributes Attributes
    {
      get
      {
        return this.Parameter.Attributes;
      }
      set
      {
        this.Parameter.Attributes = value;
      }
    }

    public string Name
    {
      get
      {
        return this.Parameter.Name;
      }
      set
      {
        this.Parameter.Name = value;
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      get
      {
        return this.Parameter.CustomAttributes;
      }
    }

    public bool HasDefault
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.Parameter.HasDefault = value;
      }
    }

    public bool HasConstant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.Parameter.HasConstant = value;
      }
    }

    public object Constant
    {
      get
      {
        return this.Parameter.Constant;
      }
      set
      {
        this.Parameter.Constant = value;
      }
    }

    public bool HasFieldMarshal
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.Parameter.HasFieldMarshal = value;
      }
    }

    public bool HasMarshalInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MarshalInfo MarshalInfo
    {
      get
      {
        return this.Parameter.MarshalInfo;
      }
      set
      {
        this.Parameter.MarshalInfo = value;
      }
    }
  }
}
