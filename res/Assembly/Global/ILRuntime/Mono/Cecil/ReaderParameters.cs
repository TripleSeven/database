﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ReaderParameters
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class ReaderParameters
  {
    private ReadingMode reading_mode;
    internal IAssemblyResolver assembly_resolver;
    internal IMetadataResolver metadata_resolver;
    private Stream symbol_stream;
    private ISymbolReaderProvider symbol_reader_provider;
    private bool read_symbols;
    private bool throw_symbols_mismatch;
    private bool projections;
    private bool in_memory;
    private bool read_write;

    public ReaderParameters()
      : this(ReadingMode.Deferred)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ReaderParameters(ReadingMode readingMode)
    {
      // ISSUE: unable to decompile the method.
    }

    public ReadingMode ReadingMode
    {
      get
      {
        return this.reading_mode;
      }
      set
      {
        this.reading_mode = value;
      }
    }

    public bool InMemory
    {
      get
      {
        return this.in_memory;
      }
      set
      {
        this.in_memory = value;
      }
    }

    public IAssemblyResolver AssemblyResolver
    {
      get
      {
        return this.assembly_resolver;
      }
      set
      {
        this.assembly_resolver = value;
      }
    }

    public IMetadataResolver MetadataResolver
    {
      get
      {
        return this.metadata_resolver;
      }
      set
      {
        this.metadata_resolver = value;
      }
    }

    public Stream SymbolStream
    {
      get
      {
        return this.symbol_stream;
      }
      set
      {
        this.symbol_stream = value;
      }
    }

    public ISymbolReaderProvider SymbolReaderProvider
    {
      get
      {
        return this.symbol_reader_provider;
      }
      set
      {
        this.symbol_reader_provider = value;
      }
    }

    public bool ReadSymbols
    {
      get
      {
        return this.read_symbols;
      }
      set
      {
        this.read_symbols = value;
      }
    }

    public bool ThrowIfSymbolsAreNotMatching
    {
      get
      {
        return this.throw_symbols_mismatch;
      }
      set
      {
        this.throw_symbols_mismatch = value;
      }
    }

    public bool ReadWrite
    {
      get
      {
        return this.read_write;
      }
      set
      {
        this.read_write = value;
      }
    }

    public bool ApplyWindowsRuntimeProjections
    {
      get
      {
        return this.projections;
      }
      set
      {
        this.projections = value;
      }
    }
  }
}
