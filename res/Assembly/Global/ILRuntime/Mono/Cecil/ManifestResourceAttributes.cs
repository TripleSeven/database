﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ManifestResourceAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum ManifestResourceAttributes : uint
  {
    VisibilityMask = 7,
    Public = 1,
    Private = 2,
  }
}
