﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodImplAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum MethodImplAttributes : ushort
  {
    CodeTypeMask = 3,
    IL = 0,
    Native = 1,
    OPTIL = 2,
    Runtime = OPTIL | Native, // 0x0003
    ManagedMask = 4,
    Unmanaged = ManagedMask, // 0x0004
    Managed = 0,
    ForwardRef = 16, // 0x0010
    PreserveSig = 128, // 0x0080
    InternalCall = 4096, // 0x1000
    Synchronized = 32, // 0x0020
    NoOptimization = 64, // 0x0040
    NoInlining = 8,
    AggressiveInlining = 256, // 0x0100
  }
}
