﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PropertyReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class PropertyReference : MemberReference
  {
    private TypeReference property_type;

    [MethodImpl((MethodImplOptions) 32768)]
    internal PropertyReference(string name, TypeReference propertyType)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference PropertyType
    {
      get
      {
        return this.property_type;
      }
      set
      {
        this.property_type = value;
      }
    }

    public abstract Collection<ParameterDefinition> Parameters { get; }

    protected override IMemberDefinition ResolveDefinition()
    {
      return (IMemberDefinition) this.Resolve();
    }

    public abstract PropertyDefinition Resolve();
  }
}
