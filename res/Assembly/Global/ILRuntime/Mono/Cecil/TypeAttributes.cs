﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum TypeAttributes : uint
  {
    VisibilityMask = 7,
    NotPublic = 0,
    Public = 1,
    NestedPublic = 2,
    NestedPrivate = NestedPublic | Public, // 0x00000003
    NestedFamily = 4,
    NestedAssembly = NestedFamily | Public, // 0x00000005
    NestedFamANDAssem = NestedFamily | NestedPublic, // 0x00000006
    NestedFamORAssem = NestedFamANDAssem | Public, // 0x00000007
    LayoutMask = 24, // 0x00000018
    AutoLayout = 0,
    SequentialLayout = 8,
    ExplicitLayout = 16, // 0x00000010
    ClassSemanticMask = 32, // 0x00000020
    Class = 0,
    Interface = ClassSemanticMask, // 0x00000020
    Abstract = 128, // 0x00000080
    Sealed = 256, // 0x00000100
    SpecialName = 1024, // 0x00000400
    Import = 4096, // 0x00001000
    Serializable = 8192, // 0x00002000
    WindowsRuntime = 16384, // 0x00004000
    StringFormatMask = 196608, // 0x00030000
    AnsiClass = 0,
    UnicodeClass = 65536, // 0x00010000
    AutoClass = 131072, // 0x00020000
    BeforeFieldInit = 1048576, // 0x00100000
    RTSpecialName = 2048, // 0x00000800
    HasSecurity = 262144, // 0x00040000
    Forwarder = 2097152, // 0x00200000
  }
}
