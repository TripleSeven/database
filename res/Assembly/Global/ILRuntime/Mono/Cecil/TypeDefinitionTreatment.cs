﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeDefinitionTreatment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  internal enum TypeDefinitionTreatment
  {
    None = 0,
    KindMask = 15, // 0x0000000F
    NormalType = 1,
    NormalAttribute = 2,
    UnmangleWindowsRuntimeName = NormalAttribute | NormalType, // 0x00000003
    PrefixWindowsRuntimeName = 4,
    RedirectToClrType = PrefixWindowsRuntimeName | NormalType, // 0x00000005
    RedirectToClrAttribute = PrefixWindowsRuntimeName | NormalAttribute, // 0x00000006
    Abstract = 16, // 0x00000010
    Internal = 32, // 0x00000020
  }
}
