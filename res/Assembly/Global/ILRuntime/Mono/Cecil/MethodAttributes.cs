﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum MethodAttributes : ushort
  {
    MemberAccessMask = 7,
    CompilerControlled = 0,
    Private = 1,
    FamANDAssem = 2,
    Assembly = FamANDAssem | Private, // 0x0003
    Family = 4,
    FamORAssem = Family | Private, // 0x0005
    Public = Family | FamANDAssem, // 0x0006
    Static = 16, // 0x0010
    Final = 32, // 0x0020
    Virtual = 64, // 0x0040
    HideBySig = 128, // 0x0080
    VtableLayoutMask = 256, // 0x0100
    ReuseSlot = 0,
    NewSlot = VtableLayoutMask, // 0x0100
    CheckAccessOnOverride = 512, // 0x0200
    Abstract = 1024, // 0x0400
    SpecialName = 2048, // 0x0800
    PInvokeImpl = 8192, // 0x2000
    UnmanagedExport = 8,
    RTSpecialName = 4096, // 0x1000
    HasSecurity = 16384, // 0x4000
    RequireSecObject = 32768, // 0x8000
  }
}
