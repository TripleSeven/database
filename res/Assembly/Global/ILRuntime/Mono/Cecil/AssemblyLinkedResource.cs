﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyLinkedResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class AssemblyLinkedResource : Resource
  {
    private AssemblyNameReference reference;

    public AssemblyLinkedResource(string name, ManifestResourceAttributes flags)
      : base(name, flags)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssemblyLinkedResource(
      string name,
      ManifestResourceAttributes flags,
      AssemblyNameReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    public AssemblyNameReference Assembly
    {
      get
      {
        return this.reference;
      }
      set
      {
        this.reference = value;
      }
    }

    public override ResourceType ResourceType
    {
      get
      {
        return ResourceType.AssemblyLinked;
      }
    }
  }
}
