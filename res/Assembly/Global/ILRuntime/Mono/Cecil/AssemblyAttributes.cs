﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum AssemblyAttributes : uint
  {
    PublicKey = 1,
    SideBySideCompatible = 0,
    Retargetable = 256, // 0x00000100
    WindowsRuntime = 512, // 0x00000200
    DisableJITCompileOptimizer = 16384, // 0x00004000
    EnableJITCompileTracking = 32768, // 0x00008000
  }
}
