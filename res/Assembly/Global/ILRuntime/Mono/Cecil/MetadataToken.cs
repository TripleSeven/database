﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MetadataToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public struct MetadataToken : IEquatable<MetadataToken>
  {
    public static readonly MetadataToken Zero = new MetadataToken(0U);
    private readonly uint token;

    public MetadataToken(uint token)
    {
      this.token = token;
    }

    public MetadataToken(TokenType type)
    {
      this = new MetadataToken(type, 0);
    }

    public MetadataToken(TokenType type, uint rid)
    {
      this.token = (uint) (type | (TokenType) rid);
    }

    public MetadataToken(TokenType type, int rid)
    {
      this.token = (uint) (type | (TokenType) rid);
    }

    public uint RID
    {
      get
      {
        return this.token & 16777215U;
      }
    }

    public TokenType TokenType
    {
      get
      {
        return (TokenType) ((int) this.token & -16777216);
      }
    }

    public int ToInt32()
    {
      return (int) this.token;
    }

    public uint ToUInt32()
    {
      return this.token;
    }

    public override int GetHashCode()
    {
      return (int) this.token;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Equals(MetadataToken other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator ==(MetadataToken one, MetadataToken other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator !=(MetadataToken one, MetadataToken other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
