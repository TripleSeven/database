﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TokenType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum TokenType : uint
  {
    Module = 0,
    TypeRef = 16777216, // 0x01000000
    TypeDef = 33554432, // 0x02000000
    Field = 67108864, // 0x04000000
    Method = 100663296, // 0x06000000
    Param = 134217728, // 0x08000000
    InterfaceImpl = 150994944, // 0x09000000
    MemberRef = 167772160, // 0x0A000000
    CustomAttribute = 201326592, // 0x0C000000
    Permission = 234881024, // 0x0E000000
    Signature = 285212672, // 0x11000000
    Event = 335544320, // 0x14000000
    Property = 385875968, // 0x17000000
    ModuleRef = 436207616, // 0x1A000000
    TypeSpec = 452984832, // 0x1B000000
    Assembly = 536870912, // 0x20000000
    AssemblyRef = 587202560, // 0x23000000
    File = 637534208, // 0x26000000
    ExportedType = 654311424, // 0x27000000
    ManifestResource = 671088640, // 0x28000000
    GenericParam = 704643072, // 0x2A000000
    MethodSpec = 721420288, // 0x2B000000
    GenericParamConstraint = 738197504, // 0x2C000000
    Document = 805306368, // 0x30000000
    MethodDebugInformation = 822083584, // 0x31000000
    LocalScope = 838860800, // 0x32000000
    LocalVariable = 855638016, // 0x33000000
    LocalConstant = 872415232, // 0x34000000
    ImportScope = 889192448, // 0x35000000
    StateMachineMethod = 905969664, // 0x36000000
    CustomDebugInformation = 922746880, // 0x37000000
    String = 1879048192, // 0x70000000
  }
}
