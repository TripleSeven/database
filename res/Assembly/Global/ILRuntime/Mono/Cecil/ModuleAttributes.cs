﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ModuleAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum ModuleAttributes
  {
    ILOnly = 1,
    Required32Bit = 2,
    ILLibrary = 4,
    StrongNameSigned = 8,
    Preferred32Bit = 131072, // 0x00020000
  }
}
