﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.CustomMarshalInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  public sealed class CustomMarshalInfo : MarshalInfo
  {
    internal Guid guid;
    internal string unmanaged_type;
    internal TypeReference managed_type;
    internal string cookie;

    public CustomMarshalInfo()
      : base(NativeType.CustomMarshaler)
    {
    }

    public Guid Guid
    {
      get
      {
        return this.guid;
      }
      set
      {
        this.guid = value;
      }
    }

    public string UnmanagedType
    {
      get
      {
        return this.unmanaged_type;
      }
      set
      {
        this.unmanaged_type = value;
      }
    }

    public TypeReference ManagedType
    {
      get
      {
        return this.managed_type;
      }
      set
      {
        this.managed_type = value;
      }
    }

    public string Cookie
    {
      get
      {
        return this.cookie;
      }
      set
      {
        this.cookie = value;
      }
    }
  }
}
