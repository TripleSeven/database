﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Range
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  internal struct Range
  {
    public uint Start;
    public uint Length;

    public Range(uint index, uint length)
    {
      this.Start = index;
      this.Length = length;
    }
  }
}
