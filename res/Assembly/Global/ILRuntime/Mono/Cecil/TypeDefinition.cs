﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class TypeDefinition : TypeReference, IMemberDefinition, ISecurityDeclarationProvider, ICustomAttributeProvider, IMetadataTokenProvider
  {
    private uint attributes;
    private TypeReference base_type;
    internal Range fields_range;
    internal Range methods_range;
    private short packing_size;
    private int class_size;
    private InterfaceImplementationCollection interfaces;
    private Collection<TypeDefinition> nested_types;
    private Collection<MethodDefinition> methods;
    private Collection<FieldDefinition> fields;
    private Collection<EventDefinition> events;
    private Collection<PropertyDefinition> properties;
    private Collection<CustomAttribute> custom_attributes;
    private Collection<SecurityDeclaration> security_declarations;

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition(string @namespace, string name, TypeAttributes attributes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition(
      string @namespace,
      string name,
      TypeAttributes attributes,
      TypeReference baseType)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeAttributes Attributes
    {
      get
      {
        return (TypeAttributes) this.attributes;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference BaseType
    {
      get
      {
        return this.base_type;
      }
      set
      {
        this.base_type = value;
      }
    }

    public override string Name
    {
      get
      {
        return base.Name;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResolveLayout()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasLayoutInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public short PackingSize
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.packing_size = value;
      }
    }

    public int ClassSize
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.class_size = value;
      }
    }

    public bool HasInterfaces
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<InterfaceImplementation> Interfaces
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasNestedTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<TypeDefinition> NestedTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasMethods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<MethodDefinition> Methods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasFields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<FieldDefinition> Fields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasEvents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<EventDefinition> Events
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasProperties
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<PropertyDefinition> Properties
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasSecurityDeclarations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<SecurityDeclaration> SecurityDeclarations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool HasGenericParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override Collection<GenericParameter> GenericParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNotPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedPrivate
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 3U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedFamily
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 4U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 5U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedFamilyAndAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 6U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedFamilyOrAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 7U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoLayout
    {
      get
      {
        return this.attributes.GetMaskedAttributes(24U, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSequentialLayout
    {
      get
      {
        return this.attributes.GetMaskedAttributes(24U, 8U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsExplicitLayout
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsClass
    {
      get
      {
        return this.attributes.GetMaskedAttributes(32U, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsInterface
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAbstract
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSealed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsImport
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSerializable
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsWindowsRuntime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAnsiClass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsUnicodeClass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoClass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsBeforeFieldInit
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntimeSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasSecurity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsEnum
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsValueType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        throw new NotSupportedException();
      }
    }

    public override bool IsPrimitive
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override MetadataType MetadataType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsDefinition
    {
      get
      {
        return true;
      }
    }

    public TypeDefinition DeclaringType
    {
      get
      {
        return (TypeDefinition) base.DeclaringType;
      }
      set
      {
        this.DeclaringType = (TypeReference) value;
      }
    }

    internal TypeDefinitionProjection WindowsRuntimeProjection
    {
      get
      {
        return (TypeDefinitionProjection) this.projection;
      }
      set
      {
        this.projection = (object) value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearFullName()
    {
      // ISSUE: unable to decompile the method.
    }

    public override TypeDefinition Resolve()
    {
      return this;
    }
  }
}
