﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class AssemblyDefinition : ICustomAttributeProvider, ISecurityDeclarationProvider, IDisposable, IMetadataTokenProvider
  {
    private AssemblyNameDefinition name;
    internal ModuleDefinition main_module;
    private Collection<ModuleDefinition> modules;
    private Collection<CustomAttribute> custom_attributes;
    private Collection<SecurityDeclaration> security_declarations;

    internal AssemblyDefinition()
    {
    }

    public AssemblyNameDefinition Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return new MetadataToken(TokenType.Assembly, 1);
      }
      set
      {
      }
    }

    public Collection<ModuleDefinition> Modules
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ModuleDefinition MainModule
    {
      get
      {
        return this.main_module;
      }
    }

    public MethodDefinition EntryPoint
    {
      get
      {
        return this.main_module.EntryPoint;
      }
      set
      {
        this.main_module.EntryPoint = value;
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasSecurityDeclarations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<SecurityDeclaration> SecurityDeclarations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    public static AssemblyDefinition ReadAssembly(string fileName)
    {
      return AssemblyDefinition.ReadAssembly(ModuleDefinition.ReadModule(fileName));
    }

    public static AssemblyDefinition ReadAssembly(
      string fileName,
      ReaderParameters parameters)
    {
      return AssemblyDefinition.ReadAssembly(ModuleDefinition.ReadModule(fileName, parameters));
    }

    public static AssemblyDefinition ReadAssembly(Stream stream)
    {
      return AssemblyDefinition.ReadAssembly(ModuleDefinition.ReadModule(stream));
    }

    public static AssemblyDefinition ReadAssembly(
      Stream stream,
      ReaderParameters parameters)
    {
      return AssemblyDefinition.ReadAssembly(ModuleDefinition.ReadModule(stream, parameters));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static AssemblyDefinition ReadAssembly(ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    public override string ToString()
    {
      return this.FullName;
    }
  }
}
