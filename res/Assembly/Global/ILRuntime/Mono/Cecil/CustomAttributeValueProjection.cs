﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.CustomAttributeValueProjection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class CustomAttributeValueProjection
  {
    public readonly AttributeTargets Targets;
    public readonly CustomAttributeValueTreatment Treatment;

    [MethodImpl((MethodImplOptions) 32768)]
    public CustomAttributeValueProjection(
      AttributeTargets targets,
      CustomAttributeValueTreatment treatment)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
