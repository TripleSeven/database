﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.EventDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class EventDefinition : EventReference, IMemberDefinition, ICustomAttributeProvider, IMetadataTokenProvider
  {
    private ushort attributes;
    private Collection<CustomAttribute> custom_attributes;
    internal MethodDefinition add_method;
    internal MethodDefinition invoke_method;
    internal MethodDefinition remove_method;
    internal Collection<MethodDefinition> other_methods;

    [MethodImpl((MethodImplOptions) 32768)]
    public EventDefinition(string name, EventAttributes attributes, TypeReference eventType)
    {
    }

    public EventAttributes Attributes
    {
      get
      {
        return (EventAttributes) this.attributes;
      }
      set
      {
        this.attributes = (ushort) value;
      }
    }

    public MethodDefinition AddMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.add_method = value;
      }
    }

    public MethodDefinition InvokeMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.invoke_method = value;
      }
    }

    public MethodDefinition RemoveMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.remove_method = value;
      }
    }

    public bool HasOtherMethods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<MethodDefinition> OtherMethods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntimeSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeDefinition DeclaringType
    {
      get
      {
        return (TypeDefinition) base.DeclaringType;
      }
      set
      {
        this.DeclaringType = (TypeReference) value;
      }
    }

    public override bool IsDefinition
    {
      get
      {
        return true;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    public override EventDefinition Resolve()
    {
      return this;
    }
  }
}
