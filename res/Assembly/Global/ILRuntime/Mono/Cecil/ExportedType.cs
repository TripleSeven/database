﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ExportedType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class ExportedType : IMetadataTokenProvider
  {
    private string @namespace;
    private string name;
    private uint attributes;
    private IMetadataScope scope;
    private ModuleDefinition module;
    private int identifier;
    private ExportedType declaring_type;
    internal MetadataToken token;

    [MethodImpl((MethodImplOptions) 32768)]
    public ExportedType(
      string @namespace,
      string name,
      ModuleDefinition module,
      IMetadataScope scope)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Namespace
    {
      get
      {
        return this.@namespace;
      }
      set
      {
        this.@namespace = value;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public TypeAttributes Attributes
    {
      get
      {
        return (TypeAttributes) this.attributes;
      }
      set
      {
        this.attributes = (uint) value;
      }
    }

    public IMetadataScope Scope
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ExportedType DeclaringType
    {
      get
      {
        return this.declaring_type;
      }
      set
      {
        this.declaring_type = value;
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }

    public int Identifier
    {
      get
      {
        return this.identifier;
      }
      set
      {
        this.identifier = value;
      }
    }

    public bool IsNotPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedPrivate
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 3U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedFamily
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 4U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 5U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedFamilyAndAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 6U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNestedFamilyOrAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 7U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoLayout
    {
      get
      {
        return this.attributes.GetMaskedAttributes(24U, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSequentialLayout
    {
      get
      {
        return this.attributes.GetMaskedAttributes(24U, 8U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsExplicitLayout
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsClass
    {
      get
      {
        return this.attributes.GetMaskedAttributes(32U, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsInterface
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAbstract
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSealed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsImport
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSerializable
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAnsiClass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsUnicodeClass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoClass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsBeforeFieldInit
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntimeSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasSecurity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsForwarder
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string ToString()
    {
      return this.FullName;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition Resolve()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal TypeReference CreateReference()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
