﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.FieldAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum FieldAttributes : ushort
  {
    FieldAccessMask = 7,
    CompilerControlled = 0,
    Private = 1,
    FamANDAssem = 2,
    Assembly = FamANDAssem | Private, // 0x0003
    Family = 4,
    FamORAssem = Family | Private, // 0x0005
    Public = Family | FamANDAssem, // 0x0006
    Static = 16, // 0x0010
    InitOnly = 32, // 0x0020
    Literal = 64, // 0x0040
    NotSerialized = 128, // 0x0080
    SpecialName = 512, // 0x0200
    PInvokeImpl = 8192, // 0x2000
    RTSpecialName = 1024, // 0x0400
    HasFieldMarshal = 4096, // 0x1000
    HasDefault = 32768, // 0x8000
    HasFieldRVA = 256, // 0x0100
  }
}
