﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.FixedArrayMarshalInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class FixedArrayMarshalInfo : MarshalInfo
  {
    internal NativeType element_type;
    internal int size;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedArrayMarshalInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public NativeType ElementType
    {
      get
      {
        return this.element_type;
      }
      set
      {
        this.element_type = value;
      }
    }

    public int Size
    {
      get
      {
        return this.size;
      }
      set
      {
        this.size = value;
      }
    }
  }
}
