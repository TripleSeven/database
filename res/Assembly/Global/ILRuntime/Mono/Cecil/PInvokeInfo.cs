﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PInvokeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class PInvokeInfo
  {
    private ushort attributes;
    private string entry_point;
    private ModuleReference module;

    [MethodImpl((MethodImplOptions) 32768)]
    public PInvokeInfo(PInvokeAttributes attributes, string entryPoint, ModuleReference module)
    {
      // ISSUE: unable to decompile the method.
    }

    public PInvokeAttributes Attributes
    {
      get
      {
        return (PInvokeAttributes) this.attributes;
      }
      set
      {
        this.attributes = (ushort) value;
      }
    }

    public string EntryPoint
    {
      get
      {
        return this.entry_point;
      }
      set
      {
        this.entry_point = value;
      }
    }

    public ModuleReference Module
    {
      get
      {
        return this.module;
      }
      set
      {
        this.module = value;
      }
    }

    public bool IsNoMangle
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 1);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCharSetNotSpec
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 6, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCharSetAnsi
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 6, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCharSetUnicode
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 6, 4U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCharSetAuto
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 6, 6U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool SupportsLastError
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 64);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCallConvWinapi
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCallConvCdecl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCallConvStdCall
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCallConvThiscall
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCallConvFastcall
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsBestFitEnabled
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsBestFitDisabled
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsThrowOnUnmappableCharEnabled
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsThrowOnUnmappableCharDisabled
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
