﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MemberDefinitionCollection`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class MemberDefinitionCollection<T> : Collection<T> where T : IMemberDefinition
  {
    private TypeDefinition container;

    internal MemberDefinitionCollection(TypeDefinition container)
    {
      this.container = container;
    }

    internal MemberDefinitionCollection(TypeDefinition container, int capacity)
      : base(capacity)
    {
      this.container = container;
    }

    protected override void OnAdd(T item, int index)
    {
      this.Attach(item);
    }

    protected override sealed void OnSet(T item, int index)
    {
      this.Attach(item);
    }

    protected override sealed void OnInsert(T item, int index)
    {
      this.Attach(item);
    }

    protected override sealed void OnRemove(T item, int index)
    {
      MemberDefinitionCollection<T>.Detach(item);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override sealed void OnClear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Attach(T element)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void Detach(T element)
    {
      element.DeclaringType = (TypeDefinition) null;
    }
  }
}
