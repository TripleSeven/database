﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public class MethodReference : MemberReference, IMethodSignature, IGenericParameterProvider, IGenericContext, IMetadataTokenProvider
  {
    private int hashCode;
    private static int instance_id;
    internal ParameterDefinitionCollection parameters;
    private MethodReturnType return_type;
    private bool has_this;
    private bool explicit_this;
    private MethodCallingConvention calling_convention;
    internal Collection<GenericParameter> generic_parameters;

    [MethodImpl((MethodImplOptions) 32768)]
    internal MethodReference()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodReference(string name, TypeReference returnType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodReference(string name, TypeReference returnType, TypeReference declaringType)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual bool HasThis
    {
      get
      {
        return this.has_this;
      }
      set
      {
        this.has_this = value;
      }
    }

    public virtual bool ExplicitThis
    {
      get
      {
        return this.explicit_this;
      }
      set
      {
        this.explicit_this = value;
      }
    }

    public virtual MethodCallingConvention CallingConvention
    {
      get
      {
        return this.calling_convention;
      }
      set
      {
        this.calling_convention = value;
      }
    }

    public virtual bool HasParameters
    {
      get
      {
        return !this.parameters.IsNullOrEmpty<ParameterDefinition>();
      }
    }

    public virtual Collection<ParameterDefinition> Parameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    IGenericParameterProvider IGenericContext.Type
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    IGenericParameterProvider IGenericContext.Method
    {
      get
      {
        return (IGenericParameterProvider) this;
      }
    }

    GenericParameterType IGenericParameterProvider.GenericParameterType
    {
      get
      {
        return GenericParameterType.Method;
      }
    }

    public virtual bool HasGenericParameters
    {
      get
      {
        return !this.generic_parameters.IsNullOrEmpty<GenericParameter>();
      }
    }

    public virtual Collection<GenericParameter> GenericParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference ReturnType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual MethodReturnType MethodReturnType
    {
      get
      {
        return this.return_type;
      }
      set
      {
        this.return_type = value;
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual bool IsGenericInstance
    {
      get
      {
        return false;
      }
    }

    public override bool ContainsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual MethodReference GetElementMethod()
    {
      return this;
    }

    protected override IMemberDefinition ResolveDefinition()
    {
      return (IMemberDefinition) this.Resolve();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual MethodDefinition Resolve()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
