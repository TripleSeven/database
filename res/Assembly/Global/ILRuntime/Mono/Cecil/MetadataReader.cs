﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MetadataReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Cecil.PE;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class MetadataReader : ByteBuffer
  {
    internal readonly Image image;
    internal readonly ModuleDefinition module;
    internal readonly MetadataSystem metadata;
    internal CodeReader code;
    internal IGenericContext context;
    private readonly MetadataReader metadata_reader;

    [MethodImpl((MethodImplOptions) 32768)]
    public MetadataReader(ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MetadataReader(Image image, ModuleDefinition module, MetadataReader metadata_reader)
    {
      // ISSUE: unable to decompile the method.
    }

    private int GetCodedIndexSize(CodedIndex index)
    {
      return this.image.GetCodedIndexSize(index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private uint ReadByIndexSize(int size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] ReadBlob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] ReadBlob(uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private uint ReadBlobIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetBlobView(uint signature, out byte[] blob, out int index, out int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ReadString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private uint ReadStringIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Guid ReadGuid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private uint ReadTableIndex(Table table)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MetadataToken ReadMetadataToken(CodedIndex index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int MoveTo(Table table)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool MoveTo(Table table, uint row)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssemblyNameDefinition ReadAssemblyNameDefinition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ModuleDefinition Populate(ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeAssemblyReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<AssemblyNameReference> ReadAssemblyReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDefinition ReadEntryPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<ModuleDefinition> ReadModules()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetModuleFileName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeModuleReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<ModuleReference> ReadModuleReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFileResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<Resource> ReadResources()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Row<FileAttributes, string, uint> ReadFileRecord(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] GetManagedResource(uint offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PopulateVersionAndFlags(AssemblyNameReference name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PopulateNameAndCulture(AssemblyNameReference name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinitionCollection ReadTypes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CompleteTypes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeTypeDefinitions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsNested(TypeAttributes attributes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasNestedTypes(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<TypeDefinition> ReadNestedTypes(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeNestedTypes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddNestedMapping(uint declaring, uint nested)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Collection<TValue> AddMapping<TKey, TValue>(
      Dictionary<TKey, Collection<TValue>> cache,
      TKey key,
      TValue value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeDefinition ReadType(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeDefinition GetNestedTypeDeclaringType(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Range ReadListRange(uint current_index, Table current, Table target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Row<short, int> ReadTypeLayout(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeTypeLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference GetTypeDefOrRef(MetadataToken token)
    {
      return (TypeReference) this.LookupToken(token);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition GetTypeDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeDefinition ReadTypeDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeTypeReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference GetTypeReference(string scope, string full_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference GetTypeReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference ReadTypeReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private IMetadataScope GetTypeReferenceScope(MetadataToken scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TypeReference> GetTypeReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference GetTypeSpecification(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    private SignatureReader ReadSignature(uint signature)
    {
      return new SignatureReader(signature, this);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasInterfaces(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public InterfaceImplementationCollection ReadInterfaces(
      TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeInterfaces()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddInterfaceMapping(uint type, Row<uint, MetadataToken> @interface)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<FieldDefinition> ReadFields(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadField(uint field_rid, Collection<FieldDefinition> fields)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFields()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference ReadFieldType(uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadFieldRVA(FieldDefinition field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] GetFieldInitializeValue(int size, uint rva)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetFieldTypeSize(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFieldRVAs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadFieldLayout(FieldDefinition field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFieldLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasEvents(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<EventDefinition> ReadEvents(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadEvent(uint event_rid, Collection<EventDefinition> events)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasProperties(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<PropertyDefinition> ReadProperties(
      TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadProperty(uint property_rid, Collection<PropertyDefinition> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeProperties()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodSemanticsAttributes ReadMethodSemantics(
      MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static EventDefinition GetEvent(TypeDefinition type, MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static PropertyDefinition GetProperty(
      TypeDefinition type,
      MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TMember GetMember<TMember>(Collection<TMember> members, MetadataToken token) where TMember : IMemberDefinition
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethodSemantics()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ReadMethods(PropertyDefinition property)
    {
      this.ReadAllSemantics(property.DeclaringType);
    }

    public void ReadMethods(EventDefinition @event)
    {
      this.ReadAllSemantics(@event.DeclaringType);
    }

    public void ReadAllSemantics(MethodDefinition method)
    {
      this.ReadAllSemantics(method.DeclaringType);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadAllSemantics(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<MethodDefinition> ReadMethods(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadPointers<TMember>(
      Table ptr,
      Table table,
      Range range,
      Collection<TMember> members,
      Action<uint, Collection<TMember>> reader)
      where TMember : IMemberDefinition
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsDeleted(IMemberDefinition member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadMethod(uint method_rid, Collection<MethodDefinition> methods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadParameters(MethodDefinition method, Range param_range)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadParameterPointers(MethodDefinition method, Range range)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadParameter(uint param_rid, MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadMethodSignature(uint signature, IMethodSignature method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PInvokeInfo ReadPInvokeInfo(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializePInvokes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGenericParameters(IGenericParameterProvider provider)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<GenericParameter> ReadGenericParameters(
      IGenericParameterProvider provider)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadGenericParametersRange(
      Range range,
      IGenericParameterProvider provider,
      GenericParameterCollection generic_parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeGenericParameters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Dictionary<MetadataToken, Range[]> InitializeRanges(
      Table table,
      Func<MetadataToken> get_next)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddRange(
      Dictionary<MetadataToken, Range[]> ranges,
      MetadataToken owner,
      Range range)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGenericConstraints(GenericParameter generic_parameter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<TypeReference> ReadGenericConstraints(
      GenericParameter generic_parameter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeGenericConstraints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGenericConstraintMapping(uint generic_parameter, MetadataToken constraint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOverrides(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<MethodReference> ReadOverrides(
      MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeOverrides()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddOverrideMapping(uint method_rid, MetadataToken @override)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodBody ReadMethodBody(MethodDefinition method)
    {
      return this.code.ReadMethodBody(method);
    }

    public int ReadCodeSize(MethodDefinition method)
    {
      return this.code.ReadCodeSize(method);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CallSite ReadCallSite(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public VariableDefinitionCollection ReadVariables(
      MetadataToken local_var_token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMetadataTokenProvider LookupToken(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldDefinition GetFieldDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private FieldDefinition LookupField(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDefinition GetMethodDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodDefinition LookupMethod(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodSpecification GetMethodSpecification(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodSpecification ReadMethodSpecSignature(
      uint signature,
      MethodReference method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MemberReference GetMemberReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MemberReference ReadMemberReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MemberReference ReadTypeMemberReference(
      MetadataToken type,
      string name,
      uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MemberReference ReadMemberReferenceSignature(
      uint signature,
      TypeReference declaring_type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MemberReference ReadMethodMemberReference(
      MetadataToken token,
      string name,
      uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMemberReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<MemberReference> GetMemberReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeConstants()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference ReadConstantSignature(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object ReadConstant(IConstantProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object ReadConstantValue(ElementType etype, uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ReadConstantString(uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object ReadConstantPrimitive(ElementType type, uint signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void InitializeCustomAttributes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasCustomAttributes(ICustomAttributeProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<CustomAttribute> ReadCustomAttributes(
      ICustomAttributeProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadCustomAttributeRange(
      Range range,
      Collection<CustomAttribute> custom_attributes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int RangesSize(Range[] ranges)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<CustomAttribute> GetCustomAttributes()
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] ReadCustomAttributeBlob(uint signature)
    {
      return this.ReadBlob(signature);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadCustomAttributeSignature(CustomAttribute attribute)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMarshalInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasMarshalInfo(IMarshalInfoProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MarshalInfo ReadMarshalInfo(IMarshalInfoProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeSecurityDeclarations()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSecurityDeclarations(ISecurityDeclarationProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<SecurityDeclaration> ReadSecurityDeclarations(
      ISecurityDeclarationProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadSecurityDeclarationRange(
      Range range,
      Collection<SecurityDeclaration> security_declarations)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] ReadSecurityDeclarationBlob(uint signature)
    {
      return this.ReadBlob(signature);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadSecurityDeclarationSignature(SecurityDeclaration declaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadXmlSecurityDeclaration(uint signature, SecurityDeclaration declaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<ExportedType> ReadExportedTypes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private IMetadataScope GetExportedTypeScope(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ModuleReference GetModuleReferenceFromFile(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeDocuments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<SequencePoint> ReadSequencePoints(
      MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Document GetDocument(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeLocalScopes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ScopeDebugInformation ReadScope(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AddScope(
      Collection<ScopeDebugInformation> scopes,
      ScopeDebugInformation scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ScopeDebugInformation ReadLocalScope(
      Row<uint, Range, Range, uint, uint, uint> record)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private VariableDebugInformation ReadLocalVariable(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConstantDebugInformation ReadLocalConstant(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeImportScopes()
    {
      // ISSUE: unable to decompile the method.
    }

    public string ReadUTF8StringBlob(uint signature)
    {
      return this.ReadStringBlob(signature, Encoding.UTF8);
    }

    private string ReadUnicodeStringBlob(uint signature)
    {
      return this.ReadStringBlob(signature, Encoding.Unicode);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ReadStringBlob(uint signature, Encoding encoding)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ImportTarget ReadImportTarget(SignatureReader signature)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeStateMachineMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDefinition ReadStateMachineKickoffMethod(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeCustomDebugInformations()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<CustomDebugInformation> GetCustomDebugInformation(
      ICustomDebugInformationProvider provider)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
