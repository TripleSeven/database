﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Resource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class Resource
  {
    private string name;
    private uint attributes;

    [MethodImpl((MethodImplOptions) 32768)]
    internal Resource(string name, ManifestResourceAttributes attributes)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public ManifestResourceAttributes Attributes
    {
      get
      {
        return (ManifestResourceAttributes) this.attributes;
      }
      set
      {
        this.attributes = (uint) value;
      }
    }

    public abstract ResourceType ResourceType { get; }

    public bool IsPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPrivate
    {
      get
      {
        return this.attributes.GetMaskedAttributes(7U, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
