﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MetadataResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public class MetadataResolver : IMetadataResolver
  {
    private readonly IAssemblyResolver assembly_resolver;

    [MethodImpl((MethodImplOptions) 32768)]
    public MetadataResolver(IAssemblyResolver assemblyResolver)
    {
      // ISSUE: unable to decompile the method.
    }

    public IAssemblyResolver AssemblyResolver
    {
      get
      {
        return this.assembly_resolver;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual TypeDefinition Resolve(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeDefinition GetType(
      ModuleDefinition module,
      TypeReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeDefinition GetTypeDefinition(
      ModuleDefinition module,
      TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual FieldDefinition Resolve(FieldReference field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private FieldDefinition GetField(TypeDefinition type, FieldReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static FieldDefinition GetField(
      Collection<FieldDefinition> fields,
      FieldReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual MethodDefinition Resolve(MethodReference method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MethodDefinition GetMethod(
      TypeDefinition type,
      MethodReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MethodDefinition GetMethod(
      Collection<MethodDefinition> methods,
      MethodReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AreSame(
      Collection<ParameterDefinition> a,
      Collection<ParameterDefinition> b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsVarArgCallTo(MethodDefinition method, MethodReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AreSame(TypeSpecification a, TypeSpecification b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AreSame(ArrayType a, ArrayType b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AreSame(IModifierType a, IModifierType b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AreSame(GenericInstanceType a, GenericInstanceType b)
    {
      // ISSUE: unable to decompile the method.
    }

    private static bool AreSame(GenericParameter a, GenericParameter b)
    {
      return a.Position == b.Position;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AreSame(TypeReference a, TypeReference b)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
