﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.LinkedResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class LinkedResource : Resource
  {
    internal byte[] hash;
    private string file;

    public LinkedResource(string name, ManifestResourceAttributes flags)
      : base(name, flags)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedResource(string name, ManifestResourceAttributes flags, string file)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] Hash
    {
      get
      {
        return this.hash;
      }
    }

    public string File
    {
      get
      {
        return this.file;
      }
      set
      {
        this.file = value;
      }
    }

    public override ResourceType ResourceType
    {
      get
      {
        return ResourceType.Linked;
      }
    }
  }
}
