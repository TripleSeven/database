﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodDefinitionProjection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class MethodDefinitionProjection
  {
    public readonly MethodAttributes Attributes;
    public readonly MethodImplAttributes ImplAttributes;
    public readonly string Name;
    public readonly MethodDefinitionTreatment Treatment;

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDefinitionProjection(MethodDefinition method, MethodDefinitionTreatment treatment)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
