﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ParameterAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum ParameterAttributes : ushort
  {
    None = 0,
    In = 1,
    Out = 2,
    Lcid = 4,
    Retval = 8,
    Optional = 16, // 0x0010
    HasDefault = 4096, // 0x1000
    HasFieldMarshal = 8192, // 0x2000
    Unused = 53216, // 0xCFE0
  }
}
