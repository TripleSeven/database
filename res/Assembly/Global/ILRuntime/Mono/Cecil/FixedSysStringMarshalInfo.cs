﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.FixedSysStringMarshalInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class FixedSysStringMarshalInfo : MarshalInfo
  {
    internal int size;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedSysStringMarshalInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Size
    {
      get
      {
        return this.size;
      }
      set
      {
        this.size = value;
      }
    }
  }
}
