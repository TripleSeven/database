﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Mdb.MethodEntryExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.CompilerServices.SymbolWriter;

namespace ILRuntime.Mono.Cecil.Mdb
{
  internal static class MethodEntryExtensions
  {
    public static bool HasColumnInfo(this MethodEntry entry)
    {
      return (entry.MethodFlags & MethodEntry.Flags.ColumnsInfoIncluded) != (MethodEntry.Flags) 0;
    }

    public static bool HasEndInfo(this MethodEntry entry)
    {
      return (entry.MethodFlags & MethodEntry.Flags.EndInfoIncluded) != (MethodEntry.Flags) 0;
    }
  }
}
