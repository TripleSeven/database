﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Mdb.MdbReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Collections.Generic;
using ILRuntime.Mono.CompilerServices.SymbolWriter;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Mdb
{
  public sealed class MdbReader : ISymbolReader, IDisposable
  {
    private readonly ModuleDefinition module;
    private readonly MonoSymbolFile symbol_file;
    private readonly Dictionary<string, Document> documents;

    [MethodImpl((MethodImplOptions) 32768)]
    public MdbReader(ModuleDefinition module, MonoSymbolFile symFile)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ProcessDebugHeader(ImageDebugHeader header)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDebugInformation Read(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ReadCodeSize(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ReadLocalVariables(MethodEntry entry, ScopeDebugInformation[] scopes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadLineNumbers(MethodEntry entry, MethodDebugInformation info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Document GetDocument(SourceFileEntry file)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ScopeDebugInformation[] ReadScopes(
      MethodEntry entry,
      MethodDebugInformation info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool AddScope(
      Collection<ScopeDebugInformation> scopes,
      ScopeDebugInformation scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private SequencePoint LineToSequencePoint(LineNumberEntry line)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Dispose()
    {
      this.symbol_file.Dispose();
    }
  }
}
