﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.EventAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum EventAttributes : ushort
  {
    None = 0,
    SpecialName = 512, // 0x0200
    RTSpecialName = 1024, // 0x0400
  }
}
