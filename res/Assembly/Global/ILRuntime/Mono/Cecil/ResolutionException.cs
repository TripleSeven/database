﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ResolutionException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace ILRuntime.Mono.Cecil
{
  [Serializable]
  public sealed class ResolutionException : Exception
  {
    private readonly MemberReference member;

    [MethodImpl((MethodImplOptions) 32768)]
    public ResolutionException(MemberReference member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ResolutionException(MemberReference member, Exception innerException)
    {
      // ISSUE: unable to decompile the method.
    }

    private ResolutionException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }

    public MemberReference Member
    {
      get
      {
        return this.member;
      }
    }

    public IMetadataScope Scope
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
