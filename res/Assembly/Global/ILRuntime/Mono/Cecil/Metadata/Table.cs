﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.Table
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal enum Table : byte
  {
    Module = 0,
    TypeRef = 1,
    TypeDef = 2,
    FieldPtr = 3,
    Field = 4,
    MethodPtr = 5,
    Method = 6,
    ParamPtr = 7,
    Param = 8,
    InterfaceImpl = 9,
    MemberRef = 10, // 0x0A
    Constant = 11, // 0x0B
    CustomAttribute = 12, // 0x0C
    FieldMarshal = 13, // 0x0D
    DeclSecurity = 14, // 0x0E
    ClassLayout = 15, // 0x0F
    FieldLayout = 16, // 0x10
    StandAloneSig = 17, // 0x11
    EventMap = 18, // 0x12
    EventPtr = 19, // 0x13
    Event = 20, // 0x14
    PropertyMap = 21, // 0x15
    PropertyPtr = 22, // 0x16
    Property = 23, // 0x17
    MethodSemantics = 24, // 0x18
    MethodImpl = 25, // 0x19
    ModuleRef = 26, // 0x1A
    TypeSpec = 27, // 0x1B
    ImplMap = 28, // 0x1C
    FieldRVA = 29, // 0x1D
    EncLog = 30, // 0x1E
    EncMap = 31, // 0x1F
    Assembly = 32, // 0x20
    AssemblyProcessor = 33, // 0x21
    AssemblyOS = 34, // 0x22
    AssemblyRef = 35, // 0x23
    AssemblyRefProcessor = 36, // 0x24
    AssemblyRefOS = 37, // 0x25
    File = 38, // 0x26
    ExportedType = 39, // 0x27
    ManifestResource = 40, // 0x28
    NestedClass = 41, // 0x29
    GenericParam = 42, // 0x2A
    MethodSpec = 43, // 0x2B
    GenericParamConstraint = 44, // 0x2C
    Document = 48, // 0x30
    MethodDebugInformation = 49, // 0x31
    LocalScope = 50, // 0x32
    LocalVariable = 51, // 0x33
    LocalConstant = 52, // 0x34
    ImportScope = 53, // 0x35
    StateMachineMethod = 54, // 0x36
    CustomDebugInformation = 55, // 0x37
  }
}
