﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.Row`6
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal struct Row<T1, T2, T3, T4, T5, T6>
  {
    internal T1 Col1;
    internal T2 Col2;
    internal T3 Col3;
    internal T4 Col4;
    internal T5 Col5;
    internal T6 Col6;

    [MethodImpl((MethodImplOptions) 32768)]
    public Row(T1 col1, T2 col2, T3 col3, T4 col4, T5 col5, T6 col6)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
