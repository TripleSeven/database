﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.Row`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal struct Row<T1, T2>
  {
    internal T1 Col1;
    internal T2 Col2;

    public Row(T1 col1, T2 col2)
    {
      this.Col1 = col1;
      this.Col2 = col2;
    }
  }
}
