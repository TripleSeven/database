﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.BlobHeap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal sealed class BlobHeap : Heap
  {
    public BlobHeap(byte[] data)
      : base(data)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] Read(uint index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetView(uint signature, out byte[] buffer, out int index, out int length)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
