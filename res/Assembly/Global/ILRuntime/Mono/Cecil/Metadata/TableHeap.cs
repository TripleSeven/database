﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.TableHeap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal sealed class TableHeap : Heap
  {
    public long Valid;
    public long Sorted;
    public readonly TableInformation[] Tables;

    [MethodImpl((MethodImplOptions) 32768)]
    public TableHeap(byte[] data)
    {
      // ISSUE: unable to decompile the method.
    }

    public TableInformation this[Table table]
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasTable(Table table)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
