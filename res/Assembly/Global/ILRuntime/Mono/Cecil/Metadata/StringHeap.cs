﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.StringHeap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal class StringHeap : Heap
  {
    private readonly Dictionary<uint, string> strings;

    [MethodImpl((MethodImplOptions) 32768)]
    public StringHeap(byte[] data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string Read(uint index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual string ReadStringAt(uint index)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
