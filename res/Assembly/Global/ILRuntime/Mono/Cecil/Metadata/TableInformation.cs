﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Metadata.TableInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil.Metadata
{
  internal struct TableInformation
  {
    public uint Offset;
    public uint Length;
    public uint RowSize;

    public bool IsLarge
    {
      get
      {
        return this.Length > (uint) ushort.MaxValue;
      }
    }
  }
}
