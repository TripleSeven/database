﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.IMethodSignature
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;

namespace ILRuntime.Mono.Cecil
{
  public interface IMethodSignature : IMetadataTokenProvider
  {
    bool HasThis { get; set; }

    bool ExplicitThis { get; set; }

    MethodCallingConvention CallingConvention { get; set; }

    bool HasParameters { get; }

    Collection<ParameterDefinition> Parameters { get; }

    TypeReference ReturnType { get; set; }

    MethodReturnType MethodReturnType { get; }
  }
}
