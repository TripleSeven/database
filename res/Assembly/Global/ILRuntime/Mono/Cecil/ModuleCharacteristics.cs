﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ModuleCharacteristics
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum ModuleCharacteristics
  {
    HighEntropyVA = 32, // 0x00000020
    DynamicBase = 64, // 0x00000040
    NoSEH = 1024, // 0x00000400
    NXCompat = 256, // 0x00000100
    AppContainer = 4096, // 0x00001000
    TerminalServerAware = 32768, // 0x00008000
  }
}
