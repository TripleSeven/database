﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.CustomAttributeNamedArgument
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public struct CustomAttributeNamedArgument
  {
    private readonly string name;
    private readonly CustomAttributeArgument argument;

    [MethodImpl((MethodImplOptions) 32768)]
    public CustomAttributeNamedArgument(string name, CustomAttributeArgument argument)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public CustomAttributeArgument Argument
    {
      get
      {
        return this.argument;
      }
    }
  }
}
