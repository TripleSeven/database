﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ModuleDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Cecil.PE;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class ModuleDefinition : ModuleReference, ICustomAttributeProvider, ICustomDebugInformationProvider, IDisposable, IMetadataTokenProvider
  {
    internal Image Image;
    internal MetadataSystem MetadataSystem;
    internal ReadingMode ReadingMode;
    internal ISymbolReaderProvider SymbolReaderProvider;
    internal ISymbolReader symbol_reader;
    internal Disposable<IAssemblyResolver> assembly_resolver;
    internal IMetadataResolver metadata_resolver;
    internal TypeSystem type_system;
    internal readonly MetadataReader reader;
    private readonly string file_name;
    internal string runtime_version;
    internal ModuleKind kind;
    private WindowsRuntimeProjections projections;
    private MetadataKind metadata_kind;
    private TargetRuntime runtime;
    private TargetArchitecture architecture;
    private ModuleAttributes attributes;
    private ModuleCharacteristics characteristics;
    internal ushort linker_version;
    private Guid mvid;
    internal uint timestamp;
    internal AssemblyDefinition assembly;
    private MethodDefinition entry_point;
    private Collection<CustomAttribute> custom_attributes;
    private Collection<AssemblyNameReference> references;
    private Collection<ModuleReference> modules;
    private Collection<Resource> resources;
    private Collection<ExportedType> exported_types;
    private TypeDefinitionCollection types;
    internal Collection<CustomDebugInformation> custom_infos;
    private readonly object module_lock;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ModuleDefinition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal ModuleDefinition(Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMain
    {
      get
      {
        return this.kind != ModuleKind.NetModule;
      }
    }

    public ModuleKind Kind
    {
      get
      {
        return this.kind;
      }
      set
      {
        this.kind = value;
      }
    }

    public MetadataKind MetadataKind
    {
      get
      {
        return this.metadata_kind;
      }
      set
      {
        this.metadata_kind = value;
      }
    }

    internal WindowsRuntimeProjections Projections
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TargetRuntime Runtime
    {
      get
      {
        return this.runtime;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string RuntimeVersion
    {
      get
      {
        return this.runtime_version;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TargetArchitecture Architecture
    {
      get
      {
        return this.architecture;
      }
      set
      {
        this.architecture = value;
      }
    }

    public ModuleAttributes Attributes
    {
      get
      {
        return this.attributes;
      }
      set
      {
        this.attributes = value;
      }
    }

    public ModuleCharacteristics Characteristics
    {
      get
      {
        return this.characteristics;
      }
      set
      {
        this.characteristics = value;
      }
    }

    [Obsolete("Use FileName")]
    public string FullyQualifiedName
    {
      get
      {
        return this.file_name;
      }
    }

    public string FileName
    {
      get
      {
        return this.file_name;
      }
    }

    public Guid Mvid
    {
      get
      {
        return this.mvid;
      }
      set
      {
        this.mvid = value;
      }
    }

    internal bool HasImage
    {
      get
      {
        return this.Image != null;
      }
    }

    public bool HasSymbols
    {
      get
      {
        return this.symbol_reader != null;
      }
    }

    public ISymbolReader SymbolReader
    {
      get
      {
        return this.symbol_reader;
      }
    }

    public override MetadataScopeType MetadataScopeType
    {
      get
      {
        return MetadataScopeType.ModuleDefinition;
      }
    }

    public AssemblyDefinition Assembly
    {
      get
      {
        return this.assembly;
      }
    }

    public IAssemblyResolver AssemblyResolver
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IMetadataResolver MetadataResolver
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeSystem TypeSystem
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasAssemblyReferences
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<AssemblyNameReference> AssemblyReferences
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasModuleReferences
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<ModuleReference> ModuleReferences
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasResources
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<Resource> Resources
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<TypeDefinition> Types
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasExportedTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<ExportedType> ExportedTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodDefinition EntryPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.entry_point = value;
      }
    }

    public bool HasCustomDebugInformations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomDebugInformation> CustomDebugInformations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasTypeReference(string fullName)
    {
      return this.HasTypeReference(string.Empty, fullName);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasTypeReference(string scope, string fullName)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool TryGetTypeReference(string fullName, out TypeReference type)
    {
      return this.TryGetTypeReference(string.Empty, fullName, out type);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetTypeReference(string scope, string fullName, out TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference GetTypeReference(string scope, string fullname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TypeReference> GetTypeReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<MemberReference> GetMemberReferences()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<CustomAttribute> GetCustomAttributes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference GetType(string fullName, bool runtimeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition GetType(string fullName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition GetType(string @namespace, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<TypeDefinition> GetTypes()
    {
      return ModuleDefinition.GetTypes(this.Types);
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerable<TypeDefinition> GetTypes(
      Collection<TypeDefinition> types)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeDefinition GetNestedType(string fullname)
    {
      // ISSUE: unable to decompile the method.
    }

    internal FieldDefinition Resolve(FieldReference field)
    {
      return this.MetadataResolver.Resolve(field);
    }

    internal MethodDefinition Resolve(MethodReference method)
    {
      return this.MetadataResolver.Resolve(method);
    }

    internal TypeDefinition Resolve(TypeReference type)
    {
      return this.MetadataResolver.Resolve(type);
    }

    public IMetadataTokenProvider LookupToken(int token)
    {
      return this.LookupToken(new MetadataToken((uint) token));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMetadataTokenProvider LookupToken(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    internal object SyncRoot
    {
      get
      {
        return this.module_lock;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void Read<TItem>(TItem item, Action<TItem, MetadataReader> read)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal TRet Read<TItem, TRet>(TItem item, Func<TItem, MetadataReader, TRet> read)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal TRet Read<TItem, TRet>(
      ref TRet variable,
      TItem item,
      Func<TItem, MetadataReader, TRet> read)
      where TRet : class
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasDebugHeader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ImageDebugHeader GetDebugHeader()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadSymbols()
    {
      // ISSUE: unable to decompile the method.
    }

    public void ReadSymbols(ISymbolReader reader)
    {
      this.ReadSymbols(reader, true);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadSymbols(ISymbolReader reader, bool throwIfSymbolsAreNotMaching)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ModuleDefinition ReadModule(string fileName)
    {
      return ModuleDefinition.ReadModule(fileName, new ReaderParameters(ReadingMode.Deferred));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ModuleDefinition ReadModule(
      string fileName,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Stream GetFileStream(
      string fileName,
      FileMode mode,
      FileAccess access,
      FileShare share)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ModuleDefinition ReadModule(Stream stream)
    {
      return ModuleDefinition.ReadModule(stream, new ReaderParameters(ReadingMode.Deferred));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ModuleDefinition ReadModule(
      Stream stream,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ModuleDefinition ReadModule(
      Disposable<Stream> stream,
      string fileName,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
