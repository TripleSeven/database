﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MetadataType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum MetadataType : byte
  {
    Void = 1,
    Boolean = 2,
    Char = 3,
    SByte = 4,
    Byte = 5,
    Int16 = 6,
    UInt16 = 7,
    Int32 = 8,
    UInt32 = 9,
    Int64 = 10, // 0x0A
    UInt64 = 11, // 0x0B
    Single = 12, // 0x0C
    Double = 13, // 0x0D
    String = 14, // 0x0E
    Pointer = 15, // 0x0F
    ByReference = 16, // 0x10
    ValueType = 17, // 0x11
    Class = 18, // 0x12
    Var = 19, // 0x13
    Array = 20, // 0x14
    GenericInstance = 21, // 0x15
    TypedByReference = 22, // 0x16
    IntPtr = 24, // 0x18
    UIntPtr = 25, // 0x19
    FunctionPointer = 27, // 0x1B
    Object = 28, // 0x1C
    MVar = 30, // 0x1E
    RequiredModifier = 31, // 0x1F
    OptionalModifier = 32, // 0x20
    Sentinel = 65, // 0x41
    Pinned = 69, // 0x45
  }
}
