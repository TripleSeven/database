﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ModuleReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public class ModuleReference : IMetadataScope, IMetadataTokenProvider
  {
    private string name;
    internal MetadataToken token;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ModuleReference()
    {
      // ISSUE: unable to decompile the method.
    }

    public ModuleReference(string name)
      : this()
    {
      this.name = name;
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public virtual MetadataScopeType MetadataScopeType
    {
      get
      {
        return MetadataScopeType.ModuleReference;
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }

    public override string ToString()
    {
      return this.name;
    }
  }
}
