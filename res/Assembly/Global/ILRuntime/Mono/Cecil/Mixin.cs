﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Mixin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace ILRuntime.Mono.Cecil
{
  internal static class Mixin
  {
    public const int TableCount = 58;
    public const int CodedIndexCount = 14;
    public static Version ZeroVersion;
    public const int NotResolvedMarker = -2;
    public const int NoDataMarker = -1;
    internal static object NoValue;
    internal static object NotResolved;
    public const string mscorlib = "mscorlib";
    public const string system_runtime = "System.Runtime";
    public const string system_private_corelib = "System.Private.CoreLib";
    public const string netstandard = "netstandard";

    public static ImageDebugHeaderEntry GetCodeViewEntry(
      this ImageDebugHeader header)
    {
      return header.GetEntry(ImageDebugType.CodeView);
    }

    public static ImageDebugHeaderEntry GetDeterministicEntry(
      this ImageDebugHeader header)
    {
      return header.GetEntry(ImageDebugType.Deterministic);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ImageDebugHeader AddDeterministicEntry(
      this ImageDebugHeader header)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ImageDebugHeaderEntry GetEmbeddedPortablePdbEntry(
      this ImageDebugHeader header)
    {
      return header.GetEntry(ImageDebugType.EmbeddedPortablePdb);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ImageDebugHeaderEntry GetEntry(
      this ImageDebugHeader header,
      ImageDebugType type)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetPdbFileName(string assemblyFileName)
    {
      return Path.ChangeExtension(assemblyFileName, ".pdb");
    }

    public static string GetMdbFileName(string assemblyFileName)
    {
      return assemblyFileName + ".mdb";
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsPortablePdb(string fileName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsPortablePdb(Stream stream)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint ReadCompressedUInt32(this byte[] data, ref int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MetadataToken GetMetadataToken(this CodedIndex self, uint data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetSize(this CodedIndex self, Func<Table, int> counter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Version CheckVersion(Version version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetUniqueDocument(this MethodDebugInformation info, out Document document)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ResolveConstant(
      this IConstantProvider self,
      ref object constant,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetHasCustomAttributes(
      this ICustomAttributeProvider self,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Collection<CustomAttribute> GetCustomAttributes(
      this ICustomAttributeProvider self,
      ref Collection<CustomAttribute> variable,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool ContainsGenericParameter(this IGenericInstance self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GenericInstanceFullName(this IGenericInstance self, StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetHasGenericParameters(
      this IGenericParameterProvider self,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Collection<GenericParameter> GetGenericParameters(
      this IGenericParameterProvider self,
      ref Collection<GenericParameter> collection,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetHasMarshalInfo(this IMarshalInfoProvider self, ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MarshalInfo GetMarshalInfo(
      this IMarshalInfoProvider self,
      ref MarshalInfo variable,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool GetAttributes(this uint self, uint attributes)
    {
      return ((int) self & (int) attributes) != 0;
    }

    public static uint SetAttributes(this uint self, uint attributes, bool value)
    {
      if (value)
        return self | attributes;
      return self & ~attributes;
    }

    public static bool GetMaskedAttributes(this uint self, uint mask, uint attributes)
    {
      return ((int) self & (int) mask) == (int) attributes;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint SetMaskedAttributes(this uint self, uint mask, uint attributes, bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool GetAttributes(this ushort self, ushort attributes)
    {
      return ((int) self & (int) attributes) != 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ushort SetAttributes(this ushort self, ushort attributes, bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool GetMaskedAttributes(this ushort self, ushort mask, uint attributes)
    {
      return (long) ((int) self & (int) mask) == (long) attributes;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ushort SetMaskedAttributes(
      this ushort self,
      ushort mask,
      uint attributes,
      bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HasImplicitThis(this IMethodSignature self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void MethodSignatureFullName(this IMethodSignature self, StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckModule(ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetAssemblyNameReference(
      this ModuleDefinition module,
      AssemblyNameReference name_reference,
      out AssemblyNameReference assembly_reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Equals(byte[] a, byte[] b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Equals<T>(T a, T b) where T : class, IEquatable<T>
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Equals(AssemblyNameReference a, AssemblyNameReference b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ParameterDefinition GetParameter(
      this MethodBody self,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static VariableDefinition GetVariable(this MethodBody self, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetSemantics(this MethodDefinition self, MethodSemanticsAttributes semantics)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetSemantics(
      this MethodDefinition self,
      MethodSemanticsAttributes semantics,
      bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsVarArg(this IMethodSignature self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetSentinelPosition(this IMethodSignature self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckName(object name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckFileName(string fileName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckFullName(string fullName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckStream(object stream)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckWriteSeek(Stream stream)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckReadSeek(Stream stream)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckType(object type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckType(object type, Mixin.Argument argument)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckField(object field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckMethod(object method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckParameters(object parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint GetTimestamp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HasImage(this ModuleDefinition self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetFileName(this Stream self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CopyTo(this Stream self, Stream target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TargetRuntime ParseRuntime(this string self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RuntimeVersionString(this TargetRuntime runtime)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsWindowsMetadata(this ModuleDefinition module)
    {
      return module.MetadataKind != MetadataKind.Ecma335;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static byte[] ReadAll(this Stream self)
    {
      // ISSUE: unable to decompile the method.
    }

    public static void Read(object o)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetHasSecurityDeclarations(
      this ISecurityDeclarationProvider self,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Collection<SecurityDeclaration> GetSecurityDeclarations(
      this ISecurityDeclarationProvider self,
      ref Collection<SecurityDeclaration> variable,
      ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeReference GetEnumUnderlyingType(this TypeDefinition self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeDefinition GetNestedType(
      this TypeDefinition self,
      string fullname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsPrimitive(this ElementType self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TypeFullName(this TypeReference self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsTypeOf(this TypeReference self, string @namespace, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsTypeSpecification(this TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeDefinition CheckedResolve(this TypeReference self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetCoreLibraryReference(
      this ModuleDefinition module,
      out AssemblyNameReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCoreLibrary(this ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void KnownValueType(this TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsCoreLibrary(AssemblyNameReference reference)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsNullOrEmpty<T>(this T[] self)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsNullOrEmpty<T>(this Collection<T> self)
    {
      // ISSUE: unable to decompile the method.
    }

    public static T[] Resize<T>(this T[] self, int length)
    {
      Array.Resize<T>(ref self, length);
      return self;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T[] Add<T>(this T[] self, T item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static Mixin()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum Argument
    {
      name,
      fileName,
      fullName,
      stream,
      type,
      method,
      field,
      parameters,
      module,
      modifierType,
      eventType,
      fieldType,
      declaringType,
      returnType,
      propertyType,
      interfaceType,
    }
  }
}
