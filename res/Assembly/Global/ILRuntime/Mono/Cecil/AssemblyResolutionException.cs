﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyResolutionException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace ILRuntime.Mono.Cecil
{
  [Serializable]
  public sealed class AssemblyResolutionException : FileNotFoundException
  {
    private readonly AssemblyNameReference reference;

    public AssemblyResolutionException(AssemblyNameReference reference)
      : this(reference, (Exception) null)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssemblyResolutionException(AssemblyNameReference reference, Exception innerException)
    {
      // ISSUE: unable to decompile the method.
    }

    private AssemblyResolutionException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }

    public AssemblyNameReference AssemblyReference
    {
      get
      {
        return this.reference;
      }
    }
  }
}
