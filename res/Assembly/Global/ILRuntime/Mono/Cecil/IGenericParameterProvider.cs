﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.IGenericParameterProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;

namespace ILRuntime.Mono.Cecil
{
  public interface IGenericParameterProvider : IMetadataTokenProvider
  {
    bool HasGenericParameters { get; }

    bool IsDefinition { get; }

    ModuleDefinition Module { get; }

    Collection<GenericParameter> GenericParameters { get; }

    GenericParameterType GenericParameterType { get; }
  }
}
