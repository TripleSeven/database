﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.GenericInstanceType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class GenericInstanceType : TypeSpecification, IGenericInstance, IGenericContext, IMetadataTokenProvider
  {
    private Collection<TypeReference> arguments;

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericInstanceType(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasGenericArguments
    {
      get
      {
        return !this.arguments.IsNullOrEmpty<TypeReference>();
      }
    }

    public Collection<TypeReference> GenericArguments
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override TypeReference DeclaringType
    {
      get
      {
        return this.ElementType.DeclaringType;
      }
      set
      {
        throw new NotSupportedException();
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsGenericInstance
    {
      get
      {
        return true;
      }
    }

    public override bool ContainsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    IGenericParameterProvider IGenericContext.Type
    {
      get
      {
        return (IGenericParameterProvider) this.ElementType;
      }
    }
  }
}
