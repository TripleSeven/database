﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeSystem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Metadata;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class TypeSystem
  {
    private readonly ModuleDefinition module;
    private TypeReference type_object;
    private TypeReference type_void;
    private TypeReference type_bool;
    private TypeReference type_char;
    private TypeReference type_sbyte;
    private TypeReference type_byte;
    private TypeReference type_int16;
    private TypeReference type_uint16;
    private TypeReference type_int32;
    private TypeReference type_uint32;
    private TypeReference type_int64;
    private TypeReference type_uint64;
    private TypeReference type_single;
    private TypeReference type_double;
    private TypeReference type_intptr;
    private TypeReference type_uintptr;
    private TypeReference type_string;
    private TypeReference type_typedref;

    private TypeSystem(ModuleDefinition module)
    {
      this.module = module;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static TypeSystem CreateTypeSystem(ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    internal abstract TypeReference LookupType(string @namespace, string name);

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference LookupSystemType(
      ref TypeReference reference,
      string name,
      ElementType element_type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference LookupSystemValueType(
      ref TypeReference typeRef,
      string name,
      ElementType element_type)
    {
      // ISSUE: unable to decompile the method.
    }

    [Obsolete("Use CoreLibrary")]
    public IMetadataScope Corlib
    {
      get
      {
        return this.CoreLibrary;
      }
    }

    public IMetadataScope CoreLibrary
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Object
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Void
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Boolean
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Char
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference SByte
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Byte
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Int16
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference UInt16
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Int32
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference UInt32
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Int64
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference UInt64
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Single
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference Double
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference IntPtr
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference UIntPtr
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference String
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference TypedReference
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private sealed class CoreTypeSystem : TypeSystem
    {
      public CoreTypeSystem(ModuleDefinition module)
        : base(module)
      {
      }

      [MethodImpl((MethodImplOptions) 32768)]
      internal override TypeReference LookupType(string @namespace, string name)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private TypeReference LookupTypeDefinition(string @namespace, string name)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private TypeReference LookupTypeForwarded(string @namespace, string name)
      {
        // ISSUE: unable to decompile the method.
      }

      private static void Initialize(object obj)
      {
      }
    }

    private sealed class CommonTypeSystem : TypeSystem
    {
      private AssemblyNameReference core_library;

      public CommonTypeSystem(ModuleDefinition module)
        : base(module)
      {
      }

      internal override TypeReference LookupType(string @namespace, string name)
      {
        return this.CreateTypeReference(@namespace, name);
      }

      public AssemblyNameReference GetCoreLibraryReference()
      {
        if (this.core_library != null || this.module.TryGetCoreLibraryReference(out this.core_library))
          return this.core_library;
        this.core_library = new AssemblyNameReference()
        {
          Name = "mscorlib",
          Version = this.GetCorlibVersion(),
          PublicKeyToken = new byte[8]
          {
            (byte) 183,
            (byte) 122,
            (byte) 92,
            (byte) 86,
            (byte) 25,
            (byte) 52,
            (byte) 224,
            (byte) 137
          }
        };
        this.module.AssemblyReferences.Add(this.core_library);
        return this.core_library;
      }

      private Version GetCorlibVersion()
      {
        switch (this.module.Runtime)
        {
          case TargetRuntime.Net_1_0:
          case TargetRuntime.Net_1_1:
            return new Version(1, 0, 0, 0);
          case TargetRuntime.Net_2_0:
            return new Version(2, 0, 0, 0);
          case TargetRuntime.Net_4_0:
            return new Version(4, 0, 0, 0);
          default:
            throw new NotSupportedException();
        }
      }

      private TypeReference CreateTypeReference(string @namespace, string name)
      {
        return new TypeReference(@namespace, name, this.module, (IMetadataScope) this.GetCoreLibraryReference());
      }
    }
  }
}
