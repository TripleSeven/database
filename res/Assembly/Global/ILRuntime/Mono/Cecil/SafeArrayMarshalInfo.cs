﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.SafeArrayMarshalInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class SafeArrayMarshalInfo : MarshalInfo
  {
    internal VariantType element_type;

    [MethodImpl((MethodImplOptions) 32768)]
    public SafeArrayMarshalInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public VariantType ElementType
    {
      get
      {
        return this.element_type;
      }
      set
      {
        this.element_type = value;
      }
    }
  }
}
