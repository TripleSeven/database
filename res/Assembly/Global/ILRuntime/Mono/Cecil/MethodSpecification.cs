﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodSpecification
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class MethodSpecification : MethodReference
  {
    private readonly MethodReference method;

    [MethodImpl((MethodImplOptions) 32768)]
    internal MethodSpecification(MethodReference method)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodReference ElementMethod
    {
      get
      {
        return this.method;
      }
    }

    public override string Name
    {
      get
      {
        return this.method.Name;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override MethodCallingConvention CallingConvention
    {
      get
      {
        return this.method.CallingConvention;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override bool HasThis
    {
      get
      {
        return this.method.HasThis;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override bool ExplicitThis
    {
      get
      {
        return this.method.ExplicitThis;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override MethodReturnType MethodReturnType
    {
      get
      {
        return this.method.MethodReturnType;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override TypeReference DeclaringType
    {
      get
      {
        return this.method.DeclaringType;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override ModuleDefinition Module
    {
      get
      {
        return this.method.Module;
      }
    }

    public override bool HasParameters
    {
      get
      {
        return this.method.HasParameters;
      }
    }

    public override Collection<ParameterDefinition> Parameters
    {
      get
      {
        return this.method.Parameters;
      }
    }

    public override bool ContainsGenericParameter
    {
      get
      {
        return this.method.ContainsGenericParameter;
      }
    }

    public override sealed MethodReference GetElementMethod()
    {
      return this.method.GetElementMethod();
    }
  }
}
