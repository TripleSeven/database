﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyResolveEventArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  public sealed class AssemblyResolveEventArgs : EventArgs
  {
    private readonly AssemblyNameReference reference;

    public AssemblyResolveEventArgs(AssemblyNameReference reference)
    {
      this.reference = reference;
    }

    public AssemblyNameReference AssemblyReference
    {
      get
      {
        return this.reference;
      }
    }
  }
}
