﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PE.DataDirectory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.PE
{
  internal struct DataDirectory
  {
    public readonly uint VirtualAddress;
    public readonly uint Size;

    public DataDirectory(uint rva, uint size)
    {
      this.VirtualAddress = rva;
      this.Size = size;
    }

    public bool IsZero
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
