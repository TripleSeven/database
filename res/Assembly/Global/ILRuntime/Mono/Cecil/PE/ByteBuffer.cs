﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PE.ByteBuffer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.PE
{
  internal class ByteBuffer
  {
    internal byte[] buffer;
    internal int length;
    internal int position;

    [MethodImpl((MethodImplOptions) 32768)]
    public ByteBuffer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ByteBuffer(int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ByteBuffer(byte[] buffer)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Advance(int length)
    {
      this.position += length;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte ReadByte()
    {
      // ISSUE: unable to decompile the method.
    }

    public sbyte ReadSByte()
    {
      return (sbyte) this.ReadByte();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] ReadBytes(int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort ReadUInt16()
    {
      // ISSUE: unable to decompile the method.
    }

    public short ReadInt16()
    {
      return (short) this.ReadUInt16();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public uint ReadUInt32()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ReadInt32()
    {
      return (int) this.ReadUInt32();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong ReadUInt64()
    {
      // ISSUE: unable to decompile the method.
    }

    public long ReadInt64()
    {
      return (long) this.ReadUInt64();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public uint ReadCompressedUInt32()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadCompressedInt32()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float ReadSingle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public double ReadDouble()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
