﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PE.ImageReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Metadata;
using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.PE
{
  internal sealed class ImageReader : BinaryStreamReader
  {
    private readonly Image image;
    private DataDirectory cli;
    private DataDirectory metadata;
    private uint table_heap_offset;

    [MethodImpl((MethodImplOptions) 32768)]
    public ImageReader(Disposable<Stream> stream, string file_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveTo(DataDirectory directory)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadImage()
    {
      // ISSUE: unable to decompile the method.
    }

    private TargetArchitecture ReadArchitecture()
    {
      return (TargetArchitecture) this.ReadUInt16();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ModuleKind GetModuleKind(ushort characteristics, ushort subsystem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadOptionalHeaders(
      out ushort subsystem,
      out ushort dll_characteristics,
      out ushort linker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ReadAlignedString(int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ReadZeroTerminatedString(int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadSections(ushort count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadCLIHeader()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadMetadata()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadDebugHeader()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadMetadataStream(Section section)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] ReadHeapData(uint offset, uint size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadTableHeap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void SetIndexSize(Heap heap, uint sizes, byte flag)
    {
      // ISSUE: unable to decompile the method.
    }

    private int GetTableIndexSize(Table table)
    {
      return this.image.GetTableIndexSize(table);
    }

    private int GetCodedIndexSize(CodedIndex index)
    {
      return this.image.GetCodedIndexSize(index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeTableInformations()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadPdbHeap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Image ReadImage(Disposable<Stream> stream, string file_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Image ReadPortablePdb(Disposable<Stream> stream, string file_name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
