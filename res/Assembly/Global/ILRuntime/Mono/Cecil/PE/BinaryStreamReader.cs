﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PE.BinaryStreamReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.PE
{
  internal class BinaryStreamReader : BinaryReader
  {
    public BinaryStreamReader(Stream stream)
      : base(stream)
    {
    }

    public int Position
    {
      get
      {
        return (int) this.BaseStream.Position;
      }
      set
      {
        this.BaseStream.Position = (long) value;
      }
    }

    public int Length
    {
      get
      {
        return (int) this.BaseStream.Length;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Advance(int bytes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MoveTo(uint position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Align(int align)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DataDirectory ReadDataDirectory()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
