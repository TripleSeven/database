﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PE.Image
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Cecil.Metadata;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.PE
{
  internal sealed class Image : IDisposable
  {
    public Disposable<System.IO.Stream> Stream;
    public string FileName;
    public ModuleKind Kind;
    public string RuntimeVersion;
    public TargetArchitecture Architecture;
    public ModuleCharacteristics Characteristics;
    public ushort LinkerVersion;
    public ImageDebugHeader DebugHeader;
    public Section[] Sections;
    public Section MetadataSection;
    public uint EntryPointToken;
    public uint Timestamp;
    public ModuleAttributes Attributes;
    public DataDirectory Win32Resources;
    public DataDirectory Debug;
    public DataDirectory Resources;
    public DataDirectory StrongName;
    public StringHeap StringHeap;
    public BlobHeap BlobHeap;
    public UserStringHeap UserStringHeap;
    public GuidHeap GuidHeap;
    public TableHeap TableHeap;
    public PdbHeap PdbHeap;
    private readonly int[] coded_index_sizes;
    private readonly Func<Table, int> counter;

    [MethodImpl((MethodImplOptions) 32768)]
    public Image()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasTable(Table table)
    {
      return this.GetTableLength(table) > 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTableLength(Table table)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTableIndexSize(Table table)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCodedIndexSize(CodedIndex coded_index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public uint ResolveVirtualAddress(uint rva)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public uint ResolveVirtualAddressInSection(uint rva, Section section)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Section GetSection(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Section GetSectionAtVirtualAddress(uint rva)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BinaryStreamReader GetReaderAt(uint rva)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TRet GetReaderAt<TItem, TRet>(
      uint rva,
      TItem item,
      Func<TItem, BinaryStreamReader, TRet> read)
      where TRet : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasDebugTables()
    {
      // ISSUE: unable to decompile the method.
    }

    public void Dispose()
    {
      this.Stream.Dispose();
    }
  }
}
