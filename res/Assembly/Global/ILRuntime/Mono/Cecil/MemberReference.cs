﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MemberReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class MemberReference : IMetadataTokenProvider
  {
    private string name;
    private TypeReference declaring_type;
    internal MetadataToken token;
    internal object projection;

    internal MemberReference()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal MemberReference(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual string Name
    {
      get
      {
        return this.name;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public abstract string FullName { get; }

    public virtual TypeReference DeclaringType
    {
      get
      {
        return this.declaring_type;
      }
      set
      {
        this.declaring_type = value;
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }

    public bool IsWindowsRuntimeProjection
    {
      get
      {
        return this.projection != null;
      }
    }

    internal MemberReferenceProjection WindowsRuntimeProjection
    {
      get
      {
        return (MemberReferenceProjection) this.projection;
      }
      set
      {
        this.projection = (object) value;
      }
    }

    internal bool HasImage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual ModuleDefinition Module
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public virtual bool IsDefinition
    {
      get
      {
        return false;
      }
    }

    public virtual bool ContainsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal string MemberFullName()
    {
      // ISSUE: unable to decompile the method.
    }

    public IMemberDefinition Resolve()
    {
      return this.ResolveDefinition();
    }

    protected abstract IMemberDefinition ResolveDefinition();

    public override string ToString()
    {
      return this.FullName;
    }
  }
}
