﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.WindowsRuntimeProjections
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class WindowsRuntimeProjections
  {
    private static readonly Version version;
    private static readonly byte[] contract_pk_token;
    private static readonly byte[] contract_pk;
    private static Dictionary<string, WindowsRuntimeProjections.ProjectionInfo> projections;
    private readonly ModuleDefinition module;
    private Version corlib_version;
    private AssemblyNameReference[] virtual_references;

    [MethodImpl((MethodImplOptions) 32768)]
    public WindowsRuntimeProjections(ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    private static Dictionary<string, WindowsRuntimeProjections.ProjectionInfo> Projections
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private AssemblyNameReference[] VirtualReferences
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Project(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeDefinitionTreatment GetWellKnownTypeDefinitionTreatment(
      TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool NeedsWindowsRuntimePrefix(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsClrImplementationType(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyProjection(TypeDefinition type, TypeDefinitionProjection projection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeDefinitionProjection RemoveProjection(
      TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Project(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeReferenceTreatment GetSpecialTypeReferenceTreatment(
      TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsAttribute(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsEnum(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyProjection(TypeReference type, TypeReferenceProjection projection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeReferenceProjection RemoveProjection(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Project(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static MethodDefinitionTreatment GetMethodDefinitionTreatmentFromCustomAttributes(
      MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyProjection(
      MethodDefinition method,
      MethodDefinitionProjection projection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MethodDefinitionProjection RemoveProjection(
      MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Project(FieldDefinition field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyProjection(FieldDefinition field, FieldDefinitionProjection projection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static FieldDefinitionProjection RemoveProjection(
      FieldDefinition field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Project(MemberReference member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool ImplementsRedirectedInterface(MemberReference member, out bool disposable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyProjection(MemberReference member, MemberReferenceProjection projection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MemberReferenceProjection RemoveProjection(
      MemberReference member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddVirtualReferences(Collection<AssemblyNameReference> references)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveVirtualReferences(Collection<AssemblyNameReference> references)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static AssemblyNameReference[] GetAssemblyReferences(
      AssemblyNameReference corlib)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static AssemblyNameReference GetCoreLibrary(
      Collection<AssemblyNameReference> references)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AssemblyNameReference GetAssemblyReference(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Project(ICustomAttributeProvider owner, CustomAttribute attribute)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsWindowsAttributeUsageAttribute(
      ICustomAttributeProvider owner,
      CustomAttribute attribute)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool HasAttribute(TypeDefinition type, string @namespace, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyProjection(
      CustomAttribute attribute,
      CustomAttributeValueProjection projection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CustomAttributeValueProjection RemoveProjection(
      CustomAttribute attribute)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static WindowsRuntimeProjections()
    {
      // ISSUE: unable to decompile the method.
    }

    private struct ProjectionInfo
    {
      public readonly string WinRTNamespace;
      public readonly string ClrNamespace;
      public readonly string ClrName;
      public readonly string ClrAssembly;
      public readonly bool Attribute;
      public readonly bool Disposable;

      [MethodImpl((MethodImplOptions) 32768)]
      public ProjectionInfo(
        string winrt_namespace,
        string clr_namespace,
        string clr_name,
        string clr_assembly,
        bool attribute = false,
        bool disposable = false)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
