﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.SecurityAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  [DebuggerDisplay("{AttributeType}")]
  public sealed class SecurityAttribute : ICustomAttribute
  {
    private TypeReference attribute_type;
    internal Collection<CustomAttributeNamedArgument> fields;
    internal Collection<CustomAttributeNamedArgument> properties;

    public SecurityAttribute(TypeReference attributeType)
    {
      this.attribute_type = attributeType;
    }

    public TypeReference AttributeType
    {
      get
      {
        return this.attribute_type;
      }
      set
      {
        this.attribute_type = value;
      }
    }

    public bool HasFields
    {
      get
      {
        return !this.fields.IsNullOrEmpty<CustomAttributeNamedArgument>();
      }
    }

    public Collection<CustomAttributeNamedArgument> Fields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasProperties
    {
      get
      {
        return !this.properties.IsNullOrEmpty<CustomAttributeNamedArgument>();
      }
    }

    public Collection<CustomAttributeNamedArgument> Properties
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    bool ICustomAttribute.HasConstructorArguments
    {
      get
      {
        return false;
      }
    }

    Collection<CustomAttributeArgument> ICustomAttribute.ConstructorArguments
    {
      get
      {
        throw new NotSupportedException();
      }
    }
  }
}
