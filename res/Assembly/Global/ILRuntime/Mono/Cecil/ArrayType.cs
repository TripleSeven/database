﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ArrayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class ArrayType : TypeSpecification
  {
    private Collection<ArrayDimension> dimensions;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArrayType(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArrayType(TypeReference type, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    public Collection<ArrayDimension> Dimensions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Rank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsVector
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsValueType
    {
      get
      {
        return false;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private string Suffix
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsArray
    {
      get
      {
        return true;
      }
    }
  }
}
