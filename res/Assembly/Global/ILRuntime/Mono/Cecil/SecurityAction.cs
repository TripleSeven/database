﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.SecurityAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum SecurityAction : ushort
  {
    Request = 1,
    Demand = 2,
    Assert = 3,
    Deny = 4,
    PermitOnly = 5,
    LinkDemand = 6,
    InheritDemand = 7,
    RequestMinimum = 8,
    RequestOptional = 9,
    RequestRefuse = 10, // 0x000A
    PreJitGrant = 11, // 0x000B
    PreJitDeny = 12, // 0x000C
    NonCasDemand = 13, // 0x000D
    NonCasLinkDemand = 14, // 0x000E
    NonCasInheritance = 15, // 0x000F
  }
}
