﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodDefinitionTreatment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  internal enum MethodDefinitionTreatment
  {
    None = 0,
    Dispose = 1,
    Abstract = 2,
    Private = 4,
    Public = 8,
    Runtime = 16, // 0x00000010
    InternalCall = 32, // 0x00000020
  }
}
