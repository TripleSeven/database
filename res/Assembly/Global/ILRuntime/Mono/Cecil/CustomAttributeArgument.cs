﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.CustomAttributeArgument
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public struct CustomAttributeArgument
  {
    private readonly TypeReference type;
    private readonly object value;

    [MethodImpl((MethodImplOptions) 32768)]
    public CustomAttributeArgument(TypeReference type, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference Type
    {
      get
      {
        return this.type;
      }
    }

    public object Value
    {
      get
      {
        return this.value;
      }
    }
  }
}
