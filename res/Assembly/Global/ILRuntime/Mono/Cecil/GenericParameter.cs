﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.GenericParameter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class GenericParameter : TypeReference, ICustomAttributeProvider, IMetadataTokenProvider
  {
    internal int position;
    internal GenericParameterType type;
    internal IGenericParameterProvider owner;
    private ushort attributes;
    private Collection<TypeReference> constraints;
    private Collection<CustomAttribute> custom_attributes;

    public GenericParameter(IGenericParameterProvider owner)
      : this(string.Empty, owner)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericParameter(string name, IGenericParameterProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal GenericParameter(int position, GenericParameterType type, ModuleDefinition module)
    {
      // ISSUE: unable to decompile the method.
    }

    public GenericParameterAttributes Attributes
    {
      get
      {
        return (GenericParameterAttributes) this.attributes;
      }
      set
      {
        this.attributes = (ushort) value;
      }
    }

    public int Position
    {
      get
      {
        return this.position;
      }
    }

    public GenericParameterType Type
    {
      get
      {
        return this.type;
      }
    }

    public IGenericParameterProvider Owner
    {
      get
      {
        return this.owner;
      }
    }

    public bool HasConstraints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<TypeReference> Constraints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override IMetadataScope Scope
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override TypeReference DeclaringType
    {
      get
      {
        return this.owner as TypeReference;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public MethodReference DeclaringMethod
    {
      get
      {
        return this.owner as MethodReference;
      }
    }

    public override ModuleDefinition Module
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string Namespace
    {
      get
      {
        return string.Empty;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override string FullName
    {
      get
      {
        return this.Name;
      }
    }

    public override bool IsGenericParameter
    {
      get
      {
        return true;
      }
    }

    public override bool ContainsGenericParameter
    {
      get
      {
        return true;
      }
    }

    public override MetadataType MetadataType
    {
      get
      {
        return (MetadataType) this.etype;
      }
    }

    public bool IsNonVariant
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 3, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCovariant
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 3, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsContravariant
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 3, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasReferenceTypeConstraint
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 4);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasNotNullableValueTypeConstraint
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 8);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDefaultConstructorConstraint
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 16);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ElementType ConvertGenericParameterType(GenericParameterType type)
    {
      // ISSUE: unable to decompile the method.
    }

    public override TypeDefinition Resolve()
    {
      return (TypeDefinition) null;
    }
  }
}
