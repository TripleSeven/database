﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.GenericParameterCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class GenericParameterCollection : Collection<GenericParameter>
  {
    private readonly IGenericParameterProvider owner;

    internal GenericParameterCollection(IGenericParameterProvider owner)
    {
      this.owner = owner;
    }

    internal GenericParameterCollection(IGenericParameterProvider owner, int capacity)
      : base(capacity)
    {
      this.owner = owner;
    }

    protected override void OnAdd(GenericParameter item, int index)
    {
      this.UpdateGenericParameter(item, index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnInsert(GenericParameter item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnSet(GenericParameter item, int index)
    {
      this.UpdateGenericParameter(item, index);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGenericParameter(GenericParameter item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnRemove(GenericParameter item, int index)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
