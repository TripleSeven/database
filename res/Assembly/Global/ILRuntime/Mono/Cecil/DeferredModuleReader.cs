﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.DeferredModuleReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.PE;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class DeferredModuleReader : ModuleReader
  {
    public DeferredModuleReader(Image image)
      : base(image, ReadingMode.Deferred)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ReadModule()
    {
      // ISSUE: unable to decompile the method.
    }

    public override void ReadSymbols(ModuleDefinition module)
    {
    }
  }
}
