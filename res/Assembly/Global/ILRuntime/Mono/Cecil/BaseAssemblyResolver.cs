﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.BaseAssemblyResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class BaseAssemblyResolver : IAssemblyResolver, IDisposable
  {
    private static readonly bool on_mono;
    private readonly Collection<string> directories;
    private Collection<string> gac_paths;

    [MethodImpl((MethodImplOptions) 32768)]
    protected BaseAssemblyResolver()
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddSearchDirectory(string directory)
    {
      this.directories.Add(directory);
    }

    public void RemoveSearchDirectory(string directory)
    {
      this.directories.Remove(directory);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string[] GetSearchDirectories()
    {
      // ISSUE: unable to decompile the method.
    }

    public event AssemblyResolveEventHandler ResolveFailure
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AssemblyDefinition GetAssembly(
      string file,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual AssemblyDefinition Resolve(AssemblyNameReference name)
    {
      return this.Resolve(name, new ReaderParameters());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual AssemblyDefinition Resolve(
      AssemblyNameReference name,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual AssemblyDefinition SearchDirectory(
      AssemblyNameReference name,
      IEnumerable<string> directories,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsZero(Version version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AssemblyDefinition GetCorlib(
      AssemblyNameReference reference,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Collection<string> GetGacPaths()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Collection<string> GetDefaultMonoGacPaths()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetCurrentMonoGac()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AssemblyDefinition GetAssemblyInGac(
      AssemblyNameReference reference,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AssemblyDefinition GetAssemblyInMonoGac(
      AssemblyNameReference reference,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private AssemblyDefinition GetAssemblyInNetGac(
      AssemblyNameReference reference,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetAssemblyFile(
      AssemblyNameReference reference,
      string prefix,
      string gac)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Dispose()
    {
      this.Dispose(true);
      GC.SuppressFinalize((object) this);
    }

    protected virtual void Dispose(bool disposing)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static BaseAssemblyResolver()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
