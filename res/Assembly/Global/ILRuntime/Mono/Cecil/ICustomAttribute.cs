﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ICustomAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;

namespace ILRuntime.Mono.Cecil
{
  public interface ICustomAttribute
  {
    TypeReference AttributeType { get; }

    bool HasFields { get; }

    bool HasProperties { get; }

    bool HasConstructorArguments { get; }

    Collection<CustomAttributeNamedArgument> Fields { get; }

    Collection<CustomAttributeNamedArgument> Properties { get; }

    Collection<CustomAttributeArgument> ConstructorArguments { get; }
  }
}
