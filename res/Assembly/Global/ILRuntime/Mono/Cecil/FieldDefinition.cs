﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.FieldDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class FieldDefinition : FieldReference, IMemberDefinition, IConstantProvider, IMarshalInfoProvider, ICustomAttributeProvider, IMetadataTokenProvider
  {
    private ushort attributes;
    private Collection<CustomAttribute> custom_attributes;
    private int offset;
    internal int rva;
    private byte[] initial_value;
    private object constant;
    private MarshalInfo marshal_info;

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldDefinition(string name, FieldAttributes attributes, TypeReference fieldType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResolveLayout()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasLayoutInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Offset
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.offset = value;
      }
    }

    internal FieldDefinitionProjection WindowsRuntimeProjection
    {
      get
      {
        return (FieldDefinitionProjection) this.projection;
      }
      set
      {
        this.projection = (object) value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResolveRVA()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RVA
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte[] InitialValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.initial_value = value;
        this.rva = 0;
      }
    }

    public FieldAttributes Attributes
    {
      get
      {
        return (FieldAttributes) this.attributes;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasConstant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public object Constant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.constant = value;
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasMarshalInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MarshalInfo MarshalInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.marshal_info = value;
      }
    }

    public bool IsCompilerControlled
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPrivate
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFamilyAndAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 3U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFamily
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 4U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFamilyOrAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 5U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 6U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsStatic
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 16);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsInitOnly
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 32);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsLiteral
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 64);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNotSerialized
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPInvokeImpl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntimeSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDefault
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsDefinition
    {
      get
      {
        return true;
      }
    }

    public TypeDefinition DeclaringType
    {
      get
      {
        return (TypeDefinition) base.DeclaringType;
      }
      set
      {
        this.DeclaringType = (TypeReference) value;
      }
    }

    public override FieldDefinition Resolve()
    {
      return this;
    }
  }
}
