﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.SignatureReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Cecil.PE;
using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class SignatureReader : ByteBuffer
  {
    private readonly MetadataReader reader;
    internal readonly uint start;
    internal readonly uint sig_length;

    [MethodImpl((MethodImplOptions) 32768)]
    public SignatureReader(uint blob, MetadataReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    private TypeSystem TypeSystem
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private MetadataToken ReadTypeTokenSignature()
    {
      return CodedIndex.TypeDefOrRef.GetMetadataToken(this.ReadCompressedUInt32());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GenericParameter GetGenericParameter(
      GenericParameterType type,
      uint var)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GenericParameter GetUnboundGenericParameter(
      GenericParameterType type,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CheckGenericContext(IGenericParameterProvider owner, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadGenericInstanceSignature(
      IGenericParameterProvider provider,
      IGenericInstance instance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ArrayType ReadArrayTypeSignature()
    {
      // ISSUE: unable to decompile the method.
    }

    private TypeReference GetTypeDefOrRef(MetadataToken token)
    {
      return this.reader.GetTypeDefOrRef(token);
    }

    public TypeReference ReadTypeSignature()
    {
      return this.ReadTypeSignature((ElementType) this.ReadByte());
    }

    public TypeReference ReadTypeToken()
    {
      return this.GetTypeDefOrRef(this.ReadTypeTokenSignature());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference ReadTypeSignature(ElementType etype)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadMethodSignature(IMethodSignature method)
    {
      // ISSUE: unable to decompile the method.
    }

    public object ReadConstantSignature(ElementType type)
    {
      return this.ReadPrimitiveValue(type);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadCustomAttributeConstructorArguments(
      CustomAttribute attribute,
      Collection<ParameterDefinition> parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CustomAttributeArgument ReadCustomAttributeFixedArgument(
      TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadCustomAttributeNamedArguments(
      ushort count,
      ref Collection<CustomAttributeNamedArgument> fields,
      ref Collection<CustomAttributeNamedArgument> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadCustomAttributeNamedArgument(
      ref Collection<CustomAttributeNamedArgument> fields,
      ref Collection<CustomAttributeNamedArgument> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Collection<CustomAttributeNamedArgument> GetCustomAttributeNamedArgumentCollection(
      ref Collection<CustomAttributeNamedArgument> collection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CustomAttributeArgument ReadCustomAttributeFixedArrayArgument(
      ArrayType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CustomAttributeArgument ReadCustomAttributeElement(
      TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object ReadCustomAttributeElementValue(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object ReadPrimitiveValue(ElementType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference GetPrimitiveType(ElementType etype)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeReference ReadCustomAttributeFieldOrPropType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference ReadTypeReference()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object ReadCustomAttributeEnum(TypeReference enum_type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SecurityAttribute ReadSecurityAttribute()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MarshalInfo ReadMarshalInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    private NativeType ReadNativeType()
    {
      return (NativeType) this.ReadByte();
    }

    private VariantType ReadVariantType()
    {
      return (VariantType) this.ReadByte();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ReadUTF8String()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string ReadDocumentName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Collection<SequencePoint> ReadSequencePoints(Document document)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanReadMore()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
