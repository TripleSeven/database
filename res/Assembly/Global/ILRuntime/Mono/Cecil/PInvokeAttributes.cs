﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PInvokeAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum PInvokeAttributes : ushort
  {
    NoMangle = 1,
    CharSetMask = 6,
    CharSetNotSpec = 0,
    CharSetAnsi = 2,
    CharSetUnicode = 4,
    CharSetAuto = CharSetUnicode | CharSetAnsi, // 0x0006
    SupportsLastError = 64, // 0x0040
    CallConvMask = 1792, // 0x0700
    CallConvWinapi = 256, // 0x0100
    CallConvCdecl = 512, // 0x0200
    CallConvStdCall = CallConvCdecl | CallConvWinapi, // 0x0300
    CallConvThiscall = 1024, // 0x0400
    CallConvFastcall = CallConvThiscall | CallConvWinapi, // 0x0500
    BestFitMask = 48, // 0x0030
    BestFitEnabled = 16, // 0x0010
    BestFitDisabled = 32, // 0x0020
    ThrowOnUnmappableCharMask = 12288, // 0x3000
    ThrowOnUnmappableCharEnabled = 4096, // 0x1000
    ThrowOnUnmappableCharDisabled = 8192, // 0x2000
  }
}
