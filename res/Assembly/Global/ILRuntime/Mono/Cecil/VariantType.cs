﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.VariantType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum VariantType
  {
    None = 0,
    I2 = 2,
    I4 = 3,
    R4 = 4,
    R8 = 5,
    CY = 6,
    Date = 7,
    BStr = 8,
    Dispatch = 9,
    Error = 10, // 0x0000000A
    Bool = 11, // 0x0000000B
    Variant = 12, // 0x0000000C
    Unknown = 13, // 0x0000000D
    Decimal = 14, // 0x0000000E
    I1 = 16, // 0x00000010
    UI1 = 17, // 0x00000011
    UI2 = 18, // 0x00000012
    UI4 = 19, // 0x00000013
    Int = 22, // 0x00000016
    UInt = 23, // 0x00000017
  }
}
