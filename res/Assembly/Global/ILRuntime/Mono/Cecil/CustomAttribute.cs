﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.CustomAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  [DebuggerDisplay("{AttributeType}")]
  public sealed class CustomAttribute : ICustomAttribute
  {
    internal CustomAttributeValueProjection projection;
    internal readonly uint signature;
    internal bool resolved;
    private MethodReference constructor;
    private byte[] blob;
    internal Collection<CustomAttributeArgument> arguments;
    internal Collection<CustomAttributeNamedArgument> fields;
    internal Collection<CustomAttributeNamedArgument> properties;

    [MethodImpl((MethodImplOptions) 32768)]
    internal CustomAttribute(uint signature, MethodReference constructor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CustomAttribute(MethodReference constructor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CustomAttribute(MethodReference constructor, byte[] blob)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodReference Constructor
    {
      get
      {
        return this.constructor;
      }
      set
      {
        this.constructor = value;
      }
    }

    public TypeReference AttributeType
    {
      get
      {
        return this.constructor.DeclaringType;
      }
    }

    public bool IsResolved
    {
      get
      {
        return this.resolved;
      }
    }

    public bool HasConstructorArguments
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttributeArgument> ConstructorArguments
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasFields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttributeNamedArgument> Fields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasProperties
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttributeNamedArgument> Properties
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    internal bool HasImage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    internal ModuleDefinition Module
    {
      get
      {
        return this.constructor.Module;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] GetBlob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Resolve()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
