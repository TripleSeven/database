﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeSpecification
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class TypeSpecification : TypeReference
  {
    private readonly TypeReference element_type;

    [MethodImpl((MethodImplOptions) 32768)]
    internal TypeSpecification(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference ElementType
    {
      get
      {
        return this.element_type;
      }
    }

    public override string Name
    {
      get
      {
        return this.element_type.Name;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override string Namespace
    {
      get
      {
        return this.element_type.Namespace;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override IMetadataScope Scope
    {
      get
      {
        return this.element_type.Scope;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override ModuleDefinition Module
    {
      get
      {
        return this.element_type.Module;
      }
    }

    public override string FullName
    {
      get
      {
        return this.element_type.FullName;
      }
    }

    public override bool ContainsGenericParameter
    {
      get
      {
        return this.element_type.ContainsGenericParameter;
      }
    }

    public override MetadataType MetadataType
    {
      get
      {
        return (MetadataType) this.etype;
      }
    }

    public override TypeReference GetElementType()
    {
      return this.element_type.GetElementType();
    }
  }
}
