﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.OptionalModifierType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class OptionalModifierType : TypeSpecification, IModifierType
  {
    private TypeReference modifier_type;

    [MethodImpl((MethodImplOptions) 32768)]
    public OptionalModifierType(TypeReference modifierType, TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference ModifierType
    {
      get
      {
        return this.modifier_type;
      }
      set
      {
        this.modifier_type = value;
      }
    }

    public override string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private string Suffix
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsValueType
    {
      get
      {
        return false;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override bool IsOptionalModifier
    {
      get
      {
        return true;
      }
    }

    public override bool ContainsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
