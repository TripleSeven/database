﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.EmbeddedResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class EmbeddedResource : Resource
  {
    private readonly MetadataReader reader;
    private uint? offset;
    private byte[] data;
    private Stream stream;

    [MethodImpl((MethodImplOptions) 32768)]
    public EmbeddedResource(string name, ManifestResourceAttributes attributes, byte[] data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EmbeddedResource(string name, ManifestResourceAttributes attributes, Stream stream)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal EmbeddedResource(
      string name,
      ManifestResourceAttributes attributes,
      uint offset,
      MetadataReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    public override ResourceType ResourceType
    {
      get
      {
        return ResourceType.Embedded;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Stream GetResourceStream()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] GetResourceData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static byte[] ReadStream(Stream stream)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
