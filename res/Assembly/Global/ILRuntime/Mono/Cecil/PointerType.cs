﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PointerType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class PointerType : TypeSpecification
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public PointerType(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    public override string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsValueType
    {
      get
      {
        return false;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override bool IsPointer
    {
      get
      {
        return true;
      }
    }
  }
}
