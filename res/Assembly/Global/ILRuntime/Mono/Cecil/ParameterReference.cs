﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ParameterReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public abstract class ParameterReference : IMetadataTokenProvider
  {
    private string name;
    internal int index;
    protected TypeReference parameter_type;
    internal MetadataToken token;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ParameterReference(string name, TypeReference parameterType)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public int Index
    {
      get
      {
        return this.index;
      }
    }

    public TypeReference ParameterType
    {
      get
      {
        return this.parameter_type;
      }
      set
      {
        this.parameter_type = value;
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }

    public override string ToString()
    {
      return this.name;
    }

    public abstract ParameterDefinition Resolve();
  }
}
