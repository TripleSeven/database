﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.PropertyAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum PropertyAttributes : ushort
  {
    None = 0,
    SpecialName = 512, // 0x0200
    RTSpecialName = 1024, // 0x0400
    HasDefault = 4096, // 0x1000
    Unused = 59903, // 0xE9FF
  }
}
