﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.CustomDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public abstract class CustomDebugInformation : DebugInformation
  {
    private Guid identifier;

    [MethodImpl((MethodImplOptions) 32768)]
    internal CustomDebugInformation(Guid identifier)
    {
      // ISSUE: unable to decompile the method.
    }

    public Guid Identifier
    {
      get
      {
        return this.identifier;
      }
    }

    public abstract CustomDebugInformationKind Kind { get; }
  }
}
