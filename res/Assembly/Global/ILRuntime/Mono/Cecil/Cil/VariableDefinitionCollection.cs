﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.VariableDefinitionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  internal sealed class VariableDefinitionCollection : Collection<VariableDefinition>
  {
    internal VariableDefinitionCollection()
    {
    }

    internal VariableDefinitionCollection(int capacity)
      : base(capacity)
    {
    }

    protected override void OnAdd(VariableDefinition item, int index)
    {
      item.index = index;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnInsert(VariableDefinition item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void OnSet(VariableDefinition item, int index)
    {
      item.index = index;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnRemove(VariableDefinition item, int index)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
