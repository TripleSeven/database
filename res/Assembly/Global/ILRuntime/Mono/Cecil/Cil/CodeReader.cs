﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.CodeReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.PE;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  internal sealed class CodeReader : BinaryStreamReader
  {
    internal readonly MetadataReader reader;
    private int start;
    private MethodDefinition method;
    private MethodBody body;

    [MethodImpl((MethodImplOptions) 32768)]
    public CodeReader(MetadataReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    private int Offset
    {
      get
      {
        return this.Position - this.start;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveTo(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MoveBackTo(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodBody ReadMethodBody(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReadCodeSize(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ReadCodeSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadMethodBody()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadFatMethod()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public VariableDefinitionCollection ReadVariables(
      MetadataToken local_var_token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadCode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private OpCode ReadOpCode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private object ReadOperand(Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetString(MetadataToken token)
    {
      // ISSUE: unable to decompile the method.
    }

    public ParameterDefinition GetParameter(int index)
    {
      return this.body.GetParameter(index);
    }

    public VariableDefinition GetVariable(int index)
    {
      return this.body.GetVariable(index);
    }

    public ILRuntime.Mono.Cecil.CallSite GetCallSite(MetadataToken token)
    {
      return this.reader.ReadCallSite(token);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResolveBranches(Collection<Instruction> instructions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Instruction GetInstruction(int offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Instruction GetInstruction(
      Collection<Instruction> instructions,
      int offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadSection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadSmallSection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadFatSection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadExceptionHandlers(int count, Func<int> read_entry, Func<int> read_length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadExceptionHandlerSpecific(ExceptionHandler handler)
    {
      // ISSUE: unable to decompile the method.
    }

    public MetadataToken ReadToken()
    {
      return new MetadataToken(this.ReadUInt32());
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadDebugInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadCustomDebugInformations(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadAsyncMethodBody(AsyncMethodBodyDebugInformation async_method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadStateMachineScope(
      StateMachineScopeDebugInformation state_machine_scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadSequencePoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadScopes(Collection<ScopeDebugInformation> scopes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadScope(ScopeDebugInformation scope)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
