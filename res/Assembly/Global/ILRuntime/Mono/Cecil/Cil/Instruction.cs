﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.Instruction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using System.Text;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class Instruction
  {
    internal int offset;
    internal OpCode opcode;
    internal object operand;
    internal Instruction previous;
    internal Instruction next;

    [MethodImpl((MethodImplOptions) 32768)]
    internal Instruction(int offset, OpCode opCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal Instruction(OpCode opcode, object operand)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Offset
    {
      get
      {
        return this.offset;
      }
      set
      {
        this.offset = value;
      }
    }

    public OpCode OpCode
    {
      get
      {
        return this.opcode;
      }
      set
      {
        this.opcode = value;
      }
    }

    public object Operand
    {
      get
      {
        return this.operand;
      }
      set
      {
        this.operand = value;
      }
    }

    public Instruction Previous
    {
      get
      {
        return this.previous;
      }
      set
      {
        this.previous = value;
      }
    }

    public Instruction Next
    {
      get
      {
        return this.next;
      }
      set
      {
        this.next = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendLabel(StringBuilder builder, Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, ILRuntime.Mono.Cecil.CallSite site)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, MethodReference method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, FieldReference field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, sbyte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, byte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, long value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, double value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, Instruction target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, Instruction[] targets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, VariableDefinition variable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Instruction Create(OpCode opcode, ParameterDefinition parameter)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
