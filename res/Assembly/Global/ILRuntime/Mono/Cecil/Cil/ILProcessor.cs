﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ILProcessor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ILProcessor
  {
    private readonly MethodBody body;
    private readonly Collection<Instruction> instructions;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ILProcessor(MethodBody body)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodBody Body
    {
      get
      {
        return this.body;
      }
    }

    public Instruction Create(OpCode opcode)
    {
      return Instruction.Create(opcode);
    }

    public Instruction Create(OpCode opcode, TypeReference type)
    {
      return Instruction.Create(opcode, type);
    }

    public Instruction Create(OpCode opcode, ILRuntime.Mono.Cecil.CallSite site)
    {
      return Instruction.Create(opcode, site);
    }

    public Instruction Create(OpCode opcode, MethodReference method)
    {
      return Instruction.Create(opcode, method);
    }

    public Instruction Create(OpCode opcode, FieldReference field)
    {
      return Instruction.Create(opcode, field);
    }

    public Instruction Create(OpCode opcode, string value)
    {
      return Instruction.Create(opcode, value);
    }

    public Instruction Create(OpCode opcode, sbyte value)
    {
      return Instruction.Create(opcode, value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Instruction Create(OpCode opcode, byte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Instruction Create(OpCode opcode, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    public Instruction Create(OpCode opcode, long value)
    {
      return Instruction.Create(opcode, value);
    }

    public Instruction Create(OpCode opcode, float value)
    {
      return Instruction.Create(opcode, value);
    }

    public Instruction Create(OpCode opcode, double value)
    {
      return Instruction.Create(opcode, value);
    }

    public Instruction Create(OpCode opcode, Instruction target)
    {
      return Instruction.Create(opcode, target);
    }

    public Instruction Create(OpCode opcode, Instruction[] targets)
    {
      return Instruction.Create(opcode, targets);
    }

    public Instruction Create(OpCode opcode, VariableDefinition variable)
    {
      return Instruction.Create(opcode, variable);
    }

    public Instruction Create(OpCode opcode, ParameterDefinition parameter)
    {
      return Instruction.Create(opcode, parameter);
    }

    public void Emit(OpCode opcode)
    {
      this.Append(this.Create(opcode));
    }

    public void Emit(OpCode opcode, TypeReference type)
    {
      this.Append(this.Create(opcode, type));
    }

    public void Emit(OpCode opcode, MethodReference method)
    {
      this.Append(this.Create(opcode, method));
    }

    public void Emit(OpCode opcode, ILRuntime.Mono.Cecil.CallSite site)
    {
      this.Append(this.Create(opcode, site));
    }

    public void Emit(OpCode opcode, FieldReference field)
    {
      this.Append(this.Create(opcode, field));
    }

    public void Emit(OpCode opcode, string value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, byte value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, sbyte value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, int value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, long value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, float value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, double value)
    {
      this.Append(this.Create(opcode, value));
    }

    public void Emit(OpCode opcode, Instruction target)
    {
      this.Append(this.Create(opcode, target));
    }

    public void Emit(OpCode opcode, Instruction[] targets)
    {
      this.Append(this.Create(opcode, targets));
    }

    public void Emit(OpCode opcode, VariableDefinition variable)
    {
      this.Append(this.Create(opcode, variable));
    }

    public void Emit(OpCode opcode, ParameterDefinition parameter)
    {
      this.Append(this.Create(opcode, parameter));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InsertBefore(Instruction target, Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InsertAfter(Instruction target, Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Append(Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Replace(Instruction target, Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Remove(Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
