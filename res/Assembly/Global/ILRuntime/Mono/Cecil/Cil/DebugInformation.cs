﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.DebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public abstract class DebugInformation : ICustomDebugInformationProvider, IMetadataTokenProvider
  {
    internal MetadataToken token;
    internal Collection<CustomDebugInformation> custom_infos;

    internal DebugInformation()
    {
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }

    public bool HasCustomDebugInformations
    {
      get
      {
        return !this.custom_infos.IsNullOrEmpty<CustomDebugInformation>();
      }
    }

    public Collection<CustomDebugInformation> CustomDebugInformations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
