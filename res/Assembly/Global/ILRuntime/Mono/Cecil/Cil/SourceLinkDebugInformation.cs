﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.SourceLinkDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class SourceLinkDebugInformation : CustomDebugInformation
  {
    internal string content;
    public static Guid KindIdentifier;

    [MethodImpl((MethodImplOptions) 32768)]
    public SourceLinkDebugInformation(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Content
    {
      get
      {
        return this.content;
      }
      set
      {
        this.content = value;
      }
    }

    public override CustomDebugInformationKind Kind
    {
      get
      {
        return CustomDebugInformationKind.SourceLink;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static SourceLinkDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
