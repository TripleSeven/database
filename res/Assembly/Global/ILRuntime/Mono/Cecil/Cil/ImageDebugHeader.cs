﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ImageDebugHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ImageDebugHeader
  {
    private readonly ImageDebugHeaderEntry[] entries;

    [MethodImpl((MethodImplOptions) 32768)]
    public ImageDebugHeader(ImageDebugHeaderEntry[] entries)
    {
      // ISSUE: unable to decompile the method.
    }

    public ImageDebugHeader()
      : this(Empty<ImageDebugHeaderEntry>.Array)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ImageDebugHeader(ImageDebugHeaderEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasEntries
    {
      get
      {
        return !this.entries.IsNullOrEmpty<ImageDebugHeaderEntry>();
      }
    }

    public ImageDebugHeaderEntry[] Entries
    {
      get
      {
        return this.entries;
      }
    }
  }
}
