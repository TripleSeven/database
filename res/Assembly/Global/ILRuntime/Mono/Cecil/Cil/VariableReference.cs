﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.VariableReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public abstract class VariableReference
  {
    internal int index;
    protected TypeReference variable_type;

    [MethodImpl((MethodImplOptions) 32768)]
    internal VariableReference(TypeReference variable_type)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference VariableType
    {
      get
      {
        return this.variable_type;
      }
      set
      {
        this.variable_type = value;
      }
    }

    public int Index
    {
      get
      {
        return this.index;
      }
    }

    public abstract VariableDefinition Resolve();

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
