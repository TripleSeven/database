﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ImportTarget
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ImportTarget
  {
    internal ImportTargetKind kind;
    internal string @namespace;
    internal TypeReference type;
    internal AssemblyNameReference reference;
    internal string alias;

    public ImportTarget(ImportTargetKind kind)
    {
      this.kind = kind;
    }

    public string Namespace
    {
      get
      {
        return this.@namespace;
      }
      set
      {
        this.@namespace = value;
      }
    }

    public TypeReference Type
    {
      get
      {
        return this.type;
      }
      set
      {
        this.type = value;
      }
    }

    public AssemblyNameReference AssemblyReference
    {
      get
      {
        return this.reference;
      }
      set
      {
        this.reference = value;
      }
    }

    public string Alias
    {
      get
      {
        return this.alias;
      }
      set
      {
        this.alias = value;
      }
    }

    public ImportTargetKind Kind
    {
      get
      {
        return this.kind;
      }
      set
      {
        this.kind = value;
      }
    }
  }
}
