﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ISymbolReaderProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace ILRuntime.Mono.Cecil.Cil
{
  public interface ISymbolReaderProvider
  {
    ISymbolReader GetSymbolReader(ModuleDefinition module, string fileName);

    ISymbolReader GetSymbolReader(ModuleDefinition module, Stream symbolStream);
  }
}
