﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.StateMachineScopeDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class StateMachineScopeDebugInformation : CustomDebugInformation
  {
    internal Collection<StateMachineScope> scopes;
    public static Guid KindIdentifier;

    public StateMachineScopeDebugInformation()
      : base(StateMachineScopeDebugInformation.KindIdentifier)
    {
    }

    public Collection<StateMachineScope> Scopes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override CustomDebugInformationKind Kind
    {
      get
      {
        return CustomDebugInformationKind.StateMachineScope;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static StateMachineScopeDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
