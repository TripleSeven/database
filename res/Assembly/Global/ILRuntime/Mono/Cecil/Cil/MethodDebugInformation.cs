﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.MethodDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class MethodDebugInformation : DebugInformation
  {
    internal MethodDefinition method;
    internal Collection<SequencePoint> sequence_points;
    internal ScopeDebugInformation scope;
    internal MethodDefinition kickoff_method;
    internal int code_size;
    internal MetadataToken local_var_token;

    [MethodImpl((MethodImplOptions) 32768)]
    internal MethodDebugInformation(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodDefinition Method
    {
      get
      {
        return this.method;
      }
    }

    public bool HasSequencePoints
    {
      get
      {
        return !this.sequence_points.IsNullOrEmpty<SequencePoint>();
      }
    }

    public Collection<SequencePoint> SequencePoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ScopeDebugInformation Scope
    {
      get
      {
        return this.scope;
      }
      set
      {
        this.scope = value;
      }
    }

    public MethodDefinition StateMachineKickOffMethod
    {
      get
      {
        return this.kickoff_method;
      }
      set
      {
        this.kickoff_method = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SequencePoint GetSequencePoint(Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IDictionary<Instruction, SequencePoint> GetSequencePointMapping()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<ScopeDebugInformation> GetScopes()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerable<ScopeDebugInformation> GetScopes(
      IList<ScopeDebugInformation> scopes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetName(VariableDefinition variable, out string name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
