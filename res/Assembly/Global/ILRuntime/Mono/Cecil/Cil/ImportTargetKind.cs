﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ImportTargetKind
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil.Cil
{
  public enum ImportTargetKind : byte
  {
    ImportNamespace = 1,
    ImportNamespaceInAssembly = 2,
    ImportType = 3,
    ImportXmlNamespaceWithAlias = 4,
    ImportAlias = 5,
    DefineAssemblyAlias = 6,
    DefineNamespaceAlias = 7,
    DefineNamespaceInAssemblyAlias = 8,
    DefineTypeAlias = 9,
  }
}
