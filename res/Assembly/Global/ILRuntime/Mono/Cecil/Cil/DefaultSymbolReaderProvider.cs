﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.DefaultSymbolReaderProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.IO;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public class DefaultSymbolReaderProvider : ISymbolReaderProvider
  {
    private readonly bool throw_if_no_symbol;

    public DefaultSymbolReaderProvider()
      : this(true)
    {
    }

    public DefaultSymbolReaderProvider(bool throwIfNoSymbol)
    {
      this.throw_if_no_symbol = throwIfNoSymbol;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ISymbolReader GetSymbolReader(ModuleDefinition module, string fileName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ISymbolReader GetSymbolReader(ModuleDefinition module, Stream symbolStream)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
