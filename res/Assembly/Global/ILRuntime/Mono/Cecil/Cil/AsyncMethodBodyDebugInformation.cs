﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.AsyncMethodBodyDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class AsyncMethodBodyDebugInformation : CustomDebugInformation
  {
    internal InstructionOffset catch_handler;
    internal Collection<InstructionOffset> yields;
    internal Collection<InstructionOffset> resumes;
    internal Collection<MethodDefinition> resume_methods;
    public static Guid KindIdentifier;

    [MethodImpl((MethodImplOptions) 32768)]
    internal AsyncMethodBodyDebugInformation(int catchHandler)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AsyncMethodBodyDebugInformation(Instruction catchHandler)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AsyncMethodBodyDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }

    public InstructionOffset CatchHandler
    {
      get
      {
        return this.catch_handler;
      }
      set
      {
        this.catch_handler = value;
      }
    }

    public Collection<InstructionOffset> Yields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<InstructionOffset> Resumes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<MethodDefinition> ResumeMethods
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override CustomDebugInformationKind Kind
    {
      get
      {
        return CustomDebugInformationKind.AsyncMethodBody;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static AsyncMethodBodyDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
