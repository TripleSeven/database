﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.EmbeddedSourceDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class EmbeddedSourceDebugInformation : CustomDebugInformation
  {
    internal byte[] content;
    internal bool compress;
    public static Guid KindIdentifier;

    [MethodImpl((MethodImplOptions) 32768)]
    public EmbeddedSourceDebugInformation(byte[] content, bool compress)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte[] Content
    {
      get
      {
        return this.content;
      }
      set
      {
        this.content = value;
      }
    }

    public bool Compress
    {
      get
      {
        return this.compress;
      }
      set
      {
        this.compress = value;
      }
    }

    public override CustomDebugInformationKind Kind
    {
      get
      {
        return CustomDebugInformationKind.EmbeddedSource;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static EmbeddedSourceDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
