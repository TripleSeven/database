﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.OpCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public struct OpCode : IEquatable<OpCode>
  {
    private readonly byte op1;
    private readonly byte op2;
    private readonly byte code;
    private readonly byte flow_control;
    private readonly byte opcode_type;
    private readonly byte operand_type;
    private readonly byte stack_behavior_pop;
    private readonly byte stack_behavior_push;

    [MethodImpl((MethodImplOptions) 32768)]
    internal OpCode(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return OpCodeNames.names[(int) this.Code];
      }
    }

    public int Size
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte Op1
    {
      get
      {
        return this.op1;
      }
    }

    public byte Op2
    {
      get
      {
        return this.op2;
      }
    }

    public short Value
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Code Code
    {
      get
      {
        return (Code) this.code;
      }
    }

    public FlowControl FlowControl
    {
      get
      {
        return (FlowControl) this.flow_control;
      }
    }

    public OpCodeType OpCodeType
    {
      get
      {
        return (OpCodeType) this.opcode_type;
      }
    }

    public OperandType OperandType
    {
      get
      {
        return (OperandType) this.operand_type;
      }
    }

    public StackBehaviour StackBehaviourPop
    {
      get
      {
        return (StackBehaviour) this.stack_behavior_pop;
      }
    }

    public StackBehaviour StackBehaviourPush
    {
      get
      {
        return (StackBehaviour) this.stack_behavior_push;
      }
    }

    public override int GetHashCode()
    {
      return (int) this.Value;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Equals(OpCode opcode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator ==(OpCode one, OpCode other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator !=(OpCode one, OpCode other)
    {
      // ISSUE: unable to decompile the method.
    }

    public override string ToString()
    {
      return this.Name;
    }
  }
}
