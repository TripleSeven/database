﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.EmbeddedPortablePdbReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class EmbeddedPortablePdbReader : ISymbolReader, IDisposable
  {
    private readonly PortablePdbReader reader;

    [MethodImpl((MethodImplOptions) 32768)]
    internal EmbeddedPortablePdbReader(PortablePdbReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool ProcessDebugHeader(ImageDebugHeader header)
    {
      return this.reader.ProcessDebugHeader(header);
    }

    public MethodDebugInformation Read(MethodDefinition method)
    {
      return this.reader.Read(method);
    }

    public void Dispose()
    {
      this.reader.Dispose();
    }
  }
}
