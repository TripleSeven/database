﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ConstantDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ConstantDebugInformation : DebugInformation
  {
    private string name;
    private TypeReference constant_type;
    private object value;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConstantDebugInformation(string name, TypeReference constant_type, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public TypeReference ConstantType
    {
      get
      {
        return this.constant_type;
      }
      set
      {
        this.constant_type = value;
      }
    }

    public object Value
    {
      get
      {
        return this.value;
      }
      set
      {
        this.value = value;
      }
    }
  }
}
