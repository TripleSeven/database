﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ImageDebugHeaderEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ImageDebugHeaderEntry
  {
    private ImageDebugDirectory directory;
    private readonly byte[] data;

    [MethodImpl((MethodImplOptions) 32768)]
    public ImageDebugHeaderEntry(ImageDebugDirectory directory, byte[] data)
    {
      // ISSUE: unable to decompile the method.
    }

    public ImageDebugDirectory Directory
    {
      get
      {
        return this.directory;
      }
      internal set
      {
        this.directory = value;
      }
    }

    public byte[] Data
    {
      get
      {
        return this.data;
      }
    }
  }
}
