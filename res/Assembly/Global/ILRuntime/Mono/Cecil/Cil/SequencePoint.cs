﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.SequencePoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class SequencePoint
  {
    internal InstructionOffset offset;
    private Document document;
    private int start_line;
    private int start_column;
    private int end_line;
    private int end_column;

    [MethodImpl((MethodImplOptions) 32768)]
    internal SequencePoint(int offset, Document document)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SequencePoint(Instruction instruction, Document document)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Offset
    {
      get
      {
        return this.offset.Offset;
      }
    }

    public int StartLine
    {
      get
      {
        return this.start_line;
      }
      set
      {
        this.start_line = value;
      }
    }

    public int StartColumn
    {
      get
      {
        return this.start_column;
      }
      set
      {
        this.start_column = value;
      }
    }

    public int EndLine
    {
      get
      {
        return this.end_line;
      }
      set
      {
        this.end_line = value;
      }
    }

    public int EndColumn
    {
      get
      {
        return this.end_column;
      }
      set
      {
        this.end_column = value;
      }
    }

    public bool IsHidden
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Document Document
    {
      get
      {
        return this.document;
      }
      set
      {
        this.document = value;
      }
    }
  }
}
