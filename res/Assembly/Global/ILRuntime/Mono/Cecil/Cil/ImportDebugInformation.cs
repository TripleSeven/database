﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ImportDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ImportDebugInformation : DebugInformation
  {
    internal ImportDebugInformation parent;
    internal Collection<ImportTarget> targets;

    [MethodImpl((MethodImplOptions) 32768)]
    public ImportDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasTargets
    {
      get
      {
        return !this.targets.IsNullOrEmpty<ImportTarget>();
      }
    }

    public Collection<ImportTarget> Targets
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ImportDebugInformation Parent
    {
      get
      {
        return this.parent;
      }
      set
      {
        this.parent = value;
      }
    }
  }
}
