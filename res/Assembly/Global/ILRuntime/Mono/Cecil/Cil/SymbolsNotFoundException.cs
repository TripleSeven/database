﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.SymbolsNotFoundException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.IO;
using System.Runtime.Serialization;

namespace ILRuntime.Mono.Cecil.Cil
{
  [Serializable]
  public sealed class SymbolsNotFoundException : FileNotFoundException
  {
    public SymbolsNotFoundException(string message)
      : base(message)
    {
    }

    private SymbolsNotFoundException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
