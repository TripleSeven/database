﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.StateMachineScope
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class StateMachineScope
  {
    internal InstructionOffset start;
    internal InstructionOffset end;

    [MethodImpl((MethodImplOptions) 32768)]
    internal StateMachineScope(int start, int end)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StateMachineScope(Instruction start, Instruction end)
    {
      // ISSUE: unable to decompile the method.
    }

    public InstructionOffset Start
    {
      get
      {
        return this.start;
      }
      set
      {
        this.start = value;
      }
    }

    public InstructionOffset End
    {
      get
      {
        return this.end;
      }
      set
      {
        this.end = value;
      }
    }
  }
}
