﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.VariableDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class VariableDebugInformation : DebugInformation
  {
    private string name;
    private ushort attributes;
    internal VariableIndex index;

    [MethodImpl((MethodImplOptions) 32768)]
    internal VariableDebugInformation(int index, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public VariableDebugInformation(VariableDefinition variable, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Index
    {
      get
      {
        return this.index.Index;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    public VariableAttributes Attributes
    {
      get
      {
        return (VariableAttributes) this.attributes;
      }
      set
      {
        this.attributes = (ushort) value;
      }
    }

    public bool IsDebuggerHidden
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 1);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
