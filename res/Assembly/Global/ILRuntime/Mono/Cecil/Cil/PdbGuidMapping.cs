﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.PdbGuidMapping
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  internal static class PdbGuidMapping
  {
    private static readonly Dictionary<Guid, DocumentLanguage> guid_language;
    private static readonly Dictionary<DocumentLanguage, Guid> language_guid;
    private static readonly Guid type_text;
    private static readonly Guid hash_md5;
    private static readonly Guid hash_sha1;
    private static readonly Guid hash_sha256;
    private static readonly Guid vendor_ms;

    [MethodImpl((MethodImplOptions) 32768)]
    static PdbGuidMapping()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddMapping(DocumentLanguage language, Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DocumentType ToType(this Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Guid ToGuid(this DocumentType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DocumentHashAlgorithm ToHashAlgorithm(this Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Guid ToGuid(this DocumentHashAlgorithm hash_algo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DocumentLanguage ToLanguage(this Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Guid ToGuid(this DocumentLanguage language)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DocumentLanguageVendor ToVendor(this Guid guid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Guid ToGuid(this DocumentLanguageVendor vendor)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
