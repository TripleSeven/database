﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.InstructionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  internal class InstructionCollection : Collection<Instruction>
  {
    private readonly MethodDefinition method;

    internal InstructionCollection(MethodDefinition method)
    {
      this.method = method;
    }

    internal InstructionCollection(MethodDefinition method, int capacity)
      : base(capacity)
    {
      this.method = method;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAdd(Instruction item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnInsert(Instruction item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnSet(Instruction item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnRemove(Instruction item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveSequencePoint(Instruction instruction)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
