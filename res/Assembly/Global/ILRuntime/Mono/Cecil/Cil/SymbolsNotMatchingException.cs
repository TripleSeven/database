﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.SymbolsNotMatchingException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.Serialization;

namespace ILRuntime.Mono.Cecil.Cil
{
  [Serializable]
  public sealed class SymbolsNotMatchingException : InvalidOperationException
  {
    public SymbolsNotMatchingException(string message)
      : base(message)
    {
    }

    private SymbolsNotMatchingException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
    }
  }
}
