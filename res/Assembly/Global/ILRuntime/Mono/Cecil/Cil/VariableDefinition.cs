﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.VariableDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class VariableDefinition : VariableReference
  {
    public VariableDefinition(TypeReference variableType)
      : base(variableType)
    {
    }

    public bool IsPinned
    {
      get
      {
        return this.variable_type.IsPinned;
      }
    }

    public override VariableDefinition Resolve()
    {
      return this;
    }
  }
}
