﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.BinaryCustomDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class BinaryCustomDebugInformation : CustomDebugInformation
  {
    private byte[] data;

    public BinaryCustomDebugInformation(Guid identifier, byte[] data)
      : base(identifier)
    {
      this.data = data;
    }

    public byte[] Data
    {
      get
      {
        return this.data;
      }
      set
      {
        this.data = value;
      }
    }

    public override CustomDebugInformationKind Kind
    {
      get
      {
        return CustomDebugInformationKind.Binary;
      }
    }
  }
}
