﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.ScopeDebugInformation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class ScopeDebugInformation : DebugInformation
  {
    internal InstructionOffset start;
    internal InstructionOffset end;
    internal ImportDebugInformation import;
    internal Collection<ScopeDebugInformation> scopes;
    internal Collection<VariableDebugInformation> variables;
    internal Collection<ConstantDebugInformation> constants;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ScopeDebugInformation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ScopeDebugInformation(Instruction start, Instruction end)
    {
      // ISSUE: unable to decompile the method.
    }

    public InstructionOffset Start
    {
      get
      {
        return this.start;
      }
      set
      {
        this.start = value;
      }
    }

    public InstructionOffset End
    {
      get
      {
        return this.end;
      }
      set
      {
        this.end = value;
      }
    }

    public ImportDebugInformation Import
    {
      get
      {
        return this.import;
      }
      set
      {
        this.import = value;
      }
    }

    public bool HasScopes
    {
      get
      {
        return !this.scopes.IsNullOrEmpty<ScopeDebugInformation>();
      }
    }

    public Collection<ScopeDebugInformation> Scopes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasVariables
    {
      get
      {
        return !this.variables.IsNullOrEmpty<VariableDebugInformation>();
      }
    }

    public Collection<VariableDebugInformation> Variables
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasConstants
    {
      get
      {
        return !this.constants.IsNullOrEmpty<ConstantDebugInformation>();
      }
    }

    public Collection<ConstantDebugInformation> Constants
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetName(VariableDefinition variable, out string name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
