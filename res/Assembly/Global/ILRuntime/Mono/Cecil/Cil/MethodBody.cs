﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.Cil.MethodBody
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil.Cil
{
  public sealed class MethodBody
  {
    internal readonly MethodDefinition method;
    internal ParameterDefinition this_parameter;
    internal int max_stack_size;
    internal int code_size;
    internal bool init_locals;
    internal MetadataToken local_var_token;
    internal Collection<Instruction> instructions;
    internal Collection<ExceptionHandler> exceptions;
    internal Collection<VariableDefinition> variables;

    public MethodBody(MethodDefinition method)
    {
      this.method = method;
    }

    public MethodDefinition Method
    {
      get
      {
        return this.method;
      }
    }

    public int MaxStackSize
    {
      get
      {
        return this.max_stack_size;
      }
      set
      {
        this.max_stack_size = value;
      }
    }

    public int CodeSize
    {
      get
      {
        return this.code_size;
      }
    }

    public bool InitLocals
    {
      get
      {
        return this.init_locals;
      }
      set
      {
        this.init_locals = value;
      }
    }

    public MetadataToken LocalVarToken
    {
      get
      {
        return this.local_var_token;
      }
      set
      {
        this.local_var_token = value;
      }
    }

    public Collection<Instruction> Instructions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasExceptionHandlers
    {
      get
      {
        return !this.exceptions.IsNullOrEmpty<ExceptionHandler>();
      }
    }

    public Collection<ExceptionHandler> ExceptionHandlers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasVariables
    {
      get
      {
        return !this.variables.IsNullOrEmpty<VariableDefinition>();
      }
    }

    public Collection<VariableDefinition> Variables
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ParameterDefinition ThisParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ParameterDefinition CreateThisParameter(
      MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    public ILProcessor GetILProcessor()
    {
      return new ILProcessor(this);
    }
  }
}
