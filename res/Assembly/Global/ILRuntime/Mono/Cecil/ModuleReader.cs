﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ModuleReader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.PE;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal abstract class ModuleReader
  {
    protected readonly ModuleDefinition module;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ModuleReader(Image image, ReadingMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract void ReadModule();

    public abstract void ReadSymbols(ModuleDefinition module);

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ReadModuleManifest(MetadataReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReadAssembly(MetadataReader reader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ModuleDefinition CreateModule(
      Image image,
      ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ReadSymbols(ModuleDefinition module, ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void GetMetadataKind(ModuleDefinition module, ReaderParameters parameters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ModuleReader CreateModuleReader(Image image, ReadingMode mode)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
