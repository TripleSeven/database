﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class MethodDefinition : MethodReference, IMemberDefinition, ISecurityDeclarationProvider, ICustomDebugInformationProvider, ICustomAttributeProvider, IMetadataTokenProvider
  {
    private ushort attributes;
    private ushort impl_attributes;
    internal volatile bool sem_attrs_ready;
    internal MethodSemanticsAttributes sem_attrs;
    private Collection<CustomAttribute> custom_attributes;
    private Collection<SecurityDeclaration> security_declarations;
    internal uint rva;
    internal PInvokeInfo pinvoke;
    private Collection<MethodReference> overrides;
    internal MethodBody body;
    internal MethodDebugInformation debug_info;
    internal Collection<CustomDebugInformation> custom_infos;

    [MethodImpl((MethodImplOptions) 32768)]
    internal MethodDefinition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDefinition(string name, MethodAttributes attributes, TypeReference returnType)
    {
      // ISSUE: unable to decompile the method.
    }

    public override string Name
    {
      get
      {
        return base.Name;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodAttributes Attributes
    {
      get
      {
        return (MethodAttributes) this.attributes;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodImplAttributes ImplAttributes
    {
      get
      {
        return (MethodImplAttributes) this.impl_attributes;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodSemanticsAttributes SemanticsAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.sem_attrs = value;
      }
    }

    internal MethodDefinitionProjection WindowsRuntimeProjection
    {
      get
      {
        return (MethodDefinitionProjection) this.projection;
      }
      set
      {
        this.projection = (object) value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal void ReadSemantics()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasSecurityDeclarations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<SecurityDeclaration> SecurityDeclarations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RVA
    {
      get
      {
        return (int) this.rva;
      }
    }

    public bool HasBody
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodBody Body
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodDebugInformation DebugInformation
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.debug_info = value;
      }
    }

    public bool HasPInvokeInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PInvokeInfo PInvokeInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.IsPInvokeImpl = true;
        this.pinvoke = value;
      }
    }

    public bool HasOverrides
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<MethodReference> Overrides
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool HasGenericParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override Collection<GenericParameter> GenericParameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasCustomDebugInformations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomDebugInformation> CustomDebugInformations
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCompilerControlled
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPrivate
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFamilyAndAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 2U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 3U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFamily
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 4U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFamilyOrAssembly
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 5U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPublic
    {
      get
      {
        return this.attributes.GetMaskedAttributes((ushort) 7, 6U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsStatic
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 16);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFinal
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 32);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsVirtual
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 64);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsHideBySig
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsReuseSlot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNewSlot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCheckAccessOnOverride
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAbstract
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPInvokeImpl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsUnmanagedExport
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 8);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntimeSpecialName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasSecurity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsIL
    {
      get
      {
        return this.impl_attributes.GetMaskedAttributes((ushort) 3, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNative
    {
      get
      {
        return this.impl_attributes.GetMaskedAttributes((ushort) 3, 1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRuntime
    {
      get
      {
        return this.impl_attributes.GetMaskedAttributes((ushort) 3, 3U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsUnmanaged
    {
      get
      {
        return this.impl_attributes.GetMaskedAttributes((ushort) 4, 4U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsManaged
    {
      get
      {
        return this.impl_attributes.GetMaskedAttributes((ushort) 4, 0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsForwardRef
    {
      get
      {
        return this.impl_attributes.GetAttributes((ushort) 16);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPreserveSig
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsInternalCall
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSynchronized
    {
      get
      {
        return this.impl_attributes.GetAttributes((ushort) 32);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool NoInlining
    {
      get
      {
        return this.impl_attributes.GetAttributes((ushort) 8);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool NoOptimization
    {
      get
      {
        return this.impl_attributes.GetAttributes((ushort) 64);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool AggressiveInlining
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSetter
    {
      get
      {
        return this.GetSemantics(MethodSemanticsAttributes.Setter);
      }
      set
      {
        this.SetSemantics(MethodSemanticsAttributes.Setter, value);
      }
    }

    public bool IsGetter
    {
      get
      {
        return this.GetSemantics(MethodSemanticsAttributes.Getter);
      }
      set
      {
        this.SetSemantics(MethodSemanticsAttributes.Getter, value);
      }
    }

    public bool IsOther
    {
      get
      {
        return this.GetSemantics(MethodSemanticsAttributes.Other);
      }
      set
      {
        this.SetSemantics(MethodSemanticsAttributes.Other, value);
      }
    }

    public bool IsAddOn
    {
      get
      {
        return this.GetSemantics(MethodSemanticsAttributes.AddOn);
      }
      set
      {
        this.SetSemantics(MethodSemanticsAttributes.AddOn, value);
      }
    }

    public bool IsRemoveOn
    {
      get
      {
        return this.GetSemantics(MethodSemanticsAttributes.RemoveOn);
      }
      set
      {
        this.SetSemantics(MethodSemanticsAttributes.RemoveOn, value);
      }
    }

    public bool IsFire
    {
      get
      {
        return this.GetSemantics(MethodSemanticsAttributes.Fire);
      }
      set
      {
        this.SetSemantics(MethodSemanticsAttributes.Fire, value);
      }
    }

    public TypeDefinition DeclaringType
    {
      get
      {
        return (TypeDefinition) base.DeclaringType;
      }
      set
      {
        this.DeclaringType = (TypeReference) value;
      }
    }

    public bool IsConstructor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsDefinition
    {
      get
      {
        return true;
      }
    }

    public override MethodDefinition Resolve()
    {
      return this;
    }
  }
}
