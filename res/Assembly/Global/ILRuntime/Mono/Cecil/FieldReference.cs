﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.FieldReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public class FieldReference : MemberReference
  {
    private TypeReference field_type;

    [MethodImpl((MethodImplOptions) 32768)]
    internal FieldReference()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldReference(string name, TypeReference fieldType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldReference(string name, TypeReference fieldType, TypeReference declaringType)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeReference FieldType
    {
      get
      {
        return this.field_type;
      }
      set
      {
        this.field_type = value;
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool ContainsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override IMemberDefinition ResolveDefinition()
    {
      return (IMemberDefinition) this.Resolve();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual FieldDefinition Resolve()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
