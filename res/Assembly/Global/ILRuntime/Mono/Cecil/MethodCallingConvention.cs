﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MethodCallingConvention
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum MethodCallingConvention : byte
  {
    Default = 0,
    C = 1,
    StdCall = 2,
    ThisCall = 3,
    FastCall = 4,
    VarArg = 5,
    Generic = 16, // 0x10
  }
}
