﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MarshalInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public class MarshalInfo
  {
    internal NativeType native;

    public MarshalInfo(NativeType native)
    {
      this.native = native;
    }

    public NativeType NativeType
    {
      get
      {
        return this.native;
      }
      set
      {
        this.native = value;
      }
    }
  }
}
