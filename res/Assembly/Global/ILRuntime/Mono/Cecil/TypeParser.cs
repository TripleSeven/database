﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TypeParser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using System.Text;

namespace ILRuntime.Mono.Cecil
{
  internal class TypeParser
  {
    private readonly string fullname;
    private readonly int length;
    private int position;

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeParser(string fullname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeParser.Type ParseType(bool fq_name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryGetArity(TypeParser.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryGetArity(string name, out int arity)
    {
      // ISSUE: unable to decompile the method.
    }

    private static bool ParseInt32(string value, out int result)
    {
      return int.TryParse(value, out result);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void TryAddArity(string name, ref int arity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ParsePart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsDelimiter(char chr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryParseWhiteSpace()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string[] ParseNestedNames()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryParse(char chr)
    {
      // ISSUE: unable to decompile the method.
    }

    private static void Add<T>(ref T[] array, T item)
    {
      array = array.Add<T>(item);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int[] ParseSpecs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TypeParser.Type[] ParseGenericArguments(int arity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ParseAssemblyName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TypeReference ParseType(
      ModuleDefinition module,
      string fullname,
      bool typeDefinitionOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeReference GetTypeReference(
      ModuleDefinition module,
      TypeParser.Type type_info,
      bool type_def_only)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeReference CreateSpecs(
      TypeReference type,
      TypeParser.Type type_info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeReference TryCreateGenericInstanceType(
      TypeReference type,
      TypeParser.Type type_info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SplitFullName(string fullname, out string @namespace, out string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeReference CreateReference(
      TypeParser.Type type_info,
      ModuleDefinition module,
      IMetadataScope scope)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AdjustGenericParameters(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static IMetadataScope GetMetadataScope(
      ModuleDefinition module,
      TypeParser.Type type_info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryGetDefinition(
      ModuleDefinition module,
      TypeParser.Type type_info,
      out TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryCurrentModule(ModuleDefinition module, TypeParser.Type type_info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ToParseable(TypeReference type, bool top_level = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendNamePart(string part, StringBuilder name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendType(
      TypeReference type,
      StringBuilder name,
      bool fq_name,
      bool top_level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetScopeFullName(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendTypeSpecification(TypeSpecification type, StringBuilder name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool RequiresFullyQualifiedName(TypeReference type, bool top_level)
    {
      // ISSUE: unable to decompile the method.
    }

    private class Type
    {
      public const int Ptr = -1;
      public const int ByRef = -2;
      public const int SzArray = -3;
      public string type_fullname;
      public string[] nested_names;
      public int arity;
      public int[] specs;
      public TypeParser.Type[] generic_arguments;
      public string assembly;
    }
  }
}
