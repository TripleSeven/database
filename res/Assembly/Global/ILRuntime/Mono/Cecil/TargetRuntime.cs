﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TargetRuntime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum TargetRuntime
  {
    Net_1_0,
    Net_1_1,
    Net_2_0,
    Net_4_0,
  }
}
