﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ParameterDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class ParameterDefinition : ParameterReference, ICustomAttributeProvider, IConstantProvider, IMarshalInfoProvider, IMetadataTokenProvider
  {
    private ushort attributes;
    internal IMethodSignature method;
    private object constant;
    private Collection<CustomAttribute> custom_attributes;
    private MarshalInfo marshal_info;

    [MethodImpl((MethodImplOptions) 32768)]
    internal ParameterDefinition(TypeReference parameterType, IMethodSignature method)
    {
      // ISSUE: unable to decompile the method.
    }

    public ParameterDefinition(TypeReference parameterType)
      : this(string.Empty, ParameterAttributes.None, parameterType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ParameterDefinition(
      string name,
      ParameterAttributes attributes,
      TypeReference parameterType)
    {
      // ISSUE: unable to decompile the method.
    }

    public ParameterAttributes Attributes
    {
      get
      {
        return (ParameterAttributes) this.attributes;
      }
      set
      {
        this.attributes = (ushort) value;
      }
    }

    public IMethodSignature Method
    {
      get
      {
        return this.method;
      }
    }

    public int Sequence
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasConstant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public object Constant
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.constant = value;
      }
    }

    public bool HasCustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Collection<CustomAttribute> CustomAttributes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasMarshalInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MarshalInfo MarshalInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.marshal_info = value;
      }
    }

    public bool IsIn
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 1);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsOut
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 2);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsLcid
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 4);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsReturnValue
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 8);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsOptional
    {
      get
      {
        return this.attributes.GetAttributes((ushort) 16);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDefault
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasFieldMarshal
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override ParameterDefinition Resolve()
    {
      return this;
    }
  }
}
