﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.FunctionPointerType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class FunctionPointerType : TypeSpecification, IMethodSignature, IMetadataTokenProvider
  {
    private readonly MethodReference function;

    [MethodImpl((MethodImplOptions) 32768)]
    public FunctionPointerType()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasThis
    {
      get
      {
        return this.function.HasThis;
      }
      set
      {
        this.function.HasThis = value;
      }
    }

    public bool ExplicitThis
    {
      get
      {
        return this.function.ExplicitThis;
      }
      set
      {
        this.function.ExplicitThis = value;
      }
    }

    public MethodCallingConvention CallingConvention
    {
      get
      {
        return this.function.CallingConvention;
      }
      set
      {
        this.function.CallingConvention = value;
      }
    }

    public bool HasParameters
    {
      get
      {
        return this.function.HasParameters;
      }
    }

    public Collection<ParameterDefinition> Parameters
    {
      get
      {
        return this.function.Parameters;
      }
    }

    public TypeReference ReturnType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodReturnType MethodReturnType
    {
      get
      {
        return this.function.MethodReturnType;
      }
    }

    public override string Name
    {
      get
      {
        return this.function.Name;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override string Namespace
    {
      get
      {
        return string.Empty;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override ModuleDefinition Module
    {
      get
      {
        return this.ReturnType.Module;
      }
    }

    public override IMetadataScope Scope
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public override bool IsFunctionPointer
    {
      get
      {
        return true;
      }
    }

    public override bool ContainsGenericParameter
    {
      get
      {
        return this.function.ContainsGenericParameter;
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override TypeDefinition Resolve()
    {
      return (TypeDefinition) null;
    }

    public override TypeReference GetElementType()
    {
      return (TypeReference) this;
    }
  }
}
