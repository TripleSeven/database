﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.GenericInstanceMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class GenericInstanceMethod : MethodSpecification, IGenericInstance, IGenericContext, IMetadataTokenProvider
  {
    private Collection<TypeReference> arguments;

    public GenericInstanceMethod(MethodReference method)
      : base(method)
    {
    }

    public bool HasGenericArguments
    {
      get
      {
        return !this.arguments.IsNullOrEmpty<TypeReference>();
      }
    }

    public Collection<TypeReference> GenericArguments
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override bool IsGenericInstance
    {
      get
      {
        return true;
      }
    }

    IGenericParameterProvider IGenericContext.Method
    {
      get
      {
        return (IGenericParameterProvider) this.ElementMethod;
      }
    }

    IGenericParameterProvider IGenericContext.Type
    {
      get
      {
        return (IGenericParameterProvider) this.ElementMethod.DeclaringType;
      }
    }

    public override bool ContainsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
