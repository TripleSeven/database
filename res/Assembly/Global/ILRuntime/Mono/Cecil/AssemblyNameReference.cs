﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyNameReference
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public class AssemblyNameReference : IMetadataScope, IMetadataTokenProvider
  {
    private string name;
    private string culture;
    private Version version;
    private uint attributes;
    private byte[] public_key;
    private byte[] public_key_token;
    private AssemblyHashAlgorithm hash_algorithm;
    private byte[] hash;
    internal MetadataToken token;
    private string full_name;

    [MethodImpl((MethodImplOptions) 32768)]
    internal AssemblyNameReference()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssemblyNameReference(string name, Version version)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
        this.full_name = (string) null;
      }
    }

    public string Culture
    {
      get
      {
        return this.culture;
      }
      set
      {
        this.culture = value;
        this.full_name = (string) null;
      }
    }

    public Version Version
    {
      get
      {
        return this.version;
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public AssemblyAttributes Attributes
    {
      get
      {
        return (AssemblyAttributes) this.attributes;
      }
      set
      {
        this.attributes = (uint) value;
      }
    }

    public bool HasPublicKey
    {
      get
      {
        return this.attributes.GetAttributes(1U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsSideBySideCompatible
    {
      get
      {
        return this.attributes.GetAttributes(0U);
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsRetargetable
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsWindowsRuntime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte[] PublicKey
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte[] PublicKeyToken
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.public_key_token = value;
        this.full_name = (string) null;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] HashPublicKey()
    {
      // ISSUE: unable to decompile the method.
    }

    public virtual MetadataScopeType MetadataScopeType
    {
      get
      {
        return MetadataScopeType.AssemblyNameReference;
      }
    }

    public string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AssemblyNameReference Parse(string fullName)
    {
      // ISSUE: unable to decompile the method.
    }

    public AssemblyHashAlgorithm HashAlgorithm
    {
      get
      {
        return this.hash_algorithm;
      }
      set
      {
        this.hash_algorithm = value;
      }
    }

    public virtual byte[] Hash
    {
      get
      {
        return this.hash;
      }
      set
      {
        this.hash = value;
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.token;
      }
      set
      {
        this.token = value;
      }
    }

    public override string ToString()
    {
      return this.FullName;
    }
  }
}
