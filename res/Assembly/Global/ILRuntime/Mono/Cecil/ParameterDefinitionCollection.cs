﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ParameterDefinitionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class ParameterDefinitionCollection : Collection<ParameterDefinition>
  {
    private readonly IMethodSignature method;

    internal ParameterDefinitionCollection(IMethodSignature method)
    {
      this.method = method;
    }

    internal ParameterDefinitionCollection(IMethodSignature method, int capacity)
      : base(capacity)
    {
      this.method = method;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAdd(ParameterDefinition item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnInsert(ParameterDefinition item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnSet(ParameterDefinition item, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnRemove(ParameterDefinition item, int index)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
