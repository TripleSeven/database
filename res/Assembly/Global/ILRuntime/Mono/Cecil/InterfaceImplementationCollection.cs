﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.InterfaceImplementationCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;

namespace ILRuntime.Mono.Cecil
{
  internal class InterfaceImplementationCollection : Collection<InterfaceImplementation>
  {
    private readonly TypeDefinition type;

    internal InterfaceImplementationCollection(TypeDefinition type)
    {
      this.type = type;
    }

    internal InterfaceImplementationCollection(TypeDefinition type, int length)
      : base(length)
    {
      this.type = type;
    }

    protected override void OnAdd(InterfaceImplementation item, int index)
    {
      item.type = this.type;
    }

    protected override void OnInsert(InterfaceImplementation item, int index)
    {
      item.type = this.type;
    }

    protected override void OnSet(InterfaceImplementation item, int index)
    {
      item.type = this.type;
    }

    protected override void OnRemove(InterfaceImplementation item, int index)
    {
      item.type = (TypeDefinition) null;
    }
  }
}
