﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.ArrayDimension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public struct ArrayDimension
  {
    private int? lower_bound;
    private int? upper_bound;

    public ArrayDimension(int? lowerBound, int? upperBound)
    {
      this.lower_bound = lowerBound;
      this.upper_bound = upperBound;
    }

    public int? LowerBound
    {
      get
      {
        return this.lower_bound;
      }
      set
      {
        this.lower_bound = value;
      }
    }

    public int? UpperBound
    {
      get
      {
        return this.upper_bound;
      }
      set
      {
        this.upper_bound = value;
      }
    }

    public bool IsSized
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
