﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.CallSite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Collections.Generic;
using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class CallSite : IMethodSignature, IMetadataTokenProvider
  {
    private readonly MethodReference signature;

    [MethodImpl((MethodImplOptions) 32768)]
    internal CallSite()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CallSite(TypeReference returnType)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasThis
    {
      get
      {
        return this.signature.HasThis;
      }
      set
      {
        this.signature.HasThis = value;
      }
    }

    public bool ExplicitThis
    {
      get
      {
        return this.signature.ExplicitThis;
      }
      set
      {
        this.signature.ExplicitThis = value;
      }
    }

    public MethodCallingConvention CallingConvention
    {
      get
      {
        return this.signature.CallingConvention;
      }
      set
      {
        this.signature.CallingConvention = value;
      }
    }

    public bool HasParameters
    {
      get
      {
        return this.signature.HasParameters;
      }
    }

    public Collection<ParameterDefinition> Parameters
    {
      get
      {
        return this.signature.Parameters;
      }
    }

    public TypeReference ReturnType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MethodReturnType MethodReturnType
    {
      get
      {
        return this.signature.MethodReturnType;
      }
    }

    public string Name
    {
      get
      {
        return string.Empty;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public string Namespace
    {
      get
      {
        return string.Empty;
      }
      set
      {
        throw new InvalidOperationException();
      }
    }

    public ModuleDefinition Module
    {
      get
      {
        return this.ReturnType.Module;
      }
    }

    public IMetadataScope Scope
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MetadataToken MetadataToken
    {
      get
      {
        return this.signature.token;
      }
      set
      {
        this.signature.token = value;
      }
    }

    public string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public override string ToString()
    {
      return this.FullName;
    }
  }
}
