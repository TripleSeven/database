﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.TargetArchitecture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace ILRuntime.Mono.Cecil
{
  public enum TargetArchitecture
  {
    I386 = 332, // 0x0000014C
    ARM = 448, // 0x000001C0
    ARMv7 = 452, // 0x000001C4
    IA64 = 512, // 0x00000200
    AMD64 = 34404, // 0x00008664
    ARM64 = 43620, // 0x0000AA64
  }
}
