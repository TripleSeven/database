﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.GenericParameterAttributes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace ILRuntime.Mono.Cecil
{
  [Flags]
  public enum GenericParameterAttributes : ushort
  {
    VarianceMask = 3,
    NonVariant = 0,
    Covariant = 1,
    Contravariant = 2,
    SpecialConstraintMask = 28, // 0x001C
    ReferenceTypeConstraint = 4,
    NotNullableValueTypeConstraint = 8,
    DefaultConstructorConstraint = 16, // 0x0010
  }
}
