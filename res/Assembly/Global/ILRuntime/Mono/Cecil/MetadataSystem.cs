﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.MetadataSystem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Cecil.Metadata;
using ILRuntime.Mono.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  internal sealed class MetadataSystem
  {
    internal AssemblyNameReference[] AssemblyReferences;
    internal ModuleReference[] ModuleReferences;
    internal TypeDefinition[] Types;
    internal TypeReference[] TypeReferences;
    internal FieldDefinition[] Fields;
    internal MethodDefinition[] Methods;
    internal MemberReference[] MemberReferences;
    internal Dictionary<uint, Collection<uint>> NestedTypes;
    internal Dictionary<uint, uint> ReverseNestedTypes;
    internal Dictionary<uint, Collection<Row<uint, MetadataToken>>> Interfaces;
    internal Dictionary<uint, Row<ushort, uint>> ClassLayouts;
    internal Dictionary<uint, uint> FieldLayouts;
    internal Dictionary<uint, uint> FieldRVAs;
    internal Dictionary<MetadataToken, uint> FieldMarshals;
    internal Dictionary<MetadataToken, Row<ElementType, uint>> Constants;
    internal Dictionary<uint, Collection<MetadataToken>> Overrides;
    internal Dictionary<MetadataToken, Range[]> CustomAttributes;
    internal Dictionary<MetadataToken, Range[]> SecurityDeclarations;
    internal Dictionary<uint, Range> Events;
    internal Dictionary<uint, Range> Properties;
    internal Dictionary<uint, Row<MethodSemanticsAttributes, MetadataToken>> Semantics;
    internal Dictionary<uint, Row<PInvokeAttributes, uint, uint>> PInvokes;
    internal Dictionary<MetadataToken, Range[]> GenericParameters;
    internal Dictionary<uint, Collection<MetadataToken>> GenericConstraints;
    internal Document[] Documents;
    internal Dictionary<uint, Collection<Row<uint, Range, Range, uint, uint, uint>>> LocalScopes;
    internal ImportDebugInformation[] ImportScopes;
    internal Dictionary<uint, uint> StateMachineMethods;
    internal Dictionary<MetadataToken, Row<Guid, uint, uint>[]> CustomDebugInformations;
    private static Dictionary<string, Row<ElementType, bool>> primitive_value_types;

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitializePrimitives()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void TryProcessPrimitiveTypeReference(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetPrimitiveElementType(TypeDefinition type, out ElementType etype)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryGetPrimitiveData(
      TypeReference type,
      out Row<ElementType, bool> primitive_data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssemblyNameReference GetAssemblyNameReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeDefinition GetTypeDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTypeDefinition(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TypeReference GetTypeReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTypeReference(TypeReference type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldDefinition GetFieldDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFieldDefinition(FieldDefinition field)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MethodDefinition GetMethodDefinition(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMethodDefinition(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MemberReference GetMemberReference(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMemberReference(MemberReference member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetNestedTypeMapping(TypeDefinition type, out Collection<uint> mapping)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetNestedTypeMapping(uint type_rid, Collection<uint> mapping)
    {
      this.NestedTypes[type_rid] = mapping;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveNestedTypeMapping(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetReverseNestedTypeMapping(TypeDefinition type, out uint declaring)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetReverseNestedTypeMapping(uint nested, uint declaring)
    {
      this.ReverseNestedTypes[nested] = declaring;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveReverseNestedTypeMapping(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetInterfaceMapping(
      TypeDefinition type,
      out Collection<Row<uint, MetadataToken>> mapping)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetInterfaceMapping(uint type_rid, Collection<Row<uint, MetadataToken>> mapping)
    {
      this.Interfaces[type_rid] = mapping;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveInterfaceMapping(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddPropertiesRange(uint type_rid, Range range)
    {
      this.Properties.Add(type_rid, range);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetPropertiesRange(TypeDefinition type, out Range range)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemovePropertiesRange(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    public void AddEventsRange(uint type_rid, Range range)
    {
      this.Events.Add(type_rid, range);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetEventsRange(TypeDefinition type, out Range range)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveEventsRange(TypeDefinition type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetGenericParameterRanges(IGenericParameterProvider owner, out Range[] ranges)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveGenericParameterRange(IGenericParameterProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetCustomAttributeRanges(ICustomAttributeProvider owner, out Range[] ranges)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCustomAttributeRange(ICustomAttributeProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetSecurityDeclarationRanges(
      ISecurityDeclarationProvider owner,
      out Range[] ranges)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveSecurityDeclarationRange(ISecurityDeclarationProvider owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetGenericConstraintMapping(
      GenericParameter generic_parameter,
      out Collection<MetadataToken> mapping)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetGenericConstraintMapping(uint gp_rid, Collection<MetadataToken> mapping)
    {
      this.GenericConstraints[gp_rid] = mapping;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveGenericConstraintMapping(GenericParameter generic_parameter)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetOverrideMapping(
      MethodDefinition method,
      out Collection<MetadataToken> mapping)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetOverrideMapping(uint rid, Collection<MetadataToken> mapping)
    {
      this.Overrides[rid] = mapping;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveOverrideMapping(MethodDefinition method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Document GetDocument(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetLocalScopes(
      MethodDefinition method,
      out Collection<Row<uint, Range, Range, uint, uint, uint>> scopes)
    {
      // ISSUE: unable to decompile the method.
    }

    public void SetLocalScopes(
      uint method_rid,
      Collection<Row<uint, Range, Range, uint, uint, uint>> records)
    {
      this.LocalScopes[method_rid] = records;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ImportDebugInformation GetImportScope(uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool TryGetStateMachineKickOffMethod(MethodDefinition method, out uint rid)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeDefinition GetFieldDeclaringType(uint field_rid)
    {
      return MetadataSystem.BinaryRangeSearch(this.Types, field_rid, true);
    }

    public TypeDefinition GetMethodDeclaringType(uint method_rid)
    {
      return MetadataSystem.BinaryRangeSearch(this.Types, method_rid, false);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static TypeDefinition BinaryRangeSearch(
      TypeDefinition[] types,
      uint rid,
      bool field)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
