﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.Mono.Cecil.AssemblyNameDefinition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace ILRuntime.Mono.Cecil
{
  public sealed class AssemblyNameDefinition : AssemblyNameReference
  {
    [MethodImpl((MethodImplOptions) 32768)]
    internal AssemblyNameDefinition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AssemblyNameDefinition(string name, Version version)
    {
      // ISSUE: unable to decompile the method.
    }

    public override byte[] Hash
    {
      get
      {
        return Empty<byte>.Array;
      }
    }
  }
}
