﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.TypeSystem.ILGenericParameterType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.CLR.TypeSystem
{
  internal class ILGenericParameterType : IType
  {
    private string name;
    private ILGenericParameterType arrayType;

    public ILGenericParameterType(string name)
    {
      this.name = name;
    }

    public bool IsGenericInstance
    {
      get
      {
        return false;
      }
    }

    public KeyValuePair<string, IType>[] GenericArguments
    {
      get
      {
        return (KeyValuePair<string, IType>[]) null;
      }
    }

    public bool HasGenericParameter
    {
      get
      {
        return true;
      }
    }

    public bool IsGenericParameter
    {
      get
      {
        return true;
      }
    }

    public Type TypeForCLR
    {
      get
      {
        return typeof (ILGenericParameterType);
      }
    }

    public string FullName
    {
      get
      {
        return this.name;
      }
    }

    public ILRuntime.Runtime.Enviorment.AppDomain AppDomain
    {
      get
      {
        return (ILRuntime.Runtime.Enviorment.AppDomain) null;
      }
    }

    public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false)
    {
      return (IMethod) null;
    }

    public IMethod GetMethod(
      string name,
      List<IType> param,
      IType[] genericArguments,
      IType returnType = null,
      bool declaredOnly = false)
    {
      return (IMethod) null;
    }

    public List<IMethod> GetMethods()
    {
      return (List<IMethod>) null;
    }

    public IMethod GetConstructor(List<IType> param)
    {
      return (IMethod) null;
    }

    public bool CanAssignTo(IType type)
    {
      return false;
    }

    public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments)
    {
      return (IType) null;
    }

    public IType ResolveGenericType(IType contextType)
    {
      throw new NotImplementedException();
    }

    public int GetFieldIndex(object token)
    {
      return -1;
    }

    public IType FindGenericArgument(string key)
    {
      return (IType) null;
    }

    public IType ByRefType
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public IType MakeByRefType()
    {
      return (IType) this;
    }

    public IType ArrayType
    {
      get
      {
        return (IType) this.arrayType;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeArrayType(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsValueType
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public bool IsPrimitive
    {
      get
      {
        return false;
      }
    }

    public bool IsEnum
    {
      get
      {
        return false;
      }
    }

    public bool IsInterface
    {
      get
      {
        return false;
      }
    }

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public bool IsDelegate
    {
      get
      {
        return false;
      }
    }

    public Type ReflectionType
    {
      get
      {
        throw new NotImplementedException();
      }
    }

    public IType BaseType
    {
      get
      {
        return (IType) null;
      }
    }

    public IMethod GetVirtualMethod(IMethod method)
    {
      return method;
    }

    public bool IsArray
    {
      get
      {
        return false;
      }
    }

    public bool IsByRef
    {
      get
      {
        return false;
      }
    }

    public IType ElementType
    {
      get
      {
        return (IType) null;
      }
    }

    public int ArrayRank
    {
      get
      {
        return 1;
      }
    }

    public IType[] Implements
    {
      get
      {
        return (IType[]) null;
      }
    }
  }
}
