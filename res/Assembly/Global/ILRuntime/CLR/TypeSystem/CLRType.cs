﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.TypeSystem.CLRType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Reflection;
using ILRuntime.Runtime.Enviorment;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.CLR.TypeSystem
{
  public class CLRType : IType
  {
    private static int instance_id = 536870912;
    private Type clrType;
    private bool isPrimitive;
    private bool isValueType;
    private bool isEnum;
    private Dictionary<string, List<CLRMethod>> methods;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private List<CLRMethod> constructors;
    private KeyValuePair<string, IType>[] genericArguments;
    private List<CLRType> genericInstances;
    private Dictionary<string, int> fieldMapping;
    private Dictionary<int, FieldInfo> fieldInfoCache;
    private Dictionary<int, CLRFieldGetterDelegate> fieldGetterCache;
    private Dictionary<int, CLRFieldSetterDelegate> fieldSetterCache;
    private Dictionary<int, int> fieldIdxMapping;
    private IType[] orderedFieldTypes;
    private CLRMemberwiseCloneDelegate memberwiseCloneDelegate;
    private CLRCreateDefaultInstanceDelegate createDefaultInstanceDelegate;
    private CLRCreateArrayInstanceDelegate createArrayInstanceDelegate;
    private Dictionary<int, int> fieldTokenMapping;
    private IType byRefType;
    private IType elementType;
    private Dictionary<int, IType> arrayTypes;
    private IType[] interfaces;
    private bool isDelegate;
    private IType baseType;
    private bool isBaseTypeInitialized;
    private bool interfaceInitialized;
    private bool valueTypeBinderGot;
    private ILRuntimeWrapperType wraperType;
    private ValueTypeBinder valueTypeBinder;
    private int hashCode;

    [MethodImpl((MethodImplOptions) 32768)]
    public CLRType(Type clrType, ILRuntime.Runtime.Enviorment.AppDomain appdomain)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, FieldInfo> Fields
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, int> FieldIndexMapping
    {
      get
      {
        return this.fieldIdxMapping;
      }
    }

    public IType[] OrderedFieldTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalFieldCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ILRuntime.Runtime.Enviorment.AppDomain AppDomain
    {
      get
      {
        return this.appdomain;
      }
    }

    public bool IsGenericInstance
    {
      get
      {
        return this.genericArguments != null;
      }
    }

    public KeyValuePair<string, IType>[] GenericArguments
    {
      get
      {
        return this.genericArguments;
      }
    }

    public IType ElementType
    {
      get
      {
        return this.elementType;
      }
    }

    public bool HasGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsGenericParameter
    {
      get
      {
        return this.clrType.IsGenericParameter;
      }
    }

    public bool IsInterface
    {
      get
      {
        return this.clrType.IsInterface;
      }
    }

    public Type TypeForCLR
    {
      get
      {
        return this.clrType;
      }
    }

    public Type ReflectionType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType ByRefType
    {
      get
      {
        return this.byRefType;
      }
    }

    public IType ArrayType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsArray { get; private set; }

    public int ArrayRank { get; private set; }

    public bool IsValueType
    {
      get
      {
        return this.isValueType;
      }
    }

    public bool IsByRef
    {
      get
      {
        return this.clrType.IsByRef;
      }
    }

    public bool IsDelegate
    {
      get
      {
        return this.isDelegate;
      }
    }

    public bool IsPrimitive
    {
      get
      {
        return this.isPrimitive;
      }
    }

    public bool IsEnum
    {
      get
      {
        return this.isEnum;
      }
    }

    public string FullName
    {
      get
      {
        return this.clrType.FullName;
      }
    }

    public string Name
    {
      get
      {
        return this.clrType.Name;
      }
    }

    public IType BaseType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType[] Implements
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ValueTypeBinder ValueTypeBinder
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object PerformMemberwiseClone(object target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeBaseType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeInterfaces()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object GetFieldValue(int hash, object target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStaticFieldValue(int hash, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFieldValue(int hash, ref object target, object value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CLRFieldGetterDelegate GetFieldGetter(int hash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CLRFieldSetterDelegate GetFieldSetter(int hash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FieldInfo GetField(int hash)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<IMethod> GetMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetVirtualMethod(IMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFields()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFieldIndex(object token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType FindGenericArgument(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool MatchGenericParameters(Type[] args, Type type, Type q, IType[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetMethod(
      string name,
      List<IType> param,
      IType[] genericArguments,
      IType returnType = null,
      bool declaredOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAssignTo(IType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetConstructor(List<IType> param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object CreateDefaultInstance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object CreateArrayInstance(int size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeByRefType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeArrayType(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    public IType ResolveGenericType(IType contextType)
    {
      throw new NotImplementedException();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    public override string ToString()
    {
      return this.clrType.ToString();
    }
  }
}
