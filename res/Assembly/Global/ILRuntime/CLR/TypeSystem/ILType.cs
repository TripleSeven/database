﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.TypeSystem.ILType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.Mono.Cecil;
using ILRuntime.Reflection;
using ILRuntime.Runtime.Intepreter;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ILRuntime.CLR.TypeSystem
{
  public class ILType : IType
  {
    private static int instance_id = 268435456;
    private Dictionary<string, List<ILMethod>> methods;
    private TypeReference typeRef;
    private TypeDefinition definition;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private ILMethod staticConstructor;
    private List<ILMethod> constructors;
    private IType[] fieldTypes;
    private FieldDefinition[] fieldDefinitions;
    private IType[] staticFieldTypes;
    private FieldDefinition[] staticFieldDefinitions;
    private Dictionary<string, int> fieldMapping;
    private Dictionary<string, int> staticFieldMapping;
    private ILTypeStaticInstance staticInstance;
    private Dictionary<int, int> fieldTokenMapping;
    private int fieldStartIdx;
    private int totalFieldCnt;
    private KeyValuePair<string, IType>[] genericArguments;
    private IType baseType;
    private IType byRefType;
    private IType enumType;
    private IType elementType;
    private Dictionary<int, IType> arrayTypes;
    private System.Type arrayCLRType;
    private System.Type byRefCLRType;
    private IType[] interfaces;
    private bool baseTypeInitialized;
    private bool interfaceInitialized;
    private List<ILType> genericInstances;
    private bool isDelegate;
    private ILRuntimeType reflectionType;
    private ILType genericDefinition;
    private IType firstCLRBaseType;
    private IType firstCLRInterface;
    private int hashCode;
    private bool mToStringGot;
    private bool mEqualsGot;
    private bool mGetHashCodeGot;
    private IMethod mToString;
    private IMethod mEquals;
    private IMethod mGetHashCode;
    private bool? isValueType;
    private string fullName;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILType(TypeReference def, ILRuntime.Runtime.Enviorment.AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    public TypeDefinition TypeDefinition
    {
      get
      {
        return this.definition;
      }
    }

    public IMethod ToStringMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IMethod EqualsMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IMethod GetHashCodeMethod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TypeReference TypeReference
    {
      get
      {
        return this.typeRef;
      }
      set
      {
        this.typeRef = value;
        this.RetriveDefinitino(value);
      }
    }

    public IType BaseType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType[] Implements
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ILTypeStaticInstance StaticInstance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType[] FieldTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType[] StaticFieldTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FieldDefinition[] StaticFieldDefinitions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<string, int> FieldMapping
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType FirstCLRBaseType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType FirstCLRInterface
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsGenericParameter
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<string, int> StaticFieldMapping
    {
      get
      {
        return this.staticFieldMapping;
      }
    }

    public ILRuntime.Runtime.Enviorment.AppDomain AppDomain
    {
      get
      {
        return this.appdomain;
      }
    }

    internal int FieldStartIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalFieldCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RetriveDefinitino(TypeReference def)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsGenericInstance
    {
      get
      {
        return this.genericArguments != null;
      }
    }

    public ILType GetGenericDefinition()
    {
      return this.genericDefinition;
    }

    public KeyValuePair<string, IType>[] GenericArguments
    {
      get
      {
        return this.genericArguments;
      }
    }

    public IType ElementType
    {
      get
      {
        return this.elementType;
      }
    }

    public bool IsArray { get; private set; }

    public int ArrayRank { get; private set; }

    public bool IsByRef
    {
      get
      {
        return this.typeRef.IsByReference;
      }
    }

    public bool IsValueType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsDelegate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPrimitive
    {
      get
      {
        return false;
      }
    }

    public bool IsInterface
    {
      get
      {
        return this.TypeDefinition.IsInterface;
      }
    }

    public System.Type TypeForCLR
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public System.Type ReflectionType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType ByRefType
    {
      get
      {
        return this.byRefType;
      }
    }

    public IType ArrayType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsEnum
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string FullName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      get
      {
        return this.typeRef.Name;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<IMethod> GetMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeInterfaces()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeBaseType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetMethod(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetMethod(string name, int paramCount, bool declaredOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeMethods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetVirtualMethod(IMethod method)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetMethod(
      string name,
      List<IType> param,
      IType[] genericArguments,
      IType returnType = null,
      bool declaredOnly = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckGenericArguments(ILMethod i, IType[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ILMethod CheckGenericParams(ILMethod i, List<IType> param, ref bool match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ILMethod> GetConstructors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetStaticConstroctor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetConstructor(int paramCnt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod GetConstructor(List<IType> param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFieldIndex(object token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType GetField(string name, out int fieldIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType GetField(int fieldIdx, out FieldDefinition fd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeFields()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType FindGenericArgument(string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAssignTo(IType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ILTypeInstance Instantiate(bool callDefaultConstructor = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ILTypeInstance Instantiate(object[] args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeGenericInstance(KeyValuePair<string, IType>[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeByRefType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType MakeArrayType(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType ResolveGenericType(IType contextType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStaticFieldSizeInMemory(HashSet<object> traversed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMethodBodySizeInMemory()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    public override string ToString()
    {
      return this.FullName;
    }
  }
}
