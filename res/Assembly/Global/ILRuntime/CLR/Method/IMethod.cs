﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.Method.IMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using System.Collections.Generic;

namespace ILRuntime.CLR.Method
{
  public interface IMethod
  {
    string Name { get; }

    int ParameterCount { get; }

    bool HasThis { get; }

    IType DeclearingType { get; }

    IType ReturnType { get; }

    List<IType> Parameters { get; }

    int GenericParameterCount { get; }

    bool IsGenericInstance { get; }

    bool IsConstructor { get; }

    bool IsDelegateInvoke { get; }

    bool IsStatic { get; }

    IMethod MakeGenericMethod(IType[] genericArguments);
  }
}
