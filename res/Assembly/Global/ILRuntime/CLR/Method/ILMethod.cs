﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.Method.ILMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using ILRuntime.Mono.Cecil;
using ILRuntime.Mono.Cecil.Cil;
using ILRuntime.Mono.Collections.Generic;
using ILRuntime.Reflection;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.CLR.Method
{
  public class ILMethod : IMethod
  {
    private static int instance_id = 268435456;
    private ILRuntime.Runtime.Intepreter.OpCodes.OpCode[] body;
    private MethodDefinition def;
    private List<IType> parameters;
    private AppDomain appdomain;
    private ILType declaringType;
    private ILRuntime.CLR.Method.ExceptionHandler[] exceptionHandler;
    private KeyValuePair<string, IType>[] genericParameters;
    private IType[] genericArguments;
    private Dictionary<int, int[]> jumptables;
    private bool isDelegateInvoke;
    private ILRuntimeMethodInfo refletionMethodInfo;
    private ILRuntimeConstructorInfo reflectionCtorInfo;
    private int paramCnt;
    private int localVarCnt;
    private Collection<VariableDefinition> variables;
    private int hashCode;
    private string cachedName;

    [MethodImpl((MethodImplOptions) 32768)]
    public ILMethod(MethodDefinition def, ILType type, AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    public MethodDefinition Definition
    {
      get
      {
        return this.def;
      }
    }

    public Dictionary<int, int[]> JumpTables
    {
      get
      {
        return this.jumptables;
      }
    }

    internal IDelegateAdapter DelegateAdapter { get; set; }

    internal int StartLine { get; set; }

    internal int EndLine { get; set; }

    public MethodInfo ReflectionMethodInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConstructorInfo ReflectionConstructorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    internal ILRuntime.CLR.Method.ExceptionHandler[] ExceptionHandler
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      get
      {
        return this.def.Name;
      }
    }

    public IType DeclearingType
    {
      get
      {
        return (IType) this.declaringType;
      }
    }

    public bool HasThis
    {
      get
      {
        return this.def.HasThis;
      }
    }

    public int GenericParameterCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsGenericInstance
    {
      get
      {
        return this.genericParameters != null;
      }
    }

    public Collection<VariableDefinition> Variables
    {
      get
      {
        return this.variables;
      }
    }

    public KeyValuePair<string, IType>[] GenericArguments
    {
      get
      {
        return this.genericParameters;
      }
    }

    public IType[] GenericArugmentsArray
    {
      get
      {
        return this.genericArguments;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private SequencePoint GetValidSequence(int startIdx, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IType FindGenericArgument(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    internal ILRuntime.Runtime.Intepreter.OpCodes.OpCode[] Body
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasBody
    {
      get
      {
        return this.body != null;
      }
    }

    public int LocalVariableCount
    {
      get
      {
        return this.localVarCnt;
      }
    }

    public bool IsConstructor
    {
      get
      {
        return this.def.IsConstructor;
      }
    }

    public bool IsDelegateInvoke
    {
      get
      {
        return this.isDelegateInvoke;
      }
    }

    public bool IsStatic
    {
      get
      {
        return this.def.IsStatic;
      }
    }

    public int ParameterCount
    {
      get
      {
        return this.paramCnt;
      }
    }

    public List<IType> Parameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType ReturnType { get; private set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Prewarm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCodeBody()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitToken(ref ILRuntime.Runtime.Intepreter.OpCodes.OpCode code, object token, Dictionary<Instruction, int> addr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetTypeTokenHashCode(object token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckHasGenericParamter(object token)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrepareJumpTable(object token, Dictionary<Instruction, int> addr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitParameters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod MakeGenericMethod(IType[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
