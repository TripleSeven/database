﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.Method.CLRMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.CLR.Method
{
  public class CLRMethod : IMethod
  {
    private static int instance_id = 536870912;
    private MethodInfo def;
    private ConstructorInfo cDef;
    private List<IType> parameters;
    private ParameterInfo[] parametersCLR;
    private ILRuntime.Runtime.Enviorment.AppDomain appdomain;
    private CLRType declaringType;
    private ParameterInfo[] param;
    private bool isConstructor;
    private CLRRedirectionDelegate redirect;
    private IType[] genericArguments;
    private Type[] genericArgumentsCLR;
    private object[] invocationParam;
    private bool isDelegateInvoke;
    private int hashCode;

    [MethodImpl((MethodImplOptions) 32768)]
    internal CLRMethod(MethodInfo def, CLRType type, ILRuntime.Runtime.Enviorment.AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal CLRMethod(ConstructorInfo def, CLRType type, ILRuntime.Runtime.Enviorment.AppDomain domain)
    {
      // ISSUE: unable to decompile the method.
    }

    public IType DeclearingType
    {
      get
      {
        return (IType) this.declaringType;
      }
    }

    public string Name
    {
      get
      {
        return this.def.Name;
      }
    }

    public bool HasThis
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GenericParameterCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsGenericInstance
    {
      get
      {
        return this.genericArguments != null;
      }
    }

    public bool IsDelegateInvoke
    {
      get
      {
        return this.isDelegateInvoke;
      }
    }

    public bool IsStatic
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CLRRedirectionDelegate Redirection
    {
      get
      {
        return this.redirect;
      }
    }

    public MethodInfo MethodInfo
    {
      get
      {
        return this.def;
      }
    }

    public ConstructorInfo ConstructorInfo
    {
      get
      {
        return this.cDef;
      }
    }

    public IType[] GenericArguments
    {
      get
      {
        return this.genericArguments;
      }
    }

    public Type[] GenericArgumentsCLR
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ParameterCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<IType> Parameters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ParameterInfo[] ParametersCLR
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IType ReturnType { get; private set; }

    public bool IsConstructor
    {
      get
      {
        return this.cDef != null;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitParameters()
    {
      // ISSUE: unable to decompile the method.
    }

    private unsafe StackObject* Minus(StackObject* a, int b)
    {
      return (StackObject*) ((ulong) a - (ulong) (sizeof (StackObject) * b));
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public object Invoke(
      ILIntepreter intepreter,
      StackObject* esp,
      IList<object> mStack,
      bool isNewObj = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FixReference(
      int paramCount,
      StackObject* esp,
      object[] param,
      IList<object> mStack,
      object instance,
      bool hasThis)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IMethod MakeGenericMethod(IType[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
