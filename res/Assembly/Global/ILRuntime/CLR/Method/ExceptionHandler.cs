﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.Method.ExceptionHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.TypeSystem;

namespace ILRuntime.CLR.Method
{
  internal class ExceptionHandler
  {
    public ExceptionHandlerType HandlerType { get; set; }

    public int TryStart { get; set; }

    public int TryEnd { get; set; }

    public int HandlerStart { get; set; }

    public int HandlerEnd { get; set; }

    public IType CatchType { get; set; }
  }
}
