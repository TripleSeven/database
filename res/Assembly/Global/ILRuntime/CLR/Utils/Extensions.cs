﻿// Decompiled with JetBrains decompiler
// Type: ILRuntime.CLR.Utils.Extensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace ILRuntime.CLR.Utils
{
  public static class Extensions
  {
    public static List<IType> EmptyParamList;
    private static readonly Dictionary<System.Type, Extensions.TypeFlags> typeFlags;

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<IType> GetParamList(
      this MethodReference def,
      ILRuntime.Runtime.Enviorment.AppDomain appdomain,
      IType contextType,
      IMethod contextMethod,
      IType[] genericArguments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ReplaceGenericArgument(
      string typename,
      string argumentName,
      string argumentType,
      bool isGA = false)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool FastIsEnum(this System.Type pt)
    {
      return (pt.GetTypeFlags() & Extensions.TypeFlags.IsEnum) != Extensions.TypeFlags.Default;
    }

    public static bool FastIsByRef(this System.Type pt)
    {
      return (pt.GetTypeFlags() & Extensions.TypeFlags.IsByRef) != Extensions.TypeFlags.Default;
    }

    public static bool FastIsPrimitive(this System.Type pt)
    {
      return (pt.GetTypeFlags() & Extensions.TypeFlags.IsPrimitive) != Extensions.TypeFlags.Default;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool FastIsValueType(this System.Type pt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Extensions.TypeFlags GetTypeFlags(this System.Type pt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object CheckCLRTypes(this System.Type pt, object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckMethodParams(this MethodInfo m, System.Type[] args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckMethodParams(this MethodInfo m, ParameterInfo[] args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static Extensions()
    {
      // ISSUE: unable to decompile the method.
    }

    [Flags]
    public enum TypeFlags
    {
      Default = 0,
      IsPrimitive = 1,
      IsByRef = 2,
      IsEnum = 4,
      IsDelegate = 8,
      IsValueType = 16, // 0x00000010
    }
  }
}
