﻿// Decompiled with JetBrains decompiler
// Type: Lua_BlackJack_ProjectL_Common_DataSectionMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

[Preserve]
public class Lua_BlackJack_ProjectL_Common_DataSectionMission : LuaObject
{
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int constructor(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int ClearInitedData(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int SerializeToClient(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int InitEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int InitOneOffMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int InitFinishedEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int InitFinishedOneOffMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int ResetEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddOneOffMission(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddEverydayMission(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int FinishOneOffMission(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int FinishEverydayMission(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int AddMissionProcess(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int SetMissionProcess(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int RemoveEveryDayMission(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetExistMissionIds(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int IsProcessingMission(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int GetAllProcessingMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_ProcessingEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ProcessingEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_FinishedEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_FinishedEverydayMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_ProcessingOneOffMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ProcessingOneOffMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_FinishedOneOffMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_FinishedOneOffMissions(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static void reg(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }
}
