﻿// Decompiled with JetBrains decompiler
// Type: EnableUserGuide
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.UI;

public class EnableUserGuide : IDebugCmd
{
  public void Execute(string strParams)
  {
    UserGuideUITask.Enable = bool.Parse(strParams);
  }

  public string GetHelpDesc()
  {
    return "eug true : 启用新手引导；eug false : 关闭新手引导；";
  }

  public string GetName()
  {
    return "eug";
  }
}
