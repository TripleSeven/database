﻿// Decompiled with JetBrains decompiler
// Type: FixMath.NET.Fix64
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace FixMath.NET
{
  public struct Fix64 : IEquatable<Fix64>, IComparable<Fix64>
  {
    private readonly long m_rawValue;
    public static readonly Decimal Precision;
    public static readonly Fix64 MaxValue;
    public static readonly Fix64 MinValue;
    public static readonly Fix64 One;
    public static readonly Fix64 Zero;
    public static readonly Fix64 Pi;
    public static readonly Fix64 PiOver2;
    public static readonly Fix64 PiTimes2;
    public static readonly Fix64 PiInv;
    public static readonly Fix64 PiOver2Inv;
    private static readonly Fix64 LutInterval;
    private const long MAX_VALUE = 9223372036854775807;
    private const long MIN_VALUE = -9223372036854775808;
    private const int NUM_BITS = 64;
    private const int FRACTIONAL_PLACES = 32;
    private const long ONE = 4294967296;
    private const long PI_TIMES_2 = 26986075409;
    private const long PI = 13493037704;
    private const long PI_OVER_2 = 6746518852;
    private const int LUT_SIZE = 205887;

    private Fix64(long rawValue)
    {
      this.m_rawValue = rawValue;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Fix64(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int Sign(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Abs(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 FastAbs(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Floor(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Ceiling(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Round(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 operator +(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 FastAdd(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 operator -(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 FastSub(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static long AddOverflowHelper(long x, long y, ref bool overflow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 operator *(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 FastMul(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CountLeadingZeroes(ulong x)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 operator /(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 operator %(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 FastMod(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 operator -(Fix64 x)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator ==(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator !=(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator >(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator <(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator >=(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator <=(Fix64 x, Fix64 y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Sqrt(Fix64 x)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static long ClampSinValue(long angle, out bool flipHorizontal, out bool flipVertical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Atan2(Fix64 y, Fix64 x)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static explicit operator Fix64(long value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static explicit operator long(Fix64 value)
    {
      return value.m_rawValue >> 32;
    }

    public static explicit operator Fix64(float value)
    {
      return new Fix64((long) ((double) value * 4294967296.0));
    }

    public static explicit operator float(Fix64 value)
    {
      return (float) value.m_rawValue / (float) uint.MaxValue;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static explicit operator Fix64(double value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static explicit operator double(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static explicit operator Fix64(Decimal value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static explicit operator Decimal(Fix64 value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Equals(Fix64 other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CompareTo(Fix64 other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    public static Fix64 FromRaw(long rawValue)
    {
      return new Fix64(rawValue);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void GenerateSinLut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static void GenerateTanLut()
    {
      // ISSUE: unable to decompile the method.
    }

    public long RawValue
    {
      get
      {
        return this.m_rawValue;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static Fix64()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
