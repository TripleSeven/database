﻿// Decompiled with JetBrains decompiler
// Type: Lua_BlackJack_ConfigData_ConfigDataCollectionActivityLootLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

[Preserve]
public class Lua_BlackJack_ConfigData_ConfigDataCollectionActivityLootLevelInfo : LuaObject
{
  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int constructor(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_m_battleInfo(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_m_battleInfo(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_WaypointInfo(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_WaypointInfo(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_CollectionActivity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_CollectionActivity(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_ID(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_ID(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_Name(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_Name(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_TeamName(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_TeamName(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PrevLevel(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PrevLevel(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_DaysBeforeActivate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_DaysBeforeActivate(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PlayerLevelRequired(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PlayerLevelRequired(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_MonsterLevel(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_MonsterLevel(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_BattleID(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_BattleID(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_PlayerExp(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_PlayerExp(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_HeroExp(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_HeroExp(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_GoldReward(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_GoldReward(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_EnergyCostSuccess(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_EnergyCostSuccess(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_EnergyCostFail(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_EnergyCostFail(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_EnergyCostTeam(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_EnergyCostTeam(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_FirstClearDrop(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_FirstClearDrop(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_RandomDrop(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_RandomDrop(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_TeamDrop(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_TeamDrop(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_DropDisplayCount(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_DropDisplayCount(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_Score(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_Score(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int get_Strategy(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int set_Strategy(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static void reg(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }
}
