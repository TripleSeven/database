﻿// Decompiled with JetBrains decompiler
// Type: Lua_BlackJack_ProjectL_UI_InviteNotifyUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.UI;
using SLua;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

[Preserve]
public class Lua_BlackJack_ProjectL_UI_InviteNotifyUITask : LuaObject
{
  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int constructor(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static int OnNewIntent(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int StartUITask_s(IntPtr l)
  {
    InviteNotifyUITask.StartUITask();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int CheckPendingInviteInfo_s(IntPtr l)
  {
    InviteNotifyUITask.CheckPendingInviteInfo();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  public static int DisableDisplay_s(IntPtr l)
  {
    InviteNotifyUITask.DisableDisplay();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [MonoPInvokeCallback(typeof (LuaCSFunction))]
  [Preserve]
  public static int EnableDispaly_s(IntPtr l)
  {
    InviteNotifyUITask.EnableDispaly();
    LuaObject.pushValue(l, true);
    return 1;
  }

  [Preserve]
  [MethodImpl((MethodImplOptions) 32768)]
  public static void reg(IntPtr l)
  {
    // ISSUE: unable to decompile the method.
  }
}
