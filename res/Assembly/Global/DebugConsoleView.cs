﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleView
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DebugConsoleView
{
  public Action OnOKButtonClick;
  public Action OnCloseButtonClick;
  private const int CONSOLE_MARGIN_X = 5;
  private const int CONSOLE_MARGIN_Y = 5;
  private const int SWITCHBTN_MARGIN_X = 5;
  private const int SWITCHBTN_MARGIN_Y = 5;
  private const int SWITCHBTN_WIDTH = 30;
  private const int SWITCHBTN_HEIGHT = 30;
  private int _lastFrameTouchTapCount;
  private const string CONSOLE_INPUTFIELD_NAME = "ConsoleInputField";
  private bool _ConsoleInputFieldFocusIsSet;
  private const int OKBUTTON_SIZE = 50;
  private int _consoleWidth;
  private int _consoleHeight;
  private int _preViewWidth;
  private int _preViewHeight;
  private Rect _windowRect;
  private string _instructionText;
  private Vector2 _outputScrollViewVector;
  private string _logText;
  private bool _isSwitchOn;
  private bool _isAutoScrollView;
  private bool m_isForceHide;
  private static DebugConsoleView _instance;
  private float _leftAndRightButtonDownTime;
  private float _fiveFingersDownTime;
  private float _nextUpdateFPSTime;
  private float _fps;
  private double _allocatedMemorySize;
  private double _maxAllocatedMemorySize;
  private GUIStyle m_labelStyle;
  private GUIStyle m_textfieldStyle;
  private const float ScreenDpi2FontSize = 0.14f;

  [MethodImpl((MethodImplOptions) 32768)]
  private DebugConsoleView()
  {
    // ISSUE: unable to decompile the method.
  }

  public event EventHandler<KeyDownEventArgs> OnKeyDown
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public event EventHandler<KeyUpEventArgs> OnKeyUp
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool IsForceHide
  {
    get
    {
      return this.m_isForceHide;
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool IsSwitchOn
  {
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
    get
    {
      return this._isSwitchOn;
    }
  }

  private bool IsAutoScrollView
  {
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
    get
    {
      return this._isAutoScrollView;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugConsoleView Create()
  {
    // ISSUE: unable to decompile the method.
  }

  private void _Init()
  {
    this.LoadData();
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Show()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ShowSwitchInput()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ShowSwitchTouch()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ShowConsole(int x, int y, int width, int height)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private Rect _ComputeSwitchBtnRect(int viewWidth, int viewHeight)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private string _GetSwitchButtonText()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _ProcessKeyboard()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _WindowFunction(int id)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawMainConsoleCtrl()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawInputBox_And_OKButton()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawInputBox()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawOKButton()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawCloseButton()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _DrawOutputArea()
  {
    // ISSUE: unable to decompile the method.
  }

  public string LogText
  {
    set
    {
      this._logText = value;
    }
  }

  public string InstructionText
  {
    get
    {
      return this._instructionText;
    }
    set
    {
      this._instructionText = value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetFontSize(int size)
  {
    // ISSUE: unable to decompile the method.
  }

  public static DebugConsoleView instance
  {
    get
    {
      return DebugConsoleView._instance;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ShowNextCommand()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ShowPreviousCommand()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void SaveData()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void LoadData()
  {
    // ISSUE: unable to decompile the method.
  }
}
