﻿// Decompiled with JetBrains decompiler
// Type: SaveWav
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;

public static class SaveWav
{
  private const int HEADER_SIZE = 44;

  [MethodImpl((MethodImplOptions) 32768)]
  public static bool Save(string filename, AudioClip clip)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static AudioClip TrimSilence(AudioClip clip, float min)
  {
    // ISSUE: unable to decompile the method.
  }

  public static AudioClip TrimSilence(
    List<float> samples,
    float min,
    int channels,
    int hz)
  {
    return SaveWav.TrimSilence(samples, min, channels, hz, false, false);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static AudioClip TrimSilence(
    List<float> samples,
    float min,
    int channels,
    int hz,
    bool _3D,
    bool stream)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static FileStream CreateEmpty(string filepath)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void ConvertAndWrite(FileStream fileStream, AudioClip clip)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void WriteHeader(FileStream fileStream, AudioClip clip)
  {
    // ISSUE: unable to decompile the method.
  }
}
