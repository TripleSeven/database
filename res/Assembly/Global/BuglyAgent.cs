﻿// Decompiled with JetBrains decompiler
// Type: BuglyAgent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public sealed class BuglyAgent
{
  private static bool _isInitialized;
  private static LogSeverity _autoReportLogLevel;
  private static int _crashReporterType;
  private static bool _debugMode;
  private static bool _autoQuitApplicationAfterReport;
  private static readonly int EXCEPTION_TYPE_UNCAUGHT;
  private static readonly int EXCEPTION_TYPE_CAUGHT;
  private static readonly string _pluginVersion;
  private static Func<Dictionary<string, string>> _LogCallbackExtrasHandler;
  private static bool _uncaughtAutoReportOnce;

  public static void ConfigCrashReporter(int type, int logLevel)
  {
    BuglyAgent._SetCrashReporterType(type);
    BuglyAgent._SetCrashReporterLogLevel(logLevel);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void InitWithAppId(string appId)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void EnableExceptionHandler()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void RegisterLogCallback(BuglyAgent.LogCallbackDelegate handler)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void SetLogCallbackExtrasHandler(Func<Dictionary<string, string>> handler)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ReportException(Exception e, string message)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ReportException(string name, string message, string stackTrace)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void UnregisterLogCallback(BuglyAgent.LogCallbackDelegate handler)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void SetUserId(string userId)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void SetScene(int sceneId)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void AddSceneData(string key, string value)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ConfigDebugMode(bool enable)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void ConfigAutoQuitApplication(bool autoQuit)
  {
    BuglyAgent._autoQuitApplicationAfterReport = autoQuit;
  }

  public static void ConfigAutoReportLogLevel(LogSeverity level)
  {
    BuglyAgent._autoReportLogLevel = level;
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void ConfigDefault(string channel, string version, string user, long delay)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DebugLog(string tag, string format, params object[] args)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void PrintLog(LogSeverity level, string format, params object[] args)
  {
    // ISSUE: unable to decompile the method.
  }

  private static void InitBuglyAgent(string appId)
  {
  }

  private static void ConfigDefaultBeforeInit(
    string channel,
    string version,
    string user,
    long delay)
  {
  }

  private static void EnableDebugMode(bool enable)
  {
  }

  private static void SetUserInfo(string userInfo)
  {
  }

  private static void ReportException(
    int type,
    string name,
    string message,
    string stackTrace,
    bool quitProgram)
  {
  }

  private static void SetCurrentScene(int sceneId)
  {
  }

  private static void AddKeyAndValueInScene(string key, string value)
  {
  }

  private static void AddExtraDataWithException(string key, string value)
  {
  }

  private static void LogRecord(LogSeverity level, string message)
  {
  }

  private static void SetUnityVersion()
  {
  }

  private static event BuglyAgent.LogCallbackDelegate _LogCallbackEventHandler
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public static string PluginVersion
  {
    get
    {
      return BuglyAgent._pluginVersion;
    }
  }

  public static bool IsInitialized
  {
    get
    {
      return BuglyAgent._isInitialized;
    }
  }

  public static bool AutoQuitApplicationAfterReport
  {
    get
    {
      return BuglyAgent._autoQuitApplicationAfterReport;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _SetCrashReporterType(int type)
  {
    // ISSUE: unable to decompile the method.
  }

  private static void _SetCrashReporterLogLevel(int logLevel)
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _RegisterExceptionHandler()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _UnregisterExceptionHandler()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _OnLogCallbackHandler(string condition, string stackTrace, LogType type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _OnUncaughtExceptionHandler(object sender, UnhandledExceptionEventArgs args)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _HandleException(Exception e, string message, bool uncaught)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _reportException(
    bool uncaught,
    string name,
    string reason,
    string stackTrace)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void _HandleException(
    LogSeverity logLevel,
    string name,
    string message,
    string stackTrace,
    bool uncaught)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  static BuglyAgent()
  {
    // ISSUE: unable to decompile the method.
  }

  public delegate void LogCallbackDelegate(string condition, string stackTrace, LogType type);
}
