﻿// Decompiled with JetBrains decompiler
// Type: DebugConsoleMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;

public class DebugConsoleMode : IConsoleMode
{
  private const int _maxLogLength = 10000;
  private StringBuilder _logText;
  private StringBuilder _removedLogText;
  private DebugCmdManager _debugCmdManager;
  private static DebugConsoleMode _instance;
  private List<string> _includeFilterStrings;
  private List<string> _excludeFilterStrings;
  private bool _enableRuntimeLogFile;
  private StreamWriter _runtimeLogWriter;

  [MethodImpl((MethodImplOptions) 32768)]
  private DebugConsoleMode()
  {
    // ISSUE: unable to decompile the method.
  }

  public event DebugConsoleMode.refreshDelegate refreshEvent
  {
    [MethodImpl((MethodImplOptions) 32768)] add
    {
      // ISSUE: unable to decompile the method.
    }
    [MethodImpl((MethodImplOptions) 32768)] remove
    {
      // ISSUE: unable to decompile the method.
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void SetFilterString(List<string> includeStrings, List<string> excludeStrings)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static DebugConsoleMode Create()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void _Init()
  {
    // ISSUE: unable to decompile the method.
  }

  public void _LogReceived(string log)
  {
    this.Log(log);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void _LogCallback(string condition, string stackTrace, LogType type)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ClearLog()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Log(string info)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetLogText()
  {
    return this._logText.ToString();
  }

  public void ProcessCmd(string instruction)
  {
    this._debugCmdManager.RunInstruct(instruction);
  }

  public static DebugConsoleMode instance
  {
    get
    {
      return DebugConsoleMode._instance;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Save()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Log2File(string log)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void EnableRuntimeLogFile(bool isEnable)
  {
    // ISSUE: unable to decompile the method.
  }

  public delegate void refreshDelegate();
}
