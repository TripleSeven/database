﻿// Decompiled with JetBrains decompiler
// Type: KeyUpEventArgs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class KeyUpEventArgs : EventArgs
{
  private readonly KeyCode _keyCode;

  public KeyUpEventArgs(KeyCode keyCode)
  {
    this._keyCode = keyCode;
  }

  public KeyCode KeyCode
  {
    get
    {
      return this._keyCode;
    }
  }
}
