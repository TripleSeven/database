﻿// Decompiled with JetBrains decompiler
// Type: BuglyCallback
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class BuglyCallback
{
  public abstract void OnApplicationLogCallbackHandler(
    string condition,
    string stackTrace,
    LogType type);
}
