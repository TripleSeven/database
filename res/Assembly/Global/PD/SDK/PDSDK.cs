﻿// Decompiled with JetBrains decompiler
// Type: PD.SDK.PDSDK
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace PD.SDK
{
  public class PDSDK : MonoBehaviour
  {
    public static string IOSChannelID = string.Empty;
    public bool isDebug;
    public bool isVerfy;
    public bool IsShowCommandUI;
    public static JsonData goodlistjson;
    public string promotingPayGoodsRegisterID;
    public string stringToEdit;
    public GameObject CommondUIPrefab;
    public bool isExitSuccess;
    public bool m_isCallWebView;
    public const string m_loginTypeLine = "line";
    public const string m_loginTypeGameCenter = "gamecenter";
    public const string m_loginTypeZlongame = "zlongame";
    public const string m_loginTypeGuest = "guest";
    public const string m_loginTypeFacebook = "facebook";
    public const string m_loginTypeGoogle = "google";
    public const string m_loginTypeTwitter = "twitter";
    private static bool isLogin;
    private static bool _isLogining;
    private static bool _isLogouting;
    private static bool isInit;
    private static bool _isIosReview;
    private static PDSDK _instance;
    public static Action<LoginSuccessMsg> m_eventLoginSuccess;
    public static Action<LoginSuccessMsg> m_eventOnSwitchUserSuccess;
    public static Action<string> m_eventLoginFailed;
    public static Action m_eventLogoutSuccess;
    public static Action m_eventInitSuccess;
    public static Action m_eventInitFailed;
    public static Action m_eventDoQuestionSucceed;
    public static Action m_eventAllowToBuySubscription;
    public static Action m_eventDoQuestionFailed;
    public static Action<bool> m_eventOnGetProductsListAck;
    public static Action m_eventOnPaySuccess;
    public static Action m_eventOnPayFailed;
    public static Action m_eventOnPayCancel;
    public Action<string> m_eventOnSDKPromotingPaySuccess;
    public static Action<string> m_eventQRLoginSuccess;
    public static Action<string> m_eventQRLoginFailed;
    public static Action<string> m_eventQRLoginCancel;
    public static Action m_eventOnNeedUpdateWebInfoRedMard;
    public static Action m_eventOnLowMemoryWarning;
    public static Action m_eventOnBindGuestSuccess;
    public static Action<int, int, string> m_eventOnNoticeCenterStateSuccess;
    public static Action<string> m_eventOnNoticeCenterStateFailed;
    private bool m_isRequestPermissionReturn;
    private const string m_appKey = "1537943554762";
    private const string m_downloadUrl = "http://us.zlongame.com/html/dispatch/download.html";
    public const string PDChannelGooglePay = "";
    public const string PDAndroidChannelID = "";
    public const string PDYingYongBaoChannelID = "";
    public const string PDOppoChannelID = "";

    [MethodImpl((MethodImplOptions) 32768)]
    public PDSDK()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsLogin
    {
      get
      {
        return PDSDK.isLogin;
      }
    }

    public static bool IsInit
    {
      get
      {
        return PDSDK.isInit;
      }
    }

    public static bool IsLogouting
    {
      get
      {
        return PDSDK._isLogouting;
      }
    }

    public static bool IsIosReview
    {
      get
      {
        return PDSDK._isIosReview;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsGooglePlayChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    public static PDSDK Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static bool IsIosLoginWindowsPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DoCoroutine(IEnumerator coroutine, Action onComplete = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public string DownloadClientURL
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Perform(IEnumerator coroutine, Action onComplete = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator AsyncRequestPermission(string permission, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool requestPermission(string permission, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool checkPermission(string permission, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void noticeCenterState(int mode, string noticeID)
    {
      // ISSUE: unable to decompile the method.
    }

    public void onWebViewOpen()
    {
    }

    public void onWebViewClose()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WebInvestigation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Login(string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoginWithType(string login_type, string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Logout(string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartGame(string gameparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetProductsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPayWithLimit(
      int limitLevel,
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPromotingPay(
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onSDKPromotingPaySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onSDKPromotingPayFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onSDKPromotingPayCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPay(
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoPromotingWithLimitPay(
      int limitLevel,
      string goodsName,
      int goodsNumber,
      string goodsId,
      string goodsRegisterId,
      double goodsPrice,
      string customparams,
      string goodsDes)
    {
      // ISSUE: unable to decompile the method.
    }

    public void doAddLocalPush(
      string Title,
      string Content,
      string Date,
      string Hour,
      string Min)
    {
    }

    public void doFirstScreen()
    {
      Debug.Log("PDSDK.doFirstScreen");
    }

    public void SwitchUser()
    {
      Debug.Log("PDSDK.SwitchUser");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void userCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void exit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPustToken()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetDeviceID()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetBattery()
    {
      return 0;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenInvestigation(int enqId = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenInvestigationUserInfo(int enqId = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    public void PathOrder()
    {
      Debug.Log("PDSDK.PathOrder");
    }

    public void ShowPayHelp()
    {
      Debug.Log("PDSDK.ShowPayHelp");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void printGameEventLog(string eventID, string remark = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string doSetExtData(string data, string type)
    {
      // ISSUE: unable to decompile the method.
    }

    public void doStartQRLogin(string customParams)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getChannelID()
    {
      // ISSUE: unable to decompile the method.
    }

    public void doSaveImageToPhotoLibrary(string data)
    {
    }

    public void doQQVIP()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void callWebView(
      string title,
      int fullscreen_flag,
      int title_flag,
      string action,
      string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    public void clearLocalNotifications()
    {
      Debug.Log("PDSDK.clearLocalNotifications");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void callCustomerServiceWeb()
    {
      // ISSUE: unable to decompile the method.
    }

    public void callCustomerService()
    {
      Debug.Log("PDSDK.callCustomerService");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doshare(string data)
    {
      // ISSUE: unable to decompile the method.
    }

    public void doOpenRequestReview()
    {
      Debug.Log("PDSDK.doOpenRequestReview");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UserLoginType()
    {
      // ISSUE: unable to decompile the method.
    }

    public void doUnBind()
    {
      Debug.Log("PDSDK.doUnBind");
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doBindGuest(string bind_type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void setSDKLangue(string langue)
    {
      // ISSUE: unable to decompile the method.
    }

    public void doStartRecord()
    {
    }

    public void doStopRecord()
    {
    }

    public void doBoradcast()
    {
    }

    public void doGetImagePath(int nType, int fromType)
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getSysCountry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getSysLangue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void doOpenFBFanPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string getECID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool isEuCountry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onQRLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onQRLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onQRLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLogoutSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLogoutFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onLogoutCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetProductsListSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetProductsListFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onPaySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onPayFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onPayCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetPayHistorySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onGetPayHistoryFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onInitSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onInitFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onForceUpdate(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onswitchUserSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onswitchUserFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onswitchUserCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onVerifyTokenSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onVerifyTokenFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSetExtDataSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSetExtDataFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onExitSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onExitFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onMemoryWarning(string recode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onShareSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onShareFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onShareCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStartSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStartFail(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStopSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onRecordStopFail(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onBoradcastSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onBoradcastFail(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoQuestionSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoQuestionFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void oncallWebViewSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void oncallWebViewFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void oncallWebViewCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSaveImageToPhotoLibrarySuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onDoSaveImageToPhotoLibraryFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onBindGuestSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onBindGuestFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onBindGuestCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onLangueChangeSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onLangueChangeFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onUnBindSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onUnBindFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onUnBindCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onRequestPermissionSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void onRequestPermissionFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onNoticeCenterStateSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void onNoticeCenterStateFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    public void showAndroidToast(string info)
    {
    }

    public void verifyToken(string info)
    {
      PDSDK_PC.verifyToken(info);
    }
  }
}
