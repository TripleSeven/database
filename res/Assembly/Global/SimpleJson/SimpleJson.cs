﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.SimpleJson
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace SimpleJson
{
  [GeneratedCode("simple-json", "1.0.0")]
  public static class SimpleJson
  {
    private const int TOKEN_NONE = 0;
    private const int TOKEN_CURLY_OPEN = 1;
    private const int TOKEN_CURLY_CLOSE = 2;
    private const int TOKEN_SQUARED_OPEN = 3;
    private const int TOKEN_SQUARED_CLOSE = 4;
    private const int TOKEN_COLON = 5;
    private const int TOKEN_COMMA = 6;
    private const int TOKEN_STRING = 7;
    private const int TOKEN_NUMBER = 8;
    private const int TOKEN_TRUE = 9;
    private const int TOKEN_FALSE = 10;
    private const int TOKEN_NULL = 11;
    private const int BUILDER_CAPACITY = 2000;
    private static readonly char[] EscapeTable;
    private static readonly char[] EscapeCharacters;
    private static IJsonSerializerStrategy _currentJsonSerializerStrategy;
    private static PocoJsonSerializerStrategy _pocoJsonSerializerStrategy;

    [MethodImpl((MethodImplOptions) 32768)]
    static SimpleJson()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object DeserializeObject(string json)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryDeserializeObject(string json, out object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object DeserializeObject(
      string json,
      Type type,
      IJsonSerializerStrategy jsonSerializerStrategy)
    {
      // ISSUE: unable to decompile the method.
    }

    public static object DeserializeObject(string json, Type type)
    {
      return SimpleJson.SimpleJson.DeserializeObject(json, type, (IJsonSerializerStrategy) null);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T DeserializeObject<T>(
      string json,
      IJsonSerializerStrategy jsonSerializerStrategy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T DeserializeObject<T>(string json)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string SerializeObject(
      object json,
      IJsonSerializerStrategy jsonSerializerStrategy)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string SerializeObject(object json)
    {
      return SimpleJson.SimpleJson.SerializeObject(json, SimpleJson.SimpleJson.CurrentJsonSerializerStrategy);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string EscapeToJavascriptString(string jsonString)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static IDictionary<string, object> ParseObject(
      char[] json,
      ref int index,
      ref bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static JsonArray ParseArray(char[] json, ref int index, ref bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object ParseValue(char[] json, ref int index, ref bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ParseString(char[] json, ref int index, ref bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ConvertFromUtf32(int utf32)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object ParseNumber(char[] json, ref int index, ref bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetLastIndexOfNumber(char[] json, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void EatWhitespace(char[] json, ref int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int LookAhead(char[] json, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int NextToken(char[] json, ref int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SerializeValue(
      IJsonSerializerStrategy jsonSerializerStrategy,
      object value,
      StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SerializeObject(
      IJsonSerializerStrategy jsonSerializerStrategy,
      IEnumerable keys,
      IEnumerable values,
      StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SerializeArray(
      IJsonSerializerStrategy jsonSerializerStrategy,
      IEnumerable anArray,
      StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SerializeString(string aString, StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SerializeNumber(object number, StringBuilder builder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsNumeric(object value)
    {
      // ISSUE: unable to decompile the method.
    }

    public static IJsonSerializerStrategy CurrentJsonSerializerStrategy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        SimpleJson.SimpleJson._currentJsonSerializerStrategy = value;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    public static PocoJsonSerializerStrategy PocoJsonSerializerStrategy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
