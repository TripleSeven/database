﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.Reflection.ReflectionUtils
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace SimpleJson.Reflection
{
  [GeneratedCode("reflection-utils", "1.0.0")]
  internal class ReflectionUtils
  {
    private static readonly object[] EmptyObjects = new object[0];

    public static Type GetTypeInfo(Type type)
    {
      return type;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Attribute GetAttribute(MemberInfo info, Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Type GetGenericListElementType(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Attribute GetAttribute(Type objectType, Type attributeType)
    {
      // ISSUE: unable to decompile the method.
    }

    public static Type[] GetGenericTypeArguments(Type type)
    {
      return type.GetGenericArguments();
    }

    public static bool IsTypeGeneric(Type type)
    {
      return ReflectionUtils.GetTypeInfo(type).IsGenericType;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsTypeGenericeCollectionInterface(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsAssignableFrom(Type type1, Type type2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsTypeDictionary(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsNullableType(Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object ToNullableType(object obj, Type nullableType)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsValueType(Type type)
    {
      return ReflectionUtils.GetTypeInfo(type).IsValueType;
    }

    public static IEnumerable<ConstructorInfo> GetConstructors(Type type)
    {
      return (IEnumerable<ConstructorInfo>) type.GetConstructors();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConstructorInfo GetConstructorInfo(
      Type type,
      params Type[] argsType)
    {
      // ISSUE: unable to decompile the method.
    }

    public static IEnumerable<PropertyInfo> GetProperties(Type type)
    {
      return (IEnumerable<PropertyInfo>) type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
    }

    public static IEnumerable<FieldInfo> GetFields(Type type)
    {
      return (IEnumerable<FieldInfo>) type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
    }

    public static MethodInfo GetGetterMethodInfo(PropertyInfo propertyInfo)
    {
      return propertyInfo.GetGetMethod(true);
    }

    public static MethodInfo GetSetterMethodInfo(PropertyInfo propertyInfo)
    {
      return propertyInfo.GetSetMethod(true);
    }

    public static ReflectionUtils.ConstructorDelegate GetContructor(
      ConstructorInfo constructorInfo)
    {
      return ReflectionUtils.GetConstructorByReflection(constructorInfo);
    }

    public static ReflectionUtils.ConstructorDelegate GetContructor(
      Type type,
      params Type[] argsType)
    {
      return ReflectionUtils.GetConstructorByReflection(type, argsType);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ReflectionUtils.ConstructorDelegate GetConstructorByReflection(
      ConstructorInfo constructorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ReflectionUtils.ConstructorDelegate GetConstructorByReflection(
      Type type,
      params Type[] argsType)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ReflectionUtils.GetDelegate GetGetMethod(PropertyInfo propertyInfo)
    {
      return ReflectionUtils.GetGetMethodByReflection(propertyInfo);
    }

    public static ReflectionUtils.GetDelegate GetGetMethod(FieldInfo fieldInfo)
    {
      return ReflectionUtils.GetGetMethodByReflection(fieldInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ReflectionUtils.GetDelegate GetGetMethodByReflection(
      PropertyInfo propertyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ReflectionUtils.GetDelegate GetGetMethodByReflection(
      FieldInfo fieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public static ReflectionUtils.SetDelegate GetSetMethod(PropertyInfo propertyInfo)
    {
      return ReflectionUtils.GetSetMethodByReflection(propertyInfo);
    }

    public static ReflectionUtils.SetDelegate GetSetMethod(FieldInfo fieldInfo)
    {
      return ReflectionUtils.GetSetMethodByReflection(fieldInfo);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ReflectionUtils.SetDelegate GetSetMethodByReflection(
      PropertyInfo propertyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ReflectionUtils.SetDelegate GetSetMethodByReflection(
      FieldInfo fieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate object GetDelegate(object source);

    public delegate void SetDelegate(object source, object value);

    public delegate object ConstructorDelegate(params object[] args);

    public delegate TValue ThreadSafeDictionaryValueFactory<TKey, TValue>(TKey key);

    public sealed class ThreadSafeDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable
    {
      private readonly object _lock = new object();
      private readonly ReflectionUtils.ThreadSafeDictionaryValueFactory<TKey, TValue> _valueFactory;
      private Dictionary<TKey, TValue> _dictionary;

      public ThreadSafeDictionary(
        ReflectionUtils.ThreadSafeDictionaryValueFactory<TKey, TValue> valueFactory)
      {
        this._valueFactory = valueFactory;
      }

      private TValue Get(TKey key)
      {
        TValue obj;
        if (this._dictionary == null || !this._dictionary.TryGetValue(key, out obj))
          return this.AddValue(key);
        return obj;
      }

      private TValue AddValue(TKey key)
      {
        TValue obj1 = this._valueFactory(key);
        lock (this._lock)
        {
          if (this._dictionary == null)
          {
            this._dictionary = new Dictionary<TKey, TValue>();
            this._dictionary[key] = obj1;
          }
          else
          {
            TValue obj2;
            if (this._dictionary.TryGetValue(key, out obj2))
              return obj2;
            this._dictionary = new Dictionary<TKey, TValue>((IDictionary<TKey, TValue>) this._dictionary)
            {
              [key] = obj1
            };
          }
        }
        return obj1;
      }

      public void Add(TKey key, TValue value)
      {
        throw new NotImplementedException();
      }

      public bool ContainsKey(TKey key)
      {
        return this._dictionary.ContainsKey(key);
      }

      public ICollection<TKey> Keys
      {
        get
        {
          return (ICollection<TKey>) this._dictionary.Keys;
        }
      }

      public bool Remove(TKey key)
      {
        throw new NotImplementedException();
      }

      public bool TryGetValue(TKey key, out TValue value)
      {
        value = this[key];
        return true;
      }

      public ICollection<TValue> Values
      {
        get
        {
          return (ICollection<TValue>) this._dictionary.Values;
        }
      }

      public TValue this[TKey key]
      {
        get
        {
          return this.Get(key);
        }
        set
        {
          throw new NotImplementedException();
        }
      }

      public void Add(KeyValuePair<TKey, TValue> item)
      {
        throw new NotImplementedException();
      }

      public void Clear()
      {
        throw new NotImplementedException();
      }

      public bool Contains(KeyValuePair<TKey, TValue> item)
      {
        throw new NotImplementedException();
      }

      public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
      {
        throw new NotImplementedException();
      }

      public int Count
      {
        get
        {
          return this._dictionary.Count;
        }
      }

      public bool IsReadOnly
      {
        get
        {
          throw new NotImplementedException();
        }
      }

      public bool Remove(KeyValuePair<TKey, TValue> item)
      {
        throw new NotImplementedException();
      }

      public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
      {
        return (IEnumerator<KeyValuePair<TKey, TValue>>) this._dictionary.GetEnumerator();
      }

      IEnumerator IEnumerable.GetEnumerator()
      {
        return (IEnumerator) this._dictionary.GetEnumerator();
      }
    }
  }
}
