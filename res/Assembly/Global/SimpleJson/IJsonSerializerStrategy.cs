﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.IJsonSerializerStrategy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.CodeDom.Compiler;

namespace SimpleJson
{
  [GeneratedCode("simple-json", "1.0.0")]
  public interface IJsonSerializerStrategy
  {
    bool TrySerializeNonPrimitiveObject(object input, out object output);

    object DeserializeObject(object value, Type type);
  }
}
