﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.PocoJsonSerializerStrategy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SimpleJson.Reflection;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SimpleJson
{
  [GeneratedCode("simple-json", "1.0.0")]
  public class PocoJsonSerializerStrategy : IJsonSerializerStrategy
  {
    internal IDictionary<Type, ReflectionUtils.ConstructorDelegate> ConstructorCache;
    internal IDictionary<Type, IDictionary<string, ReflectionUtils.GetDelegate>> GetCache;
    internal IDictionary<Type, IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>>> SetCache;
    internal static readonly Type[] EmptyTypes;
    internal static readonly Type[] ArrayConstructorParameterTypes;
    private static readonly string[] Iso8601Format;

    [MethodImpl((MethodImplOptions) 32768)]
    public PocoJsonSerializerStrategy()
    {
      // ISSUE: unable to decompile the method.
    }

    protected virtual string MapClrMemberNameToJsonFieldName(string clrPropertyName)
    {
      return clrPropertyName;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal virtual ReflectionUtils.ConstructorDelegate ContructorDelegateFactory(
      Type key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal virtual IDictionary<string, ReflectionUtils.GetDelegate> GetterValueFactory(
      Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal virtual IDictionary<string, KeyValuePair<Type, ReflectionUtils.SetDelegate>> SetterValueFactory(
      Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool TrySerializeNonPrimitiveObject(object input, out object output)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual object DeserializeObject(object value, Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual object SerializeEnum(Enum p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool TrySerializeKnownTypes(object input, out object output)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool TrySerializeUnknownTypes(object input, out object output)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static PocoJsonSerializerStrategy()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
