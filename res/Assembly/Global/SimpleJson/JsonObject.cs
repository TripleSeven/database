﻿// Decompiled with JetBrains decompiler
// Type: SimpleJson.JsonObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SimpleJson
{
  [EditorBrowsable(EditorBrowsableState.Never)]
  [GeneratedCode("simple-json", "1.0.0")]
  public class JsonObject : IDictionary<string, object>, ICollection<KeyValuePair<string, object>>, IEnumerable<KeyValuePair<string, object>>, IEnumerable
  {
    private readonly Dictionary<string, object> _members;

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonObject(IEqualityComparer<string> comparer)
    {
      // ISSUE: unable to decompile the method.
    }

    public object this[int index]
    {
      get
      {
        return JsonObject.GetAtIndex((IDictionary<string, object>) this._members, index);
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    internal static object GetAtIndex(IDictionary<string, object> obj, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Add(string key, object value)
    {
      this._members.Add(key, value);
    }

    public bool ContainsKey(string key)
    {
      return this._members.ContainsKey(key);
    }

    public ICollection<string> Keys
    {
      get
      {
        return (ICollection<string>) this._members.Keys;
      }
    }

    public bool Remove(string key)
    {
      return this._members.Remove(key);
    }

    public bool TryGetValue(string key, out object value)
    {
      return this._members.TryGetValue(key, out value);
    }

    public ICollection<object> Values
    {
      get
      {
        return (ICollection<object>) this._members.Values;
      }
    }

    public object this[string key]
    {
      get
      {
        return this._members[key];
      }
      set
      {
        this._members[key] = value;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Add(KeyValuePair<string, object> item)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Clear()
    {
      this._members.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Contains(KeyValuePair<string, object> item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Count
    {
      get
      {
        return this._members.Count;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Remove(KeyValuePair<string, object> item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IEnumerator IEnumerable.GetEnumerator()
    {
      // ISSUE: unable to decompile the method.
    }

    public override string ToString()
    {
      return SimpleJson.SimpleJson.SerializeObject((object) this);
    }
  }
}
