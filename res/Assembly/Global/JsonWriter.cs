﻿// Decompiled with JetBrains decompiler
// Type: JsonWriter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

public class JsonWriter
{
  private static NumberFormatInfo number_format = NumberFormatInfo.InvariantInfo;
  private WriterContext context;
  private Stack<WriterContext> ctx_stack;
  private bool has_reached_end;
  private char[] hex_seq;
  private int indentation;
  private int indent_value;
  private StringBuilder inst_string_builder;
  private bool pretty_print;
  private bool validate;
  private TextWriter writer;

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonWriter()
  {
    // ISSUE: unable to decompile the method.
  }

  public JsonWriter(StringBuilder sb)
    : this((TextWriter) new StringWriter(sb))
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public JsonWriter(TextWriter writer)
  {
    // ISSUE: unable to decompile the method.
  }

  public int IndentValue
  {
    get
    {
      return this.indent_value;
    }
    [MethodImpl((MethodImplOptions) 32768)] set
    {
      // ISSUE: unable to decompile the method.
    }
  }

  public bool PrettyPrint
  {
    get
    {
      return this.pretty_print;
    }
    set
    {
      this.pretty_print = value;
    }
  }

  public TextWriter TextWriter
  {
    get
    {
      return this.writer;
    }
  }

  public bool Validate
  {
    get
    {
      return this.validate;
    }
    set
    {
      this.validate = value;
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void DoValidation(Condition cond)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Init()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static void IntToHex(int n, char[] hex)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Indent()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Put(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  private void PutNewline()
  {
    this.PutNewline(true);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void PutNewline(bool add_comma)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void PutString(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Unindent()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public override string ToString()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Reset()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(bool boolean)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(Decimal number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(double number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(int number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(long number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void Write(ulong number)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteArrayEnd()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteArrayStart()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteObjectEnd()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WriteObjectStart()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void WritePropertyName(string property_name)
  {
    // ISSUE: unable to decompile the method.
  }
}
