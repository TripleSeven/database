﻿// Decompiled with JetBrains decompiler
// Type: PDWebViewManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using ZenFulcrum.EmbeddedBrowser;

public class PDWebViewManager : MonoBehaviour
{
  public RawImage MaskGraphic;
  public static PDWebViewManager Manager;
  private PDWebCallBackHandler m_callbackHandler;
  private string m_CurrentUrl;
  private string m_PageType;
  private Browser m_WebBrowser;
  private GameObject m_WebviewCloseBtn;
  private GameObject m_WebviewCloseBg;

  [MethodImpl((MethodImplOptions) 32768)]
  public PDWebViewManager()
  {
    // ISSUE: unable to decompile the method.
  }

  private Browser WebBrowser
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  private GameObject WebviewCloseBtn
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  private GameObject WebviewCloseBg
  {
    [MethodImpl((MethodImplOptions) 32768)] get
    {
      // ISSUE: unable to decompile the method.
    }
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ShowWebviewCloseBtn(bool isShow, string pageType = "")
  {
    // ISSUE: unable to decompile the method.
  }

  public void ShowWebviewCloseBg(bool isShow)
  {
    this.WebviewCloseBg.SetActive(isShow);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Awake()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Start()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void RegisterJSFuncAll()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void OpenWebBrowser(string url, float width = 0.0f, float height = 0.0f)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void CloseWebBrowser()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public void ClearCookie()
  {
    // ISSUE: unable to decompile the method.
  }

  [DebuggerHidden]
  [MethodImpl((MethodImplOptions) 32768)]
  private IEnumerator DelayShow()
  {
    // ISSUE: unable to decompile the method.
  }
}
