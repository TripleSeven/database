﻿// Decompiled with JetBrains decompiler
// Type: Debug
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

public static class Debug
{
  public static Thread m_mainThread;

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Log(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Log(params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogWarning(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogWarning(params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogError(string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void LogError(params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Assert(bool value, string str)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Assert(bool value, params object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  public static void WriteLine(string str)
  {
    Debug.Log(str);
  }

  public static void WriteLine(params object[] paramList)
  {
    Debug.Log(paramList);
  }

  public static void SystemLogException(params object[] paramList)
  {
    Debug.LogError(paramList);
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void Break()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private static string ParamListToString(object[] paramList)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(
    Vector3 start,
    Vector3 end,
    Color color,
    float duration,
    bool depthTest)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(Vector3 start, Vector3 end, Color color)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawLine(Vector3 start, Vector3 end)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(
    Vector3 start,
    Vector3 dir,
    Color color,
    float duration,
    bool depthTest)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(Vector3 start, Vector3 dir, Color color)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(Vector3 start, Vector3 dir)
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
  {
    // ISSUE: unable to decompile the method.
  }
}
