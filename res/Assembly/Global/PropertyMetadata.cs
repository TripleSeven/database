﻿// Decompiled with JetBrains decompiler
// Type: PropertyMetadata
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Reflection;

internal struct PropertyMetadata
{
  public MemberInfo Info;
  public bool IsField;
  public System.Type Type;
}
