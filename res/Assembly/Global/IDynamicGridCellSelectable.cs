﻿// Decompiled with JetBrains decompiler
// Type: IDynamicGridCellSelectable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

public interface IDynamicGridCellSelectable
{
  bool IsSelected { get; set; }
}
