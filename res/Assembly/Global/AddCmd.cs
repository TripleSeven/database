﻿// Decompiled with JetBrains decompiler
// Type: AddCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

public class AddCmd : IDebugCmd
{
  private const string _NAME = "intadd";
  private const string _DESC = "intadd [int] [int]: add two int num.";
  private const string _SCHEMA = "i i";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "intadd [int] [int]: add two int num.";
  }

  public string GetName()
  {
    return "intadd";
  }
}
