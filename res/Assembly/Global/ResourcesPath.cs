﻿// Decompiled with JetBrains decompiler
// Type: ResourcesPath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

public static class ResourcesPath
{
  public static readonly string dataPath;
  public static readonly string streamingAssetsPath;
  public const string PlatformKey = "StandaloneWindows";
  public static readonly string WritePath;
  public static readonly string LocalBasePath;
  public static readonly string LocalDataPath;
  public static readonly string LocalTempPath;

  [MethodImpl((MethodImplOptions) 32768)]
  static ResourcesPath()
  {
    // ISSUE: unable to decompile the method.
  }
}
