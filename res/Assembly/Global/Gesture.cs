﻿// Decompiled with JetBrains decompiler
// Type: Gesture
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Gesture : MonoBehaviour
{
  private float scaleFactor;
  private Vector2 lastPosition1;
  private Vector2 lastPosition2;
  private Vector2 lastSingleTouchPosition;
  public bool useMouse;
  private bool m_IsSingleFinger;
  public Action<Vector2> OnMoveEvent;
  public Action<float> OnScaleEvent;

  [MethodImpl((MethodImplOptions) 32768)]
  public Gesture()
  {
    // ISSUE: unable to decompile the method.
  }

  private void Start()
  {
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Update()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Scale()
  {
    // ISSUE: unable to decompile the method.
  }

  [MethodImpl((MethodImplOptions) 32768)]
  private void Move(Vector2 scenePos)
  {
    // ISSUE: unable to decompile the method.
  }
}
