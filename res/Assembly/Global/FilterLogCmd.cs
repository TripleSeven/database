﻿// Decompiled with JetBrains decompiler
// Type: FilterLogCmd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

public class FilterLogCmd : IDebugCmd
{
  private const string _NAME = "filter";
  private const string _DESC = "filter include [string]: set include filter string,\n filter exclude [string]: set exclude filter string\nfilter clear: clear all filters.\n filter string format: s1;s2;s3...";
  private const string _SCHEMA = "s s";

  [MethodImpl((MethodImplOptions) 32768)]
  public void Execute(string strParams)
  {
    // ISSUE: unable to decompile the method.
  }

  public string GetHelpDesc()
  {
    return "filter include [string]: set include filter string,\n filter exclude [string]: set exclude filter string\nfilter clear: clear all filters.\n filter string format: s1;s2;s3...";
  }

  public string GetName()
  {
    return "filter";
  }
}
