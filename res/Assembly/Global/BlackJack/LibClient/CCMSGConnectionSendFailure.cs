﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.CCMSGConnectionSendFailure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.LibClient
{
  [ProtoContract(Name = "CCMSGConnectionSendFailure")]
  [Serializable]
  public class CCMSGConnectionSendFailure
  {
    private int _MessageId;

    [MethodImpl((MethodImplOptions) 32768)]
    public CCMSGConnectionSendFailure()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "MessageId")]
    public int MessageId
    {
      get
      {
        return this._MessageId;
      }
      set
      {
        this._MessageId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ExceptionInfo")]
    public string ExceptionInfo { get; set; }
  }
}
