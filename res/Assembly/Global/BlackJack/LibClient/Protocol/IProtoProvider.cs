﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.Protocol.IProtoProvider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace BlackJack.LibClient.Protocol
{
  public interface IProtoProvider
  {
    Type GetTypeById(int vId);

    int GetIdByType(Type vType);
  }
}
