﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.ProtoException
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;

namespace BlackJack.LibClient
{
  public class ProtoException : Exception
  {
    public ProtoException(string sDesc)
      : base(sDesc)
    {
    }
  }
}
