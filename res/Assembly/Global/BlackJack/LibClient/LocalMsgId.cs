﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.LocalMsgId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.LibClient
{
  public enum LocalMsgId
  {
    CCMSGConnectionReady = 9001, // 0x00002329
    CCMSGConnectionBreak = 9002, // 0x0000232A
    CCMSGConnectionFailure = 9003, // 0x0000232B
    CCMSGConnectionSendFailure = 9004, // 0x0000232C
    CCMSGConnectionRecvFailure = 9005, // 0x0000232D
  }
}
