﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.CCMSGConnectionFailure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.LibClient
{
  [ProtoContract(Name = "CCMSGConnectionFailure")]
  [Serializable]
  public class CCMSGConnectionFailure
  {
    private int _MessageId;

    [MethodImpl((MethodImplOptions) 32768)]
    public CCMSGConnectionFailure()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "MessageId")]
    public int MessageId
    {
      get
      {
        return this._MessageId;
      }
      set
      {
        this._MessageId = value;
      }
    }
  }
}
