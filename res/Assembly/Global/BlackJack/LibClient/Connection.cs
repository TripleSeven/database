﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.LibClient.Connection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.LibClient.Protocol;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Contexts;

namespace BlackJack.LibClient
{
  [Synchronization]
  public class Connection
  {
    private IProtoProvider m_provider;
    private Socket m_socket;
    private Queue<KeyValuePair<int, object>> m_recvQueue;
    private MessageBlock m_recvCache;
    private int m_lastRecvCacheLength;
    private IPEndPoint m_ipEndPoint;
    private SocketAsyncEventArgs m_connEventArg;
    private SocketAsyncEventArgs m_receiveEventArg;
    private System.Threading.Timer m_disconnectTimer;
    private Func<Stream, Type, int, object> m_messageDeserializeAction;

    [MethodImpl((MethodImplOptions) 32768)]
    public Connection(
      IProtoProvider provider,
      Func<Stream, Type, int, object> deserializeMessageAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize(string remoteAddress, int remotePort, string remoteDomain = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartConnection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Disconnect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public KeyValuePair<int, object> GetMessagePair()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SendMessage(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    private void OnCompletedForSend(object sender, SocketAsyncEventArgs e)
    {
      this.OnCompletedForSendImpl(e);
      e.Dispose();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompletedForSendImpl(SocketAsyncEventArgs e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompletedForConnect(object sender, SocketAsyncEventArgs eventArgs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompletedForReceive(object sender, SocketAsyncEventArgs eventArgs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WriteMsg2RecvCache(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryDecodeMsg()
    {
      // ISSUE: unable to decompile the method.
    }

    public static string GetIPv6String(string mHost, string mPort)
    {
      return mHost + "&&ipv4";
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetIpType(
      string serverIp,
      string serverPorts,
      out string newServerIp,
      out AddressFamily mIpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IPAddress ParseServerAdressFromDomain(string domain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FireEventOnLogPrint(string callFun, string log = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEndpoint()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConnectionState State { get; private set; }

    public event Action<string> EventOnLogPrint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
