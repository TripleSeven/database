﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.Utils.DelayExecHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.Utils
{
  public class DelayExecHelper
  {
    private List<DelayExecHelper.DelayExecItem> m_delayExecList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DelayExecHelper()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DelayExecHelper.IDelayExecItem DelayExec(
      Action action,
      int delayTime,
      uint currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DelayExec(uint execTime, DelayExecHelper.DelayExecItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(uint currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public interface IDelayExecItem
    {
      DelayExecHelper.IDelayExecItem ContinueWith(Action action, int delayTime);
    }

    private class DelayExecItem : DelayExecHelper.IDelayExecItem
    {
      public Action m_action;
      public uint m_execTime;
      public DelayExecHelper.DelayExecItem m_continueItem;

      DelayExecHelper.IDelayExecItem DelayExecHelper.IDelayExecItem.ContinueWith(
        Action action,
        int delayTime)
      {
        this.m_continueItem = new DelayExecHelper.DelayExecItem()
        {
          m_action = action,
          m_execTime = this.m_execTime + (uint) delayTime
        };
        return (DelayExecHelper.IDelayExecItem) this.m_continueItem;
      }
    }
  }
}
