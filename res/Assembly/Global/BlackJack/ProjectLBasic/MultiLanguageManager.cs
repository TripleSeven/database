﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.MultiLanguageManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectLBasic
{
  public class MultiLanguageManager
  {
    private static Dictionary<string, string[]> m_LanguageDict;
    private const string MULTI_LANGUAGE_KEY = "USER_CURRENT_LANGUAGE";
    private const string FORCE_UPDATE_LANGUAGE_RESOURCE_KEY = "FORCE_UPDATE_LANGUAGE_RESOURCE";
    public static string CurrentLanguage;
    private static List<string> s_ForceUpdateBundleList;
    private static List<string> s_SkipUpdateBundleList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitMultiLanguageSetting;
    private static DelegateBridge __Hotfix_SetPDSDKLanguage;
    private static DelegateBridge __Hotfix_ChangeLanguage;
    private static DelegateBridge __Hotfix_IsNeedToForceUpdate;
    private static DelegateBridge __Hotfix_ClearForceUpdateFlag;
    private static DelegateBridge __Hotfix_LoadSetting;
    private static DelegateBridge __Hotfix_GetLanguageDict;
    private static DelegateBridge __Hotfix_GetLanguageList;
    private static DelegateBridge __Hotfix_IsDefaultLanguage;
    private static DelegateBridge __Hotfix_GMJson2String;
    private static DelegateBridge __Hotfix_GetCustomPreUpdateBundleList;
    private static DelegateBridge __Hotfix_GetCustomSkipPreUpdateBundleList;
    private static DelegateBridge __Hotfix_AddBundleNameToSkipUpdateList;
    private static DelegateBridge __Hotfix_AddBundleNameToForceUpdateList;
    private static DelegateBridge __Hotfix_SetConfigDataPath;
    private static DelegateBridge __Hotfix_GetImageResLanguage;

    [MethodImpl((MethodImplOptions) 32768)]
    public MultiLanguageManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool InitMultiLanguageSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetPDSDKLanguage(string language)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ChangeLanguage(string nextLanguage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsNeedToForceUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClearForceUpdateFlag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool LoadSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Dictionary<string, string[]> GetLanguageDict()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetLanguageList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsDefaultLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GMJson2String(string json)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetCustomPreUpdateBundleList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetCustomSkipPreUpdateBundleList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddBundleNameToSkipUpdateList(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddBundleNameToForceUpdateList(string bundleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetConfigDataPath(ConfigDataSettings configSetting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetImageResLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static MultiLanguageManager()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
