﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.DialogBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectLBasic
{
  public class DialogBox : MonoBehaviour
  {
    private Action<DialogBoxResult> m_callback;
    private CommonUIStateController m_uiStateController;
    private CommonUIStateController m_panelUIStateController;
    private Text m_msgText;
    private Button m_okButton;
    private Text m_okButtonText;
    private Button m_cancelButton;
    private Text m_cancelButtonText;
    private GameObject m_backgroundGameObject;
    private DialogBoxResult m_ret;
    private static DelegateBridge __Hotfix_Show_1;
    private static DelegateBridge __Hotfix_Show_0;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Cancel;
    private static DelegateBridge __Hotfix_OnCancel;
    private static DelegateBridge __Hotfix_OnOK;
    private static DelegateBridge __Hotfix_OnClose;
    private static DelegateBridge __Hotfix_OnCloseAnimationFinished;

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator Show(
      GameObject parentObj,
      string prefabPath,
      string msgText,
      string okText,
      string cancelText,
      Action<DialogBoxResult> onClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(
      string msgText,
      string okText,
      string cancelText,
      Action<DialogBoxResult> callback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Cancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOK()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClose(DialogBoxResult ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseAnimationFinished()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
