﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.ShowHideEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  public class ShowHideEvent : MonoBehaviour
  {
    private bool m_isStarted;
    private bool m_bNeedCallShow;
    public string m_eventParam;
    private static DelegateBridge __Hotfix_OnEnable;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_CallOnShowEvent;
    private static DelegateBridge __Hotfix_CallOnHideEvent;
    private static DelegateBridge __Hotfix_get_Param;
    private static DelegateBridge __Hotfix_add_m_onShowEvent;
    private static DelegateBridge __Hotfix_remove_m_onShowEvent;
    private static DelegateBridge __Hotfix_add_m_onHideEvent;
    private static DelegateBridge __Hotfix_remove_m_onHideEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CallOnShowEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CallOnHideEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    private string Param
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<string> m_onShowEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<string> m_onHideEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
