﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.IGameLogicInterface4TargetProject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.PlayerContext;

namespace BlackJack.ProjectLBasic
{
  public interface IGameLogicInterface4TargetProject
  {
    IPlayerContextNetworkClient CreateNetWorkClient();

    void OnApplicationQuit();

    void OnInitialize();
  }
}
