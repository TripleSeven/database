﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.Utility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  public class Utility
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetUpdateClientURL;
    private static DelegateBridge __Hotfix_ResizeTexture;
    private static DelegateBridge __Hotfix_SetUIStateOpen;
    private static DelegateBridge __Hotfix_SetUIStateClose;
    private static DelegateBridge __Hotfix_Co_UIStateCloseFinished;
    private static DelegateBridge __Hotfix_FindChildComponent;
    private static DelegateBridge __Hotfix_FindChildGameObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public Utility()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator GetUpdateClientURL(GameObject parentObj = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Texture2D ResizeTexture(
      Texture2D pSource,
      ImageFilterMode pFilterMode,
      float pScale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpen(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      Action<bool> EnableInput = null,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateClose(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      Action<bool> EnableInput = null,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator<object> Co_UIStateCloseFinished(
      CommonUIStateController ctrl,
      Action onEnd,
      Action<bool> EnableInput)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindChildComponent<T>(GameObject go, string name, bool warning = true) where T : Component
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildGameObject(
      GameObject go,
      string name,
      bool warning = true)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
