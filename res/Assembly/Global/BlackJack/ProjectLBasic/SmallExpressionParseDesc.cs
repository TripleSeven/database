﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectLBasic.SmallExpressionParseDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace BlackJack.ProjectLBasic
{
  [Serializable]
  public class SmallExpressionParseDesc : MonoBehaviour
  {
    [Header("表情和文字的间隙的大小(正方形，左右间隙一致):")]
    public int m_gapBetweenEmAndText;
    [Header("Emoji所占位置大小(和表情自身大小无关):")]
    public int m_emSpace;
    [Header("表情坐标在X轴的偏移:")]
    public float m_offsetX;
    [Header("表情坐标在Y轴的偏移:")]
    public float m_offsetY;
  }
}
