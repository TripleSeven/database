﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointStateType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointStateType")]
  public enum CollectionActivityWaypointStateType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_None", Value = 0)] CollectionActivityWaypointStateType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Hidden", Value = 1)] CollectionActivityWaypointStateType_Hidden,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Locked", Value = 2)] CollectionActivityWaypointStateType_Locked,
    [ProtoEnum(Name = "CollectionActivityWaypointStateType_Unlocked", Value = 3)] CollectionActivityWaypointStateType_Unlocked,
  }
}
