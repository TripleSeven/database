﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTicketLimitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTicketLimitInfo")]
  [Serializable]
  public class ConfigDataTicketLimitInfo : IExtensible
  {
    private int _ID;
    private GameFunctionType _GameFunctionTypeID;
    private int _MaxNums;
    private int _TicketId;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionTypeID")]
    public GameFunctionType GameFunctionTypeID
    {
      get
      {
        return this._GameFunctionTypeID;
      }
      set
      {
        this._GameFunctionTypeID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MaxNums")]
    public int MaxNums
    {
      get
      {
        return this._MaxNums;
      }
      set
      {
        this._MaxNums = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TicketId")]
    public int TicketId
    {
      get
      {
        return this._TicketId;
      }
      set
      {
        this._TicketId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
