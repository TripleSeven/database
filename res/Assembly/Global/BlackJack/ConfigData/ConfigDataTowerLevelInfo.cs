﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTowerLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTowerLevelInfo")]
  [Serializable]
  public class ConfigDataTowerLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _LevelRes;
    private string _ThumbImage;
    private int _Battle_ID;
    private List<int> _BattleRuleList;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "LevelRes")]
    public string LevelRes
    {
      get
      {
        return this._LevelRes;
      }
      set
      {
        this._LevelRes = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "ThumbImage")]
    public string ThumbImage
    {
      get
      {
        return this._ThumbImage;
      }
      set
      {
        this._ThumbImage = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "BattleRuleList")]
    public List<int> BattleRuleList
    {
      get
      {
        return this._BattleRuleList;
      }
      set
      {
        this._BattleRuleList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataBattleInfo BattleInfo { get; set; }
  }
}
