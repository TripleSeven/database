﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ICardPoolItemWeight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 4A4B8AB9-4759-4B30-9525-05012703C3B7
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  public interface ICardPoolItemWeight
  {
    int CardPoolItemID { get; }

    List<Goods> Items { get; }

    int ItemDefaultWeight { get; }
  }
}
