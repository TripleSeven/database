﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.AncientCallBossBehavioralDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "AncientCallBossBehavioralDesc")]
  [Serializable]
  public class AncientCallBossBehavioralDesc : IExtensible
  {
    private int _Step;
    private int _DescId;
    private int _Skill1;
    private int _Skill2;
    private int _Skill3;
    private int _Skill4;
    private int _Skill5;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Step")]
    public int Step
    {
      get
      {
        return this._Step;
      }
      set
      {
        this._Step = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DescId")]
    public int DescId
    {
      get
      {
        return this._DescId;
      }
      set
      {
        this._DescId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill1")]
    public int Skill1
    {
      get
      {
        return this._Skill1;
      }
      set
      {
        this._Skill1 = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill2")]
    public int Skill2
    {
      get
      {
        return this._Skill2;
      }
      set
      {
        this._Skill2 = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill3")]
    public int Skill3
    {
      get
      {
        return this._Skill3;
      }
      set
      {
        this._Skill3 = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill4")]
    public int Skill4
    {
      get
      {
        return this._Skill4;
      }
      set
      {
        this._Skill4 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill5")]
    public int Skill5
    {
      get
      {
        return this._Skill5;
      }
      set
      {
        this._Skill5 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
