﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroInteractionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroInteractionInfo")]
  [Serializable]
  public class ConfigDataHeroInteractionInfo : IExtensible
  {
    private int _ID;
    private List<HeroInteractionWeightResult> _Results;
    private int _NormalResultPerformance;
    private int _SmallUpResultPerformance;
    private int _BigUpResultPerformance;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Results")]
    public List<HeroInteractionWeightResult> Results
    {
      get
      {
        return this._Results;
      }
      set
      {
        this._Results = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NormalResultPerformance")]
    public int NormalResultPerformance
    {
      get
      {
        return this._NormalResultPerformance;
      }
      set
      {
        this._NormalResultPerformance = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SmallUpResultPerformance")]
    public int SmallUpResultPerformance
    {
      get
      {
        return this._SmallUpResultPerformance;
      }
      set
      {
        this._SmallUpResultPerformance = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BigUpResultPerformance")]
    public int BigUpResultPerformance
    {
      get
      {
        return this._BigUpResultPerformance;
      }
      set
      {
        this._BigUpResultPerformance = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
