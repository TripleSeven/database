﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBigExpressionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBigExpressionInfo")]
  [Serializable]
  public class ConfigDataBigExpressionInfo : IExtensible
  {
    private int _ID;
    private string _ExpressionIconPath;
    private int _Group;
    private ExpressionPurposeType _ExpressionPurposeType;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "ExpressionIconPath")]
    public string ExpressionIconPath
    {
      get
      {
        return this._ExpressionIconPath;
      }
      set
      {
        this._ExpressionIconPath = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Group")]
    public int Group
    {
      get
      {
        return this._Group;
      }
      set
      {
        this._Group = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExpressionPurposeType")]
    public ExpressionPurposeType ExpressionPurposeType
    {
      get
      {
        return this._ExpressionPurposeType;
      }
      set
      {
        this._ExpressionPurposeType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
