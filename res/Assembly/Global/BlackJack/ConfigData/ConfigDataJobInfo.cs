﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobInfo")]
  [Serializable]
  public class ConfigDataJobInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _Name_Eng;
    private int _Rank;
    private int _ChangeJobGold;
    private string _UnlockText;
    private int _Army_ID;
    private bool _IsMelee;
    private MoveType _MoveType;
    private int _BF_AttackDistance;
    private int _MeleeATK_ID;
    private int _RangeATK_ID;
    private int _MoveSPD_INI;
    private int _BF_MovePoint;
    private int _BF_ActionValue;
    private int _Behavior;
    private int _DieFlyDistanceMin;
    private int _DieFlyDistanceMax;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private PropertyModifyType _Property3_ID;
    private int _Property3_Value;
    private List<int> _AdvantagePropertyIds;
    private string _JobIcon;
    private int _BattlePowerHP;
    private int _BattlePowerAT;
    private int _BattlePowerMagic;
    private int _BattlePowerDF;
    private int _BattlePowerMagicDF;
    private int _BattlePowerDEX;
    private List<int> _Skills_ID;
    private IExtension extensionObject;
    public ConfigDataArmyInfo m_armyInfo;
    public ConfigDataSkillInfo m_meleeSkillInfo;
    public ConfigDataSkillInfo m_rangeSkillInfo;
    public ConfigDataSkillInfo[] m_jobSkillInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name_Eng")]
    public string Name_Eng
    {
      get
      {
        return this._Name_Eng;
      }
      set
      {
        this._Name_Eng = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChangeJobGold")]
    public int ChangeJobGold
    {
      get
      {
        return this._ChangeJobGold;
      }
      set
      {
        this._ChangeJobGold = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "UnlockText")]
    public string UnlockText
    {
      get
      {
        return this._UnlockText;
      }
      set
      {
        this._UnlockText = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Army_ID")]
    public int Army_ID
    {
      get
      {
        return this._Army_ID;
      }
      set
      {
        this._Army_ID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsMelee")]
    public bool IsMelee
    {
      get
      {
        return this._IsMelee;
      }
      set
      {
        this._IsMelee = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MoveType")]
    public MoveType MoveType
    {
      get
      {
        return this._MoveType;
      }
      set
      {
        this._MoveType = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_AttackDistance")]
    public int BF_AttackDistance
    {
      get
      {
        return this._BF_AttackDistance;
      }
      set
      {
        this._BF_AttackDistance = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MeleeATK_ID")]
    public int MeleeATK_ID
    {
      get
      {
        return this._MeleeATK_ID;
      }
      set
      {
        this._MeleeATK_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RangeATK_ID")]
    public int RangeATK_ID
    {
      get
      {
        return this._RangeATK_ID;
      }
      set
      {
        this._RangeATK_ID = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MoveSPD_INI")]
    public int MoveSPD_INI
    {
      get
      {
        return this._MoveSPD_INI;
      }
      set
      {
        this._MoveSPD_INI = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_MovePoint")]
    public int BF_MovePoint
    {
      get
      {
        return this._BF_MovePoint;
      }
      set
      {
        this._BF_MovePoint = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_ActionValue")]
    public int BF_ActionValue
    {
      get
      {
        return this._BF_ActionValue;
      }
      set
      {
        this._BF_ActionValue = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Behavior")]
    public int Behavior
    {
      get
      {
        return this._Behavior;
      }
      set
      {
        this._Behavior = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DieFlyDistanceMin")]
    public int DieFlyDistanceMin
    {
      get
      {
        return this._DieFlyDistanceMin;
      }
      set
      {
        this._DieFlyDistanceMin = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DieFlyDistanceMax")]
    public int DieFlyDistanceMax
    {
      get
      {
        return this._DieFlyDistanceMax;
      }
      set
      {
        this._DieFlyDistanceMax = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_ID")]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_Value")]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_ID")]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_Value")]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_ID")]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_Value")]
    public int Property3_Value
    {
      get
      {
        return this._Property3_Value;
      }
      set
      {
        this._Property3_Value = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, Name = "AdvantagePropertyIds")]
    public List<int> AdvantagePropertyIds
    {
      get
      {
        return this._AdvantagePropertyIds;
      }
      set
      {
        this._AdvantagePropertyIds = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.Default, IsRequired = true, Name = "JobIcon")]
    public string JobIcon
    {
      get
      {
        return this._JobIcon;
      }
      set
      {
        this._JobIcon = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerHP")]
    public int BattlePowerHP
    {
      get
      {
        return this._BattlePowerHP;
      }
      set
      {
        this._BattlePowerHP = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerAT")]
    public int BattlePowerAT
    {
      get
      {
        return this._BattlePowerAT;
      }
      set
      {
        this._BattlePowerAT = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerMagic")]
    public int BattlePowerMagic
    {
      get
      {
        return this._BattlePowerMagic;
      }
      set
      {
        this._BattlePowerMagic = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerDF")]
    public int BattlePowerDF
    {
      get
      {
        return this._BattlePowerDF;
      }
      set
      {
        this._BattlePowerDF = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerMagicDF")]
    public int BattlePowerMagicDF
    {
      get
      {
        return this._BattlePowerMagicDF;
      }
      set
      {
        this._BattlePowerMagicDF = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerDEX")]
    public int BattlePowerDEX
    {
      get
      {
        return this._BattlePowerDEX;
      }
      set
      {
        this._BattlePowerDEX = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, Name = "Skills_ID")]
    public List<int> Skills_ID
    {
      get
      {
        return this._Skills_ID;
      }
      set
      {
        this._Skills_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
