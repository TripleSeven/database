﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleLoseConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleLoseConditionType")]
  public enum BattleLoseConditionType
  {
    [ProtoEnum(Name = "BattleLoseConditionType_None", Value = 0)] BattleLoseConditionType_None,
    [ProtoEnum(Name = "BattleLoseConditionType_DieAll", Value = 1)] BattleLoseConditionType_DieAll,
    [ProtoEnum(Name = "BattleLoseConditionType_ActorDie", Value = 2)] BattleLoseConditionType_ActorDie,
    [ProtoEnum(Name = "BattleLoseConditionType_ActorReachPosition", Value = 3)] BattleLoseConditionType_ActorReachPosition,
    [ProtoEnum(Name = "BattleLoseConditionType_DieCount", Value = 4)] BattleLoseConditionType_DieCount,
    [ProtoEnum(Name = "BattleLoseConditionType_PlayerDieCount", Value = 5)] BattleLoseConditionType_PlayerDieCount,
    [ProtoEnum(Name = "BattleLoseConditionType_NpcDieCount", Value = 6)] BattleLoseConditionType_NpcDieCount,
    [ProtoEnum(Name = "BattleLoseConditionType_EventTrigger", Value = 7)] BattleLoseConditionType_EventTrigger,
  }
}
