﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GetPathType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GetPathType")]
  public enum GetPathType
  {
    [ProtoEnum(Name = "GetPathType_None", Value = 0)] GetPathType_None,
    [ProtoEnum(Name = "GetPathType_Rift", Value = 1)] GetPathType_Rift,
    [ProtoEnum(Name = "GetPathType_Aniki", Value = 2)] GetPathType_Aniki,
    [ProtoEnum(Name = "GetPathType_ThearchyTrial", Value = 3)] GetPathType_ThearchyTrial,
    [ProtoEnum(Name = "GetPathType_Store1", Value = 4)] GetPathType_Store1,
    [ProtoEnum(Name = "GetPathType_Store2", Value = 5)] GetPathType_Store2,
    [ProtoEnum(Name = "GetPathType_Store3", Value = 6)] GetPathType_Store3,
    [ProtoEnum(Name = "GetPathType_Summon1", Value = 7)] GetPathType_Summon1,
    [ProtoEnum(Name = "GetPathType_Summon2", Value = 8)] GetPathType_Summon2,
    [ProtoEnum(Name = "GetPathType_MemoryCorridor", Value = 9)] GetPathType_MemoryCorridor,
    [ProtoEnum(Name = "GetPathType_HeroTraining", Value = 10)] GetPathType_HeroTraining,
    [ProtoEnum(Name = "GetPathType_Store4", Value = 11)] GetPathType_Store4,
    [ProtoEnum(Name = "GetPathType_Summon3", Value = 12)] GetPathType_Summon3,
    [ProtoEnum(Name = "GetPathType_HeroList", Value = 13)] GetPathType_HeroList,
    [ProtoEnum(Name = "GetPathType_Drill1", Value = 14)] GetPathType_Drill1,
    [ProtoEnum(Name = "GetPathType_Drill2", Value = 15)] GetPathType_Drill2,
    [ProtoEnum(Name = "GetPathType_Fetter", Value = 16)] GetPathType_Fetter,
    [ProtoEnum(Name = "GetPathType_World", Value = 17)] GetPathType_World,
    [ProtoEnum(Name = "GetPathType_Arena", Value = 18)] GetPathType_Arena,
    [ProtoEnum(Name = "GetPathType_Uncharted1", Value = 19)] GetPathType_Uncharted1,
    [ProtoEnum(Name = "GetPathType_Uncharted2", Value = 20)] GetPathType_Uncharted2,
    [ProtoEnum(Name = "GetPathType_Friend", Value = 21)] GetPathType_Friend,
    [ProtoEnum(Name = "GetPathType_TreasureMap", Value = 22)] GetPathType_TreasureMap,
    [ProtoEnum(Name = "GetPathType_Mission", Value = 23)] GetPathType_Mission,
    [ProtoEnum(Name = "GetPathType_HeroDungeon", Value = 24)] GetPathType_HeroDungeon,
    [ProtoEnum(Name = "GetPathType_Store5", Value = 25)] GetPathType_Store5,
    [ProtoEnum(Name = "GetPathType_Store6", Value = 26)] GetPathType_Store6,
    [ProtoEnum(Name = "GetPathType_Activity", Value = 27)] GetPathType_Activity,
    [ProtoEnum(Name = "GetPathType_Store7", Value = 28)] GetPathType_Store7,
    [ProtoEnum(Name = "GetPathType_Store8", Value = 29)] GetPathType_Store8,
    [ProtoEnum(Name = "GetPathType_GuildStore", Value = 30)] GetPathType_GuildStore,
    [ProtoEnum(Name = "GetPathType_EternalShrine", Value = 31)] GetPathType_EternalShrine,
    [ProtoEnum(Name = "GetPathType_Guild", Value = 32)] GetPathType_Guild,
    [ProtoEnum(Name = "GetPathType_GuildGame", Value = 33)] GetPathType_GuildGame,
    [ProtoEnum(Name = "GetPathType_Team", Value = 34)] GetPathType_Team,
    [ProtoEnum(Name = "GetPathType_Friend_Add", Value = 35)] GetPathType_Friend_Add,
    [ProtoEnum(Name = "GetPathType_BuyEnergy", Value = 36)] GetPathType_BuyEnergy,
    [ProtoEnum(Name = "GetPathType_CooperateBattle", Value = 37)] GetPathType_CooperateBattle,
    [ProtoEnum(Name = "GetPathType_ClimbTower", Value = 38)] GetPathType_ClimbTower,
    [ProtoEnum(Name = "GetPathType_Bag_Item", Value = 39)] GetPathType_Bag_Item,
    [ProtoEnum(Name = "GetPathType_Bag_Fragment", Value = 40)] GetPathType_Bag_Fragment,
    [ProtoEnum(Name = "GetPathType_Bag_JobMaterial", Value = 41)] GetPathType_Bag_JobMaterial,
    [ProtoEnum(Name = "GetPathType_Bag_Equipment", Value = 42)] GetPathType_Bag_Equipment,
    [ProtoEnum(Name = "GetPathType_SummonActivity", Value = 43)] GetPathType_SummonActivity,
    [ProtoEnum(Name = "GetPathType_Uncharted3", Value = 44)] GetPathType_Uncharted3,
    [ProtoEnum(Name = "GetPathType_HeroAnthem", Value = 45)] GetPathType_HeroAnthem,
    [ProtoEnum(Name = "GetPathType_UnchartedStore_Equip", Value = 46)] GetPathType_UnchartedStore_Equip,
    [ProtoEnum(Name = "GetPathType_UnchartedStore_Skin", Value = 47)] GetPathType_UnchartedStore_Skin,
    [ProtoEnum(Name = "GetPathType_PlayerInfo", Value = 48)] GetPathType_PlayerInfo,
    [ProtoEnum(Name = "GetPathType_PeakArena", Value = 49)] GetPathType_PeakArena,
    [ProtoEnum(Name = "GetPathType_AncientCall", Value = 50)] GetPathType_AncientCall,
  }
}
