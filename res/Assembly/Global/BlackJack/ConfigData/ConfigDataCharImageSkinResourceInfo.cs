﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCharImageSkinResourceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCharImageSkinResourceInfo")]
  [Serializable]
  public class ConfigDataCharImageSkinResourceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Image;
    private string _SpineAssetPath;
    private IExtension extensionObject;
    public ConfigDataHeroSkinInfo m_heroSkinInfo;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "SpineAssetPath")]
    public string SpineAssetPath
    {
      get
      {
        return this._SpineAssetPath;
      }
      set
      {
        this._SpineAssetPath = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
