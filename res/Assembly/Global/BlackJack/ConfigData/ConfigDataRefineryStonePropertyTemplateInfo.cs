﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRefineryStonePropertyTemplateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRefineryStonePropertyTemplateInfo")]
  [Serializable]
  public class ConfigDataRefineryStonePropertyTemplateInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private PropertyModifyType _Property1_ID;
    private List<RefineryPropertyValueInfo> _ValueRange1;
    private PropertyModifyType _Property2_ID;
    private List<RefineryPropertyValueInfo> _ValueRange2;
    private PropertyModifyType _Property3_ID;
    private List<RefineryPropertyValueInfo> _ValueRange3;
    private PropertyModifyType _Property4_ID;
    private List<RefineryPropertyValueInfo> _ValueRange4;
    private PropertyModifyType _Property5_ID;
    private List<RefineryPropertyValueInfo> _ValueRange5;
    private PropertyModifyType _Property6_ID;
    private List<RefineryPropertyValueInfo> _ValueRange6;
    private PropertyModifyType _Property7_ID;
    private List<RefineryPropertyValueInfo> _ValueRange7;
    private PropertyModifyType _Property8_ID;
    private List<RefineryPropertyValueInfo> _ValueRange8;
    private PropertyModifyType _Property9_ID;
    private List<RefineryPropertyValueInfo> _ValueRange9;
    private PropertyModifyType _Property10_ID;
    private List<RefineryPropertyValueInfo> _ValueRange10;
    private IExtension extensionObject;
    public Dictionary<PropertyModifyType, List<RefineryPropertyValueInfo>> PropertyValues;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStonePropertyTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_ID")]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "ValueRange1")]
    public List<RefineryPropertyValueInfo> ValueRange1
    {
      get
      {
        return this._ValueRange1;
      }
      set
      {
        this._ValueRange1 = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_ID")]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "ValueRange2")]
    public List<RefineryPropertyValueInfo> ValueRange2
    {
      get
      {
        return this._ValueRange2;
      }
      set
      {
        this._ValueRange2 = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_ID")]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "ValueRange3")]
    public List<RefineryPropertyValueInfo> ValueRange3
    {
      get
      {
        return this._ValueRange3;
      }
      set
      {
        this._ValueRange3 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property4_ID")]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "ValueRange4")]
    public List<RefineryPropertyValueInfo> ValueRange4
    {
      get
      {
        return this._ValueRange4;
      }
      set
      {
        this._ValueRange4 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property5_ID")]
    public PropertyModifyType Property5_ID
    {
      get
      {
        return this._Property5_ID;
      }
      set
      {
        this._Property5_ID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, Name = "ValueRange5")]
    public List<RefineryPropertyValueInfo> ValueRange5
    {
      get
      {
        return this._ValueRange5;
      }
      set
      {
        this._ValueRange5 = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property6_ID")]
    public PropertyModifyType Property6_ID
    {
      get
      {
        return this._Property6_ID;
      }
      set
      {
        this._Property6_ID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "ValueRange6")]
    public List<RefineryPropertyValueInfo> ValueRange6
    {
      get
      {
        return this._ValueRange6;
      }
      set
      {
        this._ValueRange6 = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property7_ID")]
    public PropertyModifyType Property7_ID
    {
      get
      {
        return this._Property7_ID;
      }
      set
      {
        this._Property7_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "ValueRange7")]
    public List<RefineryPropertyValueInfo> ValueRange7
    {
      get
      {
        return this._ValueRange7;
      }
      set
      {
        this._ValueRange7 = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property8_ID")]
    public PropertyModifyType Property8_ID
    {
      get
      {
        return this._Property8_ID;
      }
      set
      {
        this._Property8_ID = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, Name = "ValueRange8")]
    public List<RefineryPropertyValueInfo> ValueRange8
    {
      get
      {
        return this._ValueRange8;
      }
      set
      {
        this._ValueRange8 = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property9_ID")]
    public PropertyModifyType Property9_ID
    {
      get
      {
        return this._Property9_ID;
      }
      set
      {
        this._Property9_ID = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, Name = "ValueRange9")]
    public List<RefineryPropertyValueInfo> ValueRange9
    {
      get
      {
        return this._ValueRange9;
      }
      set
      {
        this._ValueRange9 = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property10_ID")]
    public PropertyModifyType Property10_ID
    {
      get
      {
        return this._Property10_ID;
      }
      set
      {
        this._Property10_ID = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, Name = "ValueRange10")]
    public List<RefineryPropertyValueInfo> ValueRange10
    {
      get
      {
        return this._ValueRange10;
      }
      set
      {
        this._ValueRange10 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
