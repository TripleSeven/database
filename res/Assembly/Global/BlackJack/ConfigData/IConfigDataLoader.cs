﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.IConfigDataLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.UtilityTools;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  public interface IConfigDataLoader
  {
    ConfigDataActivityCardPoolGroupInfo GetConfigDataActivityCardPoolGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataActivityCardPoolGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataActivityCardPoolGroupInfo>> GetAllConfigDataActivityCardPoolGroupInfo();

    ConfigDataAncientCallBehavioralDescInfo GetConfigDataAncientCallBehavioralDescInfo(
      int key);

    IEnumerable LuaGetAllConfigDataAncientCallBehavioralDescInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAncientCallBehavioralDescInfo>> GetAllConfigDataAncientCallBehavioralDescInfo();

    ConfigDataAncientCallBossInfo GetConfigDataAncientCallBossInfo(
      int key);

    IEnumerable LuaGetAllConfigDataAncientCallBossInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAncientCallBossInfo>> GetAllConfigDataAncientCallBossInfo();

    ConfigDataAncientCallPeriodInfo GetConfigDataAncientCallPeriodInfo(
      int key);

    IEnumerable LuaGetAllConfigDataAncientCallPeriodInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAncientCallPeriodInfo>> GetAllConfigDataAncientCallPeriodInfo();

    ConfigDataAncientCallRecordRewardInfo GetConfigDataAncientCallRecordRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataAncientCallRecordRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAncientCallRecordRewardInfo>> GetAllConfigDataAncientCallRecordRewardInfo();

    ConfigDataAncientCallSingleBossPointsInfo GetConfigDataAncientCallSingleBossPointsInfo(
      int key);

    IEnumerable LuaGetAllConfigDataAncientCallSingleBossPointsInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAncientCallSingleBossPointsInfo>> GetAllConfigDataAncientCallSingleBossPointsInfo();

    ConfigDataAncientCallTotalBossPointsInfo GetConfigDataAncientCallTotalBossPointsInfo(
      int key);

    IEnumerable LuaGetAllConfigDataAncientCallTotalBossPointsInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAncientCallTotalBossPointsInfo>> GetAllConfigDataAncientCallTotalBossPointsInfo();

    ConfigDataAnikiGymInfo GetConfigDataAnikiGymInfo(int key);

    IEnumerable LuaGetAllConfigDataAnikiGymInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAnikiGymInfo>> GetAllConfigDataAnikiGymInfo();

    ConfigDataAnikiLevelInfo GetConfigDataAnikiLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataAnikiLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAnikiLevelInfo>> GetAllConfigDataAnikiLevelInfo();

    ConfigDataArenaBattleInfo GetConfigDataArenaBattleInfo(int key);

    IEnumerable LuaGetAllConfigDataArenaBattleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaBattleInfo>> GetAllConfigDataArenaBattleInfo();

    ConfigDataArenaDefendRuleInfo GetConfigDataArenaDefendRuleInfo(
      int key);

    IEnumerable LuaGetAllConfigDataArenaDefendRuleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaDefendRuleInfo>> GetAllConfigDataArenaDefendRuleInfo();

    ConfigDataArenaLevelInfo GetConfigDataArenaLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataArenaLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaLevelInfo>> GetAllConfigDataArenaLevelInfo();

    ConfigDataArenaOpponentPointZoneInfo GetConfigDataArenaOpponentPointZoneInfo(
      int key);

    IEnumerable LuaGetAllConfigDataArenaOpponentPointZoneInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaOpponentPointZoneInfo>> GetAllConfigDataArenaOpponentPointZoneInfo();

    ConfigDataArenaRankRewardInfo GetConfigDataArenaRankRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataArenaRankRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaRankRewardInfo>> GetAllConfigDataArenaRankRewardInfo();

    ConfigDataArenaRobotInfo GetConfigDataArenaRobotInfo(int key);

    IEnumerable LuaGetAllConfigDataArenaRobotInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaRobotInfo>> GetAllConfigDataArenaRobotInfo();

    ConfigDataArenaSettleTimeInfo GetConfigDataArenaSettleTimeInfo(
      int key);

    IEnumerable LuaGetAllConfigDataArenaSettleTimeInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaSettleTimeInfo>> GetAllConfigDataArenaSettleTimeInfo();

    ConfigDataArenaVictoryPointsRewardInfo GetConfigDataArenaVictoryPointsRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataArenaVictoryPointsRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArenaVictoryPointsRewardInfo>> GetAllConfigDataArenaVictoryPointsRewardInfo();

    ConfigDataArmyInfo GetConfigDataArmyInfo(int key);

    IEnumerable LuaGetAllConfigDataArmyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataArmyInfo>> GetAllConfigDataArmyInfo();

    ConfigDataArmyRelation GetConfigDataArmyRelation(int key);

    IEnumerable LuaGetAllConfigDataArmyRelation();

    IEnumerable<KeyValuePair<int, ConfigDataArmyRelation>> GetAllConfigDataArmyRelation();

    ConfigDataAssetReplaceInfo GetConfigDataAssetReplaceInfo(int key);

    IEnumerable LuaGetAllConfigDataAssetReplaceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataAssetReplaceInfo>> GetAllConfigDataAssetReplaceInfo();

    ConfigDataBanditInfo GetConfigDataBanditInfo(int key);

    IEnumerable LuaGetAllConfigDataBanditInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBanditInfo>> GetAllConfigDataBanditInfo();

    ConfigDataBattleAchievementInfo GetConfigDataBattleAchievementInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleAchievementInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementInfo>> GetAllConfigDataBattleAchievementInfo();

    ConfigDataBattleAchievementRelatedInfo GetConfigDataBattleAchievementRelatedInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleAchievementRelatedInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementRelatedInfo>> GetAllConfigDataBattleAchievementRelatedInfo();

    ConfigDataBattleDialogInfo GetConfigDataBattleDialogInfo(int key);

    IEnumerable LuaGetAllConfigDataBattleDialogInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleDialogInfo>> GetAllConfigDataBattleDialogInfo();

    ConfigDataBattleEventActionInfo GetConfigDataBattleEventActionInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleEventActionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleEventActionInfo>> GetAllConfigDataBattleEventActionInfo();

    ConfigDataBattleEventTriggerInfo GetConfigDataBattleEventTriggerInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleEventTriggerInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleEventTriggerInfo>> GetAllConfigDataBattleEventTriggerInfo();

    ConfigDataBattlefieldInfo GetConfigDataBattlefieldInfo(int key);

    IEnumerable LuaGetAllConfigDataBattlefieldInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattlefieldInfo>> GetAllConfigDataBattlefieldInfo();

    ConfigDataBattleInfo GetConfigDataBattleInfo(int key);

    IEnumerable LuaGetAllConfigDataBattleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleInfo>> GetAllConfigDataBattleInfo();

    ConfigDataBattleLoseConditionInfo GetConfigDataBattleLoseConditionInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleLoseConditionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleLoseConditionInfo>> GetAllConfigDataBattleLoseConditionInfo();

    ConfigDataBattlePerformInfo GetConfigDataBattlePerformInfo(int key);

    IEnumerable LuaGetAllConfigDataBattlePerformInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattlePerformInfo>> GetAllConfigDataBattlePerformInfo();

    ConfigDataBattleRandomArmyInfo GetConfigDataBattleRandomArmyInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleRandomArmyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleRandomArmyInfo>> GetAllConfigDataBattleRandomArmyInfo();

    ConfigDataBattleRandomTalentInfo GetConfigDataBattleRandomTalentInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleRandomTalentInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleRandomTalentInfo>> GetAllConfigDataBattleRandomTalentInfo();

    ConfigDataBattleTreasureInfo GetConfigDataBattleTreasureInfo(int key);

    IEnumerable LuaGetAllConfigDataBattleTreasureInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleTreasureInfo>> GetAllConfigDataBattleTreasureInfo();

    ConfigDataBattleWinConditionInfo GetConfigDataBattleWinConditionInfo(
      int key);

    IEnumerable LuaGetAllConfigDataBattleWinConditionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBattleWinConditionInfo>> GetAllConfigDataBattleWinConditionInfo();

    ConfigDataBehavior GetConfigDataBehavior(int key);

    IEnumerable LuaGetAllConfigDataBehavior();

    IEnumerable<KeyValuePair<int, ConfigDataBehavior>> GetAllConfigDataBehavior();

    ConfigDataBehaviorChangeRule GetConfigDataBehaviorChangeRule(int key);

    IEnumerable LuaGetAllConfigDataBehaviorChangeRule();

    IEnumerable<KeyValuePair<int, ConfigDataBehaviorChangeRule>> GetAllConfigDataBehaviorChangeRule();

    ConfigDataBigExpressionInfo GetConfigDataBigExpressionInfo(int key);

    IEnumerable LuaGetAllConfigDataBigExpressionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBigExpressionInfo>> GetAllConfigDataBigExpressionInfo();

    ConfigDataBuffInfo GetConfigDataBuffInfo(int key);

    IEnumerable LuaGetAllConfigDataBuffInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBuffInfo>> GetAllConfigDataBuffInfo();

    ConfigDataBuyArenaTicketInfo GetConfigDataBuyArenaTicketInfo(int key);

    IEnumerable LuaGetAllConfigDataBuyArenaTicketInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBuyArenaTicketInfo>> GetAllConfigDataBuyArenaTicketInfo();

    ConfigDataBuyEnergyInfo GetConfigDataBuyEnergyInfo(int key);

    IEnumerable LuaGetAllConfigDataBuyEnergyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataBuyEnergyInfo>> GetAllConfigDataBuyEnergyInfo();

    ConfigDataCardPoolGroupInfo GetConfigDataCardPoolGroupInfo(int key);

    IEnumerable LuaGetAllConfigDataCardPoolGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCardPoolGroupInfo>> GetAllConfigDataCardPoolGroupInfo();

    ConfigDataCardPoolInfo GetConfigDataCardPoolInfo(int key);

    IEnumerable LuaGetAllConfigDataCardPoolInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCardPoolInfo>> GetAllConfigDataCardPoolInfo();

    ConfigDataChallengeLevelInfo GetConfigDataChallengeLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataChallengeLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataChallengeLevelInfo>> GetAllConfigDataChallengeLevelInfo();

    ConfigDataCharImageInfo GetConfigDataCharImageInfo(int key);

    IEnumerable LuaGetAllConfigDataCharImageInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCharImageInfo>> GetAllConfigDataCharImageInfo();

    ConfigDataCharImageSkinResourceInfo GetConfigDataCharImageSkinResourceInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCharImageSkinResourceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCharImageSkinResourceInfo>> GetAllConfigDataCharImageSkinResourceInfo();

    ConfigDataCollectionActivityChallengeLevelInfo GetConfigDataCollectionActivityChallengeLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityChallengeLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityChallengeLevelInfo>> GetAllConfigDataCollectionActivityChallengeLevelInfo();

    ConfigDataCollectionActivityCurrencyItemExtInfo GetConfigDataCollectionActivityCurrencyItemExtInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityCurrencyItemExtInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityCurrencyItemExtInfo>> GetAllConfigDataCollectionActivityCurrencyItemExtInfo();

    ConfigDataCollectionActivityExchangeTableInfo GetConfigDataCollectionActivityExchangeTableInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityExchangeTableInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityExchangeTableInfo>> GetAllConfigDataCollectionActivityExchangeTableInfo();

    ConfigDataCollectionActivityInfo GetConfigDataCollectionActivityInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityInfo>> GetAllConfigDataCollectionActivityInfo();

    ConfigDataCollectionActivityLootLevelInfo GetConfigDataCollectionActivityLootLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityLootLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityLootLevelInfo>> GetAllConfigDataCollectionActivityLootLevelInfo();

    ConfigDataCollectionActivityMapInfo GetConfigDataCollectionActivityMapInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityMapInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityMapInfo>> GetAllConfigDataCollectionActivityMapInfo();

    ConfigDataCollectionActivityScenarioLevelInfo GetConfigDataCollectionActivityScenarioLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityScenarioLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScenarioLevelInfo>> GetAllConfigDataCollectionActivityScenarioLevelInfo();

    ConfigDataCollectionActivityScoreRewardGroupInfo GetConfigDataCollectionActivityScoreRewardGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityScoreRewardGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScoreRewardGroupInfo>> GetAllConfigDataCollectionActivityScoreRewardGroupInfo();

    ConfigDataCollectionActivityWaypointInfo GetConfigDataCollectionActivityWaypointInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionActivityWaypointInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityWaypointInfo>> GetAllConfigDataCollectionActivityWaypointInfo();

    ConfigDataCollectionEventInfo GetConfigDataCollectionEventInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCollectionEventInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCollectionEventInfo>> GetAllConfigDataCollectionEventInfo();

    ConfigDataConfessionDialogInfo GetConfigDataConfessionDialogInfo(
      int key);

    IEnumerable LuaGetAllConfigDataConfessionDialogInfo();

    IEnumerable<KeyValuePair<int, ConfigDataConfessionDialogInfo>> GetAllConfigDataConfessionDialogInfo();

    ConfigDataConfessionLetterInfo GetConfigDataConfessionLetterInfo(
      int key);

    IEnumerable LuaGetAllConfigDataConfessionLetterInfo();

    IEnumerable<KeyValuePair<int, ConfigDataConfessionLetterInfo>> GetAllConfigDataConfessionLetterInfo();

    ConfigDataConfigableConst GetConfigDataConfigableConst(int key);

    IEnumerable LuaGetAllConfigDataConfigableConst();

    IEnumerable<KeyValuePair<int, ConfigDataConfigableConst>> GetAllConfigDataConfigableConst();

    ConfigDataConfigIDRangeInfo GetConfigDataConfigIDRangeInfo(int key);

    IEnumerable LuaGetAllConfigDataConfigIDRangeInfo();

    IEnumerable<KeyValuePair<int, ConfigDataConfigIDRangeInfo>> GetAllConfigDataConfigIDRangeInfo();

    ConfigDataCooperateBattleInfo GetConfigDataCooperateBattleInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCooperateBattleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleInfo>> GetAllConfigDataCooperateBattleInfo();

    ConfigDataCooperateBattleLevelInfo GetConfigDataCooperateBattleLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCooperateBattleLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleLevelInfo>> GetAllConfigDataCooperateBattleLevelInfo();

    ConfigDataCrystalCardPoolGroupInfo GetConfigDataCrystalCardPoolGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataCrystalCardPoolGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCrystalCardPoolGroupInfo>> GetAllConfigDataCrystalCardPoolGroupInfo();

    ConfigDataCutsceneInfo GetConfigDataCutsceneInfo(int key);

    IEnumerable LuaGetAllConfigDataCutsceneInfo();

    IEnumerable<KeyValuePair<int, ConfigDataCutsceneInfo>> GetAllConfigDataCutsceneInfo();

    ConfigDataDailyPushNotification GetConfigDataDailyPushNotification(
      int key);

    IEnumerable LuaGetAllConfigDataDailyPushNotification();

    IEnumerable<KeyValuePair<int, ConfigDataDailyPushNotification>> GetAllConfigDataDailyPushNotification();

    ConfigDataDeviceSetting GetConfigDataDeviceSetting(int key);

    IEnumerable LuaGetAllConfigDataDeviceSetting();

    IEnumerable<KeyValuePair<int, ConfigDataDeviceSetting>> GetAllConfigDataDeviceSetting();

    ConfigDataDialogChoiceInfo GetConfigDataDialogChoiceInfo(int key);

    IEnumerable LuaGetAllConfigDataDialogChoiceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataDialogChoiceInfo>> GetAllConfigDataDialogChoiceInfo();

    ConfigDataDialogInfo GetConfigDataDialogInfo(int key);

    IEnumerable LuaGetAllConfigDataDialogInfo();

    IEnumerable<KeyValuePair<int, ConfigDataDialogInfo>> GetAllConfigDataDialogInfo();

    ConfigDataEnchantStoneInfo GetConfigDataEnchantStoneInfo(int key);

    IEnumerable LuaGetAllConfigDataEnchantStoneInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEnchantStoneInfo>> GetAllConfigDataEnchantStoneInfo();

    ConfigDataEnchantTemplateInfo GetConfigDataEnchantTemplateInfo(
      int key);

    IEnumerable LuaGetAllConfigDataEnchantTemplateInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEnchantTemplateInfo>> GetAllConfigDataEnchantTemplateInfo();

    ConfigDataEquipmentInfo GetConfigDataEquipmentInfo(int key);

    IEnumerable LuaGetAllConfigDataEquipmentInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEquipmentInfo>> GetAllConfigDataEquipmentInfo();

    ConfigDataEquipmentLevelInfo GetConfigDataEquipmentLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataEquipmentLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelInfo>> GetAllConfigDataEquipmentLevelInfo();

    ConfigDataEquipmentLevelLimitInfo GetConfigDataEquipmentLevelLimitInfo(
      int key);

    IEnumerable LuaGetAllConfigDataEquipmentLevelLimitInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelLimitInfo>> GetAllConfigDataEquipmentLevelLimitInfo();

    ConfigDataErrorCodeStringTable GetConfigDataErrorCodeStringTable(
      int key);

    IEnumerable LuaGetAllConfigDataErrorCodeStringTable();

    IEnumerable<KeyValuePair<int, ConfigDataErrorCodeStringTable>> GetAllConfigDataErrorCodeStringTable();

    ConfigDataEternalShrineInfo GetConfigDataEternalShrineInfo(int key);

    IEnumerable LuaGetAllConfigDataEternalShrineInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEternalShrineInfo>> GetAllConfigDataEternalShrineInfo();

    ConfigDataEternalShrineLevelInfo GetConfigDataEternalShrineLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataEternalShrineLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEternalShrineLevelInfo>> GetAllConfigDataEternalShrineLevelInfo();

    ConfigDataEventGiftInfo GetConfigDataEventGiftInfo(int key);

    IEnumerable LuaGetAllConfigDataEventGiftInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEventGiftInfo>> GetAllConfigDataEventGiftInfo();

    ConfigDataEventInfo GetConfigDataEventInfo(int key);

    IEnumerable LuaGetAllConfigDataEventInfo();

    IEnumerable<KeyValuePair<int, ConfigDataEventInfo>> GetAllConfigDataEventInfo();

    ConfigDataExplanationInfo GetConfigDataExplanationInfo(int key);

    IEnumerable LuaGetAllConfigDataExplanationInfo();

    IEnumerable<KeyValuePair<int, ConfigDataExplanationInfo>> GetAllConfigDataExplanationInfo();

    ConfigDataFixedStoreItemInfo GetConfigDataFixedStoreItemInfo(int key);

    IEnumerable LuaGetAllConfigDataFixedStoreItemInfo();

    IEnumerable<KeyValuePair<int, ConfigDataFixedStoreItemInfo>> GetAllConfigDataFixedStoreItemInfo();

    ConfigDataFlyObjectInfo GetConfigDataFlyObjectInfo(int key);

    IEnumerable LuaGetAllConfigDataFlyObjectInfo();

    IEnumerable<KeyValuePair<int, ConfigDataFlyObjectInfo>> GetAllConfigDataFlyObjectInfo();

    ConfigDataFreeCardPoolGroupInfo GetConfigDataFreeCardPoolGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataFreeCardPoolGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataFreeCardPoolGroupInfo>> GetAllConfigDataFreeCardPoolGroupInfo();

    ConfigDataFxFlipInfo GetConfigDataFxFlipInfo(int key);

    IEnumerable LuaGetAllConfigDataFxFlipInfo();

    IEnumerable<KeyValuePair<int, ConfigDataFxFlipInfo>> GetAllConfigDataFxFlipInfo();

    ConfigDataGameFunctionOpenInfo GetConfigDataGameFunctionOpenInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGameFunctionOpenInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGameFunctionOpenInfo>> GetAllConfigDataGameFunctionOpenInfo();

    ConfigDataGamePlayTeamTypeInfo GetConfigDataGamePlayTeamTypeInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGamePlayTeamTypeInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGamePlayTeamTypeInfo>> GetAllConfigDataGamePlayTeamTypeInfo();

    ConfigDataGiftCDKeyInfo GetConfigDataGiftCDKeyInfo(int key);

    IEnumerable LuaGetAllConfigDataGiftCDKeyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGiftCDKeyInfo>> GetAllConfigDataGiftCDKeyInfo();

    ConfigDataGiftStoreItemInfo GetConfigDataGiftStoreItemInfo(int key);

    IEnumerable LuaGetAllConfigDataGiftStoreItemInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGiftStoreItemInfo>> GetAllConfigDataGiftStoreItemInfo();

    ConfigDataGoddessDialogInfo GetConfigDataGoddessDialogInfo(int key);

    IEnumerable LuaGetAllConfigDataGoddessDialogInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGoddessDialogInfo>> GetAllConfigDataGoddessDialogInfo();

    ConfigDataGroupBehavior GetConfigDataGroupBehavior(int key);

    IEnumerable LuaGetAllConfigDataGroupBehavior();

    IEnumerable<KeyValuePair<int, ConfigDataGroupBehavior>> GetAllConfigDataGroupBehavior();

    ConfigDataGuildMassiveCombatDifficultyInfo GetConfigDataGuildMassiveCombatDifficultyInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGuildMassiveCombatDifficultyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatDifficultyInfo>> GetAllConfigDataGuildMassiveCombatDifficultyInfo();

    ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo GetConfigDataGuildMassiveCombatIndividualPointsRewardsInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo>> GetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo();

    ConfigDataGuildMassiveCombatLevelInfo GetConfigDataGuildMassiveCombatLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGuildMassiveCombatLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatLevelInfo>> GetAllConfigDataGuildMassiveCombatLevelInfo();

    ConfigDataGuildMassiveCombatRewardsInfo GetConfigDataGuildMassiveCombatRewardsInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGuildMassiveCombatRewardsInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatRewardsInfo>> GetAllConfigDataGuildMassiveCombatRewardsInfo();

    ConfigDataGuildMassiveCombatStrongholdInfo GetConfigDataGuildMassiveCombatStrongholdInfo(
      int key);

    IEnumerable LuaGetAllConfigDataGuildMassiveCombatStrongholdInfo();

    IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatStrongholdInfo>> GetAllConfigDataGuildMassiveCombatStrongholdInfo();

    ConfigDataHeadFrameInfo GetConfigDataHeadFrameInfo(int key);

    IEnumerable LuaGetAllConfigDataHeadFrameInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeadFrameInfo>> GetAllConfigDataHeadFrameInfo();

    ConfigDataHeroAnthemInfo GetConfigDataHeroAnthemInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroAnthemInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroAnthemInfo>> GetAllConfigDataHeroAnthemInfo();

    ConfigDataHeroAnthemLevelInfo GetConfigDataHeroAnthemLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroAnthemLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroAnthemLevelInfo>> GetAllConfigDataHeroAnthemLevelInfo();

    ConfigDataHeroAssistantTaskGeneralInfo GetConfigDataHeroAssistantTaskGeneralInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroAssistantTaskGeneralInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskGeneralInfo>> GetAllConfigDataHeroAssistantTaskGeneralInfo();

    ConfigDataHeroAssistantTaskInfo GetConfigDataHeroAssistantTaskInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroAssistantTaskInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskInfo>> GetAllConfigDataHeroAssistantTaskInfo();

    ConfigDataHeroAssistantTaskScheduleInfo GetConfigDataHeroAssistantTaskScheduleInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroAssistantTaskScheduleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskScheduleInfo>> GetAllConfigDataHeroAssistantTaskScheduleInfo();

    ConfigDataHeroBiographyInfo GetConfigDataHeroBiographyInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroBiographyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroBiographyInfo>> GetAllConfigDataHeroBiographyInfo();

    ConfigDataHeroConfessionInfo GetConfigDataHeroConfessionInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroConfessionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroConfessionInfo>> GetAllConfigDataHeroConfessionInfo();

    ConfigDataHeroDungeonLevelInfo GetConfigDataHeroDungeonLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroDungeonLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroDungeonLevelInfo>> GetAllConfigDataHeroDungeonLevelInfo();

    ConfigDataHeroFavorabilityLevelInfo GetConfigDataHeroFavorabilityLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroFavorabilityLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroFavorabilityLevelInfo>> GetAllConfigDataHeroFavorabilityLevelInfo();

    ConfigDataHeroFetterInfo GetConfigDataHeroFetterInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroFetterInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroFetterInfo>> GetAllConfigDataHeroFetterInfo();

    ConfigDataHeroHeartFetterInfo GetConfigDataHeroHeartFetterInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroHeartFetterInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroHeartFetterInfo>> GetAllConfigDataHeroHeartFetterInfo();

    ConfigDataHeroInfo GetConfigDataHeroInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroInfo>> GetAllConfigDataHeroInfo();

    ConfigDataHeroInformationInfo GetConfigDataHeroInformationInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroInformationInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroInformationInfo>> GetAllConfigDataHeroInformationInfo();

    ConfigDataHeroInteractionInfo GetConfigDataHeroInteractionInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroInteractionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroInteractionInfo>> GetAllConfigDataHeroInteractionInfo();

    ConfigDataHeroLevelInfo GetConfigDataHeroLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroLevelInfo>> GetAllConfigDataHeroLevelInfo();

    ConfigDataHeroPerformanceInfo GetConfigDataHeroPerformanceInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroPerformanceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceInfo>> GetAllConfigDataHeroPerformanceInfo();

    ConfigDataHeroPerformanceWordInfo GetConfigDataHeroPerformanceWordInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroPerformanceWordInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceWordInfo>> GetAllConfigDataHeroPerformanceWordInfo();

    ConfigDataHeroPhantomInfo GetConfigDataHeroPhantomInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroPhantomInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomInfo>> GetAllConfigDataHeroPhantomInfo();

    ConfigDataHeroPhantomLevelInfo GetConfigDataHeroPhantomLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroPhantomLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomLevelInfo>> GetAllConfigDataHeroPhantomLevelInfo();

    ConfigDataHeroSkinInfo GetConfigDataHeroSkinInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroSkinInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroSkinInfo>> GetAllConfigDataHeroSkinInfo();

    ConfigDataHeroSkinSelfSelectedBoxInfo GetConfigDataHeroSkinSelfSelectedBoxInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroSkinSelfSelectedBoxInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroSkinSelfSelectedBoxInfo>> GetAllConfigDataHeroSkinSelfSelectedBoxInfo();

    ConfigDataHeroStarInfo GetConfigDataHeroStarInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroStarInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroStarInfo>> GetAllConfigDataHeroStarInfo();

    ConfigDataHeroTagInfo GetConfigDataHeroTagInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroTagInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroTagInfo>> GetAllConfigDataHeroTagInfo();

    ConfigDataHeroTrainningInfo GetConfigDataHeroTrainningInfo(int key);

    IEnumerable LuaGetAllConfigDataHeroTrainningInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningInfo>> GetAllConfigDataHeroTrainningInfo();

    ConfigDataHeroTrainningLevelInfo GetConfigDataHeroTrainningLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataHeroTrainningLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningLevelInfo>> GetAllConfigDataHeroTrainningLevelInfo();

    ConfigDataInitInfo GetConfigDataInitInfo(int key);

    IEnumerable LuaGetAllConfigDataInitInfo();

    IEnumerable<KeyValuePair<int, ConfigDataInitInfo>> GetAllConfigDataInitInfo();

    ConfigDataItemInfo GetConfigDataItemInfo(int key);

    IEnumerable LuaGetAllConfigDataItemInfo();

    IEnumerable<KeyValuePair<int, ConfigDataItemInfo>> GetAllConfigDataItemInfo();

    ConfigDataJobConnectionInfo GetConfigDataJobConnectionInfo(int key);

    IEnumerable LuaGetAllConfigDataJobConnectionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataJobConnectionInfo>> GetAllConfigDataJobConnectionInfo();

    ConfigDataJobInfo GetConfigDataJobInfo(int key);

    IEnumerable LuaGetAllConfigDataJobInfo();

    IEnumerable<KeyValuePair<int, ConfigDataJobInfo>> GetAllConfigDataJobInfo();

    ConfigDataJobLevelInfo GetConfigDataJobLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataJobLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataJobLevelInfo>> GetAllConfigDataJobLevelInfo();

    ConfigDataJobMaterialInfo GetConfigDataJobMaterialInfo(int key);

    IEnumerable LuaGetAllConfigDataJobMaterialInfo();

    IEnumerable<KeyValuePair<int, ConfigDataJobMaterialInfo>> GetAllConfigDataJobMaterialInfo();

    ConfigDataJobUnlockConditionInfo GetConfigDataJobUnlockConditionInfo(
      int key);

    IEnumerable LuaGetAllConfigDataJobUnlockConditionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataJobUnlockConditionInfo>> GetAllConfigDataJobUnlockConditionInfo();

    ConfigDataLanguageDataInfo GetConfigDataLanguageDataInfo(int key);

    IEnumerable LuaGetAllConfigDataLanguageDataInfo();

    IEnumerable<KeyValuePair<int, ConfigDataLanguageDataInfo>> GetAllConfigDataLanguageDataInfo();

    ConfigDataLinkageHeroInfo GetConfigDataLinkageHeroInfo(int key);

    IEnumerable LuaGetAllConfigDataLinkageHeroInfo();

    IEnumerable<KeyValuePair<int, ConfigDataLinkageHeroInfo>> GetAllConfigDataLinkageHeroInfo();

    ConfigDataMailInfo GetConfigDataMailInfo(int key);

    IEnumerable LuaGetAllConfigDataMailInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMailInfo>> GetAllConfigDataMailInfo();

    ConfigDataMemoryCorridorInfo GetConfigDataMemoryCorridorInfo(int key);

    IEnumerable LuaGetAllConfigDataMemoryCorridorInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorInfo>> GetAllConfigDataMemoryCorridorInfo();

    ConfigDataMemoryCorridorLevelInfo GetConfigDataMemoryCorridorLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataMemoryCorridorLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorLevelInfo>> GetAllConfigDataMemoryCorridorLevelInfo();

    ConfigDataMissionExtNoviceInfo GetConfigDataMissionExtNoviceInfo(
      int key);

    IEnumerable LuaGetAllConfigDataMissionExtNoviceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMissionExtNoviceInfo>> GetAllConfigDataMissionExtNoviceInfo();

    ConfigDataMissionExtRefluxInfo GetConfigDataMissionExtRefluxInfo(
      int key);

    IEnumerable LuaGetAllConfigDataMissionExtRefluxInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMissionExtRefluxInfo>> GetAllConfigDataMissionExtRefluxInfo();

    ConfigDataMissionInfo GetConfigDataMissionInfo(int key);

    IEnumerable LuaGetAllConfigDataMissionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMissionInfo>> GetAllConfigDataMissionInfo();

    ConfigDataModelSkinResourceInfo GetConfigDataModelSkinResourceInfo(
      int key);

    IEnumerable LuaGetAllConfigDataModelSkinResourceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataModelSkinResourceInfo>> GetAllConfigDataModelSkinResourceInfo();

    ConfigDataMonsterInfo GetConfigDataMonsterInfo(int key);

    IEnumerable LuaGetAllConfigDataMonsterInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMonsterInfo>> GetAllConfigDataMonsterInfo();

    ConfigDataMonthCardInfo GetConfigDataMonthCardInfo(int key);

    IEnumerable LuaGetAllConfigDataMonthCardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataMonthCardInfo>> GetAllConfigDataMonthCardInfo();

    ConfigDataMusicAppreciateTable GetConfigDataMusicAppreciateTable(
      int key);

    IEnumerable LuaGetAllConfigDataMusicAppreciateTable();

    IEnumerable<KeyValuePair<int, ConfigDataMusicAppreciateTable>> GetAllConfigDataMusicAppreciateTable();

    ConfigDataNoviceRewardInfo GetConfigDataNoviceRewardInfo(int key);

    IEnumerable LuaGetAllConfigDataNoviceRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataNoviceRewardInfo>> GetAllConfigDataNoviceRewardInfo();

    ConfigDataOperationalActivityInfo GetConfigDataOperationalActivityInfo(
      int key);

    IEnumerable LuaGetAllConfigDataOperationalActivityInfo();

    IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityInfo>> GetAllConfigDataOperationalActivityInfo();

    ConfigDataOperationalActivityItemGroupInfo GetConfigDataOperationalActivityItemGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataOperationalActivityItemGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityItemGroupInfo>> GetAllConfigDataOperationalActivityItemGroupInfo();

    ConfigDataPeakArenaBattleInfo GetConfigDataPeakArenaBattleInfo(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaBattleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaBattleInfo>> GetAllConfigDataPeakArenaBattleInfo();

    ConfigDataPeakArenaDanInfo GetConfigDataPeakArenaDanInfo(int key);

    IEnumerable LuaGetAllConfigDataPeakArenaDanInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaDanInfo>> GetAllConfigDataPeakArenaDanInfo();

    ConfigDataPeakArenaDanRewardSettleTimeInfo GetConfigDataPeakArenaDanRewardSettleTimeInfo(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaDanRewardSettleTimeInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaDanRewardSettleTimeInfo>> GetAllConfigDataPeakArenaDanRewardSettleTimeInfo();

    ConfigDataPeakArenaFirstWeekWinRewardInfo GetConfigDataPeakArenaFirstWeekWinRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaFirstWeekWinRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaFirstWeekWinRewardInfo>> GetAllConfigDataPeakArenaFirstWeekWinRewardInfo();

    ConfigDataPeakArenaKnockoutMatchTime GetConfigDataPeakArenaKnockoutMatchTime(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaKnockoutMatchTime();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaKnockoutMatchTime>> GetAllConfigDataPeakArenaKnockoutMatchTime();

    ConfigDataPeakArenaPublicHeroPoolInfo GetConfigDataPeakArenaPublicHeroPoolInfo(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaPublicHeroPoolInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaPublicHeroPoolInfo>> GetAllConfigDataPeakArenaPublicHeroPoolInfo();

    ConfigDataPeakArenaRankingRewardInfo GetConfigDataPeakArenaRankingRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaRankingRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaRankingRewardInfo>> GetAllConfigDataPeakArenaRankingRewardInfo();

    ConfigDataPeakArenaSeasonInfo GetConfigDataPeakArenaSeasonInfo(
      int key);

    IEnumerable LuaGetAllConfigDataPeakArenaSeasonInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPeakArenaSeasonInfo>> GetAllConfigDataPeakArenaSeasonInfo();

    ConfigDataPerformanceInfo GetConfigDataPerformanceInfo(int key);

    IEnumerable LuaGetAllConfigDataPerformanceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPerformanceInfo>> GetAllConfigDataPerformanceInfo();

    ConfigDataPlayerLevelInfo GetConfigDataPlayerLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataPlayerLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPlayerLevelInfo>> GetAllConfigDataPlayerLevelInfo();

    ConfigDataPrefabStateInfo GetConfigDataPrefabStateInfo(int key);

    IEnumerable LuaGetAllConfigDataPrefabStateInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPrefabStateInfo>> GetAllConfigDataPrefabStateInfo();

    ConfigDataPropertyModifyInfo GetConfigDataPropertyModifyInfo(int key);

    IEnumerable LuaGetAllConfigDataPropertyModifyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPropertyModifyInfo>> GetAllConfigDataPropertyModifyInfo();

    ConfigDataProtagonistInfo GetConfigDataProtagonistInfo(int key);

    IEnumerable LuaGetAllConfigDataProtagonistInfo();

    IEnumerable<KeyValuePair<int, ConfigDataProtagonistInfo>> GetAllConfigDataProtagonistInfo();

    ConfigDataPVPBattleInfo GetConfigDataPVPBattleInfo(int key);

    IEnumerable LuaGetAllConfigDataPVPBattleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataPVPBattleInfo>> GetAllConfigDataPVPBattleInfo();

    ConfigDataRafflePoolInfo GetConfigDataRafflePoolInfo(int key);

    IEnumerable LuaGetAllConfigDataRafflePoolInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRafflePoolInfo>> GetAllConfigDataRafflePoolInfo();

    ConfigDataRandomBoxInfo GetConfigDataRandomBoxInfo(int key);

    IEnumerable LuaGetAllConfigDataRandomBoxInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRandomBoxInfo>> GetAllConfigDataRandomBoxInfo();

    ConfigDataRandomDropRewardInfo GetConfigDataRandomDropRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRandomDropRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRandomDropRewardInfo>> GetAllConfigDataRandomDropRewardInfo();

    ConfigDataRandomNameHead GetConfigDataRandomNameHead(int key);

    IEnumerable LuaGetAllConfigDataRandomNameHead();

    IEnumerable<KeyValuePair<int, ConfigDataRandomNameHead>> GetAllConfigDataRandomNameHead();

    ConfigDataRandomNameMiddle GetConfigDataRandomNameMiddle(int key);

    IEnumerable LuaGetAllConfigDataRandomNameMiddle();

    IEnumerable<KeyValuePair<int, ConfigDataRandomNameMiddle>> GetAllConfigDataRandomNameMiddle();

    ConfigDataRandomNameTail GetConfigDataRandomNameTail(int key);

    IEnumerable LuaGetAllConfigDataRandomNameTail();

    IEnumerable<KeyValuePair<int, ConfigDataRandomNameTail>> GetAllConfigDataRandomNameTail();

    ConfigDataRandomStoreInfo GetConfigDataRandomStoreInfo(int key);

    IEnumerable LuaGetAllConfigDataRandomStoreInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRandomStoreInfo>> GetAllConfigDataRandomStoreInfo();

    ConfigDataRandomStoreItemInfo GetConfigDataRandomStoreItemInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRandomStoreItemInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRandomStoreItemInfo>> GetAllConfigDataRandomStoreItemInfo();

    ConfigDataRandomStoreLevelZoneInfo GetConfigDataRandomStoreLevelZoneInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRandomStoreLevelZoneInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRandomStoreLevelZoneInfo>> GetAllConfigDataRandomStoreLevelZoneInfo();

    ConfigDataRankInfo GetConfigDataRankInfo(int key);

    IEnumerable LuaGetAllConfigDataRankInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRankInfo>> GetAllConfigDataRankInfo();

    ConfigDataRealTimePVPBattleInfo GetConfigDataRealTimePVPBattleInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPBattleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPBattleInfo>> GetAllConfigDataRealTimePVPBattleInfo();

    ConfigDataRealTimePVPDanInfo GetConfigDataRealTimePVPDanInfo(int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPDanInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanInfo>> GetAllConfigDataRealTimePVPDanInfo();

    ConfigDataRealTimePVPDanRewardInfo GetConfigDataRealTimePVPDanRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPDanRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanRewardInfo>> GetAllConfigDataRealTimePVPDanRewardInfo();

    ConfigDataRealTimePVPLocalRankingRewardInfo GetConfigDataRealTimePVPLocalRankingRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPLocalRankingRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPLocalRankingRewardInfo>> GetAllConfigDataRealTimePVPLocalRankingRewardInfo();

    ConfigDataRealTimePVPNoviceMatchmakingInfo GetConfigDataRealTimePVPNoviceMatchmakingInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPNoviceMatchmakingInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPNoviceMatchmakingInfo>> GetAllConfigDataRealTimePVPNoviceMatchmakingInfo();

    ConfigDataRealTimePVPRankingRewardInfo GetConfigDataRealTimePVPRankingRewardInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPRankingRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPRankingRewardInfo>> GetAllConfigDataRealTimePVPRankingRewardInfo();

    ConfigDataRealTimePVPSettleTimeInfo GetConfigDataRealTimePVPSettleTimeInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPSettleTimeInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPSettleTimeInfo>> GetAllConfigDataRealTimePVPSettleTimeInfo();

    ConfigDataRealTimePVPWinsBonusInfo GetConfigDataRealTimePVPWinsBonusInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRealTimePVPWinsBonusInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPWinsBonusInfo>> GetAllConfigDataRealTimePVPWinsBonusInfo();

    ConfigDataRechargeStoreItemInfo GetConfigDataRechargeStoreItemInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRechargeStoreItemInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRechargeStoreItemInfo>> GetAllConfigDataRechargeStoreItemInfo();

    ConfigDataRefineryStoneInfo GetConfigDataRefineryStoneInfo(int key);

    IEnumerable LuaGetAllConfigDataRefineryStoneInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRefineryStoneInfo>> GetAllConfigDataRefineryStoneInfo();

    ConfigDataRefineryStonePropertyTemplateInfo GetConfigDataRefineryStonePropertyTemplateInfo(
      int key);

    IEnumerable LuaGetAllConfigDataRefineryStonePropertyTemplateInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRefineryStonePropertyTemplateInfo>> GetAllConfigDataRefineryStonePropertyTemplateInfo();

    ConfigDataRefluxRewardInfo GetConfigDataRefluxRewardInfo(int key);

    IEnumerable LuaGetAllConfigDataRefluxRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRefluxRewardInfo>> GetAllConfigDataRefluxRewardInfo();

    ConfigDataRegionInfo GetConfigDataRegionInfo(int key);

    IEnumerable LuaGetAllConfigDataRegionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRegionInfo>> GetAllConfigDataRegionInfo();

    ConfigDataResonanceInfo GetConfigDataResonanceInfo(int key);

    IEnumerable LuaGetAllConfigDataResonanceInfo();

    IEnumerable<KeyValuePair<int, ConfigDataResonanceInfo>> GetAllConfigDataResonanceInfo();

    ConfigDataRiftChapterInfo GetConfigDataRiftChapterInfo(int key);

    IEnumerable LuaGetAllConfigDataRiftChapterInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRiftChapterInfo>> GetAllConfigDataRiftChapterInfo();

    ConfigDataRiftLevelInfo GetConfigDataRiftLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataRiftLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataRiftLevelInfo>> GetAllConfigDataRiftLevelInfo();

    ConfigDataScenarioInfo GetConfigDataScenarioInfo(int key);

    IEnumerable LuaGetAllConfigDataScenarioInfo();

    IEnumerable<KeyValuePair<int, ConfigDataScenarioInfo>> GetAllConfigDataScenarioInfo();

    ConfigDataScoreLevelInfo GetConfigDataScoreLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataScoreLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataScoreLevelInfo>> GetAllConfigDataScoreLevelInfo();

    ConfigDataSelectContentInfo GetConfigDataSelectContentInfo(int key);

    IEnumerable LuaGetAllConfigDataSelectContentInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSelectContentInfo>> GetAllConfigDataSelectContentInfo();

    ConfigDataSelectProbabilityInfo GetConfigDataSelectProbabilityInfo(
      int key);

    IEnumerable LuaGetAllConfigDataSelectProbabilityInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSelectProbabilityInfo>> GetAllConfigDataSelectProbabilityInfo();

    ConfigDataSelfSelectedBoxInfo GetConfigDataSelfSelectedBoxInfo(
      int key);

    IEnumerable LuaGetAllConfigDataSelfSelectedBoxInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSelfSelectedBoxInfo>> GetAllConfigDataSelfSelectedBoxInfo();

    ConfigDataSensitiveWords GetConfigDataSensitiveWords(int key);

    IEnumerable LuaGetAllConfigDataSensitiveWords();

    IEnumerable<KeyValuePair<int, ConfigDataSensitiveWords>> GetAllConfigDataSensitiveWords();

    ConfigDataSignRewardInfo GetConfigDataSignRewardInfo(int key);

    IEnumerable LuaGetAllConfigDataSignRewardInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSignRewardInfo>> GetAllConfigDataSignRewardInfo();

    ConfigDataSkillInfo GetConfigDataSkillInfo(int key);

    IEnumerable LuaGetAllConfigDataSkillInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSkillInfo>> GetAllConfigDataSkillInfo();

    ConfigDataSmallExpressionPathInfo GetConfigDataSmallExpressionPathInfo(
      int key);

    IEnumerable LuaGetAllConfigDataSmallExpressionPathInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSmallExpressionPathInfo>> GetAllConfigDataSmallExpressionPathInfo();

    ConfigDataSoldierInfo GetConfigDataSoldierInfo(int key);

    IEnumerable LuaGetAllConfigDataSoldierInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSoldierInfo>> GetAllConfigDataSoldierInfo();

    ConfigDataSoldierSkinInfo GetConfigDataSoldierSkinInfo(int key);

    IEnumerable LuaGetAllConfigDataSoldierSkinInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSoldierSkinInfo>> GetAllConfigDataSoldierSkinInfo();

    ConfigDataSoundTable GetConfigDataSoundTable(int key);

    IEnumerable LuaGetAllConfigDataSoundTable();

    IEnumerable<KeyValuePair<int, ConfigDataSoundTable>> GetAllConfigDataSoundTable();

    ConfigDataSpineAnimationSoundTable GetConfigDataSpineAnimationSoundTable(
      int key);

    IEnumerable LuaGetAllConfigDataSpineAnimationSoundTable();

    IEnumerable<KeyValuePair<int, ConfigDataSpineAnimationSoundTable>> GetAllConfigDataSpineAnimationSoundTable();

    ConfigDataSST_0_CN GetConfigDataSST_0_CN(int key);

    IEnumerable LuaGetAllConfigDataSST_0_CN();

    IEnumerable<KeyValuePair<int, ConfigDataSST_0_CN>> GetAllConfigDataSST_0_CN();

    ConfigDataSST_0_EN GetConfigDataSST_0_EN(int key);

    IEnumerable LuaGetAllConfigDataSST_0_EN();

    IEnumerable<KeyValuePair<int, ConfigDataSST_0_EN>> GetAllConfigDataSST_0_EN();

    ConfigDataSST_1_CN GetConfigDataSST_1_CN(int key);

    IEnumerable LuaGetAllConfigDataSST_1_CN();

    IEnumerable<KeyValuePair<int, ConfigDataSST_1_CN>> GetAllConfigDataSST_1_CN();

    ConfigDataSST_1_EN GetConfigDataSST_1_EN(int key);

    IEnumerable LuaGetAllConfigDataSST_1_EN();

    IEnumerable<KeyValuePair<int, ConfigDataSST_1_EN>> GetAllConfigDataSST_1_EN();

    ConfigDataStaticBoxInfo GetConfigDataStaticBoxInfo(int key);

    IEnumerable LuaGetAllConfigDataStaticBoxInfo();

    IEnumerable<KeyValuePair<int, ConfigDataStaticBoxInfo>> GetAllConfigDataStaticBoxInfo();

    ConfigDataStoreCurrencyInfo GetConfigDataStoreCurrencyInfo(int key);

    IEnumerable LuaGetAllConfigDataStoreCurrencyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataStoreCurrencyInfo>> GetAllConfigDataStoreCurrencyInfo();

    ConfigDataStoreInfo GetConfigDataStoreInfo(int key);

    IEnumerable LuaGetAllConfigDataStoreInfo();

    IEnumerable<KeyValuePair<int, ConfigDataStoreInfo>> GetAllConfigDataStoreInfo();

    ConfigDataStoryOutlineInfo GetConfigDataStoryOutlineInfo(int key);

    IEnumerable LuaGetAllConfigDataStoryOutlineInfo();

    IEnumerable<KeyValuePair<int, ConfigDataStoryOutlineInfo>> GetAllConfigDataStoryOutlineInfo();

    ConfigDataStringTable GetConfigDataStringTable(int key);

    IEnumerable LuaGetAllConfigDataStringTable();

    IEnumerable<KeyValuePair<int, ConfigDataStringTable>> GetAllConfigDataStringTable();

    ConfigDataStringTableForListInfo GetConfigDataStringTableForListInfo(
      int key);

    IEnumerable LuaGetAllConfigDataStringTableForListInfo();

    IEnumerable<KeyValuePair<int, ConfigDataStringTableForListInfo>> GetAllConfigDataStringTableForListInfo();

    ConfigDataSurveyInfo GetConfigDataSurveyInfo(int key);

    IEnumerable LuaGetAllConfigDataSurveyInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSurveyInfo>> GetAllConfigDataSurveyInfo();

    ConfigDataSystemBroadcastInfo GetConfigDataSystemBroadcastInfo(
      int key);

    IEnumerable LuaGetAllConfigDataSystemBroadcastInfo();

    IEnumerable<KeyValuePair<int, ConfigDataSystemBroadcastInfo>> GetAllConfigDataSystemBroadcastInfo();

    ConfigDataTarotInfo GetConfigDataTarotInfo(int key);

    IEnumerable LuaGetAllConfigDataTarotInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTarotInfo>> GetAllConfigDataTarotInfo();

    ConfigDataTerrainEffectInfo GetConfigDataTerrainEffectInfo(int key);

    IEnumerable LuaGetAllConfigDataTerrainEffectInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTerrainEffectInfo>> GetAllConfigDataTerrainEffectInfo();

    ConfigDataTerrainInfo GetConfigDataTerrainInfo(int key);

    IEnumerable LuaGetAllConfigDataTerrainInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTerrainInfo>> GetAllConfigDataTerrainInfo();

    ConfigDataThearchyTrialInfo GetConfigDataThearchyTrialInfo(int key);

    IEnumerable LuaGetAllConfigDataThearchyTrialInfo();

    IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialInfo>> GetAllConfigDataThearchyTrialInfo();

    ConfigDataThearchyTrialLevelInfo GetConfigDataThearchyTrialLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataThearchyTrialLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialLevelInfo>> GetAllConfigDataThearchyTrialLevelInfo();

    ConfigDataTicketLimitGameFunctionTypeInfo GetConfigDataTicketLimitGameFunctionTypeInfo(
      int key);

    IEnumerable LuaGetAllConfigDataTicketLimitGameFunctionTypeInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTicketLimitGameFunctionTypeInfo>> GetAllConfigDataTicketLimitGameFunctionTypeInfo();

    ConfigDataTicketLimitInfo GetConfigDataTicketLimitInfo(int key);

    IEnumerable LuaGetAllConfigDataTicketLimitInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTicketLimitInfo>> GetAllConfigDataTicketLimitInfo();

    ConfigDataTitleInfo GetConfigDataTitleInfo(int key);

    IEnumerable LuaGetAllConfigDataTitleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTitleInfo>> GetAllConfigDataTitleInfo();

    ConfigDataTowerBattleRuleInfo GetConfigDataTowerBattleRuleInfo(
      int key);

    IEnumerable LuaGetAllConfigDataTowerBattleRuleInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTowerBattleRuleInfo>> GetAllConfigDataTowerBattleRuleInfo();

    ConfigDataTowerBonusHeroGroupInfo GetConfigDataTowerBonusHeroGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataTowerBonusHeroGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTowerBonusHeroGroupInfo>> GetAllConfigDataTowerBonusHeroGroupInfo();

    ConfigDataTowerFloorInfo GetConfigDataTowerFloorInfo(int key);

    IEnumerable LuaGetAllConfigDataTowerFloorInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTowerFloorInfo>> GetAllConfigDataTowerFloorInfo();

    ConfigDataTowerLevelInfo GetConfigDataTowerLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataTowerLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTowerLevelInfo>> GetAllConfigDataTowerLevelInfo();

    ConfigDataTrainingCourseInfo GetConfigDataTrainingCourseInfo(int key);

    IEnumerable LuaGetAllConfigDataTrainingCourseInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTrainingCourseInfo>> GetAllConfigDataTrainingCourseInfo();

    ConfigDataTrainingRoomInfo GetConfigDataTrainingRoomInfo(int key);

    IEnumerable LuaGetAllConfigDataTrainingRoomInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomInfo>> GetAllConfigDataTrainingRoomInfo();

    ConfigDataTrainingRoomLevelInfo GetConfigDataTrainingRoomLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataTrainingRoomLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomLevelInfo>> GetAllConfigDataTrainingRoomLevelInfo();

    ConfigDataTrainingTechInfo GetConfigDataTrainingTechInfo(int key);

    IEnumerable LuaGetAllConfigDataTrainingTechInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTrainingTechInfo>> GetAllConfigDataTrainingTechInfo();

    ConfigDataTrainingTechLevelInfo GetConfigDataTrainingTechLevelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataTrainingTechLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTrainingTechLevelInfo>> GetAllConfigDataTrainingTechLevelInfo();

    ConfigDataTranslate GetConfigDataTranslate(int key);

    IEnumerable LuaGetAllConfigDataTranslate();

    IEnumerable<KeyValuePair<int, ConfigDataTranslate>> GetAllConfigDataTranslate();

    ConfigDataTreasureLevelInfo GetConfigDataTreasureLevelInfo(int key);

    IEnumerable LuaGetAllConfigDataTreasureLevelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataTreasureLevelInfo>> GetAllConfigDataTreasureLevelInfo();

    ConfigDataUnchartedScoreInfo GetConfigDataUnchartedScoreInfo(int key);

    IEnumerable LuaGetAllConfigDataUnchartedScoreInfo();

    IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreInfo>> GetAllConfigDataUnchartedScoreInfo();

    ConfigDataUnchartedScoreModelInfo GetConfigDataUnchartedScoreModelInfo(
      int key);

    IEnumerable LuaGetAllConfigDataUnchartedScoreModelInfo();

    IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreModelInfo>> GetAllConfigDataUnchartedScoreModelInfo();

    ConfigDataUnchartedScoreRewardGroupInfo GetConfigDataUnchartedScoreRewardGroupInfo(
      int key);

    IEnumerable LuaGetAllConfigDataUnchartedScoreRewardGroupInfo();

    IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreRewardGroupInfo>> GetAllConfigDataUnchartedScoreRewardGroupInfo();

    ConfigDataUserGuide GetConfigDataUserGuide(int key);

    IEnumerable LuaGetAllConfigDataUserGuide();

    IEnumerable<KeyValuePair<int, ConfigDataUserGuide>> GetAllConfigDataUserGuide();

    ConfigDataUserGuideDialogInfo GetConfigDataUserGuideDialogInfo(
      int key);

    IEnumerable LuaGetAllConfigDataUserGuideDialogInfo();

    IEnumerable<KeyValuePair<int, ConfigDataUserGuideDialogInfo>> GetAllConfigDataUserGuideDialogInfo();

    ConfigDataUserGuideStep GetConfigDataUserGuideStep(int key);

    IEnumerable LuaGetAllConfigDataUserGuideStep();

    IEnumerable<KeyValuePair<int, ConfigDataUserGuideStep>> GetAllConfigDataUserGuideStep();

    ConfigDataVersionInfo GetConfigDataVersionInfo(int key);

    IEnumerable LuaGetAllConfigDataVersionInfo();

    IEnumerable<KeyValuePair<int, ConfigDataVersionInfo>> GetAllConfigDataVersionInfo();

    ConfigDataWaypointInfo GetConfigDataWaypointInfo(int key);

    IEnumerable LuaGetAllConfigDataWaypointInfo();

    IEnumerable<KeyValuePair<int, ConfigDataWaypointInfo>> GetAllConfigDataWaypointInfo();

    ConfigDataWorldMapInfo GetConfigDataWorldMapInfo(int key);

    IEnumerable LuaGetAllConfigDataWorldMapInfo();

    IEnumerable<KeyValuePair<int, ConfigDataWorldMapInfo>> GetAllConfigDataWorldMapInfo();

    ConfigDataST_CN GetConfigDataST_CN(int key);

    IEnumerable<KeyValuePair<int, ConfigDataST_CN>> GetAllConfigDataST_CN();

    IEnumerable LuaGetAllConfigDataST_CN();

    IEnumerator GetConfigDataST_CN(int key, Action<ConfigDataST_CN> onResult);

    ConfigDataST_EN GetConfigDataST_EN(int key);

    IEnumerable<KeyValuePair<int, ConfigDataST_EN>> GetAllConfigDataST_EN();

    IEnumerable LuaGetAllConfigDataST_EN();

    IEnumerator GetConfigDataST_EN(int key, Action<ConfigDataST_EN> onResult);

    int UtilityInitialize();

    ArmyRelationData UtilityGetArmyRelationData(ArmyTag attacker, ArmyTag target);

    ConfigDataGameFunctionOpenInfo UtilityGetGameFunctionOpenInfo(
      GameFunctionType gameFunctionType);

    int UtilityGetConfigableConst(ConfigableConstId id);

    string UtilityGetConfigableConstString(ConfigableConstId id);

    string UtilityGetStringByStringTable(StringTableId id);

    int UtilityGetVersion(VersionInfoId id);

    string UtilityGetVersionString(VersionInfoId id);

    string UtilityGetSound(SoundTableId id);

    string UtilityGetFxFlipName(string fxName);

    SensitiveWords UtilityGetSensitiveWords();

    int Const_BattleActorMoveSpeed { get; set; }

    int Const_CombatHeroDistance { get; set; }

    int Const_CombatSplitScreenDistance { get; set; }

    int Const_MeleeATKPunish_Mult { get; set; }

    int Const_SoldierMoveDelay { get; set; }

    int Const_SoldierReturnDelay { get; set; }

    int Const_SkillPauseTime { get; set; }

    int Const_SkillPreCastDelay { get; set; }

    int Const_DamagePostDelay { get; set; }

    int Const_BuffHitPostDelay { get; set; }

    int Const_SoldierCountMax { get; set; }

    int Const_HPBarFxAccumulateTime { get; set; }

    int Const_HPBarFxAccumulateDamage { get; set; }

    int Const_CriticalDamageBase { get; set; }

    int ConfigableConstId_FlushTime { get; set; }

    int ConfigableConstId_FlushPeriodDay { get; set; }

    int ConfigableConstId_TimeEventStartTime { get; set; }

    int ConfigableConstId_TimeEventEndTime { get; set; }

    int ConfigableConstId_TimeEventMinCount { get; set; }

    int ConfigableConstId_TimeEventMaxCount { get; set; }

    int ConfigableConstId_RiftLevelActionEventProbality { get; set; }

    int ConfigableConstId_ScenarioActionEventProbality { get; set; }

    int ConfigableConstId_EventActionEventProbality { get; set; }

    int ConfigableConstId_ProbalityMax { get; set; }

    int ConfigableConstId_ActionEventMaxCount { get; set; }

    int ConfigableConstId_MapRandomEventMaxCount { get; set; }

    int ConfigableConstId_EnergyAddPeriod { get; set; }

    int ConfigableConstId_EnergyMax { get; set; }

    int ConfigableConstId_BagMaxCapacity { get; set; }

    int ConfigableConstId_HeroJobRankMax { get; set; }

    int ConfigableConstId_HeroLevelUpCeiling { get; set; }

    int ConfigableConstId_SelectSkillsMaxCount { get; set; }

    int ConfigableConstId_BagGridMaxCapacity { get; set; }

    int ConfigableConstId_HeroStarLevelMax { get; set; }

    int ConfigableConstId_RiftLevelMaxStar { get; set; }

    int ConfigableConstId_JobStartLeve { get; set; }

    int ConfigableConstId_FirstScenarioId { get; set; }

    int ConfigableConstId_FirstWayPointId { get; set; }

    int ConfigableConstId_MailBoxMaxSize { get; set; }

    int ConfigableConstId_WeaponSlotId { get; set; }

    int ConfigableConstId_ArmorSlotId { get; set; }

    int ConfigableConstId_OrnamentSlotId { get; set; }

    int ConfigableConstId_ShoeSlotId { get; set; }

    int ConfigableConstId_FirstInitId { get; set; }

    int ConfigableConstId_PlayerLevelMaxLevel { get; set; }

    int ConfigableConstId_ChatMessageMaxLength { get; set; }

    int ConfigableConstId_WorldChatIntervalTime { get; set; }

    int ConfigableConstId_PeakArenaPlayOffChatIntervalTime { get; set; }

    int ConfigableConstId_AllowChatPlayerLevel { get; set; }

    int ConfigableConstId_SystemSelectCardHeroBroadcast { get; set; }

    int ConfigableConstId_MagicStoneId { get; set; }

    int ConfigableConstId_BuyEnergyCount { get; set; }

    int ConfigableConstId_HeroCommentMaxLength { get; set; }

    int ConfigableConstId_HeroCommentMaxNums { get; set; }

    int ConfigableConstId_PlayerSingleHeroCommentMaxNums { get; set; }

    int ConfigableConstId_MaxPopularCommentEntry { get; set; }

    int ConfigableConstId_AllowArenaPlayerLevel { get; set; }

    int ConfigableConstId_ArenaGivenTicketMaxNums { get; set; }

    int ConfigableConstId_LevelRaidTicketID { get; set; }

    int ConfigableConstId_ArenaFightExpiredTimeInterval { get; set; }

    int ConfigableConstId_ProtagonistHeroID { get; set; }

    int ConfigableConstId_PlayerNameMaxLength { get; set; }

    int ConfigableConstId_ArenaBattleReportMaxNums { get; set; }

    int ConfigableConstId_ArenaVictoryPointsRewardMaxVictionaryPoints { get; set; }

    int ConfigableConstId_ArenaOneTimeGiveTicketsNums { get; set; }

    int ConfigableConstId_ArenaInitialPoints { get; set; }

    int ConfigableConstId_ArenaOneTimeAttackUseTicketsNums { get; set; }

    int ConfigableConstId_ArenaAttackSuccessRandomDropRewardID { get; set; }

    int ConfigableConstId_ArenaRealWeekSettleDeltaTime { get; set; }

    int ConfigableConstId_ChangeNameCostNums { get; set; }

    int ConfigableConstId_PlayerInitialHeroInteractNums { get; set; }

    int ConfigableConstId_HeroInteractNumsRecoveryPeriod { get; set; }

    int ConfigableConstId_HeroIteractMaxNums { get; set; }

    int ConfigableConstId_HeroIntimateMaxValue { get; set; }

    int ConfigableConstId_EnhanceEquipmentConsumeGoldPerExp { get; set; }

    int ConfigableConstId_LevelUpEquipmentStarConsumeGoldPerStar { get; set; }

    int ConfigableConstId_BuyArenaTicketCount { get; set; }

    int ConfigableConstId_HeroDungeonLevelMaxStar { get; set; }

    int ConfigableConstId_ArenaAddHeroExp { get; set; }

    int ConfigableConstId_ArenaAddPlayerExp { get; set; }

    int ConfigableConstId_SystemSelectCardEquipmentBroadcast { get; set; }

    int ConfigableConstId_UserGuideRandomEventId { get; set; }

    int ConfigableConstId_WriteSurveyPlayerLevel { get; set; }

    int ConfigableConstId_AutoEquipmentDeltaTime { get; set; }

    int ConfigableConstId_MaxLevelDanmakuEntryLength { get; set; }

    int ConfigableConstId_MaxLevelDanmakuEntryNumsPerTurn { get; set; }

    int ConfigableConstId_TeamRoomInviteDeltaTime { get; set; }

    int BuyEnergyMaxNums { get; set; }

    int BuyArenaTicketMaxNums { get; set; }

    int ConfigableConstId_BusinessCardDescMaxLength { get; set; }

    int ConfigableConstId_BusinessCardHeroMaxNums { get; set; }

    int ConfigableConstId_SendFriendShipPointsEverytime { get; set; }

    int ConfigableConstId_SendFriendShipPointsMaxTimes { get; set; }

    int ConfigableConstId_ReceiveFriendShipPointsMaxTimes { get; set; }

    int ConfigableConstId_UseBoxItemMaxCount { get; set; }

    int ConfigableConstId_ServerTolerateClientSpeedUpMaxNums { get; set; }

    int ConfigableConstId_ServerCheckClientHeartBeatReachMinPeriod { get; set; }

    int ConfigableConstId_ClientSendHeartBeatPeriod { get; set; }

    int ConfigableConstId_ClientCheckOnlinePeriod { get; set; }

    int ConfigableConstId_SearchUserByNameResultMax { get; set; }

    int ConfigableConstId_MaxDomesticFriends { get; set; }

    int ConfigableConstId_MaxForeignFriends { get; set; }

    int ConfigableConstId_MaxBlacklist { get; set; }

    int ConfigableConstId_MaxInvites { get; set; }

    int ConfigableConstId_MaxSuggestedUsers { get; set; }

    int ConfigableConstId_MaxLevelDiffMultiply { get; set; }

    int ConfigableConstId_SuggestedUserLevelDiff { get; set; }

    int ConfigableConstId_InviteExpireSeconds { get; set; }

    int ConfigableConstId_RecentContactsMaxCount { get; set; }

    int ConfigableConstId_HeroAssistantTaskHeroAssignMaxCount { get; set; }

    int ConfigableConstId_MaxGroupsPerUser { get; set; }

    int ConfigableConstId_MaxGroupMembers { get; set; }

    int ConfigableConstId_BattleInviteFriendsToPracticeMaxWaitInterval { get; set; }

    int ConfigableConstId_DayBonusNum_Aniki { get; set; }

    int ConfigableConstId_DayBonusNum_ThearchyTrial { get; set; }

    int ConfigableConstId_DayBonusNum_HeroTrain { get; set; }

    int ConfigableConstId_DayBonusNum_MemoryCorridor { get; set; }

    int ConfigableConstId_DayBonusNum_Crusade { get; set; }

    int ConfigableConstId_FriendPointsFightWithFriends { get; set; }

    int ConfigableConstId_MaxFriendPointsFightWithFriendsEveryday { get; set; }

    int ConfigableConstId_BattleRoomPlayerReconnectTimeOutTime { get; set; }

    int ConfigableConstId_TeamBattleRoomPlayerActionClientTimeOutTime { get; set; }

    int ConfigableConstId_PVPBattleRoomPlayerActionClientTimeOutTime { get; set; }

    int ConfigableConstId_TeamBattleRoomPlayerActionServerTimeOutTime { get; set; }

    int ConfigableConstId_PVPBattleRoomPlayerActionServerTimeOutTime { get; set; }

    int ConfigableConstId_TeamFriendShipPointRewardPerFriend { get; set; }

    int ConfigableConstId_MaxEnchantSamePropertyNums { get; set; }

    int ConfigableConstId_TeamInvitationTimeoutTime { get; set; }

    int ConfigableConstId_PVPInvitationTimeoutTime { get; set; }

    int ConfigableConstId_DecomposeEquipmentBackGoldPercent { get; set; }

    int ConfigableConstId_RecentContactsChatMaxCount { get; set; }

    int ConfigableConstId_RecentContactsTeamBattleMaxCount { get; set; }

    int ConfigableConstId_DayBonusNum_CooperateBattle { get; set; }

    int ConfigableConstId_HeroAssistantTaskSlotCount { get; set; }

    int ConfigableConstId_ChatGroupNameMaxLength { get; set; }

    int ConfigableConstId_ChatGroupCreateMinUserCount { get; set; }

    int ConfigableConstId_RefluxBeginLevelLimit { get; set; }

    int ConfigableConstId_HeroAnthemLevelLimit { get; set; }

    int ConfigableConstId_AncientCallLevelLimit { get; set; }

    int ConfigableConstId_ChatGroupDisbandUserCount { get; set; }

    int ConfigableConstId_DefaultHeadFrameId { get; set; }

    int ConfigableConstId_RequestAppReviewInScenario { get; set; }

    int ConfigableConstId_IsRequestAppReviewOn { get; set; }

    int ConfigableConstId_GainMaximum { get; set; }

    int RealTimePVPInclusiveMinDan { get; set; }

    int RealTimePVPExclusiveMaxDan { get; set; }

    int RealTimeArenaNewPlayerMatchCount { get; set; }

    int ConfigableConstId_RealTimePVPMinRequiredLevel { get; set; }

    int ConfigableConstId_RealTimePVPBotPlayDeadIntervalMin { get; set; }

    int ConfigableConstId_RealTimePVPBotPlayDeadIntervalMax { get; set; }

    int ConfigableConstId_AlchemyMaxNum { get; set; }

    int ConfigableConstId_ItemComposeMaxLimit { get; set; }

    int ConfigableConstId_RealTimePVPBattleReportMaxNums { get; set; }

    int ConfigableConstId_NewUserAccumulatedMinValue { get; set; }

    int ConfigableConstId_NewUserAccumulatedMaxValue { get; set; }

    int ConfigableConstId_OldUserAccumulatedMinValue { get; set; }

    int ConfigableConstId_OldUserAccumulatedMaxValue { get; set; }

    int ConfigableConstId_GuildCreateJoinLevel { get; set; }

    int ConfigableConstId_GuildCreateItemId { get; set; }

    int ConfigableConstId_GuildMemberCountMax { get; set; }

    int ConfigableConstId_GuildReJoinCoolDownTime { get; set; }

    int ConfigableConstId_GuildJoinApplicationPlayerCountMax { get; set; }

    int ConfigableConstId_GuildInviteCountMax { get; set; }

    int ConfigableConstId_GuildChatHistoryCount { get; set; }

    int ConfigableConstId_GuildHiringDeclarationMaxLength { get; set; }

    int ConfigableConstId_GuildVicePresidentMaxNums { get; set; }

    int ConfigableConstId_GuildEliteMinTotalActivities { get; set; }

    int ConfigableConstId_RankListGuildNums { get; set; }

    int ConfigableConstId_GuildVicePresidentCanUsurpTime { get; set; }

    int ConfigableConstId_GuildEliteCanUsurpTime { get; set; }

    int ConfigableConstId_GuildChangeNameCrystalCost { get; set; }

    int ConfigableConstId_GuildDailyMaxActivities { get; set; }

    int ConfigableConstId_GuildAnnouncementMaxLength { get; set; }

    int ConfigableConstId_GuildWeeklyMaxActivities { get; set; }

    int ConfigableConstId_GuildNameMaxLength { get; set; }

    int ConfigableConstId_GuildLogMaxNUms { get; set; }

    int ConfigableConstId_NewUserSecondAccumulatedMinValue { get; set; }

    int ConfigableConstId_NewUserSecondAccumulatedMaxValue { get; set; }

    int ConfigableConstId_RandomStoreManualFlushMaxNums { get; set; }

    int ConfigableConstId_HeroDungeonDailyChallengeMaxNums { get; set; }

    int ConfigableConstId_GuildMassiveCombatAvailableCountPerWeek { get; set; }

    int ConfigableConstId_GuildMassiveCombatMinTitleToStart { get; set; }

    int ConfigableConstId_GuildMassiveCombatMinTitleToSurrender { get; set; }

    int ConfigableConstId_GuildMassiveCombatMaxHeroTagIdForStronghold { get; set; }

    int[] GuildMassiveCombatRandomHeroTagIds { get; set; }

    int ConfigableConstId_EternalShrineDailyChallengeCountMax { get; set; }

    bool ConfigableConstId_AppleSubscribeMonthCardCanRepeatlyBuy { get; set; }

    int ConfigableConstId_PeakArenaBanTime { get; set; }

    int ConfigableConstId_PeakArenaPickTime { get; set; }

    int ConfigableConstId_PeakArenaClientStartBPStageLoadTime { get; set; }

    int ConfigableConstId_PeakArenaClientStartBattleLoadTime { get; set; }

    int ConfigableConstId_PeakArenaBPPublicTime { get; set; }

    int ConfigableConstId_PeakArenaReadyTime { get; set; }

    int ConfigableConstId_PeakArenaBattleRoomPlayerActionTime { get; set; }

    int ConfigableConstId_PeakArenaBattlePublicTime { get; set; }

    List<PeakAreanaBPFlowInfo> PeakAreanaBPFlowInfos { get; set; }

    RandomStoreData RandomStoreData { get; set; }

    RandomDropDataInfo RandomDropData { get; set; }

    ClientRandomDropData ClientRandomDropData { get; set; }

    FixedStoreDataInfo FixedStoreData { get; set; }

    SignRewardDataInfo SignRewardData { get; set; }

    MissionDataInfo MissionData { get; set; }

    ThearchyTrialDataInfo ThearchyTrialData { get; set; }

    EternalShrineDataInfo EternalShrineData { get; set; }

    AnikiGymDataInfo AnikiGymData { get; set; }

    MemoryCorridorDataInfo MemoryCorridorData { get; set; }

    HeroTrainningDataInfo HeroTrainningData { get; set; }

    Dictionary<int, ConfigDataTicketLimitInfo> TicketId2TicketLimitInfo { get; set; }

    BlackJack.ConfigData.RealTimePVPAvailableTime[] RealTimePVPAvailableTime { get; set; }

    DayOfWeek[] RealTimePVPOpenWeekday { get; set; }

    DayOfWeek[] PeakArenaOpenWeekday { get; set; }

    int PeakArenaInclusiveMinDan { get; set; }

    int PeakArenaExclusiveMaxDan { get; set; }

    BlackJack.ConfigData.PeakArenaRegularAvailableTime[] PeakArenaRegularAvailableTime { get; set; }

    List<ConfigDataPeakArenaDanInfo> PeakArenaDanInfoSortedById { get; set; }

    List<ConfigDataPeakArenaDanInfo> PeakArenaDanInfoSortedByBaselineScore { get; set; }

    int ConfigableConstId_PeakArenaBattleTeamHeroCount { get; set; }

    int ConfigableConstId_PeakArenaPlayerEnterLevel { get; set; }

    int ConfigableConstId_PeakArenaPreRankingMatchCount { get; set; }

    int ConfigableConstId_PeakArenaPreRankingMatchWinScore { get; set; }

    int ConfigableConstId_PeakArenaPreRankingMatchLossScore { get; set; }

    int ConfigableConstId_PeakArenaPreRankingMatchmakingDanMax { get; set; }

    int ConfigableConstId_PeakArenaMatchmakingSameOpponentAvoidMatchCount { get; set; }

    int ConfigableConstId_PeakArenaLuckyHeroOwnerRewardMailTemplateId { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolRCount { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSRCount { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSSRCount { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolRMinRank { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSRMinRank { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSSRMinRank { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolRMaxRank { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSRMaxRank { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSSRMaxRank { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolRRangeMin { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSRRangeMin { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSSRRangeMin { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolRRangeMax { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSRRangeMax { get; set; }

    int ConfigableConstId_PeakArenaHeroPoolSSRRangeMax { get; set; }

    int ConfigableConstId_PeakArenaInitScore { get; set; }

    int ConfigableConstId_BusinessCardHeroicMomentMaxNums { get; set; }

    int ConfigableConstId_ChatShareEquipeNumLimit { get; set; }

    int ConfigableConstId_PeakArenaBattleReportMaxNums { get; set; }

    int ConfigableConstId_PeakArenaJettonId { get; set; }

    int ConfigableConstId_PeakArenaJettonExchangeGoldNums { get; set; }

    int ConfigableConstId_PeakArenaJettonFunctionMinPlayerLvl { get; set; }

    int ConfigableConstId_PeakArenaJettonGiveNums { get; set; }

    int ConfigableConstId_PeakArenaJettonDailyBoughtMaxNums { get; set; }

    int ConfigableConstId_PeakArenaJettonDailyBetJettonMaxNums { get; set; }

    int ConfigableConstId_PeakArenaJettonBoughtCurrencyCost { get; set; }

    int ConfigableConstId_PeakArenaJettonBoughtCurrencyType { get; set; }

    int ConfigableConstId_PeakArenaJettonGotNumsOnceBought { get; set; }

    int ConfigableConstId_PeakArenaPlayOffReadyTime { get; set; }

    int ConfigableConstId_PeakArenaLadderDailyQuickLeavingToleranceCount { get; set; }

    int ConfigableConstId_PeakArenaLadderDailyQuickLeavingBattleDuration { get; set; }

    int ConfigableConstId_StoryKilometersSecond { get; set; }

    int ConfigableConstId_StoryKilometersSecondSpeed { get; set; }

    int ConfigableConstId_UserGuideStoryOutlineInfoID { get; set; }

    int ConfigableConstId_HeroFinalJobRank { get; set; }

    int ConfigableConstId_HeroJobNormalSlotPropertyMaxNums { get; set; }

    int ConfigableConstId_HeroJobSpecificSlotPropertyMaxNums { get; set; }

    int ConfigableConstId_UnchartedLevelRaidTimesPerDay { get; set; }

    int ConfigableConstId_UnchartedLevelRaidTimesExtraAddByMonthCard { get; set; }
  }
}
