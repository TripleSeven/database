﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGiftStoreItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGiftStoreItemInfo")]
  [Serializable]
  public class ConfigDataGiftStoreItemInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private StoreId _StoreId;
    private GoodsType _ItemType;
    private int _ItemId;
    private int _Nums;
    private string _ShowStartTime;
    private string _ShowEndTime;
    private bool _IsOperateGoods;
    private BuyRuleType _BuyLimitType;
    private int _Param;
    private int _Count;
    private List<Goods> _FirstReward;
    private double _FirstPrice;
    private double _NormalPrice;
    private LabelType _Lable;
    private bool _IsAppleSubscribe;
    private int _FirstBuyCompensation;
    private int _NormalBuyCompensation;
    private int _PlayerBuyMinLevel;
    private int _Sort;
    private int _IsRecommendDisplay;
    private string _RecommendDisplayStartTime;
    private string _DiscountText;
    private string _BigAdvertisingImage;
    private string _Desc;
    private int _IsStrongRecommend;
    private string _SmallAdvertisingImage;
    private string _RecommendDesc;
    private bool _IsShowIOSSpecificGo;
    private int _PreUnlockGoodsId;
    private int _DollGroup;
    private bool _NotShowInStore;
    private int _OpenActivityIdAfterPurchase;
    private bool _IsHideItemList;
    private GiftType _GiftType;
    private IExtension extensionObject;
    public int PostUnlockItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StoreId")]
    public StoreId StoreId
    {
      get
      {
        return this._StoreId;
      }
      set
      {
        this._StoreId = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemType")]
    public GoodsType ItemType
    {
      get
      {
        return this._ItemType;
      }
      set
      {
        this._ItemType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemId")]
    public int ItemId
    {
      get
      {
        return this._ItemId;
      }
      set
      {
        this._ItemId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Nums")]
    public int Nums
    {
      get
      {
        return this._Nums;
      }
      set
      {
        this._Nums = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "ShowStartTime")]
    public string ShowStartTime
    {
      get
      {
        return this._ShowStartTime;
      }
      set
      {
        this._ShowStartTime = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "ShowEndTime")]
    public string ShowEndTime
    {
      get
      {
        return this._ShowEndTime;
      }
      set
      {
        this._ShowEndTime = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsOperateGoods")]
    public bool IsOperateGoods
    {
      get
      {
        return this._IsOperateGoods;
      }
      set
      {
        this._IsOperateGoods = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyLimitType")]
    public BuyRuleType BuyLimitType
    {
      get
      {
        return this._BuyLimitType;
      }
      set
      {
        this._BuyLimitType = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param")]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Count")]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "FirstReward")]
    public List<Goods> FirstReward
    {
      get
      {
        return this._FirstReward;
      }
      set
      {
        this._FirstReward = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstPrice")]
    public double FirstPrice
    {
      get
      {
        return this._FirstPrice;
      }
      set
      {
        this._FirstPrice = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NormalPrice")]
    public double NormalPrice
    {
      get
      {
        return this._NormalPrice;
      }
      set
      {
        this._NormalPrice = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Lable")]
    public LabelType Lable
    {
      get
      {
        return this._Lable;
      }
      set
      {
        this._Lable = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAppleSubscribe")]
    public bool IsAppleSubscribe
    {
      get
      {
        return this._IsAppleSubscribe;
      }
      set
      {
        this._IsAppleSubscribe = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstBuyCompensation")]
    public int FirstBuyCompensation
    {
      get
      {
        return this._FirstBuyCompensation;
      }
      set
      {
        this._FirstBuyCompensation = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NormalBuyCompensation")]
    public int NormalBuyCompensation
    {
      get
      {
        return this._NormalBuyCompensation;
      }
      set
      {
        this._NormalBuyCompensation = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerBuyMinLevel")]
    public int PlayerBuyMinLevel
    {
      get
      {
        return this._PlayerBuyMinLevel;
      }
      set
      {
        this._PlayerBuyMinLevel = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Sort")]
    public int Sort
    {
      get
      {
        return this._Sort;
      }
      set
      {
        this._Sort = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "IsRecommendDisplay")]
    public int IsRecommendDisplay
    {
      get
      {
        return this._IsRecommendDisplay;
      }
      set
      {
        this._IsRecommendDisplay = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, IsRequired = true, Name = "RecommendDisplayStartTime")]
    public string RecommendDisplayStartTime
    {
      get
      {
        return this._RecommendDisplayStartTime;
      }
      set
      {
        this._RecommendDisplayStartTime = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, IsRequired = true, Name = "DiscountText")]
    public string DiscountText
    {
      get
      {
        return this._DiscountText;
      }
      set
      {
        this._DiscountText = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, IsRequired = true, Name = "BigAdvertisingImage")]
    public string BigAdvertisingImage
    {
      get
      {
        return this._BigAdvertisingImage;
      }
      set
      {
        this._BigAdvertisingImage = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "IsStrongRecommend")]
    public int IsStrongRecommend
    {
      get
      {
        return this._IsStrongRecommend;
      }
      set
      {
        this._IsStrongRecommend = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.Default, IsRequired = true, Name = "SmallAdvertisingImage")]
    public string SmallAdvertisingImage
    {
      get
      {
        return this._SmallAdvertisingImage;
      }
      set
      {
        this._SmallAdvertisingImage = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.Default, IsRequired = true, Name = "RecommendDesc")]
    public string RecommendDesc
    {
      get
      {
        return this._RecommendDesc;
      }
      set
      {
        this._RecommendDesc = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsShowIOSSpecificGo")]
    public bool IsShowIOSSpecificGo
    {
      get
      {
        return this._IsShowIOSSpecificGo;
      }
      set
      {
        this._IsShowIOSSpecificGo = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PreUnlockGoodsId")]
    public int PreUnlockGoodsId
    {
      get
      {
        return this._PreUnlockGoodsId;
      }
      set
      {
        this._PreUnlockGoodsId = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DollGroup")]
    public int DollGroup
    {
      get
      {
        return this._DollGroup;
      }
      set
      {
        this._DollGroup = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.Default, IsRequired = true, Name = "NotShowInStore")]
    public bool NotShowInStore
    {
      get
      {
        return this._NotShowInStore;
      }
      set
      {
        this._NotShowInStore = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenActivityIdAfterPurchase")]
    public int OpenActivityIdAfterPurchase
    {
      get
      {
        return this._OpenActivityIdAfterPurchase;
      }
      set
      {
        this._OpenActivityIdAfterPurchase = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsHideItemList")]
    public bool IsHideItemList
    {
      get
      {
        return this._IsHideItemList;
      }
      set
      {
        this._IsHideItemList = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GiftType")]
    public GiftType GiftType
    {
      get
      {
        return this._GiftType;
      }
      set
      {
        this._GiftType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
