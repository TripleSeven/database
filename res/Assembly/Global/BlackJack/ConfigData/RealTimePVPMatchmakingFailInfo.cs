﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPMatchmakingFailInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPMatchmakingFailInfo")]
  [Serializable]
  public class RealTimePVPMatchmakingFailInfo : IExtensible
  {
    private int _DanMin;
    private int _DanMax;
    private bool _IsBot;
    private int _BotLevelAdjustment;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanMin")]
    public int DanMin
    {
      get
      {
        return this._DanMin;
      }
      set
      {
        this._DanMin = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanMax")]
    public int DanMax
    {
      get
      {
        return this._DanMax;
      }
      set
      {
        this._DanMax = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsBot")]
    public bool IsBot
    {
      get
      {
        return this._IsBot;
      }
      set
      {
        this._IsBot = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BotLevelAdjustment")]
    public int BotLevelAdjustment
    {
      get
      {
        return this._BotLevelAdjustment;
      }
      set
      {
        this._BotLevelAdjustment = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
