﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionDataInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class MissionDataInfo
  {
    public List<ConfigDataMissionInfo> EverydayMissions;
    public List<ConfigDataMissionInfo> OneOffMissions;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionDataInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
