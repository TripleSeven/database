﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMonsterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMonsterInfo")]
  [Serializable]
  public class ConfigDataMonsterInfo : IExtensible
  {
    private int _ID;
    private bool _IsBoss;
    private bool _NoDie;
    private int _ActionLoop;
    private bool _AddDamage;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsBoss")]
    public bool IsBoss
    {
      get
      {
        return this._IsBoss;
      }
      set
      {
        this._IsBoss = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "NoDie")]
    public bool NoDie
    {
      get
      {
        return this._NoDie;
      }
      set
      {
        this._NoDie = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActionLoop")]
    public int ActionLoop
    {
      get
      {
        return this._ActionLoop;
      }
      set
      {
        this._ActionLoop = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "AddDamage")]
    public bool AddDamage
    {
      get
      {
        return this._AddDamage;
      }
      set
      {
        this._AddDamage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
