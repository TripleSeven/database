﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PointRaceRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PointRaceRewardInfo")]
  [Serializable]
  public class PointRaceRewardInfo : IExtensible
  {
    private int _BattleNum;
    private int _DanId;
    private int _TemplateMailId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleNum")]
    public int BattleNum
    {
      get
      {
        return this._BattleNum;
      }
      set
      {
        this._BattleNum = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanId")]
    public int DanId
    {
      get
      {
        return this._DanId;
      }
      set
      {
        this._DanId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TemplateMailId")]
    public int TemplateMailId
    {
      get
      {
        return this._TemplateMailId;
      }
      set
      {
        this._TemplateMailId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
