﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LinkageHeroId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LinkageHeroId")]
  public enum LinkageHeroId
  {
    [ProtoEnum(Name = "LinkageHeroId_None", Value = 0)] LinkageHeroId_None,
    [ProtoEnum(Name = "LinkageHeroId_KongGui", Value = 1)] LinkageHeroId_KongGui,
    [ProtoEnum(Name = "LinkageHeroId_YingZhan", Value = 2)] LinkageHeroId_YingZhan,
  }
}
