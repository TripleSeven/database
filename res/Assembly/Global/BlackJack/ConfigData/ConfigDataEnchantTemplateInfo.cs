﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEnchantTemplateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEnchantTemplateInfo")]
  [Serializable]
  public class ConfigDataEnchantTemplateInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private PropertyModifyType _Property1_ID;
    private List<EnchantPropertyValueInfo> _ValueRange1;
    private int _Weight1;
    private PropertyModifyType _Property2_ID;
    private List<EnchantPropertyValueInfo> _ValueRange2;
    private int _Weight2;
    private PropertyModifyType _Property3_ID;
    private List<EnchantPropertyValueInfo> _ValueRange3;
    private int _Weight3;
    private PropertyModifyType _Property4_ID;
    private List<EnchantPropertyValueInfo> _ValueRange4;
    private int _Weight4;
    private PropertyModifyType _Property5_ID;
    private List<EnchantPropertyValueInfo> _ValueRange5;
    private int _Weight5;
    private PropertyModifyType _Property6_ID;
    private List<EnchantPropertyValueInfo> _ValueRange6;
    private int _Weight6;
    private PropertyModifyType _Property7_ID;
    private List<EnchantPropertyValueInfo> _ValueRange7;
    private int _Weight7;
    private PropertyModifyType _Property8_ID;
    private List<EnchantPropertyValueInfo> _ValueRange8;
    private int _Weight8;
    private PropertyModifyType _Property9_ID;
    private List<EnchantPropertyValueInfo> _ValueRange9;
    private int _Weight9;
    private PropertyModifyType _Property10_ID;
    private List<EnchantPropertyValueInfo> _ValueRange10;
    private int _Weight10;
    private PropertyModifyType _Property11_ID;
    private List<EnchantPropertyValueInfo> _ValueRange11;
    private int _Weight11;
    private PropertyModifyType _Property12_ID;
    private List<EnchantPropertyValueInfo> _ValueRange12;
    private int _Weight12;
    private PropertyModifyType _Property13_ID;
    private List<EnchantPropertyValueInfo> _ValueRange13;
    private int _Weight13;
    private PropertyModifyType _Property14_ID;
    private List<EnchantPropertyValueInfo> _ValueRange14;
    private int _Weight14;
    private PropertyModifyType _Property15_ID;
    private List<EnchantPropertyValueInfo> _ValueRange15;
    private int _Weight15;
    private PropertyModifyType _Property16_ID;
    private List<EnchantPropertyValueInfo> _ValueRange16;
    private int _Weight16;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_ID")]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "ValueRange1")]
    public List<EnchantPropertyValueInfo> ValueRange1
    {
      get
      {
        return this._ValueRange1;
      }
      set
      {
        this._ValueRange1 = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight1")]
    public int Weight1
    {
      get
      {
        return this._Weight1;
      }
      set
      {
        this._Weight1 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_ID")]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "ValueRange2")]
    public List<EnchantPropertyValueInfo> ValueRange2
    {
      get
      {
        return this._ValueRange2;
      }
      set
      {
        this._ValueRange2 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight2")]
    public int Weight2
    {
      get
      {
        return this._Weight2;
      }
      set
      {
        this._Weight2 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_ID")]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "ValueRange3")]
    public List<EnchantPropertyValueInfo> ValueRange3
    {
      get
      {
        return this._ValueRange3;
      }
      set
      {
        this._ValueRange3 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight3")]
    public int Weight3
    {
      get
      {
        return this._Weight3;
      }
      set
      {
        this._Weight3 = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property4_ID")]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "ValueRange4")]
    public List<EnchantPropertyValueInfo> ValueRange4
    {
      get
      {
        return this._ValueRange4;
      }
      set
      {
        this._ValueRange4 = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight4")]
    public int Weight4
    {
      get
      {
        return this._Weight4;
      }
      set
      {
        this._Weight4 = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property5_ID")]
    public PropertyModifyType Property5_ID
    {
      get
      {
        return this._Property5_ID;
      }
      set
      {
        this._Property5_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "ValueRange5")]
    public List<EnchantPropertyValueInfo> ValueRange5
    {
      get
      {
        return this._ValueRange5;
      }
      set
      {
        this._ValueRange5 = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight5")]
    public int Weight5
    {
      get
      {
        return this._Weight5;
      }
      set
      {
        this._Weight5 = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property6_ID")]
    public PropertyModifyType Property6_ID
    {
      get
      {
        return this._Property6_ID;
      }
      set
      {
        this._Property6_ID = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, Name = "ValueRange6")]
    public List<EnchantPropertyValueInfo> ValueRange6
    {
      get
      {
        return this._ValueRange6;
      }
      set
      {
        this._ValueRange6 = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight6")]
    public int Weight6
    {
      get
      {
        return this._Weight6;
      }
      set
      {
        this._Weight6 = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property7_ID")]
    public PropertyModifyType Property7_ID
    {
      get
      {
        return this._Property7_ID;
      }
      set
      {
        this._Property7_ID = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, Name = "ValueRange7")]
    public List<EnchantPropertyValueInfo> ValueRange7
    {
      get
      {
        return this._ValueRange7;
      }
      set
      {
        this._ValueRange7 = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight7")]
    public int Weight7
    {
      get
      {
        return this._Weight7;
      }
      set
      {
        this._Weight7 = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property8_ID")]
    public PropertyModifyType Property8_ID
    {
      get
      {
        return this._Property8_ID;
      }
      set
      {
        this._Property8_ID = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, Name = "ValueRange8")]
    public List<EnchantPropertyValueInfo> ValueRange8
    {
      get
      {
        return this._ValueRange8;
      }
      set
      {
        this._ValueRange8 = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight8")]
    public int Weight8
    {
      get
      {
        return this._Weight8;
      }
      set
      {
        this._Weight8 = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property9_ID")]
    public PropertyModifyType Property9_ID
    {
      get
      {
        return this._Property9_ID;
      }
      set
      {
        this._Property9_ID = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.Default, Name = "ValueRange9")]
    public List<EnchantPropertyValueInfo> ValueRange9
    {
      get
      {
        return this._ValueRange9;
      }
      set
      {
        this._ValueRange9 = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight9")]
    public int Weight9
    {
      get
      {
        return this._Weight9;
      }
      set
      {
        this._Weight9 = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property10_ID")]
    public PropertyModifyType Property10_ID
    {
      get
      {
        return this._Property10_ID;
      }
      set
      {
        this._Property10_ID = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.Default, Name = "ValueRange10")]
    public List<EnchantPropertyValueInfo> ValueRange10
    {
      get
      {
        return this._ValueRange10;
      }
      set
      {
        this._ValueRange10 = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight10")]
    public int Weight10
    {
      get
      {
        return this._Weight10;
      }
      set
      {
        this._Weight10 = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property11_ID")]
    public PropertyModifyType Property11_ID
    {
      get
      {
        return this._Property11_ID;
      }
      set
      {
        this._Property11_ID = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.Default, Name = "ValueRange11")]
    public List<EnchantPropertyValueInfo> ValueRange11
    {
      get
      {
        return this._ValueRange11;
      }
      set
      {
        this._ValueRange11 = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight11")]
    public int Weight11
    {
      get
      {
        return this._Weight11;
      }
      set
      {
        this._Weight11 = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property12_ID")]
    public PropertyModifyType Property12_ID
    {
      get
      {
        return this._Property12_ID;
      }
      set
      {
        this._Property12_ID = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.Default, Name = "ValueRange12")]
    public List<EnchantPropertyValueInfo> ValueRange12
    {
      get
      {
        return this._ValueRange12;
      }
      set
      {
        this._ValueRange12 = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight12")]
    public int Weight12
    {
      get
      {
        return this._Weight12;
      }
      set
      {
        this._Weight12 = value;
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property13_ID")]
    public PropertyModifyType Property13_ID
    {
      get
      {
        return this._Property13_ID;
      }
      set
      {
        this._Property13_ID = value;
      }
    }

    [ProtoMember(41, DataFormat = DataFormat.Default, Name = "ValueRange13")]
    public List<EnchantPropertyValueInfo> ValueRange13
    {
      get
      {
        return this._ValueRange13;
      }
      set
      {
        this._ValueRange13 = value;
      }
    }

    [ProtoMember(42, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight13")]
    public int Weight13
    {
      get
      {
        return this._Weight13;
      }
      set
      {
        this._Weight13 = value;
      }
    }

    [ProtoMember(43, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property14_ID")]
    public PropertyModifyType Property14_ID
    {
      get
      {
        return this._Property14_ID;
      }
      set
      {
        this._Property14_ID = value;
      }
    }

    [ProtoMember(44, DataFormat = DataFormat.Default, Name = "ValueRange14")]
    public List<EnchantPropertyValueInfo> ValueRange14
    {
      get
      {
        return this._ValueRange14;
      }
      set
      {
        this._ValueRange14 = value;
      }
    }

    [ProtoMember(45, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight14")]
    public int Weight14
    {
      get
      {
        return this._Weight14;
      }
      set
      {
        this._Weight14 = value;
      }
    }

    [ProtoMember(46, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property15_ID")]
    public PropertyModifyType Property15_ID
    {
      get
      {
        return this._Property15_ID;
      }
      set
      {
        this._Property15_ID = value;
      }
    }

    [ProtoMember(47, DataFormat = DataFormat.Default, Name = "ValueRange15")]
    public List<EnchantPropertyValueInfo> ValueRange15
    {
      get
      {
        return this._ValueRange15;
      }
      set
      {
        this._ValueRange15 = value;
      }
    }

    [ProtoMember(48, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight15")]
    public int Weight15
    {
      get
      {
        return this._Weight15;
      }
      set
      {
        this._Weight15 = value;
      }
    }

    [ProtoMember(49, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property16_ID")]
    public PropertyModifyType Property16_ID
    {
      get
      {
        return this._Property16_ID;
      }
      set
      {
        this._Property16_ID = value;
      }
    }

    [ProtoMember(50, DataFormat = DataFormat.Default, Name = "ValueRange16")]
    public List<EnchantPropertyValueInfo> ValueRange16
    {
      get
      {
        return this._ValueRange16;
      }
      set
      {
        this._ValueRange16 = value;
      }
    }

    [ProtoMember(51, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight16")]
    public int Weight16
    {
      get
      {
        return this._Weight16;
      }
      set
      {
        this._Weight16 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
