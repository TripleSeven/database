﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuyRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuyRuleType")]
  public enum BuyRuleType
  {
    [ProtoEnum(Name = "BuyRuleType_FixedTime", Value = 1)] BuyRuleType_FixedTime = 1,
    [ProtoEnum(Name = "BuyRuleType_WeekTime", Value = 2)] BuyRuleType_WeekTime = 2,
    [ProtoEnum(Name = "BuyRuleType_MonthTime", Value = 3)] BuyRuleType_MonthTime = 3,
    [ProtoEnum(Name = "BuyRuleType_Forever", Value = 4)] BuyRuleType_Forever = 4,
    [ProtoEnum(Name = "BuyRuleType_Weekend", Value = 5)] BuyRuleType_Weekend = 5,
  }
}
