﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMissionExtRefluxInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMissionExtRefluxInfo")]
  [Serializable]
  public class ConfigDataMissionExtRefluxInfo : IExtensible
  {
    private int _ID;
    private int _RefluxPoints;
    private int _ActivateTime;
    private int _DeactivateTime;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RefluxPoints")]
    public int RefluxPoints
    {
      get
      {
        return this._RefluxPoints;
      }
      set
      {
        this._RefluxPoints = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivateTime")]
    public int ActivateTime
    {
      get
      {
        return this._ActivateTime;
      }
      set
      {
        this._ActivateTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DeactivateTime")]
    public int DeactivateTime
    {
      get
      {
        return this._DeactivateTime;
      }
      set
      {
        this._DeactivateTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
