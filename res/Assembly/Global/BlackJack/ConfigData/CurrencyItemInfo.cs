﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CurrencyItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CurrencyItemInfo")]
  [Serializable]
  public class CurrencyItemInfo : IExtensible
  {
    private int _ItemId;
    private int _Multiplier;
    private int _ExtInfoID;
    private IExtension extensionObject;
    public ConfigDataCollectionActivityCurrencyItemExtInfo m_extInfo;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemId")]
    public int ItemId
    {
      get
      {
        return this._ItemId;
      }
      set
      {
        this._ItemId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Multiplier")]
    public int Multiplier
    {
      get
      {
        return this._Multiplier;
      }
      set
      {
        this._Multiplier = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExtInfoID")]
    public int ExtInfoID
    {
      get
      {
        return this._ExtInfoID;
      }
      set
      {
        this._ExtInfoID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
