﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBanditInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBanditInfo")]
  [Serializable]
  public class ConfigDataBanditInfo : IExtensible
  {
    private int _ID;
    private int _HeroID;
    private int _Gold;
    private int _Rarity;
    private string _IconResource;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroID")]
    public int HeroID
    {
      get
      {
        return this._HeroID;
      }
      set
      {
        this._HeroID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Gold")]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rarity")]
    public int Rarity
    {
      get
      {
        return this._Rarity;
      }
      set
      {
        this._Rarity = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IconResource")]
    public string IconResource
    {
      get
      {
        return this._IconResource;
      }
      set
      {
        this._IconResource = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
