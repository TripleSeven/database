﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FetterCompleteConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FetterCompleteConditionType")]
  public enum FetterCompleteConditionType
  {
    [ProtoEnum(Name = "FetterCompleteConditionType_None", Value = 0)] FetterCompleteConditionType_None,
    [ProtoEnum(Name = "FetterCompleteConditionType_HeroFavorabilityLevel", Value = 1)] FetterCompleteConditionType_HeroFavorabilityLevel,
    [ProtoEnum(Name = "FetterCompleteConditionType_Mission", Value = 2)] FetterCompleteConditionType_Mission,
  }
}
