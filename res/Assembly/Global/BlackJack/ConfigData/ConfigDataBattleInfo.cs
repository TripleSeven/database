﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleInfo")]
  [Serializable]
  public class ConfigDataBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _WinDesc;
    private string _LoseDesc;
    private int _Battlefield_ID;
    private int _CameraX;
    private int _CameraY;
    private string _PrepareMusic;
    private string _BattleMusic;
    private string _EnemyBattleMusic;
    private int _TurnMax;
    private int _AllyNumber;
    private List<ParamPosition> _AllyPositions;
    private List<int> _AllyDirs;
    private List<BattlePosActor> _EnemyActors;
    private List<int> _EnemyDirs;
    private List<int> _EnemyActorsBehavior;
    private List<int> _EnemyGroups;
    private List<int> _EnemyGroupBehavior;
    private List<BattlePosActor> _NpcActors;
    private List<int> _NpcDirs;
    private List<int> _NpcActorsBehavior;
    private List<int> _NpcGroups;
    private List<int> _NpcGroupBehavior;
    private List<int> _WinConditions_ID;
    private List<int> _LoseConditions_ID;
    private List<int> _EventTriggers_ID;
    private List<int> _BattleTreasures_ID;
    private int _BeforePerformBattle_ID;
    private int _AfterPerformBattle_ID;
    private int _BattlePerform_ID;
    private List<int> _BattleRandomEnemies_ID;
    private BattleArmyRandomRuleType _BattleEnemyRandomRule;
    private int _EnemyRandomTalent_ID;
    private List<int> _EnemyRandomTalentProbilities;
    private List<int> _ImperativeBattleHeroes_ID;
    private IExtension extensionObject;
    public ConfigDataBattlefieldInfo m_battlefieldInfo;
    public ConfigDataBattleInfo m_beforePerformBattleInfo;
    public ConfigDataBattleInfo m_afterPerformBattleInfo;
    public ConfigDataBattlePerformInfo m_battlePerformInfo;
    public Dictionary<int, int> m_enemyGroupBehaviors;
    public Dictionary<int, int> m_npcGroupBehaviors;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "WinDesc")]
    public string WinDesc
    {
      get
      {
        return this._WinDesc;
      }
      set
      {
        this._WinDesc = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "LoseDesc")]
    public string LoseDesc
    {
      get
      {
        return this._LoseDesc;
      }
      set
      {
        this._LoseDesc = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battlefield_ID")]
    public int Battlefield_ID
    {
      get
      {
        return this._Battlefield_ID;
      }
      set
      {
        this._Battlefield_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraX")]
    public int CameraX
    {
      get
      {
        return this._CameraX;
      }
      set
      {
        this._CameraX = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraY")]
    public int CameraY
    {
      get
      {
        return this._CameraY;
      }
      set
      {
        this._CameraY = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "PrepareMusic")]
    public string PrepareMusic
    {
      get
      {
        return this._PrepareMusic;
      }
      set
      {
        this._PrepareMusic = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleMusic")]
    public string BattleMusic
    {
      get
      {
        return this._BattleMusic;
      }
      set
      {
        this._BattleMusic = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "EnemyBattleMusic")]
    public string EnemyBattleMusic
    {
      get
      {
        return this._EnemyBattleMusic;
      }
      set
      {
        this._EnemyBattleMusic = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TurnMax")]
    public int TurnMax
    {
      get
      {
        return this._TurnMax;
      }
      set
      {
        this._TurnMax = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AllyNumber")]
    public int AllyNumber
    {
      get
      {
        return this._AllyNumber;
      }
      set
      {
        this._AllyNumber = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, Name = "AllyPositions")]
    public List<ParamPosition> AllyPositions
    {
      get
      {
        return this._AllyPositions;
      }
      set
      {
        this._AllyPositions = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, Name = "AllyDirs")]
    public List<int> AllyDirs
    {
      get
      {
        return this._AllyDirs;
      }
      set
      {
        this._AllyDirs = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, Name = "EnemyActors")]
    public List<BattlePosActor> EnemyActors
    {
      get
      {
        return this._EnemyActors;
      }
      set
      {
        this._EnemyActors = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, Name = "EnemyDirs")]
    public List<int> EnemyDirs
    {
      get
      {
        return this._EnemyDirs;
      }
      set
      {
        this._EnemyDirs = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, Name = "EnemyActorsBehavior")]
    public List<int> EnemyActorsBehavior
    {
      get
      {
        return this._EnemyActorsBehavior;
      }
      set
      {
        this._EnemyActorsBehavior = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, Name = "EnemyGroups")]
    public List<int> EnemyGroups
    {
      get
      {
        return this._EnemyGroups;
      }
      set
      {
        this._EnemyGroups = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, Name = "EnemyGroupBehavior")]
    public List<int> EnemyGroupBehavior
    {
      get
      {
        return this._EnemyGroupBehavior;
      }
      set
      {
        this._EnemyGroupBehavior = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, Name = "NpcActors")]
    public List<BattlePosActor> NpcActors
    {
      get
      {
        return this._NpcActors;
      }
      set
      {
        this._NpcActors = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, Name = "NpcDirs")]
    public List<int> NpcDirs
    {
      get
      {
        return this._NpcDirs;
      }
      set
      {
        this._NpcDirs = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, Name = "NpcActorsBehavior")]
    public List<int> NpcActorsBehavior
    {
      get
      {
        return this._NpcActorsBehavior;
      }
      set
      {
        this._NpcActorsBehavior = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, Name = "NpcGroups")]
    public List<int> NpcGroups
    {
      get
      {
        return this._NpcGroups;
      }
      set
      {
        this._NpcGroups = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, Name = "NpcGroupBehavior")]
    public List<int> NpcGroupBehavior
    {
      get
      {
        return this._NpcGroupBehavior;
      }
      set
      {
        this._NpcGroupBehavior = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, Name = "WinConditions_ID")]
    public List<int> WinConditions_ID
    {
      get
      {
        return this._WinConditions_ID;
      }
      set
      {
        this._WinConditions_ID = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, Name = "LoseConditions_ID")]
    public List<int> LoseConditions_ID
    {
      get
      {
        return this._LoseConditions_ID;
      }
      set
      {
        this._LoseConditions_ID = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, Name = "EventTriggers_ID")]
    public List<int> EventTriggers_ID
    {
      get
      {
        return this._EventTriggers_ID;
      }
      set
      {
        this._EventTriggers_ID = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, Name = "BattleTreasures_ID")]
    public List<int> BattleTreasures_ID
    {
      get
      {
        return this._BattleTreasures_ID;
      }
      set
      {
        this._BattleTreasures_ID = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BeforePerformBattle_ID")]
    public int BeforePerformBattle_ID
    {
      get
      {
        return this._BeforePerformBattle_ID;
      }
      set
      {
        this._BeforePerformBattle_ID = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AfterPerformBattle_ID")]
    public int AfterPerformBattle_ID
    {
      get
      {
        return this._AfterPerformBattle_ID;
      }
      set
      {
        this._AfterPerformBattle_ID = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePerform_ID")]
    public int BattlePerform_ID
    {
      get
      {
        return this._BattlePerform_ID;
      }
      set
      {
        this._BattlePerform_ID = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, Name = "BattleRandomEnemies_ID")]
    public List<int> BattleRandomEnemies_ID
    {
      get
      {
        return this._BattleRandomEnemies_ID;
      }
      set
      {
        this._BattleRandomEnemies_ID = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleEnemyRandomRule")]
    public BattleArmyRandomRuleType BattleEnemyRandomRule
    {
      get
      {
        return this._BattleEnemyRandomRule;
      }
      set
      {
        this._BattleEnemyRandomRule = value;
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnemyRandomTalent_ID")]
    public int EnemyRandomTalent_ID
    {
      get
      {
        return this._EnemyRandomTalent_ID;
      }
      set
      {
        this._EnemyRandomTalent_ID = value;
      }
    }

    [ProtoMember(41, DataFormat = DataFormat.TwosComplement, Name = "EnemyRandomTalentProbilities")]
    public List<int> EnemyRandomTalentProbilities
    {
      get
      {
        return this._EnemyRandomTalentProbilities;
      }
      set
      {
        this._EnemyRandomTalentProbilities = value;
      }
    }

    [ProtoMember(42, DataFormat = DataFormat.TwosComplement, Name = "ImperativeBattleHeroes_ID")]
    public List<int> ImperativeBattleHeroes_ID
    {
      get
      {
        return this._ImperativeBattleHeroes_ID;
      }
      set
      {
        this._ImperativeBattleHeroes_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
