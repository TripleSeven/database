﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRandomStoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRandomStoreInfo")]
  [Serializable]
  public class ConfigDataRandomStoreInfo : IExtensible
  {
    private int _ID;
    private int _ItemCount;
    private FlushRuleType _FlushRuleType;
    private int _Param;
    private GoodsType _CurrencyType;
    private int _Price;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemCount")]
    public int ItemCount
    {
      get
      {
        return this._ItemCount;
      }
      set
      {
        this._ItemCount = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FlushRuleType")]
    public FlushRuleType FlushRuleType
    {
      get
      {
        return this._FlushRuleType;
      }
      set
      {
        this._FlushRuleType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param")]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrencyType")]
    public GoodsType CurrencyType
    {
      get
      {
        return this._CurrencyType;
      }
      set
      {
        this._CurrencyType = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Price")]
    public int Price
    {
      get
      {
        return this._Price;
      }
      set
      {
        this._Price = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
