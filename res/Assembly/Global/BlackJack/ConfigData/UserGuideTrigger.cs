﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.UserGuideTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "UserGuideTrigger")]
  public enum UserGuideTrigger
  {
    [ProtoEnum(Name = "UserGuideTrigger_", Value = 0)] UserGuideTrigger_,
    [ProtoEnum(Name = "UserGuideTrigger_ShowObject", Value = 1)] UserGuideTrigger_ShowObject,
    [ProtoEnum(Name = "UserGuideTrigger_FinishGuideStepN", Value = 2)] UserGuideTrigger_FinishGuideStepN,
    [ProtoEnum(Name = "UserGuideTrigger_HideObject", Value = 3)] UserGuideTrigger_HideObject,
    [ProtoEnum(Name = "UserGuideTrigger_EndUIState", Value = 4)] UserGuideTrigger_EndUIState,
    [ProtoEnum(Name = "UserGuideTrigger_SelectBattleHeroN", Value = 5)] UserGuideTrigger_SelectBattleHeroN,
    [ProtoEnum(Name = "UserGuideTrigger_DeselectBattleHeroN", Value = 6)] UserGuideTrigger_DeselectBattleHeroN,
    [ProtoEnum(Name = "UserGuideTrigger_FinishUserGuideN", Value = 7)] UserGuideTrigger_FinishUserGuideN,
    [ProtoEnum(Name = "UserGuideTrigger_WorldUIGetReady", Value = 8)] UserGuideTrigger_WorldUIGetReady,
    [ProtoEnum(Name = "UserGuideTrigger_BuyGiftStoreGoodsN", Value = 9)] UserGuideTrigger_BuyGiftStoreGoodsN,
  }
}
