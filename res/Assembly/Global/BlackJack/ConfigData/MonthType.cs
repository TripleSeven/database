﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MonthType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MonthType")]
  public enum MonthType
  {
    [ProtoEnum(Name = "MonthType_January", Value = 1)] MonthType_January = 1,
    [ProtoEnum(Name = "MonthType_February", Value = 2)] MonthType_February = 2,
    [ProtoEnum(Name = "MonthType_March", Value = 3)] MonthType_March = 3,
    [ProtoEnum(Name = "MonthType_April", Value = 4)] MonthType_April = 4,
    [ProtoEnum(Name = "MonthType_May", Value = 5)] MonthType_May = 5,
    [ProtoEnum(Name = "MonthType_June", Value = 6)] MonthType_June = 6,
    [ProtoEnum(Name = "MonthType_July", Value = 7)] MonthType_July = 7,
    [ProtoEnum(Name = "MonthType_August", Value = 8)] MonthType_August = 8,
    [ProtoEnum(Name = "MonthType_September", Value = 9)] MonthType_September = 9,
    [ProtoEnum(Name = "MonthType_October", Value = 10)] MonthType_October = 10, // 0x0000000A
    [ProtoEnum(Name = "MonthType_November", Value = 11)] MonthType_November = 11, // 0x0000000B
    [ProtoEnum(Name = "MonthType_December", Value = 12)] MonthType_December = 12, // 0x0000000C
  }
}
