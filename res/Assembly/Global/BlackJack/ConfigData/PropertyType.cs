﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PropertyType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PropertyType")]
  public enum PropertyType
  {
    [ProtoEnum(Name = "PropertyType_None", Value = 0)] PropertyType_None,
    [ProtoEnum(Name = "PropertyType_HP", Value = 1)] PropertyType_HP,
    [ProtoEnum(Name = "PropertyType_AT", Value = 2)] PropertyType_AT,
    [ProtoEnum(Name = "PropertyType_DF", Value = 3)] PropertyType_DF,
    [ProtoEnum(Name = "PropertyType_Magic", Value = 4)] PropertyType_Magic,
    [ProtoEnum(Name = "PropertyType_MagicDF", Value = 5)] PropertyType_MagicDF,
    [ProtoEnum(Name = "PropertyType_DEX", Value = 6)] PropertyType_DEX,
    [ProtoEnum(Name = "PropertyType_BFMovePoint", Value = 7)] PropertyType_BFMovePoint,
    [ProtoEnum(Name = "PropertyType_BFAttackDistance", Value = 8)] PropertyType_BFAttackDistance,
  }
}
