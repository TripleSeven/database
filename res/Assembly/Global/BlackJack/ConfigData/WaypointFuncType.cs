﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointFuncType")]
  public enum WaypointFuncType
  {
    [ProtoEnum(Name = "WaypointFuncType_None", Value = 0)] WaypointFuncType_None,
    [ProtoEnum(Name = "WaypointFuncType_Scenario", Value = 1)] WaypointFuncType_Scenario,
    [ProtoEnum(Name = "WaypointFuncType_Portal", Value = 2)] WaypointFuncType_Portal,
    [ProtoEnum(Name = "WaypointFuncType_Event", Value = 3)] WaypointFuncType_Event,
  }
}
