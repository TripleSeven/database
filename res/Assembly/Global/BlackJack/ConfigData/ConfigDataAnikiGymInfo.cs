﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAnikiGymInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAnikiGymInfo")]
  [Serializable]
  public class ConfigDataAnikiGymInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _TeamName;
    private List<int> _LevelList;
    private List<int> _StartTimeList;
    private string _OpenDesc;
    private string _Icon;
    private IExtension extensionObject;
    public List<ConfigDataAnikiLevelInfo> m_levelInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiGymInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "LevelList")]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
      set
      {
        this._LevelList = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "StartTimeList")]
    public List<int> StartTimeList
    {
      get
      {
        return this._StartTimeList;
      }
      set
      {
        this._StartTimeList = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenDesc")]
    public string OpenDesc
    {
      get
      {
        return this._OpenDesc;
      }
      set
      {
        this._OpenDesc = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
