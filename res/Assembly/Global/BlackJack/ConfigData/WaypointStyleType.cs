﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointStyleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointStyleType")]
  public enum WaypointStyleType
  {
    [ProtoEnum(Name = "WaypointStyleType_None", Value = 0)] WaypointStyleType_None,
    [ProtoEnum(Name = "WaypointStyleType_Forest", Value = 1)] WaypointStyleType_Forest,
    [ProtoEnum(Name = "WaypointStyleType_Mountain", Value = 2)] WaypointStyleType_Mountain,
    [ProtoEnum(Name = "WaypointStyleType_Cave", Value = 3)] WaypointStyleType_Cave,
    [ProtoEnum(Name = "WaypointStyleType_Village", Value = 4)] WaypointStyleType_Village,
    [ProtoEnum(Name = "WaypointStyleType_ActivityEvent", Value = 5)] WaypointStyleType_ActivityEvent,
  }
}
