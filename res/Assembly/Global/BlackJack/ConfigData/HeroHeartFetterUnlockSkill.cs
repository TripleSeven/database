﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroHeartFetterUnlockSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroHeartFetterUnlockSkill")]
  [Serializable]
  public class HeroHeartFetterUnlockSkill : IExtensible
  {
    private int _SkillId;
    private int _HeartFetterLevel;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillId")]
    public int SkillId
    {
      get
      {
        return this._SkillId;
      }
      set
      {
        this._SkillId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeartFetterLevel")]
    public int HeartFetterLevel
    {
      get
      {
        return this._HeartFetterLevel;
      }
      set
      {
        this._HeartFetterLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
