﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.VersionInfoId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "VersionInfoId")]
  public enum VersionInfoId
  {
    [ProtoEnum(Name = "VersionInfoId_ClientProgram", Value = 1)] VersionInfoId_ClientProgram = 1,
    [ProtoEnum(Name = "VersionInfoId_AssetBundle", Value = 2)] VersionInfoId_AssetBundle = 2,
    [ProtoEnum(Name = "VersionInfoId_BattleReport", Value = 3)] VersionInfoId_BattleReport = 3,
  }
}
