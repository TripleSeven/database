﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataModelSkinResourceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataModelSkinResourceInfo")]
  [Serializable]
  public class ConfigDataModelSkinResourceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Model;
    private string _CombatModel;
    private int _MeleeATK_ID;
    private int _RangeATK_ID;
    private int _Skill_ID;
    private IExtension extensionObject;
    public ConfigDataSkillInfo m_meleeSkillInfo;
    public ConfigDataSkillInfo m_rangeSkillInfo;
    public ConfigDataSkillInfo m_skinSkillInfo;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "CombatModel")]
    public string CombatModel
    {
      get
      {
        return this._CombatModel;
      }
      set
      {
        this._CombatModel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MeleeATK_ID")]
    public int MeleeATK_ID
    {
      get
      {
        return this._MeleeATK_ID;
      }
      set
      {
        this._MeleeATK_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RangeATK_ID")]
    public int RangeATK_ID
    {
      get
      {
        return this._RangeATK_ID;
      }
      set
      {
        this._RangeATK_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill_ID")]
    public int Skill_ID
    {
      get
      {
        return this._Skill_ID;
      }
      set
      {
        this._Skill_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
