﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractionWeightResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractionWeightResult")]
  [Serializable]
  public class HeroInteractionWeightResult : IExtensible
  {
    private HeroInteractionResultType _ResultType;
    private int _Weight;
    private int _FavourabilityExp;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ResultType")]
    public HeroInteractionResultType ResultType
    {
      get
      {
        return this._ResultType;
      }
      set
      {
        this._ResultType = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight")]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavourabilityExp")]
    public int FavourabilityExp
    {
      get
      {
        return this._FavourabilityExp;
      }
      set
      {
        this._FavourabilityExp = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
