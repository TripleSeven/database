﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataWorldMapInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataWorldMapInfo")]
  [Serializable]
  public class ConfigDataWorldMapInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private List<int> _Regions_ID;
    private int _StartRegion_ID;
    private int _StartWaypoint_ID;
    private string _WorldMap;
    private int _BackgroundWidth;
    private int _BackgroundHeight;
    private IExtension extensionObject;
    public ConfigDataRegionInfo[] m_regionInfos;
    public ConfigDataRegionInfo m_startRegionInfo;
    public ConfigDataWaypointInfo m_startWaypointInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWorldMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "Regions_ID")]
    public List<int> Regions_ID
    {
      get
      {
        return this._Regions_ID;
      }
      set
      {
        this._Regions_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartRegion_ID")]
    public int StartRegion_ID
    {
      get
      {
        return this._StartRegion_ID;
      }
      set
      {
        this._StartRegion_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartWaypoint_ID")]
    public int StartWaypoint_ID
    {
      get
      {
        return this._StartWaypoint_ID;
      }
      set
      {
        this._StartWaypoint_ID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "WorldMap")]
    public string WorldMap
    {
      get
      {
        return this._WorldMap;
      }
      set
      {
        this._WorldMap = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundWidth")]
    public int BackgroundWidth
    {
      get
      {
        return this._BackgroundWidth;
      }
      set
      {
        this._BackgroundWidth = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundHeight")]
    public int BackgroundHeight
    {
      get
      {
        return this._BackgroundHeight;
      }
      set
      {
        this._BackgroundHeight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
