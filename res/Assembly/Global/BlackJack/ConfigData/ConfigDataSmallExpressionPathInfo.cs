﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSmallExpressionPathInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSmallExpressionPathInfo")]
  [Serializable]
  public class ConfigDataSmallExpressionPathInfo : IExtensible
  {
    private int _ID;
    private string _SmallExpressionIconPath;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "SmallExpressionIconPath")]
    public string SmallExpressionIconPath
    {
      get
      {
        return this._SmallExpressionIconPath;
      }
      set
      {
        this._SmallExpressionIconPath = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
