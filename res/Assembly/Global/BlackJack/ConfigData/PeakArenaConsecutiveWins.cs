﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakArenaConsecutiveWins
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PeakArenaConsecutiveWins")]
  [Serializable]
  public class PeakArenaConsecutiveWins : IExtensible
  {
    private int _ConsecutiveWins;
    private int _ScoreBonus;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsecutiveWins")]
    public int ConsecutiveWins
    {
      get
      {
        return this._ConsecutiveWins;
      }
      set
      {
        this._ConsecutiveWins = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreBonus")]
    public int ScoreBonus
    {
      get
      {
        return this._ScoreBonus;
      }
      set
      {
        this._ScoreBonus = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
