﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataFixedStoreItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataFixedStoreItemInfo")]
  [Serializable]
  public class ConfigDataFixedStoreItemInfo : IExtensible
  {
    private int _ID;
    private StoreId _StoreID;
    private string _Name;
    private GoodsType _ItemType;
    private int _ItemId;
    private int _Nums;
    private string _ShowStartTime;
    private string _ShowEndTime;
    private BuyRuleType _BuyLimitType;
    private int _Param;
    private int _Count;
    private GoodsType _CurrencyType;
    private List<Goods> _FirstReward;
    private int _FirstPrice;
    private int _NormalPrice;
    private int _DiscountPrice;
    private string _DiscountStartTime;
    private string _DiscountEndTime;
    private LabelType _Lable;
    private bool _IsFirstOnSale;
    private int _UISort;
    private int _PlayerBuyMinLevel;
    private bool _SoldOutShow;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFixedStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StoreID")]
    public StoreId StoreID
    {
      get
      {
        return this._StoreID;
      }
      set
      {
        this._StoreID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemType")]
    public GoodsType ItemType
    {
      get
      {
        return this._ItemType;
      }
      set
      {
        this._ItemType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemId")]
    public int ItemId
    {
      get
      {
        return this._ItemId;
      }
      set
      {
        this._ItemId = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Nums")]
    public int Nums
    {
      get
      {
        return this._Nums;
      }
      set
      {
        this._Nums = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "ShowStartTime")]
    public string ShowStartTime
    {
      get
      {
        return this._ShowStartTime;
      }
      set
      {
        this._ShowStartTime = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "ShowEndTime")]
    public string ShowEndTime
    {
      get
      {
        return this._ShowEndTime;
      }
      set
      {
        this._ShowEndTime = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyLimitType")]
    public BuyRuleType BuyLimitType
    {
      get
      {
        return this._BuyLimitType;
      }
      set
      {
        this._BuyLimitType = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param")]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Count")]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrencyType")]
    public GoodsType CurrencyType
    {
      get
      {
        return this._CurrencyType;
      }
      set
      {
        this._CurrencyType = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "FirstReward")]
    public List<Goods> FirstReward
    {
      get
      {
        return this._FirstReward;
      }
      set
      {
        this._FirstReward = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstPrice")]
    public int FirstPrice
    {
      get
      {
        return this._FirstPrice;
      }
      set
      {
        this._FirstPrice = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NormalPrice")]
    public int NormalPrice
    {
      get
      {
        return this._NormalPrice;
      }
      set
      {
        this._NormalPrice = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DiscountPrice")]
    public int DiscountPrice
    {
      get
      {
        return this._DiscountPrice;
      }
      set
      {
        this._DiscountPrice = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "DiscountStartTime")]
    public string DiscountStartTime
    {
      get
      {
        return this._DiscountStartTime;
      }
      set
      {
        this._DiscountStartTime = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, IsRequired = true, Name = "DiscountEndTime")]
    public string DiscountEndTime
    {
      get
      {
        return this._DiscountEndTime;
      }
      set
      {
        this._DiscountEndTime = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Lable")]
    public LabelType Lable
    {
      get
      {
        return this._Lable;
      }
      set
      {
        this._Lable = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsFirstOnSale")]
    public bool IsFirstOnSale
    {
      get
      {
        return this._IsFirstOnSale;
      }
      set
      {
        this._IsFirstOnSale = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UISort")]
    public int UISort
    {
      get
      {
        return this._UISort;
      }
      set
      {
        this._UISort = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerBuyMinLevel")]
    public int PlayerBuyMinLevel
    {
      get
      {
        return this._PlayerBuyMinLevel;
      }
      set
      {
        this._PlayerBuyMinLevel = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, IsRequired = true, Name = "SoldOutShow")]
    public bool SoldOutShow
    {
      get
      {
        return this._SoldOutShow;
      }
      set
      {
        this._SoldOutShow = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
