﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataScenarioInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataScenarioInfo")]
  [Serializable]
  public class ConfigDataScenarioInfo : IExtensible
  {
    private int _ID;
    private string _Chapter;
    private string _Name;
    private int _Waypoint_ID;
    private int _NextScenario_ID;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _DialogBefore_ID;
    private int _DialogAfter_ID;
    private List<ScenarioInfoUnlockCondition> _UnlockCondition;
    private int _PlayerExpReward;
    private int _HeroExpReward;
    private int _GoldReward;
    private int _EnergySuccess;
    private int _EnergyFail;
    private List<Goods> _FirstReward;
    private int _Drop_ID;
    private string _Strategy;
    private bool _IsOpened;
    private IExtension extensionObject;
    public ConfigDataWaypointInfo m_waypointInfo;
    public ConfigDataScenarioInfo m_nextScenarioInfo;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;
    public int m_scenarioDepth;
    public ConfigDataRegionInfo m_regionInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Chapter")]
    public string Chapter
    {
      get
      {
        return this._Chapter;
      }
      set
      {
        this._Chapter = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Waypoint_ID")]
    public int Waypoint_ID
    {
      get
      {
        return this._Waypoint_ID;
      }
      set
      {
        this._Waypoint_ID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextScenario_ID")]
    public int NextScenario_ID
    {
      get
      {
        return this._NextScenario_ID;
      }
      set
      {
        this._NextScenario_ID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogBefore_ID")]
    public int DialogBefore_ID
    {
      get
      {
        return this._DialogBefore_ID;
      }
      set
      {
        this._DialogBefore_ID = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogAfter_ID")]
    public int DialogAfter_ID
    {
      get
      {
        return this._DialogAfter_ID;
      }
      set
      {
        this._DialogAfter_ID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "UnlockCondition")]
    public List<ScenarioInfoUnlockCondition> UnlockCondition
    {
      get
      {
        return this._UnlockCondition;
      }
      set
      {
        this._UnlockCondition = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExpReward")]
    public int PlayerExpReward
    {
      get
      {
        return this._PlayerExpReward;
      }
      set
      {
        this._PlayerExpReward = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExpReward")]
    public int HeroExpReward
    {
      get
      {
        return this._HeroExpReward;
      }
      set
      {
        this._HeroExpReward = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoldReward")]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergySuccess")]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFail")]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, Name = "FirstReward")]
    public List<Goods> FirstReward
    {
      get
      {
        return this._FirstReward;
      }
      set
      {
        this._FirstReward = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Drop_ID")]
    public int Drop_ID
    {
      get
      {
        return this._Drop_ID;
      }
      set
      {
        this._Drop_ID = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strategy")]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsOpened")]
    public bool IsOpened
    {
      get
      {
        return this._IsOpened;
      }
      set
      {
        this._IsOpened = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
