﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityExchangeTableInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityExchangeTableInfo")]
  [Serializable]
  public class ConfigDataCollectionActivityExchangeTableInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _DisplayID;
    private List<LevelInfo> _PrevLevel;
    private List<Goods> _Rewards;
    private List<Goods> _Requirements;
    private int _ExchangeCountMax;
    private int _HoursBeforeActivate;
    private int _CoverHero;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityExchangeTableInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayID")]
    public int DisplayID
    {
      get
      {
        return this._DisplayID;
      }
      set
      {
        this._DisplayID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "PrevLevel")]
    public List<LevelInfo> PrevLevel
    {
      get
      {
        return this._PrevLevel;
      }
      set
      {
        this._PrevLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Rewards")]
    public List<Goods> Rewards
    {
      get
      {
        return this._Rewards;
      }
      set
      {
        this._Rewards = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Requirements")]
    public List<Goods> Requirements
    {
      get
      {
        return this._Requirements;
      }
      set
      {
        this._Requirements = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExchangeCountMax")]
    public int ExchangeCountMax
    {
      get
      {
        return this._ExchangeCountMax;
      }
      set
      {
        this._ExchangeCountMax = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HoursBeforeActivate")]
    public int HoursBeforeActivate
    {
      get
      {
        return this._HoursBeforeActivate;
      }
      set
      {
        this._HoursBeforeActivate = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CoverHero")]
    public int CoverHero
    {
      get
      {
        return this._CoverHero;
      }
      set
      {
        this._CoverHero = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
