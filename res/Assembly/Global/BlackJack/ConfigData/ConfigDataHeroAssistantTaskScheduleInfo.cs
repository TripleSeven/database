﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskScheduleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskScheduleInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskScheduleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private List<int> _TaskIDs;
    private int _Weekday;
    private List<int> _RecommendHeroes;
    private List<int> _SoldierTypeList;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskScheduleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Resource")]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "TaskIDs")]
    public List<int> TaskIDs
    {
      get
      {
        return this._TaskIDs;
      }
      set
      {
        this._TaskIDs = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weekday")]
    public int Weekday
    {
      get
      {
        return this._Weekday;
      }
      set
      {
        this._Weekday = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "RecommendHeroes")]
    public List<int> RecommendHeroes
    {
      get
      {
        return this._RecommendHeroes;
      }
      set
      {
        this._RecommendHeroes = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "SoldierTypeList")]
    public List<int> SoldierTypeList
    {
      get
      {
        return this._SoldierTypeList;
      }
      set
      {
        this._SoldierTypeList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
