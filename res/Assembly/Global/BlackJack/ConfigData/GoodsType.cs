﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GoodsType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GoodsType")]
  public enum GoodsType
  {
    [ProtoEnum(Name = "GoodsType_None", Value = 0)] GoodsType_None,
    [ProtoEnum(Name = "GoodsType_Gold", Value = 1)] GoodsType_Gold,
    [ProtoEnum(Name = "GoodsType_Crystal", Value = 2)] GoodsType_Crystal,
    [ProtoEnum(Name = "GoodsType_Energy", Value = 3)] GoodsType_Energy,
    [ProtoEnum(Name = "GoodsType_Hero", Value = 4)] GoodsType_Hero,
    [ProtoEnum(Name = "GoodsType_JobMaterial", Value = 5)] GoodsType_JobMaterial,
    [ProtoEnum(Name = "GoodsType_Item", Value = 6)] GoodsType_Item,
    [ProtoEnum(Name = "GoodsType_Equipment", Value = 7)] GoodsType_Equipment,
    [ProtoEnum(Name = "GoodsType_ArenaTicket", Value = 8)] GoodsType_ArenaTicket,
    [ProtoEnum(Name = "GoodsType_ArenaHonour", Value = 9)] GoodsType_ArenaHonour,
    [ProtoEnum(Name = "GoodsType_PlayerExp", Value = 10)] GoodsType_PlayerExp,
    [ProtoEnum(Name = "GoodsType_TrainingGroundTechMaterial", Value = 11)] GoodsType_TrainingGroundTechMaterial,
    [ProtoEnum(Name = "GoodsType_FriendshipPoints", Value = 12)] GoodsType_FriendshipPoints,
    [ProtoEnum(Name = "GoodsType_EnchantStone", Value = 13)] GoodsType_EnchantStone,
    [ProtoEnum(Name = "GoodsType_MonthCard", Value = 14)] GoodsType_MonthCard,
    [ProtoEnum(Name = "GoodsType_HeadFrame", Value = 15)] GoodsType_HeadFrame,
    [ProtoEnum(Name = "GoodsType_HeroSkin", Value = 16)] GoodsType_HeroSkin,
    [ProtoEnum(Name = "GoodsType_SoldierSkin", Value = 17)] GoodsType_SoldierSkin,
    [ProtoEnum(Name = "GoodsType_SkinTicket", Value = 18)] GoodsType_SkinTicket,
    [ProtoEnum(Name = "GoodsType_RealTimePVPHonor", Value = 19)] GoodsType_RealTimePVPHonor,
    [ProtoEnum(Name = "GoodsType_MemoryEssence", Value = 20)] GoodsType_MemoryEssence,
    [ProtoEnum(Name = "GoodsType_MithralStone", Value = 21)] GoodsType_MithralStone,
    [ProtoEnum(Name = "GoodsType_BrillianceMithralStone", Value = 22)] GoodsType_BrillianceMithralStone,
    [ProtoEnum(Name = "GoodsType_GuildMedal", Value = 23)] GoodsType_GuildMedal,
    [ProtoEnum(Name = "GoodsType_ChallengePoint", Value = 24)] GoodsType_ChallengePoint,
    [ProtoEnum(Name = "GoodsType_FashionPoint", Value = 25)] GoodsType_FashionPoint,
    [ProtoEnum(Name = "GoodsType_Title", Value = 26)] GoodsType_Title,
    [ProtoEnum(Name = "GoodsType_RefineryStone", Value = 27)] GoodsType_RefineryStone,
  }
}
