﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RafflePoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RafflePoolType")]
  public enum RafflePoolType
  {
    [ProtoEnum(Name = "RafflePoolType_None", Value = 0)] RafflePoolType_None,
    [ProtoEnum(Name = "RafflePoolType_Raffle", Value = 1)] RafflePoolType_Raffle,
    [ProtoEnum(Name = "RafflePoolType_Tarot", Value = 2)] RafflePoolType_Tarot,
  }
}
