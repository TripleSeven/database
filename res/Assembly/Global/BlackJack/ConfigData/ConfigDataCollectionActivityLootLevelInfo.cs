﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionActivityLootLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionActivityLootLevelInfo")]
  [Serializable]
  public class ConfigDataCollectionActivityLootLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TeamName;
    private List<LevelInfo> _PrevLevel;
    private int _DaysBeforeActivate;
    private int _PlayerLevelRequired;
    private int _MonsterLevel;
    private int _BattleID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _GoldReward;
    private int _EnergyCostSuccess;
    private int _EnergyCostFail;
    private int _EnergyCostTeam;
    private List<Goods> _FirstClearDrop;
    private int _RandomDrop;
    private int _TeamDrop;
    private int _DropDisplayCount;
    private int _Score;
    private string _Strategy;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataCollectionActivityWaypointInfo WaypointInfo;
    public ConfigDataCollectionActivityInfo CollectionActivity;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityLootLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "PrevLevel")]
    public List<LevelInfo> PrevLevel
    {
      get
      {
        return this._PrevLevel;
      }
      set
      {
        this._PrevLevel = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DaysBeforeActivate")]
    public int DaysBeforeActivate
    {
      get
      {
        return this._DaysBeforeActivate;
      }
      set
      {
        this._DaysBeforeActivate = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerLevelRequired")]
    public int PlayerLevelRequired
    {
      get
      {
        return this._PlayerLevelRequired;
      }
      set
      {
        this._PlayerLevelRequired = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleID")]
    public int BattleID
    {
      get
      {
        return this._BattleID;
      }
      set
      {
        this._BattleID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExp")]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExp")]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoldReward")]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyCostSuccess")]
    public int EnergyCostSuccess
    {
      get
      {
        return this._EnergyCostSuccess;
      }
      set
      {
        this._EnergyCostSuccess = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyCostFail")]
    public int EnergyCostFail
    {
      get
      {
        return this._EnergyCostFail;
      }
      set
      {
        this._EnergyCostFail = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyCostTeam")]
    public int EnergyCostTeam
    {
      get
      {
        return this._EnergyCostTeam;
      }
      set
      {
        this._EnergyCostTeam = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "FirstClearDrop")]
    public List<Goods> FirstClearDrop
    {
      get
      {
        return this._FirstClearDrop;
      }
      set
      {
        this._FirstClearDrop = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomDrop")]
    public int RandomDrop
    {
      get
      {
        return this._RandomDrop;
      }
      set
      {
        this._RandomDrop = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TeamDrop")]
    public int TeamDrop
    {
      get
      {
        return this._TeamDrop;
      }
      set
      {
        this._TeamDrop = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropDisplayCount")]
    public int DropDisplayCount
    {
      get
      {
        return this._DropDisplayCount;
      }
      set
      {
        this._DropDisplayCount = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Score")]
    public int Score
    {
      get
      {
        return this._Score;
      }
      set
      {
        this._Score = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strategy")]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
