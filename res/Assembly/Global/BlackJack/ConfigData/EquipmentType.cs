﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EquipmentType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EquipmentType")]
  public enum EquipmentType
  {
    [ProtoEnum(Name = "EquipmentType_Weapon", Value = 0)] EquipmentType_Weapon,
    [ProtoEnum(Name = "EquipmentType_Armor", Value = 1)] EquipmentType_Armor,
    [ProtoEnum(Name = "EquipmentType_Helmet", Value = 2)] EquipmentType_Helmet,
    [ProtoEnum(Name = "EquipmentType_Ornament", Value = 3)] EquipmentType_Ornament,
    [ProtoEnum(Name = "EquipmentType_Enhancement", Value = 4)] EquipmentType_Enhancement,
    [ProtoEnum(Name = "EquipmentType_LevelUpStar", Value = 5)] EquipmentType_LevelUpStar,
  }
}
