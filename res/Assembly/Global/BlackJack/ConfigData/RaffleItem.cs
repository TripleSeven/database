﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RaffleItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RaffleItem")]
  [Serializable]
  public class RaffleItem : IExtensible
  {
    private int _RaffleID;
    private int _RaffleLevel;
    private GoodsType _GoodsType;
    private int _ItemID;
    private int _ItemCount;
    private int _Weight;
    private int _UpperBound;
    private int _LowerBound;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RaffleID")]
    public int RaffleID
    {
      get
      {
        return this._RaffleID;
      }
      set
      {
        this._RaffleID = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RaffleLevel")]
    public int RaffleLevel
    {
      get
      {
        return this._RaffleLevel;
      }
      set
      {
        this._RaffleLevel = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoodsType")]
    public GoodsType GoodsType
    {
      get
      {
        return this._GoodsType;
      }
      set
      {
        this._GoodsType = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemID")]
    public int ItemID
    {
      get
      {
        return this._ItemID;
      }
      set
      {
        this._ItemID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemCount")]
    public int ItemCount
    {
      get
      {
        return this._ItemCount;
      }
      set
      {
        this._ItemCount = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight")]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UpperBound")]
    public int UpperBound
    {
      get
      {
        return this._UpperBound;
      }
      set
      {
        this._UpperBound = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LowerBound")]
    public int LowerBound
    {
      get
      {
        return this._LowerBound;
      }
      set
      {
        this._LowerBound = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
