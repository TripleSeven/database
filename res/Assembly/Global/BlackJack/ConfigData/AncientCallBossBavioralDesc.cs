﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.AncientCallBossBavioralDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using SLua;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "AncientCallBossBavioralDesc")]
  [HotFix(true, m_isCtorOnly = true)]
  [Serializable]
  public class AncientCallBossBavioralDesc : IExtensible
  {
    private int _Step;
    private int _DescId;
    private int _Skill1;
    private int _Skill2;
    private int _Skill3;
    private int _Skill4;
    private int _Skill5;
    private IExtension extensionObject;
    private BJLuaObjHelper m_luaObjHelper;
    private ObjectLuaHotFixState m_hotfixState;
    private LuaFunction m_ctor_hotfix;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossBavioralDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Step")]
    public int Step
    {
      get
      {
        return this._Step;
      }
      set
      {
        this._Step = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DescId")]
    public int DescId
    {
      get
      {
        return this._DescId;
      }
      set
      {
        this._DescId = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill1")]
    public int Skill1
    {
      get
      {
        return this._Skill1;
      }
      set
      {
        this._Skill1 = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill2")]
    public int Skill2
    {
      get
      {
        return this._Skill2;
      }
      set
      {
        this._Skill2 = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill3")]
    public int Skill3
    {
      get
      {
        return this._Skill3;
      }
      set
      {
        this._Skill3 = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill4")]
    public int Skill4
    {
      get
      {
        return this._Skill4;
      }
      set
      {
        this._Skill4 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Skill5")]
    public int Skill5
    {
      get
      {
        return this._Skill5;
      }
      set
      {
        this._Skill5 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool InitHotFix([In] LuaTable obj0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLuaObjHelperDisposed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TryInitHotFix(string luaModuleName = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
