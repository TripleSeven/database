﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGuildMassiveCombatDifficultyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGuildMassiveCombatDifficultyInfo")]
  [Serializable]
  public class ConfigDataGuildMassiveCombatDifficultyInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _SuggestedLevel;
    private List<LevelAndMembers> _Requirements;
    private List<int> _StrongholdList;
    private List<Rewards> _RewardsInfo;
    private int _BonusGuildCoins;
    private int _IndividualPointsRewardsGroupID;
    private int _StrongholdRewardMailTemplateId;
    private int _IndividualRewardMailTemplateId;
    private IExtension extensionObject;
    public List<Rewards> SortedRewardsInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatDifficultyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "SuggestedLevel")]
    public string SuggestedLevel
    {
      get
      {
        return this._SuggestedLevel;
      }
      set
      {
        this._SuggestedLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Requirements")]
    public List<LevelAndMembers> Requirements
    {
      get
      {
        return this._Requirements;
      }
      set
      {
        this._Requirements = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "StrongholdList")]
    public List<int> StrongholdList
    {
      get
      {
        return this._StrongholdList;
      }
      set
      {
        this._StrongholdList = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "RewardsInfo")]
    public List<Rewards> RewardsInfo
    {
      get
      {
        return this._RewardsInfo;
      }
      set
      {
        this._RewardsInfo = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BonusGuildCoins")]
    public int BonusGuildCoins
    {
      get
      {
        return this._BonusGuildCoins;
      }
      set
      {
        this._BonusGuildCoins = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "IndividualPointsRewardsGroupID")]
    public int IndividualPointsRewardsGroupID
    {
      get
      {
        return this._IndividualPointsRewardsGroupID;
      }
      set
      {
        this._IndividualPointsRewardsGroupID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StrongholdRewardMailTemplateId")]
    public int StrongholdRewardMailTemplateId
    {
      get
      {
        return this._StrongholdRewardMailTemplateId;
      }
      set
      {
        this._StrongholdRewardMailTemplateId = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "IndividualRewardMailTemplateId")]
    public int IndividualRewardMailTemplateId
    {
      get
      {
        return this._IndividualRewardMailTemplateId;
      }
      set
      {
        this._IndividualRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
