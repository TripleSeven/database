﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleWinConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleWinConditionType")]
  public enum BattleWinConditionType
  {
    [ProtoEnum(Name = "BattleWinConditionType_None", Value = 0)] BattleWinConditionType_None,
    [ProtoEnum(Name = "BattleWinConditionType_KillAll", Value = 1)] BattleWinConditionType_KillAll,
    [ProtoEnum(Name = "BattleWinConditionType_KillActor", Value = 2)] BattleWinConditionType_KillActor,
    [ProtoEnum(Name = "BattleWinConditionType_ActorReachPosition", Value = 3)] BattleWinConditionType_ActorReachPosition,
    [ProtoEnum(Name = "BattleWinConditionType_ActorNotDead", Value = 4)] BattleWinConditionType_ActorNotDead,
    [ProtoEnum(Name = "BattleWinConditionType_Turn", Value = 5)] BattleWinConditionType_Turn,
    [ProtoEnum(Name = "BattleWinConditionType_KillCount", Value = 6)] BattleWinConditionType_KillCount,
    [ProtoEnum(Name = "BattleWinConditionType_KillOthers", Value = 7)] BattleWinConditionType_KillOthers,
    [ProtoEnum(Name = "BattleWinConditionType_ActorHpLess", Value = 8)] BattleWinConditionType_ActorHpLess,
    [ProtoEnum(Name = "BattleWinConditionType_EventTrigger", Value = 9)] BattleWinConditionType_EventTrigger,
  }
}
