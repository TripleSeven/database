﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionEventAppearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionEventAppearConditionType")]
  public enum CollectionEventAppearConditionType
  {
    [ProtoEnum(Name = "CollectionEventAppearConditionType_None", Value = 0)] CollectionEventAppearConditionType_None,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteScenario", Value = 1)] CollectionEventAppearConditionType_CompleteScenario,
    [ProtoEnum(Name = "CollectionEventAppearConditionType_CompleteChallengeLevel", Value = 2)] CollectionEventAppearConditionType_CompleteChallengeLevel,
  }
}
