﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroDungeonLevellUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroDungeonLevellUnlockConditionType")]
  public enum HeroDungeonLevellUnlockConditionType
  {
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_None", Value = 0)] HeroDungeonLevellUnlockConditionType_None,
    [ProtoEnum(Name = "HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel", Value = 1)] HeroDungeonLevellUnlockConditionType_HeroFavorabilityLevel,
  }
}
