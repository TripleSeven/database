﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingTechInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingTechInfo")]
  [Serializable]
  public class ConfigDataTrainingTechInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Resource;
    private List<int> _PreTechIDs;
    private List<int> _PreTechLevel;
    private int _RoomLevelRequired;
    private List<int> _SoldierIDRelated;
    private List<int> _ArmyIDRelated;
    private bool _IsSummon;
    private TechDisplayType _TechType;
    private List<int> _TechLevelupInfoList;
    private IExtension extensionObject;
    public List<ConfigDataTrainingTechLevelInfo> m_techLevelupInfo;
    public List<TrainingTechInfo> m_Infos;
    public int m_courseId;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Resource")]
    public string Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "PreTechIDs")]
    public List<int> PreTechIDs
    {
      get
      {
        return this._PreTechIDs;
      }
      set
      {
        this._PreTechIDs = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "PreTechLevel")]
    public List<int> PreTechLevel
    {
      get
      {
        return this._PreTechLevel;
      }
      set
      {
        this._PreTechLevel = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomLevelRequired")]
    public int RoomLevelRequired
    {
      get
      {
        return this._RoomLevelRequired;
      }
      set
      {
        this._RoomLevelRequired = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "SoldierIDRelated")]
    public List<int> SoldierIDRelated
    {
      get
      {
        return this._SoldierIDRelated;
      }
      set
      {
        this._SoldierIDRelated = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "ArmyIDRelated")]
    public List<int> ArmyIDRelated
    {
      get
      {
        return this._ArmyIDRelated;
      }
      set
      {
        this._ArmyIDRelated = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsSummon")]
    public bool IsSummon
    {
      get
      {
        return this._IsSummon;
      }
      set
      {
        this._IsSummon = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TechType")]
    public TechDisplayType TechType
    {
      get
      {
        return this._TechType;
      }
      set
      {
        this._TechType = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "TechLevelupInfoList")]
    public List<int> TechLevelupInfoList
    {
      get
      {
        return this._TechLevelupInfoList;
      }
      set
      {
        this._TechLevelupInfoList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
