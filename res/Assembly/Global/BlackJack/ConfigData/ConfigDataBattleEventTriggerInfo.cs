﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleEventTriggerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleEventTriggerInfo")]
  [Serializable]
  public class ConfigDataBattleEventTriggerInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private BattleEventTriggerType _TriggerType;
    private List<int> _Param1;
    private List<int> _Param2;
    private List<ParamPosition> _Param3;
    private List<int> _Actions_ID;
    private int _TriggerCountMax;
    private int _TriggerTurnCountMax;
    private IExtension extensionObject;
    public ConfigDataBattleEventActionInfo[] m_battleEventActionInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventTriggerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TriggerType")]
    public BattleEventTriggerType TriggerType
    {
      get
      {
        return this._TriggerType;
      }
      set
      {
        this._TriggerType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "Param1")]
    public List<int> Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "Param2")]
    public List<int> Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Param3")]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Actions_ID")]
    public List<int> Actions_ID
    {
      get
      {
        return this._Actions_ID;
      }
      set
      {
        this._Actions_ID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TriggerCountMax")]
    public int TriggerCountMax
    {
      get
      {
        return this._TriggerCountMax;
      }
      set
      {
        this._TriggerCountMax = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TriggerTurnCountMax")]
    public int TriggerTurnCountMax
    {
      get
      {
        return this._TriggerTurnCountMax;
      }
      set
      {
        this._TriggerTurnCountMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param3FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
