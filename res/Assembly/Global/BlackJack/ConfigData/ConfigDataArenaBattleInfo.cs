﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArenaBattleInfo")]
  [Serializable]
  public class ConfigDataArenaBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Battlefield_ID;
    private int _CameraX;
    private int _CameraY;
    private int _DefendCameraX;
    private int _DefendCameraY;
    private string _PrepareMusic;
    private string _BattleMusic;
    private string _DefendBattleMusic;
    private int _AttackNumber;
    private List<ParamPosition> _AttackPositions;
    private List<int> _AttackDirs;
    private int _DefendNumber;
    private List<ParamPosition> _DefendPositions;
    private List<int> _DefendDirs;
    private List<int> _EventTriggers_ID;
    private string _Image;
    private IExtension extensionObject;
    public ConfigDataBattlefieldInfo m_battlefieldInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battlefield_ID")]
    public int Battlefield_ID
    {
      get
      {
        return this._Battlefield_ID;
      }
      set
      {
        this._Battlefield_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraX")]
    public int CameraX
    {
      get
      {
        return this._CameraX;
      }
      set
      {
        this._CameraX = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraY")]
    public int CameraY
    {
      get
      {
        return this._CameraY;
      }
      set
      {
        this._CameraY = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefendCameraX")]
    public int DefendCameraX
    {
      get
      {
        return this._DefendCameraX;
      }
      set
      {
        this._DefendCameraX = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefendCameraY")]
    public int DefendCameraY
    {
      get
      {
        return this._DefendCameraY;
      }
      set
      {
        this._DefendCameraY = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "PrepareMusic")]
    public string PrepareMusic
    {
      get
      {
        return this._PrepareMusic;
      }
      set
      {
        this._PrepareMusic = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleMusic")]
    public string BattleMusic
    {
      get
      {
        return this._BattleMusic;
      }
      set
      {
        this._BattleMusic = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefendBattleMusic")]
    public string DefendBattleMusic
    {
      get
      {
        return this._DefendBattleMusic;
      }
      set
      {
        this._DefendBattleMusic = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AttackNumber")]
    public int AttackNumber
    {
      get
      {
        return this._AttackNumber;
      }
      set
      {
        this._AttackNumber = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, Name = "AttackPositions")]
    public List<ParamPosition> AttackPositions
    {
      get
      {
        return this._AttackPositions;
      }
      set
      {
        this._AttackPositions = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, Name = "AttackDirs")]
    public List<int> AttackDirs
    {
      get
      {
        return this._AttackDirs;
      }
      set
      {
        this._AttackDirs = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefendNumber")]
    public int DefendNumber
    {
      get
      {
        return this._DefendNumber;
      }
      set
      {
        this._DefendNumber = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, Name = "DefendPositions")]
    public List<ParamPosition> DefendPositions
    {
      get
      {
        return this._DefendPositions;
      }
      set
      {
        this._DefendPositions = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, Name = "DefendDirs")]
    public List<int> DefendDirs
    {
      get
      {
        return this._DefendDirs;
      }
      set
      {
        this._DefendDirs = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, Name = "EventTriggers_ID")]
    public List<int> EventTriggers_ID
    {
      get
      {
        return this._EventTriggers_ID;
      }
      set
      {
        this._EventTriggers_ID = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
