﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMonthCardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMonthCardInfo")]
  [Serializable]
  public class ConfigDataMonthCardInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _ValidDays;
    private string _Desc;
    private List<Goods> _Reward;
    private int _OpenEverydayTaskId;
    private int _HeroExpAddRate;
    private int _HeroFavourabilityExpAddRate;
    private List<GameFuncDayBonus> _DayBonusAdd;
    private GameFunctionType _OpenGameFunc;
    private bool _IsAppleSubscribe;
    private int _FightFailEnergyDecreaseRate;
    private string _Icon;
    private List<GameFuncDayTimes> _UnchartedRaidTimesAdd;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ValidDays")]
    public int ValidDays
    {
      get
      {
        return this._ValidDays;
      }
      set
      {
        this._ValidDays = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenEverydayTaskId")]
    public int OpenEverydayTaskId
    {
      get
      {
        return this._OpenEverydayTaskId;
      }
      set
      {
        this._OpenEverydayTaskId = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExpAddRate")]
    public int HeroExpAddRate
    {
      get
      {
        return this._HeroExpAddRate;
      }
      set
      {
        this._HeroExpAddRate = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroFavourabilityExpAddRate")]
    public int HeroFavourabilityExpAddRate
    {
      get
      {
        return this._HeroFavourabilityExpAddRate;
      }
      set
      {
        this._HeroFavourabilityExpAddRate = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "DayBonusAdd")]
    public List<GameFuncDayBonus> DayBonusAdd
    {
      get
      {
        return this._DayBonusAdd;
      }
      set
      {
        this._DayBonusAdd = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenGameFunc")]
    public GameFunctionType OpenGameFunc
    {
      get
      {
        return this._OpenGameFunc;
      }
      set
      {
        this._OpenGameFunc = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAppleSubscribe")]
    public bool IsAppleSubscribe
    {
      get
      {
        return this._IsAppleSubscribe;
      }
      set
      {
        this._IsAppleSubscribe = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FightFailEnergyDecreaseRate")]
    public int FightFailEnergyDecreaseRate
    {
      get
      {
        return this._FightFailEnergyDecreaseRate;
      }
      set
      {
        this._FightFailEnergyDecreaseRate = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "UnchartedRaidTimesAdd")]
    public List<GameFuncDayTimes> UnchartedRaidTimesAdd
    {
      get
      {
        return this._UnchartedRaidTimesAdd;
      }
      set
      {
        this._UnchartedRaidTimesAdd = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
