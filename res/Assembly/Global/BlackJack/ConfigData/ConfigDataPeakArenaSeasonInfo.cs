﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaSeasonInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaSeasonInfo")]
  [Serializable]
  public class ConfigDataPeakArenaSeasonInfo : IExtensible
  {
    private int _ID;
    private string _Desc;
    private int _PointRaceWeeks;
    private string _SeasonStartTime;
    private List<PointRaceRewardInfo> _PointRaceRewards;
    private string _PlayOff256StartTime;
    private string _PlayOff128StartTime;
    private string _PlayOff64StartTime;
    private string _PlayOff32StartTime;
    private string _PlayOff16StartTime;
    private string _PlayOff8StartTime;
    private string _PlayOff4StartTime;
    private string _PlayOff2StartTime;
    private int _PlayOff256BattlesNumber;
    private int _PlayOff128BattlesNumber;
    private int _PlayOff64BattlesNumber;
    private int _PlayOff32BattlesNumber;
    private int _PlayOff16BattlesNumber;
    private int _PlayOff8BattlesNumber;
    private int _PlayOff4BattlesNumber;
    private int _PlayOff2BattlesNumber;
    private IExtension extensionObject;
    public DateTime SeasonStartDateTime;
    public DateTime PlayOff256StartDateTime;
    public DateTime PlayOff128StartDateTime;
    public DateTime PlayOff64StartDateTime;
    public DateTime PlayOff32StartDateTime;
    public DateTime PlayOff16StartDateTime;
    public DateTime PlayOff8StartDateTime;
    public DateTime PlayOff4StartDateTime;
    public DateTime PlayOff2StartDateTime;
    public DateTime SeasonEndDateTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PointRaceWeeks")]
    public int PointRaceWeeks
    {
      get
      {
        return this._PointRaceWeeks;
      }
      set
      {
        this._PointRaceWeeks = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "SeasonStartTime")]
    public string SeasonStartTime
    {
      get
      {
        return this._SeasonStartTime;
      }
      set
      {
        this._SeasonStartTime = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "PointRaceRewards")]
    public List<PointRaceRewardInfo> PointRaceRewards
    {
      get
      {
        return this._PointRaceRewards;
      }
      set
      {
        this._PointRaceRewards = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff256StartTime")]
    public string PlayOff256StartTime
    {
      get
      {
        return this._PlayOff256StartTime;
      }
      set
      {
        this._PlayOff256StartTime = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff128StartTime")]
    public string PlayOff128StartTime
    {
      get
      {
        return this._PlayOff128StartTime;
      }
      set
      {
        this._PlayOff128StartTime = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff64StartTime")]
    public string PlayOff64StartTime
    {
      get
      {
        return this._PlayOff64StartTime;
      }
      set
      {
        this._PlayOff64StartTime = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff32StartTime")]
    public string PlayOff32StartTime
    {
      get
      {
        return this._PlayOff32StartTime;
      }
      set
      {
        this._PlayOff32StartTime = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff16StartTime")]
    public string PlayOff16StartTime
    {
      get
      {
        return this._PlayOff16StartTime;
      }
      set
      {
        this._PlayOff16StartTime = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff8StartTime")]
    public string PlayOff8StartTime
    {
      get
      {
        return this._PlayOff8StartTime;
      }
      set
      {
        this._PlayOff8StartTime = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff4StartTime")]
    public string PlayOff4StartTime
    {
      get
      {
        return this._PlayOff4StartTime;
      }
      set
      {
        this._PlayOff4StartTime = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayOff2StartTime")]
    public string PlayOff2StartTime
    {
      get
      {
        return this._PlayOff2StartTime;
      }
      set
      {
        this._PlayOff2StartTime = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff256BattlesNumber")]
    public int PlayOff256BattlesNumber
    {
      get
      {
        return this._PlayOff256BattlesNumber;
      }
      set
      {
        this._PlayOff256BattlesNumber = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff128BattlesNumber")]
    public int PlayOff128BattlesNumber
    {
      get
      {
        return this._PlayOff128BattlesNumber;
      }
      set
      {
        this._PlayOff128BattlesNumber = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff64BattlesNumber")]
    public int PlayOff64BattlesNumber
    {
      get
      {
        return this._PlayOff64BattlesNumber;
      }
      set
      {
        this._PlayOff64BattlesNumber = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff32BattlesNumber")]
    public int PlayOff32BattlesNumber
    {
      get
      {
        return this._PlayOff32BattlesNumber;
      }
      set
      {
        this._PlayOff32BattlesNumber = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff16BattlesNumber")]
    public int PlayOff16BattlesNumber
    {
      get
      {
        return this._PlayOff16BattlesNumber;
      }
      set
      {
        this._PlayOff16BattlesNumber = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff8BattlesNumber")]
    public int PlayOff8BattlesNumber
    {
      get
      {
        return this._PlayOff8BattlesNumber;
      }
      set
      {
        this._PlayOff8BattlesNumber = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff4BattlesNumber")]
    public int PlayOff4BattlesNumber
    {
      get
      {
        return this._PlayOff4BattlesNumber;
      }
      set
      {
        this._PlayOff4BattlesNumber = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayOff2BattlesNumber")]
    public int PlayOff2BattlesNumber
    {
      get
      {
        return this._PlayOff2BattlesNumber;
      }
      set
      {
        this._PlayOff2BattlesNumber = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
