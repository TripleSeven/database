﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SkillAIType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SkillAIType")]
  public enum SkillAIType
  {
    [ProtoEnum(Name = "SkillAIType_DamageSingle", Value = 1)] SkillAIType_DamageSingle = 1,
    [ProtoEnum(Name = "SkillAIType_DamageAssault", Value = 2)] SkillAIType_DamageAssault = 2,
    [ProtoEnum(Name = "SkillAIType_DamageAOE", Value = 3)] SkillAIType_DamageAOE = 3,
    [ProtoEnum(Name = "SkillAIType_DamageAOESelf", Value = 4)] SkillAIType_DamageAOESelf = 4,
    [ProtoEnum(Name = "SkillAIType_Heal", Value = 5)] SkillAIType_Heal = 5,
    [ProtoEnum(Name = "SkillAIType_HealRange", Value = 6)] SkillAIType_HealRange = 6,
    [ProtoEnum(Name = "SkillAIType_HealRangeSelf", Value = 7)] SkillAIType_HealRangeSelf = 7,
    [ProtoEnum(Name = "SkillAIType_BuffSingle", Value = 8)] SkillAIType_BuffSingle = 8,
    [ProtoEnum(Name = "SkillAIType_BuffAOE", Value = 9)] SkillAIType_BuffAOE = 9,
    [ProtoEnum(Name = "SkillAIType_Dispel", Value = 10)] SkillAIType_Dispel = 10, // 0x0000000A
    [ProtoEnum(Name = "SkillAIType_BuffSelf1", Value = 11)] SkillAIType_BuffSelf1 = 11, // 0x0000000B
    [ProtoEnum(Name = "SkillAIType_BuffSelf2", Value = 12)] SkillAIType_BuffSelf2 = 12, // 0x0000000C
    [ProtoEnum(Name = "SkillAIType_Guard", Value = 13)] SkillAIType_Guard = 13, // 0x0000000D
    [ProtoEnum(Name = "SkillAIType_NewTurn", Value = 14)] SkillAIType_NewTurn = 14, // 0x0000000E
    [ProtoEnum(Name = "SkillAIType_RemoveCD", Value = 15)] SkillAIType_RemoveCD = 15, // 0x0000000F
    [ProtoEnum(Name = "SkillAIType_Summon", Value = 16)] SkillAIType_Summon = 16, // 0x00000010
    [ProtoEnum(Name = "SkillAIType_Teleport", Value = 17)] SkillAIType_Teleport = 17, // 0x00000011
    [ProtoEnum(Name = "SkillAIType_DamageAOESelfSingle", Value = 18)] SkillAIType_DamageAOESelfSingle = 18, // 0x00000012
    [ProtoEnum(Name = "SkillAIType_TargetEnemyWithN", Value = 19)] SkillAIType_TargetEnemyWithN = 19, // 0x00000013
    [ProtoEnum(Name = "SkillAIType_BuffSelfAOE", Value = 20)] SkillAIType_BuffSelfAOE = 20, // 0x00000014
    [ProtoEnum(Name = "SkillAIType_DamageLineAOE", Value = 21)] SkillAIType_DamageLineAOE = 21, // 0x00000015
    [ProtoEnum(Name = "SkillAIType_None", Value = 22)] SkillAIType_None = 22, // 0x00000016
    [ProtoEnum(Name = "SkillAIType_DamageSummon", Value = 23)] SkillAIType_DamageSummon = 23, // 0x00000017
  }
}
