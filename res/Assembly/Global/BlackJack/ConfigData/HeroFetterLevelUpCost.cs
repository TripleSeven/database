﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroFetterLevelUpCost
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroFetterLevelUpCost")]
  [Serializable]
  public class HeroFetterLevelUpCost : IExtensible
  {
    private int _Level;
    private GoodsType _ItemType;
    private int _ItemId;
    private int _Count;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemType")]
    public GoodsType ItemType
    {
      get
      {
        return this._ItemType;
      }
      set
      {
        this._ItemType = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemId")]
    public int ItemId
    {
      get
      {
        return this._ItemId;
      }
      set
      {
        this._ItemId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Count")]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
