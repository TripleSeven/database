﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingRoomLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingRoomLevelInfo")]
  [Serializable]
  public class ConfigDataTrainingRoomLevelInfo : IExtensible
  {
    private int _ID;
    private int _CurrentLevel;
    private int _ExpToNextLevel;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentLevel")]
    public int CurrentLevel
    {
      get
      {
        return this._CurrentLevel;
      }
      set
      {
        this._CurrentLevel = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExpToNextLevel")]
    public int ExpToNextLevel
    {
      get
      {
        return this._ExpToNextLevel;
      }
      set
      {
        this._ExpToNextLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
