﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroPerformanceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroPerformanceInfo")]
  [Serializable]
  public class ConfigDataHeroPerformanceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Word;
    private string _Animation;
    private string _Voice;
    private List<HeroPerformanceUnlcokCondition> _UnlockConditions;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPerformanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Word")]
    public string Word
    {
      get
      {
        return this._Word;
      }
      set
      {
        this._Word = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Animation")]
    public string Animation
    {
      get
      {
        return this._Animation;
      }
      set
      {
        this._Animation = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Voice")]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "UnlockConditions")]
    public List<HeroPerformanceUnlcokCondition> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
      set
      {
        this._UnlockConditions = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
