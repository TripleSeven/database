﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ActivityParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ActivityParam")]
  [Serializable]
  public class ActivityParam : IExtensible
  {
    private int _Parm1;
    private int _Parm2;
    private int _Parm3;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Parm1")]
    public int Parm1
    {
      get
      {
        return this._Parm1;
      }
      set
      {
        this._Parm1 = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Parm2")]
    public int Parm2
    {
      get
      {
        return this._Parm2;
      }
      set
      {
        this._Parm2 = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Parm3")]
    public int Parm3
    {
      get
      {
        return this._Parm3;
      }
      set
      {
        this._Parm3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
