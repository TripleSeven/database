﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSoldierInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSoldierInfo")]
  [Serializable]
  public class ConfigDataSoldierInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _Strong;
    private string _Weak;
    private int _Radius;
    private int _Height;
    private int _FootHeight;
    private List<int> _Skills_ID;
    private List<int> _Old_Buffs_ID;
    private int _Army_ID;
    private bool _IsMelee;
    private MoveType _MoveType;
    private int _BF_AttackDistance;
    private int _MeleeATK_ID;
    private int _RangeATK_ID;
    private int _AttackSPD_INI;
    private int _MoveSPD_INI;
    private int _HP_INI;
    private int _AT_INI;
    private int _DF_INI;
    private int _MagicDF_INI;
    private int _HP_UP;
    private int _AT_UP;
    private int _DF_UP;
    private int _MagicDF_UP;
    private int _CriticalDamage;
    private int _CriticalRate;
    private int _BF_MovePoint;
    private int _BattlePowerHP;
    private int _BattlePowerAT;
    private int _BattlePowerDF;
    private int _BattlePowerMagicDF;
    private int _DieFlyDistanceMin;
    private int _DieFlyDistanceMax;
    private string _Model;
    private string _Model2;
    private string _CombatModel;
    private string _CombatModel2;
    private List<ReplaceAnim> _ReplaceAnims;
    private int _ModelScale;
    private int _BF_ModelScale;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private string _Sound_Enter;
    private string _Sound_Die;
    private int _Rank;
    private bool _IsEnemy;
    private string _GetSoldierDesc;
    private List<int> _GetSoldierHeros_ID;
    private int _GetSoldierTechId;
    private List<int> _SoldierSkins_ID;
    private IExtension extensionObject;
    public ConfigDataArmyInfo m_armyInfo;
    public ConfigDataSkillInfo m_meleeSkillInfo;
    public ConfigDataSkillInfo m_rangeSkillInfo;
    public ConfigDataSkillInfo[] m_skillInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strong")]
    public string Strong
    {
      get
      {
        return this._Strong;
      }
      set
      {
        this._Strong = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "Weak")]
    public string Weak
    {
      get
      {
        return this._Weak;
      }
      set
      {
        this._Weak = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Radius")]
    public int Radius
    {
      get
      {
        return this._Radius;
      }
      set
      {
        this._Radius = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Height")]
    public int Height
    {
      get
      {
        return this._Height;
      }
      set
      {
        this._Height = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FootHeight")]
    public int FootHeight
    {
      get
      {
        return this._FootHeight;
      }
      set
      {
        this._FootHeight = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "Skills_ID")]
    public List<int> Skills_ID
    {
      get
      {
        return this._Skills_ID;
      }
      set
      {
        this._Skills_ID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, Name = "Old_Buffs_ID")]
    public List<int> Old_Buffs_ID
    {
      get
      {
        return this._Old_Buffs_ID;
      }
      set
      {
        this._Old_Buffs_ID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Army_ID")]
    public int Army_ID
    {
      get
      {
        return this._Army_ID;
      }
      set
      {
        this._Army_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsMelee")]
    public bool IsMelee
    {
      get
      {
        return this._IsMelee;
      }
      set
      {
        this._IsMelee = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MoveType")]
    public MoveType MoveType
    {
      get
      {
        return this._MoveType;
      }
      set
      {
        this._MoveType = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_AttackDistance")]
    public int BF_AttackDistance
    {
      get
      {
        return this._BF_AttackDistance;
      }
      set
      {
        this._BF_AttackDistance = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MeleeATK_ID")]
    public int MeleeATK_ID
    {
      get
      {
        return this._MeleeATK_ID;
      }
      set
      {
        this._MeleeATK_ID = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RangeATK_ID")]
    public int RangeATK_ID
    {
      get
      {
        return this._RangeATK_ID;
      }
      set
      {
        this._RangeATK_ID = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AttackSPD_INI")]
    public int AttackSPD_INI
    {
      get
      {
        return this._AttackSPD_INI;
      }
      set
      {
        this._AttackSPD_INI = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MoveSPD_INI")]
    public int MoveSPD_INI
    {
      get
      {
        return this._MoveSPD_INI;
      }
      set
      {
        this._MoveSPD_INI = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HP_INI")]
    public int HP_INI
    {
      get
      {
        return this._HP_INI;
      }
      set
      {
        this._HP_INI = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AT_INI")]
    public int AT_INI
    {
      get
      {
        return this._AT_INI;
      }
      set
      {
        this._AT_INI = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DF_INI")]
    public int DF_INI
    {
      get
      {
        return this._DF_INI;
      }
      set
      {
        this._DF_INI = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDF_INI")]
    public int MagicDF_INI
    {
      get
      {
        return this._MagicDF_INI;
      }
      set
      {
        this._MagicDF_INI = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HP_UP")]
    public int HP_UP
    {
      get
      {
        return this._HP_UP;
      }
      set
      {
        this._HP_UP = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AT_UP")]
    public int AT_UP
    {
      get
      {
        return this._AT_UP;
      }
      set
      {
        this._AT_UP = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DF_UP")]
    public int DF_UP
    {
      get
      {
        return this._DF_UP;
      }
      set
      {
        this._DF_UP = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDF_UP")]
    public int MagicDF_UP
    {
      get
      {
        return this._MagicDF_UP;
      }
      set
      {
        this._MagicDF_UP = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CriticalDamage")]
    public int CriticalDamage
    {
      get
      {
        return this._CriticalDamage;
      }
      set
      {
        this._CriticalDamage = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CriticalRate")]
    public int CriticalRate
    {
      get
      {
        return this._CriticalRate;
      }
      set
      {
        this._CriticalRate = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_MovePoint")]
    public int BF_MovePoint
    {
      get
      {
        return this._BF_MovePoint;
      }
      set
      {
        this._BF_MovePoint = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerHP")]
    public int BattlePowerHP
    {
      get
      {
        return this._BattlePowerHP;
      }
      set
      {
        this._BattlePowerHP = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerAT")]
    public int BattlePowerAT
    {
      get
      {
        return this._BattlePowerAT;
      }
      set
      {
        this._BattlePowerAT = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerDF")]
    public int BattlePowerDF
    {
      get
      {
        return this._BattlePowerDF;
      }
      set
      {
        this._BattlePowerDF = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePowerMagicDF")]
    public int BattlePowerMagicDF
    {
      get
      {
        return this._BattlePowerMagicDF;
      }
      set
      {
        this._BattlePowerMagicDF = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DieFlyDistanceMin")]
    public int DieFlyDistanceMin
    {
      get
      {
        return this._DieFlyDistanceMin;
      }
      set
      {
        this._DieFlyDistanceMin = value;
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DieFlyDistanceMax")]
    public int DieFlyDistanceMax
    {
      get
      {
        return this._DieFlyDistanceMax;
      }
      set
      {
        this._DieFlyDistanceMax = value;
      }
    }

    [ProtoMember(41, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(42, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model2")]
    public string Model2
    {
      get
      {
        return this._Model2;
      }
      set
      {
        this._Model2 = value;
      }
    }

    [ProtoMember(43, DataFormat = DataFormat.Default, IsRequired = true, Name = "CombatModel")]
    public string CombatModel
    {
      get
      {
        return this._CombatModel;
      }
      set
      {
        this._CombatModel = value;
      }
    }

    [ProtoMember(44, DataFormat = DataFormat.Default, IsRequired = true, Name = "CombatModel2")]
    public string CombatModel2
    {
      get
      {
        return this._CombatModel2;
      }
      set
      {
        this._CombatModel2 = value;
      }
    }

    [ProtoMember(45, DataFormat = DataFormat.Default, Name = "ReplaceAnims")]
    public List<ReplaceAnim> ReplaceAnims
    {
      get
      {
        return this._ReplaceAnims;
      }
      set
      {
        this._ReplaceAnims = value;
      }
    }

    [ProtoMember(46, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModelScale")]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    [ProtoMember(47, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_ModelScale")]
    public int BF_ModelScale
    {
      get
      {
        return this._BF_ModelScale;
      }
      set
      {
        this._BF_ModelScale = value;
      }
    }

    [ProtoMember(48, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelScale")]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(49, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelOffsetX")]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(50, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelOffsetY")]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    [ProtoMember(51, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound_Enter")]
    public string Sound_Enter
    {
      get
      {
        return this._Sound_Enter;
      }
      set
      {
        this._Sound_Enter = value;
      }
    }

    [ProtoMember(52, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound_Die")]
    public string Sound_Die
    {
      get
      {
        return this._Sound_Die;
      }
      set
      {
        this._Sound_Die = value;
      }
    }

    [ProtoMember(53, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(54, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsEnemy")]
    public bool IsEnemy
    {
      get
      {
        return this._IsEnemy;
      }
      set
      {
        this._IsEnemy = value;
      }
    }

    [ProtoMember(55, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetSoldierDesc")]
    public string GetSoldierDesc
    {
      get
      {
        return this._GetSoldierDesc;
      }
      set
      {
        this._GetSoldierDesc = value;
      }
    }

    [ProtoMember(56, DataFormat = DataFormat.TwosComplement, Name = "GetSoldierHeros_ID")]
    public List<int> GetSoldierHeros_ID
    {
      get
      {
        return this._GetSoldierHeros_ID;
      }
      set
      {
        this._GetSoldierHeros_ID = value;
      }
    }

    [ProtoMember(57, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GetSoldierTechId")]
    public int GetSoldierTechId
    {
      get
      {
        return this._GetSoldierTechId;
      }
      set
      {
        this._GetSoldierTechId = value;
      }
    }

    [ProtoMember(58, DataFormat = DataFormat.TwosComplement, Name = "SoldierSkins_ID")]
    public List<int> SoldierSkins_ID
    {
      get
      {
        return this._SoldierSkins_ID;
      }
      set
      {
        this._SoldierSkins_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetLevelUpSkillInfo(int skillLevelUp)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
