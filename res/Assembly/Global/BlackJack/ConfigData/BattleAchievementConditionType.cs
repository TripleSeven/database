﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleAchievementConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleAchievementConditionType")]
  public enum BattleAchievementConditionType
  {
    [ProtoEnum(Name = "BattleAchievementConditionType_None", Value = 0)] BattleAchievementConditionType_None,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorNotDead", Value = 1)] BattleAchievementConditionType_ActorNotDead,
    [ProtoEnum(Name = "BattleAchievementConditionType_KillActor", Value = 2)] BattleAchievementConditionType_KillActor,
    [ProtoEnum(Name = "BattleAchievementConditionType_TurnCount", Value = 3)] BattleAchievementConditionType_TurnCount,
    [ProtoEnum(Name = "BattleAchievementConditionType_SkillKillActor", Value = 4)] BattleAchievementConditionType_SkillKillActor,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorCount", Value = 5)] BattleAchievementConditionType_ActorCount,
    [ProtoEnum(Name = "BattleAchievementConditionType_Dialog", Value = 6)] BattleAchievementConditionType_Dialog,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorReachPosition", Value = 7)] BattleAchievementConditionType_ActorReachPosition,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorNotReachPosition", Value = 8)] BattleAchievementConditionType_ActorNotReachPosition,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorNotDamage", Value = 9)] BattleAchievementConditionType_ActorNotDamage,
    [ProtoEnum(Name = "BattleAchievementConditionType_KillCount", Value = 10)] BattleAchievementConditionType_KillCount,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorHpLess", Value = 11)] BattleAchievementConditionType_ActorHpLess,
    [ProtoEnum(Name = "BattleAchievementConditionType_PlayerDieLess", Value = 12)] BattleAchievementConditionType_PlayerDieLess,
    [ProtoEnum(Name = "BattleAchievementConditionType_NpcDieLess", Value = 13)] BattleAchievementConditionType_NpcDieLess,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorOnStage", Value = 14)] BattleAchievementConditionType_ActorOnStage,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorKillActor", Value = 15)] BattleAchievementConditionType_ActorKillActor,
    [ProtoEnum(Name = "BattleAchievementConditionType_BattleEventTriggered", Value = 16)] BattleAchievementConditionType_BattleEventTriggered,
    [ProtoEnum(Name = "BattleAchievementConditionType_ActorKillCount", Value = 17)] BattleAchievementConditionType_ActorKillCount,
    [ProtoEnum(Name = "BattleAchievementConditionType_BossPhase", Value = 18)] BattleAchievementConditionType_BossPhase,
    [ProtoEnum(Name = "BattleAchievementConditionType_HeroTagDamage", Value = 19)] BattleAchievementConditionType_HeroTagDamage,
    [ProtoEnum(Name = "BattleAchievementConditionType_BossPhaseKillCount", Value = 20)] BattleAchievementConditionType_BossPhaseKillCount,
    [ProtoEnum(Name = "BattleAchievementConditionType_BossPhaseDieCount", Value = 21)] BattleAchievementConditionType_BossPhaseDieCount,
    [ProtoEnum(Name = "BattleAchievementConditionType_ContinuousTrigger", Value = 22)] BattleAchievementConditionType_ContinuousTrigger,
  }
}
