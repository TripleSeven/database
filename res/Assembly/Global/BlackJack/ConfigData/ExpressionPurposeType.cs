﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ExpressionPurposeType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ExpressionPurposeType")]
  public enum ExpressionPurposeType
  {
    [ProtoEnum(Name = "ExpressionPurposeType_Chat", Value = 1)] ExpressionPurposeType_Chat = 1,
    [ProtoEnum(Name = "ExpressionPurposeType_Combat", Value = 2)] ExpressionPurposeType_Combat = 2,
  }
}
