﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRealTimePVPDanInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRealTimePVPDanInfo")]
  [Serializable]
  public class ConfigDataRealTimePVPDanInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _BPRule;
    private int _RelegationScore;
    private int _PromotionScore;
    private int _PostSeasonScoreDecrease;
    private List<RealTimePVPConsecutiveWins> _ConsecutiveWinScoreBonus;
    private List<RealTimePVPConsecutiveLosses> _ConsecutiveLossScoreProtection;
    private int _WinBasicScore;
    private int _DanDiffUpperBound;
    private int _DanDiffLowerBound;
    private List<WaitingTimeInfo> _WaitingTimeAdjustment;
    private List<RealTimePVPConsecutiveWinsMatchmakingInfo> _ConsecutiveWinsMatchmakingAdjustment;
    private List<RealTimePVPConsecutiveLossesMatchmakingInfo> _ConsecutiveLossesMatchmakingAdjustment;
    private List<RealTimePVPMatchmakingFailInfo> _MatchmakingFailAdjustment;
    private int _PromotionChallengeCount;
    private int _PromotionChallengeWinsRequired;
    private string _Icon;
    private List<FriendlyWaitingTimeInfo> _FriendlyWaitingTimeAdjustment;
    private int _LossBasicScore;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPDanInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BPRule")]
    public int BPRule
    {
      get
      {
        return this._BPRule;
      }
      set
      {
        this._BPRule = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RelegationScore")]
    public int RelegationScore
    {
      get
      {
        return this._RelegationScore;
      }
      set
      {
        this._RelegationScore = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PromotionScore")]
    public int PromotionScore
    {
      get
      {
        return this._PromotionScore;
      }
      set
      {
        this._PromotionScore = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PostSeasonScoreDecrease")]
    public int PostSeasonScoreDecrease
    {
      get
      {
        return this._PostSeasonScoreDecrease;
      }
      set
      {
        this._PostSeasonScoreDecrease = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "ConsecutiveWinScoreBonus")]
    public List<RealTimePVPConsecutiveWins> ConsecutiveWinScoreBonus
    {
      get
      {
        return this._ConsecutiveWinScoreBonus;
      }
      set
      {
        this._ConsecutiveWinScoreBonus = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "ConsecutiveLossScoreProtection")]
    public List<RealTimePVPConsecutiveLosses> ConsecutiveLossScoreProtection
    {
      get
      {
        return this._ConsecutiveLossScoreProtection;
      }
      set
      {
        this._ConsecutiveLossScoreProtection = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinBasicScore")]
    public int WinBasicScore
    {
      get
      {
        return this._WinBasicScore;
      }
      set
      {
        this._WinBasicScore = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiffUpperBound")]
    public int DanDiffUpperBound
    {
      get
      {
        return this._DanDiffUpperBound;
      }
      set
      {
        this._DanDiffUpperBound = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiffLowerBound")]
    public int DanDiffLowerBound
    {
      get
      {
        return this._DanDiffLowerBound;
      }
      set
      {
        this._DanDiffLowerBound = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "WaitingTimeAdjustment")]
    public List<WaitingTimeInfo> WaitingTimeAdjustment
    {
      get
      {
        return this._WaitingTimeAdjustment;
      }
      set
      {
        this._WaitingTimeAdjustment = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, Name = "ConsecutiveWinsMatchmakingAdjustment")]
    public List<RealTimePVPConsecutiveWinsMatchmakingInfo> ConsecutiveWinsMatchmakingAdjustment
    {
      get
      {
        return this._ConsecutiveWinsMatchmakingAdjustment;
      }
      set
      {
        this._ConsecutiveWinsMatchmakingAdjustment = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, Name = "ConsecutiveLossesMatchmakingAdjustment")]
    public List<RealTimePVPConsecutiveLossesMatchmakingInfo> ConsecutiveLossesMatchmakingAdjustment
    {
      get
      {
        return this._ConsecutiveLossesMatchmakingAdjustment;
      }
      set
      {
        this._ConsecutiveLossesMatchmakingAdjustment = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "MatchmakingFailAdjustment")]
    public List<RealTimePVPMatchmakingFailInfo> MatchmakingFailAdjustment
    {
      get
      {
        return this._MatchmakingFailAdjustment;
      }
      set
      {
        this._MatchmakingFailAdjustment = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PromotionChallengeCount")]
    public int PromotionChallengeCount
    {
      get
      {
        return this._PromotionChallengeCount;
      }
      set
      {
        this._PromotionChallengeCount = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PromotionChallengeWinsRequired")]
    public int PromotionChallengeWinsRequired
    {
      get
      {
        return this._PromotionChallengeWinsRequired;
      }
      set
      {
        this._PromotionChallengeWinsRequired = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, Name = "FriendlyWaitingTimeAdjustment")]
    public List<FriendlyWaitingTimeInfo> FriendlyWaitingTimeAdjustment
    {
      get
      {
        return this._FriendlyWaitingTimeAdjustment;
      }
      set
      {
        this._FriendlyWaitingTimeAdjustment = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LossBasicScore")]
    public int LossBasicScore
    {
      get
      {
        return this._LossBasicScore;
      }
      set
      {
        this._LossBasicScore = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
