﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCollectionEventInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCollectionEventInfo")]
  [Serializable]
  public class ConfigDataCollectionEventInfo : IExtensible
  {
    private int _ID;
    private int _EventID;
    private List<CollectionEventInfoAppearCondition> _AppearCondition;
    private List<CollectionEventInfoDisappearCondition> _DisappearCondition;
    private IExtension extensionObject;
    public ConfigDataEventInfo EventInfo;
    public ConfigDataCollectionActivityWaypointInfo WaypointInfo;
    public ConfigDataCollectionActivityInfo CollectionActivity;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EventID")]
    public int EventID
    {
      get
      {
        return this._EventID;
      }
      set
      {
        this._EventID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "AppearCondition")]
    public List<CollectionEventInfoAppearCondition> AppearCondition
    {
      get
      {
        return this._AppearCondition;
      }
      set
      {
        this._AppearCondition = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "DisappearCondition")]
    public List<CollectionEventInfoDisappearCondition> DisappearCondition
    {
      get
      {
        return this._DisappearCondition;
      }
      set
      {
        this._DisappearCondition = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
