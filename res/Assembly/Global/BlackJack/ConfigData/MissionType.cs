﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionType")]
  public enum MissionType
  {
    [ProtoEnum(Name = "MissionType_CardPoolSelectCard", Value = 1)] MissionType_CardPoolSelectCard = 1,
    [ProtoEnum(Name = "MissionType_SummonHero", Value = 2)] MissionType_SummonHero = 2,
    [ProtoEnum(Name = "MissionType_ConsumeEnergy", Value = 3)] MissionType_ConsumeEnergy = 3,
    [ProtoEnum(Name = "MissionType_CompleteEvent", Value = 4)] MissionType_CompleteEvent = 4,
    [ProtoEnum(Name = "MissionType_CompleteLevel", Value = 5)] MissionType_CompleteLevel = 5,
    [ProtoEnum(Name = "MissionType_DirectActivation", Value = 6)] MissionType_DirectActivation = 6,
    [ProtoEnum(Name = "MissionType_CompleteAllEverydayMission", Value = 7)] MissionType_CompleteAllEverydayMission = 7,
    [ProtoEnum(Name = "MissionType_CompleteScenario", Value = 8)] MissionType_CompleteScenario = 8,
    [ProtoEnum(Name = "MissionType_ComleteSimpleRiftLevel", Value = 9)] MissionType_ComleteSimpleRiftLevel = 9,
    [ProtoEnum(Name = "MissionType_ComleteDiffcultRiftLevel", Value = 10)] MissionType_ComleteDiffcultRiftLevel = 10, // 0x0000000A
    [ProtoEnum(Name = "MissionType_GetRiftLevelFightAchievement", Value = 11)] MissionType_GetRiftLevelFightAchievement = 11, // 0x0000000B
    [ProtoEnum(Name = "MissionType_GetRiftLevelFightStar", Value = 12)] MissionType_GetRiftLevelFightStar = 12, // 0x0000000C
    [ProtoEnum(Name = "MissionType_ConsumeCrystal", Value = 13)] MissionType_ConsumeCrystal = 13, // 0x0000000D
    [ProtoEnum(Name = "MissionType_ConsumeGold", Value = 14)] MissionType_ConsumeGold = 14, // 0x0000000E
    [ProtoEnum(Name = "MissionType_UseAnyHeroExpItem", Value = 15)] MissionType_UseAnyHeroExpItem = 15, // 0x0000000F
    [ProtoEnum(Name = "MissionType_AllHeroAllJobLevelUpNums", Value = 16)] MissionType_AllHeroAllJobLevelUpNums = 16, // 0x00000010
    [ProtoEnum(Name = "MissionType_AllHerohaveJobNums", Value = 17)] MissionType_AllHerohaveJobNums = 17, // 0x00000011
    [ProtoEnum(Name = "MissionType_AllHeroHaveSkillNums", Value = 18)] MissionType_AllHeroHaveSkillNums = 18, // 0x00000012
    [ProtoEnum(Name = "MissionType_AllHeroHaveSoliderNums", Value = 19)] MissionType_AllHeroHaveSoliderNums = 19, // 0x00000013
    [ProtoEnum(Name = "MissionType_AllJobMasterHeroNums", Value = 20)] MissionType_AllJobMasterHeroNums = 20, // 0x00000014
    [ProtoEnum(Name = "MissionType_HeroMasterJobNums", Value = 21)] MissionType_HeroMasterJobNums = 21, // 0x00000015
    [ProtoEnum(Name = "MissionType_HaveStarHeroNums", Value = 22)] MissionType_HaveStarHeroNums = 22, // 0x00000016
    [ProtoEnum(Name = "MissionType_PlayerLevelUp", Value = 23)] MissionType_PlayerLevelUp = 23, // 0x00000017
    [ProtoEnum(Name = "MissionType_UseCrystalBuyEnergyNums", Value = 24)] MissionType_UseCrystalBuyEnergyNums = 24, // 0x00000018
    [ProtoEnum(Name = "MissionType_LoginGameDays", Value = 25)] MissionType_LoginGameDays = 25, // 0x00000019
    [ProtoEnum(Name = "MissionType_RankJobHeroNums", Value = 26)] MissionType_RankJobHeroNums = 26, // 0x0000001A
    [ProtoEnum(Name = "MissionType_SpecificHeroFightNums", Value = 27)] MissionType_SpecificHeroFightNums = 27, // 0x0000001B
    [ProtoEnum(Name = "MissionType_CompleteBattleAchievement", Value = 28)] MissionType_CompleteBattleAchievement = 28, // 0x0000001C
    [ProtoEnum(Name = "MissionType_SpecificHeroMasterSpecificJobRelationId", Value = 29)] MissionType_SpecificHeroMasterSpecificJobRelationId = 29, // 0x0000001D
    [ProtoEnum(Name = "MissionType_ArenaConsecutiveVictoryNums", Value = 30)] MissionType_ArenaConsecutiveVictoryNums = 30, // 0x0000001E
    [ProtoEnum(Name = "MissionType_HasAboveLevelHeroNums", Value = 31)] MissionType_HasAboveLevelHeroNums = 31, // 0x0000001F
    [ProtoEnum(Name = "MissionType_TransferNewJobNums", Value = 32)] MissionType_TransferNewJobNums = 32, // 0x00000020
    [ProtoEnum(Name = "MissionType_HaveRankHeroNums", Value = 33)] MissionType_HaveRankHeroNums = 33, // 0x00000021
    [ProtoEnum(Name = "MissionType_SpecificHeroReachSpecificLevel", Value = 34)] MissionType_SpecificHeroReachSpecificLevel = 34, // 0x00000022
    [ProtoEnum(Name = "MissionType_TrainingTechToLevel", Value = 35)] MissionType_TrainingTechToLevel = 35, // 0x00000023
    [ProtoEnum(Name = "MissionType_EquipmentToLevel", Value = 36)] MissionType_EquipmentToLevel = 36, // 0x00000024
    [ProtoEnum(Name = "MissionType_FinishTeamBattle", Value = 37)] MissionType_FinishTeamBattle = 37, // 0x00000025
    [ProtoEnum(Name = "MissionType_FavorabilityToLevel", Value = 38)] MissionType_FavorabilityToLevel = 38, // 0x00000026
    [ProtoEnum(Name = "MissionType_FetterToLevel", Value = 39)] MissionType_FetterToLevel = 39, // 0x00000027
    [ProtoEnum(Name = "MissionType_EnchatEquipments", Value = 40)] MissionType_EnchatEquipments = 40, // 0x00000028
    [ProtoEnum(Name = "MissionType_TotalHeroJobLevelUp", Value = 41)] MissionType_TotalHeroJobLevelUp = 41, // 0x00000029
    [ProtoEnum(Name = "MissionType_AddFriend", Value = 42)] MissionType_AddFriend = 42, // 0x0000002A
    [ProtoEnum(Name = "MissionType_AssignHeroToTask", Value = 43)] MissionType_AssignHeroToTask = 43, // 0x0000002B
    [ProtoEnum(Name = "MissionType_BattlePractice", Value = 44)] MissionType_BattlePractice = 44, // 0x0000002C
    [ProtoEnum(Name = "MissionType_RealTimeArena", Value = 45)] MissionType_RealTimeArena = 45, // 0x0000002D
    [ProtoEnum(Name = "MissionType_ReachedSpecificRealTimeArenaDan", Value = 46)] MissionType_ReachedSpecificRealTimeArenaDan = 46, // 0x0000002E
    [ProtoEnum(Name = "MissionType_DailyEquipmentEnchant", Value = 47)] MissionType_DailyEquipmentEnchant = 47, // 0x0000002F
    [ProtoEnum(Name = "MissionType_BuyGiftStoreGoods", Value = 48)] MissionType_BuyGiftStoreGoods = 48, // 0x00000030
    [ProtoEnum(Name = "MissionType_BuyRechargeStoreGoods", Value = 49)] MissionType_BuyRechargeStoreGoods = 49, // 0x00000031
    [ProtoEnum(Name = "MissionType_Share", Value = 50)] MissionType_Share = 50, // 0x00000032
    [ProtoEnum(Name = "MissionType_MissionSelectCard", Value = 51)] MissionType_MissionSelectCard = 51, // 0x00000033
    [ProtoEnum(Name = "MissionType_ClimbTower2SpecificFloor", Value = 52)] MissionType_ClimbTower2SpecificFloor = 52, // 0x00000034
    [ProtoEnum(Name = "MissionType_AccumulateLoginAfterActivityOpen", Value = 53)] MissionType_AccumulateLoginAfterActivityOpen = 53, // 0x00000035
    [ProtoEnum(Name = "MissionType_TrainningTechLevelUp", Value = 54)] MissionType_TrainningTechLevelUp = 54, // 0x00000036
    [ProtoEnum(Name = "MissionType_EquipmentLevelUp", Value = 55)] MissionType_EquipmentLevelUp = 55, // 0x00000037
    [ProtoEnum(Name = "MissionType_HeroFetterLevelUp", Value = 56)] MissionType_HeroFetterLevelUp = 56, // 0x00000038
    [ProtoEnum(Name = "MissionType_JoinGuild", Value = 57)] MissionType_JoinGuild = 57, // 0x00000039
    [ProtoEnum(Name = "MissionType_GuildMassiveCombat", Value = 58)] MissionType_GuildMassiveCombat = 58, // 0x0000003A
    [ProtoEnum(Name = "MissionType_SucceedAttackClimbTowerLevel", Value = 59)] MissionType_SucceedAttackClimbTowerLevel = 59, // 0x0000003B
    [ProtoEnum(Name = "MissionType_EquipmentStarLevelUp", Value = 60)] MissionType_EquipmentStarLevelUp = 60, // 0x0000003C
    [ProtoEnum(Name = "MissionType_CompleteMissions", Value = 61)] MissionType_CompleteMissions = 61, // 0x0000003D
    [ProtoEnum(Name = "MissionType_KillGoblin", Value = 62)] MissionType_KillGoblin = 62, // 0x0000003E
    [ProtoEnum(Name = "MissionType_GetAncientCallBossDamageEvaluate", Value = 63)] MissionType_GetAncientCallBossDamageEvaluate = 63, // 0x0000003F
    [ProtoEnum(Name = "MissionType_CrystalTicketSummon", Value = 64)] MissionType_CrystalTicketSummon = 64, // 0x00000040
    [ProtoEnum(Name = "MissionType_ClientDecidedAchievement", Value = 65)] MissionType_ClientDecidedAchievement = 65, // 0x00000041
  }
}
