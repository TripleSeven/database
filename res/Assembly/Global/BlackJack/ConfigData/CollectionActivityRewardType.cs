﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityRewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityRewardType")]
  public enum CollectionActivityRewardType
  {
    [ProtoEnum(Name = "CollectionActivityRewardType_None", Value = 0)] CollectionActivityRewardType_None,
    [ProtoEnum(Name = "CollectionActivityRewardType_Exchange", Value = 1)] CollectionActivityRewardType_Exchange,
    [ProtoEnum(Name = "CollectionActivityRewardType_Score", Value = 2)] CollectionActivityRewardType_Score,
  }
}
