﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroInfo")]
  [Serializable]
  public class ConfigDataHeroInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _Name_Eng;
    private bool _Useable;
    private int _Sex;
    private int _Star;
    private int _Rank;
    private List<HeroInfoStarToRank> _StarToRank;
    private int _FragmentItem_ID;
    private int _ExchangedFragmentCount;
    private int _Soldier_ID;
    private List<int> _SelectableSoldier_ID;
    private List<int> _Skills_ID;
    private List<int> _HiddenSkills_ID;
    private int _HPCmd_INI;
    private int _DFCmd_INI;
    private int _ATCmd_INI;
    private int _MagicDFCmd_INI;
    private string _CmdRating;
    private List<int> _HPStar;
    private List<int> _ATStar;
    private List<int> _MagicStar;
    private List<int> _DFStar;
    private List<int> _MagicDFStar;
    private List<int> _DEXStar;
    private int _JobConnection_ID;
    private List<int> _UseableJobConnections_ID;
    private int _CharImage_ID;
    private int _HeroInformation_ID;
    private int _Monster_ID;
    private List<int> _TechCanLearnSoldiers_ID;
    private List<int> _Skins_ID;
    private bool _HeroArchiveShow;
    private List<BlackJack.ConfigData.HeroBelongProduction> _HeroBelongProduction;
    private string _HeroShowName;
    private IExtension extensionObject;
    public ConfigDataSoldierInfo m_soldierInfo;
    public ConfigDataSkillInfo[] m_skillInfos;
    public ConfigDataSkillInfo[] m_hiddenSkillInfos;
    public ConfigDataJobConnectionInfo m_jobConnectionInfo;
    public ConfigDataJobConnectionInfo[] m_useableJobConnectionInfos;
    public ConfigDataCharImageInfo m_charImageInfo;
    public ConfigDataCharImageInfo[] m_starToCharImageInfos;
    public ConfigDataHeroInformationInfo m_informationInfo;
    public ConfigDataMonsterInfo m_monsterInfo;
    public List<int> m_heroTagIds;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name_Eng")]
    public string Name_Eng
    {
      get
      {
        return this._Name_Eng;
      }
      set
      {
        this._Name_Eng = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Useable")]
    public bool Useable
    {
      get
      {
        return this._Useable;
      }
      set
      {
        this._Useable = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Sex")]
    public int Sex
    {
      get
      {
        return this._Sex;
      }
      set
      {
        this._Sex = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Star")]
    public int Star
    {
      get
      {
        return this._Star;
      }
      set
      {
        this._Star = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "StarToRank")]
    public List<HeroInfoStarToRank> StarToRank
    {
      get
      {
        return this._StarToRank;
      }
      set
      {
        this._StarToRank = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FragmentItem_ID")]
    public int FragmentItem_ID
    {
      get
      {
        return this._FragmentItem_ID;
      }
      set
      {
        this._FragmentItem_ID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExchangedFragmentCount")]
    public int ExchangedFragmentCount
    {
      get
      {
        return this._ExchangedFragmentCount;
      }
      set
      {
        this._ExchangedFragmentCount = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Soldier_ID")]
    public int Soldier_ID
    {
      get
      {
        return this._Soldier_ID;
      }
      set
      {
        this._Soldier_ID = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, Name = "SelectableSoldier_ID")]
    public List<int> SelectableSoldier_ID
    {
      get
      {
        return this._SelectableSoldier_ID;
      }
      set
      {
        this._SelectableSoldier_ID = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, Name = "Skills_ID")]
    public List<int> Skills_ID
    {
      get
      {
        return this._Skills_ID;
      }
      set
      {
        this._Skills_ID = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, Name = "HiddenSkills_ID")]
    public List<int> HiddenSkills_ID
    {
      get
      {
        return this._HiddenSkills_ID;
      }
      set
      {
        this._HiddenSkills_ID = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HPCmd_INI")]
    public int HPCmd_INI
    {
      get
      {
        return this._HPCmd_INI;
      }
      set
      {
        this._HPCmd_INI = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DFCmd_INI")]
    public int DFCmd_INI
    {
      get
      {
        return this._DFCmd_INI;
      }
      set
      {
        this._DFCmd_INI = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ATCmd_INI")]
    public int ATCmd_INI
    {
      get
      {
        return this._ATCmd_INI;
      }
      set
      {
        this._ATCmd_INI = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDFCmd_INI")]
    public int MagicDFCmd_INI
    {
      get
      {
        return this._MagicDFCmd_INI;
      }
      set
      {
        this._MagicDFCmd_INI = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, IsRequired = true, Name = "CmdRating")]
    public string CmdRating
    {
      get
      {
        return this._CmdRating;
      }
      set
      {
        this._CmdRating = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, Name = "HPStar")]
    public List<int> HPStar
    {
      get
      {
        return this._HPStar;
      }
      set
      {
        this._HPStar = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, Name = "ATStar")]
    public List<int> ATStar
    {
      get
      {
        return this._ATStar;
      }
      set
      {
        this._ATStar = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, Name = "MagicStar")]
    public List<int> MagicStar
    {
      get
      {
        return this._MagicStar;
      }
      set
      {
        this._MagicStar = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, Name = "DFStar")]
    public List<int> DFStar
    {
      get
      {
        return this._DFStar;
      }
      set
      {
        this._DFStar = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, Name = "MagicDFStar")]
    public List<int> MagicDFStar
    {
      get
      {
        return this._MagicDFStar;
      }
      set
      {
        this._MagicDFStar = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, Name = "DEXStar")]
    public List<int> DEXStar
    {
      get
      {
        return this._DEXStar;
      }
      set
      {
        this._DEXStar = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JobConnection_ID")]
    public int JobConnection_ID
    {
      get
      {
        return this._JobConnection_ID;
      }
      set
      {
        this._JobConnection_ID = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, Name = "UseableJobConnections_ID")]
    public List<int> UseableJobConnections_ID
    {
      get
      {
        return this._UseableJobConnections_ID;
      }
      set
      {
        this._UseableJobConnections_ID = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImage_ID")]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroInformation_ID")]
    public int HeroInformation_ID
    {
      get
      {
        return this._HeroInformation_ID;
      }
      set
      {
        this._HeroInformation_ID = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Monster_ID")]
    public int Monster_ID
    {
      get
      {
        return this._Monster_ID;
      }
      set
      {
        this._Monster_ID = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, Name = "TechCanLearnSoldiers_ID")]
    public List<int> TechCanLearnSoldiers_ID
    {
      get
      {
        return this._TechCanLearnSoldiers_ID;
      }
      set
      {
        this._TechCanLearnSoldiers_ID = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, Name = "Skins_ID")]
    public List<int> Skins_ID
    {
      get
      {
        return this._Skins_ID;
      }
      set
      {
        this._Skins_ID = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.Default, IsRequired = true, Name = "HeroArchiveShow")]
    public bool HeroArchiveShow
    {
      get
      {
        return this._HeroArchiveShow;
      }
      set
      {
        this._HeroArchiveShow = value;
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.TwosComplement, Name = "HeroBelongProduction")]
    public List<BlackJack.ConfigData.HeroBelongProduction> HeroBelongProduction
    {
      get
      {
        return this._HeroBelongProduction;
      }
      set
      {
        this._HeroBelongProduction = value;
      }
    }

    [ProtoMember(41, DataFormat = DataFormat.Default, IsRequired = true, Name = "HeroShowName")]
    public string HeroShowName
    {
      get
      {
        return this._HeroShowName;
      }
      set
      {
        this._HeroShowName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRank(int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetCharImageInfo(int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
