﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionPeriodType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionPeriodType")]
  public enum MissionPeriodType
  {
    [ProtoEnum(Name = "MissionPeriodType_Everyday", Value = 1)] MissionPeriodType_Everyday = 1,
    [ProtoEnum(Name = "MissionPeriodType_OneOff", Value = 2)] MissionPeriodType_OneOff = 2,
  }
}
