﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftChapterInfoUnlockConditions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftChapterInfoUnlockConditions")]
  [Serializable]
  public class RiftChapterInfoUnlockConditions : IExtensible
  {
    private RiftChapterUnlockConditionType _ConditionType;
    private int _Param;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConditionType")]
    public RiftChapterUnlockConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param")]
    public int Param
    {
      get
      {
        return this._Param;
      }
      set
      {
        this._Param = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
