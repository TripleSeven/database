﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaOpponentPointZoneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArenaOpponentPointZoneInfo")]
  [Serializable]
  public class ConfigDataArenaOpponentPointZoneInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _MinPercent;
    private int _MaxPercent;
    private int _VictoryPoint;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MinPercent")]
    public int MinPercent
    {
      get
      {
        return this._MinPercent;
      }
      set
      {
        this._MinPercent = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MaxPercent")]
    public int MaxPercent
    {
      get
      {
        return this._MaxPercent;
      }
      set
      {
        this._MaxPercent = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "VictoryPoint")]
    public int VictoryPoint
    {
      get
      {
        return this._VictoryPoint;
      }
      set
      {
        this._VictoryPoint = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
