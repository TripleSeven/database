﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAncientCallBehavioralDescInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAncientCallBehavioralDescInfo")]
  [Serializable]
  public class ConfigDataAncientCallBehavioralDescInfo : IExtensible
  {
    private int _ID;
    private List<AncientCallBossBehavioralDesc> _BehavioralDesc;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallBehavioralDescInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "BehavioralDesc")]
    public List<AncientCallBossBehavioralDesc> BehavioralDesc
    {
      get
      {
        return this._BehavioralDesc;
      }
      set
      {
        this._BehavioralDesc = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
