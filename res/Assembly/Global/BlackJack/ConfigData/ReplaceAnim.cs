﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ReplaceAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ReplaceAnim")]
  [Serializable]
  public class ReplaceAnim : IExtensible
  {
    private string _DefaultName;
    private string _ReplaceName;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefaultName")]
    public string DefaultName
    {
      get
      {
        return this._DefaultName;
      }
      set
      {
        this._DefaultName = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ReplaceName")]
    public string ReplaceName
    {
      get
      {
        return this._ReplaceName;
      }
      set
      {
        this._ReplaceName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
