﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventGiftUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventGiftUnlockConditionType")]
  public enum EventGiftUnlockConditionType
  {
    [ProtoEnum(Name = "EventGiftUnlockConditionType_None", Value = 0)] EventGiftUnlockConditionType_None,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_PlayerLevel", Value = 1)] EventGiftUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_SummonRankHero", Value = 2)] EventGiftUnlockConditionType_SummonRankHero,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_CompleteLevel", Value = 3)] EventGiftUnlockConditionType_CompleteLevel,
    [ProtoEnum(Name = "EventGiftUnlockConditionType_ReachOfflineArenaScore", Value = 4)] EventGiftUnlockConditionType_ReachOfflineArenaScore,
  }
}
