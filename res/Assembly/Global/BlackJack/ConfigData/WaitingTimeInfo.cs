﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaitingTimeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaitingTimeInfo")]
  [Serializable]
  public class WaitingTimeInfo : IExtensible
  {
    private int _WaitingTime;
    private int _Min;
    private int _Max;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WaitingTime")]
    public int WaitingTime
    {
      get
      {
        return this._WaitingTime;
      }
      set
      {
        this._WaitingTime = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Min")]
    public int Min
    {
      get
      {
        return this._Min;
      }
      set
      {
        this._Min = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Max")]
    public int Max
    {
      get
      {
        return this._Max;
      }
      set
      {
        this._Max = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
