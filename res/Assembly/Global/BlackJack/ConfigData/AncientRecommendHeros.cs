﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.AncientRecommendHeros
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "AncientRecommendHeros")]
  [Serializable]
  public class AncientRecommendHeros : IExtensible
  {
    private int _Hero;
    private int _DescId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Hero")]
    public int Hero
    {
      get
      {
        return this._Hero;
      }
      set
      {
        this._Hero = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DescId")]
    public int DescId
    {
      get
      {
        return this._DescId;
      }
      set
      {
        this._DescId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
