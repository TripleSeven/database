﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleEventActionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleEventActionType")]
  public enum BattleEventActionType
  {
    [ProtoEnum(Name = "BattleEventActionType_None", Value = 0)] BattleEventActionType_None,
    [ProtoEnum(Name = "BattleEventActionType_Relief", Value = 1)] BattleEventActionType_Relief,
    [ProtoEnum(Name = "BattleEventActionType_Retreat", Value = 2)] BattleEventActionType_Retreat,
    [ProtoEnum(Name = "BattleEventActionType_Dialog", Value = 3)] BattleEventActionType_Dialog,
    [ProtoEnum(Name = "BattleEventActionType_ChangeTeam", Value = 4)] BattleEventActionType_ChangeTeam,
    [ProtoEnum(Name = "BattleEventActionType_AttachBuff", Value = 5)] BattleEventActionType_AttachBuff,
    [ProtoEnum(Name = "BattleEventActionType_Music", Value = 6)] BattleEventActionType_Music,
    [ProtoEnum(Name = "BattleEventActionType_ChangeTerrain", Value = 7)] BattleEventActionType_ChangeTerrain,
    [ProtoEnum(Name = "BattleEventActionType_Perform", Value = 8)] BattleEventActionType_Perform,
    [ProtoEnum(Name = "BattleEventActionType_ChangeBehavior", Value = 9)] BattleEventActionType_ChangeBehavior,
    [ProtoEnum(Name = "BattleEventActionType_RetreatPosition", Value = 10)] BattleEventActionType_RetreatPosition,
    [ProtoEnum(Name = "BattleEventActionType_Replace", Value = 11)] BattleEventActionType_Replace,
    [ProtoEnum(Name = "BattleEventActionType_RemoveBuff", Value = 12)] BattleEventActionType_RemoveBuff,
    [ProtoEnum(Name = "BattleEventActionType_AttachBuffPosition", Value = 13)] BattleEventActionType_AttachBuffPosition,
    [ProtoEnum(Name = "BattleEventActionType_Teleport", Value = 14)] BattleEventActionType_Teleport,
    [ProtoEnum(Name = "BattleEventActionType_ResetEvent", Value = 15)] BattleEventActionType_ResetEvent,
    [ProtoEnum(Name = "BattleEventActionType_DisableEvent", Value = 16)] BattleEventActionType_DisableEvent,
    [ProtoEnum(Name = "BattleEventActionType_SetVariable", Value = 17)] BattleEventActionType_SetVariable,
    [ProtoEnum(Name = "BattleEventActionType_AddVariable", Value = 18)] BattleEventActionType_AddVariable,
    [ProtoEnum(Name = "BattleEventActionType_TerrainEffect", Value = 19)] BattleEventActionType_TerrainEffect,
    [ProtoEnum(Name = "BattleEventActionType_RandomAction", Value = 20)] BattleEventActionType_RandomAction,
    [ProtoEnum(Name = "BattleEventActionType_ActorSkill", Value = 21)] BattleEventActionType_ActorSkill,
    [ProtoEnum(Name = "BattleEventActionType_BossPhase", Value = 22)] BattleEventActionType_BossPhase,
    [ProtoEnum(Name = "BattleEventActionType_AddHp", Value = 23)] BattleEventActionType_AddHp,
  }
}
