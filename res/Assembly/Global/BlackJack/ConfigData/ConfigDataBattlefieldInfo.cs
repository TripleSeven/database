﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattlefieldInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattlefieldInfo")]
  [Serializable]
  public class ConfigDataBattlefieldInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _Width;
    private int _Height;
    private List<int> _Terrains;
    private string _BattleMap;
    private int _BackgroundWidth;
    private int _BackgroundHeight;
    private float _BackgroundOffsetX;
    private float _BackgroundOffsetY;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlefieldInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Width")]
    public int Width
    {
      get
      {
        return this._Width;
      }
      set
      {
        this._Width = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Height")]
    public int Height
    {
      get
      {
        return this._Height;
      }
      set
      {
        this._Height = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Terrains")]
    public List<int> Terrains
    {
      get
      {
        return this._Terrains;
      }
      set
      {
        this._Terrains = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleMap")]
    public string BattleMap
    {
      get
      {
        return this._BattleMap;
      }
      set
      {
        this._BattleMap = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundWidth")]
    public int BackgroundWidth
    {
      get
      {
        return this._BackgroundWidth;
      }
      set
      {
        this._BackgroundWidth = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BackgroundHeight")]
    public int BackgroundHeight
    {
      get
      {
        return this._BackgroundHeight;
      }
      set
      {
        this._BackgroundHeight = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "BackgroundOffsetX")]
    public float BackgroundOffsetX
    {
      get
      {
        return this._BackgroundOffsetX;
      }
      set
      {
        this._BackgroundOffsetX = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "BackgroundOffsetY")]
    public float BackgroundOffsetY
    {
      get
      {
        return this._BackgroundOffsetY;
      }
      set
      {
        this._BackgroundOffsetY = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
