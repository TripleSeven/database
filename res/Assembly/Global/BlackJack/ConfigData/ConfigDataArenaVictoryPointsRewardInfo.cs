﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataArenaVictoryPointsRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataArenaVictoryPointsRewardInfo")]
  [Serializable]
  public class ConfigDataArenaVictoryPointsRewardInfo : IExtensible
  {
    private int _ID;
    private List<Goods> _Reward;
    private int _VictoryPoints;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaVictoryPointsRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "VictoryPoints")]
    public int VictoryPoints
    {
      get
      {
        return this._VictoryPoints;
      }
      set
      {
        this._VictoryPoints = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
