﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroSkinInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroSkinInfo")]
  [Serializable]
  public class ConfigDataHeroSkinInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _SpecifiedHero;
    private int _CharImageSkinResource_ID;
    private List<JobConnection2SkinResource> _SpecifiedModelSkinResource;
    private string _Desc;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private int _CharImage_ID;
    private int _HeroInformation_ID;
    private string _PeakArenaSignTitle;
    private IExtension extensionObject;
    public int FixedStoreItemId;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SpecifiedHero")]
    public int SpecifiedHero
    {
      get
      {
        return this._SpecifiedHero;
      }
      set
      {
        this._SpecifiedHero = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImageSkinResource_ID")]
    public int CharImageSkinResource_ID
    {
      get
      {
        return this._CharImageSkinResource_ID;
      }
      set
      {
        this._CharImageSkinResource_ID = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "SpecifiedModelSkinResource")]
    public List<JobConnection2SkinResource> SpecifiedModelSkinResource
    {
      get
      {
        return this._SpecifiedModelSkinResource;
      }
      set
      {
        this._SpecifiedModelSkinResource = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetPathDesc")]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImage_ID")]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroInformation_ID")]
    public int HeroInformation_ID
    {
      get
      {
        return this._HeroInformation_ID;
      }
      set
      {
        this._HeroInformation_ID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "PeakArenaSignTitle")]
    public string PeakArenaSignTitle
    {
      get
      {
        return this._PeakArenaSignTitle;
      }
      set
      {
        this._PeakArenaSignTitle = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
