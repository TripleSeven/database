﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMusicAppreciateTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMusicAppreciateTable")]
  [Serializable]
  public class ConfigDataMusicAppreciateTable : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _Tab;
    private int _SortOrder;
    private string _SoundName;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Tab")]
    public int Tab
    {
      get
      {
        return this._Tab;
      }
      set
      {
        this._Tab = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SortOrder")]
    public int SortOrder
    {
      get
      {
        return this._SortOrder;
      }
      set
      {
        this._SortOrder = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "SoundName")]
    public string SoundName
    {
      get
      {
        return this._SoundName;
      }
      set
      {
        this._SoundName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
