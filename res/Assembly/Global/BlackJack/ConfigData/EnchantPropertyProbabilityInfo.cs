﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EnchantPropertyProbabilityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  public class EnchantPropertyProbabilityInfo
  {
    public PropertyModifyType Id { get; set; }

    public List<EnchantPropertyValueInfo> PropertyValues { get; set; }

    public int Weight { get; set; }
  }
}
