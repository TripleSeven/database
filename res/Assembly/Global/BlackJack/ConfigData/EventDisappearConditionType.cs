﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.EventDisappearConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "EventDisappearConditionType")]
  public enum EventDisappearConditionType
  {
    [ProtoEnum(Name = "EventDisappearConditionType_None", Value = 0)] EventDisappearConditionType_None,
    [ProtoEnum(Name = "EventDisappearConditionType_Complete", Value = 1)] EventDisappearConditionType_Complete,
    [ProtoEnum(Name = "EventDisappearConditionType_LifeTime", Value = 2)] EventDisappearConditionType_LifeTime,
  }
}
