﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobUnlockConditionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobUnlockConditionInfo")]
  [Serializable]
  public class ConfigDataJobUnlockConditionInfo : IExtensible
  {
    private int _ID;
    private string _Desc;
    private int _AchievementID;
    private List<Goods> _ItemCost;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobUnlockConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AchievementID")]
    public int AchievementID
    {
      get
      {
        return this._AchievementID;
      }
      set
      {
        this._AchievementID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "ItemCost")]
    public List<Goods> ItemCost
    {
      get
      {
        return this._ItemCost;
      }
      set
      {
        this._ItemCost = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
