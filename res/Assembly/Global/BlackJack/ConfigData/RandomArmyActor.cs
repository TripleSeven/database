﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomArmyActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RandomArmyActor")]
  [Serializable]
  public class RandomArmyActor : IExtensible
  {
    private int _HeroID;
    private int _Level;
    private int _AI;
    private int _Weight;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroID")]
    public int HeroID
    {
      get
      {
        return this._HeroID;
      }
      set
      {
        this._HeroID = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      get
      {
        return this._Level;
      }
      set
      {
        this._Level = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AI")]
    public int AI
    {
      get
      {
        return this._AI;
      }
      set
      {
        this._AI = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight")]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
