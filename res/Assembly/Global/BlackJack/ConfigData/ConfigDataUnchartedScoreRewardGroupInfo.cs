﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUnchartedScoreRewardGroupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUnchartedScoreRewardGroupInfo")]
  [Serializable]
  public class ConfigDataUnchartedScoreRewardGroupInfo : IExtensible
  {
    private int _ID;
    private int _GroupId;
    private int _Score;
    private string _Name;
    private List<Goods> _RewardList;
    private int _RewardDisplayCount;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreRewardGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GroupId")]
    public int GroupId
    {
      get
      {
        return this._GroupId;
      }
      set
      {
        this._GroupId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Score")]
    public int Score
    {
      get
      {
        return this._Score;
      }
      set
      {
        this._Score = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "RewardList")]
    public List<Goods> RewardList
    {
      get
      {
        return this._RewardList;
      }
      set
      {
        this._RewardList = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RewardDisplayCount")]
    public int RewardDisplayCount
    {
      get
      {
        return this._RewardDisplayCount;
      }
      set
      {
        this._RewardDisplayCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
