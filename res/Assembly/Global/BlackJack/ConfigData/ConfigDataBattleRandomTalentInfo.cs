﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleRandomTalentInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleRandomTalentInfo")]
  [Serializable]
  public class ConfigDataBattleRandomTalentInfo : IExtensible
  {
    private int _ID;
    private List<RandomTalent> _RandomTalents;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleRandomTalentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "RandomTalents")]
    public List<RandomTalent> RandomTalents
    {
      get
      {
        return this._RandomTalents;
      }
      set
      {
        this._RandomTalents = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
