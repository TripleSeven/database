﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FriendlyWaitingTimeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FriendlyWaitingTimeInfo")]
  [Serializable]
  public class FriendlyWaitingTimeInfo : IExtensible
  {
    private int _WaitingTime;
    private int _TopBattlePowerMin;
    private int _TopBattlePowerMax;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WaitingTime")]
    public int WaitingTime
    {
      get
      {
        return this._WaitingTime;
      }
      set
      {
        this._WaitingTime = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TopBattlePowerMin")]
    public int TopBattlePowerMin
    {
      get
      {
        return this._TopBattlePowerMin;
      }
      set
      {
        this._TopBattlePowerMin = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TopBattlePowerMax")]
    public int TopBattlePowerMax
    {
      get
      {
        return this._TopBattlePowerMax;
      }
      set
      {
        this._TopBattlePowerMax = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
