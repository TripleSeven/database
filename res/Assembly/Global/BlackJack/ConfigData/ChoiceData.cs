﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ChoiceData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ChoiceData")]
  [Serializable]
  public class ChoiceData : IExtensible
  {
    private int _ChoiceID;
    private int _NextDialogID;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChoiceID")]
    public int ChoiceID
    {
      get
      {
        return this._ChoiceID;
      }
      set
      {
        this._ChoiceID = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextDialogID")]
    public int NextDialogID
    {
      get
      {
        return this._NextDialogID;
      }
      set
      {
        this._NextDialogID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
