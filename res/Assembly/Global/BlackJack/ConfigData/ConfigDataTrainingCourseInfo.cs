﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingCourseInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingCourseInfo")]
  [Serializable]
  public class ConfigDataTrainingCourseInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private int _RoomID;
    private int _RoomLevel;
    private List<int> _Techs;
    private string _SkillTreeName;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingCourseInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Resource")]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomID")]
    public int RoomID
    {
      get
      {
        return this._RoomID;
      }
      set
      {
        this._RoomID = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomLevel")]
    public int RoomLevel
    {
      get
      {
        return this._RoomLevel;
      }
      set
      {
        this._RoomLevel = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "Techs")]
    public List<int> Techs
    {
      get
      {
        return this._Techs;
      }
      set
      {
        this._Techs = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "SkillTreeName")]
    public string SkillTreeName
    {
      get
      {
        return this._SkillTreeName;
      }
      set
      {
        this._SkillTreeName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
