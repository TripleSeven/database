﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataDailyPushNotification
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataDailyPushNotification")]
  [Serializable]
  public class ConfigDataDailyPushNotification : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Content;
    private int _Hour;
    private int _Minute;
    private UserGuideCondition _PushCondition;
    private string _PCParam;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Title")]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Content")]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Hour")]
    public int Hour
    {
      get
      {
        return this._Hour;
      }
      set
      {
        this._Hour = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Minute")]
    public int Minute
    {
      get
      {
        return this._Minute;
      }
      set
      {
        this._Minute = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PushCondition")]
    public UserGuideCondition PushCondition
    {
      get
      {
        return this._PushCondition;
      }
      set
      {
        this._PushCondition = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "PCParam")]
    public string PCParam
    {
      get
      {
        return this._PCParam;
      }
      set
      {
        this._PCParam = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
