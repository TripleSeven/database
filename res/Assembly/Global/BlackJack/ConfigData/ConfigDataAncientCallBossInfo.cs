﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAncientCallBossInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAncientCallBossInfo")]
  [Serializable]
  public class ConfigDataAncientCallBossInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _RecordReward_ID;
    private List<int> _Mission_ID;
    private List<int> _Achievement_ID;
    private List<int> _BossSkillList;
    private string _BossProcess;
    private List<int> _UpHeroTag_IDS;
    private List<AncientRecommendHeros> _RecommendHeros;
    private int _UpSkill_ID;
    private string _UpSkillDesc;
    private List<Behavioral> _BehavioralCycle;
    private string _BossImage;
    private string _BossImage2;
    private string _Model;
    private string _Fx;
    private int _UI_ModelScale;
    private int _UI_ModelOffsetX;
    private int _UI_ModelOffsetY;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallBossInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RecordReward_ID")]
    public int RecordReward_ID
    {
      get
      {
        return this._RecordReward_ID;
      }
      set
      {
        this._RecordReward_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "Mission_ID")]
    public List<int> Mission_ID
    {
      get
      {
        return this._Mission_ID;
      }
      set
      {
        this._Mission_ID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Achievement_ID")]
    public List<int> Achievement_ID
    {
      get
      {
        return this._Achievement_ID;
      }
      set
      {
        this._Achievement_ID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "BossSkillList")]
    public List<int> BossSkillList
    {
      get
      {
        return this._BossSkillList;
      }
      set
      {
        this._BossSkillList = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "BossProcess")]
    public string BossProcess
    {
      get
      {
        return this._BossProcess;
      }
      set
      {
        this._BossProcess = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "UpHeroTag_IDS")]
    public List<int> UpHeroTag_IDS
    {
      get
      {
        return this._UpHeroTag_IDS;
      }
      set
      {
        this._UpHeroTag_IDS = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, Name = "RecommendHeros")]
    public List<AncientRecommendHeros> RecommendHeros
    {
      get
      {
        return this._RecommendHeros;
      }
      set
      {
        this._RecommendHeros = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UpSkill_ID")]
    public int UpSkill_ID
    {
      get
      {
        return this._UpSkill_ID;
      }
      set
      {
        this._UpSkill_ID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "UpSkillDesc")]
    public string UpSkillDesc
    {
      get
      {
        return this._UpSkillDesc;
      }
      set
      {
        this._UpSkillDesc = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, Name = "BehavioralCycle")]
    public List<Behavioral> BehavioralCycle
    {
      get
      {
        return this._BehavioralCycle;
      }
      set
      {
        this._BehavioralCycle = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "BossImage")]
    public string BossImage
    {
      get
      {
        return this._BossImage;
      }
      set
      {
        this._BossImage = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "BossImage2")]
    public string BossImage2
    {
      get
      {
        return this._BossImage2;
      }
      set
      {
        this._BossImage2 = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "Fx")]
    public string Fx
    {
      get
      {
        return this._Fx;
      }
      set
      {
        this._Fx = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelScale")]
    public int UI_ModelScale
    {
      get
      {
        return this._UI_ModelScale;
      }
      set
      {
        this._UI_ModelScale = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelOffsetX")]
    public int UI_ModelOffsetX
    {
      get
      {
        return this._UI_ModelOffsetX;
      }
      set
      {
        this._UI_ModelOffsetX = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UI_ModelOffsetY")]
    public int UI_ModelOffsetY
    {
      get
      {
        return this._UI_ModelOffsetY;
      }
      set
      {
        this._UI_ModelOffsetY = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataAncientCallRecordRewardGroupInfo RecordRewardGroup { get; set; }

    public ConfigDataBattleInfo BattleInfo { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetEvaluateByDamage(int damage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRecordRewardMailTemplateId(int damage)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GetRecordId(int damage)
    {
      return this.RecordRewardGroup.GetRecordId(damage);
    }
  }
}
