﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo")]
  [Serializable]
  public class ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo : IExtensible
  {
    private int _ID;
    private int _RewardGroupID;
    private List<Goods> _RewardItems;
    private int _Points;
    private string _Name;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RewardGroupID")]
    public int RewardGroupID
    {
      get
      {
        return this._RewardGroupID;
      }
      set
      {
        this._RewardGroupID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "RewardItems")]
    public List<Goods> RewardItems
    {
      get
      {
        return this._RewardItems;
      }
      set
      {
        this._RewardItems = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Points")]
    public int Points
    {
      get
      {
        return this._Points;
      }
      set
      {
        this._Points = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
