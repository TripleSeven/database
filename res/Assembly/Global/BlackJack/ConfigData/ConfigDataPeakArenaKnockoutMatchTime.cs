﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaKnockoutMatchTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaKnockoutMatchTime")]
  [Serializable]
  public class ConfigDataPeakArenaKnockoutMatchTime : IExtensible
  {
    private int _ID;
    private int _Season;
    private int _Round;
    private string _Date;
    private string _Time;
    private string _Match;
    private string _MatchType;
    private string _TitleName;
    private string _EndTime;
    private string _PushNotificationTime;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Season")]
    public int Season
    {
      get
      {
        return this._Season;
      }
      set
      {
        this._Season = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      get
      {
        return this._Round;
      }
      set
      {
        this._Round = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Date")]
    public string Date
    {
      get
      {
        return this._Date;
      }
      set
      {
        this._Date = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Time")]
    public string Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Match")]
    public string Match
    {
      get
      {
        return this._Match;
      }
      set
      {
        this._Match = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "MatchType")]
    public string MatchType
    {
      get
      {
        return this._MatchType;
      }
      set
      {
        this._MatchType = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "TitleName")]
    public string TitleName
    {
      get
      {
        return this._TitleName;
      }
      set
      {
        this._TitleName = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "EndTime")]
    public string EndTime
    {
      get
      {
        return this._EndTime;
      }
      set
      {
        this._EndTime = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "PushNotificationTime")]
    public string PushNotificationTime
    {
      get
      {
        return this._PushNotificationTime;
      }
      set
      {
        this._PushNotificationTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
