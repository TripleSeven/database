﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTerrainInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTerrainInfo")]
  [Serializable]
  public class ConfigDataTerrainInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _MovePoint_Ride;
    private int _MovePoint_Walk;
    private int _MovePoint_Water;
    private int _MovePoint_Fly;
    private int _MovePoint_FieldArmy;
    private int _BattleBonus;
    private int _Damage;
    private string _Fx;
    private string _InfoImage;
    private string _MapTileImage;
    private string _Background;
    private int _ColorR;
    private int _ColorG;
    private int _ColorB;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MovePoint_Ride")]
    public int MovePoint_Ride
    {
      get
      {
        return this._MovePoint_Ride;
      }
      set
      {
        this._MovePoint_Ride = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MovePoint_Walk")]
    public int MovePoint_Walk
    {
      get
      {
        return this._MovePoint_Walk;
      }
      set
      {
        this._MovePoint_Walk = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MovePoint_Water")]
    public int MovePoint_Water
    {
      get
      {
        return this._MovePoint_Water;
      }
      set
      {
        this._MovePoint_Water = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MovePoint_Fly")]
    public int MovePoint_Fly
    {
      get
      {
        return this._MovePoint_Fly;
      }
      set
      {
        this._MovePoint_Fly = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MovePoint_FieldArmy")]
    public int MovePoint_FieldArmy
    {
      get
      {
        return this._MovePoint_FieldArmy;
      }
      set
      {
        this._MovePoint_FieldArmy = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleBonus")]
    public int BattleBonus
    {
      get
      {
        return this._BattleBonus;
      }
      set
      {
        this._BattleBonus = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Damage")]
    public int Damage
    {
      get
      {
        return this._Damage;
      }
      set
      {
        this._Damage = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "Fx")]
    public string Fx
    {
      get
      {
        return this._Fx;
      }
      set
      {
        this._Fx = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "InfoImage")]
    public string InfoImage
    {
      get
      {
        return this._InfoImage;
      }
      set
      {
        this._InfoImage = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "MapTileImage")]
    public string MapTileImage
    {
      get
      {
        return this._MapTileImage;
      }
      set
      {
        this._MapTileImage = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "Background")]
    public string Background
    {
      get
      {
        return this._Background;
      }
      set
      {
        this._Background = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ColorR")]
    public int ColorR
    {
      get
      {
        return this._ColorR;
      }
      set
      {
        this._ColorR = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ColorG")]
    public int ColorG
    {
      get
      {
        return this._ColorG;
      }
      set
      {
        this._ColorG = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ColorB")]
    public int ColorB
    {
      get
      {
        return this._ColorB;
      }
      set
      {
        this._ColorB = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
