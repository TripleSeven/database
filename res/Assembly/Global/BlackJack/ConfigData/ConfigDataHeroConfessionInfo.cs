﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroConfessionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroConfessionInfo")]
  [Serializable]
  public class ConfigDataHeroConfessionInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _BackGroundImage;
    private string _CharImage;
    private int _PreWordID;
    private int _CorrectID;
    private int _WrongID;
    private int _UnknownID;
    private int _ConfessionLetterID;
    private int _LastWordID;
    private int _UnlockHeroHeartFetterMinLevel;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "BackGroundImage")]
    public string BackGroundImage
    {
      get
      {
        return this._BackGroundImage;
      }
      set
      {
        this._BackGroundImage = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "CharImage")]
    public string CharImage
    {
      get
      {
        return this._CharImage;
      }
      set
      {
        this._CharImage = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PreWordID")]
    public int PreWordID
    {
      get
      {
        return this._PreWordID;
      }
      set
      {
        this._PreWordID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CorrectID")]
    public int CorrectID
    {
      get
      {
        return this._CorrectID;
      }
      set
      {
        this._CorrectID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WrongID")]
    public int WrongID
    {
      get
      {
        return this._WrongID;
      }
      set
      {
        this._WrongID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UnknownID")]
    public int UnknownID
    {
      get
      {
        return this._UnknownID;
      }
      set
      {
        this._UnknownID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConfessionLetterID")]
    public int ConfessionLetterID
    {
      get
      {
        return this._ConfessionLetterID;
      }
      set
      {
        this._ConfessionLetterID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastWordID")]
    public int LastWordID
    {
      get
      {
        return this._LastWordID;
      }
      set
      {
        this._LastWordID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UnlockHeroHeartFetterMinLevel")]
    public int UnlockHeroHeartFetterMinLevel
    {
      get
      {
        return this._UnlockHeroHeartFetterMinLevel;
      }
      set
      {
        this._UnlockHeroHeartFetterMinLevel = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
