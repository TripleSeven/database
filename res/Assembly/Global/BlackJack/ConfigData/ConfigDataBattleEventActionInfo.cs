﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleEventActionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleEventActionInfo")]
  [Serializable]
  public class ConfigDataBattleEventActionInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private BattleEventActionType _ActionType;
    private List<int> _Param1;
    private List<int> _Param2;
    private List<ParamPosition> _Param3;
    private List<int> _Param4;
    private string _Param5;
    private List<int> _Param6;
    private List<int> _ReliefActorsBehavior;
    private List<int> _ReliefGroups;
    private List<int> _ReliefGroupBehavior;
    private List<int> _ReliefRandomArmies_ID;
    private IExtension extensionObject;
    public Dictionary<int, int> m_reliefGroupBehaviors;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventActionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActionType")]
    public BattleEventActionType ActionType
    {
      get
      {
        return this._ActionType;
      }
      set
      {
        this._ActionType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "Param1")]
    public List<int> Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "Param2")]
    public List<int> Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Param3")]
    public List<ParamPosition> Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "Param4")]
    public List<int> Param4
    {
      get
      {
        return this._Param4;
      }
      set
      {
        this._Param4 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Param5")]
    public string Param5
    {
      get
      {
        return this._Param5;
      }
      set
      {
        this._Param5 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "Param6")]
    public List<int> Param6
    {
      get
      {
        return this._Param6;
      }
      set
      {
        this._Param6 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "ReliefActorsBehavior")]
    public List<int> ReliefActorsBehavior
    {
      get
      {
        return this._ReliefActorsBehavior;
      }
      set
      {
        this._ReliefActorsBehavior = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "ReliefGroups")]
    public List<int> ReliefGroups
    {
      get
      {
        return this._ReliefGroups;
      }
      set
      {
        this._ReliefGroups = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "ReliefGroupBehavior")]
    public List<int> ReliefGroupBehavior
    {
      get
      {
        return this._ReliefGroupBehavior;
      }
      set
      {
        this._ReliefGroupBehavior = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, Name = "ReliefRandomArmies_ID")]
    public List<int> ReliefRandomArmies_ID
    {
      get
      {
        return this._ReliefRandomArmies_ID;
      }
      set
      {
        this._ReliefRandomArmies_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Dictionary<int, int> ComputeGroupBehaviorDict(
      List<int> groups,
      List<int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param1FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param2FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param3FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param4FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Param6FirstValue()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
