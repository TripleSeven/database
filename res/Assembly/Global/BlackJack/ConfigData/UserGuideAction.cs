﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.UserGuideAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "UserGuideAction")]
  public enum UserGuideAction
  {
    [ProtoEnum(Name = "UserGuideAction_", Value = 0)] UserGuideAction_,
    [ProtoEnum(Name = "UserGuideAction_Click", Value = 1)] UserGuideAction_Click,
    [ProtoEnum(Name = "UserGuideAction_ShowSignOn", Value = 2)] UserGuideAction_ShowSignOn,
    [ProtoEnum(Name = "UserGuideAction_EndStep", Value = 3)] UserGuideAction_EndStep,
    [ProtoEnum(Name = "UserGuideAction_MoveWorldCameraTo", Value = 4)] UserGuideAction_MoveWorldCameraTo,
    [ProtoEnum(Name = "UserGuideAction_MoveBattleCameraTo", Value = 5)] UserGuideAction_MoveBattleCameraTo,
    [ProtoEnum(Name = "UserGuideAction_ClickBattleGrid", Value = 6)] UserGuideAction_ClickBattleGrid,
    [ProtoEnum(Name = "UserGuideAction_TemporaryDisableObject", Value = 7)] UserGuideAction_TemporaryDisableObject,
    [ProtoEnum(Name = "UserGuideAction_TemporaryDeactiveObject", Value = 8)] UserGuideAction_TemporaryDeactiveObject,
    [ProtoEnum(Name = "UserGuideAction_ScrollToItem", Value = 9)] UserGuideAction_ScrollToItem,
    [ProtoEnum(Name = "UserGuideAction_WaitSeconds", Value = 10)] UserGuideAction_WaitSeconds,
    [ProtoEnum(Name = "UserGuideAction_WaitObjectClickable", Value = 11)] UserGuideAction_WaitObjectClickable,
    [ProtoEnum(Name = "UserGuideAction_TemporaryDisableMoveBattleCamera", Value = 12)] UserGuideAction_TemporaryDisableMoveBattleCamera,
    [ProtoEnum(Name = "UserGuideAction_FinishUserGuide", Value = 13)] UserGuideAction_FinishUserGuide,
    [ProtoEnum(Name = "UserGuideAction_EnableMoveBattleCamera", Value = 14)] UserGuideAction_EnableMoveBattleCamera,
    [ProtoEnum(Name = "UserGuideAction_EnableInput", Value = 15)] UserGuideAction_EnableInput,
    [ProtoEnum(Name = "UserGuideAction_ShowActivityNotice", Value = 16)] UserGuideAction_ShowActivityNotice,
    [ProtoEnum(Name = "UserGuideAction_IfSeflActiveThenGoto", Value = 17)] UserGuideAction_IfSeflActiveThenGoto,
    [ProtoEnum(Name = "UserGuideAction_Trigger", Value = 18)] UserGuideAction_Trigger,
  }
}
