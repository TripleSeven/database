﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMissionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMissionInfo")]
  [Serializable]
  public class ConfigDataMissionInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Desc;
    private MissionPeriodType _MissionPeriod;
    private MissionColumnType _MissionColumn;
    private MissionType _MissionType;
    private int _Param1;
    private int _Param2;
    private int _Param3;
    private int _Param4;
    private List<int> _Param5;
    private int _MissionUnlockPlayerLvl;
    private int _MissionUnlockPreTaskID;
    private int _MissionUnlockScenarioID;
    private int _SortID;
    private List<Goods> _Reward;
    private int _NoviceExt;
    private List<GetPathData> _GetPathList;
    private bool _IsMonthCardMission;
    private bool _Obsoleted;
    private List<Goods> _LevelMaxReward;
    private bool _InEveryDay;
    private int _RefluxExt;
    private IExtension extensionObject;
    public ConfigDataMissionExtNoviceInfo m_Novice;
    public ConfigDataMissionExtRefluxInfo m_Reflux;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Title")]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionPeriod")]
    public MissionPeriodType MissionPeriod
    {
      get
      {
        return this._MissionPeriod;
      }
      set
      {
        this._MissionPeriod = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionColumn")]
    public MissionColumnType MissionColumn
    {
      get
      {
        return this._MissionColumn;
      }
      set
      {
        this._MissionColumn = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionType")]
    public MissionType MissionType
    {
      get
      {
        return this._MissionType;
      }
      set
      {
        this._MissionType = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param1")]
    public int Param1
    {
      get
      {
        return this._Param1;
      }
      set
      {
        this._Param1 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param2")]
    public int Param2
    {
      get
      {
        return this._Param2;
      }
      set
      {
        this._Param2 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param3")]
    public int Param3
    {
      get
      {
        return this._Param3;
      }
      set
      {
        this._Param3 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Param4")]
    public int Param4
    {
      get
      {
        return this._Param4;
      }
      set
      {
        this._Param4 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "Param5")]
    public List<int> Param5
    {
      get
      {
        return this._Param5;
      }
      set
      {
        this._Param5 = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionUnlockPlayerLvl")]
    public int MissionUnlockPlayerLvl
    {
      get
      {
        return this._MissionUnlockPlayerLvl;
      }
      set
      {
        this._MissionUnlockPlayerLvl = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionUnlockPreTaskID")]
    public int MissionUnlockPreTaskID
    {
      get
      {
        return this._MissionUnlockPreTaskID;
      }
      set
      {
        this._MissionUnlockPreTaskID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MissionUnlockScenarioID")]
    public int MissionUnlockScenarioID
    {
      get
      {
        return this._MissionUnlockScenarioID;
      }
      set
      {
        this._MissionUnlockScenarioID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SortID")]
    public int SortID
    {
      get
      {
        return this._SortID;
      }
      set
      {
        this._SortID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NoviceExt")]
    public int NoviceExt
    {
      get
      {
        return this._NoviceExt;
      }
      set
      {
        this._NoviceExt = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsMonthCardMission")]
    public bool IsMonthCardMission
    {
      get
      {
        return this._IsMonthCardMission;
      }
      set
      {
        this._IsMonthCardMission = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "Obsoleted")]
    public bool Obsoleted
    {
      get
      {
        return this._Obsoleted;
      }
      set
      {
        this._Obsoleted = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, Name = "LevelMaxReward")]
    public List<Goods> LevelMaxReward
    {
      get
      {
        return this._LevelMaxReward;
      }
      set
      {
        this._LevelMaxReward = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, IsRequired = true, Name = "InEveryDay")]
    public bool InEveryDay
    {
      get
      {
        return this._InEveryDay;
      }
      set
      {
        this._InEveryDay = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RefluxExt")]
    public int RefluxExt
    {
      get
      {
        return this._RefluxExt;
      }
      set
      {
        this._RefluxExt = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetRealRewards(bool isLevelMax)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
