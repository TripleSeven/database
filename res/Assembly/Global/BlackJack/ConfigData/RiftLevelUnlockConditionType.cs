﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelUnlockConditionType")]
  public enum RiftLevelUnlockConditionType
  {
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_None", Value = 0)] RiftLevelUnlockConditionType_None,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Scenario", Value = 1)] RiftLevelUnlockConditionType_Scenario,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Achievement", Value = 2)] RiftLevelUnlockConditionType_Achievement,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_Hero", Value = 3)] RiftLevelUnlockConditionType_Hero,
    [ProtoEnum(Name = "RiftLevelUnlockConditionType_RiftLevel", Value = 4)] RiftLevelUnlockConditionType_RiftLevel,
  }
}
