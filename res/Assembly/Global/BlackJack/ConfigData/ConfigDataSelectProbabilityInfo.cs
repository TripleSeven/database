﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSelectProbabilityInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSelectProbabilityInfo")]
  [Serializable]
  public class ConfigDataSelectProbabilityInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _SSRCardProbability;
    private string _SRCardProbability;
    private string _RCardProbability;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "SSRCardProbability")]
    public string SSRCardProbability
    {
      get
      {
        return this._SSRCardProbability;
      }
      set
      {
        this._SSRCardProbability = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "SRCardProbability")]
    public string SRCardProbability
    {
      get
      {
        return this._SRCardProbability;
      }
      set
      {
        this._SRCardProbability = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "RCardProbability")]
    public string RCardProbability
    {
      get
      {
        return this._RCardProbability;
      }
      set
      {
        this._RCardProbability = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
