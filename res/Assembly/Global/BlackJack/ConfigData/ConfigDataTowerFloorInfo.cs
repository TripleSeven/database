﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTowerFloorInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTowerFloorInfo")]
  [Serializable]
  public class ConfigDataTowerFloorInfo : IExtensible
  {
    private int _ID;
    private string _BigFloorName;
    private string _Name;
    private List<Goods> _RewardList;
    private List<int> _RandomLevelIdList;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _MonsterLevel;
    private int _PlayerExp;
    private int _BonusSkill_ID;
    private int _HasBonusHero;
    private string _BonusHeroDesc;
    private List<string> _BonusHeroDescParam;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerFloorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "BigFloorName")]
    public string BigFloorName
    {
      get
      {
        return this._BigFloorName;
      }
      set
      {
        this._BigFloorName = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "RewardList")]
    public List<Goods> RewardList
    {
      get
      {
        return this._RewardList;
      }
      set
      {
        this._RewardList = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "RandomLevelIdList")]
    public List<int> RandomLevelIdList
    {
      get
      {
        return this._RandomLevelIdList;
      }
      set
      {
        this._RandomLevelIdList = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergySuccess")]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFail")]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExp")]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BonusSkill_ID")]
    public int BonusSkill_ID
    {
      get
      {
        return this._BonusSkill_ID;
      }
      set
      {
        this._BonusSkill_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HasBonusHero")]
    public int HasBonusHero
    {
      get
      {
        return this._HasBonusHero;
      }
      set
      {
        this._HasBonusHero = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "BonusHeroDesc")]
    public string BonusHeroDesc
    {
      get
      {
        return this._BonusHeroDesc;
      }
      set
      {
        this._BonusHeroDesc = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "BonusHeroDescParam")]
    public List<string> BonusHeroDescParam
    {
      get
      {
        return this._BonusHeroDescParam;
      }
      set
      {
        this._BonusHeroDescParam = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
