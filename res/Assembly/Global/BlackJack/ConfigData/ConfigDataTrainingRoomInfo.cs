﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingRoomInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingRoomInfo")]
  [Serializable]
  public class ConfigDataTrainingRoomInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<int> _SoldierType;
    private int _LevelToUnlock;
    private IExtension extensionObject;
    public List<int> m_totalLevelupExpSteps;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingRoomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "SoldierType")]
    public List<int> SoldierType
    {
      get
      {
        return this._SoldierType;
      }
      set
      {
        this._SoldierType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelToUnlock")]
    public int LevelToUnlock
    {
      get
      {
        return this._LevelToUnlock;
      }
      set
      {
        this._LevelToUnlock = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
