﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBuffInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBuffInfo")]
  [Serializable]
  public class ConfigDataBuffInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private bool _IsDebuff;
    private bool _IsEnhance;
    private bool _CanDispel;
    private int _Time;
    private int _Cond_HP_Target;
    private int _Cond_HP_Operator;
    private int _Cond_HP_Value;
    private BuffConditionType _ConditionType;
    private List<int> _ConditionParam;
    private BuffType _BuffType;
    private int _BuffTypeParam1;
    private int _BuffTypeParam2;
    private int _BuffTypeParam3;
    private List<int> _BuffTypeParam4;
    private PropertyModifyType _BuffTypeParam5;
    private List<int> _BuffTypeParam6;
    private bool _SelfNoExtraTime;
    private int _SubType;
    private int _ReplaceRule;
    private int _ReplacePriority;
    private List<FightTag> _FightTags;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private PropertyModifyType _Property3_ID;
    private int _Property3_Value;
    private PropertyModifyType _Property4_ID;
    private int _Property4_Value;
    private int _CDBuff_ID;
    private string _Effect_Attach;
    private string _Effect_Process;
    private string _Effect_Acting;
    private string _Effect_ActingTarget;
    private string _Icon;
    private bool _IconDisplay;
    private IExtension extensionObject;
    public ConfigDataBuffInfo m_cdBuffInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuffInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsDebuff")]
    public bool IsDebuff
    {
      get
      {
        return this._IsDebuff;
      }
      set
      {
        this._IsDebuff = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsEnhance")]
    public bool IsEnhance
    {
      get
      {
        return this._IsEnhance;
      }
      set
      {
        this._IsEnhance = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "CanDispel")]
    public bool CanDispel
    {
      get
      {
        return this._CanDispel;
      }
      set
      {
        this._CanDispel = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time")]
    public int Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Cond_HP_Target")]
    public int Cond_HP_Target
    {
      get
      {
        return this._Cond_HP_Target;
      }
      set
      {
        this._Cond_HP_Target = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Cond_HP_Operator")]
    public int Cond_HP_Operator
    {
      get
      {
        return this._Cond_HP_Operator;
      }
      set
      {
        this._Cond_HP_Operator = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Cond_HP_Value")]
    public int Cond_HP_Value
    {
      get
      {
        return this._Cond_HP_Value;
      }
      set
      {
        this._Cond_HP_Value = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConditionType")]
    public BuffConditionType ConditionType
    {
      get
      {
        return this._ConditionType;
      }
      set
      {
        this._ConditionType = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "ConditionParam")]
    public List<int> ConditionParam
    {
      get
      {
        return this._ConditionParam;
      }
      set
      {
        this._ConditionParam = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffType")]
    public BuffType BuffType
    {
      get
      {
        return this._BuffType;
      }
      set
      {
        this._BuffType = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffTypeParam1")]
    public int BuffTypeParam1
    {
      get
      {
        return this._BuffTypeParam1;
      }
      set
      {
        this._BuffTypeParam1 = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffTypeParam2")]
    public int BuffTypeParam2
    {
      get
      {
        return this._BuffTypeParam2;
      }
      set
      {
        this._BuffTypeParam2 = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffTypeParam3")]
    public int BuffTypeParam3
    {
      get
      {
        return this._BuffTypeParam3;
      }
      set
      {
        this._BuffTypeParam3 = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, Name = "BuffTypeParam4")]
    public List<int> BuffTypeParam4
    {
      get
      {
        return this._BuffTypeParam4;
      }
      set
      {
        this._BuffTypeParam4 = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffTypeParam5")]
    public PropertyModifyType BuffTypeParam5
    {
      get
      {
        return this._BuffTypeParam5;
      }
      set
      {
        this._BuffTypeParam5 = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, Name = "BuffTypeParam6")]
    public List<int> BuffTypeParam6
    {
      get
      {
        return this._BuffTypeParam6;
      }
      set
      {
        this._BuffTypeParam6 = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "SelfNoExtraTime")]
    public bool SelfNoExtraTime
    {
      get
      {
        return this._SelfNoExtraTime;
      }
      set
      {
        this._SelfNoExtraTime = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SubType")]
    public int SubType
    {
      get
      {
        return this._SubType;
      }
      set
      {
        this._SubType = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReplaceRule")]
    public int ReplaceRule
    {
      get
      {
        return this._ReplaceRule;
      }
      set
      {
        this._ReplaceRule = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReplacePriority")]
    public int ReplacePriority
    {
      get
      {
        return this._ReplacePriority;
      }
      set
      {
        this._ReplacePriority = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, Name = "FightTags")]
    public List<FightTag> FightTags
    {
      get
      {
        return this._FightTags;
      }
      set
      {
        this._FightTags = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_ID")]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_Value")]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_ID")]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_Value")]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_ID")]
    public PropertyModifyType Property3_ID
    {
      get
      {
        return this._Property3_ID;
      }
      set
      {
        this._Property3_ID = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property3_Value")]
    public int Property3_Value
    {
      get
      {
        return this._Property3_Value;
      }
      set
      {
        this._Property3_Value = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property4_ID")]
    public PropertyModifyType Property4_ID
    {
      get
      {
        return this._Property4_ID;
      }
      set
      {
        this._Property4_ID = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property4_Value")]
    public int Property4_Value
    {
      get
      {
        return this._Property4_Value;
      }
      set
      {
        this._Property4_Value = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CDBuff_ID")]
    public int CDBuff_ID
    {
      get
      {
        return this._CDBuff_ID;
      }
      set
      {
        this._CDBuff_ID = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Attach")]
    public string Effect_Attach
    {
      get
      {
        return this._Effect_Attach;
      }
      set
      {
        this._Effect_Attach = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Process")]
    public string Effect_Process
    {
      get
      {
        return this._Effect_Process;
      }
      set
      {
        this._Effect_Process = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Acting")]
    public string Effect_Acting
    {
      get
      {
        return this._Effect_Acting;
      }
      set
      {
        this._Effect_Acting = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_ActingTarget")]
    public string Effect_ActingTarget
    {
      get
      {
        return this._Effect_ActingTarget;
      }
      set
      {
        this._Effect_ActingTarget = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.Default, IsRequired = true, Name = "IconDisplay")]
    public bool IconDisplay
    {
      get
      {
        return this._IconDisplay;
      }
      set
      {
        this._IconDisplay = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAuraBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsModifyAuraBuff()
    {
      return this.BuffType == BuffType.BuffType_HeroAuraModify;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInfiniteTime()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
