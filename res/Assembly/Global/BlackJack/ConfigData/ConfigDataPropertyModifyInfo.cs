﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPropertyModifyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPropertyModifyInfo")]
  [Serializable]
  public class ConfigDataPropertyModifyInfo : IExtensible
  {
    private int _ID;
    private PropertyModifyType _PropertyModifyType;
    private string _Name;
    private bool _IsAddType;
    private bool _IsDynamic;
    private PropertyType _PropertyType;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PropertyModifyType")]
    public PropertyModifyType PropertyModifyType
    {
      get
      {
        return this._PropertyModifyType;
      }
      set
      {
        this._PropertyModifyType = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAddType")]
    public bool IsAddType
    {
      get
      {
        return this._IsAddType;
      }
      set
      {
        this._IsAddType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsDynamic")]
    public bool IsDynamic
    {
      get
      {
        return this._IsDynamic;
      }
      set
      {
        this._IsDynamic = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PropertyType")]
    public PropertyType PropertyType
    {
      get
      {
        return this._PropertyType;
      }
      set
      {
        this._PropertyType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
