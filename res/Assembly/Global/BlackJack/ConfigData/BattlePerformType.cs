﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattlePerformType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattlePerformType")]
  public enum BattlePerformType
  {
    [ProtoEnum(Name = "BattlePerformType_None", Value = 0)] BattlePerformType_None,
    [ProtoEnum(Name = "BattlePerformType_Dialog", Value = 1)] BattlePerformType_Dialog,
    [ProtoEnum(Name = "BattlePerformType_PlayMusic", Value = 2)] BattlePerformType_PlayMusic,
    [ProtoEnum(Name = "BattlePerformType_PlaySound", Value = 3)] BattlePerformType_PlaySound,
    [ProtoEnum(Name = "BattlePerformType_PlayFx", Value = 4)] BattlePerformType_PlayFx,
    [ProtoEnum(Name = "BattlePerformType_PlayActorFx", Value = 5)] BattlePerformType_PlayActorFx,
    [ProtoEnum(Name = "BattlePerformType_ChangeTerrain", Value = 6)] BattlePerformType_ChangeTerrain,
    [ProtoEnum(Name = "BattlePerformType_CameraFocusPosition", Value = 7)] BattlePerformType_CameraFocusPosition,
    [ProtoEnum(Name = "BattlePerformType_CameraFocusActor", Value = 8)] BattlePerformType_CameraFocusActor,
    [ProtoEnum(Name = "BattlePerformType_CreateActor", Value = 9)] BattlePerformType_CreateActor,
    [ProtoEnum(Name = "BattlePerformType_CreateActorNear", Value = 10)] BattlePerformType_CreateActorNear,
    [ProtoEnum(Name = "BattlePerformType_RemoveActor", Value = 11)] BattlePerformType_RemoveActor,
    [ProtoEnum(Name = "BattlePerformType_ActorMove", Value = 12)] BattlePerformType_ActorMove,
    [ProtoEnum(Name = "BattlePerformType_ActorMoveNear", Value = 13)] BattlePerformType_ActorMoveNear,
    [ProtoEnum(Name = "BattlePerformType_ActorAttack", Value = 14)] BattlePerformType_ActorAttack,
    [ProtoEnum(Name = "BattlePerformType_ActorSkill", Value = 15)] BattlePerformType_ActorSkill,
    [ProtoEnum(Name = "BattlePerformType_ActorDir", Value = 16)] BattlePerformType_ActorDir,
    [ProtoEnum(Name = "BattlePerformType_ActorAnimation", Value = 17)] BattlePerformType_ActorAnimation,
    [ProtoEnum(Name = "BattlePerformType_ActorIdle", Value = 18)] BattlePerformType_ActorIdle,
    [ProtoEnum(Name = "BattlePerformType_WaitTime", Value = 19)] BattlePerformType_WaitTime,
    [ProtoEnum(Name = "BattlePerformType_StopBattle", Value = 20)] BattlePerformType_StopBattle,
    [ProtoEnum(Name = "BattlePerformType_TerrainEffect", Value = 21)] BattlePerformType_TerrainEffect,
  }
}
