﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionColumnType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionColumnType")]
  public enum MissionColumnType
  {
    [ProtoEnum(Name = "MissionColumnType_Everyday", Value = 1)] MissionColumnType_Everyday = 1,
    [ProtoEnum(Name = "MissionColumnType_Challenge", Value = 2)] MissionColumnType_Challenge = 2,
    [ProtoEnum(Name = "MissionColumnType_Achievements", Value = 3)] MissionColumnType_Achievements = 3,
    [ProtoEnum(Name = "MissionColumnType_HeroFetter", Value = 4)] MissionColumnType_HeroFetter = 4,
    [ProtoEnum(Name = "MissionColumnType_BeginningOfTheLegend", Value = 5)] MissionColumnType_BeginningOfTheLegend = 5,
    [ProtoEnum(Name = "MissionColumnType_RefluxActivity", Value = 6)] MissionColumnType_RefluxActivity = 6,
    [ProtoEnum(Name = "MissionColumnType_AncientCall", Value = 7)] MissionColumnType_AncientCall = 7,
  }
}
