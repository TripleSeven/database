﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FavourabilityPerformance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FavourabilityPerformance")]
  [Serializable]
  public class FavourabilityPerformance : IExtensible
  {
    private int _FavourabilityLevel;
    private int _PerformanceId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavourabilityLevel")]
    public int FavourabilityLevel
    {
      get
      {
        return this._FavourabilityLevel;
      }
      set
      {
        this._FavourabilityLevel = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PerformanceId")]
    public int PerformanceId
    {
      get
      {
        return this._PerformanceId;
      }
      set
      {
        this._PerformanceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
