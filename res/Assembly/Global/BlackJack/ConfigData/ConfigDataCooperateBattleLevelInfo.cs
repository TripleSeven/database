﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCooperateBattleLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCooperateBattleLevelInfo")]
  [Serializable]
  public class ConfigDataCooperateBattleLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TeamName;
    private int _PlayerLevelRequired;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _Gold;
    private int _DropID;
    private int _ItemDropCountDisplay;
    private string _Icon;
    private int _DayBonusDrop_ID;
    private int _DayBonusExtraDrop_ID;
    private int _TeamDrop_ID;
    private int _OperationalActivityDrop_ID;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataCooperateBattleInfo m_groupInfo;
    public ConfigDataRandomDropRewardInfo m_randomDropInfo;
    public ConfigDataRandomDropRewardInfo m_teamRandomDropInfo;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerLevelRequired")]
    public int PlayerLevelRequired
    {
      get
      {
        return this._PlayerLevelRequired;
      }
      set
      {
        this._PlayerLevelRequired = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergySuccess")]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFail")]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExp")]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExp")]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Gold")]
    public int Gold
    {
      get
      {
        return this._Gold;
      }
      set
      {
        this._Gold = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropID")]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemDropCountDisplay")]
    public int ItemDropCountDisplay
    {
      get
      {
        return this._ItemDropCountDisplay;
      }
      set
      {
        this._ItemDropCountDisplay = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DayBonusDrop_ID")]
    public int DayBonusDrop_ID
    {
      get
      {
        return this._DayBonusDrop_ID;
      }
      set
      {
        this._DayBonusDrop_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DayBonusExtraDrop_ID")]
    public int DayBonusExtraDrop_ID
    {
      get
      {
        return this._DayBonusExtraDrop_ID;
      }
      set
      {
        this._DayBonusExtraDrop_ID = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TeamDrop_ID")]
    public int TeamDrop_ID
    {
      get
      {
        return this._TeamDrop_ID;
      }
      set
      {
        this._TeamDrop_ID = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OperationalActivityDrop_ID")]
    public int OperationalActivityDrop_ID
    {
      get
      {
        return this._OperationalActivityDrop_ID;
      }
      set
      {
        this._OperationalActivityDrop_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
