﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEquipmentInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEquipmentInfo")]
  [Serializable]
  public class ConfigDataEquipmentInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private EquipmentType _EquipmentType;
    private List<int> _ArmyIds;
    private string _Desc;
    private List<int> _JobIds;
    private int _Rank;
    private int _BornStarLevel;
    private PropertyModifyType _Property1_ID;
    private int _Property1_Value;
    private int _Property1_LevelUpValue;
    private PropertyModifyType _Property2_ID;
    private int _Property2_Value;
    private int _Property2_LevelUpValue;
    private int _Exp;
    private string _Icon;
    private List<int> _SkillIds;
    private List<int> _SkillLevels;
    private int _SkillHero;
    private int _SortIndex;
    private List<Goods> _LevelUpGoods1;
    private List<Goods> _LevelUpGoods2;
    private List<Goods> _LevelUpGoods3;
    private List<Goods> _LevelUpGoods4;
    private List<Goods> _LevelUpGoods5;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private int _AlchemyGold;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private bool _ArchiveDisplay;
    private List<int> _EnchantTemplateIds;
    private string _EquipCoditionDesc;
    private bool _IgnoreCost;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EquipmentType")]
    public EquipmentType EquipmentType
    {
      get
      {
        return this._EquipmentType;
      }
      set
      {
        this._EquipmentType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "ArmyIds")]
    public List<int> ArmyIds
    {
      get
      {
        return this._ArmyIds;
      }
      set
      {
        this._ArmyIds = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "JobIds")]
    public List<int> JobIds
    {
      get
      {
        return this._JobIds;
      }
      set
      {
        this._JobIds = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BornStarLevel")]
    public int BornStarLevel
    {
      get
      {
        return this._BornStarLevel;
      }
      set
      {
        this._BornStarLevel = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_ID")]
    public PropertyModifyType Property1_ID
    {
      get
      {
        return this._Property1_ID;
      }
      set
      {
        this._Property1_ID = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_Value")]
    public int Property1_Value
    {
      get
      {
        return this._Property1_Value;
      }
      set
      {
        this._Property1_Value = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property1_LevelUpValue")]
    public int Property1_LevelUpValue
    {
      get
      {
        return this._Property1_LevelUpValue;
      }
      set
      {
        this._Property1_LevelUpValue = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_ID")]
    public PropertyModifyType Property2_ID
    {
      get
      {
        return this._Property2_ID;
      }
      set
      {
        this._Property2_ID = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_Value")]
    public int Property2_Value
    {
      get
      {
        return this._Property2_Value;
      }
      set
      {
        this._Property2_Value = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Property2_LevelUpValue")]
    public int Property2_LevelUpValue
    {
      get
      {
        return this._Property2_LevelUpValue;
      }
      set
      {
        this._Property2_LevelUpValue = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Exp")]
    public int Exp
    {
      get
      {
        return this._Exp;
      }
      set
      {
        this._Exp = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, Name = "SkillIds")]
    public List<int> SkillIds
    {
      get
      {
        return this._SkillIds;
      }
      set
      {
        this._SkillIds = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, Name = "SkillLevels")]
    public List<int> SkillLevels
    {
      get
      {
        return this._SkillLevels;
      }
      set
      {
        this._SkillLevels = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillHero")]
    public int SkillHero
    {
      get
      {
        return this._SkillHero;
      }
      set
      {
        this._SkillHero = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SortIndex")]
    public int SortIndex
    {
      get
      {
        return this._SortIndex;
      }
      set
      {
        this._SortIndex = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, Name = "LevelUpGoods1")]
    public List<Goods> LevelUpGoods1
    {
      get
      {
        return this._LevelUpGoods1;
      }
      set
      {
        this._LevelUpGoods1 = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, Name = "LevelUpGoods2")]
    public List<Goods> LevelUpGoods2
    {
      get
      {
        return this._LevelUpGoods2;
      }
      set
      {
        this._LevelUpGoods2 = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, Name = "LevelUpGoods3")]
    public List<Goods> LevelUpGoods3
    {
      get
      {
        return this._LevelUpGoods3;
      }
      set
      {
        this._LevelUpGoods3 = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, Name = "LevelUpGoods4")]
    public List<Goods> LevelUpGoods4
    {
      get
      {
        return this._LevelUpGoods4;
      }
      set
      {
        this._LevelUpGoods4 = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, Name = "LevelUpGoods5")]
    public List<Goods> LevelUpGoods5
    {
      get
      {
        return this._LevelUpGoods5;
      }
      set
      {
        this._LevelUpGoods5 = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetPathDesc")]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AlchemyGold")]
    public int AlchemyGold
    {
      get
      {
        return this._AlchemyGold;
      }
      set
      {
        this._AlchemyGold = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomDropRewardID")]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayRewardCount")]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.Default, IsRequired = true, Name = "ArchiveDisplay")]
    public bool ArchiveDisplay
    {
      get
      {
        return this._ArchiveDisplay;
      }
      set
      {
        this._ArchiveDisplay = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, Name = "EnchantTemplateIds")]
    public List<int> EnchantTemplateIds
    {
      get
      {
        return this._EnchantTemplateIds;
      }
      set
      {
        this._EnchantTemplateIds = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.Default, IsRequired = true, Name = "EquipCoditionDesc")]
    public string EquipCoditionDesc
    {
      get
      {
        return this._EquipCoditionDesc;
      }
      set
      {
        this._EquipCoditionDesc = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.Default, IsRequired = true, Name = "IgnoreCost")]
    public bool IgnoreCost
    {
      get
      {
        return this._IgnoreCost;
      }
      set
      {
        this._IgnoreCost = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActiveSkillId(int equipmentLevel)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
