﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ArmyTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ArmyTag")]
  public enum ArmyTag
  {
    [ProtoEnum(Name = "ArmyTag_None", Value = 0)] ArmyTag_None,
    [ProtoEnum(Name = "ArmyTag_Spear", Value = 1)] ArmyTag_Spear,
    [ProtoEnum(Name = "ArmyTag_Walk", Value = 2)] ArmyTag_Walk,
    [ProtoEnum(Name = "ArmyTag_Ride", Value = 3)] ArmyTag_Ride,
    [ProtoEnum(Name = "ArmyTag_Fly", Value = 4)] ArmyTag_Fly,
    [ProtoEnum(Name = "ArmyTag_Water", Value = 5)] ArmyTag_Water,
    [ProtoEnum(Name = "ArmyTag_Bow", Value = 6)] ArmyTag_Bow,
    [ProtoEnum(Name = "ArmyTag_Magic", Value = 7)] ArmyTag_Magic,
    [ProtoEnum(Name = "ArmyTag_Monk", Value = 8)] ArmyTag_Monk,
    [ProtoEnum(Name = "ArmyTag_Demon", Value = 9)] ArmyTag_Demon,
    [ProtoEnum(Name = "ArmyTag_Death", Value = 10)] ArmyTag_Death,
    [ProtoEnum(Name = "ArmyTag_Boss", Value = 11)] ArmyTag_Boss,
    [ProtoEnum(Name = "ArmyTag_PuppetWalk", Value = 12)] ArmyTag_PuppetWalk,
    [ProtoEnum(Name = "ArmyTag_PuppetSpear", Value = 13)] ArmyTag_PuppetSpear,
    [ProtoEnum(Name = "ArmyTag_PuppetRide", Value = 14)] ArmyTag_PuppetRide,
    [ProtoEnum(Name = "ArmyTag_PuppetDemon", Value = 15)] ArmyTag_PuppetDemon,
    [ProtoEnum(Name = "ArmyTag_WaterBeast", Value = 16)] ArmyTag_WaterBeast,
    [ProtoEnum(Name = "ArmyTag_Assassin", Value = 17)] ArmyTag_Assassin,
    [ProtoEnum(Name = "ArmyTag_AnikiWalk", Value = 18)] ArmyTag_AnikiWalk,
    [ProtoEnum(Name = "ArmyTag_AnikiSpear", Value = 19)] ArmyTag_AnikiSpear,
    [ProtoEnum(Name = "ArmyTag_AnikiRide", Value = 20)] ArmyTag_AnikiRide,
    [ProtoEnum(Name = "ArmyTag_AnikiFly", Value = 21)] ArmyTag_AnikiFly,
    [ProtoEnum(Name = "ArmyTag_AnikiBow", Value = 22)] ArmyTag_AnikiBow,
    [ProtoEnum(Name = "ArmyTag_AnikiDemon", Value = 23)] ArmyTag_AnikiDemon,
    [ProtoEnum(Name = "ArmyTag_Boss1", Value = 24)] ArmyTag_Boss1,
    [ProtoEnum(Name = "ArmyTag_Boss2", Value = 25)] ArmyTag_Boss2,
    [ProtoEnum(Name = "ArmyTag_Boss3", Value = 26)] ArmyTag_Boss3,
    [ProtoEnum(Name = "ArmyTag_Boss4", Value = 27)] ArmyTag_Boss4,
    [ProtoEnum(Name = "ArmyTag_Boss5", Value = 28)] ArmyTag_Boss5,
    [ProtoEnum(Name = "ArmyTag_Boss6", Value = 29)] ArmyTag_Boss6,
    [ProtoEnum(Name = "ArmyTag_Boss7", Value = 30)] ArmyTag_Boss7,
    [ProtoEnum(Name = "ArmyTag_Boss8", Value = 31)] ArmyTag_Boss8,
    [ProtoEnum(Name = "ArmyTag_Boss9", Value = 32)] ArmyTag_Boss9,
    [ProtoEnum(Name = "ArmyTag_Boss10", Value = 33)] ArmyTag_Boss10,
    [ProtoEnum(Name = "ArmyTag_Count", Value = 34)] ArmyTag_Count,
  }
}
