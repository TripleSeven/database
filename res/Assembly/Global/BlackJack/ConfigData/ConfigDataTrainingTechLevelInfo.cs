﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTrainingTechLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTrainingTechLevelInfo")]
  [Serializable]
  public class ConfigDataTrainingTechLevelInfo : IExtensible
  {
    private int _ID;
    private string _Description;
    private int _PreTechIDs;
    private int _LevelupGoldCost;
    private List<Goods> _LevelupMaterialsCost;
    private int _RoomExp;
    private int _SoldierIDUnlocked;
    private int _SoldierSkillLevelup;
    private int _SoldierSkillID;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Description")]
    public string Description
    {
      get
      {
        return this._Description;
      }
      set
      {
        this._Description = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PreTechIDs")]
    public int PreTechIDs
    {
      get
      {
        return this._PreTechIDs;
      }
      set
      {
        this._PreTechIDs = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LevelupGoldCost")]
    public int LevelupGoldCost
    {
      get
      {
        return this._LevelupGoldCost;
      }
      set
      {
        this._LevelupGoldCost = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "LevelupMaterialsCost")]
    public List<Goods> LevelupMaterialsCost
    {
      get
      {
        return this._LevelupMaterialsCost;
      }
      set
      {
        this._LevelupMaterialsCost = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomExp")]
    public int RoomExp
    {
      get
      {
        return this._RoomExp;
      }
      set
      {
        this._RoomExp = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SoldierIDUnlocked")]
    public int SoldierIDUnlocked
    {
      get
      {
        return this._SoldierIDUnlocked;
      }
      set
      {
        this._SoldierIDUnlocked = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SoldierSkillLevelup")]
    public int SoldierSkillLevelup
    {
      get
      {
        return this._SoldierSkillLevelup;
      }
      set
      {
        this._SoldierSkillLevelup = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SoldierSkillID")]
    public int SoldierSkillID
    {
      get
      {
        return this._SoldierSkillID;
      }
      set
      {
        this._SoldierSkillID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
