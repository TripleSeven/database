﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRafflePoolInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRafflePoolInfo")]
  [Serializable]
  public class ConfigDataRafflePoolInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private RafflePoolType _RafflePoolType;
    private GoodsType _GoodsType;
    private int _DrawItemID;
    private int _FreeDrawCount;
    private List<Int32Pair> _Costs;
    private List<RaffleItem> _RaffleItems;
    private List<TarotIDPair> _TarotIDList;
    private string _Description;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRafflePoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RafflePoolType")]
    public RafflePoolType RafflePoolType
    {
      get
      {
        return this._RafflePoolType;
      }
      set
      {
        this._RafflePoolType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoodsType")]
    public GoodsType GoodsType
    {
      get
      {
        return this._GoodsType;
      }
      set
      {
        this._GoodsType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DrawItemID")]
    public int DrawItemID
    {
      get
      {
        return this._DrawItemID;
      }
      set
      {
        this._DrawItemID = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FreeDrawCount")]
    public int FreeDrawCount
    {
      get
      {
        return this._FreeDrawCount;
      }
      set
      {
        this._FreeDrawCount = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Costs")]
    public List<Int32Pair> Costs
    {
      get
      {
        return this._Costs;
      }
      set
      {
        this._Costs = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "RaffleItems")]
    public List<RaffleItem> RaffleItems
    {
      get
      {
        return this._RaffleItems;
      }
      set
      {
        this._RaffleItems = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "TarotIDList")]
    public List<TarotIDPair> TarotIDList
    {
      get
      {
        return this._TarotIDList;
      }
      set
      {
        this._TarotIDList = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "Description")]
    public string Description
    {
      get
      {
        return this._Description;
      }
      set
      {
        this._Description = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
