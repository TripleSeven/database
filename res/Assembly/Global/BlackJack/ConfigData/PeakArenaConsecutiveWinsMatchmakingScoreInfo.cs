﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakArenaConsecutiveWinsMatchmakingScoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PeakArenaConsecutiveWinsMatchmakingScoreInfo")]
  [Serializable]
  public class PeakArenaConsecutiveWinsMatchmakingScoreInfo : IExtensible
  {
    private int _Count;
    private int _ScoreAdjust;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Count")]
    public int Count
    {
      get
      {
        return this._Count;
      }
      set
      {
        this._Count = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreAdjust")]
    public int ScoreAdjust
    {
      get
      {
        return this._ScoreAdjust;
      }
      set
      {
        this._ScoreAdjust = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
