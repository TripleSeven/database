﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAncientCallTotalBossPointsInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAncientCallTotalBossPointsInfo")]
  [Serializable]
  public class ConfigDataAncientCallTotalBossPointsInfo : IExtensible
  {
    private int _ID;
    private int _Ranking;
    private int _TotalRankingRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Ranking")]
    public int Ranking
    {
      get
      {
        return this._Ranking;
      }
      set
      {
        this._Ranking = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalRankingRewardMailTemplateId")]
    public int TotalRankingRewardMailTemplateId
    {
      get
      {
        return this._TotalRankingRewardMailTemplateId;
      }
      set
      {
        this._TotalRankingRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
