﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCooperateBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCooperateBattleInfo")]
  [Serializable]
  public class ConfigDataCooperateBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _TeamName;
    private string _Image;
    private string _BriefView;
    private List<int> _LevelList;
    private string _OpenHour;
    private string _CloseHour;
    private string _OpenHour2;
    private string _CloseHour2;
    private List<int> _OpenWeekDays;
    private IExtension extensionObject;
    public List<ConfigDataCooperateBattleLevelInfo> m_levelInfos;
    public TimeSpan OpenTimeSpan;
    public TimeSpan CloseTimeSpan;
    public TimeSpan OpenTimeSpan2;
    public TimeSpan CloseTimeSpan2;
    public List<DayOfWeek> OpenDaysOfWeek;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "BriefView")]
    public string BriefView
    {
      get
      {
        return this._BriefView;
      }
      set
      {
        this._BriefView = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "LevelList")]
    public List<int> LevelList
    {
      get
      {
        return this._LevelList;
      }
      set
      {
        this._LevelList = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenHour")]
    public string OpenHour
    {
      get
      {
        return this._OpenHour;
      }
      set
      {
        this._OpenHour = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "CloseHour")]
    public string CloseHour
    {
      get
      {
        return this._CloseHour;
      }
      set
      {
        this._CloseHour = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenHour2")]
    public string OpenHour2
    {
      get
      {
        return this._OpenHour2;
      }
      set
      {
        this._OpenHour2 = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "CloseHour2")]
    public string CloseHour2
    {
      get
      {
        return this._CloseHour2;
      }
      set
      {
        this._CloseHour2 = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "OpenWeekDays")]
    public List<int> OpenWeekDays
    {
      get
      {
        return this._OpenWeekDays;
      }
      set
      {
        this._OpenWeekDays = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
