﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.GiftType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "GiftType")]
  public enum GiftType
  {
    [ProtoEnum(Name = "GiftType_Normal", Value = 0)] GiftType_Normal,
    [ProtoEnum(Name = "GiftType_Event", Value = 1)] GiftType_Event,
  }
}
