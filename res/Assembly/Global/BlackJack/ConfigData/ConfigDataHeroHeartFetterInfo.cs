﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroHeartFetterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroHeartFetterInfo")]
  [Serializable]
  public class ConfigDataHeroHeartFetterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _MaxLevel;
    private List<HeroHeartFetterUnlockCondition> _UnlockConditions;
    private List<Goods> _UnlockReward;
    private List<int> _HeroHeartFetterSkills;
    private List<HeroFetterLevelUpCost> _LevelUpMaterials;
    private List<int> _LevelUpGold;
    private List<HeroHeartFetterUnlockSkill> _UnlockSkills_ID;
    private int _ShownSkillCount;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MaxLevel")]
    public int MaxLevel
    {
      get
      {
        return this._MaxLevel;
      }
      set
      {
        this._MaxLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "UnlockConditions")]
    public List<HeroHeartFetterUnlockCondition> UnlockConditions
    {
      get
      {
        return this._UnlockConditions;
      }
      set
      {
        this._UnlockConditions = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "UnlockReward")]
    public List<Goods> UnlockReward
    {
      get
      {
        return this._UnlockReward;
      }
      set
      {
        this._UnlockReward = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "HeroHeartFetterSkills")]
    public List<int> HeroHeartFetterSkills
    {
      get
      {
        return this._HeroHeartFetterSkills;
      }
      set
      {
        this._HeroHeartFetterSkills = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "LevelUpMaterials")]
    public List<HeroFetterLevelUpCost> LevelUpMaterials
    {
      get
      {
        return this._LevelUpMaterials;
      }
      set
      {
        this._LevelUpMaterials = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "LevelUpGold")]
    public List<int> LevelUpGold
    {
      get
      {
        return this._LevelUpGold;
      }
      set
      {
        this._LevelUpGold = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "UnlockSkills_ID")]
    public List<HeroHeartFetterUnlockSkill> UnlockSkills_ID
    {
      get
      {
        return this._UnlockSkills_ID;
      }
      set
      {
        this._UnlockSkills_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ShownSkillCount")]
    public int ShownSkillCount
    {
      get
      {
        return this._ShownSkillCount;
      }
      set
      {
        this._ShownSkillCount = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
