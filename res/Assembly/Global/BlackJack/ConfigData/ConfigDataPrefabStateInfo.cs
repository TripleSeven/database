﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPrefabStateInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPrefabStateInfo")]
  [Serializable]
  public class ConfigDataPrefabStateInfo : IExtensible
  {
    private int _ID;
    private string _Prefab;
    private string _State;
    private bool _IsLoop;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Prefab")]
    public string Prefab
    {
      get
      {
        return this._Prefab;
      }
      set
      {
        this._Prefab = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "State")]
    public string State
    {
      get
      {
        return this._State;
      }
      set
      {
        this._State = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsLoop")]
    public bool IsLoop
    {
      get
      {
        return this._IsLoop;
      }
      set
      {
        this._IsLoop = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
