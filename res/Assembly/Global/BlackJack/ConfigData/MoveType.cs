﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MoveType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MoveType")]
  public enum MoveType
  {
    [ProtoEnum(Name = "MoveType_None", Value = 0)] MoveType_None,
    [ProtoEnum(Name = "MoveType_Ride", Value = 1)] MoveType_Ride,
    [ProtoEnum(Name = "MoveType_Walk", Value = 2)] MoveType_Walk,
    [ProtoEnum(Name = "MoveType_Water", Value = 3)] MoveType_Water,
    [ProtoEnum(Name = "MoveType_Fly", Value = 4)] MoveType_Fly,
    [ProtoEnum(Name = "MoveType_FieldArmy", Value = 5)] MoveType_FieldArmy,
  }
}
