﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTranslate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTranslate")]
  [Serializable]
  public class ConfigDataTranslate : IExtensible
  {
    private int _ID;
    private string _Chinese;
    private string _TargetLanguage;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Chinese")]
    public string Chinese
    {
      get
      {
        return this._Chinese;
      }
      set
      {
        this._Chinese = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "TargetLanguage")]
    public string TargetLanguage
    {
      get
      {
        return this._TargetLanguage;
      }
      set
      {
        this._TargetLanguage = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
