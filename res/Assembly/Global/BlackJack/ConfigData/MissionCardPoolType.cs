﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.MissionCardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "MissionCardPoolType")]
  public enum MissionCardPoolType
  {
    [ProtoEnum(Name = "MissionCardPoolType_None", Value = 0)] MissionCardPoolType_None,
    [ProtoEnum(Name = "MissionCardPoolType_Free", Value = 1)] MissionCardPoolType_Free,
    [ProtoEnum(Name = "MissionCardPoolType_Hero", Value = 2)] MissionCardPoolType_Hero,
    [ProtoEnum(Name = "MissionCardPoolType_Equipment", Value = 3)] MissionCardPoolType_Equipment,
  }
}
