﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RealTimePVPConsecutiveLosses
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RealTimePVPConsecutiveLosses")]
  [Serializable]
  public class RealTimePVPConsecutiveLosses : IExtensible
  {
    private int _ConsecutiveLosses;
    private int _ScoreProtection;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsecutiveLosses")]
    public int ConsecutiveLosses
    {
      get
      {
        return this._ConsecutiveLosses;
      }
      set
      {
        this._ConsecutiveLosses = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreProtection")]
    public int ScoreProtection
    {
      get
      {
        return this._ScoreProtection;
      }
      set
      {
        this._ScoreProtection = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
