﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroPerformanceUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroPerformanceUnlockConditionType")]
  public enum HeroPerformanceUnlockConditionType
  {
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_None", Value = 0)] HeroPerformanceUnlockConditionType_None,
    [ProtoEnum(Name = "HeroPerformanceUnlockConditionType_HeroFavourabilityLevel", Value = 1)] HeroPerformanceUnlockConditionType_HeroFavourabilityLevel,
  }
}
