﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.FlushRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "FlushRuleType")]
  public enum FlushRuleType
  {
    [ProtoEnum(Name = "FlushRuleType_None", Value = 0)] FlushRuleType_None,
    [ProtoEnum(Name = "FlushRuleType_Period", Value = 1)] FlushRuleType_Period,
    [ProtoEnum(Name = "FlushRuleType_FixedTime", Value = 2)] FlushRuleType_FixedTime,
  }
}
