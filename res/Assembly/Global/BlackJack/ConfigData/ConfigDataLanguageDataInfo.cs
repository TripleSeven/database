﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataLanguageDataInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataLanguageDataInfo")]
  [Serializable]
  public class ConfigDataLanguageDataInfo : IExtensible
  {
    private int _ID;
    private string _SystemLanguage;
    private string _LanguageAbbr;
    private string _LanguageName;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "SystemLanguage")]
    public string SystemLanguage
    {
      get
      {
        return this._SystemLanguage;
      }
      set
      {
        this._SystemLanguage = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "LanguageAbbr")]
    public string LanguageAbbr
    {
      get
      {
        return this._LanguageAbbr;
      }
      set
      {
        this._LanguageAbbr = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "LanguageName")]
    public string LanguageName
    {
      get
      {
        return this._LanguageName;
      }
      set
      {
        this._LanguageName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
