﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.WaypointInfoStateList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "WaypointInfoStateList")]
  [Serializable]
  public class WaypointInfoStateList : IExtensible
  {
    private int _ScenarioId;
    private string _StateName;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScenarioId")]
    public int ScenarioId
    {
      get
      {
        return this._ScenarioId;
      }
      set
      {
        this._ScenarioId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "StateName")]
    public string StateName
    {
      get
      {
        return this._StateName;
      }
      set
      {
        this._StateName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
