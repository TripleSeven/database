﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSkillInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSkillInfo")]
  [Serializable]
  public class ConfigDataSkillInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private bool _IsMagic;
    private bool _IsActiveSkill;
    private bool _IsSupport;
    private bool _IsRangeSkill;
    private bool _AIIsRangeSkill;
    private bool _IsNewTurn;
    private int _NewTurnMovePoint;
    private List<int> _LimitArmys_ID;
    private List<int> _PassiveBuffs_ID;
    private List<int> _SelfBuffs_ID;
    private SkillAIType _SkillAIType;
    private int _TargetWithNParam;
    private int _SkillTargetAIType;
    private SkillType _SkillType;
    private int _SkillTypeParam1;
    private int _SkillTypeParam2;
    private int _SkillTypeParam3;
    private int _Time_Sing;
    private int _Power;
    private int _AttackDistance;
    private int _HitCountMax;
    private int _AttackCount;
    private int _Time_NextAttack;
    private List<int> _Buffs_ID;
    private int _BuffRate;
    private int _BuffNum;
    private int _HeroMoveDelay;
    private int _Time_EffCast1;
    private int _Time_EffCast2;
    private int _Time_EffMagic1;
    private int _Time_EffMagic2;
    private int _Time_Hit;
    private int _Time_End;
    private List<int> _Time_MultiHit;
    private string _Anim_Cast;
    private string _Effect_Sing;
    private string _Effect_Cast1;
    private string _Effect_Cast1_Far;
    private string _Effect_Cast2;
    private string _Effect_Cast2_Far;
    private string _Effect_Magic1;
    private string _Effect_Magic1_Far;
    private string _Effect_Magic2;
    private string _Effect_Magic2_Far;
    private string _Effect_Hit;
    private string _Effect_PreCast;
    private int _CutsceneType;
    private int _DeadAnimType;
    private int _BF_InitCooldown;
    private int _BF_Cooldown;
    private int _BF_Distance;
    private int _BF_Range;
    private int _BF_RangeShape;
    private SkillTargetType _BF_TargetFaction;
    private SkillTargetType _BF_TargetType;
    private int _CastSkillShape;
    private string _Icon;
    private int _SkillCost;
    private string _TypeText;
    private string _CDText;
    private string _DistanceText;
    private string _RangeText;
    private int _BattlePower;
    private IExtension extensionObject;
    public ConfigDataBuffInfo[] m_buffInfos;
    public ConfigDataBuffInfo[] m_passiveBuffInfos;
    public ConfigDataBuffInfo[] m_selfBuffInfos;
    public bool m_isNormalAttack;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsMagic")]
    public bool IsMagic
    {
      get
      {
        return this._IsMagic;
      }
      set
      {
        this._IsMagic = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsActiveSkill")]
    public bool IsActiveSkill
    {
      get
      {
        return this._IsActiveSkill;
      }
      set
      {
        this._IsActiveSkill = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsSupport")]
    public bool IsSupport
    {
      get
      {
        return this._IsSupport;
      }
      set
      {
        this._IsSupport = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsRangeSkill")]
    public bool IsRangeSkill
    {
      get
      {
        return this._IsRangeSkill;
      }
      set
      {
        this._IsRangeSkill = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "AIIsRangeSkill")]
    public bool AIIsRangeSkill
    {
      get
      {
        return this._AIIsRangeSkill;
      }
      set
      {
        this._AIIsRangeSkill = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsNewTurn")]
    public bool IsNewTurn
    {
      get
      {
        return this._IsNewTurn;
      }
      set
      {
        this._IsNewTurn = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NewTurnMovePoint")]
    public int NewTurnMovePoint
    {
      get
      {
        return this._NewTurnMovePoint;
      }
      set
      {
        this._NewTurnMovePoint = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "LimitArmys_ID")]
    public List<int> LimitArmys_ID
    {
      get
      {
        return this._LimitArmys_ID;
      }
      set
      {
        this._LimitArmys_ID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, Name = "PassiveBuffs_ID")]
    public List<int> PassiveBuffs_ID
    {
      get
      {
        return this._PassiveBuffs_ID;
      }
      set
      {
        this._PassiveBuffs_ID = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, Name = "SelfBuffs_ID")]
    public List<int> SelfBuffs_ID
    {
      get
      {
        return this._SelfBuffs_ID;
      }
      set
      {
        this._SelfBuffs_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillAIType")]
    public SkillAIType SkillAIType
    {
      get
      {
        return this._SkillAIType;
      }
      set
      {
        this._SkillAIType = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TargetWithNParam")]
    public int TargetWithNParam
    {
      get
      {
        return this._TargetWithNParam;
      }
      set
      {
        this._TargetWithNParam = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillTargetAIType")]
    public int SkillTargetAIType
    {
      get
      {
        return this._SkillTargetAIType;
      }
      set
      {
        this._SkillTargetAIType = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillType")]
    public SkillType SkillType
    {
      get
      {
        return this._SkillType;
      }
      set
      {
        this._SkillType = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillTypeParam1")]
    public int SkillTypeParam1
    {
      get
      {
        return this._SkillTypeParam1;
      }
      set
      {
        this._SkillTypeParam1 = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillTypeParam2")]
    public int SkillTypeParam2
    {
      get
      {
        return this._SkillTypeParam2;
      }
      set
      {
        this._SkillTypeParam2 = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillTypeParam3")]
    public int SkillTypeParam3
    {
      get
      {
        return this._SkillTypeParam3;
      }
      set
      {
        this._SkillTypeParam3 = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_Sing")]
    public int Time_Sing
    {
      get
      {
        return this._Time_Sing;
      }
      set
      {
        this._Time_Sing = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Power")]
    public int Power
    {
      get
      {
        return this._Power;
      }
      set
      {
        this._Power = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AttackDistance")]
    public int AttackDistance
    {
      get
      {
        return this._AttackDistance;
      }
      set
      {
        this._AttackDistance = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HitCountMax")]
    public int HitCountMax
    {
      get
      {
        return this._HitCountMax;
      }
      set
      {
        this._HitCountMax = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AttackCount")]
    public int AttackCount
    {
      get
      {
        return this._AttackCount;
      }
      set
      {
        this._AttackCount = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_NextAttack")]
    public int Time_NextAttack
    {
      get
      {
        return this._Time_NextAttack;
      }
      set
      {
        this._Time_NextAttack = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, Name = "Buffs_ID")]
    public List<int> Buffs_ID
    {
      get
      {
        return this._Buffs_ID;
      }
      set
      {
        this._Buffs_ID = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffRate")]
    public int BuffRate
    {
      get
      {
        return this._BuffRate;
      }
      set
      {
        this._BuffRate = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuffNum")]
    public int BuffNum
    {
      get
      {
        return this._BuffNum;
      }
      set
      {
        this._BuffNum = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroMoveDelay")]
    public int HeroMoveDelay
    {
      get
      {
        return this._HeroMoveDelay;
      }
      set
      {
        this._HeroMoveDelay = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_EffCast1")]
    public int Time_EffCast1
    {
      get
      {
        return this._Time_EffCast1;
      }
      set
      {
        this._Time_EffCast1 = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_EffCast2")]
    public int Time_EffCast2
    {
      get
      {
        return this._Time_EffCast2;
      }
      set
      {
        this._Time_EffCast2 = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_EffMagic1")]
    public int Time_EffMagic1
    {
      get
      {
        return this._Time_EffMagic1;
      }
      set
      {
        this._Time_EffMagic1 = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_EffMagic2")]
    public int Time_EffMagic2
    {
      get
      {
        return this._Time_EffMagic2;
      }
      set
      {
        this._Time_EffMagic2 = value;
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_Hit")]
    public int Time_Hit
    {
      get
      {
        return this._Time_Hit;
      }
      set
      {
        this._Time_Hit = value;
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time_End")]
    public int Time_End
    {
      get
      {
        return this._Time_End;
      }
      set
      {
        this._Time_End = value;
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.TwosComplement, Name = "Time_MultiHit")]
    public List<int> Time_MultiHit
    {
      get
      {
        return this._Time_MultiHit;
      }
      set
      {
        this._Time_MultiHit = value;
      }
    }

    [ProtoMember(41, DataFormat = DataFormat.Default, IsRequired = true, Name = "Anim_Cast")]
    public string Anim_Cast
    {
      get
      {
        return this._Anim_Cast;
      }
      set
      {
        this._Anim_Cast = value;
      }
    }

    [ProtoMember(42, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Sing")]
    public string Effect_Sing
    {
      get
      {
        return this._Effect_Sing;
      }
      set
      {
        this._Effect_Sing = value;
      }
    }

    [ProtoMember(43, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Cast1")]
    public string Effect_Cast1
    {
      get
      {
        return this._Effect_Cast1;
      }
      set
      {
        this._Effect_Cast1 = value;
      }
    }

    [ProtoMember(44, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Cast1_Far")]
    public string Effect_Cast1_Far
    {
      get
      {
        return this._Effect_Cast1_Far;
      }
      set
      {
        this._Effect_Cast1_Far = value;
      }
    }

    [ProtoMember(45, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Cast2")]
    public string Effect_Cast2
    {
      get
      {
        return this._Effect_Cast2;
      }
      set
      {
        this._Effect_Cast2 = value;
      }
    }

    [ProtoMember(46, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Cast2_Far")]
    public string Effect_Cast2_Far
    {
      get
      {
        return this._Effect_Cast2_Far;
      }
      set
      {
        this._Effect_Cast2_Far = value;
      }
    }

    [ProtoMember(47, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Magic1")]
    public string Effect_Magic1
    {
      get
      {
        return this._Effect_Magic1;
      }
      set
      {
        this._Effect_Magic1 = value;
      }
    }

    [ProtoMember(48, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Magic1_Far")]
    public string Effect_Magic1_Far
    {
      get
      {
        return this._Effect_Magic1_Far;
      }
      set
      {
        this._Effect_Magic1_Far = value;
      }
    }

    [ProtoMember(49, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Magic2")]
    public string Effect_Magic2
    {
      get
      {
        return this._Effect_Magic2;
      }
      set
      {
        this._Effect_Magic2 = value;
      }
    }

    [ProtoMember(50, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Magic2_Far")]
    public string Effect_Magic2_Far
    {
      get
      {
        return this._Effect_Magic2_Far;
      }
      set
      {
        this._Effect_Magic2_Far = value;
      }
    }

    [ProtoMember(51, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_Hit")]
    public string Effect_Hit
    {
      get
      {
        return this._Effect_Hit;
      }
      set
      {
        this._Effect_Hit = value;
      }
    }

    [ProtoMember(52, DataFormat = DataFormat.Default, IsRequired = true, Name = "Effect_PreCast")]
    public string Effect_PreCast
    {
      get
      {
        return this._Effect_PreCast;
      }
      set
      {
        this._Effect_PreCast = value;
      }
    }

    [ProtoMember(53, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CutsceneType")]
    public int CutsceneType
    {
      get
      {
        return this._CutsceneType;
      }
      set
      {
        this._CutsceneType = value;
      }
    }

    [ProtoMember(54, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DeadAnimType")]
    public int DeadAnimType
    {
      get
      {
        return this._DeadAnimType;
      }
      set
      {
        this._DeadAnimType = value;
      }
    }

    [ProtoMember(55, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_InitCooldown")]
    public int BF_InitCooldown
    {
      get
      {
        return this._BF_InitCooldown;
      }
      set
      {
        this._BF_InitCooldown = value;
      }
    }

    [ProtoMember(56, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_Cooldown")]
    public int BF_Cooldown
    {
      get
      {
        return this._BF_Cooldown;
      }
      set
      {
        this._BF_Cooldown = value;
      }
    }

    [ProtoMember(57, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_Distance")]
    public int BF_Distance
    {
      get
      {
        return this._BF_Distance;
      }
      set
      {
        this._BF_Distance = value;
      }
    }

    [ProtoMember(58, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_Range")]
    public int BF_Range
    {
      get
      {
        return this._BF_Range;
      }
      set
      {
        this._BF_Range = value;
      }
    }

    [ProtoMember(59, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_RangeShape")]
    public int BF_RangeShape
    {
      get
      {
        return this._BF_RangeShape;
      }
      set
      {
        this._BF_RangeShape = value;
      }
    }

    [ProtoMember(60, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_TargetFaction")]
    public SkillTargetType BF_TargetFaction
    {
      get
      {
        return this._BF_TargetFaction;
      }
      set
      {
        this._BF_TargetFaction = value;
      }
    }

    [ProtoMember(61, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BF_TargetType")]
    public SkillTargetType BF_TargetType
    {
      get
      {
        return this._BF_TargetType;
      }
      set
      {
        this._BF_TargetType = value;
      }
    }

    [ProtoMember(62, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CastSkillShape")]
    public int CastSkillShape
    {
      get
      {
        return this._CastSkillShape;
      }
      set
      {
        this._CastSkillShape = value;
      }
    }

    [ProtoMember(63, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(64, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillCost")]
    public int SkillCost
    {
      get
      {
        return this._SkillCost;
      }
      set
      {
        this._SkillCost = value;
      }
    }

    [ProtoMember(65, DataFormat = DataFormat.Default, IsRequired = true, Name = "TypeText")]
    public string TypeText
    {
      get
      {
        return this._TypeText;
      }
      set
      {
        this._TypeText = value;
      }
    }

    [ProtoMember(66, DataFormat = DataFormat.Default, IsRequired = true, Name = "CDText")]
    public string CDText
    {
      get
      {
        return this._CDText;
      }
      set
      {
        this._CDText = value;
      }
    }

    [ProtoMember(67, DataFormat = DataFormat.Default, IsRequired = true, Name = "DistanceText")]
    public string DistanceText
    {
      get
      {
        return this._DistanceText;
      }
      set
      {
        this._DistanceText = value;
      }
    }

    [ProtoMember(68, DataFormat = DataFormat.Default, IsRequired = true, Name = "RangeText")]
    public string RangeText
    {
      get
      {
        return this._RangeText;
      }
      set
      {
        this._RangeText = value;
      }
    }

    [ProtoMember(69, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattlePower")]
    public int BattlePower
    {
      get
      {
        return this._BattlePower;
      }
      set
      {
        this._BattlePower = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public bool IsNormalAttack()
    {
      return this.m_isNormalAttack;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattlefieldSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDamageSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPhysicalDamageSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMagicDamageSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHealSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsSummonSkill()
    {
      return this.SkillType == SkillType.SkillType_BF_Summon;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnySummonSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsTeleportSkill()
    {
      return this.SkillType == SkillType.SkillType_BF_Teleport;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyTeleportSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPassiveSkill()
    {
      return this.SkillType == SkillType.SkillType_Passive;
    }

    public bool IsCombineSkill()
    {
      return this.SkillType == SkillType.SkillType_BF_SuperMagicDamage;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanDispelBuff()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
