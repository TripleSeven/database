﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.JobConnection2SkinResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "JobConnection2SkinResource")]
  [Serializable]
  public class JobConnection2SkinResource : IExtensible
  {
    private int _JobConnectionId;
    private int _SkinResourceId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JobConnectionId")]
    public int JobConnectionId
    {
      get
      {
        return this._JobConnectionId;
      }
      set
      {
        this._JobConnectionId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkinResourceId")]
    public int SkinResourceId
    {
      get
      {
        return this._SkinResourceId;
      }
      set
      {
        this._SkinResourceId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
