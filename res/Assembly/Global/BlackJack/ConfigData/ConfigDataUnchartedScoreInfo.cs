﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUnchartedScoreInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUnchartedScoreInfo")]
  [Serializable]
  public class ConfigDataUnchartedScoreInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _PageImage;
    private string _UIState;
    private string _BackgroundImage;
    private string _ScoreName;
    private List<Int32Pair> _BonusHeroIdList;
    private int _UnchartedScoreRewardGroupId;
    private int _ScoreItemId;
    private List<int> _ChallengeLevelIdList;
    private List<int> _ScoreLevelIdList;
    private int _ScoreLevelBonusCount;
    private int _ScoreLevelBonus;
    private List<int> _ModelIdList;
    private IExtension extensionObject;
    public Dictionary<int, int> HeroId2Bonus;
    public Dictionary<int, ConfigDataChallengeLevelInfo> Id2ChallengeLevelInfos;
    public Dictionary<int, ConfigDataScoreLevelInfo> Id2ScoreLevelInfos;
    public Dictionary<int, List<Goods>> Score2GoodsList;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "PageImage")]
    public string PageImage
    {
      get
      {
        return this._PageImage;
      }
      set
      {
        this._PageImage = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "UIState")]
    public string UIState
    {
      get
      {
        return this._UIState;
      }
      set
      {
        this._UIState = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "BackgroundImage")]
    public string BackgroundImage
    {
      get
      {
        return this._BackgroundImage;
      }
      set
      {
        this._BackgroundImage = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "ScoreName")]
    public string ScoreName
    {
      get
      {
        return this._ScoreName;
      }
      set
      {
        this._ScoreName = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "BonusHeroIdList")]
    public List<Int32Pair> BonusHeroIdList
    {
      get
      {
        return this._BonusHeroIdList;
      }
      set
      {
        this._BonusHeroIdList = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UnchartedScoreRewardGroupId")]
    public int UnchartedScoreRewardGroupId
    {
      get
      {
        return this._UnchartedScoreRewardGroupId;
      }
      set
      {
        this._UnchartedScoreRewardGroupId = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreItemId")]
    public int ScoreItemId
    {
      get
      {
        return this._ScoreItemId;
      }
      set
      {
        this._ScoreItemId = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "ChallengeLevelIdList")]
    public List<int> ChallengeLevelIdList
    {
      get
      {
        return this._ChallengeLevelIdList;
      }
      set
      {
        this._ChallengeLevelIdList = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, Name = "ScoreLevelIdList")]
    public List<int> ScoreLevelIdList
    {
      get
      {
        return this._ScoreLevelIdList;
      }
      set
      {
        this._ScoreLevelIdList = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreLevelBonusCount")]
    public int ScoreLevelBonusCount
    {
      get
      {
        return this._ScoreLevelBonusCount;
      }
      set
      {
        this._ScoreLevelBonusCount = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreLevelBonus")]
    public int ScoreLevelBonus
    {
      get
      {
        return this._ScoreLevelBonus;
      }
      set
      {
        this._ScoreLevelBonus = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, Name = "ModelIdList")]
    public List<int> ModelIdList
    {
      get
      {
        return this._ModelIdList;
      }
      set
      {
        this._ModelIdList = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
