﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroAssistantTaskInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroAssistantTaskInfo")]
  [Serializable]
  public class ConfigDataHeroAssistantTaskInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<string> _Resource;
    private int _RequiredUserLevel;
    private int _CompletePoints;
    private List<CompleteValueDropID> _Rewards;
    private IExtension extensionObject;
    public ConfigDataHeroAssistantTaskScheduleInfo m_schedule;
    public ConfigDataHeroAssistantTaskGeneralInfo m_general;
    public List<int> m_rewardCompleteRate;
    public List<int> m_rewardDropId;
    public List<int> m_rewardWorkSeconds;
    public List<int> m_rewardDropCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Resource")]
    public List<string> Resource
    {
      get
      {
        return this._Resource;
      }
      set
      {
        this._Resource = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RequiredUserLevel")]
    public int RequiredUserLevel
    {
      get
      {
        return this._RequiredUserLevel;
      }
      set
      {
        this._RequiredUserLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CompletePoints")]
    public int CompletePoints
    {
      get
      {
        return this._CompletePoints;
      }
      set
      {
        this._CompletePoints = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Rewards")]
    public List<CompleteValueDropID> Rewards
    {
      get
      {
        return this._Rewards;
      }
      set
      {
        this._Rewards = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
