﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RiftLevelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RiftLevelType")]
  public enum RiftLevelType
  {
    [ProtoEnum(Name = "RiftLevelType_None", Value = 0)] RiftLevelType_None,
    [ProtoEnum(Name = "RiftLevelType_Scenario", Value = 1)] RiftLevelType_Scenario,
    [ProtoEnum(Name = "RiftLevelType_Event", Value = 2)] RiftLevelType_Event,
  }
}
