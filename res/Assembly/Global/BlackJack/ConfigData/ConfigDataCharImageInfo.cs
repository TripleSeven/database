﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCharImageInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCharImageInfo")]
  [Serializable]
  public class ConfigDataCharImageInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Spine;
    private string _HeroPainting;
    private string _Image;
    private int _Direction;
    private string _CVName;
    private int _HeroDetailScale;
    private int _HeroDetailYOffset;
    private int _HeroFetterYOffset;
    private int _HeroConfessionYOffset;
    private int _HeroConfessionEndingYOffset;
    private int _HeroConfessionEndingScaleOffset;
    private int _CombatScale;
    private int _CombatYOffset;
    private int _BattleDialogScale;
    private int _BattleDialogYOffset;
    private int _DialogScale;
    private int _DialogYOffset;
    private string _RoundHeadImage;
    private string _SummonHeadImage;
    private string _CardHeadImage;
    private string _SmallHeadImage;
    private int _SkillCutscene_ID;
    private string _Sound_Die;
    private List<PerformanceEffect> _Performances;
    private List<PerformanceEffect> _BreakPerformances;
    private List<PerformanceEffect> _SummonPerformances;
    private List<PerformanceEffect> _JobTransferPerformances;
    private string _BattleActionVoice1;
    private string _BattleActionVoice2;
    private string _BattleActionVoice3;
    private List<FavourabilityPerformance> _NewPerformances;
    private List<FavourabilityPerformance> _NewBreakPerformances;
    private List<FavourabilityPerformance> _NewSummonPerformances;
    private List<FavourabilityPerformance> _NewJobTransferPerformances;
    private IExtension extensionObject;
    public ConfigDataCutsceneInfo m_skillCutsceneInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Spine")]
    public string Spine
    {
      get
      {
        return this._Spine;
      }
      set
      {
        this._Spine = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "HeroPainting")]
    public string HeroPainting
    {
      get
      {
        return this._HeroPainting;
      }
      set
      {
        this._HeroPainting = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Image")]
    public string Image
    {
      get
      {
        return this._Image;
      }
      set
      {
        this._Image = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Direction")]
    public int Direction
    {
      get
      {
        return this._Direction;
      }
      set
      {
        this._Direction = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "CVName")]
    public string CVName
    {
      get
      {
        return this._CVName;
      }
      set
      {
        this._CVName = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroDetailScale")]
    public int HeroDetailScale
    {
      get
      {
        return this._HeroDetailScale;
      }
      set
      {
        this._HeroDetailScale = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroDetailYOffset")]
    public int HeroDetailYOffset
    {
      get
      {
        return this._HeroDetailYOffset;
      }
      set
      {
        this._HeroDetailYOffset = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroFetterYOffset")]
    public int HeroFetterYOffset
    {
      get
      {
        return this._HeroFetterYOffset;
      }
      set
      {
        this._HeroFetterYOffset = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroConfessionYOffset")]
    public int HeroConfessionYOffset
    {
      get
      {
        return this._HeroConfessionYOffset;
      }
      set
      {
        this._HeroConfessionYOffset = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroConfessionEndingYOffset")]
    public int HeroConfessionEndingYOffset
    {
      get
      {
        return this._HeroConfessionEndingYOffset;
      }
      set
      {
        this._HeroConfessionEndingYOffset = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroConfessionEndingScaleOffset")]
    public int HeroConfessionEndingScaleOffset
    {
      get
      {
        return this._HeroConfessionEndingScaleOffset;
      }
      set
      {
        this._HeroConfessionEndingScaleOffset = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CombatScale")]
    public int CombatScale
    {
      get
      {
        return this._CombatScale;
      }
      set
      {
        this._CombatScale = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CombatYOffset")]
    public int CombatYOffset
    {
      get
      {
        return this._CombatYOffset;
      }
      set
      {
        this._CombatYOffset = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleDialogScale")]
    public int BattleDialogScale
    {
      get
      {
        return this._BattleDialogScale;
      }
      set
      {
        this._BattleDialogScale = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleDialogYOffset")]
    public int BattleDialogYOffset
    {
      get
      {
        return this._BattleDialogYOffset;
      }
      set
      {
        this._BattleDialogYOffset = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogScale")]
    public int DialogScale
    {
      get
      {
        return this._DialogScale;
      }
      set
      {
        this._DialogScale = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogYOffset")]
    public int DialogYOffset
    {
      get
      {
        return this._DialogYOffset;
      }
      set
      {
        this._DialogYOffset = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "RoundHeadImage")]
    public string RoundHeadImage
    {
      get
      {
        return this._RoundHeadImage;
      }
      set
      {
        this._RoundHeadImage = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "SummonHeadImage")]
    public string SummonHeadImage
    {
      get
      {
        return this._SummonHeadImage;
      }
      set
      {
        this._SummonHeadImage = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, IsRequired = true, Name = "CardHeadImage")]
    public string CardHeadImage
    {
      get
      {
        return this._CardHeadImage;
      }
      set
      {
        this._CardHeadImage = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, IsRequired = true, Name = "SmallHeadImage")]
    public string SmallHeadImage
    {
      get
      {
        return this._SmallHeadImage;
      }
      set
      {
        this._SmallHeadImage = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillCutscene_ID")]
    public int SkillCutscene_ID
    {
      get
      {
        return this._SkillCutscene_ID;
      }
      set
      {
        this._SkillCutscene_ID = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound_Die")]
    public string Sound_Die
    {
      get
      {
        return this._Sound_Die;
      }
      set
      {
        this._Sound_Die = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.Default, Name = "Performances")]
    public List<PerformanceEffect> Performances
    {
      get
      {
        return this._Performances;
      }
      set
      {
        this._Performances = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.Default, Name = "BreakPerformances")]
    public List<PerformanceEffect> BreakPerformances
    {
      get
      {
        return this._BreakPerformances;
      }
      set
      {
        this._BreakPerformances = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.Default, Name = "SummonPerformances")]
    public List<PerformanceEffect> SummonPerformances
    {
      get
      {
        return this._SummonPerformances;
      }
      set
      {
        this._SummonPerformances = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.Default, Name = "JobTransferPerformances")]
    public List<PerformanceEffect> JobTransferPerformances
    {
      get
      {
        return this._JobTransferPerformances;
      }
      set
      {
        this._JobTransferPerformances = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleActionVoice1")]
    public string BattleActionVoice1
    {
      get
      {
        return this._BattleActionVoice1;
      }
      set
      {
        this._BattleActionVoice1 = value;
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleActionVoice2")]
    public string BattleActionVoice2
    {
      get
      {
        return this._BattleActionVoice2;
      }
      set
      {
        this._BattleActionVoice2 = value;
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleActionVoice3")]
    public string BattleActionVoice3
    {
      get
      {
        return this._BattleActionVoice3;
      }
      set
      {
        this._BattleActionVoice3 = value;
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.Default, Name = "NewPerformances")]
    public List<FavourabilityPerformance> NewPerformances
    {
      get
      {
        return this._NewPerformances;
      }
      set
      {
        this._NewPerformances = value;
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.Default, Name = "NewBreakPerformances")]
    public List<FavourabilityPerformance> NewBreakPerformances
    {
      get
      {
        return this._NewBreakPerformances;
      }
      set
      {
        this._NewBreakPerformances = value;
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.Default, Name = "NewSummonPerformances")]
    public List<FavourabilityPerformance> NewSummonPerformances
    {
      get
      {
        return this._NewSummonPerformances;
      }
      set
      {
        this._NewSummonPerformances = value;
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.Default, Name = "NewJobTransferPerformances")]
    public List<FavourabilityPerformance> NewJobTransferPerformances
    {
      get
      {
        return this._NewJobTransferPerformances;
      }
      set
      {
        this._NewJobTransferPerformances = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
