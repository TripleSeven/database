﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataDeviceSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataDeviceSetting")]
  [Serializable]
  public class ConfigDataDeviceSetting : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _DeviceModel;
    private bool _MarginFixHorizontal;
    private bool _MarginFixVertical;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "DeviceModel")]
    public string DeviceModel
    {
      get
      {
        return this._DeviceModel;
      }
      set
      {
        this._DeviceModel = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "MarginFixHorizontal")]
    public bool MarginFixHorizontal
    {
      get
      {
        return this._MarginFixHorizontal;
      }
      set
      {
        this._MarginFixHorizontal = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "MarginFixVertical")]
    public bool MarginFixVertical
    {
      get
      {
        return this._MarginFixVertical;
      }
      set
      {
        this._MarginFixVertical = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
