﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUserGuideDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUserGuideDialogInfo")]
  [Serializable]
  public class ConfigDataUserGuideDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextDialog_ID;
    private int _FrameType;
    private string _CharName;
    private int _CharImage_ID;
    private int _Position;
    private int _EnterType;
    private string _PreAnimation;
    private string _PreFacialAnimation;
    private string _IdleAnimation;
    private string _IdleFacialAnimation;
    private string _Voice;
    private string _Words;
    private string _WordsKey;
    private IExtension extensionObject;
    public ConfigDataUserGuideDialogInfo m_nextDialogInfo;
    public ConfigDataCharImageInfo m_charImageInfo;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextDialog_ID")]
    public int NextDialog_ID
    {
      get
      {
        return this._NextDialog_ID;
      }
      set
      {
        this._NextDialog_ID = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FrameType")]
    public int FrameType
    {
      get
      {
        return this._FrameType;
      }
      set
      {
        this._FrameType = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "CharName")]
    public string CharName
    {
      get
      {
        return this._CharName;
      }
      set
      {
        this._CharName = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImage_ID")]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Position")]
    public int Position
    {
      get
      {
        return this._Position;
      }
      set
      {
        this._Position = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnterType")]
    public int EnterType
    {
      get
      {
        return this._EnterType;
      }
      set
      {
        this._EnterType = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "PreAnimation")]
    public string PreAnimation
    {
      get
      {
        return this._PreAnimation;
      }
      set
      {
        this._PreAnimation = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "PreFacialAnimation")]
    public string PreFacialAnimation
    {
      get
      {
        return this._PreFacialAnimation;
      }
      set
      {
        this._PreFacialAnimation = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "IdleAnimation")]
    public string IdleAnimation
    {
      get
      {
        return this._IdleAnimation;
      }
      set
      {
        this._IdleAnimation = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "IdleFacialAnimation")]
    public string IdleFacialAnimation
    {
      get
      {
        return this._IdleFacialAnimation;
      }
      set
      {
        this._IdleFacialAnimation = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "Voice")]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "Words")]
    public string Words
    {
      get
      {
        return this._Words;
      }
      set
      {
        this._Words = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "WordsKey")]
    public string WordsKey
    {
      get
      {
        return this._WordsKey;
      }
      set
      {
        this._WordsKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
