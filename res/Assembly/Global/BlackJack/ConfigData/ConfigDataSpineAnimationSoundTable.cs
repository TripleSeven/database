﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataSpineAnimationSoundTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataSpineAnimationSoundTable")]
  [Serializable]
  public class ConfigDataSpineAnimationSoundTable : IExtensible
  {
    private int _ID;
    private string _SpineDataName;
    private string _AnimationName;
    private string _EventName;
    private string _SoundName;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "SpineDataName")]
    public string SpineDataName
    {
      get
      {
        return this._SpineDataName;
      }
      set
      {
        this._SpineDataName = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "AnimationName")]
    public string AnimationName
    {
      get
      {
        return this._AnimationName;
      }
      set
      {
        this._AnimationName = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "EventName")]
    public string EventName
    {
      get
      {
        return this._EventName;
      }
      set
      {
        this._EventName = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "SoundName")]
    public string SoundName
    {
      get
      {
        return this._SoundName;
      }
      set
      {
        this._SoundName = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
