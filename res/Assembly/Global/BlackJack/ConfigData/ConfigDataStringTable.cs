﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataStringTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataStringTable")]
  [Serializable]
  public class ConfigDataStringTable : IExtensible
  {
    private int _ID;
    private string _Value;
    private string _ValueStrKey;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Value")]
    public string Value
    {
      get
      {
        return this._Value;
      }
      set
      {
        this._Value = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "ValueStrKey")]
    public string ValueStrKey
    {
      get
      {
        return this._ValueStrKey;
      }
      set
      {
        this._ValueStrKey = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
