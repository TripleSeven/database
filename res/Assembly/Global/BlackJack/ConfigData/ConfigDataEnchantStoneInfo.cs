﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEnchantStoneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEnchantStoneInfo")]
  [Serializable]
  public class ConfigDataEnchantStoneInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rank;
    private int _SellGold;
    private ItemDisplayType _DisplayType;
    private string _Icon;
    private List<GetPathData> _GetPathList;
    private string _GetPathDesc;
    private int _AlchemyGold;
    private int _RandomDropRewardID;
    private int _DisplayRewardCount;
    private int _Resonance_ID;
    private int _CostGold;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rank")]
    public int Rank
    {
      get
      {
        return this._Rank;
      }
      set
      {
        this._Rank = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SellGold")]
    public int SellGold
    {
      get
      {
        return this._SellGold;
      }
      set
      {
        this._SellGold = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayType")]
    public ItemDisplayType DisplayType
    {
      get
      {
        return this._DisplayType;
      }
      set
      {
        this._DisplayType = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "GetPathList")]
    public List<GetPathData> GetPathList
    {
      get
      {
        return this._GetPathList;
      }
      set
      {
        this._GetPathList = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "GetPathDesc")]
    public string GetPathDesc
    {
      get
      {
        return this._GetPathDesc;
      }
      set
      {
        this._GetPathDesc = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AlchemyGold")]
    public int AlchemyGold
    {
      get
      {
        return this._AlchemyGold;
      }
      set
      {
        this._AlchemyGold = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomDropRewardID")]
    public int RandomDropRewardID
    {
      get
      {
        return this._RandomDropRewardID;
      }
      set
      {
        this._RandomDropRewardID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DisplayRewardCount")]
    public int DisplayRewardCount
    {
      get
      {
        return this._DisplayRewardCount;
      }
      set
      {
        this._DisplayRewardCount = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Resonance_ID")]
    public int Resonance_ID
    {
      get
      {
        return this._Resonance_ID;
      }
      set
      {
        this._Resonance_ID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CostGold")]
    public int CostGold
    {
      get
      {
        return this._CostGold;
      }
      set
      {
        this._CostGold = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
