﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TechDisplayType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TechDisplayType")]
  public enum TechDisplayType
  {
    [ProtoEnum(Name = "TechDisplayType_None", Value = 0)] TechDisplayType_None,
    [ProtoEnum(Name = "TechDisplayType_SoldierTypeLevelUp", Value = 1)] TechDisplayType_SoldierTypeLevelUp,
    [ProtoEnum(Name = "TechDisplayType_SoldierLevelUp", Value = 2)] TechDisplayType_SoldierLevelUp,
    [ProtoEnum(Name = "TechDisplayType_SkillLevelUp", Value = 3)] TechDisplayType_SkillLevelUp,
  }
}
