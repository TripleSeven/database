﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class TrainingTechInfo
  {
    public int ID { get; set; }

    public int TechID { get; set; }

    public int RoomExp { get; set; }

    public int SoldierIDUnlocked { get; set; }

    public int SoldierSkillLevelup { get; set; }

    public int SoldierSkillID { get; set; }

    public int Gold { get; set; }

    public int RoomLevel { get; set; }

    public List<Goods> Materials { get; set; }

    public List<int> PreIds { get; set; }

    public List<int> PreTechLevels { get; set; }

    public ConfigDataTrainingTechLevelInfo Config { get; set; }

    public TrainingTechResourceRequirements LevelupRequirements
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
