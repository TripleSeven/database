﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGamePlayTeamTypeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGamePlayTeamTypeInfo")]
  [Serializable]
  public class ConfigDataGamePlayTeamTypeInfo : IExtensible
  {
    private int _ID;
    private string _GamePlayName;
    private int _TeamType;
    private int _BattleType;
    private int _SubGameType_ID;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "GamePlayName")]
    public string GamePlayName
    {
      get
      {
        return this._GamePlayName;
      }
      set
      {
        this._GamePlayName = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TeamType")]
    public int TeamType
    {
      get
      {
        return this._TeamType;
      }
      set
      {
        this._TeamType = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleType")]
    public int BattleType
    {
      get
      {
        return this._BattleType;
      }
      set
      {
        this._BattleType = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SubGameType_ID")]
    public int SubGameType_ID
    {
      get
      {
        return this._SubGameType_ID;
      }
      set
      {
        this._SubGameType_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
