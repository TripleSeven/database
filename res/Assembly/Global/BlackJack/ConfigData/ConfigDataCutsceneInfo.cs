﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataCutsceneInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataCutsceneInfo")]
  [Serializable]
  public class ConfigDataCutsceneInfo : IExtensible
  {
    private int _ID;
    private int _Time;
    private string _Head;
    private string _Sound1;
    private string _Sound2;
    private string _Sound3;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Time")]
    public int Time
    {
      get
      {
        return this._Time;
      }
      set
      {
        this._Time = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Head")]
    public string Head
    {
      get
      {
        return this._Head;
      }
      set
      {
        this._Head = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound1")]
    public string Sound1
    {
      get
      {
        return this._Sound1;
      }
      set
      {
        this._Sound1 = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound2")]
    public string Sound2
    {
      get
      {
        return this._Sound2;
      }
      set
      {
        this._Sound2 = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "Sound3")]
    public string Sound3
    {
      get
      {
        return this._Sound3;
      }
      set
      {
        this._Sound3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
