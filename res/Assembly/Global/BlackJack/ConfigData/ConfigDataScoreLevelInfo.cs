﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataScoreLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataScoreLevelInfo")]
  [Serializable]
  public class ConfigDataScoreLevelInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _TeamName;
    private int _PlayerLevelMin;
    private List<int> _UnlockDaysDelayGroup;
    private int _EnergySuccess;
    private int _EnergyFail;
    private int _EnergyTeam;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _PlayerExp;
    private int _HeroExp;
    private int _GoldReward;
    private int _ScoreBase;
    private int _DropID;
    private int _ItemDropCountDisplay;
    private int _TeamDrop_ID;
    private int _DayBonusDrop_ID;
    private string _Strategy;
    private IExtension extensionObject;
    public HashSet<int> UnlockDaysDelayGroupSet;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScoreLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "TeamName")]
    public string TeamName
    {
      get
      {
        return this._TeamName;
      }
      set
      {
        this._TeamName = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerLevelMin")]
    public int PlayerLevelMin
    {
      get
      {
        return this._PlayerLevelMin;
      }
      set
      {
        this._PlayerLevelMin = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "UnlockDaysDelayGroup")]
    public List<int> UnlockDaysDelayGroup
    {
      get
      {
        return this._UnlockDaysDelayGroup;
      }
      set
      {
        this._UnlockDaysDelayGroup = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergySuccess")]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFail")]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyTeam")]
    public int EnergyTeam
    {
      get
      {
        return this._EnergyTeam;
      }
      set
      {
        this._EnergyTeam = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExp")]
    public int PlayerExp
    {
      get
      {
        return this._PlayerExp;
      }
      set
      {
        this._PlayerExp = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExp")]
    public int HeroExp
    {
      get
      {
        return this._HeroExp;
      }
      set
      {
        this._HeroExp = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoldReward")]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreBase")]
    public int ScoreBase
    {
      get
      {
        return this._ScoreBase;
      }
      set
      {
        this._ScoreBase = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropID")]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemDropCountDisplay")]
    public int ItemDropCountDisplay
    {
      get
      {
        return this._ItemDropCountDisplay;
      }
      set
      {
        this._ItemDropCountDisplay = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TeamDrop_ID")]
    public int TeamDrop_ID
    {
      get
      {
        return this._TeamDrop_ID;
      }
      set
      {
        this._TeamDrop_ID = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DayBonusDrop_ID")]
    public int DayBonusDrop_ID
    {
      get
      {
        return this._DayBonusDrop_ID;
      }
      set
      {
        this._DayBonusDrop_ID = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strategy")]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public ConfigDataBattleInfo BattleInfo { get; set; }

    public ConfigDataRandomDropRewardInfo RandomDropInfo { get; set; }

    public ConfigDataRandomDropRewardInfo TeamRandomDropInfo { get; set; }

    public ConfigDataRandomDropRewardInfo DailyRandomDropInfo { get; set; }

    public ConfigDataUnchartedScoreInfo UnchartedScoreInfo { get; set; }
  }
}
