﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroBelongProduction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroBelongProduction")]
  public enum HeroBelongProduction
  {
    [ProtoEnum(Name = "HeroBelongProduction_None", Value = 0)] HeroBelongProduction_None,
    [ProtoEnum(Name = "HeroBelongProduction_KongGui", Value = 1)] HeroBelongProduction_KongGui,
    [ProtoEnum(Name = "HeroBelongProduction_YingZhan", Value = 2)] HeroBelongProduction_YingZhan,
    [ProtoEnum(Name = "HeroBelongProduction_LM", Value = 3)] HeroBelongProduction_LM,
    [ProtoEnum(Name = "HeroBelongProduction_L1", Value = 4)] HeroBelongProduction_L1,
    [ProtoEnum(Name = "HeroBelongProduction_L2", Value = 5)] HeroBelongProduction_L2,
    [ProtoEnum(Name = "HeroBelongProduction_L3", Value = 6)] HeroBelongProduction_L3,
    [ProtoEnum(Name = "HeroBelongProduction_L4", Value = 7)] HeroBelongProduction_L4,
    [ProtoEnum(Name = "HeroBelongProduction_L5", Value = 8)] HeroBelongProduction_L5,
    [ProtoEnum(Name = "HeroBelongProduction_LR", Value = 9)] HeroBelongProduction_LR,
  }
}
