﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataLinkageHeroInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataLinkageHeroInfo")]
  [Serializable]
  public class ConfigDataLinkageHeroInfo : IExtensible
  {
    private int _ID;
    private HeroBelongProduction _HeroBelongProduction;
    private string _Name;
    private string _HeroShowBG;
    private string _Copyright;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroBelongProduction")]
    public HeroBelongProduction HeroBelongProduction
    {
      get
      {
        return this._HeroBelongProduction;
      }
      set
      {
        this._HeroBelongProduction = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "HeroShowBG")]
    public string HeroShowBG
    {
      get
      {
        return this._HeroShowBG;
      }
      set
      {
        this._HeroShowBG = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Copyright")]
    public string Copyright
    {
      get
      {
        return this._Copyright;
      }
      set
      {
        this._Copyright = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
