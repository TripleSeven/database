﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PeakAreanaBPFlowInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ConfigData
{
  public class PeakAreanaBPFlowInfo
  {
    public int BPType;
    public int PlayerIndex;

    public int Nums { get; set; }
  }
}
