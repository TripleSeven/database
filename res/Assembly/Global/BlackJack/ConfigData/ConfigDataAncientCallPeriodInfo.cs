﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAncientCallPeriodInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAncientCallPeriodInfo")]
  [Serializable]
  public class ConfigDataAncientCallPeriodInfo : IExtensible
  {
    private int _ID;
    private string _PeriodStartTime;
    private List<int> _BossList;
    private int _SingleBossLastDays;
    private IExtension extensionObject;
    public List<ConfigDataAncientCallBossInfo> Bosses;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "PeriodStartTime")]
    public string PeriodStartTime
    {
      get
      {
        return this._PeriodStartTime;
      }
      set
      {
        this._PeriodStartTime = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "BossList")]
    public List<int> BossList
    {
      get
      {
        return this._BossList;
      }
      set
      {
        this._BossList = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SingleBossLastDays")]
    public int SingleBossLastDays
    {
      get
      {
        return this._SingleBossLastDays;
      }
      set
      {
        this._SingleBossLastDays = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    public DateTime PeriodStartDateTime { get; set; }

    public DateTime PeriodEndDateTime { get; set; }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFinalBossId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentBossIndex(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetBossShowStartTime(int bossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetBossShowEndTime(int bossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallBossInfo GetBossConfigByIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossIdByIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
