﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomTalent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "RandomTalent")]
  [Serializable]
  public class RandomTalent : IExtensible
  {
    private int _SkillId;
    private int _Weight;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkillId")]
    public int SkillId
    {
      get
      {
        return this._SkillId;
      }
      set
      {
        this._SkillId = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Weight")]
    public int Weight
    {
      get
      {
        return this._Weight;
      }
      set
      {
        this._Weight = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
