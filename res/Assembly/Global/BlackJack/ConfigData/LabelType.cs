﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.LabelType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "LabelType")]
  public enum LabelType
  {
    [ProtoEnum(Name = "LabelType_None", Value = 0)] LabelType_None,
    [ProtoEnum(Name = "LabelType_FixedTime", Value = 1)] LabelType_FixedTime,
    [ProtoEnum(Name = "LabelType_Recommand", Value = 2)] LabelType_Recommand,
    [ProtoEnum(Name = "LabelType_Discount", Value = 3)] LabelType_Discount,
    [ProtoEnum(Name = "LabelType_FirstDiscount", Value = 4)] LabelType_FirstDiscount,
  }
}
