﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRandomDropRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRandomDropRewardInfo")]
  [Serializable]
  public class ConfigDataRandomDropRewardInfo : IExtensible
  {
    private int _ID;
    private int _DropID;
    private int _GroupIndex;
    private int _DropCount;
    private List<WeightGoods> _DropRewards;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomDropRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropID")]
    public int DropID
    {
      get
      {
        return this._DropID;
      }
      set
      {
        this._DropID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GroupIndex")]
    public int GroupIndex
    {
      get
      {
        return this._GroupIndex;
      }
      set
      {
        this._GroupIndex = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DropCount")]
    public int DropCount
    {
      get
      {
        return this._DropCount;
      }
      set
      {
        this._DropCount = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "DropRewards")]
    public List<WeightGoods> DropRewards
    {
      get
      {
        return this._DropRewards;
      }
      set
      {
        this._DropRewards = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
