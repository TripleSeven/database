﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroInteractor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroInteractor")]
  [Serializable]
  public class HeroInteractor : IExtensible
  {
    private int _FavorabilityLevel;
    private int _HeroInteractionId;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavorabilityLevel")]
    public int FavorabilityLevel
    {
      get
      {
        return this._FavorabilityLevel;
      }
      set
      {
        this._FavorabilityLevel = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroInteractionId")]
    public int HeroInteractionId
    {
      get
      {
        return this._HeroInteractionId;
      }
      set
      {
        this._HeroInteractionId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
