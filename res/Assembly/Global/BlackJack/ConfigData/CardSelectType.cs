﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardSelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardSelectType")]
  public enum CardSelectType
  {
    [ProtoEnum(Name = "CardSelectType_SingleSelect", Value = 1)] CardSelectType_SingleSelect = 1,
    [ProtoEnum(Name = "CardSelectType_TenSelect", Value = 2)] CardSelectType_TenSelect = 2,
    [ProtoEnum(Name = "CardSelectType_BothSelect", Value = 3)] CardSelectType_BothSelect = 3,
  }
}
