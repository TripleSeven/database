﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BuffConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BuffConditionType")]
  public enum BuffConditionType
  {
    [ProtoEnum(Name = "BuffConditionType_None", Value = 0)] BuffConditionType_None,
    [ProtoEnum(Name = "BuffConditionType_IsAlone", Value = 1)] BuffConditionType_IsAlone,
    [ProtoEnum(Name = "BuffConditionType_NotAlone", Value = 2)] BuffConditionType_NotAlone,
    [ProtoEnum(Name = "BuffConditionType_Terrain", Value = 3)] BuffConditionType_Terrain,
    [ProtoEnum(Name = "BuffConditionType_HeroArmy", Value = 4)] BuffConditionType_HeroArmy,
    [ProtoEnum(Name = "BuffConditionType_TerrainIsDF", Value = 5)] BuffConditionType_TerrainIsDF,
    [ProtoEnum(Name = "BuffConditionType_ArmyCombination", Value = 6)] BuffConditionType_ArmyCombination,
    [ProtoEnum(Name = "BuffConditionType_HeroJob", Value = 7)] BuffConditionType_HeroJob,
    [ProtoEnum(Name = "BuffConditionType_HeroInfo", Value = 8)] BuffConditionType_HeroInfo,
    [ProtoEnum(Name = "BuffConditionType_IsMove", Value = 9)] BuffConditionType_IsMove,
    [ProtoEnum(Name = "BuffConditionType_BuffID", Value = 10)] BuffConditionType_BuffID,
  }
}
