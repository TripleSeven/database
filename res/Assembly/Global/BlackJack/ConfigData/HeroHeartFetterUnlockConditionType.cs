﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.HeroHeartFetterUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "HeroHeartFetterUnlockConditionType")]
  public enum HeroHeartFetterUnlockConditionType
  {
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_None", Value = 0)] HeroHeartFetterUnlockConditionType_None,
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_HeroFetterLevel", Value = 1)] HeroHeartFetterUnlockConditionType_HeroFetterLevel,
    [ProtoEnum(Name = "HeroHeartFetterUnlockConditionType_HeroLevel", Value = 2)] HeroHeartFetterUnlockConditionType_HeroLevel,
  }
}
