﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.PerformanceEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "PerformanceEffect")]
  [Serializable]
  public class PerformanceEffect : IExtensible
  {
    private string _Anim;
    private int _Word;
    private string _Voice;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Anim")]
    public string Anim
    {
      get
      {
        return this._Anim;
      }
      set
      {
        this._Anim = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Word")]
    public int Word
    {
      get
      {
        return this._Word;
      }
      set
      {
        this._Word = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Voice")]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
