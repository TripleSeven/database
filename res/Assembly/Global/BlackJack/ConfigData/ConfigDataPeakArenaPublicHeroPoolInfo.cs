﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataPeakArenaPublicHeroPoolInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataPeakArenaPublicHeroPoolInfo")]
  [Serializable]
  public class ConfigDataPeakArenaPublicHeroPoolInfo : IExtensible
  {
    private int _ID;
    private int _HeroId;
    private int _InclusiveMinRank;
    private int _ExclusiveMaxRank;
    private int _InclusiveMinRange;
    private int _ExclusiveMaxRange;
    private int _InclusiveMinPower;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroId")]
    public int HeroId
    {
      get
      {
        return this._HeroId;
      }
      set
      {
        this._HeroId = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InclusiveMinRank")]
    public int InclusiveMinRank
    {
      get
      {
        return this._InclusiveMinRank;
      }
      set
      {
        this._InclusiveMinRank = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExclusiveMaxRank")]
    public int ExclusiveMaxRank
    {
      get
      {
        return this._ExclusiveMaxRank;
      }
      set
      {
        this._ExclusiveMaxRank = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InclusiveMinRange")]
    public int InclusiveMinRange
    {
      get
      {
        return this._InclusiveMinRange;
      }
      set
      {
        this._InclusiveMinRange = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExclusiveMaxRange")]
    public int ExclusiveMaxRange
    {
      get
      {
        return this._ExclusiveMaxRange;
      }
      set
      {
        this._ExclusiveMaxRange = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InclusiveMinPower")]
    public int InclusiveMinPower
    {
      get
      {
        return this._InclusiveMinPower;
      }
      set
      {
        this._InclusiveMinPower = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
