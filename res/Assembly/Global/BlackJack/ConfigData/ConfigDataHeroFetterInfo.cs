﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataHeroFetterInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataHeroFetterInfo")]
  [Serializable]
  public class ConfigDataHeroFetterInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Icon;
    private int _MaxLevel;
    private List<HeroFetterCompletionCondition> _CompletionConditions;
    private List<Goods> _Reward;
    private List<int> _GotSkills_ID;
    private List<HeroFetterLevelUpCost> _LevelUpMaterials;
    private List<int> _LevelUpGold;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MaxLevel")]
    public int MaxLevel
    {
      get
      {
        return this._MaxLevel;
      }
      set
      {
        this._MaxLevel = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "CompletionConditions")]
    public List<HeroFetterCompletionCondition> CompletionConditions
    {
      get
      {
        return this._CompletionConditions;
      }
      set
      {
        this._CompletionConditions = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "GotSkills_ID")]
    public List<int> GotSkills_ID
    {
      get
      {
        return this._GotSkills_ID;
      }
      set
      {
        this._GotSkills_ID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "LevelUpMaterials")]
    public List<HeroFetterLevelUpCost> LevelUpMaterials
    {
      get
      {
        return this._LevelUpMaterials;
      }
      set
      {
        this._LevelUpMaterials = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "LevelUpGold")]
    public List<int> LevelUpGold
    {
      get
      {
        return this._LevelUpGold;
      }
      set
      {
        this._LevelUpGold = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
