﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataUserGuideStep
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataUserGuideStep")]
  [Serializable]
  public class ConfigDataUserGuideStep : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextUserGuide_ID;
    private List<UserGuideTrigger> _NextTrigger;
    private List<string> _NTParam;
    private int _UserGuideDialog_ID;
    private int _BattleDialog_ID;
    private int _BattleMapX;
    private int _BattleMapY;
    private string _ClickObjectPath;
    private string _UserGuideTips;
    private string _FunctionOpenText;
    private string _FunctionOpenPrefab;
    private string _PagesPrefab;
    private UserGuideAction _StartAction1;
    private string _SAParam1;
    private UserGuideAction _StartAction2;
    private string _SAParam2;
    private UserGuideAction _StartAction3;
    private string _SAParam3;
    private IExtension extensionObject;
    public ConfigDataUserGuideStep m_nextUserGuideInfo;
    public ConfigDataBattleDialogInfo m_battleDialogInfo;
    public ConfigDataUserGuideDialogInfo m_userGuideDialogInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuideStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextUserGuide_ID")]
    public int NextUserGuide_ID
    {
      get
      {
        return this._NextUserGuide_ID;
      }
      set
      {
        this._NextUserGuide_ID = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "NextTrigger")]
    public List<UserGuideTrigger> NextTrigger
    {
      get
      {
        return this._NextTrigger;
      }
      set
      {
        this._NextTrigger = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "NTParam")]
    public List<string> NTParam
    {
      get
      {
        return this._NTParam;
      }
      set
      {
        this._NTParam = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "UserGuideDialog_ID")]
    public int UserGuideDialog_ID
    {
      get
      {
        return this._UserGuideDialog_ID;
      }
      set
      {
        this._UserGuideDialog_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleDialog_ID")]
    public int BattleDialog_ID
    {
      get
      {
        return this._BattleDialog_ID;
      }
      set
      {
        this._BattleDialog_ID = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleMapX")]
    public int BattleMapX
    {
      get
      {
        return this._BattleMapX;
      }
      set
      {
        this._BattleMapX = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleMapY")]
    public int BattleMapY
    {
      get
      {
        return this._BattleMapY;
      }
      set
      {
        this._BattleMapY = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "ClickObjectPath")]
    public string ClickObjectPath
    {
      get
      {
        return this._ClickObjectPath;
      }
      set
      {
        this._ClickObjectPath = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "UserGuideTips")]
    public string UserGuideTips
    {
      get
      {
        return this._UserGuideTips;
      }
      set
      {
        this._UserGuideTips = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "FunctionOpenText")]
    public string FunctionOpenText
    {
      get
      {
        return this._FunctionOpenText;
      }
      set
      {
        this._FunctionOpenText = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "FunctionOpenPrefab")]
    public string FunctionOpenPrefab
    {
      get
      {
        return this._FunctionOpenPrefab;
      }
      set
      {
        this._FunctionOpenPrefab = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "PagesPrefab")]
    public string PagesPrefab
    {
      get
      {
        return this._PagesPrefab;
      }
      set
      {
        this._PagesPrefab = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartAction1")]
    public UserGuideAction StartAction1
    {
      get
      {
        return this._StartAction1;
      }
      set
      {
        this._StartAction1 = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "SAParam1")]
    public string SAParam1
    {
      get
      {
        return this._SAParam1;
      }
      set
      {
        this._SAParam1 = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartAction2")]
    public UserGuideAction StartAction2
    {
      get
      {
        return this._StartAction2;
      }
      set
      {
        this._StartAction2 = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, IsRequired = true, Name = "SAParam2")]
    public string SAParam2
    {
      get
      {
        return this._SAParam2;
      }
      set
      {
        this._SAParam2 = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartAction3")]
    public UserGuideAction StartAction3
    {
      get
      {
        return this._StartAction3;
      }
      set
      {
        this._StartAction3 = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "SAParam3")]
    public string SAParam3
    {
      get
      {
        return this._SAParam3;
      }
      set
      {
        this._SAParam3 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
