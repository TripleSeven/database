﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMissionExtNoviceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMissionExtNoviceInfo")]
  [Serializable]
  public class ConfigDataMissionExtNoviceInfo : IExtensible
  {
    private int _ID;
    private int _NovicePoints;
    private int _ActivateTime;
    private int _DeactivateTime;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NovicePoints")]
    public int NovicePoints
    {
      get
      {
        return this._NovicePoints;
      }
      set
      {
        this._NovicePoints = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivateTime")]
    public int ActivateTime
    {
      get
      {
        return this._ActivateTime;
      }
      set
      {
        this._ActivateTime = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DeactivateTime")]
    public int DeactivateTime
    {
      get
      {
        return this._DeactivateTime;
      }
      set
      {
        this._DeactivateTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
