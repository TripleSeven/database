﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CardPoolType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CardPoolType")]
  public enum CardPoolType
  {
    [ProtoEnum(Name = "CardPoolType_FreeCardPool", Value = 1)] CardPoolType_FreeCardPool = 1,
    [ProtoEnum(Name = "CardPoolType_CrystalCardPool", Value = 2)] CardPoolType_CrystalCardPool = 2,
    [ProtoEnum(Name = "CardPoolType_ActivityCardPool", Value = 3)] CardPoolType_ActivityCardPool = 3,
  }
}
