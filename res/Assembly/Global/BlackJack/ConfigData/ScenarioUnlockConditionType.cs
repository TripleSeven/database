﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ScenarioUnlockConditionType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ScenarioUnlockConditionType")]
  public enum ScenarioUnlockConditionType
  {
    [ProtoEnum(Name = "ScenarioUnlockConditionType_None", Value = 0)] ScenarioUnlockConditionType_None,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_PlayerLevel", Value = 1)] ScenarioUnlockConditionType_PlayerLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_RiftLevel", Value = 2)] ScenarioUnlockConditionType_RiftLevel,
    [ProtoEnum(Name = "ScenarioUnlockConditionType_Count", Value = 3)] ScenarioUnlockConditionType_Count,
  }
}
