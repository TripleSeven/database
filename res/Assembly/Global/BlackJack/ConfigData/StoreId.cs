﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.StoreId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "StoreId")]
  public enum StoreId
  {
    [ProtoEnum(Name = "StoreId_None", Value = 0)] StoreId_None,
    [ProtoEnum(Name = "StoreId_BlackMarket", Value = 1)] StoreId_BlackMarket,
    [ProtoEnum(Name = "StoreId_Crystal", Value = 2)] StoreId_Crystal,
    [ProtoEnum(Name = "StoreId_Honor", Value = 3)] StoreId_Honor,
    [ProtoEnum(Name = "StoreId_Friendship", Value = 4)] StoreId_Friendship,
    [ProtoEnum(Name = "StoreId_SkinHero", Value = 5)] StoreId_SkinHero,
    [ProtoEnum(Name = "StoreId_SkinSoldier", Value = 6)] StoreId_SkinSoldier,
    [ProtoEnum(Name = "StoreId_Gift", Value = 7)] StoreId_Gift,
    [ProtoEnum(Name = "StoreId_Privilege", Value = 8)] StoreId_Privilege,
    [ProtoEnum(Name = "StoreId_Medal", Value = 9)] StoreId_Medal,
    [ProtoEnum(Name = "StoreId_Union", Value = 10)] StoreId_Union,
    [ProtoEnum(Name = "StoreId_Recharge", Value = 11)] StoreId_Recharge,
    [ProtoEnum(Name = "StoreId_Memory", Value = 12)] StoreId_Memory,
    [ProtoEnum(Name = "StoreId_Equipment", Value = 13)] StoreId_Equipment,
    [ProtoEnum(Name = "StoreId_GuildPerson", Value = 14)] StoreId_GuildPerson,
    [ProtoEnum(Name = "StoreId_UnchartedEquip", Value = 15)] StoreId_UnchartedEquip,
    [ProtoEnum(Name = "StoreId_UnchartedSkin", Value = 16)] StoreId_UnchartedSkin,
  }
}
