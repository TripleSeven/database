﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomDropRewardGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class RandomDropRewardGroup
  {
    public int GroupIndex;
    public int DropCount;
    public Dictionary<int, WeightGoods> DropRewards;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomDropRewardGroup()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
