﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrainingTechResourceRequirements
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class TrainingTechResourceRequirements
  {
    public int Gold;
    public int RoomLevel;
    public List<int> PreTechs;
    public List<int> PreTechLevels;
    public List<Goods> Materials;

    [MethodImpl((MethodImplOptions) 32768)]
    public static TrainingTechResourceRequirements operator +(
      TrainingTechResourceRequirements x,
      TrainingTechResourceRequirements y)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
