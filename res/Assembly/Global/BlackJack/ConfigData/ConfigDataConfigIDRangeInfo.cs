﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataConfigIDRangeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataConfigIDRangeInfo")]
  [Serializable]
  public class ConfigDataConfigIDRangeInfo : IExtensible
  {
    private int _ID;
    private string _ConfigObjectName;
    private List<int> _ConfigIDRange;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfigIDRangeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ConfigObjectName")]
    public string ConfigObjectName
    {
      get
      {
        return this._ConfigObjectName;
      }
      set
      {
        this._ConfigObjectName = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "ConfigIDRange")]
    public List<int> ConfigIDRange
    {
      get
      {
        return this._ConfigIDRange;
      }
      set
      {
        this._ConfigIDRange = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
