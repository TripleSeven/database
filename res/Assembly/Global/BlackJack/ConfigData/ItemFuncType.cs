﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ItemFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ItemFuncType")]
  public enum ItemFuncType
  {
    [ProtoEnum(Name = "ItemFuncType_None", Value = 0)] ItemFuncType_None,
    [ProtoEnum(Name = "ItemFuncType_Gold", Value = 1)] ItemFuncType_Gold,
    [ProtoEnum(Name = "ItemFuncType_Crystal", Value = 2)] ItemFuncType_Crystal,
    [ProtoEnum(Name = "ItemFuncType_Energy", Value = 3)] ItemFuncType_Energy,
    [ProtoEnum(Name = "ItemFuncType_HeroEXP", Value = 4)] ItemFuncType_HeroEXP,
    [ProtoEnum(Name = "ItemFuncType_PlayerEXP", Value = 5)] ItemFuncType_PlayerEXP,
    [ProtoEnum(Name = "ItemFuncType_HeroFragment", Value = 6)] ItemFuncType_HeroFragment,
    [ProtoEnum(Name = "ItemFuncType_JobMaterialFragment", Value = 7)] ItemFuncType_JobMaterialFragment,
    [ProtoEnum(Name = "ItemFuncType_StaticBox", Value = 8)] ItemFuncType_StaticBox,
    [ProtoEnum(Name = "ItemFuncType_RandomBox", Value = 9)] ItemFuncType_RandomBox,
    [ProtoEnum(Name = "ItemFuncType_NameChange", Value = 10)] ItemFuncType_NameChange,
    [ProtoEnum(Name = "ItemFuncType_AddHeroFavorabilityExp", Value = 11)] ItemFuncType_AddHeroFavorabilityExp,
    [ProtoEnum(Name = "ItemFuncType_SelfSelectedBox", Value = 12)] ItemFuncType_SelfSelectedBox,
    [ProtoEnum(Name = "ItemFuncType_HeroSkinSelectedBox", Value = 13)] ItemFuncType_HeroSkinSelectedBox,
    [ProtoEnum(Name = "ItemFuncType_ItemCompose", Value = 14)] ItemFuncType_ItemCompose,
  }
}
