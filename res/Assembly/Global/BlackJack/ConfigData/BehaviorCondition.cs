﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BehaviorCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BehaviorCondition")]
  public enum BehaviorCondition
  {
    [ProtoEnum(Name = "BehaviorCondition_EnemyEnterMoveAndAttackRange", Value = 1)] BehaviorCondition_EnemyEnterMoveAndAttackRange = 1,
    [ProtoEnum(Name = "BehaviorCondition_LeaderStartAttack", Value = 2)] BehaviorCondition_LeaderStartAttack = 2,
    [ProtoEnum(Name = "BehaviorCondition_EnemyHPPercentLessEqual", Value = 3)] BehaviorCondition_EnemyHPPercentLessEqual = 3,
    [ProtoEnum(Name = "BehaviorCondition_NoEnemyEnterMoveAndAttackRange", Value = 4)] BehaviorCondition_NoEnemyEnterMoveAndAttackRange = 4,
    [ProtoEnum(Name = "BehaviorCondition_EveryTurn", Value = 5)] BehaviorCondition_EveryTurn = 5,
    [ProtoEnum(Name = "BehaviorCondition_Never", Value = 6)] BehaviorCondition_Never = 6,
    [ProtoEnum(Name = "BehaviorCondition_TurnN", Value = 7)] BehaviorCondition_TurnN = 7,
    [ProtoEnum(Name = "BehaviorCondition_AttackedByEnemy", Value = 8)] BehaviorCondition_AttackedByEnemy = 8,
    [ProtoEnum(Name = "BehaviorCondition_EnemyEnterMoveAndAttackRangeOfMoveTarget", Value = 9)] BehaviorCondition_EnemyEnterMoveAndAttackRangeOfMoveTarget = 9,
    [ProtoEnum(Name = "BehaviorCondition_NoEnemyEnterMoveAndAttackRangeOfMoveTarget", Value = 10)] BehaviorCondition_NoEnemyEnterMoveAndAttackRangeOfMoveTarget = 10, // 0x0000000A
    [ProtoEnum(Name = "BehaviorCondition_DistanceToMoveTargetLestEqual", Value = 11)] BehaviorCondition_DistanceToMoveTargetLestEqual = 11, // 0x0000000B
    [ProtoEnum(Name = "BehaviorCondition_SelfHPPercentLessEqual", Value = 12)] BehaviorCondition_SelfHPPercentLessEqual = 12, // 0x0000000C
    [ProtoEnum(Name = "BehaviorCondition_LeaderStopAttack", Value = 13)] BehaviorCondition_LeaderStopAttack = 13, // 0x0000000D
    [ProtoEnum(Name = "BehaviorCondition_DoingBehaviorN", Value = 14)] BehaviorCondition_DoingBehaviorN = 14, // 0x0000000E
    [ProtoEnum(Name = "BehaviorCondition_AttackedByEnemyInLastTurn", Value = 15)] BehaviorCondition_AttackedByEnemyInLastTurn = 15, // 0x0000000F
    [ProtoEnum(Name = "BehaviorCondition_FoundEnemyWithBuffN", Value = 16)] BehaviorCondition_FoundEnemyWithBuffN = 16, // 0x00000010
    [ProtoEnum(Name = "BehaviorCondition_NoEnemyWithBuffN", Value = 17)] BehaviorCondition_NoEnemyWithBuffN = 17, // 0x00000011
    [ProtoEnum(Name = "BehaviorCondition_MemberAttackedByEnemy", Value = 18)] BehaviorCondition_MemberAttackedByEnemy = 18, // 0x00000012
    [ProtoEnum(Name = "BehaviorCondition_MemberHPPercentLessEqual", Value = 19)] BehaviorCondition_MemberHPPercentLessEqual = 19, // 0x00000013
    [ProtoEnum(Name = "BehaviorCondition_EnemyEnterMoveAndAttackRangeExcept", Value = 20)] BehaviorCondition_EnemyEnterMoveAndAttackRangeExcept = 20, // 0x00000014
    [ProtoEnum(Name = "BehaviorCondition_NoEnemyEnterMoveAndAttackRangeExcept", Value = 21)] BehaviorCondition_NoEnemyEnterMoveAndAttackRangeExcept = 21, // 0x00000015
    [ProtoEnum(Name = "BehaviorCondition_SelfHPPercentGreaterEqual", Value = 22)] BehaviorCondition_SelfHPPercentGreaterEqual = 22, // 0x00000016
    [ProtoEnum(Name = "BehaviorCondition_HeroNExist", Value = 23)] BehaviorCondition_HeroNExist = 23, // 0x00000017
    [ProtoEnum(Name = "BehaviorCondition_GroupMemberDead", Value = 24)] BehaviorCondition_GroupMemberDead = 24, // 0x00000018
    [ProtoEnum(Name = "BehaviorCondition_TurnCountN", Value = 25)] BehaviorCondition_TurnCountN = 25, // 0x00000019
    [ProtoEnum(Name = "BehaviorCondition_EnemyBlockWayToMoveTarget", Value = 26)] BehaviorCondition_EnemyBlockWayToMoveTarget = 26, // 0x0000001A
    [ProtoEnum(Name = "BehaviorCondition_HasWayToMoveTarget", Value = 27)] BehaviorCondition_HasWayToMoveTarget = 27, // 0x0000001B
    [ProtoEnum(Name = "BehaviorCondition_FoundMemberWithBuffN", Value = 28)] BehaviorCondition_FoundMemberWithBuffN = 28, // 0x0000001C
    [ProtoEnum(Name = "BehaviorCondition_NoMemberWithBuffN", Value = 29)] BehaviorCondition_NoMemberWithBuffN = 29, // 0x0000001D
    [ProtoEnum(Name = "BehaviorCondition_FoundSelfWithBuffN", Value = 30)] BehaviorCondition_FoundSelfWithBuffN = 30, // 0x0000001E
    [ProtoEnum(Name = "BehaviorCondition_FoundSelfWithoutBuffN", Value = 31)] BehaviorCondition_FoundSelfWithoutBuffN = 31, // 0x0000001F
    [ProtoEnum(Name = "BehaviorCondition_HPDecreased", Value = 32)] BehaviorCondition_HPDecreased = 32, // 0x00000020
    [ProtoEnum(Name = "BehaviorCondition_AttackedOrDebuffedByEnemy", Value = 33)] BehaviorCondition_AttackedOrDebuffedByEnemy = 33, // 0x00000021
    [ProtoEnum(Name = "BehaviorCondition_EnemyEnterNGridsLineRange", Value = 34)] BehaviorCondition_EnemyEnterNGridsLineRange = 34, // 0x00000022
    [ProtoEnum(Name = "BehaviorCondition_NoEnemyEnterNGridsLineRange", Value = 35)] BehaviorCondition_NoEnemyEnterNGridsLineRange = 35, // 0x00000023
  }
}
