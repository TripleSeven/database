﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SelectSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SelectSkill")]
  public enum SelectSkill
  {
    [ProtoEnum(Name = "SelectSkill_DefaultSelection", Value = 1)] SelectSkill_DefaultSelection = 1,
    [ProtoEnum(Name = "SelectSkill_DirectReachTargetSkill", Value = 2)] SelectSkill_DirectReachTargetSkill = 2,
    [ProtoEnum(Name = "SelectSkill_ExcludeSkillID", Value = 3)] SelectSkill_ExcludeSkillID = 3,
    [ProtoEnum(Name = "SelectSkill_IncludeSkillID", Value = 4)] SelectSkill_IncludeSkillID = 4,
  }
}
