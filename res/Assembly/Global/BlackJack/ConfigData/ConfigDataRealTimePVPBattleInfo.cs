﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRealTimePVPBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRealTimePVPBattleInfo")]
  [Serializable]
  public class ConfigDataRealTimePVPBattleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private string _WinDesc;
    private string _LoseDesc;
    private int _Battlefield_ID;
    private int _CameraX;
    private int _CameraY;
    private int _DefendCameraX;
    private int _DefendCameraY;
    private string _PrepareMusic;
    private string _BattleMusic;
    private string _DefendBattleMusic;
    private int _TurnMax;
    private List<int> _WinConditions_ID;
    private List<int> _LoseConditions_ID;
    private List<int> _EventTriggers_ID;
    private int _AttackNumber;
    private List<ParamPosition> _AttackPositions;
    private List<int> _AttackDirs;
    private int _DefendNumber;
    private List<ParamPosition> _DefendPositions;
    private List<int> _DefendDirs;
    private IExtension extensionObject;
    public ConfigDataBattlefieldInfo m_battlefieldInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "WinDesc")]
    public string WinDesc
    {
      get
      {
        return this._WinDesc;
      }
      set
      {
        this._WinDesc = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "LoseDesc")]
    public string LoseDesc
    {
      get
      {
        return this._LoseDesc;
      }
      set
      {
        this._LoseDesc = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battlefield_ID")]
    public int Battlefield_ID
    {
      get
      {
        return this._Battlefield_ID;
      }
      set
      {
        this._Battlefield_ID = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraX")]
    public int CameraX
    {
      get
      {
        return this._CameraX;
      }
      set
      {
        this._CameraX = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CameraY")]
    public int CameraY
    {
      get
      {
        return this._CameraY;
      }
      set
      {
        this._CameraY = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefendCameraX")]
    public int DefendCameraX
    {
      get
      {
        return this._DefendCameraX;
      }
      set
      {
        this._DefendCameraX = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefendCameraY")]
    public int DefendCameraY
    {
      get
      {
        return this._DefendCameraY;
      }
      set
      {
        this._DefendCameraY = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "PrepareMusic")]
    public string PrepareMusic
    {
      get
      {
        return this._PrepareMusic;
      }
      set
      {
        this._PrepareMusic = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleMusic")]
    public string BattleMusic
    {
      get
      {
        return this._BattleMusic;
      }
      set
      {
        this._BattleMusic = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefendBattleMusic")]
    public string DefendBattleMusic
    {
      get
      {
        return this._DefendBattleMusic;
      }
      set
      {
        this._DefendBattleMusic = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TurnMax")]
    public int TurnMax
    {
      get
      {
        return this._TurnMax;
      }
      set
      {
        this._TurnMax = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, Name = "WinConditions_ID")]
    public List<int> WinConditions_ID
    {
      get
      {
        return this._WinConditions_ID;
      }
      set
      {
        this._WinConditions_ID = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, Name = "LoseConditions_ID")]
    public List<int> LoseConditions_ID
    {
      get
      {
        return this._LoseConditions_ID;
      }
      set
      {
        this._LoseConditions_ID = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, Name = "EventTriggers_ID")]
    public List<int> EventTriggers_ID
    {
      get
      {
        return this._EventTriggers_ID;
      }
      set
      {
        this._EventTriggers_ID = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AttackNumber")]
    public int AttackNumber
    {
      get
      {
        return this._AttackNumber;
      }
      set
      {
        this._AttackNumber = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.Default, Name = "AttackPositions")]
    public List<ParamPosition> AttackPositions
    {
      get
      {
        return this._AttackPositions;
      }
      set
      {
        this._AttackPositions = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, Name = "AttackDirs")]
    public List<int> AttackDirs
    {
      get
      {
        return this._AttackDirs;
      }
      set
      {
        this._AttackDirs = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DefendNumber")]
    public int DefendNumber
    {
      get
      {
        return this._DefendNumber;
      }
      set
      {
        this._DefendNumber = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.Default, Name = "DefendPositions")]
    public List<ParamPosition> DefendPositions
    {
      get
      {
        return this._DefendPositions;
      }
      set
      {
        this._DefendPositions = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, Name = "DefendDirs")]
    public List<int> DefendDirs
    {
      get
      {
        return this._DefendDirs;
      }
      set
      {
        this._DefendDirs = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
