﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataJobLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataJobLevelInfo")]
  [Serializable]
  public class ConfigDataJobLevelInfo : IExtensible
  {
    private int _ID;
    private string _Desc;
    private List<Goods> _Materials;
    private int _JobLevelUpHeroLevel;
    private int _HP_INI;
    private int _HP_UP;
    private int _AT_INI;
    private int _AT_UP;
    private int _Magic_INI;
    private int _Magic_UP;
    private int _DF_INI;
    private int _DF_UP;
    private int _MagicDF_INI;
    private int _MagicDF_UP;
    private int _DEX_INI;
    private int _DEX_UP;
    private int _GotSkill_ID;
    private int _GotSoldier_ID;
    private IExtension extensionObject;
    public int[] m_jobMaterialLevels;
    public ConfigDataSkillInfo m_gotSkillInfo;
    public ConfigDataSoldierInfo m_gotSoldierInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "Materials")]
    public List<Goods> Materials
    {
      get
      {
        return this._Materials;
      }
      set
      {
        this._Materials = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JobLevelUpHeroLevel")]
    public int JobLevelUpHeroLevel
    {
      get
      {
        return this._JobLevelUpHeroLevel;
      }
      set
      {
        this._JobLevelUpHeroLevel = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HP_INI")]
    public int HP_INI
    {
      get
      {
        return this._HP_INI;
      }
      set
      {
        this._HP_INI = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HP_UP")]
    public int HP_UP
    {
      get
      {
        return this._HP_UP;
      }
      set
      {
        this._HP_UP = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AT_INI")]
    public int AT_INI
    {
      get
      {
        return this._AT_INI;
      }
      set
      {
        this._AT_INI = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AT_UP")]
    public int AT_UP
    {
      get
      {
        return this._AT_UP;
      }
      set
      {
        this._AT_UP = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Magic_INI")]
    public int Magic_INI
    {
      get
      {
        return this._Magic_INI;
      }
      set
      {
        this._Magic_INI = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Magic_UP")]
    public int Magic_UP
    {
      get
      {
        return this._Magic_UP;
      }
      set
      {
        this._Magic_UP = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DF_INI")]
    public int DF_INI
    {
      get
      {
        return this._DF_INI;
      }
      set
      {
        this._DF_INI = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DF_UP")]
    public int DF_UP
    {
      get
      {
        return this._DF_UP;
      }
      set
      {
        this._DF_UP = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDF_INI")]
    public int MagicDF_INI
    {
      get
      {
        return this._MagicDF_INI;
      }
      set
      {
        this._MagicDF_INI = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MagicDF_UP")]
    public int MagicDF_UP
    {
      get
      {
        return this._MagicDF_UP;
      }
      set
      {
        this._MagicDF_UP = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DEX_INI")]
    public int DEX_INI
    {
      get
      {
        return this._DEX_INI;
      }
      set
      {
        this._DEX_INI = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DEX_UP")]
    public int DEX_UP
    {
      get
      {
        return this._DEX_UP;
      }
      set
      {
        this._DEX_UP = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GotSkill_ID")]
    public int GotSkill_ID
    {
      get
      {
        return this._GotSkill_ID;
      }
      set
      {
        this._GotSkill_ID = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GotSoldier_ID")]
    public int GotSoldier_ID
    {
      get
      {
        return this._GotSoldier_ID;
      }
      set
      {
        this._GotSoldier_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
