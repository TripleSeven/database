﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataResonanceInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataResonanceInfo")]
  [Serializable]
  public class ConfigDataResonanceInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _SmallIcon;
    private string _InactiveIcon;
    private string _ActiveIcon;
    private string _Affix;
    private int _Effect1;
    private int _Effect2;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "SmallIcon")]
    public string SmallIcon
    {
      get
      {
        return this._SmallIcon;
      }
      set
      {
        this._SmallIcon = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "InactiveIcon")]
    public string InactiveIcon
    {
      get
      {
        return this._InactiveIcon;
      }
      set
      {
        this._InactiveIcon = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "ActiveIcon")]
    public string ActiveIcon
    {
      get
      {
        return this._ActiveIcon;
      }
      set
      {
        this._ActiveIcon = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Affix")]
    public string Affix
    {
      get
      {
        return this._Affix;
      }
      set
      {
        this._Affix = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Effect1")]
    public int Effect1
    {
      get
      {
        return this._Effect1;
      }
      set
      {
        this._Effect1 = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Effect2")]
    public int Effect2
    {
      get
      {
        return this._Effect2;
      }
      set
      {
        this._Effect2 = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
