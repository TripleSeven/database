﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityScoreRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BlackJack.ConfigData
{
  public class CollectionActivityScoreRewardInfo
  {
    public int Score;
    public List<Goods> Rewards;
  }
}
