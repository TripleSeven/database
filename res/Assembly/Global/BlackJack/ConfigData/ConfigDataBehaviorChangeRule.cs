﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBehaviorChangeRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBehaviorChangeRule")]
  [Serializable]
  public class ConfigDataBehaviorChangeRule : IExtensible
  {
    private int _ID;
    private BehaviorCondition _ChangeCondition;
    private string _CCParam;
    private int _NextBehaviorID;
    private IExtension extensionObject;
    public ConfigDataBehavior.ParamData CCParamData;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehaviorChangeRule()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChangeCondition")]
    public BehaviorCondition ChangeCondition
    {
      get
      {
        return this._ChangeCondition;
      }
      set
      {
        this._ChangeCondition = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "CCParam")]
    public string CCParam
    {
      get
      {
        return this._CCParam;
      }
      set
      {
        this._CCParam = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextBehaviorID")]
    public int NextBehaviorID
    {
      get
      {
        return this._NextBehaviorID;
      }
      set
      {
        this._NextBehaviorID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Initialize()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
