﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TrackType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TrackType")]
  public enum TrackType
  {
    [ProtoEnum(Name = "TrackType_None", Value = 0)] TrackType_None,
    [ProtoEnum(Name = "TrackType_Parabolic", Value = 1)] TrackType_Parabolic,
    [ProtoEnum(Name = "TrackType_Straight", Value = 2)] TrackType_Straight,
    [ProtoEnum(Name = "TrackType_DownStraight", Value = 3)] TrackType_DownStraight,
    [ProtoEnum(Name = "TrackType_UpStraight", Value = 4)] TrackType_UpStraight,
  }
}
