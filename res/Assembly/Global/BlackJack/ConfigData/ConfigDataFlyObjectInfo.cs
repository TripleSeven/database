﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataFlyObjectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataFlyObjectInfo")]
  [Serializable]
  public class ConfigDataFlyObjectInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Speed;
    private int _Distance;
    private TrackType _TrackType;
    private int _Gravity;
    private int _CollisionRadius;
    private string _Model;
    private int _ModelScale;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Speed")]
    public int Speed
    {
      get
      {
        return this._Speed;
      }
      set
      {
        this._Speed = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Distance")]
    public int Distance
    {
      get
      {
        return this._Distance;
      }
      set
      {
        this._Distance = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TrackType")]
    public TrackType TrackType
    {
      get
      {
        return this._TrackType;
      }
      set
      {
        this._TrackType = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Gravity")]
    public int Gravity
    {
      get
      {
        return this._Gravity;
      }
      set
      {
        this._Gravity = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CollisionRadius")]
    public int CollisionRadius
    {
      get
      {
        return this._CollisionRadius;
      }
      set
      {
        this._CollisionRadius = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModelScale")]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
