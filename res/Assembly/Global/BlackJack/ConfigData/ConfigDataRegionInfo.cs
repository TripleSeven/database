﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRegionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRegionInfo")]
  [Serializable]
  public class ConfigDataRegionInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private List<int> _Waypoints_ID;
    private int _OpenByScenario_ID;
    private IExtension extensionObject;
    public ConfigDataWaypointInfo[] m_waypointInfos;
    public ConfigDataScenarioInfo m_openByScenarioInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRegionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "Waypoints_ID")]
    public List<int> Waypoints_ID
    {
      get
      {
        return this._Waypoints_ID;
      }
      set
      {
        this._Waypoints_ID = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenByScenario_ID")]
    public int OpenByScenario_ID
    {
      get
      {
        return this._OpenByScenario_ID;
      }
      set
      {
        this._OpenByScenario_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
