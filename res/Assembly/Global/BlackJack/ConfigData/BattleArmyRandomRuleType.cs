﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.BattleArmyRandomRuleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "BattleArmyRandomRuleType")]
  public enum BattleArmyRandomRuleType
  {
    [ProtoEnum(Name = "BattleArmyRandomRuleType_None", Value = 0)] BattleArmyRandomRuleType_None,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_EveryTime", Value = 1)] BattleArmyRandomRuleType_EveryTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_DailyTime", Value = 2)] BattleArmyRandomRuleType_DailyTime,
    [ProtoEnum(Name = "BattleArmyRandomRuleType_ClimbTower", Value = 3)] BattleArmyRandomRuleType_ClimbTower,
  }
}
