﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ParamPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ParamPosition")]
  [Serializable]
  public class ParamPosition : IExtensible
  {
    private int _X;
    private int _Y;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "X")]
    public int X
    {
      get
      {
        return this._X;
      }
      set
      {
        this._X = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Y")]
    public int Y
    {
      get
      {
        return this._Y;
      }
      set
      {
        this._Y = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
