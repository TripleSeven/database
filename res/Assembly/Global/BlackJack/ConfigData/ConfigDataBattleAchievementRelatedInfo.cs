﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataBattleAchievementRelatedInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataBattleAchievementRelatedInfo")]
  [Serializable]
  public class ConfigDataBattleAchievementRelatedInfo : IExtensible
  {
    private int _ID;
    private int _Achievement_ID;
    private IExtension extensionObject;
    public ConfigDataBattleAchievementInfo m_achievementInfo;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Achievement_ID")]
    public int Achievement_ID
    {
      get
      {
        return this._Achievement_ID;
      }
      set
      {
        this._Achievement_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
