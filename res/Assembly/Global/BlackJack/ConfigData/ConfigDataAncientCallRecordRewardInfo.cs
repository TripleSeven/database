﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataAncientCallRecordRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataAncientCallRecordRewardInfo")]
  [Serializable]
  public class ConfigDataAncientCallRecordRewardInfo : IExtensible
  {
    private int _ID;
    private int _RecordReward_Id;
    private string _Evaluate;
    private int _Damage;
    private int _SingleBossRewardMailTemplateId;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RecordReward_Id")]
    public int RecordReward_Id
    {
      get
      {
        return this._RecordReward_Id;
      }
      set
      {
        this._RecordReward_Id = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Evaluate")]
    public string Evaluate
    {
      get
      {
        return this._Evaluate;
      }
      set
      {
        this._Evaluate = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Damage")]
    public int Damage
    {
      get
      {
        return this._Damage;
      }
      set
      {
        this._Damage = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SingleBossRewardMailTemplateId")]
    public int SingleBossRewardMailTemplateId
    {
      get
      {
        return this._SingleBossRewardMailTemplateId;
      }
      set
      {
        this._SingleBossRewardMailTemplateId = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
