﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataRechargeStoreItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataRechargeStoreItemInfo")]
  [Serializable]
  public class ConfigDataRechargeStoreItemInfo : IExtensible
  {
    private int _ID;
    private double _Price;
    private int _GotCrystalNums;
    private int _FirstBoughtReward;
    private int _RepeatlyBoughtReward;
    private string _Icon;
    private bool _IsBestValue;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Price")]
    public double Price
    {
      get
      {
        return this._Price;
      }
      set
      {
        this._Price = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GotCrystalNums")]
    public int GotCrystalNums
    {
      get
      {
        return this._GotCrystalNums;
      }
      set
      {
        this._GotCrystalNums = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FirstBoughtReward")]
    public int FirstBoughtReward
    {
      get
      {
        return this._FirstBoughtReward;
      }
      set
      {
        this._FirstBoughtReward = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RepeatlyBoughtReward")]
    public int RepeatlyBoughtReward
    {
      get
      {
        return this._RepeatlyBoughtReward;
      }
      set
      {
        this._RepeatlyBoughtReward = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsBestValue")]
    public bool IsBestValue
    {
      get
      {
        return this._IsBestValue;
      }
      set
      {
        this._IsBestValue = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
