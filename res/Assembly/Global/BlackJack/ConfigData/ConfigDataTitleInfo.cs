﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataTitleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataTitleInfo")]
  [Serializable]
  public class ConfigDataTitleInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private int _Rarity;
    private string _ResourceLocation;
    private string _FrameLocation;
    private string _Icon;
    private string _EndTime;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Rarity")]
    public int Rarity
    {
      get
      {
        return this._Rarity;
      }
      set
      {
        this._Rarity = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "ResourceLocation")]
    public string ResourceLocation
    {
      get
      {
        return this._ResourceLocation;
      }
      set
      {
        this._ResourceLocation = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "FrameLocation")]
    public string FrameLocation
    {
      get
      {
        return this._FrameLocation;
      }
      set
      {
        this._FrameLocation = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "EndTime")]
    public string EndTime
    {
      get
      {
        return this._EndTime;
      }
      set
      {
        this._EndTime = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
