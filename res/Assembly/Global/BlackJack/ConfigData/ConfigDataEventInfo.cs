﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEventInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEventInfo")]
  [Serializable]
  public class ConfigDataEventInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private string _Desc;
    private EventFuncType _FuncType;
    private int _EnergySuccess;
    private int _EnergyFail;
    private List<EventInfoDisappearCondition> _DisappearCondition;
    private int _MonsterLevel;
    private int _Battle_ID;
    private int _DialogBefore_ID;
    private int _DialogAfter_ID;
    private int _PlayerExpReward;
    private int _HeroExpReward;
    private int _GoldReward;
    private List<Goods> _Reward;
    private int _Drop_ID;
    private WaypointStyleType _StyleType;
    private string _Model;
    private int _ModelScale;
    private string _MapIcon;
    private int _MapIconY;
    private string _Icon;
    private bool _IsDanger;
    private int _CharImage_ID;
    private string _Strategy;
    private int _OperationalActivityDrop_ID;
    private IExtension extensionObject;
    public ConfigDataBattleInfo m_battleInfo;
    public ConfigDataDialogInfo m_dialogInfoBefore;
    public ConfigDataDialogInfo m_dialogInfoAfter;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Desc")]
    public string Desc
    {
      get
      {
        return this._Desc;
      }
      set
      {
        this._Desc = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FuncType")]
    public EventFuncType FuncType
    {
      get
      {
        return this._FuncType;
      }
      set
      {
        this._FuncType = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergySuccess")]
    public int EnergySuccess
    {
      get
      {
        return this._EnergySuccess;
      }
      set
      {
        this._EnergySuccess = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFail")]
    public int EnergyFail
    {
      get
      {
        return this._EnergyFail;
      }
      set
      {
        this._EnergyFail = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "DisappearCondition")]
    public List<EventInfoDisappearCondition> DisappearCondition
    {
      get
      {
        return this._DisappearCondition;
      }
      set
      {
        this._DisappearCondition = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MonsterLevel")]
    public int MonsterLevel
    {
      get
      {
        return this._MonsterLevel;
      }
      set
      {
        this._MonsterLevel = value;
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Battle_ID")]
    public int Battle_ID
    {
      get
      {
        return this._Battle_ID;
      }
      set
      {
        this._Battle_ID = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogBefore_ID")]
    public int DialogBefore_ID
    {
      get
      {
        return this._DialogBefore_ID;
      }
      set
      {
        this._DialogBefore_ID = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DialogAfter_ID")]
    public int DialogAfter_ID
    {
      get
      {
        return this._DialogAfter_ID;
      }
      set
      {
        this._DialogAfter_ID = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerExpReward")]
    public int PlayerExpReward
    {
      get
      {
        return this._PlayerExpReward;
      }
      set
      {
        this._PlayerExpReward = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroExpReward")]
    public int HeroExpReward
    {
      get
      {
        return this._HeroExpReward;
      }
      set
      {
        this._HeroExpReward = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoldReward")]
    public int GoldReward
    {
      get
      {
        return this._GoldReward;
      }
      set
      {
        this._GoldReward = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, Name = "Reward")]
    public List<Goods> Reward
    {
      get
      {
        return this._Reward;
      }
      set
      {
        this._Reward = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Drop_ID")]
    public int Drop_ID
    {
      get
      {
        return this._Drop_ID;
      }
      set
      {
        this._Drop_ID = value;
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StyleType")]
    public WaypointStyleType StyleType
    {
      get
      {
        return this._StyleType;
      }
      set
      {
        this._StyleType = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, IsRequired = true, Name = "Model")]
    public string Model
    {
      get
      {
        return this._Model;
      }
      set
      {
        this._Model = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ModelScale")]
    public int ModelScale
    {
      get
      {
        return this._ModelScale;
      }
      set
      {
        this._ModelScale = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, IsRequired = true, Name = "MapIcon")]
    public string MapIcon
    {
      get
      {
        return this._MapIcon;
      }
      set
      {
        this._MapIcon = value;
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MapIconY")]
    public int MapIconY
    {
      get
      {
        return this._MapIconY;
      }
      set
      {
        this._MapIconY = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.Default, IsRequired = true, Name = "Icon")]
    public string Icon
    {
      get
      {
        return this._Icon;
      }
      set
      {
        this._Icon = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsDanger")]
    public bool IsDanger
    {
      get
      {
        return this._IsDanger;
      }
      set
      {
        this._IsDanger = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharImage_ID")]
    public int CharImage_ID
    {
      get
      {
        return this._CharImage_ID;
      }
      set
      {
        this._CharImage_ID = value;
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.Default, IsRequired = true, Name = "Strategy")]
    public string Strategy
    {
      get
      {
        return this._Strategy;
      }
      set
      {
        this._Strategy = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OperationalActivityDrop_ID")]
    public int OperationalActivityDrop_ID
    {
      get
      {
        return this._OperationalActivityDrop_ID;
      }
      set
      {
        this._OperationalActivityDrop_ID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
