﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ClientConfigDataLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.UtilityTools;
using ProtoBuf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class ClientConfigDataLoader : ClientConfigDataLoaderBase, IConfigDataLoader
  {
    protected int m_allConfigsCount;
    protected int m_loadingConfigIndex;
    private Dictionary<int, ConfigDataActivityCardPoolGroupInfo> m_ConfigDataActivityCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataAncientCallBehavioralDescInfo> m_ConfigDataAncientCallBehavioralDescInfoData;
    private Dictionary<int, ConfigDataAncientCallBossInfo> m_ConfigDataAncientCallBossInfoData;
    private Dictionary<int, ConfigDataAncientCallPeriodInfo> m_ConfigDataAncientCallPeriodInfoData;
    private Dictionary<int, ConfigDataAncientCallRecordRewardInfo> m_ConfigDataAncientCallRecordRewardInfoData;
    private Dictionary<int, ConfigDataAncientCallSingleBossPointsInfo> m_ConfigDataAncientCallSingleBossPointsInfoData;
    private Dictionary<int, ConfigDataAncientCallTotalBossPointsInfo> m_ConfigDataAncientCallTotalBossPointsInfoData;
    private Dictionary<int, ConfigDataAnikiGymInfo> m_ConfigDataAnikiGymInfoData;
    private Dictionary<int, ConfigDataAnikiLevelInfo> m_ConfigDataAnikiLevelInfoData;
    private Dictionary<int, ConfigDataArenaBattleInfo> m_ConfigDataArenaBattleInfoData;
    private Dictionary<int, ConfigDataArenaDefendRuleInfo> m_ConfigDataArenaDefendRuleInfoData;
    private Dictionary<int, ConfigDataArenaLevelInfo> m_ConfigDataArenaLevelInfoData;
    private Dictionary<int, ConfigDataArenaOpponentPointZoneInfo> m_ConfigDataArenaOpponentPointZoneInfoData;
    private Dictionary<int, ConfigDataArenaRankRewardInfo> m_ConfigDataArenaRankRewardInfoData;
    private Dictionary<int, ConfigDataArenaRobotInfo> m_ConfigDataArenaRobotInfoData;
    private Dictionary<int, ConfigDataArenaSettleTimeInfo> m_ConfigDataArenaSettleTimeInfoData;
    private Dictionary<int, ConfigDataArenaVictoryPointsRewardInfo> m_ConfigDataArenaVictoryPointsRewardInfoData;
    private Dictionary<int, ConfigDataArmyInfo> m_ConfigDataArmyInfoData;
    private Dictionary<int, ConfigDataArmyRelation> m_ConfigDataArmyRelationData;
    private Dictionary<int, ConfigDataAssetReplaceInfo> m_ConfigDataAssetReplaceInfoData;
    private Dictionary<int, ConfigDataBanditInfo> m_ConfigDataBanditInfoData;
    private Dictionary<int, ConfigDataBattleAchievementInfo> m_ConfigDataBattleAchievementInfoData;
    private Dictionary<int, ConfigDataBattleAchievementRelatedInfo> m_ConfigDataBattleAchievementRelatedInfoData;
    private Dictionary<int, ConfigDataBattleDialogInfo> m_ConfigDataBattleDialogInfoData;
    private Dictionary<int, ConfigDataBattleEventActionInfo> m_ConfigDataBattleEventActionInfoData;
    private Dictionary<int, ConfigDataBattleEventTriggerInfo> m_ConfigDataBattleEventTriggerInfoData;
    private Dictionary<int, ConfigDataBattlefieldInfo> m_ConfigDataBattlefieldInfoData;
    private Dictionary<int, ConfigDataBattleInfo> m_ConfigDataBattleInfoData;
    private Dictionary<int, ConfigDataBattleLoseConditionInfo> m_ConfigDataBattleLoseConditionInfoData;
    private Dictionary<int, ConfigDataBattlePerformInfo> m_ConfigDataBattlePerformInfoData;
    private Dictionary<int, ConfigDataBattleRandomArmyInfo> m_ConfigDataBattleRandomArmyInfoData;
    private Dictionary<int, ConfigDataBattleRandomTalentInfo> m_ConfigDataBattleRandomTalentInfoData;
    private Dictionary<int, ConfigDataBattleTreasureInfo> m_ConfigDataBattleTreasureInfoData;
    private Dictionary<int, ConfigDataBattleWinConditionInfo> m_ConfigDataBattleWinConditionInfoData;
    private Dictionary<int, ConfigDataBehavior> m_ConfigDataBehaviorData;
    private Dictionary<int, ConfigDataBehaviorChangeRule> m_ConfigDataBehaviorChangeRuleData;
    private Dictionary<int, ConfigDataBigExpressionInfo> m_ConfigDataBigExpressionInfoData;
    private Dictionary<int, ConfigDataBuffInfo> m_ConfigDataBuffInfoData;
    private Dictionary<int, ConfigDataBuyArenaTicketInfo> m_ConfigDataBuyArenaTicketInfoData;
    private Dictionary<int, ConfigDataBuyEnergyInfo> m_ConfigDataBuyEnergyInfoData;
    private Dictionary<int, ConfigDataCardPoolGroupInfo> m_ConfigDataCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataCardPoolInfo> m_ConfigDataCardPoolInfoData;
    private Dictionary<int, ConfigDataChallengeLevelInfo> m_ConfigDataChallengeLevelInfoData;
    private Dictionary<int, ConfigDataCharImageInfo> m_ConfigDataCharImageInfoData;
    private Dictionary<int, ConfigDataCharImageSkinResourceInfo> m_ConfigDataCharImageSkinResourceInfoData;
    private Dictionary<int, ConfigDataCollectionActivityChallengeLevelInfo> m_ConfigDataCollectionActivityChallengeLevelInfoData;
    private Dictionary<int, ConfigDataCollectionActivityCurrencyItemExtInfo> m_ConfigDataCollectionActivityCurrencyItemExtInfoData;
    private Dictionary<int, ConfigDataCollectionActivityExchangeTableInfo> m_ConfigDataCollectionActivityExchangeTableInfoData;
    private Dictionary<int, ConfigDataCollectionActivityInfo> m_ConfigDataCollectionActivityInfoData;
    private Dictionary<int, ConfigDataCollectionActivityLootLevelInfo> m_ConfigDataCollectionActivityLootLevelInfoData;
    private Dictionary<int, ConfigDataCollectionActivityMapInfo> m_ConfigDataCollectionActivityMapInfoData;
    private Dictionary<int, ConfigDataCollectionActivityScenarioLevelInfo> m_ConfigDataCollectionActivityScenarioLevelInfoData;
    private Dictionary<int, ConfigDataCollectionActivityScoreRewardGroupInfo> m_ConfigDataCollectionActivityScoreRewardGroupInfoData;
    private Dictionary<int, ConfigDataCollectionActivityWaypointInfo> m_ConfigDataCollectionActivityWaypointInfoData;
    private Dictionary<int, ConfigDataCollectionEventInfo> m_ConfigDataCollectionEventInfoData;
    private Dictionary<int, ConfigDataConfessionDialogInfo> m_ConfigDataConfessionDialogInfoData;
    private Dictionary<int, ConfigDataConfessionLetterInfo> m_ConfigDataConfessionLetterInfoData;
    private Dictionary<int, ConfigDataConfigableConst> m_ConfigDataConfigableConstData;
    private Dictionary<int, ConfigDataConfigIDRangeInfo> m_ConfigDataConfigIDRangeInfoData;
    private Dictionary<int, ConfigDataCooperateBattleInfo> m_ConfigDataCooperateBattleInfoData;
    private Dictionary<int, ConfigDataCooperateBattleLevelInfo> m_ConfigDataCooperateBattleLevelInfoData;
    private Dictionary<int, ConfigDataCrystalCardPoolGroupInfo> m_ConfigDataCrystalCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataCutsceneInfo> m_ConfigDataCutsceneInfoData;
    private Dictionary<int, ConfigDataDailyPushNotification> m_ConfigDataDailyPushNotificationData;
    private Dictionary<int, ConfigDataDeviceSetting> m_ConfigDataDeviceSettingData;
    private Dictionary<int, ConfigDataDialogChoiceInfo> m_ConfigDataDialogChoiceInfoData;
    private Dictionary<int, ConfigDataDialogInfo> m_ConfigDataDialogInfoData;
    private Dictionary<int, ConfigDataEnchantStoneInfo> m_ConfigDataEnchantStoneInfoData;
    private Dictionary<int, ConfigDataEnchantTemplateInfo> m_ConfigDataEnchantTemplateInfoData;
    private Dictionary<int, ConfigDataEquipmentInfo> m_ConfigDataEquipmentInfoData;
    private Dictionary<int, ConfigDataEquipmentLevelInfo> m_ConfigDataEquipmentLevelInfoData;
    private Dictionary<int, ConfigDataEquipmentLevelLimitInfo> m_ConfigDataEquipmentLevelLimitInfoData;
    private Dictionary<int, ConfigDataErrorCodeStringTable> m_ConfigDataErrorCodeStringTableData;
    private Dictionary<int, ConfigDataEternalShrineInfo> m_ConfigDataEternalShrineInfoData;
    private Dictionary<int, ConfigDataEternalShrineLevelInfo> m_ConfigDataEternalShrineLevelInfoData;
    private Dictionary<int, ConfigDataEventGiftInfo> m_ConfigDataEventGiftInfoData;
    private Dictionary<int, ConfigDataEventInfo> m_ConfigDataEventInfoData;
    private Dictionary<int, ConfigDataExplanationInfo> m_ConfigDataExplanationInfoData;
    private Dictionary<int, ConfigDataFixedStoreItemInfo> m_ConfigDataFixedStoreItemInfoData;
    private Dictionary<int, ConfigDataFlyObjectInfo> m_ConfigDataFlyObjectInfoData;
    private Dictionary<int, ConfigDataFreeCardPoolGroupInfo> m_ConfigDataFreeCardPoolGroupInfoData;
    private Dictionary<int, ConfigDataFxFlipInfo> m_ConfigDataFxFlipInfoData;
    private Dictionary<int, ConfigDataGameFunctionOpenInfo> m_ConfigDataGameFunctionOpenInfoData;
    private Dictionary<int, ConfigDataGamePlayTeamTypeInfo> m_ConfigDataGamePlayTeamTypeInfoData;
    private Dictionary<int, ConfigDataGiftCDKeyInfo> m_ConfigDataGiftCDKeyInfoData;
    private Dictionary<int, ConfigDataGiftStoreItemInfo> m_ConfigDataGiftStoreItemInfoData;
    private Dictionary<int, ConfigDataGoddessDialogInfo> m_ConfigDataGoddessDialogInfoData;
    private Dictionary<int, ConfigDataGroupBehavior> m_ConfigDataGroupBehaviorData;
    private Dictionary<int, ConfigDataGuildMassiveCombatDifficultyInfo> m_ConfigDataGuildMassiveCombatDifficultyInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo> m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatLevelInfo> m_ConfigDataGuildMassiveCombatLevelInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatRewardsInfo> m_ConfigDataGuildMassiveCombatRewardsInfoData;
    private Dictionary<int, ConfigDataGuildMassiveCombatStrongholdInfo> m_ConfigDataGuildMassiveCombatStrongholdInfoData;
    private Dictionary<int, ConfigDataHeadFrameInfo> m_ConfigDataHeadFrameInfoData;
    private Dictionary<int, ConfigDataHeroAnthemInfo> m_ConfigDataHeroAnthemInfoData;
    private Dictionary<int, ConfigDataHeroAnthemLevelInfo> m_ConfigDataHeroAnthemLevelInfoData;
    private Dictionary<int, ConfigDataHeroAssistantTaskGeneralInfo> m_ConfigDataHeroAssistantTaskGeneralInfoData;
    private Dictionary<int, ConfigDataHeroAssistantTaskInfo> m_ConfigDataHeroAssistantTaskInfoData;
    private Dictionary<int, ConfigDataHeroAssistantTaskScheduleInfo> m_ConfigDataHeroAssistantTaskScheduleInfoData;
    private Dictionary<int, ConfigDataHeroBiographyInfo> m_ConfigDataHeroBiographyInfoData;
    private Dictionary<int, ConfigDataHeroConfessionInfo> m_ConfigDataHeroConfessionInfoData;
    private Dictionary<int, ConfigDataHeroDungeonLevelInfo> m_ConfigDataHeroDungeonLevelInfoData;
    private Dictionary<int, ConfigDataHeroFavorabilityLevelInfo> m_ConfigDataHeroFavorabilityLevelInfoData;
    private Dictionary<int, ConfigDataHeroFetterInfo> m_ConfigDataHeroFetterInfoData;
    private Dictionary<int, ConfigDataHeroHeartFetterInfo> m_ConfigDataHeroHeartFetterInfoData;
    private Dictionary<int, ConfigDataHeroInfo> m_ConfigDataHeroInfoData;
    private Dictionary<int, ConfigDataHeroInformationInfo> m_ConfigDataHeroInformationInfoData;
    private Dictionary<int, ConfigDataHeroInteractionInfo> m_ConfigDataHeroInteractionInfoData;
    private Dictionary<int, ConfigDataHeroLevelInfo> m_ConfigDataHeroLevelInfoData;
    private Dictionary<int, ConfigDataHeroPerformanceInfo> m_ConfigDataHeroPerformanceInfoData;
    private Dictionary<int, ConfigDataHeroPerformanceWordInfo> m_ConfigDataHeroPerformanceWordInfoData;
    private Dictionary<int, ConfigDataHeroPhantomInfo> m_ConfigDataHeroPhantomInfoData;
    private Dictionary<int, ConfigDataHeroPhantomLevelInfo> m_ConfigDataHeroPhantomLevelInfoData;
    private Dictionary<int, ConfigDataHeroSkinInfo> m_ConfigDataHeroSkinInfoData;
    private Dictionary<int, ConfigDataHeroSkinSelfSelectedBoxInfo> m_ConfigDataHeroSkinSelfSelectedBoxInfoData;
    private Dictionary<int, ConfigDataHeroStarInfo> m_ConfigDataHeroStarInfoData;
    private Dictionary<int, ConfigDataHeroTagInfo> m_ConfigDataHeroTagInfoData;
    private Dictionary<int, ConfigDataHeroTrainningInfo> m_ConfigDataHeroTrainningInfoData;
    private Dictionary<int, ConfigDataHeroTrainningLevelInfo> m_ConfigDataHeroTrainningLevelInfoData;
    private Dictionary<int, ConfigDataInitInfo> m_ConfigDataInitInfoData;
    private Dictionary<int, ConfigDataItemInfo> m_ConfigDataItemInfoData;
    private Dictionary<int, ConfigDataJobConnectionInfo> m_ConfigDataJobConnectionInfoData;
    private Dictionary<int, ConfigDataJobInfo> m_ConfigDataJobInfoData;
    private Dictionary<int, ConfigDataJobLevelInfo> m_ConfigDataJobLevelInfoData;
    private Dictionary<int, ConfigDataJobMaterialInfo> m_ConfigDataJobMaterialInfoData;
    private Dictionary<int, ConfigDataJobUnlockConditionInfo> m_ConfigDataJobUnlockConditionInfoData;
    private Dictionary<int, ConfigDataLanguageDataInfo> m_ConfigDataLanguageDataInfoData;
    private Dictionary<int, ConfigDataLinkageHeroInfo> m_ConfigDataLinkageHeroInfoData;
    private Dictionary<int, ConfigDataMailInfo> m_ConfigDataMailInfoData;
    private Dictionary<int, ConfigDataMemoryCorridorInfo> m_ConfigDataMemoryCorridorInfoData;
    private Dictionary<int, ConfigDataMemoryCorridorLevelInfo> m_ConfigDataMemoryCorridorLevelInfoData;
    private Dictionary<int, ConfigDataMissionExtNoviceInfo> m_ConfigDataMissionExtNoviceInfoData;
    private Dictionary<int, ConfigDataMissionExtRefluxInfo> m_ConfigDataMissionExtRefluxInfoData;
    private Dictionary<int, ConfigDataMissionInfo> m_ConfigDataMissionInfoData;
    private Dictionary<int, ConfigDataModelSkinResourceInfo> m_ConfigDataModelSkinResourceInfoData;
    private Dictionary<int, ConfigDataMonsterInfo> m_ConfigDataMonsterInfoData;
    private Dictionary<int, ConfigDataMonthCardInfo> m_ConfigDataMonthCardInfoData;
    private Dictionary<int, ConfigDataMusicAppreciateTable> m_ConfigDataMusicAppreciateTableData;
    private Dictionary<int, ConfigDataNoviceRewardInfo> m_ConfigDataNoviceRewardInfoData;
    private Dictionary<int, ConfigDataOperationalActivityInfo> m_ConfigDataOperationalActivityInfoData;
    private Dictionary<int, ConfigDataOperationalActivityItemGroupInfo> m_ConfigDataOperationalActivityItemGroupInfoData;
    private Dictionary<int, ConfigDataPeakArenaBattleInfo> m_ConfigDataPeakArenaBattleInfoData;
    private Dictionary<int, ConfigDataPeakArenaDanInfo> m_ConfigDataPeakArenaDanInfoData;
    private Dictionary<int, ConfigDataPeakArenaDanRewardSettleTimeInfo> m_ConfigDataPeakArenaDanRewardSettleTimeInfoData;
    private Dictionary<int, ConfigDataPeakArenaFirstWeekWinRewardInfo> m_ConfigDataPeakArenaFirstWeekWinRewardInfoData;
    private Dictionary<int, ConfigDataPeakArenaKnockoutMatchTime> m_ConfigDataPeakArenaKnockoutMatchTimeData;
    private Dictionary<int, ConfigDataPeakArenaPublicHeroPoolInfo> m_ConfigDataPeakArenaPublicHeroPoolInfoData;
    private Dictionary<int, ConfigDataPeakArenaRankingRewardInfo> m_ConfigDataPeakArenaRankingRewardInfoData;
    private Dictionary<int, ConfigDataPeakArenaSeasonInfo> m_ConfigDataPeakArenaSeasonInfoData;
    private Dictionary<int, ConfigDataPerformanceInfo> m_ConfigDataPerformanceInfoData;
    private Dictionary<int, ConfigDataPlayerLevelInfo> m_ConfigDataPlayerLevelInfoData;
    private Dictionary<int, ConfigDataPrefabStateInfo> m_ConfigDataPrefabStateInfoData;
    private Dictionary<int, ConfigDataPropertyModifyInfo> m_ConfigDataPropertyModifyInfoData;
    private Dictionary<int, ConfigDataProtagonistInfo> m_ConfigDataProtagonistInfoData;
    private Dictionary<int, ConfigDataPVPBattleInfo> m_ConfigDataPVPBattleInfoData;
    private Dictionary<int, ConfigDataRafflePoolInfo> m_ConfigDataRafflePoolInfoData;
    private Dictionary<int, ConfigDataRandomBoxInfo> m_ConfigDataRandomBoxInfoData;
    private Dictionary<int, ConfigDataRandomDropRewardInfo> m_ConfigDataRandomDropRewardInfoData;
    private Dictionary<int, ConfigDataRandomNameHead> m_ConfigDataRandomNameHeadData;
    private Dictionary<int, ConfigDataRandomNameMiddle> m_ConfigDataRandomNameMiddleData;
    private Dictionary<int, ConfigDataRandomNameTail> m_ConfigDataRandomNameTailData;
    private Dictionary<int, ConfigDataRandomStoreInfo> m_ConfigDataRandomStoreInfoData;
    private Dictionary<int, ConfigDataRandomStoreItemInfo> m_ConfigDataRandomStoreItemInfoData;
    private Dictionary<int, ConfigDataRandomStoreLevelZoneInfo> m_ConfigDataRandomStoreLevelZoneInfoData;
    private Dictionary<int, ConfigDataRankInfo> m_ConfigDataRankInfoData;
    private Dictionary<int, ConfigDataRealTimePVPBattleInfo> m_ConfigDataRealTimePVPBattleInfoData;
    private Dictionary<int, ConfigDataRealTimePVPDanInfo> m_ConfigDataRealTimePVPDanInfoData;
    private Dictionary<int, ConfigDataRealTimePVPDanRewardInfo> m_ConfigDataRealTimePVPDanRewardInfoData;
    private Dictionary<int, ConfigDataRealTimePVPLocalRankingRewardInfo> m_ConfigDataRealTimePVPLocalRankingRewardInfoData;
    private Dictionary<int, ConfigDataRealTimePVPNoviceMatchmakingInfo> m_ConfigDataRealTimePVPNoviceMatchmakingInfoData;
    private Dictionary<int, ConfigDataRealTimePVPRankingRewardInfo> m_ConfigDataRealTimePVPRankingRewardInfoData;
    private Dictionary<int, ConfigDataRealTimePVPSettleTimeInfo> m_ConfigDataRealTimePVPSettleTimeInfoData;
    private Dictionary<int, ConfigDataRealTimePVPWinsBonusInfo> m_ConfigDataRealTimePVPWinsBonusInfoData;
    private Dictionary<int, ConfigDataRechargeStoreItemInfo> m_ConfigDataRechargeStoreItemInfoData;
    private Dictionary<int, ConfigDataRefineryStoneInfo> m_ConfigDataRefineryStoneInfoData;
    private Dictionary<int, ConfigDataRefineryStonePropertyTemplateInfo> m_ConfigDataRefineryStonePropertyTemplateInfoData;
    private Dictionary<int, ConfigDataRefluxRewardInfo> m_ConfigDataRefluxRewardInfoData;
    private Dictionary<int, ConfigDataRegionInfo> m_ConfigDataRegionInfoData;
    private Dictionary<int, ConfigDataResonanceInfo> m_ConfigDataResonanceInfoData;
    private Dictionary<int, ConfigDataRiftChapterInfo> m_ConfigDataRiftChapterInfoData;
    private Dictionary<int, ConfigDataRiftLevelInfo> m_ConfigDataRiftLevelInfoData;
    private Dictionary<int, ConfigDataScenarioInfo> m_ConfigDataScenarioInfoData;
    private Dictionary<int, ConfigDataScoreLevelInfo> m_ConfigDataScoreLevelInfoData;
    private Dictionary<int, ConfigDataSelectContentInfo> m_ConfigDataSelectContentInfoData;
    private Dictionary<int, ConfigDataSelectProbabilityInfo> m_ConfigDataSelectProbabilityInfoData;
    private Dictionary<int, ConfigDataSelfSelectedBoxInfo> m_ConfigDataSelfSelectedBoxInfoData;
    private Dictionary<int, ConfigDataSensitiveWords> m_ConfigDataSensitiveWordsData;
    private Dictionary<int, ConfigDataSignRewardInfo> m_ConfigDataSignRewardInfoData;
    private Dictionary<int, ConfigDataSkillInfo> m_ConfigDataSkillInfoData;
    private Dictionary<int, ConfigDataSmallExpressionPathInfo> m_ConfigDataSmallExpressionPathInfoData;
    private Dictionary<int, ConfigDataSoldierInfo> m_ConfigDataSoldierInfoData;
    private Dictionary<int, ConfigDataSoldierSkinInfo> m_ConfigDataSoldierSkinInfoData;
    private Dictionary<int, ConfigDataSoundTable> m_ConfigDataSoundTableData;
    private Dictionary<int, ConfigDataSpineAnimationSoundTable> m_ConfigDataSpineAnimationSoundTableData;
    private Dictionary<int, ConfigDataSST_0_CN> m_ConfigDataSST_0_CNData;
    private Dictionary<int, ConfigDataSST_0_EN> m_ConfigDataSST_0_ENData;
    private Dictionary<int, ConfigDataSST_1_CN> m_ConfigDataSST_1_CNData;
    private Dictionary<int, ConfigDataSST_1_EN> m_ConfigDataSST_1_ENData;
    private Dictionary<int, ConfigDataStaticBoxInfo> m_ConfigDataStaticBoxInfoData;
    private Dictionary<int, ConfigDataStoreCurrencyInfo> m_ConfigDataStoreCurrencyInfoData;
    private Dictionary<int, ConfigDataStoreInfo> m_ConfigDataStoreInfoData;
    private Dictionary<int, ConfigDataStoryOutlineInfo> m_ConfigDataStoryOutlineInfoData;
    private Dictionary<int, ConfigDataStringTable> m_ConfigDataStringTableData;
    private Dictionary<int, ConfigDataStringTableForListInfo> m_ConfigDataStringTableForListInfoData;
    private Dictionary<int, ConfigDataSurveyInfo> m_ConfigDataSurveyInfoData;
    private Dictionary<int, ConfigDataSystemBroadcastInfo> m_ConfigDataSystemBroadcastInfoData;
    private Dictionary<int, ConfigDataTarotInfo> m_ConfigDataTarotInfoData;
    private Dictionary<int, ConfigDataTerrainEffectInfo> m_ConfigDataTerrainEffectInfoData;
    private Dictionary<int, ConfigDataTerrainInfo> m_ConfigDataTerrainInfoData;
    private Dictionary<int, ConfigDataThearchyTrialInfo> m_ConfigDataThearchyTrialInfoData;
    private Dictionary<int, ConfigDataThearchyTrialLevelInfo> m_ConfigDataThearchyTrialLevelInfoData;
    private Dictionary<int, ConfigDataTicketLimitGameFunctionTypeInfo> m_ConfigDataTicketLimitGameFunctionTypeInfoData;
    private Dictionary<int, ConfigDataTicketLimitInfo> m_ConfigDataTicketLimitInfoData;
    private Dictionary<int, ConfigDataTitleInfo> m_ConfigDataTitleInfoData;
    private Dictionary<int, ConfigDataTowerBattleRuleInfo> m_ConfigDataTowerBattleRuleInfoData;
    private Dictionary<int, ConfigDataTowerBonusHeroGroupInfo> m_ConfigDataTowerBonusHeroGroupInfoData;
    private Dictionary<int, ConfigDataTowerFloorInfo> m_ConfigDataTowerFloorInfoData;
    private Dictionary<int, ConfigDataTowerLevelInfo> m_ConfigDataTowerLevelInfoData;
    private Dictionary<int, ConfigDataTrainingCourseInfo> m_ConfigDataTrainingCourseInfoData;
    private Dictionary<int, ConfigDataTrainingRoomInfo> m_ConfigDataTrainingRoomInfoData;
    private Dictionary<int, ConfigDataTrainingRoomLevelInfo> m_ConfigDataTrainingRoomLevelInfoData;
    private Dictionary<int, ConfigDataTrainingTechInfo> m_ConfigDataTrainingTechInfoData;
    private Dictionary<int, ConfigDataTrainingTechLevelInfo> m_ConfigDataTrainingTechLevelInfoData;
    private Dictionary<int, ConfigDataTranslate> m_ConfigDataTranslateData;
    private Dictionary<int, ConfigDataTreasureLevelInfo> m_ConfigDataTreasureLevelInfoData;
    private Dictionary<int, ConfigDataUnchartedScoreInfo> m_ConfigDataUnchartedScoreInfoData;
    private Dictionary<int, ConfigDataUnchartedScoreModelInfo> m_ConfigDataUnchartedScoreModelInfoData;
    private Dictionary<int, ConfigDataUnchartedScoreRewardGroupInfo> m_ConfigDataUnchartedScoreRewardGroupInfoData;
    private Dictionary<int, ConfigDataUserGuide> m_ConfigDataUserGuideData;
    private Dictionary<int, ConfigDataUserGuideDialogInfo> m_ConfigDataUserGuideDialogInfoData;
    private Dictionary<int, ConfigDataUserGuideStep> m_ConfigDataUserGuideStepData;
    private Dictionary<int, ConfigDataVersionInfo> m_ConfigDataVersionInfoData;
    private Dictionary<int, ConfigDataWaypointInfo> m_ConfigDataWaypointInfoData;
    private Dictionary<int, ConfigDataWorldMapInfo> m_ConfigDataWorldMapInfoData;
    private Dictionary<int, ConfigDataST_CN> m_ConfigDataST_CNData;
    private Dictionary<int, ConfigDataST_EN> m_ConfigDataST_ENData;
    private const int SignRewardSpecificErrorIdIncrementValue = -1;
    private const int ChapterSpecificErrorIdIncrementValue = -2;
    private const int EventSpecificErrorIdIncrementValue = -3;
    private const int RiftLevelSpecificErrorIdIncrementValue = -4;
    private const int InitInfoSpecificErrorIdIncrementValue = -5;
    private const int RandomDropSpecificErrorIdIncrementValue = -6;
    private const int ScenarioSpecificErrorIdIncrementValue = -7;
    private const int FreeCardPoolSpecificErrorIdIncrementValue = -8;
    private const int CrystalCardPoolSpecificErrorIdIncrementValue = -9;
    private const int ActivityCardPoolSpecificErrorIdIncrementValue = -10;
    private const int HeroJobUnlockConditionsSpecificErrorIdIncrementValue = -23;
    private const int ErrCodeFixedStoreItemFirstRewardSpecificErrorIdIncrementValue = -33;
    private const int ErrCodeFixedStoreSellItemSpecificErrorIdIncrementValue = -34;
    private const int ErrCodeCoachFavorabilityLevelUpCostItemSpecificErrorIdIncrementValue = -40;
    private const int ErrCodeHeroDungeonStarRewardSpecificErrorIdIncrementValue = -53;
    private const int ErrCodeHeroDungeonLevelAchievementRewardSpecificErrorIdIncrementValue = -54;
    private const int ErrCodeOperationalActivityItemGroupItemSpecificErrorIdIncreasementValue = -65;
    private const int ErrCodeStaticBoxItemSpecificErrorIdIncreasementValue = -68;
    private const int ErrCodeBattleTreasureRewardSpecificErrorIdIncreasementValue = -70;
    public const int ErrCodeGiftStoreFirstRewardSpecificErrorIdIncreasementValue = -91;
    private Dictionary<System.Type, object> m_emptyListMap;
    private List<ConfigDataAncientCallSingleBossPointsInfo> m_bossPointsInfo;
    private ArmyRelationData[,] m_armyRelationTable;
    private Dictionary<string, string> m_fxFlipTable;
    private Dictionary<int, ConfigDataGameFunctionOpenInfo> m_gameFunctionOpenInfos;
    private Dictionary<int, ConfigDataAncientCallRecordRewardGroupInfo> m_ancientCallRecordRewardGroupInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientConfigDataLoader()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool StartInitializeFromAsset(Action<bool> onEnd, out int initLoadDataCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override HashSet<string> GetAllInitLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override Dictionary<string, List<ClientConfigDataLoaderBase.LazyLoadConfigAssetInfo>> GetAllLazyLoadConfigDataAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator OnInitLoadFromAssetEndWorker(
      int totalWorkerCount,
      int workerId,
      int loadCountForSingleYield,
      Action<bool> onEnd,
      List<string> skipTypeList = null,
      List<string> filterTypeList = null)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override void AddConfigDataItemForLuaDummyType(string typeName, DummyType dataItem)
    {
    }

    protected override List<string> GetAllInitLoadConfigDataTypeNameForLuaDummy()
    {
      return new List<string>();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLazyLoadFromAssetEnd(
      string configDataName,
      string configAssetName,
      UnityEngine.Object lasset,
      Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string GetLazyLoadConfigAssetNameByKey(string configDataName, int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataActivityCardPoolGroupInfo GetConfigDataActivityCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataActivityCardPoolGroupInfo>> GetAllConfigDataActivityCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataActivityCardPoolGroupInfo>>) this.m_ConfigDataActivityCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataActivityCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallBehavioralDescInfo GetConfigDataAncientCallBehavioralDescInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAncientCallBehavioralDescInfo>> GetAllConfigDataAncientCallBehavioralDescInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAncientCallBehavioralDescInfo>>) this.m_ConfigDataAncientCallBehavioralDescInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAncientCallBehavioralDescInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallBossInfo GetConfigDataAncientCallBossInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAncientCallBossInfo>> GetAllConfigDataAncientCallBossInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAncientCallBossInfo>>) this.m_ConfigDataAncientCallBossInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAncientCallBossInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallPeriodInfo GetConfigDataAncientCallPeriodInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAncientCallPeriodInfo>> GetAllConfigDataAncientCallPeriodInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAncientCallPeriodInfo>>) this.m_ConfigDataAncientCallPeriodInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAncientCallPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallRecordRewardInfo GetConfigDataAncientCallRecordRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAncientCallRecordRewardInfo>> GetAllConfigDataAncientCallRecordRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAncientCallRecordRewardInfo>>) this.m_ConfigDataAncientCallRecordRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAncientCallRecordRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallSingleBossPointsInfo GetConfigDataAncientCallSingleBossPointsInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAncientCallSingleBossPointsInfo>> GetAllConfigDataAncientCallSingleBossPointsInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAncientCallSingleBossPointsInfo>>) this.m_ConfigDataAncientCallSingleBossPointsInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAncientCallSingleBossPointsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAncientCallTotalBossPointsInfo GetConfigDataAncientCallTotalBossPointsInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAncientCallTotalBossPointsInfo>> GetAllConfigDataAncientCallTotalBossPointsInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAncientCallTotalBossPointsInfo>>) this.m_ConfigDataAncientCallTotalBossPointsInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAncientCallTotalBossPointsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiGymInfo GetConfigDataAnikiGymInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAnikiGymInfo>> GetAllConfigDataAnikiGymInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAnikiGymInfo>>) this.m_ConfigDataAnikiGymInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAnikiGymInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiLevelInfo GetConfigDataAnikiLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAnikiLevelInfo>> GetAllConfigDataAnikiLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAnikiLevelInfo>>) this.m_ConfigDataAnikiLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAnikiLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaBattleInfo GetConfigDataArenaBattleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaBattleInfo>> GetAllConfigDataArenaBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaBattleInfo>>) this.m_ConfigDataArenaBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaDefendRuleInfo GetConfigDataArenaDefendRuleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaDefendRuleInfo>> GetAllConfigDataArenaDefendRuleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaDefendRuleInfo>>) this.m_ConfigDataArenaDefendRuleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaDefendRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaLevelInfo GetConfigDataArenaLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaLevelInfo>> GetAllConfigDataArenaLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaLevelInfo>>) this.m_ConfigDataArenaLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaOpponentPointZoneInfo GetConfigDataArenaOpponentPointZoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaOpponentPointZoneInfo>> GetAllConfigDataArenaOpponentPointZoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaOpponentPointZoneInfo>>) this.m_ConfigDataArenaOpponentPointZoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaOpponentPointZoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaRankRewardInfo GetConfigDataArenaRankRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaRankRewardInfo>> GetAllConfigDataArenaRankRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaRankRewardInfo>>) this.m_ConfigDataArenaRankRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaRankRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaRobotInfo GetConfigDataArenaRobotInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaRobotInfo>> GetAllConfigDataArenaRobotInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaRobotInfo>>) this.m_ConfigDataArenaRobotInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaRobotInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaSettleTimeInfo GetConfigDataArenaSettleTimeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaSettleTimeInfo>> GetAllConfigDataArenaSettleTimeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaSettleTimeInfo>>) this.m_ConfigDataArenaSettleTimeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaSettleTimeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArenaVictoryPointsRewardInfo GetConfigDataArenaVictoryPointsRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArenaVictoryPointsRewardInfo>> GetAllConfigDataArenaVictoryPointsRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArenaVictoryPointsRewardInfo>>) this.m_ConfigDataArenaVictoryPointsRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArenaVictoryPointsRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyInfo GetConfigDataArmyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArmyInfo>> GetAllConfigDataArmyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArmyInfo>>) this.m_ConfigDataArmyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArmyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyRelation GetConfigDataArmyRelation(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataArmyRelation>> GetAllConfigDataArmyRelation()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataArmyRelation>>) this.m_ConfigDataArmyRelationData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAssetReplaceInfo GetConfigDataAssetReplaceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataAssetReplaceInfo>> GetAllConfigDataAssetReplaceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataAssetReplaceInfo>>) this.m_ConfigDataAssetReplaceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataAssetReplaceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBanditInfo GetConfigDataBanditInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBanditInfo>> GetAllConfigDataBanditInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBanditInfo>>) this.m_ConfigDataBanditInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBanditInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleAchievementInfo GetConfigDataBattleAchievementInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementInfo>> GetAllConfigDataBattleAchievementInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementInfo>>) this.m_ConfigDataBattleAchievementInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleAchievementInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleAchievementRelatedInfo GetConfigDataBattleAchievementRelatedInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementRelatedInfo>> GetAllConfigDataBattleAchievementRelatedInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleAchievementRelatedInfo>>) this.m_ConfigDataBattleAchievementRelatedInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleAchievementRelatedInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleDialogInfo GetConfigDataBattleDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleDialogInfo>> GetAllConfigDataBattleDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleDialogInfo>>) this.m_ConfigDataBattleDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventActionInfo GetConfigDataBattleEventActionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleEventActionInfo>> GetAllConfigDataBattleEventActionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleEventActionInfo>>) this.m_ConfigDataBattleEventActionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleEventActionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleEventTriggerInfo GetConfigDataBattleEventTriggerInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleEventTriggerInfo>> GetAllConfigDataBattleEventTriggerInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleEventTriggerInfo>>) this.m_ConfigDataBattleEventTriggerInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleEventTriggerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlefieldInfo GetConfigDataBattlefieldInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattlefieldInfo>> GetAllConfigDataBattlefieldInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattlefieldInfo>>) this.m_ConfigDataBattlefieldInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattlefieldInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleInfo GetConfigDataBattleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleInfo>> GetAllConfigDataBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleInfo>>) this.m_ConfigDataBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleLoseConditionInfo GetConfigDataBattleLoseConditionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleLoseConditionInfo>> GetAllConfigDataBattleLoseConditionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleLoseConditionInfo>>) this.m_ConfigDataBattleLoseConditionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleLoseConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattlePerformInfo GetConfigDataBattlePerformInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattlePerformInfo>> GetAllConfigDataBattlePerformInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattlePerformInfo>>) this.m_ConfigDataBattlePerformInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattlePerformInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleRandomArmyInfo GetConfigDataBattleRandomArmyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleRandomArmyInfo>> GetAllConfigDataBattleRandomArmyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleRandomArmyInfo>>) this.m_ConfigDataBattleRandomArmyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleRandomArmyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleRandomTalentInfo GetConfigDataBattleRandomTalentInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleRandomTalentInfo>> GetAllConfigDataBattleRandomTalentInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleRandomTalentInfo>>) this.m_ConfigDataBattleRandomTalentInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleRandomTalentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleTreasureInfo GetConfigDataBattleTreasureInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleTreasureInfo>> GetAllConfigDataBattleTreasureInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleTreasureInfo>>) this.m_ConfigDataBattleTreasureInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleTreasureInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleWinConditionInfo GetConfigDataBattleWinConditionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBattleWinConditionInfo>> GetAllConfigDataBattleWinConditionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBattleWinConditionInfo>>) this.m_ConfigDataBattleWinConditionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBattleWinConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehavior GetConfigDataBehavior(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBehavior>> GetAllConfigDataBehavior()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBehavior>>) this.m_ConfigDataBehaviorData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBehaviorChangeRule GetConfigDataBehaviorChangeRule(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBehaviorChangeRule>> GetAllConfigDataBehaviorChangeRule()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBehaviorChangeRule>>) this.m_ConfigDataBehaviorChangeRuleData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBehaviorChangeRule()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBigExpressionInfo GetConfigDataBigExpressionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBigExpressionInfo>> GetAllConfigDataBigExpressionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBigExpressionInfo>>) this.m_ConfigDataBigExpressionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBigExpressionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuffInfo GetConfigDataBuffInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBuffInfo>> GetAllConfigDataBuffInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBuffInfo>>) this.m_ConfigDataBuffInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBuffInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuyArenaTicketInfo GetConfigDataBuyArenaTicketInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBuyArenaTicketInfo>> GetAllConfigDataBuyArenaTicketInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBuyArenaTicketInfo>>) this.m_ConfigDataBuyArenaTicketInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBuyArenaTicketInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBuyEnergyInfo GetConfigDataBuyEnergyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataBuyEnergyInfo>> GetAllConfigDataBuyEnergyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataBuyEnergyInfo>>) this.m_ConfigDataBuyEnergyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataBuyEnergyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolGroupInfo GetConfigDataCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCardPoolGroupInfo>> GetAllConfigDataCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCardPoolGroupInfo>>) this.m_ConfigDataCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCardPoolInfo GetConfigDataCardPoolInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCardPoolInfo>> GetAllConfigDataCardPoolInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCardPoolInfo>>) this.m_ConfigDataCardPoolInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCardPoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataChallengeLevelInfo GetConfigDataChallengeLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataChallengeLevelInfo>> GetAllConfigDataChallengeLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataChallengeLevelInfo>>) this.m_ConfigDataChallengeLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetConfigDataCharImageInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCharImageInfo>> GetAllConfigDataCharImageInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCharImageInfo>>) this.m_ConfigDataCharImageInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo GetConfigDataCharImageSkinResourceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCharImageSkinResourceInfo>> GetAllConfigDataCharImageSkinResourceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCharImageSkinResourceInfo>>) this.m_ConfigDataCharImageSkinResourceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCharImageSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityChallengeLevelInfo GetConfigDataCollectionActivityChallengeLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityChallengeLevelInfo>> GetAllConfigDataCollectionActivityChallengeLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityChallengeLevelInfo>>) this.m_ConfigDataCollectionActivityChallengeLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityCurrencyItemExtInfo GetConfigDataCollectionActivityCurrencyItemExtInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityCurrencyItemExtInfo>> GetAllConfigDataCollectionActivityCurrencyItemExtInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityCurrencyItemExtInfo>>) this.m_ConfigDataCollectionActivityCurrencyItemExtInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityCurrencyItemExtInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityExchangeTableInfo GetConfigDataCollectionActivityExchangeTableInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityExchangeTableInfo>> GetAllConfigDataCollectionActivityExchangeTableInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityExchangeTableInfo>>) this.m_ConfigDataCollectionActivityExchangeTableInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityExchangeTableInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetConfigDataCollectionActivityInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityInfo>> GetAllConfigDataCollectionActivityInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityInfo>>) this.m_ConfigDataCollectionActivityInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityLootLevelInfo GetConfigDataCollectionActivityLootLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityLootLevelInfo>> GetAllConfigDataCollectionActivityLootLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityLootLevelInfo>>) this.m_ConfigDataCollectionActivityLootLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityLootLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityMapInfo GetConfigDataCollectionActivityMapInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityMapInfo>> GetAllConfigDataCollectionActivityMapInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityMapInfo>>) this.m_ConfigDataCollectionActivityMapInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetConfigDataCollectionActivityScenarioLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScenarioLevelInfo>> GetAllConfigDataCollectionActivityScenarioLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScenarioLevelInfo>>) this.m_ConfigDataCollectionActivityScenarioLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityScenarioLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScoreRewardGroupInfo GetConfigDataCollectionActivityScoreRewardGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScoreRewardGroupInfo>> GetAllConfigDataCollectionActivityScoreRewardGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityScoreRewardGroupInfo>>) this.m_ConfigDataCollectionActivityScoreRewardGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityScoreRewardGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityWaypointInfo GetConfigDataCollectionActivityWaypointInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityWaypointInfo>> GetAllConfigDataCollectionActivityWaypointInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionActivityWaypointInfo>>) this.m_ConfigDataCollectionActivityWaypointInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionActivityWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionEventInfo GetConfigDataCollectionEventInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCollectionEventInfo>> GetAllConfigDataCollectionEventInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCollectionEventInfo>>) this.m_ConfigDataCollectionEventInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCollectionEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfessionDialogInfo GetConfigDataConfessionDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataConfessionDialogInfo>> GetAllConfigDataConfessionDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfessionDialogInfo>>) this.m_ConfigDataConfessionDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfessionDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfessionLetterInfo GetConfigDataConfessionLetterInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataConfessionLetterInfo>> GetAllConfigDataConfessionLetterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfessionLetterInfo>>) this.m_ConfigDataConfessionLetterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfessionLetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfigableConst GetConfigDataConfigableConst(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataConfigableConst>> GetAllConfigDataConfigableConst()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfigableConst>>) this.m_ConfigDataConfigableConstData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfigableConst()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataConfigIDRangeInfo GetConfigDataConfigIDRangeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataConfigIDRangeInfo>> GetAllConfigDataConfigIDRangeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataConfigIDRangeInfo>>) this.m_ConfigDataConfigIDRangeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataConfigIDRangeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleInfo GetConfigDataCooperateBattleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleInfo>> GetAllConfigDataCooperateBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleInfo>>) this.m_ConfigDataCooperateBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCooperateBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCooperateBattleLevelInfo GetConfigDataCooperateBattleLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleLevelInfo>> GetAllConfigDataCooperateBattleLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCooperateBattleLevelInfo>>) this.m_ConfigDataCooperateBattleLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCooperateBattleLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCrystalCardPoolGroupInfo GetConfigDataCrystalCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCrystalCardPoolGroupInfo>> GetAllConfigDataCrystalCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCrystalCardPoolGroupInfo>>) this.m_ConfigDataCrystalCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCrystalCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCutsceneInfo GetConfigDataCutsceneInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataCutsceneInfo>> GetAllConfigDataCutsceneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataCutsceneInfo>>) this.m_ConfigDataCutsceneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataCutsceneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDailyPushNotification GetConfigDataDailyPushNotification(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataDailyPushNotification>> GetAllConfigDataDailyPushNotification()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDailyPushNotification>>) this.m_ConfigDataDailyPushNotificationData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDailyPushNotification()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDeviceSetting GetConfigDataDeviceSetting(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataDeviceSetting>> GetAllConfigDataDeviceSetting()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDeviceSetting>>) this.m_ConfigDataDeviceSettingData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDeviceSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDialogChoiceInfo GetConfigDataDialogChoiceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataDialogChoiceInfo>> GetAllConfigDataDialogChoiceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDialogChoiceInfo>>) this.m_ConfigDataDialogChoiceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDialogChoiceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataDialogInfo GetConfigDataDialogInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataDialogInfo>> GetAllConfigDataDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataDialogInfo>>) this.m_ConfigDataDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantStoneInfo GetConfigDataEnchantStoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEnchantStoneInfo>> GetAllConfigDataEnchantStoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEnchantStoneInfo>>) this.m_ConfigDataEnchantStoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEnchantStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEnchantTemplateInfo GetConfigDataEnchantTemplateInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEnchantTemplateInfo>> GetAllConfigDataEnchantTemplateInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEnchantTemplateInfo>>) this.m_ConfigDataEnchantTemplateInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEnchantTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentInfo GetConfigDataEquipmentInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEquipmentInfo>> GetAllConfigDataEquipmentInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEquipmentInfo>>) this.m_ConfigDataEquipmentInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEquipmentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentLevelInfo GetConfigDataEquipmentLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelInfo>> GetAllConfigDataEquipmentLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelInfo>>) this.m_ConfigDataEquipmentLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEquipmentLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentLevelLimitInfo GetConfigDataEquipmentLevelLimitInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelLimitInfo>> GetAllConfigDataEquipmentLevelLimitInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEquipmentLevelLimitInfo>>) this.m_ConfigDataEquipmentLevelLimitInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEquipmentLevelLimitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataErrorCodeStringTable GetConfigDataErrorCodeStringTable(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataErrorCodeStringTable>> GetAllConfigDataErrorCodeStringTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataErrorCodeStringTable>>) this.m_ConfigDataErrorCodeStringTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataErrorCodeStringTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEternalShrineInfo GetConfigDataEternalShrineInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEternalShrineInfo>> GetAllConfigDataEternalShrineInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEternalShrineInfo>>) this.m_ConfigDataEternalShrineInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEternalShrineInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEternalShrineLevelInfo GetConfigDataEternalShrineLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEternalShrineLevelInfo>> GetAllConfigDataEternalShrineLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEternalShrineLevelInfo>>) this.m_ConfigDataEternalShrineLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEternalShrineLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventGiftInfo GetConfigDataEventGiftInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEventGiftInfo>> GetAllConfigDataEventGiftInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEventGiftInfo>>) this.m_ConfigDataEventGiftInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEventGiftInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo GetConfigDataEventInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataEventInfo>> GetAllConfigDataEventInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataEventInfo>>) this.m_ConfigDataEventInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataExplanationInfo GetConfigDataExplanationInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataExplanationInfo>> GetAllConfigDataExplanationInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataExplanationInfo>>) this.m_ConfigDataExplanationInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataExplanationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFixedStoreItemInfo GetConfigDataFixedStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataFixedStoreItemInfo>> GetAllConfigDataFixedStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFixedStoreItemInfo>>) this.m_ConfigDataFixedStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFixedStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFlyObjectInfo GetConfigDataFlyObjectInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataFlyObjectInfo>> GetAllConfigDataFlyObjectInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFlyObjectInfo>>) this.m_ConfigDataFlyObjectInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFlyObjectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFreeCardPoolGroupInfo GetConfigDataFreeCardPoolGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataFreeCardPoolGroupInfo>> GetAllConfigDataFreeCardPoolGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFreeCardPoolGroupInfo>>) this.m_ConfigDataFreeCardPoolGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFreeCardPoolGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataFxFlipInfo GetConfigDataFxFlipInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataFxFlipInfo>> GetAllConfigDataFxFlipInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataFxFlipInfo>>) this.m_ConfigDataFxFlipInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataFxFlipInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGameFunctionOpenInfo GetConfigDataGameFunctionOpenInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGameFunctionOpenInfo>> GetAllConfigDataGameFunctionOpenInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGameFunctionOpenInfo>>) this.m_ConfigDataGameFunctionOpenInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGameFunctionOpenInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGamePlayTeamTypeInfo GetConfigDataGamePlayTeamTypeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGamePlayTeamTypeInfo>> GetAllConfigDataGamePlayTeamTypeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGamePlayTeamTypeInfo>>) this.m_ConfigDataGamePlayTeamTypeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGamePlayTeamTypeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftCDKeyInfo GetConfigDataGiftCDKeyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGiftCDKeyInfo>> GetAllConfigDataGiftCDKeyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGiftCDKeyInfo>>) this.m_ConfigDataGiftCDKeyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGiftCDKeyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGiftStoreItemInfo GetConfigDataGiftStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGiftStoreItemInfo>> GetAllConfigDataGiftStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGiftStoreItemInfo>>) this.m_ConfigDataGiftStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGiftStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGoddessDialogInfo GetConfigDataGoddessDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGoddessDialogInfo>> GetAllConfigDataGoddessDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGoddessDialogInfo>>) this.m_ConfigDataGoddessDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGoddessDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGroupBehavior GetConfigDataGroupBehavior(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGroupBehavior>> GetAllConfigDataGroupBehavior()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGroupBehavior>>) this.m_ConfigDataGroupBehaviorData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGroupBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatDifficultyInfo GetConfigDataGuildMassiveCombatDifficultyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatDifficultyInfo>> GetAllConfigDataGuildMassiveCombatDifficultyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatDifficultyInfo>>) this.m_ConfigDataGuildMassiveCombatDifficultyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatDifficultyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo GetConfigDataGuildMassiveCombatIndividualPointsRewardsInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo>> GetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatIndividualPointsRewardsInfo>>) this.m_ConfigDataGuildMassiveCombatIndividualPointsRewardsInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatIndividualPointsRewardsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatLevelInfo GetConfigDataGuildMassiveCombatLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatLevelInfo>> GetAllConfigDataGuildMassiveCombatLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatLevelInfo>>) this.m_ConfigDataGuildMassiveCombatLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatRewardsInfo GetConfigDataGuildMassiveCombatRewardsInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatRewardsInfo>> GetAllConfigDataGuildMassiveCombatRewardsInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatRewardsInfo>>) this.m_ConfigDataGuildMassiveCombatRewardsInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatRewardsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGuildMassiveCombatStrongholdInfo GetConfigDataGuildMassiveCombatStrongholdInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatStrongholdInfo>> GetAllConfigDataGuildMassiveCombatStrongholdInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataGuildMassiveCombatStrongholdInfo>>) this.m_ConfigDataGuildMassiveCombatStrongholdInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataGuildMassiveCombatStrongholdInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeadFrameInfo GetConfigDataHeadFrameInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeadFrameInfo>> GetAllConfigDataHeadFrameInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeadFrameInfo>>) this.m_ConfigDataHeadFrameInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeadFrameInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAnthemInfo GetConfigDataHeroAnthemInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroAnthemInfo>> GetAllConfigDataHeroAnthemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAnthemInfo>>) this.m_ConfigDataHeroAnthemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAnthemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAnthemLevelInfo GetConfigDataHeroAnthemLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroAnthemLevelInfo>> GetAllConfigDataHeroAnthemLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAnthemLevelInfo>>) this.m_ConfigDataHeroAnthemLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAnthemLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskGeneralInfo GetConfigDataHeroAssistantTaskGeneralInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskGeneralInfo>> GetAllConfigDataHeroAssistantTaskGeneralInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskGeneralInfo>>) this.m_ConfigDataHeroAssistantTaskGeneralInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAssistantTaskGeneralInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskInfo GetConfigDataHeroAssistantTaskInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskInfo>> GetAllConfigDataHeroAssistantTaskInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskInfo>>) this.m_ConfigDataHeroAssistantTaskInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAssistantTaskInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAssistantTaskScheduleInfo GetConfigDataHeroAssistantTaskScheduleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskScheduleInfo>> GetAllConfigDataHeroAssistantTaskScheduleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroAssistantTaskScheduleInfo>>) this.m_ConfigDataHeroAssistantTaskScheduleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroAssistantTaskScheduleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroBiographyInfo GetConfigDataHeroBiographyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroBiographyInfo>> GetAllConfigDataHeroBiographyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroBiographyInfo>>) this.m_ConfigDataHeroBiographyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroBiographyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroConfessionInfo GetConfigDataHeroConfessionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroConfessionInfo>> GetAllConfigDataHeroConfessionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroConfessionInfo>>) this.m_ConfigDataHeroConfessionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroConfessionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroDungeonLevelInfo GetConfigDataHeroDungeonLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroDungeonLevelInfo>> GetAllConfigDataHeroDungeonLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroDungeonLevelInfo>>) this.m_ConfigDataHeroDungeonLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroDungeonLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFavorabilityLevelInfo GetConfigDataHeroFavorabilityLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroFavorabilityLevelInfo>> GetAllConfigDataHeroFavorabilityLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroFavorabilityLevelInfo>>) this.m_ConfigDataHeroFavorabilityLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroFavorabilityLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroFetterInfo GetConfigDataHeroFetterInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroFetterInfo>> GetAllConfigDataHeroFetterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroFetterInfo>>) this.m_ConfigDataHeroFetterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroHeartFetterInfo GetConfigDataHeroHeartFetterInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroHeartFetterInfo>> GetAllConfigDataHeroHeartFetterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroHeartFetterInfo>>) this.m_ConfigDataHeroHeartFetterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInfo GetConfigDataHeroInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroInfo>> GetAllConfigDataHeroInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroInfo>>) this.m_ConfigDataHeroInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInformationInfo GetConfigDataHeroInformationInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroInformationInfo>> GetAllConfigDataHeroInformationInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroInformationInfo>>) this.m_ConfigDataHeroInformationInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroInformationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroInteractionInfo GetConfigDataHeroInteractionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroInteractionInfo>> GetAllConfigDataHeroInteractionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroInteractionInfo>>) this.m_ConfigDataHeroInteractionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroInteractionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroLevelInfo GetConfigDataHeroLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroLevelInfo>> GetAllConfigDataHeroLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroLevelInfo>>) this.m_ConfigDataHeroLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPerformanceInfo GetConfigDataHeroPerformanceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceInfo>> GetAllConfigDataHeroPerformanceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceInfo>>) this.m_ConfigDataHeroPerformanceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPerformanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPerformanceWordInfo GetConfigDataHeroPerformanceWordInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceWordInfo>> GetAllConfigDataHeroPerformanceWordInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPerformanceWordInfo>>) this.m_ConfigDataHeroPerformanceWordInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPerformanceWordInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomInfo GetConfigDataHeroPhantomInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomInfo>> GetAllConfigDataHeroPhantomInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomInfo>>) this.m_ConfigDataHeroPhantomInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPhantomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroPhantomLevelInfo GetConfigDataHeroPhantomLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomLevelInfo>> GetAllConfigDataHeroPhantomLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroPhantomLevelInfo>>) this.m_ConfigDataHeroPhantomLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroPhantomLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinInfo GetConfigDataHeroSkinInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroSkinInfo>> GetAllConfigDataHeroSkinInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroSkinInfo>>) this.m_ConfigDataHeroSkinInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroSkinSelfSelectedBoxInfo GetConfigDataHeroSkinSelfSelectedBoxInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroSkinSelfSelectedBoxInfo>> GetAllConfigDataHeroSkinSelfSelectedBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroSkinSelfSelectedBoxInfo>>) this.m_ConfigDataHeroSkinSelfSelectedBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroSkinSelfSelectedBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroStarInfo GetConfigDataHeroStarInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroStarInfo>> GetAllConfigDataHeroStarInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroStarInfo>>) this.m_ConfigDataHeroStarInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroStarInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTagInfo GetConfigDataHeroTagInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroTagInfo>> GetAllConfigDataHeroTagInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroTagInfo>>) this.m_ConfigDataHeroTagInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroTagInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTrainningInfo GetConfigDataHeroTrainningInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningInfo>> GetAllConfigDataHeroTrainningInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningInfo>>) this.m_ConfigDataHeroTrainningInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroTrainningInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTrainningLevelInfo GetConfigDataHeroTrainningLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningLevelInfo>> GetAllConfigDataHeroTrainningLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataHeroTrainningLevelInfo>>) this.m_ConfigDataHeroTrainningLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataHeroTrainningLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataInitInfo GetConfigDataInitInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataInitInfo>> GetAllConfigDataInitInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataInitInfo>>) this.m_ConfigDataInitInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataInitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataItemInfo GetConfigDataItemInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataItemInfo>> GetAllConfigDataItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataItemInfo>>) this.m_ConfigDataItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobConnectionInfo GetConfigDataJobConnectionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataJobConnectionInfo>> GetAllConfigDataJobConnectionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobConnectionInfo>>) this.m_ConfigDataJobConnectionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobInfo GetConfigDataJobInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataJobInfo>> GetAllConfigDataJobInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobInfo>>) this.m_ConfigDataJobInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobLevelInfo GetConfigDataJobLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataJobLevelInfo>> GetAllConfigDataJobLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobLevelInfo>>) this.m_ConfigDataJobLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobMaterialInfo GetConfigDataJobMaterialInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataJobMaterialInfo>> GetAllConfigDataJobMaterialInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobMaterialInfo>>) this.m_ConfigDataJobMaterialInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobMaterialInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataJobUnlockConditionInfo GetConfigDataJobUnlockConditionInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataJobUnlockConditionInfo>> GetAllConfigDataJobUnlockConditionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataJobUnlockConditionInfo>>) this.m_ConfigDataJobUnlockConditionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataJobUnlockConditionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataLanguageDataInfo GetConfigDataLanguageDataInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataLanguageDataInfo>> GetAllConfigDataLanguageDataInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataLanguageDataInfo>>) this.m_ConfigDataLanguageDataInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataLanguageDataInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataLinkageHeroInfo GetConfigDataLinkageHeroInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataLinkageHeroInfo>> GetAllConfigDataLinkageHeroInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataLinkageHeroInfo>>) this.m_ConfigDataLinkageHeroInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataLinkageHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMailInfo GetConfigDataMailInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMailInfo>> GetAllConfigDataMailInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMailInfo>>) this.m_ConfigDataMailInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMailInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMemoryCorridorInfo GetConfigDataMemoryCorridorInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorInfo>> GetAllConfigDataMemoryCorridorInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorInfo>>) this.m_ConfigDataMemoryCorridorInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMemoryCorridorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMemoryCorridorLevelInfo GetConfigDataMemoryCorridorLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorLevelInfo>> GetAllConfigDataMemoryCorridorLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMemoryCorridorLevelInfo>>) this.m_ConfigDataMemoryCorridorLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMemoryCorridorLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionExtNoviceInfo GetConfigDataMissionExtNoviceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMissionExtNoviceInfo>> GetAllConfigDataMissionExtNoviceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMissionExtNoviceInfo>>) this.m_ConfigDataMissionExtNoviceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMissionExtNoviceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionExtRefluxInfo GetConfigDataMissionExtRefluxInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMissionExtRefluxInfo>> GetAllConfigDataMissionExtRefluxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMissionExtRefluxInfo>>) this.m_ConfigDataMissionExtRefluxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMissionExtRefluxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMissionInfo GetConfigDataMissionInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMissionInfo>> GetAllConfigDataMissionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMissionInfo>>) this.m_ConfigDataMissionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetConfigDataModelSkinResourceInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataModelSkinResourceInfo>> GetAllConfigDataModelSkinResourceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataModelSkinResourceInfo>>) this.m_ConfigDataModelSkinResourceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataModelSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMonsterInfo GetConfigDataMonsterInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMonsterInfo>> GetAllConfigDataMonsterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMonsterInfo>>) this.m_ConfigDataMonsterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMonsterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMonthCardInfo GetConfigDataMonthCardInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMonthCardInfo>> GetAllConfigDataMonthCardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMonthCardInfo>>) this.m_ConfigDataMonthCardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMusicAppreciateTable GetConfigDataMusicAppreciateTable(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataMusicAppreciateTable>> GetAllConfigDataMusicAppreciateTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataMusicAppreciateTable>>) this.m_ConfigDataMusicAppreciateTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataMusicAppreciateTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataNoviceRewardInfo GetConfigDataNoviceRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataNoviceRewardInfo>> GetAllConfigDataNoviceRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataNoviceRewardInfo>>) this.m_ConfigDataNoviceRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataNoviceRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityInfo GetConfigDataOperationalActivityInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityInfo>> GetAllConfigDataOperationalActivityInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityInfo>>) this.m_ConfigDataOperationalActivityInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataOperationalActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataOperationalActivityItemGroupInfo GetConfigDataOperationalActivityItemGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityItemGroupInfo>> GetAllConfigDataOperationalActivityItemGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataOperationalActivityItemGroupInfo>>) this.m_ConfigDataOperationalActivityItemGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataOperationalActivityItemGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaBattleInfo GetConfigDataPeakArenaBattleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaBattleInfo>> GetAllConfigDataPeakArenaBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaBattleInfo>>) this.m_ConfigDataPeakArenaBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetConfigDataPeakArenaDanInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaDanInfo>> GetAllConfigDataPeakArenaDanInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaDanInfo>>) this.m_ConfigDataPeakArenaDanInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaDanInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanRewardSettleTimeInfo GetConfigDataPeakArenaDanRewardSettleTimeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaDanRewardSettleTimeInfo>> GetAllConfigDataPeakArenaDanRewardSettleTimeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaDanRewardSettleTimeInfo>>) this.m_ConfigDataPeakArenaDanRewardSettleTimeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaDanRewardSettleTimeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaFirstWeekWinRewardInfo GetConfigDataPeakArenaFirstWeekWinRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaFirstWeekWinRewardInfo>> GetAllConfigDataPeakArenaFirstWeekWinRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaFirstWeekWinRewardInfo>>) this.m_ConfigDataPeakArenaFirstWeekWinRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaFirstWeekWinRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaKnockoutMatchTime GetConfigDataPeakArenaKnockoutMatchTime(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaKnockoutMatchTime>> GetAllConfigDataPeakArenaKnockoutMatchTime()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaKnockoutMatchTime>>) this.m_ConfigDataPeakArenaKnockoutMatchTimeData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaKnockoutMatchTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaPublicHeroPoolInfo GetConfigDataPeakArenaPublicHeroPoolInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaPublicHeroPoolInfo>> GetAllConfigDataPeakArenaPublicHeroPoolInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaPublicHeroPoolInfo>>) this.m_ConfigDataPeakArenaPublicHeroPoolInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaPublicHeroPoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaRankingRewardInfo GetConfigDataPeakArenaRankingRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaRankingRewardInfo>> GetAllConfigDataPeakArenaRankingRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaRankingRewardInfo>>) this.m_ConfigDataPeakArenaRankingRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaRankingRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo GetConfigDataPeakArenaSeasonInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPeakArenaSeasonInfo>> GetAllConfigDataPeakArenaSeasonInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPeakArenaSeasonInfo>>) this.m_ConfigDataPeakArenaSeasonInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPeakArenaSeasonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPerformanceInfo GetConfigDataPerformanceInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPerformanceInfo>> GetAllConfigDataPerformanceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPerformanceInfo>>) this.m_ConfigDataPerformanceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPerformanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPlayerLevelInfo GetConfigDataPlayerLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPlayerLevelInfo>> GetAllConfigDataPlayerLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPlayerLevelInfo>>) this.m_ConfigDataPlayerLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPlayerLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPrefabStateInfo GetConfigDataPrefabStateInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPrefabStateInfo>> GetAllConfigDataPrefabStateInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPrefabStateInfo>>) this.m_ConfigDataPrefabStateInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPrefabStateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPropertyModifyInfo GetConfigDataPropertyModifyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPropertyModifyInfo>> GetAllConfigDataPropertyModifyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPropertyModifyInfo>>) this.m_ConfigDataPropertyModifyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPropertyModifyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataProtagonistInfo GetConfigDataProtagonistInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataProtagonistInfo>> GetAllConfigDataProtagonistInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataProtagonistInfo>>) this.m_ConfigDataProtagonistInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataProtagonistInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPVPBattleInfo GetConfigDataPVPBattleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataPVPBattleInfo>> GetAllConfigDataPVPBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataPVPBattleInfo>>) this.m_ConfigDataPVPBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataPVPBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRafflePoolInfo GetConfigDataRafflePoolInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRafflePoolInfo>> GetAllConfigDataRafflePoolInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRafflePoolInfo>>) this.m_ConfigDataRafflePoolInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRafflePoolInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomBoxInfo GetConfigDataRandomBoxInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomBoxInfo>> GetAllConfigDataRandomBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomBoxInfo>>) this.m_ConfigDataRandomBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomDropRewardInfo GetConfigDataRandomDropRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomDropRewardInfo>> GetAllConfigDataRandomDropRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomDropRewardInfo>>) this.m_ConfigDataRandomDropRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomDropRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomNameHead GetConfigDataRandomNameHead(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomNameHead>> GetAllConfigDataRandomNameHead()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomNameHead>>) this.m_ConfigDataRandomNameHeadData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomNameHead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomNameMiddle GetConfigDataRandomNameMiddle(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomNameMiddle>> GetAllConfigDataRandomNameMiddle()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomNameMiddle>>) this.m_ConfigDataRandomNameMiddleData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomNameMiddle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomNameTail GetConfigDataRandomNameTail(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomNameTail>> GetAllConfigDataRandomNameTail()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomNameTail>>) this.m_ConfigDataRandomNameTailData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomNameTail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomStoreInfo GetConfigDataRandomStoreInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomStoreInfo>> GetAllConfigDataRandomStoreInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomStoreInfo>>) this.m_ConfigDataRandomStoreInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomStoreItemInfo GetConfigDataRandomStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomStoreItemInfo>> GetAllConfigDataRandomStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomStoreItemInfo>>) this.m_ConfigDataRandomStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRandomStoreLevelZoneInfo GetConfigDataRandomStoreLevelZoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRandomStoreLevelZoneInfo>> GetAllConfigDataRandomStoreLevelZoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRandomStoreLevelZoneInfo>>) this.m_ConfigDataRandomStoreLevelZoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRandomStoreLevelZoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRankInfo GetConfigDataRankInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRankInfo>> GetAllConfigDataRankInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRankInfo>>) this.m_ConfigDataRankInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRankInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPBattleInfo GetConfigDataRealTimePVPBattleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPBattleInfo>> GetAllConfigDataRealTimePVPBattleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPBattleInfo>>) this.m_ConfigDataRealTimePVPBattleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPDanInfo GetConfigDataRealTimePVPDanInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanInfo>> GetAllConfigDataRealTimePVPDanInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanInfo>>) this.m_ConfigDataRealTimePVPDanInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPDanInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPDanRewardInfo GetConfigDataRealTimePVPDanRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanRewardInfo>> GetAllConfigDataRealTimePVPDanRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPDanRewardInfo>>) this.m_ConfigDataRealTimePVPDanRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPDanRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPLocalRankingRewardInfo GetConfigDataRealTimePVPLocalRankingRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPLocalRankingRewardInfo>> GetAllConfigDataRealTimePVPLocalRankingRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPLocalRankingRewardInfo>>) this.m_ConfigDataRealTimePVPLocalRankingRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPLocalRankingRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPNoviceMatchmakingInfo GetConfigDataRealTimePVPNoviceMatchmakingInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPNoviceMatchmakingInfo>> GetAllConfigDataRealTimePVPNoviceMatchmakingInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPNoviceMatchmakingInfo>>) this.m_ConfigDataRealTimePVPNoviceMatchmakingInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPNoviceMatchmakingInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPRankingRewardInfo GetConfigDataRealTimePVPRankingRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPRankingRewardInfo>> GetAllConfigDataRealTimePVPRankingRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPRankingRewardInfo>>) this.m_ConfigDataRealTimePVPRankingRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPRankingRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPSettleTimeInfo GetConfigDataRealTimePVPSettleTimeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPSettleTimeInfo>> GetAllConfigDataRealTimePVPSettleTimeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPSettleTimeInfo>>) this.m_ConfigDataRealTimePVPSettleTimeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPSettleTimeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRealTimePVPWinsBonusInfo GetConfigDataRealTimePVPWinsBonusInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPWinsBonusInfo>> GetAllConfigDataRealTimePVPWinsBonusInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRealTimePVPWinsBonusInfo>>) this.m_ConfigDataRealTimePVPWinsBonusInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRealTimePVPWinsBonusInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRechargeStoreItemInfo GetConfigDataRechargeStoreItemInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRechargeStoreItemInfo>> GetAllConfigDataRechargeStoreItemInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRechargeStoreItemInfo>>) this.m_ConfigDataRechargeStoreItemInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRechargeStoreItemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStoneInfo GetConfigDataRefineryStoneInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRefineryStoneInfo>> GetAllConfigDataRefineryStoneInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRefineryStoneInfo>>) this.m_ConfigDataRefineryStoneInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRefineryStoneInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefineryStonePropertyTemplateInfo GetConfigDataRefineryStonePropertyTemplateInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRefineryStonePropertyTemplateInfo>> GetAllConfigDataRefineryStonePropertyTemplateInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRefineryStonePropertyTemplateInfo>>) this.m_ConfigDataRefineryStonePropertyTemplateInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRefineryStonePropertyTemplateInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRefluxRewardInfo GetConfigDataRefluxRewardInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRefluxRewardInfo>> GetAllConfigDataRefluxRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRefluxRewardInfo>>) this.m_ConfigDataRefluxRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRefluxRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRegionInfo GetConfigDataRegionInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRegionInfo>> GetAllConfigDataRegionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRegionInfo>>) this.m_ConfigDataRegionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRegionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataResonanceInfo GetConfigDataResonanceInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataResonanceInfo>> GetAllConfigDataResonanceInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataResonanceInfo>>) this.m_ConfigDataResonanceInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataResonanceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftChapterInfo GetConfigDataRiftChapterInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRiftChapterInfo>> GetAllConfigDataRiftChapterInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRiftChapterInfo>>) this.m_ConfigDataRiftChapterInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRiftChapterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftLevelInfo GetConfigDataRiftLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataRiftLevelInfo>> GetAllConfigDataRiftLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataRiftLevelInfo>>) this.m_ConfigDataRiftLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataRiftLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScenarioInfo GetConfigDataScenarioInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataScenarioInfo>> GetAllConfigDataScenarioInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataScenarioInfo>>) this.m_ConfigDataScenarioInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScoreLevelInfo GetConfigDataScoreLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataScoreLevelInfo>> GetAllConfigDataScoreLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataScoreLevelInfo>>) this.m_ConfigDataScoreLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataScoreLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSelectContentInfo GetConfigDataSelectContentInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSelectContentInfo>> GetAllConfigDataSelectContentInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSelectContentInfo>>) this.m_ConfigDataSelectContentInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSelectContentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSelectProbabilityInfo GetConfigDataSelectProbabilityInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSelectProbabilityInfo>> GetAllConfigDataSelectProbabilityInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSelectProbabilityInfo>>) this.m_ConfigDataSelectProbabilityInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSelectProbabilityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSelfSelectedBoxInfo GetConfigDataSelfSelectedBoxInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSelfSelectedBoxInfo>> GetAllConfigDataSelfSelectedBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSelfSelectedBoxInfo>>) this.m_ConfigDataSelfSelectedBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSelfSelectedBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSensitiveWords GetConfigDataSensitiveWords(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSensitiveWords>> GetAllConfigDataSensitiveWords()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSensitiveWords>>) this.m_ConfigDataSensitiveWordsData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSensitiveWords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSignRewardInfo GetConfigDataSignRewardInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSignRewardInfo>> GetAllConfigDataSignRewardInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSignRewardInfo>>) this.m_ConfigDataSignRewardInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSignRewardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetConfigDataSkillInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSkillInfo>> GetAllConfigDataSkillInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSkillInfo>>) this.m_ConfigDataSkillInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSmallExpressionPathInfo GetConfigDataSmallExpressionPathInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSmallExpressionPathInfo>> GetAllConfigDataSmallExpressionPathInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSmallExpressionPathInfo>>) this.m_ConfigDataSmallExpressionPathInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSmallExpressionPathInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierInfo GetConfigDataSoldierInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSoldierInfo>> GetAllConfigDataSoldierInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSoldierInfo>>) this.m_ConfigDataSoldierInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoldierSkinInfo GetConfigDataSoldierSkinInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSoldierSkinInfo>> GetAllConfigDataSoldierSkinInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSoldierSkinInfo>>) this.m_ConfigDataSoldierSkinInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSoldierSkinInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSoundTable GetConfigDataSoundTable(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSoundTable>> GetAllConfigDataSoundTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSoundTable>>) this.m_ConfigDataSoundTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSoundTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSpineAnimationSoundTable GetConfigDataSpineAnimationSoundTable(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSpineAnimationSoundTable>> GetAllConfigDataSpineAnimationSoundTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSpineAnimationSoundTable>>) this.m_ConfigDataSpineAnimationSoundTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSpineAnimationSoundTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_0_CN GetConfigDataSST_0_CN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSST_0_CN>> GetAllConfigDataSST_0_CN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_0_CN>>) this.m_ConfigDataSST_0_CNData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_0_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_0_EN GetConfigDataSST_0_EN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSST_0_EN>> GetAllConfigDataSST_0_EN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_0_EN>>) this.m_ConfigDataSST_0_ENData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_0_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_1_CN GetConfigDataSST_1_CN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSST_1_CN>> GetAllConfigDataSST_1_CN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_1_CN>>) this.m_ConfigDataSST_1_CNData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_1_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSST_1_EN GetConfigDataSST_1_EN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSST_1_EN>> GetAllConfigDataSST_1_EN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSST_1_EN>>) this.m_ConfigDataSST_1_ENData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSST_1_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStaticBoxInfo GetConfigDataStaticBoxInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataStaticBoxInfo>> GetAllConfigDataStaticBoxInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStaticBoxInfo>>) this.m_ConfigDataStaticBoxInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStaticBoxInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStoreCurrencyInfo GetConfigDataStoreCurrencyInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataStoreCurrencyInfo>> GetAllConfigDataStoreCurrencyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStoreCurrencyInfo>>) this.m_ConfigDataStoreCurrencyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStoreCurrencyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStoreInfo GetConfigDataStoreInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataStoreInfo>> GetAllConfigDataStoreInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStoreInfo>>) this.m_ConfigDataStoreInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStoryOutlineInfo GetConfigDataStoryOutlineInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataStoryOutlineInfo>> GetAllConfigDataStoryOutlineInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStoryOutlineInfo>>) this.m_ConfigDataStoryOutlineInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStoryOutlineInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStringTable GetConfigDataStringTable(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataStringTable>> GetAllConfigDataStringTable()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStringTable>>) this.m_ConfigDataStringTableData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStringTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataStringTableForListInfo GetConfigDataStringTableForListInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataStringTableForListInfo>> GetAllConfigDataStringTableForListInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataStringTableForListInfo>>) this.m_ConfigDataStringTableForListInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataStringTableForListInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSurveyInfo GetConfigDataSurveyInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSurveyInfo>> GetAllConfigDataSurveyInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSurveyInfo>>) this.m_ConfigDataSurveyInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSurveyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSystemBroadcastInfo GetConfigDataSystemBroadcastInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataSystemBroadcastInfo>> GetAllConfigDataSystemBroadcastInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataSystemBroadcastInfo>>) this.m_ConfigDataSystemBroadcastInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataSystemBroadcastInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTarotInfo GetConfigDataTarotInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTarotInfo>> GetAllConfigDataTarotInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTarotInfo>>) this.m_ConfigDataTarotInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTarotInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainEffectInfo GetConfigDataTerrainEffectInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTerrainEffectInfo>> GetAllConfigDataTerrainEffectInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTerrainEffectInfo>>) this.m_ConfigDataTerrainEffectInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTerrainEffectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetConfigDataTerrainInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTerrainInfo>> GetAllConfigDataTerrainInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTerrainInfo>>) this.m_ConfigDataTerrainInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataThearchyTrialInfo GetConfigDataThearchyTrialInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialInfo>> GetAllConfigDataThearchyTrialInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialInfo>>) this.m_ConfigDataThearchyTrialInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataThearchyTrialInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataThearchyTrialLevelInfo GetConfigDataThearchyTrialLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialLevelInfo>> GetAllConfigDataThearchyTrialLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataThearchyTrialLevelInfo>>) this.m_ConfigDataThearchyTrialLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataThearchyTrialLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTicketLimitGameFunctionTypeInfo GetConfigDataTicketLimitGameFunctionTypeInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTicketLimitGameFunctionTypeInfo>> GetAllConfigDataTicketLimitGameFunctionTypeInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTicketLimitGameFunctionTypeInfo>>) this.m_ConfigDataTicketLimitGameFunctionTypeInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTicketLimitGameFunctionTypeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTicketLimitInfo GetConfigDataTicketLimitInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTicketLimitInfo>> GetAllConfigDataTicketLimitInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTicketLimitInfo>>) this.m_ConfigDataTicketLimitInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTicketLimitInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTitleInfo GetConfigDataTitleInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTitleInfo>> GetAllConfigDataTitleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTitleInfo>>) this.m_ConfigDataTitleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTitleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBattleRuleInfo GetConfigDataTowerBattleRuleInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTowerBattleRuleInfo>> GetAllConfigDataTowerBattleRuleInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerBattleRuleInfo>>) this.m_ConfigDataTowerBattleRuleInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerBattleRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerBonusHeroGroupInfo GetConfigDataTowerBonusHeroGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTowerBonusHeroGroupInfo>> GetAllConfigDataTowerBonusHeroGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerBonusHeroGroupInfo>>) this.m_ConfigDataTowerBonusHeroGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerBonusHeroGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerFloorInfo GetConfigDataTowerFloorInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTowerFloorInfo>> GetAllConfigDataTowerFloorInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerFloorInfo>>) this.m_ConfigDataTowerFloorInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerFloorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTowerLevelInfo GetConfigDataTowerLevelInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTowerLevelInfo>> GetAllConfigDataTowerLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTowerLevelInfo>>) this.m_ConfigDataTowerLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTowerLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingCourseInfo GetConfigDataTrainingCourseInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTrainingCourseInfo>> GetAllConfigDataTrainingCourseInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingCourseInfo>>) this.m_ConfigDataTrainingCourseInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingCourseInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingRoomInfo GetConfigDataTrainingRoomInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomInfo>> GetAllConfigDataTrainingRoomInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomInfo>>) this.m_ConfigDataTrainingRoomInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingRoomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingRoomLevelInfo GetConfigDataTrainingRoomLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomLevelInfo>> GetAllConfigDataTrainingRoomLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingRoomLevelInfo>>) this.m_ConfigDataTrainingRoomLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingRoomLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechInfo GetConfigDataTrainingTechInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTrainingTechInfo>> GetAllConfigDataTrainingTechInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingTechInfo>>) this.m_ConfigDataTrainingTechInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingTechInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTrainingTechLevelInfo GetConfigDataTrainingTechLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTrainingTechLevelInfo>> GetAllConfigDataTrainingTechLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTrainingTechLevelInfo>>) this.m_ConfigDataTrainingTechLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTrainingTechLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTranslate GetConfigDataTranslate(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTranslate>> GetAllConfigDataTranslate()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTranslate>>) this.m_ConfigDataTranslateData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTranslate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTreasureLevelInfo GetConfigDataTreasureLevelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataTreasureLevelInfo>> GetAllConfigDataTreasureLevelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataTreasureLevelInfo>>) this.m_ConfigDataTreasureLevelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataTreasureLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo GetConfigDataUnchartedScoreInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreInfo>> GetAllConfigDataUnchartedScoreInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreInfo>>) this.m_ConfigDataUnchartedScoreInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreModelInfo GetConfigDataUnchartedScoreModelInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreModelInfo>> GetAllConfigDataUnchartedScoreModelInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreModelInfo>>) this.m_ConfigDataUnchartedScoreModelInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUnchartedScoreModelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreRewardGroupInfo GetConfigDataUnchartedScoreRewardGroupInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreRewardGroupInfo>> GetAllConfigDataUnchartedScoreRewardGroupInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUnchartedScoreRewardGroupInfo>>) this.m_ConfigDataUnchartedScoreRewardGroupInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUnchartedScoreRewardGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuide GetConfigDataUserGuide(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataUserGuide>> GetAllConfigDataUserGuide()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUserGuide>>) this.m_ConfigDataUserGuideData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuideDialogInfo GetConfigDataUserGuideDialogInfo(
      int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataUserGuideDialogInfo>> GetAllConfigDataUserGuideDialogInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUserGuideDialogInfo>>) this.m_ConfigDataUserGuideDialogInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUserGuideDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUserGuideStep GetConfigDataUserGuideStep(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataUserGuideStep>> GetAllConfigDataUserGuideStep()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataUserGuideStep>>) this.m_ConfigDataUserGuideStepData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataUserGuideStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataVersionInfo GetConfigDataVersionInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataVersionInfo>> GetAllConfigDataVersionInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataVersionInfo>>) this.m_ConfigDataVersionInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataVersionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo GetConfigDataWaypointInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataWaypointInfo>> GetAllConfigDataWaypointInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataWaypointInfo>>) this.m_ConfigDataWaypointInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWorldMapInfo GetConfigDataWorldMapInfo(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataWorldMapInfo>> GetAllConfigDataWorldMapInfo()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataWorldMapInfo>>) this.m_ConfigDataWorldMapInfoData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataWorldMapInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataST_CN GetConfigDataST_CN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetConfigDataST_CN(int key, Action<ConfigDataST_CN> onResult)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataST_CN>> GetAllConfigDataST_CN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataST_CN>>) this.m_ConfigDataST_CNData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataST_CN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConfigDataST_CNReady(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearConfigDataST_CN()
    {
      this.m_ConfigDataST_CNData.Clear();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataST_EN GetConfigDataST_EN(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetConfigDataST_EN(int key, Action<ConfigDataST_EN> onResult)
    {
      // ISSUE: unable to decompile the method.
    }

    public IEnumerable<KeyValuePair<int, ConfigDataST_EN>> GetAllConfigDataST_EN()
    {
      return (IEnumerable<KeyValuePair<int, ConfigDataST_EN>>) this.m_ConfigDataST_ENData;
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable LuaGetAllConfigDataST_EN()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConfigDataST_ENReady(int key)
    {
      // ISSUE: unable to decompile the method.
    }

    public void ClearConfigDataST_EN()
    {
      this.m_ConfigDataST_ENData.Clear();
    }

    public float LoadingProgress
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int UtilityInitialize()
    {
      return this.ReleaseInitialize();
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int EditorInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ReleaseInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PreInitialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReplaceAssetPaths()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ReplaceAssetPath(string oldPath, Dictionary<string, string> replacePath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendBattleLevelAchievementToList(
      int achievementRelatedId,
      List<Goods> rewards,
      List<BattleLevelAchievement> list)
    {
      // ISSUE: unable to decompile the method.
    }

    public ArmyRelationData UtilityGetArmyRelationData(
      ArmyTag attacker,
      ArmyTag target)
    {
      return this.m_armyRelationTable[(int) attacker, (int) target];
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGameFunctionOpenInfo UtilityGetGameFunctionOpenInfo(
      GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UtilityGetConfigableConst(ConfigableConstId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetConfigableConstString(ConfigableConstId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetStringByStringTable(StringTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetFxFlipName(string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UtilityGetVersion(VersionInfoId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetVersionString(VersionInfoId id)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetSound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGoods(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CheckGoodsList(List<Goods> goodsList, int incrementValue, string descPrefix)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> WeightGoodsToGoods(List<WeightGoods> weightGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int PreCheckData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int PostCheckData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckAssetsExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckAssetExist(string assetName, string tableName, string fieldName, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool CheckBattlePositionDuplicate(
      int battleId,
      List<GridPosition> positions,
      int x,
      int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string UtilityGetErrorCodeString(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CheckRandomBoxGoodsList(List<RandomBoxGoods> randomBoxGoodsList, string descPrefix)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddArmyRelationDataAttack(ArmyTag a, ArmyTag b, int attack, int magic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddArmyRelationDataDefend(ArmyTag a, ArmyTag b, int defend, int magicDefend)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<T> InternEmptyList<T>(List<T> list)
    {
      // ISSUE: unable to decompile the method.
    }

    public SensitiveWords UtilityGetSensitiveWords()
    {
      return this.SensitiveWords;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAncientCallSingleBossPointByRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Const_BattleActorMoveSpeed { get; set; }

    public int Const_CombatHeroDistance { get; set; }

    public int Const_CombatSplitScreenDistance { get; set; }

    public int Const_MeleeATKPunish_Mult { get; set; }

    public int Const_SoldierMoveDelay { get; set; }

    public int Const_SoldierReturnDelay { get; set; }

    public int Const_SkillPauseTime { get; set; }

    public int Const_SkillPreCastDelay { get; set; }

    public int Const_DamagePostDelay { get; set; }

    public int Const_BuffHitPostDelay { get; set; }

    public int Const_SoldierCountMax { get; set; }

    public int Const_HPBarFxAccumulateTime { get; set; }

    public int Const_HPBarFxAccumulateDamage { get; set; }

    public int Const_CriticalDamageBase { get; set; }

    public int ConfigableConstId_FlushTime { get; set; }

    public int ConfigableConstId_FlushPeriodDay { get; set; }

    public int ConfigableConstId_TimeEventStartTime { get; set; }

    public int ConfigableConstId_TimeEventEndTime { get; set; }

    public int ConfigableConstId_TimeEventMinCount { get; set; }

    public int ConfigableConstId_TimeEventMaxCount { get; set; }

    public int ConfigableConstId_RiftLevelActionEventProbality { get; set; }

    public int ConfigableConstId_ScenarioActionEventProbality { get; set; }

    public int ConfigableConstId_EventActionEventProbality { get; set; }

    public int ConfigableConstId_ProbalityMax { get; set; }

    public int ConfigableConstId_ActionEventMaxCount { get; set; }

    public int ConfigableConstId_MapRandomEventMaxCount { get; set; }

    public int ConfigableConstId_EnergyAddPeriod { get; set; }

    public int ConfigableConstId_EnergyMax { get; set; }

    public int ConfigableConstId_HeroJobMaterialMaxNums { get; set; }

    public int ConfigableConstId_BagMaxCapacity { get; set; }

    public int ConfigableConstId_HeroJobRankMax { get; set; }

    public int ConfigableConstId_HeroLevelUpCeiling { get; set; }

    public int ConfigableConstId_SelectSkillsMaxCount { get; set; }

    public int ConfigableConstId_BagGridMaxCapacity { get; set; }

    public int ConfigableConstId_HeroStarLevelMax { get; set; }

    public int ConfigableConstId_RiftLevelMaxStar { get; set; }

    public int ConfigableConstId_JobStartLeve { get; set; }

    public int ConfigableConstId_FirstScenarioId { get; set; }

    public int ConfigableConstId_FirstWayPointId { get; set; }

    public int ConfigableConstId_MailBoxMaxSize { get; set; }

    public int ConfigableConstId_WeaponSlotId { get; set; }

    public int ConfigableConstId_ArmorSlotId { get; set; }

    public int ConfigableConstId_OrnamentSlotId { get; set; }

    public int ConfigableConstId_ShoeSlotId { get; set; }

    public int ConfigableConstId_FirstInitId { get; set; }

    public int ConfigableConstId_PlayerLevelMaxLevel { get; set; }

    public int ConfigableConstId_ChatMessageMaxLength { get; set; }

    public int ConfigableConstId_WorldChatIntervalTime { get; set; }

    public int ConfigableConstId_PeakArenaPlayOffChatIntervalTime { get; set; }

    public int ConfigableConstId_AllowChatPlayerLevel { get; set; }

    public int ConfigableConstId_SystemSelectCardHeroBroadcast { get; set; }

    public int ConfigableConstId_MagicStoneId { get; set; }

    public int ConfigableConstId_BuyEnergyCount { get; set; }

    public int ConfigableConstId_HeroCommentMaxLength { get; set; }

    public int ConfigableConstId_HeroCommentMaxNums { get; set; }

    public int ConfigableConstId_PlayerSingleHeroCommentMaxNums { get; set; }

    public int ConfigableConstId_MaxPopularCommentEntry { get; set; }

    public int ConfigableConstId_ThearchyTrailEverydayChallengeNums { get; set; }

    public int ConfigableConstId_AnikiGymEverydayChallengeNums { get; set; }

    public int ConfigableConstId_AllowArenaPlayerLevel { get; set; }

    public int ConfigableConstId_ArenaGivenTicketMaxNums { get; set; }

    public int ConfigableConstId_LevelRaidTicketID { get; set; }

    public int ConfigableConstId_ArenaFightExpiredTimeInterval { get; set; }

    public int ConfigableConstId_ProtagonistHeroID { get; set; }

    public int ConfigableConstId_PlayerNameMaxLength { get; set; }

    public int ConfigableConstId_ArenaBattleReportMaxNums { get; set; }

    public int ConfigableConstId_ArenaVictoryPointsRewardMaxVictionaryPoints { get; set; }

    public int ConfigableConstId_ArenaOneTimeGiveTicketsNums { get; set; }

    public int ConfigableConstId_ArenaInitialPoints { get; set; }

    public int ConfigableConstId_ArenaOneTimeAttackUseTicketsNums { get; set; }

    public int ConfigableConstId_ArenaAttackSuccessRandomDropRewardID { get; set; }

    public int ConfigableConstId_ArenaRealWeekSettleDeltaTime { get; set; }

    public int ConfigableConstId_ArenaFindOpponentOneDirectionMaxOpponentNums { get; set; }

    public int ConfigableConstId_ChangeNameCostNums { get; set; }

    public int ConfigableConstId_PlayerInitialHeroInteractNums { get; set; }

    public int ConfigableConstId_HeroInteractNumsRecoveryPeriod { get; set; }

    public int ConfigableConstId_HeroIteractMaxNums { get; set; }

    public int ConfigableConstId_HeroIntimateMaxValue { get; set; }

    public int ConfigableConstId_EnhanceEquipmentConsumeGoldPerExp { get; set; }

    public int ConfigableConstId_LevelUpEquipmentStarConsumeGoldPerStar { get; set; }

    public int ConfigableConstId_BuyArenaTicketCount { get; set; }

    public int ConfigableConstId_HeroDungeonLevelMaxStar { get; set; }

    public int ConfigableConstId_ArenaAddHeroExp { get; set; }

    public int ConfigableConstId_ArenaAddPlayerExp { get; set; }

    public int ConfigableConstId_SystemSelectCardEquipmentBroadcast { get; set; }

    public int ConfigableConstId_AnikiGymLevelTicketID { get; set; }

    public int ConfigableConstId_TherachyTrialTicketID { get; set; }

    public int ConfigableConstId_GoldRank { get; set; }

    public int ConfigableConstId_CrystalRank { get; set; }

    public int ConfigableConstId_SkinTicketRank { get; set; }

    public int ConfigableConstId_ArenaHonourRank { get; set; }

    public int ConfigableConstId_EnergyRank { get; set; }

    public int EquipmentTableRankMax { get; set; }

    public int ConfigableConstId_PlayerExpRank { get; set; }

    public int ConfigableConstId_MemoryEssence { get; set; }

    public int ConfigableConstId_MithralStoneRank { get; set; }

    public int ConfigableConstId_BrillianceMithralStoneRank { get; set; }

    public int ConfigableConstId_FriendshipPointsRank { get; set; }

    public int ConfigableConstId_ArenaTicketRank { get; set; }

    public int ConfigableConstId_UserGuideRandomEventId { get; set; }

    public int ConfigableConstId_WriteSurveyPlayerLevel { get; set; }

    public int ConfigableConstId_AutoEquipmentDeltaTime { get; set; }

    public int ConfigableConstId_MaxLevelDanmakuEntryLength { get; set; }

    public int ConfigableConstId_MaxLevelDanmakuEntryNumsPerTurn { get; set; }

    public int ConfigableConstId_TeamRoomInviteDeltaTime { get; set; }

    public int ConfigableConstId_BusinessCardDescMaxLength { get; set; }

    public int ConfigableConstId_BusinessCardHeroMaxNums { get; set; }

    public int ConfigableConstId_SendFriendShipPointsEverytime { get; set; }

    public int ConfigableConstId_SendFriendShipPointsMaxTimes { get; set; }

    public int ConfigableConstId_ReceiveFriendShipPointsMaxTimes { get; set; }

    public int ConfigableConstId_SearchUserByNameResultMax { get; set; }

    public int ConfigableConstId_MaxDomesticFriends { get; set; }

    public int ConfigableConstId_MaxForeignFriends { get; set; }

    public int ConfigableConstId_MaxBlacklist { get; set; }

    public int ConfigableConstId_MaxInvites { get; set; }

    public int ConfigableConstId_MaxSuggestedUsers { get; set; }

    public int ConfigableConstId_MaxLevelDiffMultiply { get; set; }

    public int ConfigableConstId_SuggestedUserLevelDiff { get; set; }

    public int ConfigableConstId_InviteExpireSeconds { get; set; }

    public int ConfigableConstId_RecentContactsMaxCount { get; set; }

    public int ConfigableConstId_HeroAssistantTaskHeroAssignMaxCount { get; set; }

    public int BuyEnergyMaxNums { get; set; }

    public int ConfigableConstId_MaxGroupsPerUser { get; set; }

    public int ConfigableConstId_MaxGroupMembers { get; set; }

    public int ConfigableConstId_BattleInviteFriendsToPracticeMaxWaitInterval { get; set; }

    public int ConfigableConstId_DayBonusNum_Aniki { get; set; }

    public int ConfigableConstId_DayBonusNum_ThearchyTrial { get; set; }

    public int ConfigableConstId_DayBonusNum_HeroTrain { get; set; }

    public int ConfigableConstId_DayBonusNum_MemoryCorridor { get; set; }

    public int ConfigableConstId_DayBonusNum_Crusade { get; set; }

    public int BuyArenaTicketMaxNums { get; set; }

    public int ConfigableConstId_UseBoxItemMaxCount { get; set; }

    public int ConfigableConstId_ServerTolerateClientSpeedUpMaxNums { get; set; }

    public int ConfigableConstId_ServerCheckClientHeartBeatReachMinPeriod { get; set; }

    public int ConfigableConstId_ClientSendHeartBeatPeriod { get; set; }

    public int ConfigableConstId_ClientCheckOnlinePeriod { get; set; }

    public int ConfigableConstId_RealTimePVPBattleStartTime { get; set; }

    public int ConfigableConstId_RealTimePVPGoldBanHeroCount { get; set; }

    public int ConfigableConstId_RealTimePVPGoldCountdown { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn1 { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn2 { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn3 { get; set; }

    public int ConfigableConstId_RefluxBeginLevelLimit { get; set; }

    public int ConfigableConstId_HeroAnthemLevelLimit { get; set; }

    public int ConfigableConstId_AncientCallLevelLimit { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn4 { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn5 { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn6 { get; set; }

    public int ConfigableConstId_RealTimePVPGoldHeroCount_Turn7 { get; set; }

    public int ConfigableConstId_RealTimePVPGoldProtectHeroCount { get; set; }

    public int ConfigableConstId_RealTimePVPInitialScore { get; set; }

    public int ConfigableConstId_RealTimePVPMatchmakingExpectedTime { get; set; }

    public int ConfigableConstId_RealTimePVPMatchmakingExpectedTimeAdjust { get; set; }

    public int ConfigableConstId_RealTimePVPMinRequiredLevel { get; set; }

    public int ConfigableConstId_RealTimePVPSilverCountdown { get; set; }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn1 { get; set; }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn2 { get; set; }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn3 { get; set; }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn4 { get; set; }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn5 { get; set; }

    public int ConfigableConstId_RealTimePVPSilverHeroCount_Turn6 { get; set; }

    public int ConfigableConstId_RealTimePVPTurnInterval { get; set; }

    public int ConfigableConstId_RealTimePVPPromotionSucceedScoreBonus { get; set; }

    public int ConfigableConstId_RealTimePVPPromotionFailedScorePenalty { get; set; }

    public int ConfigableConstId_RealTimePVPScoreMax { get; set; }

    public int ConfigableConstId_PeakArenaRankListFlushNum { get; set; }

    public int ConfigableConstId_FriendPointsFightWithFriends { get; set; }

    public int ConfigableConstId_MaxFriendPointsFightWithFriendsEveryday { get; set; }

    public int ConfigableConstId_BattleRoomPlayerReconnectTimeOutTime { get; set; }

    public int ConfigableConstId_TeamBattleRoomPlayerActionClientTimeOutTime { get; set; }

    public int ConfigableConstId_PVPBattleRoomPlayerActionClientTimeOutTime { get; set; }

    public int ConfigableConstId_TeamBattleRoomPlayerActionServerTimeOutTime { get; set; }

    public int ConfigableConstId_PVPBattleRoomPlayerActionServerTimeOutTime { get; set; }

    public int ConfigableConstId_TeamFriendShipPointRewardPerFriend { get; set; }

    public int ConfigableConstId_MaxEnchantSamePropertyNums { get; set; }

    public int ConfigableConstId_TeamInvitationTimeoutTime { get; set; }

    public int ConfigableConstId_PVPInvitationTimeoutTime { get; set; }

    public int ConfigableConstId_DecomposeEquipmentBackGoldPercent { get; set; }

    public int ConfigableConstId_RecentContactsChatMaxCount { get; set; }

    public int ConfigableConstId_RecentContactsTeamBattleMaxCount { get; set; }

    public int ConfigableConstId_DayBonusNum_CooperateBattle { get; set; }

    public int ConfigableConstId_HeroAssistantTaskSlotCount { get; set; }

    public int ConfigableConstId_ChatGroupNameMaxLength { get; set; }

    public int ConfigableConstId_ChatGroupCreateMinUserCount { get; set; }

    public int ConfigableConstId_ChatGroupDisbandUserCount { get; set; }

    public int ConfigableConstId_DefaultHeadFrameId { get; set; }

    public int ConfigableConstId_RequestAppReviewInScenario { get; set; }

    public int ConfigableConstId_IsRequestAppReviewOn { get; set; }

    public int ConfigableConstId_GainMaximum { get; set; }

    public int ConfigableConstId_AlchemyMaxNum { get; set; }

    public int ConfigableConstId_ItemComposeMaxLimit { get; set; }

    public int ConfigableConstId_PrivilegeItemRank { get; set; }

    private int ConfigableConstId_Battle_ClientCheckOnlinePeriod { get; set; }

    public int RealTimePVPInclusiveMinDan { get; set; }

    public int RealTimePVPExclusiveMaxDan { get; set; }

    public int RealTimeArenaNewPlayerMatchCount { get; set; }

    public int ConfigableConstId_RealTimePVPBotPlayDeadIntervalMin { get; set; }

    public int ConfigableConstId_RealTimePVPBotPlayDeadIntervalMax { get; set; }

    public int ConfigableConstId_RealTimePVPBattleReportMaxNums { get; set; }

    public int ConfigableConstId_NewUserAccumulatedMinValue { get; set; }

    public int ConfigableConstId_NewUserAccumulatedMaxValue { get; set; }

    public int ConfigableConstId_OldUserAccumulatedMinValue { get; set; }

    public int ConfigableConstId_OldUserAccumulatedMaxValue { get; set; }

    public int ConfigableConstId_BattleRoomPlayerActionCountdownBigNumberTime { get; set; }

    public int ConfigableConstId_GuildCreateJoinLevel { get; set; }

    public int ConfigableConstId_GuildCreateItemId { get; set; }

    public int ConfigableConstId_GuildMemberCountMax { get; set; }

    public int ConfigableConstId_GuildMaxBattlePower { get; set; }

    public int ConfigableConstId_GuildReJoinCoolDownTime { get; set; }

    public int ConfigableConstId_GuildJoinApplicationPlayerCountMax { get; set; }

    public int ConfigableConstId_GuildInviteCountMax { get; set; }

    public int ConfigableConstId_GuildChatHistoryCount { get; set; }

    public int ConfigableConstId_GuildHiringDeclarationMaxLength { get; set; }

    public int ConfigableConstId_GuildVicePresidentMaxNums { get; set; }

    public int ConfigableConstId_GuildEliteMinTotalActivities { get; set; }

    public int ConfigableConstId_RankListGuildNums { get; set; }

    public int ConfigableConstId_GuildVicePresidentCanUsurpTime { get; set; }

    public int ConfigableConstId_GuildEliteCanUsurpTime { get; set; }

    public int ConfigableConstId_GuildChangeNameCrystalCost { get; set; }

    public int ConfigableConstId_GuildDailyMaxActivities { get; set; }

    public int ConfigableConstId_GuildAnnouncementMaxLength { get; set; }

    public int ConfigableConstId_GuildWeeklyMaxActivities { get; set; }

    public int ConfigableConstId_GuildNameMaxLength { get; set; }

    public int ConfigableConstId_GuildLogMaxNUms { get; set; }

    public int ConfigableConstId_GuildMedalRank { get; set; }

    public int ConfigableConstId_NewUserSecondAccumulatedMinValue { get; set; }

    public int ConfigableConstId_NewUserSecondAccumulatedMaxValue { get; set; }

    public int ConfigableConstId_RandomStoreManualFlushMaxNums { get; set; }

    public int ConfigableConstId_HeroDungeonDailyChallengeMaxNums { get; set; }

    public int ConfigableConstId_GuildMassiveCombatAvailableCountPerWeek { get; set; }

    public int[] GuildMassiveCombatRandomHeroTagIds { get; set; }

    public int ConfigableConstId_GuildMassiveCombatMinTitleToStart { get; set; }

    public int ConfigableConstId_GuildMassiveCombatMinTitleToSurrender { get; set; }

    public int ConfigableConstId_GuildMassiveCombatMaxHeroTagIdForStronghold { get; set; }

    public int ConfigableConstId_EternalShrineDailyChallengeCountMax { get; set; }

    public bool ConfigableConstId_AppleSubscribeMonthCardCanRepeatlyBuy { get; set; }

    public int ConfigableConstId_PeakArenaRestTime { get; set; }

    public int ConfigableConstId_PeakArenaBanTime { get; set; }

    public int ConfigableConstId_PeakArenaPickTime { get; set; }

    public int ConfigableConstId_PeakArenaBPPublicTime { get; set; }

    public int ConfigableConstId_PeakArenaReadyTime { get; set; }

    public int ConfigableConstId_PeakArenaBattleRoomPlayerActionTime { get; set; }

    public int ConfigableConstId_PeakArenaBattlePublicTime { get; set; }

    public int ConfigableConstId_PeakArenaTotalDisconnectTime { get; set; }

    public int ConfigableConstId_PeakArenaBattleTeamHeroCount { get; set; }

    public int ConfigableConstId_PeakArenaPlayerEnterLevel { get; set; }

    public int ConfigableConstId_PeakArenaPreRankingMatchCount { get; set; }

    public int ConfigableConstId_PeakArenaPreRankingMatchWinScore { get; set; }

    public int ConfigableConstId_PeakArenaPreRankingMatchLossScore { get; set; }

    public int ConfigableConstId_PeakArenaPreRankingMatchmakingDanMax { get; set; }

    public int ConfigableConstId_PeakArenaMatchmakingSameOpponentAvoidMatchCount { get; set; }

    public int ConfigableConstId_PeakArenaLuckyHeroOwnerRewardMailTemplateId { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolRCount { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSRCount { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSSRCount { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolRMinRank { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSRMinRank { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSSRMinRank { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolRMaxRank { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSRMaxRank { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSSRMaxRank { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolRRangeMin { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSRRangeMin { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSSRRangeMin { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolRRangeMax { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSRRangeMax { get; set; }

    public int ConfigableConstId_PeakArenaHeroPoolSSRRangeMax { get; set; }

    public int ConfigableConstId_PeakArenaInitScore { get; set; }

    public int ConfigableConstId_PeakArenaClientStartBPStageLoadTime { get; set; }

    public int ConfigableConstId_PeakArenaClientStartBattleLoadTime { get; set; }

    public int ConfigableConstId_ChatShareEquipeNumLimit { get; set; }

    public int ConfigableConstId_PeakArenaBattleReportMaxNums { get; set; }

    public int ConfigableConstId_BusinessCardHeroicMomentMaxNums { get; set; }

    public List<PeakAreanaBPFlowInfo> PeakAreanaBPFlowInfos { get; set; }

    public int ConfigableConstId_PeakArenaLadderDailyQuickLeavingToleranceCount { get; set; }

    public int ConfigableConstId_PeakArenaLadderDailyQuickLeavingBattleDuration { get; set; }

    public int ConfigableConstId_PeakArenaLiveDelayTime { get; set; }

    public int ConfigableConstId_StoryKilometersSecond { get; set; }

    public int ConfigableConstId_StoryKilometersSecondSpeed { get; set; }

    public int ConfigableConstId_UserGuideStoryOutlineInfoID { get; set; }

    public RandomStoreData RandomStoreData { get; set; }

    public RandomDropDataInfo RandomDropData { get; set; }

    public ClientRandomDropData ClientRandomDropData { get; set; }

    public FixedStoreDataInfo FixedStoreData { get; set; }

    public SignRewardDataInfo SignRewardData { get; set; }

    public SensitiveWords SensitiveWords { get; set; }

    public MissionDataInfo MissionData { get; set; }

    public ThearchyTrialDataInfo ThearchyTrialData { get; set; }

    public EternalShrineDataInfo EternalShrineData { get; set; }

    public AnikiGymDataInfo AnikiGymData { get; set; }

    public MemoryCorridorDataInfo MemoryCorridorData { get; set; }

    public HeroTrainningDataInfo HeroTrainningData { get; set; }

    public Dictionary<int, ConfigDataTicketLimitInfo> TicketId2TicketLimitInfo { get; set; }

    public Dictionary<int, List<EnchantPropertyProbabilityInfo>> EnchantPropertyProbabilityInfos { get; set; }

    public BlackJack.ConfigData.RealTimePVPAvailableTime[] RealTimePVPAvailableTime { get; set; }

    public DayOfWeek[] RealTimePVPOpenWeekday { get; set; }

    public DayOfWeek[] PeakArenaOpenWeekday { get; set; }

    public int PeakArenaInclusiveMinDan { get; set; }

    public int PeakArenaExclusiveMaxDan { get; set; }

    public int ConfigableConstId_PeakArenaJettonId { get; set; }

    public int ConfigableConstId_PeakArenaJettonExchangeGoldNums { get; set; }

    public int ConfigableConstId_PeakArenaJettonFunctionMinPlayerLvl { get; set; }

    public int ConfigableConstId_PeakArenaJettonGiveNums { get; set; }

    public int ConfigableConstId_PeakArenaJettonDailyBoughtMaxNums { get; set; }

    public int ConfigableConstId_PeakArenaJettonDailyBetJettonMaxNums { get; set; }

    public int ConfigableConstId_PeakArenaJettonBoughtCurrencyCost { get; set; }

    public int ConfigableConstId_PeakArenaJettonBoughtCurrencyType { get; set; }

    public int ConfigableConstId_PeakArenaJettonGotNumsOnceBought { get; set; }

    public int ConfigableConstId_PeakArenaPlayOffReadyTime { get; set; }

    public int ConfigableConstId_HeroFinalJobRank { get; set; }

    public int ConfigableConstId_HeroJobNormalSlotPropertyMaxNums { get; set; }

    public int ConfigableConstId_HeroJobSpecificSlotPropertyMaxNums { get; set; }

    public int ConfigableConstId_UnchartedLevelRaidTimesPerDay { get; set; }

    public int ConfigableConstId_UnchartedLevelRaidTimesExtraAddByMonthCard { get; set; }

    public BlackJack.ConfigData.PeakArenaRegularAvailableTime[] PeakArenaRegularAvailableTime { get; set; }

    public List<ConfigDataPeakArenaDanInfo> PeakArenaDanInfoSortedById { get; set; }

    public List<ConfigDataPeakArenaDanInfo> PeakArenaDanInfoSortedByBaselineScore { get; set; }

    public Dictionary<int, List<ConfigDataMusicAppreciateTable>> MusicAppreciateDic { get; set; }
  }
}
