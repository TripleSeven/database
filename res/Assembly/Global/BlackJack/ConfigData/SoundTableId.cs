﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.SoundTableId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "SoundTableId")]
  public enum SoundTableId
  {
    [ProtoEnum(Name = "SoundTableId_None", Value = 0)] SoundTableId_None,
    [ProtoEnum(Name = "SoundTableId_SelectHero", Value = 1)] SoundTableId_SelectHero,
    [ProtoEnum(Name = "SoundTableId_DragHero", Value = 2)] SoundTableId_DragHero,
    [ProtoEnum(Name = "SoundTableId_DropHero", Value = 3)] SoundTableId_DropHero,
    [ProtoEnum(Name = "SoundTableId_Error", Value = 4)] SoundTableId_Error,
    [ProtoEnum(Name = "SoundTableId_LoginMusic", Value = 5)] SoundTableId_LoginMusic,
    [ProtoEnum(Name = "SoundTableId_ChangeJobMusic", Value = 6)] SoundTableId_ChangeJobMusic,
    [ProtoEnum(Name = "SoundTableId_ChangeJobMusic2", Value = 7)] SoundTableId_ChangeJobMusic2,
    [ProtoEnum(Name = "SoundTableId_GoddessDialogMusic", Value = 8)] SoundTableId_GoddessDialogMusic,
    [ProtoEnum(Name = "SoundTableId_BattleMoveWalk", Value = 9)] SoundTableId_BattleMoveWalk,
    [ProtoEnum(Name = "SoundTableId_BattleMoveRide", Value = 10)] SoundTableId_BattleMoveRide,
    [ProtoEnum(Name = "SoundTableId_BattleMoveFly", Value = 11)] SoundTableId_BattleMoveFly,
    [ProtoEnum(Name = "SoundTableId_HeroJobTransferMusic", Value = 12)] SoundTableId_HeroJobTransferMusic,
    [ProtoEnum(Name = "SoundTableId_ShowStartAnimation", Value = 13)] SoundTableId_ShowStartAnimation,
    [ProtoEnum(Name = "SoundTableId_HeroDieAtCombat", Value = 14)] SoundTableId_HeroDieAtCombat,
    [ProtoEnum(Name = "SoundTableId_AddExpToHero", Value = 15)] SoundTableId_AddExpToHero,
    [ProtoEnum(Name = "SoundTableId_SkipCombatHeroDead", Value = 16)] SoundTableId_SkipCombatHeroDead,
    [ProtoEnum(Name = "SoundTableId_SkipCombatSoldierDead", Value = 17)] SoundTableId_SkipCombatSoldierDead,
    [ProtoEnum(Name = "SoundTableId_SkipCombatHpBreakL", Value = 18)] SoundTableId_SkipCombatHpBreakL,
    [ProtoEnum(Name = "SoundTableId_SkipCombatHpBreakS", Value = 19)] SoundTableId_SkipCombatHpBreakS,
    [ProtoEnum(Name = "SoundTableId_HeroDungeonEnter", Value = 20)] SoundTableId_HeroDungeonEnter,
    [ProtoEnum(Name = "SoundTableId_HeroDungeonGearRoll", Value = 21)] SoundTableId_HeroDungeonGearRoll,
    [ProtoEnum(Name = "SoundTableId_ShowDangerRegion", Value = 22)] SoundTableId_ShowDangerRegion,
  }
}
