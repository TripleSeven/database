﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.RandomDropRewardArea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  public class RandomDropRewardArea
  {
    public List<RandomDropRewardGroup> groups;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomDropRewardArea()
    {
      // ISSUE: unable to decompile the method.
    }

    public int AreaId { get; set; }

    public int CanDropMaxCount { get; set; }
  }
}
