﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataMailInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataMailInfo")]
  [Serializable]
  public class ConfigDataMailInfo : IExtensible
  {
    private int _ID;
    private string _Title;
    private string _Content;
    private List<Goods> _Attachments;
    private uint _ExpiredTime;
    private int _ReadedExpiredTime;
    private bool _GotDeleted;
    private GameFunctionType _GameFunctionType;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMailInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Title")]
    public string Title
    {
      get
      {
        return this._Title;
      }
      set
      {
        this._Title = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Content")]
    public string Content
    {
      get
      {
        return this._Content;
      }
      set
      {
        this._Content = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "Attachments")]
    public List<Goods> Attachments
    {
      get
      {
        return this._Attachments;
      }
      set
      {
        this._Attachments = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExpiredTime")]
    public uint ExpiredTime
    {
      get
      {
        return this._ExpiredTime;
      }
      set
      {
        this._ExpiredTime = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadedExpiredTime")]
    public int ReadedExpiredTime
    {
      get
      {
        return this._ReadedExpiredTime;
      }
      set
      {
        this._ReadedExpiredTime = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "GotDeleted")]
    public bool GotDeleted
    {
      get
      {
        return this._GotDeleted;
      }
      set
      {
        this._GotDeleted = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionType")]
    public GameFunctionType GameFunctionType
    {
      get
      {
        return this._GameFunctionType;
      }
      set
      {
        this._GameFunctionType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
