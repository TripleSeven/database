﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGoddessDialogInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGoddessDialogInfo")]
  [Serializable]
  public class ConfigDataGoddessDialogInfo : IExtensible
  {
    private int _ID;
    private string _Name;
    private int _NextDialog_ID;
    private string _PreAnimation;
    private string _PreFacialAnimation;
    private string _IdleAnimation;
    private string _IdleFacialAnimation;
    private string _Voice;
    private string _Words;
    private string _FeedbackVoice1;
    private string _FeedbackVoice2;
    private string _FeedbackVoice3;
    private string _FeedbackText1;
    private string _FeedbackText2;
    private string _FeedbackText3;
    private string _Choice1Text;
    private int _Choice1NextDialog_ID;
    private List<ChoiceValue> _Choice1Value;
    private string _Choice2Text;
    private int _Choice2NextDialog_ID;
    private List<ChoiceValue> _Choice2Value;
    private string _Choice3Text;
    private int _Choice3NextDialog_ID;
    private List<ChoiceValue> _Choice3Value;
    private GoddessDialogFuncType _FunctionType;
    private IExtension extensionObject;
    public ConfigDataGoddessDialogInfo m_nextDialogInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGoddessDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextDialog_ID")]
    public int NextDialog_ID
    {
      get
      {
        return this._NextDialog_ID;
      }
      set
      {
        this._NextDialog_ID = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "PreAnimation")]
    public string PreAnimation
    {
      get
      {
        return this._PreAnimation;
      }
      set
      {
        this._PreAnimation = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "PreFacialAnimation")]
    public string PreFacialAnimation
    {
      get
      {
        return this._PreFacialAnimation;
      }
      set
      {
        this._PreFacialAnimation = value;
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "IdleAnimation")]
    public string IdleAnimation
    {
      get
      {
        return this._IdleAnimation;
      }
      set
      {
        this._IdleAnimation = value;
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "IdleFacialAnimation")]
    public string IdleFacialAnimation
    {
      get
      {
        return this._IdleFacialAnimation;
      }
      set
      {
        this._IdleFacialAnimation = value;
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "Voice")]
    public string Voice
    {
      get
      {
        return this._Voice;
      }
      set
      {
        this._Voice = value;
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "Words")]
    public string Words
    {
      get
      {
        return this._Words;
      }
      set
      {
        this._Words = value;
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "FeedbackVoice1")]
    public string FeedbackVoice1
    {
      get
      {
        return this._FeedbackVoice1;
      }
      set
      {
        this._FeedbackVoice1 = value;
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "FeedbackVoice2")]
    public string FeedbackVoice2
    {
      get
      {
        return this._FeedbackVoice2;
      }
      set
      {
        this._FeedbackVoice2 = value;
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "FeedbackVoice3")]
    public string FeedbackVoice3
    {
      get
      {
        return this._FeedbackVoice3;
      }
      set
      {
        this._FeedbackVoice3 = value;
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = true, Name = "FeedbackText1")]
    public string FeedbackText1
    {
      get
      {
        return this._FeedbackText1;
      }
      set
      {
        this._FeedbackText1 = value;
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = true, Name = "FeedbackText2")]
    public string FeedbackText2
    {
      get
      {
        return this._FeedbackText2;
      }
      set
      {
        this._FeedbackText2 = value;
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "FeedbackText3")]
    public string FeedbackText3
    {
      get
      {
        return this._FeedbackText3;
      }
      set
      {
        this._FeedbackText3 = value;
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.Default, IsRequired = true, Name = "Choice1Text")]
    public string Choice1Text
    {
      get
      {
        return this._Choice1Text;
      }
      set
      {
        this._Choice1Text = value;
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Choice1NextDialog_ID")]
    public int Choice1NextDialog_ID
    {
      get
      {
        return this._Choice1NextDialog_ID;
      }
      set
      {
        this._Choice1NextDialog_ID = value;
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, Name = "Choice1Value")]
    public List<ChoiceValue> Choice1Value
    {
      get
      {
        return this._Choice1Value;
      }
      set
      {
        this._Choice1Value = value;
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.Default, IsRequired = true, Name = "Choice2Text")]
    public string Choice2Text
    {
      get
      {
        return this._Choice2Text;
      }
      set
      {
        this._Choice2Text = value;
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Choice2NextDialog_ID")]
    public int Choice2NextDialog_ID
    {
      get
      {
        return this._Choice2NextDialog_ID;
      }
      set
      {
        this._Choice2NextDialog_ID = value;
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.Default, Name = "Choice2Value")]
    public List<ChoiceValue> Choice2Value
    {
      get
      {
        return this._Choice2Value;
      }
      set
      {
        this._Choice2Value = value;
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.Default, IsRequired = true, Name = "Choice3Text")]
    public string Choice3Text
    {
      get
      {
        return this._Choice3Text;
      }
      set
      {
        this._Choice3Text = value;
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Choice3NextDialog_ID")]
    public int Choice3NextDialog_ID
    {
      get
      {
        return this._Choice3NextDialog_ID;
      }
      set
      {
        this._Choice3NextDialog_ID = value;
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.Default, Name = "Choice3Value")]
    public List<ChoiceValue> Choice3Value
    {
      get
      {
        return this._Choice3Value;
      }
      set
      {
        this._Choice3Value = value;
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FunctionType")]
    public GoddessDialogFuncType FunctionType
    {
      get
      {
        return this._FunctionType;
      }
      set
      {
        this._FunctionType = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
