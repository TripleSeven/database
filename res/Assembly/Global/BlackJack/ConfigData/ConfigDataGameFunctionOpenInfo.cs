﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataGameFunctionOpenInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataGameFunctionOpenInfo")]
  [Serializable]
  public class ConfigDataGameFunctionOpenInfo : IExtensible
  {
    private int _ID;
    private GameFunctionType _GameFunctionType;
    private string _Name;
    private GameFunctionOpenConditionType _OpenCondition;
    private int _OpenConditionParam1;
    private string _Message;
    private IExtension extensionObject;

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionType")]
    public GameFunctionType GameFunctionType
    {
      get
      {
        return this._GameFunctionType;
      }
      set
      {
        this._GameFunctionType = value;
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      get
      {
        return this._Name;
      }
      set
      {
        this._Name = value;
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenCondition")]
    public GameFunctionOpenConditionType OpenCondition
    {
      get
      {
        return this._OpenCondition;
      }
      set
      {
        this._OpenCondition = value;
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "OpenConditionParam1")]
    public int OpenConditionParam1
    {
      get
      {
        return this._OpenConditionParam1;
      }
      set
      {
        this._OpenConditionParam1 = value;
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Message")]
    public string Message
    {
      get
      {
        return this._Message;
      }
      set
      {
        this._Message = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
