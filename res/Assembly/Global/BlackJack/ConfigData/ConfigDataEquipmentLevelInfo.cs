﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.ConfigDataEquipmentLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "ConfigDataEquipmentLevelInfo")]
  [Serializable]
  public class ConfigDataEquipmentLevelInfo : IExtensible
  {
    private int _ID;
    private List<int> _RankEquipmentNextExps;
    private IExtension extensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEquipmentLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ID")]
    public int ID
    {
      get
      {
        return this._ID;
      }
      set
      {
        this._ID = value;
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "RankEquipmentNextExps")]
    public List<int> RankEquipmentNextExps
    {
      get
      {
        return this._RankEquipmentNextExps;
      }
      set
      {
        this._RankEquipmentNextExps = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
