﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.TarotIDPair
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;
using System;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "TarotIDPair")]
  [Serializable]
  public class TarotIDPair : IExtensible
  {
    private int _RaffleID;
    private int _TarotID;
    private IExtension extensionObject;

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RaffleID")]
    public int RaffleID
    {
      get
      {
        return this._RaffleID;
      }
      set
      {
        this._RaffleID = value;
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TarotID")]
    public int TarotID
    {
      get
      {
        return this._TarotID;
      }
      set
      {
        this._TarotID = value;
      }
    }

    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      return Extensible.GetExtensionObject(ref this.extensionObject, createIfMissing);
    }
  }
}
