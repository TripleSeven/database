﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ConfigData.CollectionActivityWaypointFuncType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using ProtoBuf;

namespace BlackJack.ConfigData
{
  [ProtoContract(Name = "CollectionActivityWaypointFuncType")]
  public enum CollectionActivityWaypointFuncType
  {
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_None", Value = 0)] CollectionActivityWaypointFuncType_None,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Default", Value = 1)] CollectionActivityWaypointFuncType_Default,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Empty", Value = 2)] CollectionActivityWaypointFuncType_Empty,
    [ProtoEnum(Name = "CollectionActivityWaypointFuncType_Event", Value = 3)] CollectionActivityWaypointFuncType_Event,
  }
}
