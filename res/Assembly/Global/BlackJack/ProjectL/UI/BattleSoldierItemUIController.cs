﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleSoldierItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleSoldierItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierRangeText;
    [AutoBind("./Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMoveText;
    [AutoBind("./Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImage;
    [AutoBind("./Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImage2;
    [AutoBind("./TitleBg/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./SelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectButton;
    [AutoBind("./SelectTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectPanel;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGrapgic;
    private UISpineGraphic m_graphic;
    private ConfigDataModelSkinResourceInfo m_solderSkinResInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSoldierItem;
    private static DelegateBridge __Hotfix_OnSelectButtonClick;
    private static DelegateBridge __Hotfix_SetSelectPanel;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_OnDestroy;
    private static DelegateBridge __Hotfix_add_EventOnSoldierItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnSoldierItemClick;
    private static DelegateBridge __Hotfix_add_EventOnSelectButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSelectButtonClick;
    private static DelegateBridge __Hotfix_set_SoldierInfo;
    private static DelegateBridge __Hotfix_get_SoldierInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleSoldierItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleSoldierItemUIController> EventOnSelectButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
