﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildMemberInfoItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildMemberInfoItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_headIconStateCtrl;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIcon;
    [AutoBind("./Char/HeadIcon/HeadIconGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconGrey;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_HeadFrameDummy;
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Char/PositionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_positionText;
    [AutoBind("./PowerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_powerText;
    [AutoBind("./ExploitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_exploitText;
    [AutoBind("./VitalityText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_vitalityText;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineStateCtrl;
    [AutoBind("./StateGroup/OffLine/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_OffLineText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitItemInfo;
    private static DelegateBridge __Hotfix_SetTitleTextByEnum;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;
    private static DelegateBridge __Hotfix_get_Member;
    private static DelegateBridge __Hotfix_set_Member;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitItemInfo(GuildMember member, bool isSelf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string SetTitleTextByEnum(GuildTitle title)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GuildMemberInfoItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GuildMember Member
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
