﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSimpleItemInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroSimpleItemInfoUIController : UIControllerBase
  {
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Job/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroJobText;
    [AutoBind("./Army", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyImage;
    [AutoBind("./Army/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_armyText;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStars;
    [AutoBind("./Property/HP/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHPImage;
    [AutoBind("./Property/DF/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroDFImage;
    [AutoBind("./Property/AT/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroATImage;
    [AutoBind("./Property/MagicDF/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroMagicDFImage;
    [AutoBind("./Property/Magic/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroMagicImage;
    [AutoBind("./Property/DEX/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroDEXImage;
    [AutoBind("./Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_talentIconImg;
    [AutoBind("./Talent/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talentNameText;
    [AutoBind("./Talent/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talentDescText;
    [AutoBind("./FinalJobs/Job1/NameText_CH", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob1CHNameText;
    [AutoBind("./FinalJobs/Job1/NameText_EN", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob1ENNameText;
    [AutoBind("./FinalJobs/Job1/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finalJob1GraphicObj;
    [AutoBind("./FinalJobs/Job2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finalJob2Obj;
    [AutoBind("./FinalJobs/Job2/NameText_CH", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob2CHNameText;
    [AutoBind("./FinalJobs/Job2/NameText_EN", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_finalJob2ENNameText;
    [AutoBind("./FinalJobs/Job2/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finalJob2GraphicObj;
    public Hero m_hero;
    private UISpineGraphic m_heroJobGraphic1;
    private UISpineGraphic m_heroJobGraphic2;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitHeroSimpleItemInfo;
    private static DelegateBridge __Hotfix_SetValues;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroSimpleItemInfo(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetValues()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ref UISpineGraphic m_graphic,
      GameObject m_grapgicObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(ref UISpineGraphic m_graphic)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
