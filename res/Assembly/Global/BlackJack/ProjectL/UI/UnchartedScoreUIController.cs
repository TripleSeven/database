﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedScoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedScoreUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamUIStateController;
    [AutoBind("./PlayerResource/DailyReward/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyRewardCountText;
    [AutoBind("./ActivitiesName/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./ActivityInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activityUIStateController;
    [AutoBind("./Background/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_backgroundImage;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGo;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./LevelList/ToggleGroup/ScoreToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_scoreLevelToggle;
    [AutoBind("./LevelList/ToggleGroup/ChallengeToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_challengeLevelToggle;
    [AutoBind("./RewardGroup/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardButton;
    [AutoBind("./RewardGroup/RecommendHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendHeroButton;
    [AutoBind("./RewardGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./RewardPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_scoreRewardUIStateController;
    [AutoBind("./RewardPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreRewardBGButton;
    [AutoBind("./RewardPanel/Detail/ListScrollView/Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private DynamicGrid m_scoreRewardGrid;
    [AutoBind("./RewardPanel/Detail/CountScore/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreRewardScoreText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/UnchartedScoreLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unchartedScoreLevelListItemPrefab;
    private bool m_isIgnoreToggleEvent;
    private ConfigDataUnchartedScoreInfo m_unchartedScoreInfo;
    private UISpineGraphic m_graphic;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetBattleType;
    private static DelegateBridge __Hotfix_SetScore;
    private static DelegateBridge __Hotfix_SetUnchartedScoreInfo;
    private static DelegateBridge __Hotfix_SetAllUnchartedScoreLevelListItems;
    private static DelegateBridge __Hotfix_SetAllUnchartedChallengeLevelListItems;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_SetDailyRewardCount;
    private static DelegateBridge __Hotfix_OnScoreRewardGridCellInit;
    private static DelegateBridge __Hotfix_OnScrollRewardGridCellUpdate;
    private static DelegateBridge __Hotfix_ShowScoreReward;
    private static DelegateBridge __Hotfix_Co_ScrollToItem;
    private static DelegateBridge __Hotfix_ScrollToItem;
    private static DelegateBridge __Hotfix_HideScoreReward;
    private static DelegateBridge __Hotfix_ShowRecommendHero;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnTeamButtonClick;
    private static DelegateBridge __Hotfix_OnRewardButtonClick;
    private static DelegateBridge __Hotfix_OnRecommendHeroButtonClick;
    private static DelegateBridge __Hotfix_OnScoreLevelToggleValueChanged;
    private static DelegateBridge __Hotfix_OnChallengeLevelToggleValueChanged;
    private static DelegateBridge __Hotfix_OnScoreRewardCloseButtonClick;
    private static DelegateBridge __Hotfix_UnchartedScoreLevelListItem_OnStartButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnShowTeam;
    private static DelegateBridge __Hotfix_remove_EventOnShowTeam;
    private static DelegateBridge __Hotfix_add_EventOnChangeBattleType;
    private static DelegateBridge __Hotfix_remove_EventOnChangeBattleType;
    private static DelegateBridge __Hotfix_add_EventOnStartUnchartedScoreLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartUnchartedScoreLevel;
    private static DelegateBridge __Hotfix_add_EventOnStartUnchartedChallengeLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartUnchartedChallengeLevel;

    private UnchartedScoreUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleType(BattleType battleType, bool isOpening)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScore(int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnchartedScoreInfo(
      ConfigDataUnchartedScoreInfo unchartedScoreInfo,
      ConfigDataUnchartedScoreModelInfo unchartedScoreModelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllUnchartedScoreLevelListItems(IEnumerable<ConfigDataScoreLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllUnchartedChallengeLevelListItems(
      IEnumerable<ConfigDataChallengeLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataUnchartedScoreModelInfo unchartedScoreModelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyRewardCount(int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DynamicGridCellController OnScoreRewardGridCellInit(
      GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScrollRewardGridCellUpdate(DynamicGridCellController cell)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowScoreReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideScoreReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRecommendHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreLevelToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChallengeLevelToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedScoreLevelListItem_OnStartButtonClick(
      UnchartedScoreLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType> EventOnChangeBattleType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataScoreLevelInfo> EventOnStartUnchartedScoreLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataChallengeLevelInfo> EventOnStartUnchartedChallengeLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
