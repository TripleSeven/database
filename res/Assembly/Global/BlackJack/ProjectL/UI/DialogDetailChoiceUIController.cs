﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogDetailChoiceUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public sealed class DialogDetailChoiceUIController : UIControllerBase
  {
    private DialogDetailChoiceItemUIController m_choiceItem;
    [AutoBind("./NameGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject NameGroupGameObject;
    private static DelegateBridge __Hotfix_SetDialogDetailChoice;
    private static DelegateBridge __Hotfix_SetButtonName;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialogDetailChoice(DialogDetailChoiceItemUIController choiceItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetButtonName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
