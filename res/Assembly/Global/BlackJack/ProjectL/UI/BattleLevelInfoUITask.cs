﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleLevelInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class BattleLevelInfoUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private BattleLevelInfoUIController m_battleLevelInfoUIController;
    private RaidLevelUITask m_raidLevelUITask;
    private ConfigDataRiftLevelInfo m_riftLevelInfo;
    private ConfigDataHeroDungeonLevelInfo m_heroDungeonLevelInfo;
    private BattleType m_battleType;
    private NeedGoods m_needGoods;
    private bool m_needOpenTween;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateRiftLevelInfo;
    private static DelegateBridge __Hotfix_UpdateHeroDungeonLevelInfo;
    private static DelegateBridge __Hotfix_StartRiftRaidLevelUITask;
    private static DelegateBridge __Hotfix_StartHeroDungeonLevelRaidUITask;
    private static DelegateBridge __Hotfix_RaidUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartRiftLevelRaidNetTask;
    private static DelegateBridge __Hotfix_StartHeroDungeonLevelRaidNetTask;
    private static DelegateBridge __Hotfix_BattleLevelInfoUIController_OnStartBattle;
    private static DelegateBridge __Hotfix_BattleLevelInfoUIController_OnRaidBattle;
    private static DelegateBridge __Hotfix_BattleLevelInfoUIController_OnShowAchievement;
    private static DelegateBridge __Hotfix_BattleLevelInfoUIController_OnClose;
    private static DelegateBridge __Hotfix_RaidLevelUITask_OnRiftRaidClose;
    private static DelegateBridge __Hotfix_RaidLevelUITask_OnHeroDungeonRaidClose;
    private static DelegateBridge __Hotfix_RaidLevelUITask_OnRiftRaidComplete;
    private static DelegateBridge __Hotfix_RaidLevelUITask_OnHeroDungeonRaidComplete;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnRiftRaidComplete;
    private static DelegateBridge __Hotfix_remove_EventOnRiftRaidComplete;
    private static DelegateBridge __Hotfix_add_EventOnHeroDungeonRaidComplete;
    private static DelegateBridge __Hotfix_remove_EventOnHeroDungeonRaidComplete;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleLevelInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRiftLevelInfo(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroDungeonLevelInfo(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftRaidLevelUITask(
      int riftLevelID,
      BattleReward reward,
      List<Goods> extraReward,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelRaidUITask(
      int heroDungeonLevelID,
      BattleReward reward,
      List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftLevelRaidNetTask(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelRaidNetTask(int levelId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnRaidBattle(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnShowAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnRiftRaidClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnHeroDungeonRaidClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnRiftRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RaidLevelUITask_OnHeroDungeonRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRiftRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroDungeonRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
