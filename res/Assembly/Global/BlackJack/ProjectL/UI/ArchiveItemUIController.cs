﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArchiveItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArchiveItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_ItemButton;
    [AutoBind("./SelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectImage;
    [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImage;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrEffect;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starGroup;
    [AutoBind("./DarkCoverImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_darkCoverImage;
    public EquipmentArchiveUIController.EquipmentInfoWrap m_equipmentInfoWrap;
    private const int SSRRank = 4;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetEquipmentInfo;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_OnItemClick;
    private static DelegateBridge __Hotfix_SetSelected;
    private static DelegateBridge __Hotfix_add_OnItemClickEvent;
    private static DelegateBridge __Hotfix_remove_OnItemClickEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEquipmentInfo(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool isSelected)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ArchiveItemUIController> OnItemClickEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
