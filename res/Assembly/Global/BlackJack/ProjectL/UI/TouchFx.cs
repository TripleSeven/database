﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TouchFx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class TouchFx
  {
    private string m_hitFxName;
    private string m_loopFxName;
    private Camera m_camera;
    private GameObject m_disableInputGameObject;
    private GraphicPool m_graphicPool;
    private FxPlayer m_fxPlayer;
    private List<TouchFx.TouchState> m_touchStates;
    private GenericGraphic[] m_loopFxs;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetHitFxName;
    private static DelegateBridge __Hotfix_SetLoopFxName;
    private static DelegateBridge __Hotfix_SetDisableInputGameObject;
    private static DelegateBridge __Hotfix_SetTouchFXParentActive;
    private static DelegateBridge __Hotfix_ClearFx;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Tick;

    [MethodImpl((MethodImplOptions) 32768)]
    public TouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject parent, Camera camera)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHitFxName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLoopFxName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDisableInputGameObject(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchFXParentActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    public struct TouchState
    {
      public int fingerId;
      public Vector2 position;
      public TouchPhase phase;
    }
  }
}
