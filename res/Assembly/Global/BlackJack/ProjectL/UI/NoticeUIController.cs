﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class NoticeUIController : UIControllerBase
  {
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_noticeButton;
    [AutoBind("./Button/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeDetailGo;
    [AutoBind("./Button/Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_noticeText;
    [AutoBind("./Button/Detail/VoiceImage", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_noticeVoiceImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private static bool s_isForceHide;
    private static NoticeUIController s_instance;
    private const float NoticeTweenSpeed = 100f;
    private const float ShowNoticeInterval = 1f;
    private const float ShowNoticeTime = 5f;
    private Vector3 m_noticeTextInitPos;
    private float m_noticeTextInitWidth;
    private Coroutine m_delayHideNoticeCoroutine;
    private TweenPos m_noticeShowTween;
    private TweenPos m_noticeMoveTween;
    private NoticeText m_currentNoticeText;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_IsNoticeAvailable;
    private static DelegateBridge __Hotfix_ShowNotice;
    private static DelegateBridge __Hotfix_SetPositionState;
    private static DelegateBridge __Hotfix_ShowNextNotice;
    private static DelegateBridge __Hotfix_OnNoticeShowTweenFinished;
    private static DelegateBridge __Hotfix_OnNoticeMoveTweenFinished;
    private static DelegateBridge __Hotfix_DelayHideNotice;
    private static DelegateBridge __Hotfix_OnNoticeButtonClick;
    private static DelegateBridge __Hotfix_get_IsForceHide;
    private static DelegateBridge __Hotfix_set_IsForceHide;
    private static DelegateBridge __Hotfix_add_EventOnNoticeClick;
    private static DelegateBridge __Hotfix_remove_EventOnNoticeClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNoticeAvailable(ChatChannel chatChannel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNotice(
      string txt,
      string stateName,
      ChatChannel chatChannel,
      ChatContentType chatContentType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPositionState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeShowTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeMoveTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayHideNotice(float time, float additiveTime = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsForceHide
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<NoticeText> EventOnNoticeClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
