﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UserBindUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UserBindUIController : UIControllerBase
  {
    [AutoBind("./IDVerbSequencePanel/VerbSequence/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./IDVerbSequencePanel/VerbSequence/Button4", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    [AutoBind("./IDVerbSequencePanel/VerbSequence/ButtonGroup/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TwitterButton;
    [AutoBind("./IDVerbSequencePanel/VerbSequence/ButtonGroup/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_FacebookButton;
    [AutoBind("./IDVerbSequencePanel/VerbSequence/ButtonGroup/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_GoogleButton;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnDetailButtonClick;
    private static DelegateBridge __Hotfix_OnFacebookButtonClick;
    private static DelegateBridge __Hotfix_OnTwitterBindClick;
    private static DelegateBridge __Hotfix_OnGoogleBindClick;
    private static DelegateBridge __Hotfix_SetLinkageState;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnConfirm;
    private static DelegateBridge __Hotfix_remove_EventOnConfirm;

    [MethodImpl((MethodImplOptions) 32768)]
    private UserBindUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFacebookButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterBindClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoogleBindClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLinkageState(string user_type)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
