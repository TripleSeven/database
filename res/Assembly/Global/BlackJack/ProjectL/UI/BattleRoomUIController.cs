﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleRoomUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleRoomUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("/PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerGroupGameObject;
    [AutoBind("./ExpressionGroup/Empty", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatEmptyGameObject;
    [AutoBind("./ExpressionGroup/ExpressionButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_expressionButton;
    [AutoBind("./ExpressionGroup/TalkButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button TalkButton;
    [AutoBind("./ExpressionGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_expressionGroupUIStateController;
    [AutoBind("./ExpressionGroupDummy/BGMask", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx ButtonBGMask;
    [AutoBind("./TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_myTimeGroupUIStateController;
    [AutoBind("./TimeGroup/SurplusTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_myTimeText;
    [AutoBind("./TimeGroup/ReserveTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_myTimeText2;
    [AutoBind("./BigNumberEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bigTimeUIStateController;
    [AutoBind("./BigNumberEffect/Number", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bigTimeText;
    [AutoBind("./ActionPlayer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_otherPlayerUIStateController;
    [AutoBind("./ActionPlayer/Image", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_otherPlayerEnemyUIStateController;
    [AutoBind("./ActionPlayer/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_otherPlayerTagImage;
    [AutoBind("./ActionPlayer/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_otherPlayerTimeGroupUIStateController;
    [AutoBind("./ActionPlayer/TimeGroup/SurplusTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_otherPlayerTimeText;
    [AutoBind("./ActionPlayer/TimeGroup/ReserveTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_otherPlayerTimeText2;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/BattleTeamPlayer", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleTeamPlayerPrefab;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private List<BattleTeamPlayerUIController> m_players;
    private BigExpressionController m_bigExpressionCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_StartBattle;
    private static DelegateBridge __Hotfix_AddPlayers;
    private static DelegateBridge __Hotfix_AddPlayer;
    private static DelegateBridge __Hotfix_ClearPlayers;
    private static DelegateBridge __Hotfix_SetPlayerStatus;
    private static DelegateBridge __Hotfix_SetPlayerAction;
    private static DelegateBridge __Hotfix_SetPlayerHeroCount;
    private static DelegateBridge __Hotfix_SetPlayerHeroAlive;
    private static DelegateBridge __Hotfix_ShowPlayerChat;
    private static DelegateBridge __Hotfix_ShowPlayerBigExpression;
    private static DelegateBridge __Hotfix_ShowPlayerVoice;
    private static DelegateBridge __Hotfix_GetPlayerUIController;
    private static DelegateBridge __Hotfix_SetOtherActionAsTeammate;
    private static DelegateBridge __Hotfix_SetOtherActionAsEnemy;
    private static DelegateBridge __Hotfix_ShowOtherActionCountdown;
    private static DelegateBridge __Hotfix_HideOtherActionCountdown;
    private static DelegateBridge __Hotfix_SetOtherActionCountdown;
    private static DelegateBridge __Hotfix_ShowMyActionCountdown;
    private static DelegateBridge __Hotfix_HideMyActionCountdown;
    private static DelegateBridge __Hotfix_SetMyActionCountdown;
    private static DelegateBridge __Hotfix_ShowBigCountdown;
    private static DelegateBridge __Hotfix_SetCanUseChat;
    private static DelegateBridge __Hotfix_OnExpressionButtonClick;
    private static DelegateBridge __Hotfix_OnBigExpressionClick;
    private static DelegateBridge __Hotfix_OnBigExpressionBGClick;
    private static DelegateBridge __Hotfix_ShowExpressionGroup;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddPlayers(BattleRoom battleRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayer(BattleRoomPlayer player, int heroCount, bool showPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerStatus(int playerIndex, PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerAction(int playerIndex, bool isAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeroCount(int playerIndex, int heroCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeroAlive(int playerIndex, int heroIndex, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerChat(int playerIndex, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerBigExpression(int playerIndex, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerVoice(int playerIndex, ChatVoiceMessage voiceMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleTeamPlayerUIController GetPlayerUIController(
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOtherActionAsTeammate(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOtherActionAsEnemy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOtherActionCountdown(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideOtherActionCountdown(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOtherActionCountdown(BattleRoomType battleRoomType, TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMyActionCountdown(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMyActionCountdown(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyActionCountdown(BattleRoomType battleRoomType, TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBigCountdown(int sec)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanUseChat(bool canChat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowExpressionGroup(bool show)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
