﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaHeroCardCircleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaHeroCardCircleUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroItemButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroItemUIStateController;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./ArmyIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyIconImage;
    [AutoBind("./Mercenary", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mercenaryGameObject;
    [AutoBind("./LevelBGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    public PeakArenaManagementTeamUIController.HeroWrap m_heroWrap;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroListItemInfo;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListItemInfo(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PeakArenaHeroCardCircleUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
