﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroKeywordUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroKeywordUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tagPanelStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelBGButton;
    [AutoBind("./KeywordPanelDetil/InfoPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelTitleText;
    [AutoBind("./KeywordPanelDetil/InfoPanel/ScrollRect/ViewPort/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelDescText;
    [AutoBind("./KeywordPanelDetil/InfoPanel/KeywordIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tagPanelIconImage;
    [AutoBind("./KeywordPanelDetil/InfoPanel/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelListScrollViewContent;
    [AutoBind("./KeywordPanelDetil/InfoPanel/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_tagPanelListScrollRect;
    [AutoBind("./Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelHeroIconImagePrefab;
    [AutoBind("./KeywordPanelDetil/InfoPanel/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelCloseBtn;
    [AutoBind("./KeywordPanelDetil/RuleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bottomText;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_CloseTagPanel;
    private static DelegateBridge __Hotfix_SetTagPanelInfo;
    private static DelegateBridge __Hotfix_SortRelatedHeroIdByRank;
    private static DelegateBridge __Hotfix_OnHideEnd;
    private static DelegateBridge __Hotfix_add_eventOnHideEnd;
    private static DelegateBridge __Hotfix_remove_eventOnHideEnd;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseTagPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTagPanelInfo(
      ConfigDataHeroTagInfo tagInfo,
      bool showSuperMark,
      string bottomTextStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortRelatedHeroIdByRank(int heroId1, int heroId2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHideEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action eventOnHideEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
