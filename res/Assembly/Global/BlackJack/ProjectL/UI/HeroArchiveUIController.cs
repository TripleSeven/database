﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroArchiveUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroArchiveUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prefabAnimation;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Margin/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroShowButton;
    [AutoBind("./Margin/LinkageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_linkageHeroShowButton;
    [AutoBind("./HeroListGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroContent;
    [AutoBind("./HeroListGroup/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_heroContentInfinityGrid;
    [AutoBind("./Prefab/HeroHeadPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroHeadPrefab;
    [AutoBind("./CountLimit/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ownHeroCountText;
    [AutoBind("./HeroGroup/HeroEnglishNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroEnglishNameText;
    [AutoBind("./HeroGroup/PaperRightImage/JobPatternTitleBgImage/SpineGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_spineJobDummy;
    [AutoBind("./HeroGroup/PaperRightImage/HeroPatternTitle/HeroDescScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_introTextscrollRect;
    [AutoBind("./HeroGroup/PaperRightImage/HeroPatternTitle/HeroDescScrollView/Viewport/Content/HeroMoreText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroIntroText;
    [AutoBind("./HeroGroup/PaperRightImage/ApproachPatternTitleBgImage/ExplainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroObtainText;
    [AutoBind("./HeroGroup/HeroDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroDrawShowDummy;
    [AutoBind("./HeroGroup/HeroMessageGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroMessageGroup;
    [AutoBind("./HeroGroup/GradeToggle/All", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charALLToggle;
    [AutoBind("./HeroGroup/GradeToggle/SSR", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charSSRToggle;
    [AutoBind("./HeroGroup/GradeToggle/SR", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charSRToggle;
    [AutoBind("./HeroGroup/GradeToggle/R", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charRToggle;
    private HeroMessageGroupUIComponent m_messageGroupUIComponent;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<HeroArchiveUIController.HeroWrap> m_selectHeroList;
    private List<HeroArchiveUIController.HeroWrap> m_heroAllList;
    private List<HeroArchiveUIController.HeroWrap> m_heroSSRList;
    private List<HeroArchiveUIController.HeroWrap> m_heroSRList;
    private List<HeroArchiveUIController.HeroWrap> m_heroRList;
    private List<HeroHeadUIController> m_heroHeadUIControllerList;
    private HeroArchiveUIController.HeroWrap m_selectHeroWrap;
    private GameObject m_selectHeroPainting;
    private List<UISpineGraphic> m_jobSpineGraphicList;
    private int m_allOwnHeroCount;
    private int m_ssrOwnHeroCount;
    private int m_srOwnHeroCount;
    private int m_rOwnHeroCount;
    private const int FinalJobRank = 4;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetTask;
    private static DelegateBridge __Hotfix_EnterController;
    private static DelegateBridge __Hotfix_HeroListCompare;
    private static DelegateBridge __Hotfix_GetHeroFinalJobList;
    private static DelegateBridge __Hotfix_HeroCountRefresh;
    private static DelegateBridge __Hotfix_HeroDrawRefresh;
    private static DelegateBridge __Hotfix_CreateHeroGraphic;
    private static DelegateBridge __Hotfix_CreateHeroHeadList;
    private static DelegateBridge __Hotfix_DestroyHeroHeadList;
    private static DelegateBridge __Hotfix_RefreshHeroHeadWithHeroData;
    private static DelegateBridge __Hotfix_OnReturnClick;
    private static DelegateBridge __Hotfix_OnCharFilterClick;
    private static DelegateBridge __Hotfix_OnHeroHeadClick;
    private static DelegateBridge __Hotfix_OnHeroShowClick;
    private static DelegateBridge __Hotfix_OnLinkageHeroShowClick;
    private static DelegateBridge __Hotfix_OnHeroHeadListRefresh;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroArchiveUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTask(ArchiveUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListCompare(
      HeroArchiveUIController.HeroWrap heroWrap1,
      HeroArchiveUIController.HeroWrap heroWrap2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataJobConnectionInfo> GetHeroFinalJobList(
      ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCountRefresh(int ownCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDrawRefresh(HeroHeadUIController heroHeadUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateHeroGraphic(HeroArchiveUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateHeroHeadList(List<HeroArchiveUIController.HeroWrap> heroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyHeroHeadList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshHeroHeadWithHeroData(HeroArchiveUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharFilterClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroHeadClick(HeroHeadUIController heroHeadUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLinkageHeroShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroHeadListRefresh(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    public class HeroWrap
    {
      public ConfigDataHeroInfo hero;
      public bool isUnlocked;
      public bool isSelect;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public HeroWrap()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
