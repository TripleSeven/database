﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedStoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedStoreUIController : UIControllerBase
  {
    private int m_autoOpenItemId = -1;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unchartedStoreAnimation;
    [AutoBind("./StoreUIPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./StoreUIPanel/PlayerResource", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerCurrencyNode;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeItemContent;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabEquip", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabSkin", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinToggle;
    [AutoBind("./Prefab/StoreItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeItem;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private UnchartedStoreUITask m_unchartedStoreUITask;
    private CurrencyUIController m_currencyUIController;
    private UnchartedStoreUIComponent m_equipStoreUIComponent;
    private UnchartedStoreUIComponent m_skinStoreUIComponent;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SwitchTab;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_OnReturnClick;
    private static DelegateBridge __Hotfix_OnStoreTabClick;
    private static DelegateBridge __Hotfix_ClearStoreItem;
    private static DelegateBridge __Hotfix_OnAddCurrencyClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchTab(StoreId storeId, int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreTabClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStoreItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAddCurrencyClick(GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
