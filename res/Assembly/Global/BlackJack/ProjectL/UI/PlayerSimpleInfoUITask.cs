﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerSimpleInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class PlayerSimpleInfoUITask : UITask
  {
    private PlayerSimpleInfoUIController m_mainCtrl;
    private BusinessCard m_currInfo;
    private UIIntent m_returnUITaskIntent;
    private Vector3 m_panelPos;
    private PlayerSimpleInfoUITask.PostionType m_showPanelType;
    private string m_deleteFriendUserID;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string ModeParams_Stranger = "StrangerInfoMode";
    public const string ModeParams_Friend = "FriendInfoMode";
    public const string ModeParams_Guild = "GuildInfoMode";
    public const string Params_PanelPos = "PlayerSimpleInfoPanelPostion";
    public const string Params_PanelType = "PlayerSimpleInfoPanelType";
    public const string Params_ReturnToIntent = "PlayerSimpleInfoReturnToIntent";
    private List<string> m_friendAddList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnShowPlayerInfo;
    private static DelegateBridge __Hotfix_ShowPlayerSimpleInfoPanel;
    private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_IsInBattle;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnWatchCard;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnPrivateChat;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnPK;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnPeakArenaPK;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnDeleteFriend;
    private static DelegateBridge __Hotfix_OnDeleteFreindDialogBoxCallback;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnAddFriend;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnBlock;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnLike;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnReturn;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnExpel;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnVChairmanChange;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_ChairmanMove;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUIController_OnChairmanRelieve;
    private static DelegateBridge __Hotfix_ClosePanel;
    private static DelegateBridge __Hotfix_RegisterUIEvent;
    private static DelegateBridge __Hotfix_UnRegiterUIEvent;
    private static DelegateBridge __Hotfix_SetPanelPostion;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnGetSocialRelation;
    private static DelegateBridge __Hotfix_remove_EventOnGetSocialRelation;
    private static DelegateBridge __Hotfix_add_EventOnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBlockUser;
    private static DelegateBridge __Hotfix_remove_EventOnBlockUser;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnShowPlayerInfo(
      string userID,
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType postionType = PlayerSimpleInfoUITask.PostionType.Right,
      UIIntent returnIntent = null,
      bool isFromGuild = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerSimpleInfoUITask ShowPlayerSimpleInfoPanel(
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType postionType = PlayerSimpleInfoUITask.PostionType.Right,
      UIIntent returnIntent = null,
      bool isFromGuild = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void InitlizeBeforeManagerStartIt()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsInBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnWatchCard(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnPrivateChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnPK(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnPeakArenaPK(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnDeleteFriend(BusinessCard userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeleteFreindDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnAddFriend(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnBlock(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnLike(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnExpel(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnVChairmanChange(string userId, bool isAppiont)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_ChairmanMove(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUIController_OnChairmanRelieve(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterUIEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnRegiterUIEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPanelPostion()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetSocialRelation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BusinessCard> EventOnPrivateChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBlockUser
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum PostionType
    {
      UseInput,
      Left,
      Right,
    }
  }
}
