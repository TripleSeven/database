﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedStoreUIComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedStoreUIComponent
  {
    private StoreId m_fixedStoreId;
    private GameObject m_itemPrefab;
    private GameObject m_content;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private UnchartedStoreUITask m_unchartedStoreUITask;
    private UnchartedStoreUIController m_unchartedStoreUIController;
    private CurrencyUIController m_currencyUIController;
    private GameObjectPool<StoreItemUIController> m_fixedStoreItemPool;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetFixedStoreInfo;
    private static DelegateBridge __Hotfix_ItemSort;
    private static DelegateBridge __Hotfix_SetCurrencyState;
    private static DelegateBridge __Hotfix_ClearFiexdStoreItem;
    private static DelegateBridge __Hotfix_OnStoreItemClick;
    private static DelegateBridge __Hotfix_OnBuyItemSuccess;
    private static DelegateBridge __Hotfix_add_EventOnBuyItemSuccess;
    private static DelegateBridge __Hotfix_remove_EventOnBuyItemSuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedStoreUIComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      GameObject itemPrefab,
      GameObject itemContent,
      CurrencyUIController currencyUIController,
      UnchartedStoreUIController unchartedStoreUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFixedStoreInfo(StoreId fixedStoreID, int goodsId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ItemSort(FixedStoreItem item1, FixedStoreItem item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrencyState(List<GoodsType> currencyList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearFiexdStoreItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStoreItemClick(StoreItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyItemSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBuyItemSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
