﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelectCardClock3DController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class SelectCardClock3DController : UIControllerBase
  {
    [AutoBind("/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clockEffect;
    [AutoBind("/Mesh_FX_Clock", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_clockEffectStateCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowClockEffect;
    private static DelegateBridge __Hotfix_OnClockDrag;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClockEffect(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClockDrag(int rank, Action finishedEvent)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
