﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaReportListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaReportListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Player_Lt/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_leftPlayerIconImage;
    [AutoBind("./Player_Lt/ModeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_leftPlayerModeStateCtrl;
    [AutoBind("./Player_Lt/MatchGroup/WinPlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_leftPlayerMatchStateCtrl;
    [AutoBind("./Player_Lt/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerNameText;
    [AutoBind("./Player_Rt/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rightPlayerIconImage;
    [AutoBind("./Player_Rt/ModeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rightPlayerModeStateCtrl;
    [AutoBind("./Player_Rt/MatchGroup/WinPlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rightPlayerMatchStateCtrl;
    [AutoBind("./Player_Rt/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerNameText;
    [AutoBind("./Date", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleDateText;
    [AutoBind("./Date/TextTime", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleDateTimesText;
    [AutoBind("./TitleText/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./BattleTime/Text_Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleNameText;
    [AutoBind("./BattleTime/ChangeName/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleNameChangeButton;
    [AutoBind("./ButtonGroup/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareButton;
    [AutoBind("./ButtonGroup/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shareButtonStateCtrl;
    [AutoBind("./ButtonGroup/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_replayButton;
    [AutoBind("./ButtonGroup/CancelShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelShareButton;
    private ProjectLPlayerContext m_playerContext;
    private BattleReportHead m_battleReport;
    private ulong m_battleReportId;
    private string m_sourceUserId;
    private PeakArenaPlayOffInfoGetNetTask m_curNetTask;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetPeakArenaReportListItem;
    private static DelegateBridge __Hotfix_SetReportType;
    private static DelegateBridge __Hotfix_SetPeakArenaReportListItemInfo;
    private static DelegateBridge __Hotfix_SetBattleReportName;
    private static DelegateBridge __Hotfix_GetBattleReportInstanceId;
    private static DelegateBridge __Hotfix_GetMatchStateCtrlNameByWinCount;
    private static DelegateBridge __Hotfix_OnReplyButtonClick;
    private static DelegateBridge __Hotfix_OnShareButtonClick;
    private static DelegateBridge __Hotfix_OnCancelShareClick;
    private static DelegateBridge __Hotfix_OnBattleNameChangeButtonClick;
    private static DelegateBridge __Hotfix_OnReportRenameSuccess;
    private static DelegateBridge __Hotfix_add_EventOnReplyButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnReplyButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShareButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnShareButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCancelShareClick;
    private static DelegateBridge __Hotfix_remove_EventOnCancelShareClick;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportNameChangeSuccess;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportNameChangeSuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaReportListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaReportListItem(
      BattleReportHead report,
      string reportName,
      string sourceUserId,
      PeakArenaReportListItemUIController.ReportType reportType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetReportType(
      PeakArenaReportListItemUIController.ReportType reportType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaReportListItemInfo(
      PeakArenaLadderBattleReport battleReport,
      string reportName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportName(BattleReportHead report, string reportName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetBattleReportInstanceId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetMatchStateCtrlNameByWinCount(bool isLeft, int totalCount, int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelShareClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleNameChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportRenameSuccess(ulong reportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ulong> EventOnReplyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnShareButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnCancelShareClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnBattleReportNameChangeSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum ReportType
    {
      Replay,
      MyShare,
      ReportShare,
    }
  }
}
