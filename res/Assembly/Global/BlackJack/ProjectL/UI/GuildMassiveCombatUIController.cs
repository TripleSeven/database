﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildMassiveCombatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildMassiveCombatUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoButton;
    [AutoBind("./Margin/RankingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankingButton;
    [AutoBind("./Margin/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardsButton;
    [AutoBind("./RightButtom/GiveUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_giveUpButton;
    [AutoBind("./Margin/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/ChatButton/Redmark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatButtonRedmark;
    [AutoBind("./CompletePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_completePanelGameObject;
    [AutoBind("./CompletePanel/CompleteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_completeButton;
    [AutoBind("./WorldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_worldpButton;
    [AutoBind("./DifficultLevel/ModoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_difficultLevelStateCtrl;
    [AutoBind("./GuildCoinPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildCoinText;
    [AutoBind("./IntegralPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildIntegralText;
    [AutoBind("./IntegralPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildIntegralButton;
    [AutoBind("./GuildCoinPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildCoinButton;
    [AutoBind("./IntegralPanelDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_integralPanelDescGo;
    [AutoBind("./IntegralPanelDesc/BG/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_integralPanelBoundauryGo;
    [AutoBind("./IntegralPanelDesc/BG/ItemInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralPanelDescNameText;
    [AutoBind("./IntegralPanelDesc/BG/BGImage/FrameImage/BottomImage2/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralPanelDescText;
    [AutoBind("./QuestPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questPanelGo;
    [AutoBind("./ContributionPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_contributionPanelStateCtrl;
    [AutoBind("./ContributionPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_contributionPanelBackBGButton;
    [AutoBind("./RewardInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardInfoPanelStateCtrl;
    [AutoBind("./RewardInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardInfoBackBGButton;
    [AutoBind("./QuestInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_questInfoPanelStateCtrl;
    [AutoBind("./QuestInfoPanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_questInfoPanelBackBGButton;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/StageImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_questInfoPanelStageImage;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/RewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questInfoPanelRewardGroup;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/Hard/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_questInfoPanelHardText;
    [AutoBind("./QuestInfoPanel/Detail/LeftPart/SuppressValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_questInfoPanelSuppressValueText;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_questInfoPanelHeroListScrollRect;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questInfoPanelHeroListScrollContent;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_questInfoPanelTeamButton;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/PersonalButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_questInfoPanelPersonalButton;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/BuffDesc/Text/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_questInfoPanelBuffIconImage;
    [AutoBind("./QuestInfoPanel/Detail/RightPart/BuffDesc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_questInfoPanelBuffText;
    [AutoBind("./Prefab/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_questInfoPanelHeroListItemPrefab;
    [AutoBind("./WinOrFailedEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildMassiveCombatResultStateCtrl;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GuildMassiveCombatInfo m_curCombatInfo;
    private GuildMassiveCombatStronghold m_curSelectStrongHold;
    private List<GuildMassiveCombatStrongHoldUIController> m_strongHoldsCtrlList;
    private bool m_isShowGiveupDialogBox;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUpdateView;
    private static DelegateBridge __Hotfix_UpdateStrongHolds;
    private static DelegateBridge __Hotfix_OnGuildMassiveCombatStrongHoldClick;
    private static DelegateBridge __Hotfix_ShowStrongholdInfoPanel;
    private static DelegateBridge __Hotfix_CloseStrongholdInfoPanel;
    private static DelegateBridge __Hotfix_SetRewardGroup;
    private static DelegateBridge __Hotfix_SetHeroListPanel;
    private static DelegateBridge __Hotfix_GetCombatHeroList;
    private static DelegateBridge __Hotfix_ComparerCombatHeroByPower;
    private static DelegateBridge __Hotfix_OnQuestInfoPanelBackBgButtonClick;
    private static DelegateBridge __Hotfix_ShowCombatResultEffect;
    private static DelegateBridge __Hotfix_ShowGiveupButton;
    private static DelegateBridge __Hotfix_ShowCompletePanel;
    private static DelegateBridge __Hotfix_Co_ShowCombatResultEffect;
    private static DelegateBridge __Hotfix_OnQuestInfoPanelPersonalButtonClick;
    private static DelegateBridge __Hotfix_OnQuestInfoPanelTeamButtonClick;
    private static DelegateBridge __Hotfix_OnRankingButtonClick;
    private static DelegateBridge __Hotfix_OnRewardsButtonClick;
    private static DelegateBridge __Hotfix_OnGiveUpButtonClick;
    private static DelegateBridge __Hotfix_CloseGiveupDialogBox;
    private static DelegateBridge __Hotfix_OnCompleteButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnWorldButtonClick;
    private static DelegateBridge __Hotfix_OnGuildIntegralButtonClick;
    private static DelegateBridge __Hotfix_OnGuildCoinButtonClick;
    private static DelegateBridge __Hotfix_UpdateChatButtonRedmark;
    private static DelegateBridge __Hotfix_OnInfoButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_CloseSubWindow;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnChatButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnChatButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturnToWorld;
    private static DelegateBridge __Hotfix_remove_EventOnReturnToWorld;
    private static DelegateBridge __Hotfix_add_EventOnGiveUpButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGiveUpButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCompleteButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnCompleteButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnSelectStronghold;
    private static DelegateBridge __Hotfix_remove_EventOnSelectStronghold;
    private static DelegateBridge __Hotfix_add_EventOnSingleBattle;
    private static DelegateBridge __Hotfix_remove_EventOnSingleBattle;
    private static DelegateBridge __Hotfix_add_EventOnTeamBattle;
    private static DelegateBridge __Hotfix_remove_EventOnTeamBattle;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GuildMassiveCombatUpdateView(GuildMassiveCombatInfo combatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStrongHolds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildMassiveCombatStrongHoldClick(GuildMassiveCombatStrongHoldUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStrongholdInfoPanel(GuildMassiveCombatStronghold strongHold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseStrongholdInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardGroup(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroListPanel(GuildMassiveCombatStronghold strongHoldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Hero> GetCombatHeroList(GuildMassiveCombatStronghold strongHoldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComparerCombatHeroByPower(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuestInfoPanelBackBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCombatResultEffect(bool isWin, Action onFinish = null, float delay = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGiveupButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCompletePanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowCombatResultEffect(
      bool isWin,
      Action onFinish,
      float delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuestInfoPanelPersonalButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuestInfoPanelTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardsButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiveUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseGiveupDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWorldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildIntegralButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildCoinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatButtonRedmark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseSubWindow()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturnToWorld
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGiveUpButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCompleteButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatStronghold> EventOnSelectStronghold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatStronghold> EventOnSingleBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GuildMassiveCombatStronghold> EventOnTeamBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
