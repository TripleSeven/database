﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EventGiftBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EventGiftBoxUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailGameObject;
    [AutoBind("./Detail/PackageDetail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/ToggleGroupScrollView/Viewport/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_toggleGroup;
    [AutoBind("./Detail/ToggleGroupScrollView/Viewport/ToggleGroup/PackageToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_packagePrefab;
    [AutoBind("./Detail/PackageDetail/ActiveImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_activeImage;
    [AutoBind("./Detail/PackageDetail/ActiveImage/DetailText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_giftBoxName;
    [AutoBind("./Detail/PackageDetail/ActiveImage/NoticText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_giftBoxDesc;
    [AutoBind("./Detail/PackageDetail/ActiveTime", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activeTimeAnimation;
    [AutoBind("./Detail/PackageDetail/ActiveTime/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activiTimeText;
    [AutoBind("./Detail/PackageDetail/BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buyTimesAnimation;
    [AutoBind("./Detail/PackageDetail/BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buyTimesGameObject;
    [AutoBind("./Detail/PackageDetail/BuyTimes/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyTimesText;
    [AutoBind("./Detail/PackageDetail/ValueOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_saleOffGameObject;
    [AutoBind("./Detail/PackageDetail/ValueOffImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_saleOffText;
    [AutoBind("./Detail/PackageDetail/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyButton;
    [AutoBind("./Detail/PackageDetail/Button/IconImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_buyButtonIcon;
    [AutoBind("./Detail/PackageDetail/Button/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyButtonText;
    [AutoBind("./Detail/PackageDetail/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGroupGameObject;
    [AutoBind("./Detail/PackageDetail/ItemGroup/ItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_itemScrollView;
    [AutoBind("./Detail/PackageDetail/ItemGroup/ItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_itemContent;
    [AutoBind("./Prefab/PackageItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ItemPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private EventGiftBoxUITask m_eventGiftBoxUITask;
    private EventGiftBoxToggleUIController m_selectToggleUIController;
    private List<EventGiftBoxToggleUIController> m_toggleUIControllerList;
    private ItemPay m_itemPay;
    private GameObjectPool<EventGiftBoxItemUIController> m_ItemPool;
    private bool m_isProcessingPaySuccess;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewToggleList;
    private static DelegateBridge __Hotfix_OnGiftBoxClick;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnBuyClick;
    private static DelegateBridge __Hotfix_OnPaySuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    public EventGiftBoxUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Init(List<PDSDKGood> pdSDKGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewToggleList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftBoxClick(
      EventGiftBoxToggleUIController eventGiftBoxItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPaySuccess()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
