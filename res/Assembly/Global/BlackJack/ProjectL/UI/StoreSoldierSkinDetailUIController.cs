﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreSoldierSkinDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class StoreSoldierSkinDetailUIController : UIControllerBase
  {
    public List<SoldierShowItemController> m_soldierDefaultSkinItemCtrlList;
    public List<SoldierShowItemController> m_soldierBuySkinItemCtrlList;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_panelBgButton;
    [AutoBind("./LayoutRoot/BuyAndGot/SureBtn", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_buyButton;
    [AutoBind("./LayoutRoot/BuyAndGot/SureBtn/SureText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_priceText;
    [AutoBind("./LayoutRoot/BuyAndGot/SureBtn/SureIcon/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_currencyImage;
    [AutoBind("./LayoutRoot/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_nameText;
    [AutoBind("./LayoutRoot/BuyAndGot", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_buyStateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_panelStateUICtrl;
    [AutoBind("./LayoutRoot/SoldierRight/SoldierGroup/Soldier1", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierDefaultSkinItem1;
    [AutoBind("./LayoutRoot/SoldierRight/SoldierGroup/Soldier2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierDefaultSkinItem2;
    [AutoBind("./LayoutRoot/SoldierLeft/SoldierGroup/Soldier1", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierBuySkinItem1;
    [AutoBind("./LayoutRoot/SoldierLeft/SoldierGroup/Soldier2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_soldierBuySkinItem2;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateSoldierSkinDetailPanelByFixedStoreItemConfigInfo;
    private static DelegateBridge __Hotfix_UpdateSoldierSkinDetailPanelBySoldierSkinConfigInfo;
    private static DelegateBridge __Hotfix_PlayPanelOpenAnim;
    private static DelegateBridge __Hotfix_PlayPanelCloseAnim;
    private static DelegateBridge __Hotfix_SetBuyButtonToHasBuyMode;
    private static DelegateBridge __Hotfix_SetPanelBuyButtonState;
    private static DelegateBridge __Hotfix_SetSoldierDefaultAndSkinItemInfo;
    private static DelegateBridge __Hotfix_SetSpineAnim;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreSoldierSkinDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSoldierSkinDetailPanelByFixedStoreItemConfigInfo(
      ConfigDataFixedStoreItemInfo fixedStoreItemConfig,
      bool isNeedBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSoldierSkinDetailPanelBySoldierSkinConfigInfo(
      ConfigDataSoldierSkinInfo skinConfigInfo,
      bool isNeedBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPanelOpenAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPanelCloseAnim(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuyButtonToHasBuyMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetPanelBuyButtonState(bool isNeedBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSoldierDefaultAndSkinItemInfo(
      string skinName,
      List<Soldier2SkinResource> soldier2SkinResList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSpineAnim(
      string modelPath,
      ref UISpineGraphic graphic,
      GameObject go,
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
