﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerSettingUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using BlackJack.ProjectLBasic;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class PlayerSettingUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private PlayerSettingUIController m_playerSettingUIController;
    private IConfigDataLoader m_configDataLoader;
    private int m_selectedLanguageIndex;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_InitLanguageToggles;
    private static DelegateBridge __Hotfix_OnLanguageChangeDialogBoxBtnClicked;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnClose;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnMusicVolumeChanged;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnSFXVolumeChanged;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnVoiceVolumeChanged;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnBGMIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnSoundIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnVoiceIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnEndActionIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnNotifyRecoveryEnergyIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnNotifyRandomEventIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnNotifyArenaTicketIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnNotifyStoreRefreshIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnSetBattleAnimationMode;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnPowerSaveModeIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnResourceLowQualityModeIsOn;
    private static DelegateBridge __Hotfix_PlayerSettingUIController_OnLanguageIndexChanged;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSettingUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLanguageToggles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLanguageChangeDialogBoxBtnClicked(DialogBoxResult ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnMusicVolumeChanged(float volume)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnSFXVolumeChanged(float volume)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnVoiceVolumeChanged(float volume)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnBGMIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnSoundIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnVoiceIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnEndActionIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnNotifyRecoveryEnergyIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnNotifyRandomEventIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnNotifyArenaTicketIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnNotifyStoreRefreshIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnSetBattleAnimationMode(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnPowerSaveModeIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnResourceLowQualityModeIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSettingUIController_OnLanguageIndexChanged(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
