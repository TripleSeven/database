﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattlePrepareUIController : UIControllerBase
  {
    private static int s_testSoldierCount0 = 10;
    private static int s_testSoldierCount1 = 10;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/Title/PauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pauseButton;
    [AutoBind("./Margin/Title/ActorCount/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_stageActorCountText;
    [AutoBind("./Margin/Title/BattlePower", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battlePowerUIStateController;
    [AutoBind("./Margin/Title/BattlePower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battlePowerText;
    [AutoBind("./Margin/Title/BattlePower/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battlePowerArrowGo;
    [AutoBind("./Margin/RulePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleRulePanelUIStateController;
    [AutoBind("./Margin/RulePanel/Desc/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleRuleTitleText;
    [AutoBind("./Margin/RulePanel/Desc/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleRuleDescText;
    [AutoBind("./Margin/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/ChatButton/CountPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatRedPoint;
    [AutoBind("./Margin/ActionOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_actionOrderButton;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./RecommendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendHeroButton;
    [AutoBind("./TeamReady", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamReadyGameObject;
    [AutoBind("./TeamReady/Time", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamReadyTimeUIStateController;
    [AutoBind("./TeamReady/Time/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamReadyTimeText;
    [AutoBind("./TeamReady/Wait", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamReadyWaitGameObject;
    [AutoBind("./TeamReady/Wait/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamReadyWaitTeammateText;
    [AutoBind("./TeamReady/Wait/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamReadyWaitOpponentText;
    [AutoBind("./Test", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testGameObject;
    [AutoBind("./Test/TestOnStageToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_testOnStageToggle;
    [AutoBind("./Test/SoldierInputField0", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSoldierInputField0;
    [AutoBind("./Test/SoldierInputField1", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSoldierInputField1;
    [AutoBind("./Test/SkillInputField0", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSkillInputField0;
    [AutoBind("./Test/SkillInputField1", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testSkillInputField1;
    [AutoBind("./Test/TalentInputField0", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testTalentInputField0;
    [AutoBind("./Test/TalentInputField1", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_testTalentInputField1;
    [AutoBind("./ActorList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actorListGameObject;
    [AutoBind("./ActorList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_actorListScrollRect;
    [AutoBind("./ActorList/Disable", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actorListDisableGameObject;
    [AutoBind("./TerrainInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_terrainInfoGameObject;
    [AutoBind("./TerrainInfo/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoText;
    [AutoBind("./TerrainInfo/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_terrainInfoImage;
    [AutoBind("./TerrainInfo/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoDefText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroButtonPrefab;
    private GameObject m_armyRelationGameObject;
    private CommonUIStateController m_armyRelationButtonUIStateController;
    private CommonUIStateController m_armyRelationDescUIStateController;
    private ClientBattle m_clientBattle;
    private Camera m_camera;
    private float m_chatRedPointLastTime;
    private bool m_isIgnorePointerClick;
    private BattlePrepareStageActor m_pointerDownStageActor;
    private bool m_isIgnoreToggleEvent;
    private bool m_isSkipStageActorGraphic;
    private int m_stageActorCountMax;
    private int m_turnStageActorCountMax;
    private bool m_isStageActorChanged;
    private List<HeroDragButton> m_heroButtons;
    private List<BattlePrepareStageActor> m_stageActors;
    private List<BattlePrepareTreasure> m_treasures;
    private List<GridPosition>[] m_stagePositions;
    private List<int>[] m_stageDirs;
    private HeroDragButton m_draggingHeroButton;
    private static int s_testSkillId0;
    private static int s_testSkillId1;
    private static int s_testTalentId0;
    private static int s_testTalentId1;
    private static bool s_isTestOnStage;
    private int m_battlePowerValue;
    private Coroutine m_setBattlePowerValueCoroutine;
    private bool m_hasBattleRule;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_OnApplicationPause;
    private static DelegateBridge __Hotfix_OnApplicationFocus;
    private static DelegateBridge __Hotfix_CancelDragging;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_CheckStageActorChange;
    private static DelegateBridge __Hotfix_PrepareBattle;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_SetStageActorCountMax;
    private static DelegateBridge __Hotfix_SetTurnStageActorCountMax;
    private static DelegateBridge __Hotfix_ClearHeros;
    private static DelegateBridge __Hotfix_AddHero;
    private static DelegateBridge __Hotfix_ClearStagePositions;
    private static DelegateBridge __Hotfix_AddStagePosition;
    private static DelegateBridge __Hotfix_GetStagePositions;
    private static DelegateBridge __Hotfix_GetStageDirection;
    private static DelegateBridge __Hotfix_CreateHeroButton;
    private static DelegateBridge __Hotfix_SkipStageActorGraphic;
    private static DelegateBridge __Hotfix_EnableHeroOnStage;
    private static DelegateBridge __Hotfix_HeroOnStage_1;
    private static DelegateBridge __Hotfix_IsSkillListEqual;
    private static DelegateBridge __Hotfix_HeroOnStage_0;
    private static DelegateBridge __Hotfix_ActorOffStage;
    private static DelegateBridge __Hotfix_ActorOnStageMove;
    private static DelegateBridge __Hotfix_ActorOnStageExchange;
    private static DelegateBridge __Hotfix_SortChildByY;
    private static DelegateBridge __Hotfix_CompareTransformY;
    private static DelegateBridge __Hotfix_ClearStageActors;
    private static DelegateBridge __Hotfix_GetStageActor;
    private static DelegateBridge __Hotfix_GetStageActorByHeroId;
    private static DelegateBridge __Hotfix_GetStageActors;
    private static DelegateBridge __Hotfix_UpdateStageActor;
    private static DelegateBridge __Hotfix_AddBattleTreasure;
    private static DelegateBridge __Hotfix_ClearBattleTreasures;
    private static DelegateBridge __Hotfix_ShowTerrainInfo;
    private static DelegateBridge __Hotfix_HideTerrainInfo;
    private static DelegateBridge __Hotfix_ShowArmyRelationDesc;
    private static DelegateBridge __Hotfix_HideArmyRelationDesc;
    private static DelegateBridge __Hotfix_HideArmyRelation;
    private static DelegateBridge __Hotfix_IsArmyRelationDescVisible;
    private static DelegateBridge __Hotfix_ShowClimbTowerBattleRule;
    private static DelegateBridge __Hotfix_ShowEternalShrineBattleRule;
    private static DelegateBridge __Hotfix_ShowBattleRulePanel;
    private static DelegateBridge __Hotfix_HideBattleRulePanel;
    private static DelegateBridge __Hotfix_ShowRecommendHeroButton;
    private static DelegateBridge __Hotfix_ShowActionOrderButton;
    private static DelegateBridge __Hotfix_ShowChatButton;
    private static DelegateBridge __Hotfix_SetBattlePower;
    private static DelegateBridge __Hotfix_Co_SetBattlePowerValue;
    private static DelegateBridge __Hotfix_UpdateTestUI;
    private static DelegateBridge __Hotfix_UpdateTestValuesToInputField;
    private static DelegateBridge __Hotfix_SetInputFieldValue;
    private static DelegateBridge __Hotfix_UpdateTestValuesFromInputField;
    private static DelegateBridge __Hotfix_GetInputFieldValue;
    private static DelegateBridge __Hotfix_ShowStartButton;
    private static DelegateBridge __Hotfix_ShowHeroList;
    private static DelegateBridge __Hotfix_ShowTeamReadyCountdown;
    private static DelegateBridge __Hotfix_HideTeamReadyCountdown;
    private static DelegateBridge __Hotfix_ShowTeamReadyWait;
    private static DelegateBridge __Hotfix_HideTeamReadyWait;
    private static DelegateBridge __Hotfix_OnPauseButtonClick;
    private static DelegateBridge __Hotfix_OnArmyRelationButtonClick;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnRecommendHeroButtonClick;
    private static DelegateBridge __Hotfix_OnActionOrderButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_IsTestBattle;
    private static DelegateBridge __Hotfix_IsDeveloperSingleBattle;
    private static DelegateBridge __Hotfix_OnTestOnStageToggle;
    private static DelegateBridge __Hotfix_IsTestOnStage;
    private static DelegateBridge __Hotfix_GetTestSoldierCount;
    private static DelegateBridge __Hotfix_GetTestSkillId;
    private static DelegateBridge __Hotfix_GetTestTalentId;
    private static DelegateBridge __Hotfix_ClearPointerDownStageActor;
    private static DelegateBridge __Hotfix_OnScenePointerDown;
    private static DelegateBridge __Hotfix_OnScenePointerUp;
    private static DelegateBridge __Hotfix_OnScenePointerClick;
    private static DelegateBridge __Hotfix_OnSceneBeginDrag;
    private static DelegateBridge __Hotfix_OnSceneEndDrag;
    private static DelegateBridge __Hotfix_OnSceneDrag;
    private static DelegateBridge __Hotfix_OnScene3DTouch;
    private static DelegateBridge __Hotfix_CreateDraggingHeroButton;
    private static DelegateBridge __Hotfix_DestroyDragginHeroButton;
    private static DelegateBridge __Hotfix_MoveDraggingHeroButton;
    private static DelegateBridge __Hotfix_DropHeroButton;
    private static DelegateBridge __Hotfix_ComputeStageActorCount;
    private static DelegateBridge __Hotfix_ComputeSelfStageActorCount;
    private static DelegateBridge __Hotfix_UpdateStageActorCount;
    private static DelegateBridge __Hotfix_HeroDragButton_OnBeginDrag;
    private static DelegateBridge __Hotfix_HeroDragButton_OnEndDrag;
    private static DelegateBridge __Hotfix_HeroDragButton_OnDrag;
    private static DelegateBridge __Hotfix_HeroDragButton_OnDrop;
    private static DelegateBridge __Hotfix_HeroDragButton_OnClick;
    private static DelegateBridge __Hotfix_RefreshChatRedState;
    private static DelegateBridge __Hotfix_HideActorInfo;
    private static DelegateBridge __Hotfix_add_EventOnPauseBattle;
    private static DelegateBridge __Hotfix_remove_EventOnPauseBattle;
    private static DelegateBridge __Hotfix_add_EventOnShowArmyRelation;
    private static DelegateBridge __Hotfix_remove_EventOnShowArmyRelation;
    private static DelegateBridge __Hotfix_add_EventOnStart;
    private static DelegateBridge __Hotfix_remove_EventOnStart;
    private static DelegateBridge __Hotfix_add_EventOnShowRecommendHero;
    private static DelegateBridge __Hotfix_remove_EventOnShowRecommendHero;
    private static DelegateBridge __Hotfix_add_EventOnShowActionOrder;
    private static DelegateBridge __Hotfix_remove_EventOnShowActionOrder;
    private static DelegateBridge __Hotfix_add_EventOnTestOnStage;
    private static DelegateBridge __Hotfix_remove_EventOnTestOnStage;
    private static DelegateBridge __Hotfix_add_EventOnShowMyActorInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowMyActorInfo;
    private static DelegateBridge __Hotfix_add_EventOnHideActorInfo;
    private static DelegateBridge __Hotfix_remove_EventOnHideActorInfo;
    private static DelegateBridge __Hotfix_add_EventOnStageActorChange;
    private static DelegateBridge __Hotfix_remove_EventOnStageActorChange;
    private static DelegateBridge __Hotfix_add_EventOnShowChat;
    private static DelegateBridge __Hotfix_remove_EventOnShowChat;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnHeroOnStage;
    private static DelegateBridge __Hotfix_remove_EventOnHeroOnStage;
    private static DelegateBridge __Hotfix_add_EventOnStageActorOffStage;
    private static DelegateBridge __Hotfix_remove_EventOnStageActorOffStage;
    private static DelegateBridge __Hotfix_add_EventOnStageActorMove;
    private static DelegateBridge __Hotfix_remove_EventOnStageActorMove;
    private static DelegateBridge __Hotfix_add_EventOnStageActorSwap;
    private static DelegateBridge __Hotfix_remove_EventOnStageActorSwap;
    private static DelegateBridge __Hotfix_add_EventOnPointerDown;
    private static DelegateBridge __Hotfix_remove_EventOnPointerDown;
    private static DelegateBridge __Hotfix_add_EventOnPointerUp;
    private static DelegateBridge __Hotfix_remove_EventOnPointerUp;
    private static DelegateBridge __Hotfix_add_EventOnPointerClick;
    private static DelegateBridge __Hotfix_remove_EventOnPointerClick;
    private static DelegateBridge __Hotfix_add_EventOnBeginDragHero;
    private static DelegateBridge __Hotfix_remove_EventOnBeginDragHero;
    private static DelegateBridge __Hotfix_add_EventOnEndDragHero;
    private static DelegateBridge __Hotfix_remove_EventOnEndDragHero;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CancelDragging()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStageActorCountMax(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTurnStageActorCountMax(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(BattleHero hero, StageActorTagType tagType = StageActorTagType.None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStagePosition(GridPosition p, int dir, StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> GetStagePositions(StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetStageDirection(GridPosition p, StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroDragButton CreateHeroButton(BattleHero hero, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkipStageActorGraphic(bool skip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableHeroOnStage(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor HeroOnStage(
      BattleHero hero,
      GridPosition pos,
      int dir,
      int team,
      StagePositionType posType,
      StageActorTagType tagType = StageActorTagType.None,
      int behaviorId = -1,
      int groupId = 0,
      int playerIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsSkillListEqual(List<int> skillIds1, List<int> skillIds2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor HeroOnStage(
      BattleHero hero,
      GridPosition pos,
      int team,
      StagePositionType posType,
      int playerIndex,
      StageActorTagType tagType = StageActorTagType.None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOffStage(BattlePrepareStageActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOnStageMove(BattlePrepareStageActor sa, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActorOnStageExchange(BattlePrepareStageActor sa1, BattlePrepareStageActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortChildByY(Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareTransformY(Transform t0, Transform t1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor GetStageActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor GetStageActorByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattlePrepareStageActor> GetStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateStageActor(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleTreasure(ConfigDataBattleTreasureInfo treasureInfo, bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTerrainInfo(ConfigDataTerrainInfo terrain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArmyRelationDescVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClimbTowerBattleRule(ConfigDataTowerBattleRuleInfo ruleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEternalShrineBattleRule(ConfigDataEternalShrineInfo eternalShrineInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBattleRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRecommendHeroButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActionOrderButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChatButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattlePower(int battlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetBattlePowerValue(int newValue, int oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestValuesToInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void SetInputFieldValue(InputField inputField, int value, int defaultValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestValuesFromInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetInputFieldValue(InputField inputField, int defaultValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStartButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHeroList(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTeamReadyCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideTeamReadyCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTeamReadyWait(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideTeamReadyWait()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPauseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArmyRelationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActionOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTestBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDeveloperSingleBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestOnStageToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTestOnStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestSoldierCount(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestSkillId(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestTalentId(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPointerDownStageActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScene3DTouch(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingHeroButton(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginHeroButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingHeroButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DropHeroButton(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeStageActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeSelfStageActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnBeginDrag(HeroDragButton b, PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnEndDrag(HeroDragButton b, PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroDragButton_OnClick(HeroDragButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshChatRedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPauseBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowArmyRelation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRecommendHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowActionOrder
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTestOnStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero> EventOnShowMyActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHideActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStageActorChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero, GridPosition, int> EventOnHeroOnStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattlePrepareStageActor> EventOnStageActorOffStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattlePrepareStageActor, GridPosition> EventOnStageActorMove
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattlePrepareStageActor, BattlePrepareStageActor> EventOnStageActorSwap
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBeginDragHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndDragHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
