﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleResultUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleResultUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./AccountingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_accountingPanelUIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starsGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_star1UIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_star2UIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_star2Text;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star3", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_star3UIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Stars/Star3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_star3Text;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaResultGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult/ArenaPoint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult/VictoryPoint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaVictoryPointText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/ArenaResult/ArenaPoint/AutoFightText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaAutoFightGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_realtimePVPResultGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Score/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_realtimePVPScoreText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Win/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_realtimePVPWinText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Score/WinBonusText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_realtimePVPWinBonusGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/Score/WinBonusText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_realtimePVPWinBonusText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/RealtimePVPResult/RankProtect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_realtimePVPRankProtectGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Gold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goldGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_expGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/EXP/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerGroupLvText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/EXP/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Exp/LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerLevelUpGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/HeroExp/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/HeroExp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroExpGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enemyBoomToGoldText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyBoomToGoldGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/TreasureMapRewardScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_treasureMapRewardGroupGameObject;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/TreasureMapRewardScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_treasureMapRewardGroupScrollRect;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/EnemyBoomToGold/TreasureMapRewardScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GridLayoutGroup m_treasureMapRewardGroupGridLayputGroup;
    [AutoBind("./AchievementPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementPanelUIStateController;
    [AutoBind("./AchievementPanel/AchievementInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementGameObject;
    [AutoBind("./ClickScreenContinue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickScreenContinueGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemPrefab;
    [AutoBind("./Prefabs/EnemyItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyItemPrefab;
    private List<RewardHeroUIController> m_rewardHeros;
    private BattleAchievementItemUIController m_achievementItemUIController;
    private bool m_isClick;
    private PlayerLevelUpUITask m_playerLevelUpUITask;
    private float m_playerExpTotalWidth;
    private float m_singleAddExp;
    private float m_beforePlayerExp;
    private int m_finalPlayerExp;
    private float m_singleAddGold;
    private float m_beforePlayerGold;
    private int m_finalPlayerGold;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowBattleResult;
    private static DelegateBridge __Hotfix_Co_ShowBattleResult;
    private static DelegateBridge __Hotfix_AddPlayerExpBarWidth;
    private static DelegateBridge __Hotfix_UpdateTextValue;
    private static DelegateBridge __Hotfix_Co_ShowStars;
    private static DelegateBridge __Hotfix_Co_ShowHeros;
    private static DelegateBridge __Hotfix_Co_ShowEnemyBoomToGold;
    private static DelegateBridge __Hotfix_AutoMoveItemWhenOutOfScrollRect;
    private static DelegateBridge __Hotfix_Co_ShowPlayerLevelUp;
    private static DelegateBridge __Hotfix_Co_ShowAchievements;
    private static DelegateBridge __Hotfix_Co_SetGoldValueChanged;
    private static DelegateBridge __Hotfix_Co_SetAndWaitUIState;
    private static DelegateBridge __Hotfix_Co_WaitClick;
    private static DelegateBridge __Hotfix_PlayerLevelUpUITask_OnClose;
    private static DelegateBridge __Hotfix_ClearHeroItems;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleResultUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleResult(
      BattleType battleType,
      List<Hero> heros,
      int stars,
      int starTurnMax,
      int starDeadMax,
      int turn,
      BattleReward reward,
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowBattleResult(
      BattleType battleType,
      List<Hero> heros,
      int stars,
      int starTurnMax,
      int starDeadMax,
      int turn,
      BattleReward reward,
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AddPlayerExpBarWidth()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTextValue(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowStars(
      BattleType battleType,
      int stars,
      int starTurnMax,
      int starDeadMax,
      int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowHeros(
      BattleReward reward,
      List<Hero> heros,
      BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowEnemyBoomToGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoMoveItemWhenOutOfScrollRect(EnemyBoomToGoldUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowPlayerLevelUp(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowAchievements(
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetGoldValueChanged(float newValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelUpUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearHeroItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
