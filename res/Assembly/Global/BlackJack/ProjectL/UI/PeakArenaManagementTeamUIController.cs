﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaManagementTeamUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaManagementTeamUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamCompiledButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamCompiledButtonAnimation;
    [AutoBind("./RightPanel/ToggleGroup/HeroToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroToggle;
    [AutoBind("./RightPanel/ToggleGroup/MercenaryToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mercenaryToggle;
    [AutoBind("./RightPanel/ToggleGroup/HeroToggle/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroSelectCountText;
    [AutoBind("./RightPanel/ToggleGroup/MercenaryToggle/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_mercenarySelectCountText;
    [AutoBind("./RightPanel/FilterButton/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortButton;
    [AutoBind("./RightPanel/FilterButton/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sortButtonText;
    [AutoBind("./RightPanel/FilterButton/SortTypes/BGImages/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroSortBGButton;
    [AutoBind("./RightPanel/FilterButton/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroSortPanelAnimation;
    [AutoBind("./RightPanel/FilterButton/SortTypes/GridLayout/Power", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_powerToggle;
    [AutoBind("./RightPanel/FilterButton/SortTypes/GridLayout/Rank", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rankToggle;
    [AutoBind("./RightPanel/FilterButton/SortTypes/GridLayout/LV", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_lvToggle;
    [AutoBind("./RightPanel/HeroListScroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_heroListInfinityGrid;
    [AutoBind("./RightPanel/MercenaryProhibit", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mercenaryProhibitGameObject;
    [AutoBind("./Prefab/HeroListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemPrefab;
    [AutoBind("./LeftPanel/ChoosenHeroList/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_heroContentLeft;
    [AutoBind("./Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemCirclePrefab;
    [AutoBind("./LeftPanel/HeroCountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectCountText;
    [AutoBind("./LeftPanel/HeroCountGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxCountText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaManagementTeamUITask m_peakArenaManagementTeamUITask;
    private List<PeakArenaManagementTeamUIController.HeroWrap> m_heroWrapList;
    private List<PeakArenaManagementTeamUIController.HeroWrap> m_mercenaryWrapList;
    private List<PeakArenaManagementTeamUIController.HeroWrap> m_selectHeroWrapList;
    private Toggle m_selectHeroListToggle;
    private Toggle m_selectSortToggle;
    private List<PeakArenaHeroCardUIController> m_heroCardUIControllerList;
    private GameObjectPool<PeakArenaHeroCardCircleUIController> m_heroCardCirclePool;
    private int m_selectHeroCount;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_UpdateHeroData;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_LeftHeroListRefresh;
    private static DelegateBridge __Hotfix_HeroListRefresh;
    private static DelegateBridge __Hotfix_UpdateDataHeroSelect;
    private static DelegateBridge __Hotfix_IsBattleTeamModify;
    private static DelegateBridge __Hotfix_GetSelectHeroCount;
    private static DelegateBridge __Hotfix_HeroRankSort;
    private static DelegateBridge __Hotfix_HeroPowerSort;
    private static DelegateBridge __Hotfix_HeroLvSort;
    private static DelegateBridge __Hotfix_SortHero;
    private static DelegateBridge __Hotfix_OnReturnClick;
    private static DelegateBridge __Hotfix_OnHelpClick;
    private static DelegateBridge __Hotfix_OnTeamCompiledClick;
    private static DelegateBridge __Hotfix_OnHeroTabClick;
    private static DelegateBridge __Hotfix_OnSortClick;
    private static DelegateBridge __Hotfix_OnSortBgClick;
    private static DelegateBridge __Hotfix_OnSortTabClick;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_OnHeroDetailClick;
    private static DelegateBridge __Hotfix_OnHeroItemCircleClick;
    private static DelegateBridge __Hotfix_UpdateInfinityHeroItemCallback;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaManagementTeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateHeroData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LeftHeroListRefresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroListRefresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDataHeroSelect(
      PeakArenaManagementTeamUIController.HeroWrap selectHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBattleTeamModify()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSelectHeroCount(
      List<PeakArenaManagementTeamUIController.HeroWrap> heroWrapList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroRankSort(
      PeakArenaManagementTeamUIController.HeroWrap hero1,
      PeakArenaManagementTeamUIController.HeroWrap hero2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroPowerSort(
      PeakArenaManagementTeamUIController.HeroWrap hero1,
      PeakArenaManagementTeamUIController.HeroWrap hero2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroLvSort(
      PeakArenaManagementTeamUIController.HeroWrap hero1,
      PeakArenaManagementTeamUIController.HeroWrap hero2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortHero(Toggle selectSortToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamCompiledClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroTabClick(GameObject selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortBgClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortTabClick(Toggle selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroItemClick(PeakArenaHeroCardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroDetailClick(PeakArenaHeroCardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroItemCircleClick(PeakArenaHeroCardCircleUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateInfinityHeroItemCallback(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    public class HeroWrap
    {
      public Hero hero;
      public List<EquipmentBagItem> equipmentList;
      public bool isSelect;
      public bool otherSameHeroSelect;
      public bool isMercenary;
      public string serverName;
      public string playerName;
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_GetEquipmentByID;

      [MethodImpl((MethodImplOptions) 32768)]
      public HeroWrap()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public EquipmentBagItem GetEquipmentByID(ulong equipmentId)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
