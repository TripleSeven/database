﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSkillItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroSkillItemUIController : UIControllerBase, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillCost;
    [AutoBind("./CostsBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillCostBg;
    [AutoBind("./Select", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoSelectPanel;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillIconImg;
    [AutoBind("./LimitTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_limitTagObj;
    [AutoBind("./PassivityText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_passivityText;
    [AutoBind("./ChoosenImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_choosenImage;
    public ConfigDataSkillInfo m_skillInfo;
    private bool m_isSkillLimited;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSkillItem;
    private static DelegateBridge __Hotfix_SetSelectPanelActive;
    private static DelegateBridge __Hotfix_SetChoosenImageActive;
    private static DelegateBridge __Hotfix_SetLimitTagObjActive;
    private static DelegateBridge __Hotfix_SetCostPanelActive;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_add_EventOnShowDesc;
    private static DelegateBridge __Hotfix_remove_EventOnShowDesc;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSkillItem(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectPanelActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChoosenImageActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTagObjActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCostPanelActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroSkillItemUIController> EventOnShowDesc
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
