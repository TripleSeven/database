﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using MarchingBytes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TestUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./SystemInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_systemInfoButton;
    [AutoBind("./GoddessDialogButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goddessDialogButton;
    [AutoBind("./ClearUserGuideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearUserGuideButton;
    [AutoBind("./CompleteUserGuideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_completeUserGuideButton;
    [AutoBind("./BecomeStrong/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_becomeStrongButton;
    [AutoBind("./BecomeStrong/PlayerLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongPlayerLevelInput;
    [AutoBind("./BecomeStrong/HeroStarLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongHeroStarLevelInput;
    [AutoBind("./BecomeStrong/EquipmentLevelInput", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_becomeStrongEquipmentLevelInput;
    [AutoBind("./ClearStageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clearStageButton;
    [AutoBind("./ReportBugButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_openReportBugPanelButton;
    [AutoBind("./iPhoneXToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_iPhoneXToggle;
    [AutoBind("./ReloginButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_reloginButton;
    [AutoBind("./BecomeSuperAccountButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_becomeSuperAccountButton;
    [AutoBind("./TestToggles/BattleToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleToggle;
    [AutoBind("./TestToggles/DialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dialogToggle;
    [AutoBind("./TestToggles/BattleDialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleDialogToggle;
    [AutoBind("./TestToggles/UserGuideDialogToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_userGuideDialogToggle;
    [AutoBind("./TestList/LoopVerticalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_ListItemPool;
    [AutoBind("./TestList/LoopVerticalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_LoopScroolRect;
    [AutoBind("./MonsterLevel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monsterLevelGameObject;
    [AutoBind("./MonsterLevel/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_monsterLevelInputField;
    [AutoBind("./StartTestButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startTestBattleButton;
    [AutoBind("./TestId/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_TestIdInputField;
    [AutoBind("./TestId/Next", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TestIdNextSearch;
    [AutoBind("./TestId/Home", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_TestListHomeBtn;
    [AutoBind("./SystemInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_systemInfoGameObject;
    [AutoBind("./SystemInfo/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_systemInfoBackgroundButton;
    [AutoBind("./SystemInfo/ScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private UnityEngine.UI.Text m_systemInfoText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/TestListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testListItemPrefab;
    [AutoBind("./ServerDateTime", AutoBindAttribute.InitState.Active, false)]
    private UnityEngine.UI.Text m_serverDateTimeText;
    [AutoBind("./ReportBugPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeReportBugPanelButton;
    [AutoBind("./ReportBugPanel", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_reportBugPanelGameObject;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneHour", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneHourLogTime;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneDay", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneDayLogTime;
    [AutoBind("./ReportBugPanel/Panel/LogTime/Selection/OneWeek", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_oneWeekLogTime;
    [AutoBind("./ReportBugPanel/Panel/Description/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_bugDescriptionInputField;
    [AutoBind("./ReportBugPanel/Panel/ReportButtons/Cancel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelReportBugButton;
    [AutoBind("./ReportBugPanel/Panel/ReportButtons/Submit", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_reportBugButton;
    [AutoBind("./FinishAllTasksButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_finishAllTasksButton;
    [AutoBind("./PeakTest1Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaTest1Button;
    [AutoBind("./PeakTest2Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaTest2Button;
    [AutoBind("./ClientGMPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clientGMBPanel;
    [AutoBind("./ClientGMButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clientGMButton;
    private List<TestListItemUIController> m_testListItemUIControllers;
    private bool m_isIgnoreToggleEvent;
    private List<KeyValuePair<int, string>> m_CacheTestList;
    private TestListItemUIController m_CurrSelectCtrl;
    private int m_nSearchTestListId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetTestListType;
    private static DelegateBridge __Hotfix_AddTestListItem;
    private static DelegateBridge __Hotfix_ClearTestList;
    private static DelegateBridge __Hotfix_ScrollToItem;
    private static DelegateBridge __Hotfix_GetSelectedTestListItemUIController;
    private static DelegateBridge __Hotfix_SetMonsterLevel;
    private static DelegateBridge __Hotfix_GetMonsterLevel;
    private static DelegateBridge __Hotfix_SetServerDataTime;
    private static DelegateBridge __Hotfix_ShowSystemInfo;
    private static DelegateBridge __Hotfix_BuildSystemInfoText;
    private static DelegateBridge __Hotfix_AppendLine;
    private static DelegateBridge __Hotfix_HideSystemInfo;
    private static DelegateBridge __Hotfix_InitLoopScrollViewRect;
    private static DelegateBridge __Hotfix_OnPoolObjectCreated;
    private static DelegateBridge __Hotfix_RefreshLoopVerticalScrollView;
    private static DelegateBridge __Hotfix_OnTestItemFill;
    private static DelegateBridge __Hotfix_OnTestItemClick;
    private static DelegateBridge __Hotfix_DoToggle;
    private static DelegateBridge __Hotfix_OnBattleToggle;
    private static DelegateBridge __Hotfix_OnDialogToggle;
    private static DelegateBridge __Hotfix_OnBattleDialogToggle;
    private static DelegateBridge __Hotfix_OnUserGuideDialogToggle;
    private static DelegateBridge __Hotfix_OnTestListHomeBtnClick;
    private static DelegateBridge __Hotfix_OnTestIdNextBtnClick;
    private static DelegateBridge __Hotfix_OnTestIdInput;
    private static DelegateBridge __Hotfix_OnStartTestButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnSystemInfoButtonClick;
    private static DelegateBridge __Hotfix_OnGoddessDialogButtonClick;
    private static DelegateBridge __Hotfix_OnClearUserGuideButtonClick;
    private static DelegateBridge __Hotfix_OnBecomeStrongButtonClick;
    private static DelegateBridge __Hotfix_OnBecomeSuperAccountButtonClick;
    private static DelegateBridge __Hotfix_Co_BecomeSuperAccount;
    private static DelegateBridge __Hotfix_OnFinishAllTasksButtonClick;
    private static DelegateBridge __Hotfix_OnReloginButtonClick;
    private static DelegateBridge __Hotfix_OnClientGMClick;
    private static DelegateBridge __Hotfix_OnPeakArenaTest1ButtonClick;
    private static DelegateBridge __Hotfix_OnPeakArenaTest2ButtonClick;
    private static DelegateBridge __Hotfix_Co_PeakArenaTest1;
    private static DelegateBridge __Hotfix_FinishAllTasks;
    private static DelegateBridge __Hotfix_OnCancelReportBugButtonClick;
    private static DelegateBridge __Hotfix_OniPhoneXToggleValueChanged;
    private static DelegateBridge __Hotfix_OnReportBugButtonClick;
    private static DelegateBridge __Hotfix_GetReportBugLogHours;
    private static DelegateBridge __Hotfix_OnOpenReportBugPanelButtonClick;
    private static DelegateBridge __Hotfix_get_MaxPlayerLevel;
    private static DelegateBridge __Hotfix_UpgradePlayerLevel;
    private static DelegateBridge __Hotfix_SpeedUpAllHeros;
    private static DelegateBridge __Hotfix_SetEnchantProperty;
    private static DelegateBridge __Hotfix_SetEquipMasterProperty;
    private static DelegateBridge __Hotfix_BecomeStrong;
    private static DelegateBridge __Hotfix_TenLevelUpEquipment;
    private static DelegateBridge __Hotfix_SpeedUpHero;
    private static DelegateBridge __Hotfix_ResetHero;
    private static DelegateBridge __Hotfix_GetHeroJobConnSequence;
    private static DelegateBridge __Hotfix_GetJobSequence;
    private static DelegateBridge __Hotfix_GetEquipmentLevelLimit;
    private static DelegateBridge __Hotfix_IsEquipismentMaxLevel;
    private static DelegateBridge __Hotfix_StarLevelUpTopRankEquipments;
    private static DelegateBridge __Hotfix_AddItem4StarLevelUpEquipment;
    private static DelegateBridge __Hotfix_AddGoods;
    private static DelegateBridge __Hotfix_StarLevelUpEquipment;
    private static DelegateBridge __Hotfix_EnhanceAllTopRankEquipments;
    private static DelegateBridge __Hotfix_EnhanceEquipment;
    private static DelegateBridge __Hotfix_AddAllTopRankEquipments;
    private static DelegateBridge __Hotfix_AddAllHeros;
    private static DelegateBridge __Hotfix_AddHero;
    private static DelegateBridge __Hotfix_HeroBreak;
    private static DelegateBridge __Hotfix_BuyFixedStoreItem_1;
    private static DelegateBridge __Hotfix_BuyFixedStoreItem_0;
    private static DelegateBridge __Hotfix_TransferHeroJob;
    private static DelegateBridge __Hotfix_AddAllItem;
    private static DelegateBridge __Hotfix_UpgradeHeroJobLevel;
    private static DelegateBridge __Hotfix_SendGMCommand;
    private static DelegateBridge __Hotfix_UseItem;
    private static DelegateBridge __Hotfix_AddHeroExp;
    private static DelegateBridge __Hotfix_ErrorCodeToMessage;
    private static DelegateBridge __Hotfix_OnCompleteUserGuideButtonClick;
    private static DelegateBridge __Hotfix_OnClearStageButtonClick;
    private static DelegateBridge __Hotfix_OnSystemInfoBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnChangeTestList;
    private static DelegateBridge __Hotfix_remove_EventOnChangeTestList;
    private static DelegateBridge __Hotfix_add_EventOnStartTest;
    private static DelegateBridge __Hotfix_remove_EventOnStartTest;
    private static DelegateBridge __Hotfix_add_EventOnShowSystemInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowSystemInfo;
    private static DelegateBridge __Hotfix_add_EventOnCloseSystemInfo;
    private static DelegateBridge __Hotfix_remove_EventOnCloseSystemInfo;
    private static DelegateBridge __Hotfix_add_EventOnShowGoddessDialog;
    private static DelegateBridge __Hotfix_remove_EventOnShowGoddessDialog;
    private static DelegateBridge __Hotfix_add_EventOnClearUserGuide;
    private static DelegateBridge __Hotfix_remove_EventOnClearUserGuide;
    private static DelegateBridge __Hotfix_add_EventOnCompleteUserGuide;
    private static DelegateBridge __Hotfix_remove_EventOnCompleteUserGuide;
    private static DelegateBridge __Hotfix_add_EventOnClearStage;
    private static DelegateBridge __Hotfix_remove_EventOnClearStage;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnReportBug;
    private static DelegateBridge __Hotfix_remove_EventOnReportBug;

    [MethodImpl((MethodImplOptions) 32768)]
    private TestUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestListType(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTestListItem(int id, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTestList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ScrollToItem(ScrollRect scrollRect, int itemCount, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TestListItemUIController GetSelectedTestListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMonsterLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerDataTime(DateTime dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BuildSystemInfoText(bool textColor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendLine(StringBuilder sb, bool textColor, string name, string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSystemInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshLoopVerticalScrollView(TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestItemFill(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestItemClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoToggle(bool on, TestListType listType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDialogToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleDialogToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUserGuideDialogToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestListHomeBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestIdNextBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestIdInput(string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartTestButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSystemInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoddessDialogButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearUserGuideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBecomeStrongButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBecomeSuperAccountButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_BecomeSuperAccount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFinishAllTasksButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReloginButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClientGMClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaTest1ButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaTest2ButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PeakArenaTest1(int test)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator FinishAllTasks()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelReportBugButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OniPhoneXToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportBugButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private double GetReportBugLogHours()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenReportBugPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int MaxPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UpgradePlayerLevel(int targetPlayerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator SpeedUpAllHeros(int targetHeroLevel, int targetStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SetEnchantProperty(
      ulong equipmentInstanceId,
      string values,
      Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SetEquipMasterProperty(
      int heroId,
      int jobId,
      int equipSlot,
      int propertyIndex,
      int newValue,
      Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BecomeStrong(
      int targetPlayerLevel,
      int targetStarLevel,
      int targetEquipmentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator TenLevelUpEquipment(
      ulong equipmentInstanceId,
      Action OnEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator SpeedUpHero(
      int heroID,
      int targetHeroLevel,
      int targetStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator ResetHero(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<int> GetHeroJobConnSequence(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<int> GetJobSequence(int rootJobConnectionID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetEquipmentLevelLimit(int equipmentStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsEquipismentMaxLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator StarLevelUpTopRankEquipments(
      int ssrMaterialEquipmentId,
      int srMaterialEquipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddItem4StarLevelUpEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddGoods(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator StarLevelUpEquipment(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator EnhanceAllTopRankEquipments(int targetLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator EnhanceEquipment(
      ulong instanceId,
      List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllTopRankEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddHero(List<int> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator HeroBreak(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BuyFixedStoreItem(
      int fixedStoreID,
      int fixedStoreItemGoodsID,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator BuyFixedStoreItem(
      int fixedStoreID,
      int fixedStoreItemGoodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator TransferHeroJob(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddAllItem(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UpgradeHeroJobLevel(Hero hero, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator SendGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator UseItem(GoodsType type, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator AddHeroExp(
      int heroId,
      GoodsType itemType,
      int itemID,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ErrorCodeToMessage(int code)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteUserGuideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClearStageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSystemInfoBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TestListType> EventOnChangeTestList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnStartTest
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSystemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseSystemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowGoddessDialog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCompleteUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClearStage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, double> EventOnReportBug
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
