﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleLevelInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleLevelInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Name", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nameGameObject;
    [AutoBind("./Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Name/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameNumberText;
    [AutoBind("./Panel/Desc/Scrollrect/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./Panel/Hard/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_hardText;
    [AutoBind("./Panel/WinCondition/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionGroup;
    [AutoBind("./Panel/HiddenChest", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_treasureGameObject;
    [AutoBind("./Panel/HiddenChest/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_treasureCountText;
    [AutoBind("./Panel/Stars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allStarsGameObject;
    [AutoBind("./Panel/Stars/1/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star1GameObject;
    [AutoBind("./Panel/Stars/2/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star2GameObject;
    [AutoBind("./Panel/Stars/3/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star3GameObject;
    [AutoBind("./Panel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./Panel/RewardTitleImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardTitleGameObject;
    [AutoBind("./Panel/RewardTitleImage2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardTitle2GameObject;
    [AutoBind("./Panel/Reward/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGroupGameObject;
    [AutoBind("./Panel/ChallengeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_challengeGroupGameObject;
    [AutoBind("./Panel/ChallengeGroup/ChallengeCount/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeCountText;
    [AutoBind("./Panel/AchievementButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementButton;
    [AutoBind("./Panel/AchievementButton/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementButtonText;
    [AutoBind("./Panel/StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./Panel/StartButton/EnergyText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Panel/RaidTicketGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidTicketText;
    [AutoBind("./Panel/RaidButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_raidButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementUIStateController;
    [AutoBind("./Achievement/Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementScrollRect;
    [AutoBind("./Achievement/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementBackgroundButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_conditionPrefab;
    private List<BattleAchievementItemUIController> m_achievementItems;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetRiftLevel;
    private static DelegateBridge __Hotfix_SetHeroDungeonLevel;
    private static DelegateBridge __Hotfix_SetWinCondition;
    private static DelegateBridge __Hotfix_SetStars;
    private static DelegateBridge __Hotfix_SetCanChallengeCount;
    private static DelegateBridge __Hotfix_SetBattleTreasureCount;
    private static DelegateBridge __Hotfix_SetAchievementCount;
    private static DelegateBridge __Hotfix_SetRaidTicketCount;
    private static DelegateBridge __Hotfix_SetReward;
    private static DelegateBridge __Hotfix_ShowAchievement;
    private static DelegateBridge __Hotfix_AddAchievementItem;
    private static DelegateBridge __Hotfix_ClearAchievementItems;
    private static DelegateBridge __Hotfix_SetRiftLevelShowByType;
    private static DelegateBridge __Hotfix_LevelInfoOpenTween;
    private static DelegateBridge __Hotfix_EventLevelInfoOpenTween;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnRaidButtonClick;
    private static DelegateBridge __Hotfix_OnAchievementButtonClick;
    private static DelegateBridge __Hotfix_OnAchievementBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStartBattle;
    private static DelegateBridge __Hotfix_remove_EventOnStartBattle;
    private static DelegateBridge __Hotfix_add_EventOnRaidBattle;
    private static DelegateBridge __Hotfix_remove_EventOnRaidBattle;
    private static DelegateBridge __Hotfix_add_EventOnShowAchievement;
    private static DelegateBridge __Hotfix_remove_EventOnShowAchievement;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleLevelInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRiftLevel(ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroDungeonLevel(
      ConfigDataHeroDungeonLevelInfo heroDungeonLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetWinCondition(string conditionStrs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStars(int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanChallengeCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleTreasureCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievementCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRaidTicketCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReward(List<Goods> goodList, bool firstWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAchievement(BattleLevelAchievement[] achievements, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddAchievementItem(
      ConfigDataBattleAchievementInfo achievementInfo,
      List<Goods> rewards,
      GameObject prefab,
      bool complete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAchievementItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRiftLevelShowByType(RiftLevelType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelInfoOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EventLevelInfoOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRaidButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnStartBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRaidBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowAchievement
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
