﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CourseItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CourseItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Normal/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressBar;
    [AutoBind("./Normal/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_point;
    [AutoBind("./Normal/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Normal/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_valueText;
    [AutoBind("./Normal/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_maxText;
    [AutoBind("./Lock/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockNameText;
    [AutoBind("./Normal/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMark;
    public TrainingCourse m_trainingCourse;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitCourseInfo;
    private static DelegateBridge __Hotfix_ShowSelectFrame;
    private static DelegateBridge __Hotfix_OnCourseItemClick;
    private static DelegateBridge __Hotfix_add_EventOnCourseItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnCourseItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitCourseInfo(TrainingCourse course)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCourseItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<CourseItemUIController> EventOnCourseItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
