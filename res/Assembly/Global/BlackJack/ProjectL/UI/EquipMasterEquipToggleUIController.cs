﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterEquipToggleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterEquipToggleUIController : UIControllerBase
  {
    private readonly List<GameObject> m_iconItemGameObjectList;
    [AutoBind("./Group/IconItem", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_property1IconItemGameObject;
    [AutoBind("./Group/IconItem (1)", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_property2IconItemGameObject;
    [AutoBind("./Group/IconItem (2)", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_property3IconItemGameObject;
    [AutoBind("./Group/IconItem (3)", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_property4IconItemGameObject;
    [AutoBind("./Group/IconItem (4)", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_property5IconItemGameObject;
    [AutoBind("./KeepPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_effectGameObject;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowEnchantSuccessEffect;
    private static DelegateBridge __Hotfix_UpdateToggle;
    private static DelegateBridge __Hotfix_UpdateIconImage;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipMasterEquipToggleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnchantSuccessEffect(Action onFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateToggle(Hero hero, EquipMasterEquipType equipType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateIconImage(GameObject iconItem, PropertyModifyType propertyType)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
