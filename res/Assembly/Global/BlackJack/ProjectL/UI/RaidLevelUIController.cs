﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaidLevelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RaidLevelUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/Exp/LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpEffectObj;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/RaidAgainButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_raidAgainButton;
    [AutoBind("./Panel/CannelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cannelButton;
    [AutoBind("./Panel/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Panel/Exp/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./Panel/Exp/Bar/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./Panel/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./Panel/Rewards/Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGroupGameObject;
    [AutoBind("./Panel/ExtraRewards/Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_extraRewardGoodsGroupGameObject;
    [AutoBind("./Panel/RaidTicketGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_raidTicketText;
    [AutoBind("./NeedGoodsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_needGoodsPanelCommonUIStateCtrl;
    [AutoBind("./NeedGoodsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_needGoodsPanel;
    [AutoBind("./NeedGoodsPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_needGoodsIconImage;
    [AutoBind("./NeedGoodsPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needGoodsNameText;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lackBGImageGameObject;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/LackTitleText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lackTitleGameObject;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/LackText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needGoodsLackText;
    [AutoBind("./NeedGoodsPanel/LackAndEnoughGroup/EnoughText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_needGoodsEnoughGameObject;
    [AutoBind("./NeedGoodsPanel/GainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needGoodsGainText;
    private List<RewardGoodsUIController> m_rewardGoods;
    private bool m_isClick;
    private PlayerLevelUpUITask m_playerLevelUpUITask;
    private int m_gainNeedGoodsCount;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowReward;
    private static DelegateBridge __Hotfix_Co_ShowReward;
    private static DelegateBridge __Hotfix_Co_WaitClick;
    private static DelegateBridge __Hotfix_Co_SetAndWaitUIState;
    private static DelegateBridge __Hotfix_Co_ShowPlayerLevelUp;
    private static DelegateBridge __Hotfix_ClearRewardGoods;
    private static DelegateBridge __Hotfix_SetRaidTicketCount;
    private static DelegateBridge __Hotfix_SetNeedGoods;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnRaidAgainButtonClick;
    private static DelegateBridge __Hotfix_PlayerLevelUpUITask_OnClose;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnRaidAgain;
    private static DelegateBridge __Hotfix_remove_EventOnRaidAgain;

    [MethodImpl((MethodImplOptions) 32768)]
    private RaidLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReward(BattleReward battleReward, List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowReward(BattleReward battleReward, List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowPlayerLevelUp(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRewardGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRaidTicketCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNeedGoods(
      NeedGoods needGoods,
      BattleReward battleReward,
      List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRaidAgainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerLevelUpUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRaidAgain
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
