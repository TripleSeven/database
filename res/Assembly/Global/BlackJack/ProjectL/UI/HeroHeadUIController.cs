﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroHeadUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroHeadUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroHeadButton;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroHeadAnimation;
    [AutoBind("./SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectImage;
    [AutoBind("./CardImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_cardIamge;
    [AutoBind("./HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    public HeroArchiveUIController.HeroWrap m_heroWrap;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroWrap;
    private static DelegateBridge __Hotfix_RefreshDisplay;
    private static DelegateBridge __Hotfix_OnHeroHeadClick;
    private static DelegateBridge __Hotfix_Selected;
    private static DelegateBridge __Hotfix_add_OnHeroHeadClickEvent;
    private static DelegateBridge __Hotfix_remove_OnHeroHeadClickEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroWrap(HeroArchiveUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHeroHeadClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Selected(bool isSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroHeadUIController> OnHeroHeadClickEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
