﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogDetailUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public sealed class DialogDetailUITask : UITask
  {
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private const string ParamKeyLog = "ParamKeyLog";
    private List<DialogDetailUITask.LogData> m_logDataList;
    private DialogDetailUIController m_dialogDetailUIController;
    private readonly UITaskBase.LayerDesc[] m_layerDescArray;
    private readonly UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateDataCache;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_AutomationInitUIControllers;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_DialogDetailUIController_Close;
    private static DelegateBridge __Hotfix_InitDataFromIntentForPrepare;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogDetailUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      UIIntent fromIntent,
      List<DialogDetailUITask.LogData> logDataList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutomationInitUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogDetailUIController_Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromIntentForPrepare(UIIntentCustom intent)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class LogData
    {
      public DialogDetailUITask.LogData.LogDataType m_dataType;
      public string m_data;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public LogData(DialogDetailUITask.LogData.LogDataType dataType, string data)
      {
        // ISSUE: unable to decompile the method.
      }

      public enum LogDataType
      {
        Name,
        Desc,
        Option,
      }
    }
  }
}
