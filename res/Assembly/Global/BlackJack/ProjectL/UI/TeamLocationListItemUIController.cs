﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamLocationListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamLocationListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_energyIconImage;
    [AutoBind("./ChosenText/LevelName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextChosen;
    [AutoBind("./ChosenText/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyTextChosen;
    [AutoBind("./ChosenText/X", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyXTextChosen;
    [AutoBind("./UnchosenText/LevelName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextUnchosen;
    [AutoBind("./UnchosenText/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyTextUnchosen;
    [AutoBind("./UnchosenText/X", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyXTextUnchosen;
    [AutoBind("./LockState", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockButton;
    private bool m_isLocked;
    private int m_locationId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetName;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_SetLocationId;
    private static DelegateBridge __Hotfix_GetLocationId;
    private static DelegateBridge __Hotfix_SetEnergy;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_IsLocked;
    private static DelegateBridge __Hotfix_SetToggleValue;
    private static DelegateBridge __Hotfix_GetToggleValue;
    private static DelegateBridge __Hotfix_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_OnLockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnToggleValueChanged;
    private static DelegateBridge __Hotfix_remove_EventOnToggleValueChanged;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocationId(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLocationId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnergy(int energy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValue(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetToggleValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, TeamLocationListItemUIController> EventOnToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
