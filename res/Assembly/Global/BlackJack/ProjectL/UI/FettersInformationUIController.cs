﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersInformationUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersInformationUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleLife", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggleLife;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleVoice", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggleVoice;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleLife/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleLifeRedPoint;
    [AutoBind("./InfoPanel/ToggleGroup/ToggleVoice/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleVoiceRedPoint;
    [AutoBind("./InfoPanel/LifeIntroductionScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_lifeScrollRect;
    [AutoBind("./InfoPanel/VoiceScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_voiceScrollRect;
    [AutoBind("./InfoPanel/LifeIntroductionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lifeContent;
    [AutoBind("./InfoPanel/VoiceScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_voiceContent;
    [AutoBind("./InfoPanel/VoiceScrollView/CV/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceCVNameText;
    [AutoBind("./NameInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./NameInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroNameTextStateCtrl;
    [AutoBind("./NameInfo/EngNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroEngNameText;
    [AutoBind("./NameInfo/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroRankImage;
    private Hero m_hero;
    private ConfigDataHeroInformationInfo m_heroInformationInfo;
    private List<FettersInformationLifeItemUIController> biographyCtrlList;
    private List<FettersInformationVoiceItemUIController> voiceCtrlList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_UpdateViewInFettersInformation;
    private static DelegateBridge __Hotfix_UpdateHeroLifeList;
    private static DelegateBridge __Hotfix_UpdateHeroVoiceList;
    private static DelegateBridge __Hotfix_UpdateToggleNewTag;
    private static DelegateBridge __Hotfix_OnPeofermanceVoiceButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnToggleLifeValueChange;
    private static DelegateBridge __Hotfix_OnToggleVoiceValueChange;
    private static DelegateBridge __Hotfix_HaveNewInfomation;
    private static DelegateBridge __Hotfix_HaveNewBiography;
    private static DelegateBridge __Hotfix_HaveNewVoice;
    private static DelegateBridge __Hotfix_ResetScrollViewPosition;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnVoiceItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnVoiceItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersInformationUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersInformation(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroLifeList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroVoiceList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateToggleNewTag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeofermanceVoiceButtonClick(FettersInformationVoiceItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleLifeValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleVoiceValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HaveNewInfomation(ConfigDataHeroInformationInfo heroInfomationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HaveNewBiography(ConfigDataHeroInformationInfo heroInfomationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HaveNewVoice(ConfigDataHeroInformationInfo heroInfomationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnVoiceItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
