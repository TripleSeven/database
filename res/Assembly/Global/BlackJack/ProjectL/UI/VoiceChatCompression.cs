﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceChatCompression
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using NSpeex;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class VoiceChatCompression
  {
    private static VoiceChatCompression m_instance;
    private SpeexEncoder _speexEnc;
    private SpeexDecoder _speexDec;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Compress;
    private static DelegateBridge __Hotfix_Decompress;
    private static DelegateBridge __Hotfix_FloatToShortArray;
    private static DelegateBridge __Hotfix_ShortToFloatArray;
    private static DelegateBridge __Hotfix_SpeexDecompress;
    private static DelegateBridge __Hotfix_SpeexCompress;
    private static DelegateBridge __Hotfix_get_Instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private VoiceChatCompression()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] Compress(float[] sample, VoiceChatCompressionType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float[] Decompress(byte[] bytes, int sampleLength, VoiceChatCompressionType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FloatToShortArray(float[] input, short[] output)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShortToFloatArray(short[] input, float[] output, int length)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float[] SpeexDecompress(byte[] data, int dataLength, int sampleLength)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public byte[] SpeexCompress(float[] input)
    {
      // ISSUE: unable to decompile the method.
    }

    public static VoiceChatCompression Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
