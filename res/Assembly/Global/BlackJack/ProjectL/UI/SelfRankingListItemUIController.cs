﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfRankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SelfRankingListItemUIController : RankingListItemUIController
  {
    [AutoBind("./ComparePanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController ComparePanelUIStateCtrl;
    [AutoBind("./ComparePanel/LastRankLevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text LastRankLevelText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateCompareInfo;
    private static DelegateBridge __Hotfix_EnableComparePanel;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCompareInfo(int lastRankLevel, int curRankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableComparePanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
