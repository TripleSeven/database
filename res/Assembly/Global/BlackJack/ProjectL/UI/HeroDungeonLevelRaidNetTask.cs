﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDungeonLevelRaidNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroDungeonLevelRaidNetTask : UINetTask
  {
    private int m_heroDungeonLevelID;
    private int m_count;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnHeroDungeonLevelRaidAck;
    private static DelegateBridge __Hotfix_set_Reward;
    private static DelegateBridge __Hotfix_get_Reward;
    private static DelegateBridge __Hotfix_set_ExtraReward;
    private static DelegateBridge __Hotfix_get_ExtraReward;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevelRaidNetTask(int heroDungeonLevelID, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroDungeonLevelRaidAck(
      int result,
      BattleReward reward,
      List<Goods> extraReward)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleReward Reward
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<Goods> ExtraReward
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
