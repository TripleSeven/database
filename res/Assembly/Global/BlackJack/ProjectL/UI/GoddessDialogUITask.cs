﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GoddessDialogUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class GoddessDialogUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private GoddessDialogUIController m_goddessDialogUIController;
    private ConfigDataGoddessDialogInfo m_dialogInfo;
    private ConfigDataHeroInfo m_heroInfo;
    private int m_curJobIndex;
    private List<GoddessDialogUITask.JobChoiceValue> m_jobChoiceValues;
    private int m_startDialogId;
    private int m_questionStartDialogId;
    private int m_selectDialogId;
    private int m_finalDialogId;
    private int m_protagonistId;
    private ConfigDataCharImageInfo m_goddessCharImageInfo;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_InitGoddessDialogUIController;
    private static DelegateBridge __Hotfix_UninitGoddessDialogUIController;
    private static DelegateBridge __Hotfix_InitDialogId;
    private static DelegateBridge __Hotfix_InitJobChoiceValues;
    private static DelegateBridge __Hotfix_AppendChoiceValues;
    private static DelegateBridge __Hotfix_ShowJobs;
    private static DelegateBridge __Hotfix_StartDialog;
    private static DelegateBridge __Hotfix_ShowResultJobs;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnNext;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnClose;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnChoose;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnShowJobInfo;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnConfirmJob;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnRetry;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnSelectJob;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnPrevJob;
    private static DelegateBridge __Hotfix_GoddessDialogUIController_OnNextJob;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public GoddessDialogUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitGoddessDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitGoddessDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDialogId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitJobChoiceValues()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendChoiceValues(List<ChoiceValue> choiceValues)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowJobs(int idx, bool canSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDialog(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowResultJobs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnNext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnChoose(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnShowJobInfo(
      ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnConfirmJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnRetry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnSelectJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnPrevJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUIController_OnNextJob()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class JobChoiceValue
    {
      public ConfigDataProtagonistInfo m_protagonistInfo;
      public int m_value;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public JobChoiceValue()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
