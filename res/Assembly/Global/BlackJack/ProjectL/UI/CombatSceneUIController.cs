﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CombatSceneUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Misc;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class CombatSceneUIController : UIControllerBase
  {
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/DamageNumber0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumber0Prefab;
    [AutoBind("./Prefabs/DamageNumber1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumber1Prefab;
    [AutoBind("./Prefabs/DamageNumberWeak0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberWeak0Prefab;
    [AutoBind("./Prefabs/DamageNumberWeak1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberWeak1Prefab;
    [AutoBind("./Prefabs/DamageNumberStrong0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberStrong0Prefab;
    [AutoBind("./Prefabs/DamageNumberStrong1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberStrong1Prefab;
    [AutoBind("./Prefabs/DamageNumberCritical0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberCritical0Prefab;
    [AutoBind("./Prefabs/DamageNumberCritical1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberCritical1Prefab;
    private GameObjectPool<DamageNumber> m_damageNumber0Pool;
    private GameObjectPool<DamageNumber> m_damageNumber1Pool;
    private GameObjectPool<DamageNumber> m_damageNumberWeak0Pool;
    private GameObjectPool<DamageNumber> m_damageNumberWeak1Pool;
    private GameObjectPool<DamageNumber> m_damageNumberStrong0Pool;
    private GameObjectPool<DamageNumber> m_damageNumberStrong1Pool;
    private GameObjectPool<DamageNumber> m_damageNumberCritical0Pool;
    private GameObjectPool<DamageNumber> m_damageNumberCritical1Pool;
    private GameObject m_background0GameObject;
    private GameObject m_background1GameObject;
    private GameObject m_background2GameObject;
    private GameObject m_fadeGameObject;
    private bool m_isFadeIn;
    private float m_fadeCurTime;
    private float m_fadeDuration;
    private Material m_fadeMaterial;
    private Color m_fadeColor;
    private int m_myPlayerTeam;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_PreStopCombat;
    private static DelegateBridge __Hotfix_StopCombat;
    private static DelegateBridge __Hotfix_SkillFadeIn;
    private static DelegateBridge __Hotfix_SkillFadeOut;
    private static DelegateBridge __Hotfix_HideFade;
    private static DelegateBridge __Hotfix_SetFade;
    private static DelegateBridge __Hotfix_UpdateFade;
    private static DelegateBridge __Hotfix_SetBackgroundTexture;
    private static DelegateBridge __Hotfix_OnActorHit;
    private static DelegateBridge __Hotfix_GetActorGraphicPosition;

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat(
      ConfigDataTerrainInfo terrain0,
      ConfigDataTerrainInfo terrain1,
      bool splitScreen,
      int attackerTeam,
      int myPlayerTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkillFadeIn(float time, float alpha)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkillFadeOut(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideFade()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFade(float a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFade(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBackgroundTexture(GameObject go, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skill,
      int hpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool GetActorGraphicPosition(CombatActor a, out Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
