﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedScoreLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedScoreLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Level/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelNameText;
    [AutoBind("./Level/TitleText/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./EnergyValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Locked", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockedButton;
    [AutoBind("./Locked/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockedText;
    [AutoBind("./StartButton1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton1;
    [AutoBind("./StartButton2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton2;
    [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rewardGroupTransform;
    private ConfigDataScoreLevelInfo m_unchartedScoreLevelInfo;
    private ConfigDataChallengeLevelInfo m_unchartedChallengeLevelInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetUnchartedScoreLevelInfo;
    private static DelegateBridge __Hotfix_SetUnchartedChallengeLevelInfo;
    private static DelegateBridge __Hotfix_SetLevelInfo;
    private static DelegateBridge __Hotfix_GetUnchartedScoreLevelInfo;
    private static DelegateBridge __Hotfix_GetUnchartedChallengeLevelInfo;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_SetOpenDayLocked;
    private static DelegateBridge __Hotfix_SetCompleted;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnLockedButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStartButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnStartButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnchartedScoreLevelInfo(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnchartedChallengeLevelInfo(ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevelInfo(
      string name,
      int monsterLevel,
      int energy,
      List<Goods> rewardList,
      int rewardDisplayCount,
      bool showRewardGoodCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataScoreLevelInfo GetUnchartedScoreLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataChallengeLevelInfo GetUnchartedChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool locked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpenDayLocked(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockedButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<UnchartedScoreLevelListItemUIController> EventOnStartButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
