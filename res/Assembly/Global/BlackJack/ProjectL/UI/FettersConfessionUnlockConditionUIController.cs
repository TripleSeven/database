﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersConfessionUnlockConditionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersConfessionUnlockConditionUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillIconImage;
    [AutoBind("./NameGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillNameText;
    [AutoBind("./Value/First", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillCurLevelValue;
    [AutoBind("./Value/Last", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillMaxLevelValue;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitFettersConfessionUnlockCondition;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFettersConfessionUnlockCondition(
      int heroFetterId,
      int confessLevel,
      bool isFetterUnLock,
      int curLevel)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
