﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareActorInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattlePrepareActorInfoUIController : UIControllerBase
  {
    [AutoBind("./Hero", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGameObject;
    [AutoBind("./Hero/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Hero/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroArmyIcon;
    [AutoBind("./Hero/Job/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroJobText;
    [AutoBind("./Hero/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLevelText;
    [AutoBind("./Hero/Range/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroRangeText;
    [AutoBind("./Hero/Move/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMoveText;
    [AutoBind("./HeroPanelGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroPanelGroupCtrl;
    [AutoBind("./HeroPanelGroup/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIcon;
    [AutoBind("./Hero/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroHPText;
    [AutoBind("./Hero/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHPImage;
    [AutoBind("./Hero/DEX/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDEXText;
    [AutoBind("./Hero/Attack/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroAttackText;
    [AutoBind("./Hero/Defense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDefenseText;
    [AutoBind("./Hero/Magic/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicText;
    [AutoBind("./Hero/MagicDefense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicDefenseText;
    [AutoBind("./Hero/SkillInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroSkillInfoButton;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/TalentSkill", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillInfoGroupTalentSkillImage;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo2;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo3;
    [AutoBind("./Hero/ChangeSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillChangeButton;
    [AutoBind("./SelectSkill", AutoBindAttribute.InitState.Active, false)]
    private GameObject m_selectSkillPanelGo;
    [AutoBind("./SkillList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailObj;
    [AutoBind("./SkillList/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailListObj;
    [AutoBind("./SkillList/List/Content/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentTalentObj;
    [AutoBind("./SkillList/List/Content/Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillListContentTalentIcon;
    [AutoBind("./SkillList/List/Content/Talent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillListContentTalentNameText;
    [AutoBind("./SkillList/List/Content/Talent/DescTextScrollRect/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillListContentTalentDescText;
    [AutoBind("./SkillList/List/Content/LineImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillListContentLineImage;
    [AutoBind("./SkillList/List/Content/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentListObj;
    [AutoBind("./SkillList/List/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentObj;
    [AutoBind("./SkillList/List/NoSkills", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListNoSkillsObj;
    [AutoBind("./Prefabs/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDescPrefabObj;
    [AutoBind("./Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGo;
    [AutoBind("./Soldier/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierChangeButton;
    [AutoBind("./SelectSoldier/ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeSelectSoldierButton;
    [AutoBind("./SelectSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_selectSoldierPanelUIStateController;
    [AutoBind("./SelectSoldier/SoldierItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldiersContent;
    [AutoBind("./SelectSoldier/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectSoldierDetailPanel;
    [AutoBind("./Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicGo;
    [AutoBind("./Soldier/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierArmyTypeImg;
    [AutoBind("./Soldier/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./Soldier/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierInfoBtn;
    [AutoBind("./Soldier/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPText;
    [AutoBind("./Soldier/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierPropHPImage;
    [AutoBind("./Soldier/Prop/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFText;
    [AutoBind("./Soldier/Prop/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATText;
    [AutoBind("./Soldier/Prop/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFText;
    [AutoBind("./Soldier/Prop/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddText;
    [AutoBind("./Soldier/Prop/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddText;
    [AutoBind("./Soldier/Prop/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddText;
    [AutoBind("./Soldier/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelUIStateController;
    [AutoBind("./Soldier/SoldierDetailPanel/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailIconImg;
    [AutoBind("./Soldier/SoldierDetailPanel/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailGraphicGo;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailRangeText;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailMoveText;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailTypeBgImg;
    [AutoBind("./Soldier/SoldierDetailPanel/Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailTypeBgImg2;
    [AutoBind("./Soldier/SoldierDetailPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailTitleText;
    [AutoBind("./Soldier/SoldierDetailPanel/Desc/DescTextScroll/Mask/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailDescText;
    [AutoBind("./Soldier/SoldierDetailPanel/Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailWeakText;
    [AutoBind("./Soldier/SoldierDetailPanel/Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailStrongText;
    [AutoBind("./Soldier/SoldierDetailPanel/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailHPText;
    [AutoBind("./Soldier/SoldierDetailPanel/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailDFText;
    [AutoBind("./Soldier/SoldierDetailPanel/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailATText;
    [AutoBind("./Soldier/SoldierDetailPanel/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailMagicDFText;
    [AutoBind("./Soldier/SoldierDetailPanel/HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailHPAddText;
    [AutoBind("./Soldier/SoldierDetailPanel/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailDFAddText;
    [AutoBind("./Soldier/SoldierDetailPanel/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailATAddText;
    [AutoBind("./Soldier/SoldierDetailPanel/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailMagicDFAddText;
    private BattleHero m_hero;
    private List<TrainingTech> m_trainingTechs;
    private int m_team;
    private bool m_isNpc;
    private bool m_canChangeSoldier;
    private bool m_canChangeSkill;
    private int m_heroHp;
    private int m_soldierHp;
    private ConfigDataSkillInfo m_extraTalentSkillInfo;
    private bool m_isOpened;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private List<int> m_selectSkillIds;
    private BattleSoldierItemUIController m_lastClickSoldierItem;
    private int m_maxSkillCost;
    private UISpineGraphic m_soldierGraphic;
    private UISpineGraphic m_soldierDetailGraphic;
    private GameObjectPool<SkillDesc> m_skillDescPool;
    private HeroDetailSelectSkillUIController m_selectSkillPanelUICtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_IsOpened;
    private static DelegateBridge __Hotfix_SetActorInfo;
    private static DelegateBridge __Hotfix_GetHeroId;
    private static DelegateBridge __Hotfix_GetTeam;
    private static DelegateBridge __Hotfix_CanChangeSoldier;
    private static DelegateBridge __Hotfix_CanChangeSkill;
    private static DelegateBridge __Hotfix_SetSkills;
    private static DelegateBridge __Hotfix_SetSkillsAndTalentList;
    private static DelegateBridge __Hotfix_SetSoldier;
    private static DelegateBridge __Hotfix_SetSoldierDetailPanel;
    private static DelegateBridge __Hotfix_ShowSelectSkillPanel;
    private static DelegateBridge __Hotfix_ShowSelectSoldierPanel;
    private static DelegateBridge __Hotfix_CloseSelectSoldierPanel;
    private static DelegateBridge __Hotfix_OnSoldierDetailSelectButtonClick;
    private static DelegateBridge __Hotfix_OnSoldierItemClick;
    private static DelegateBridge __Hotfix_CalcPropAddValue;
    private static DelegateBridge __Hotfix_GetSoliderSelectButtonRectTransform;
    private static DelegateBridge __Hotfix_SelectSolider;
    private static DelegateBridge __Hotfix_SetPropLevelItemImg;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_OnChangeSkillButtonClick;
    private static DelegateBridge __Hotfix_SelectSkillPanelUICtrl_OnHeroSkillsSelect;
    private static DelegateBridge __Hotfix_OnChangeSoldierButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowSelectSkillPanel;
    private static DelegateBridge __Hotfix_remove_EventOnShowSelectSkillPanel;
    private static DelegateBridge __Hotfix_add_EventOnShowSelectSoldierPanel;
    private static DelegateBridge __Hotfix_remove_EventOnShowSelectSoldierPanel;
    private static DelegateBridge __Hotfix_add_EventOnChangeSkill;
    private static DelegateBridge __Hotfix_remove_EventOnChangeSkill;
    private static DelegateBridge __Hotfix_add_EventOnChangeSoldier;
    private static DelegateBridge __Hotfix_remove_EventOnChangeSoldier;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattlePrepareActorInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(bool left)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorInfo(
      BattleHero hero,
      List<TrainingTech> techs,
      int team,
      bool isNpc,
      bool canChangeSoldier,
      bool canChangeSkill,
      ConfigDataSkillInfo extraTalentSkillInfo = null,
      int heroHp = -1,
      int soldierHp = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanChangeSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanChangeSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillsAndTalentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectSkillPanel(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectSoldierPanel(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSelectSoldierPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierDetailSelectButtonClick(BattleSoldierItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick(BattleSoldierItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropAddValue(int v0, int v1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetSoliderSelectButtonRectTransform(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SelectSolider(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropLevelItemImg(Image img, char level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectSkillPanelUICtrl_OnHeroSkillsSelect(
      int heroId,
      List<int> skillIdList,
      bool isSkillChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSoldierButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleHero> EventOnShowSelectSkillPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero> EventOnShowSelectSoldierPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero, List<int>> EventOnChangeSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleHero, int> EventOnChangeSoldier
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
