﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionEventHandleNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class CollectionEventHandleNetTask : UINetTask
  {
    private ulong m_activityInstanceId;
    private int m_collectionWaypointId;
    private int m_collectionEventId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnCollectionEventHandleAck;
    private static DelegateBridge __Hotfix_set_Reward;
    private static DelegateBridge __Hotfix_get_Reward;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionEventHandleNetTask(
      ulong activityInstanceId,
      int collectionWaypointId,
      int collectionEventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCollectionEventHandleAck(int result, BattleReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleReward Reward
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
