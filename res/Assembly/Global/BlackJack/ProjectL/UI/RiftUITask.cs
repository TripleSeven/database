﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class RiftUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private RiftBackgroundUIController m_riftBackgroundUIController;
    private RiftChapterUIController m_riftChapterUIController;
    private RiftLevelUIController m_riftLevelUIController;
    private PlayerResourceUIController m_riftChapterPlayerResourceUIController;
    private PlayerResourceUIController m_riftLevelPlayerResourceUIController;
    private ConfigDataRiftChapterInfo m_chapterInfo;
    private bool m_needReturnToChapter;
    private bool m_needPlayRiftLevelOpenAnimation;
    private BattleLevelInfoUITask m_battleLevelInfoUITask;
    private RiftUITask.ViewState m_viewState;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_InitRiftBackgroundUIController;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddGold;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_InitRiftChapterUIController;
    private static DelegateBridge __Hotfix_UninitRiftChapterUIController;
    private static DelegateBridge __Hotfix_UpdateChapters;
    private static DelegateBridge __Hotfix_ShowChapterProgress;
    private static DelegateBridge __Hotfix_HideChapterProgress;
    private static DelegateBridge __Hotfix_SelectChapter;
    private static DelegateBridge __Hotfix_RiftChapterUIController_OnReturn;
    private static DelegateBridge __Hotfix_RiftChapterUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_RiftChapterUIController_OnReturnToWorld;
    private static DelegateBridge __Hotfix_RiftChapterUIController_OnSelectChapter;
    private static DelegateBridge __Hotfix_RiftChapterUIController_OnSwitchChapter;
    private static DelegateBridge __Hotfix_RiftChapterUIController_OnGoToScenario;
    private static DelegateBridge __Hotfix_InitRiftLevelUIController;
    private static DelegateBridge __Hotfix_UninitRiftLevelUIController;
    private static DelegateBridge __Hotfix_UpdateRiftLevels;
    private static DelegateBridge __Hotfix_UpdateStarReward;
    private static DelegateBridge __Hotfix_AutoGetStarReward;
    private static DelegateBridge __Hotfix_CheckOpenRiftChapterHard;
    private static DelegateBridge __Hotfix_StartBattleLevelInfoUITask;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnReturn;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnReturnToWorld;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnChangeHard;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnSelectRiftLevel;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnGetStarReward;
    private static DelegateBridge __Hotfix_RiftLevelUIController_OnGoToScenario;
    private static DelegateBridge __Hotfix_ChestUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleLevelInfoUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleLevelInfoUITask_OnRiftRaidComplete;
    private static DelegateBridge __Hotfix_add_EventOnGoToScenario;
    private static DelegateBridge __Hotfix_remove_EventOnGoToScenario;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftBackgroundUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitRiftChapterUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChapters()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowChapterProgress(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideChapterProgress(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectChapter(ConfigDataRiftChapterInfo chapterInfo, bool playOpenAnim)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RiftChapterUIController_OnSelectChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnSwitchChapter(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftChapterUIController_OnGoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitRiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRiftLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStarReward(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetStarReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenRiftChapterHard(ConfigDataRiftChapterInfo chapterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleLevelInfoUITask(
      ConfigDataRiftLevelInfo riftLevelInfo,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnChangeHard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnSelectRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnGetStarReward(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelUIController_OnGoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLevelInfoUITask_OnRiftRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum ViewState
    {
      None,
      Chapter,
      RiftLevel,
    }
  }
}
