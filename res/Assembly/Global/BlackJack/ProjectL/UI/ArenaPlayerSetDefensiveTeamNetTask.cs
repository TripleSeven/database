﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaPlayerSetDefensiveTeamNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class ArenaPlayerSetDefensiveTeamNetTask : UINetTask
  {
    private int m_arenaDefenderRuleId;
    private int m_battleId;
    private List<ArenaPlayerDefensiveHero> m_defensiveHeroes;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnArenaPlayerSetDefensiveTeamAck;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerSetDefensiveTeamNetTask(
      int arenaDefenderRuleId,
      int battleId,
      List<ArenaPlayerDefensiveHero> defensiveHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnArenaPlayerSetDefensiveTeamAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
