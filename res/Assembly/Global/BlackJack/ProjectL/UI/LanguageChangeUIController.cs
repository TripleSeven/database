﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LanguageChangeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class LanguageChangeUIController : UIControllerBase
  {
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/LanguageGroupScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleGroup m_ToggleGroup;
    [AutoBind("./Detail/LanguageGroupScrollView/Viewport/Content/LanguageToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_languageButtonObj;
    [AutoBind("./Detail/LanguageGroupScrollView/Viewport/Content/LanguageToggle/Image", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_languageButtonToggle;
    private Dictionary<ToggleEx, string> m_ToggleDict;
    private string m_sSelectLanguage;
    private bool m_isIgnoreToggleEvent;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnLanguageClick;
    private static DelegateBridge __Hotfix_ConfirmChange;
    private static DelegateBridge __Hotfix_SetLanguageList;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnChangeLanguage;
    private static DelegateBridge __Hotfix_remove_EventOnChangeLanguage;

    [MethodImpl((MethodImplOptions) 32768)]
    private LanguageChangeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLanguageClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ConfirmChange(DialogBoxResult ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLanguageList()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnChangeLanguage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
