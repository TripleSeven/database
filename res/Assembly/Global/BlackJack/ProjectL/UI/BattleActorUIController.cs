﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleActorUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleActorUIController : UIControllerBase
  {
    [AutoBind("./HP", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_hpImage;
    [AutoBind("./Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_armyIconImage;
    [AutoBind("./PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./Enforce", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enforceGameObject;
    [AutoBind("./Npc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_npcGameObject;
    [AutoBind("./Protect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_protectGameObject;
    [AutoBind("./KickOut", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_banGameObject;
    [AutoBind("./ScoreUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scoreBonusGameObject;
    [AutoBind("./PowerUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_powerUpGameObject;
    [AutoBind("./DropUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dropUpGameObject;
    private BattleActorBuffUIController[] m_buffControllers;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetArmyInfo;
    private static DelegateBridge __Hotfix_SetHpBarType;
    private static DelegateBridge __Hotfix_ShowTag;
    private static DelegateBridge __Hotfix_ShowProtect;
    private static DelegateBridge __Hotfix_ShowBan;
    private static DelegateBridge __Hotfix_ShowPlayerIndexTag;
    private static DelegateBridge __Hotfix_SetPlayerIndex;
    private static DelegateBridge __Hotfix_SetHp;
    private static DelegateBridge __Hotfix_SetBuff;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArmyInfo(ConfigDataArmyInfo armyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHpBarType(int barType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTag(StageActorTagType tagType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowProtect(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBan(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndexTag(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHp(int hp, int hpValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBuff(List<ClientActorBuff> buffs)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
