﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaReportReplayUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaReportReplayUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Panel/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Panel/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_replayButton;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_leftResultState;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_righttResultState;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerNameText;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerLV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerLVText;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_leftPlayerIconImage;
    [AutoBind("./Panel/RightPlayer/Player/PlayerName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerNameText;
    [AutoBind("./Panel/RightPlayer/Player/PlayerLV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerLVText;
    [AutoBind("./Panel/RightPlayer/Player/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rightPlayerIconImage;
    [AutoBind("./Panel/MultiMatchResultPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_multiMatchPanel;
    [AutoBind("./Panel/MultiMatchResultPanel/LeftMatchGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_leftMultiMatchResultState;
    [AutoBind("./Panel/MultiMatchResultPanel/RightMatchGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rightMultiMatchResultState;
    [AutoBind("./Panel/IdolsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jettonPanel;
    [AutoBind("./Panel/IdolsPanel/LeftIdols/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftJettonCountText;
    [AutoBind("./Panel/IdolsPanel/RightIdols/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightJettonCountText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaReportReplayUITask m_peakArenaReportReplayUITask;
    private PeakArenaPlayOffMatchupInfo m_matchInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnReplayClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(PeakArenaPlayOffMatchupInfo matchInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnReplayClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
