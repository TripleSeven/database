﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScreenRecorderUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ScreenRecorderUI : PrefabControllerBase, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
  {
    private Toggle m_screenRecordToggle;
    private GameObject m_recordIndicator;
    private Text m_recordTimerText;
    private float m_recordTime;
    private bool m_isPreviewing;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_OnScreenRecordToggleValueChanged;
    private static DelegateBridge __Hotfix_ToggleScreenRecord;
    private static DelegateBridge __Hotfix_FormatRecordTimeString;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnBeginDrag;
    private static DelegateBridge __Hotfix_OnDrag;
    private static DelegateBridge __Hotfix_OnEndDrag;
    private static DelegateBridge __Hotfix_SetDraggedPosition;
    private static DelegateBridge __Hotfix_StartRecord;
    private static DelegateBridge __Hotfix_StopRecord;
    private static DelegateBridge __Hotfix_UpdateScreenRecord;

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenRecordToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ToggleScreenRecord(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string FormatRecordTimeString(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDraggedPosition(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateScreenRecord()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
