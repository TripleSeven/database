﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutPlanUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaKnockoutPlanUIController : UIControllerBase
  {
    [AutoBind("./PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerGroup;
    [AutoBind("./ButtonGroup/ReplayButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_replayButtonGroup;
    [AutoBind("./ButtonGroup/WatchButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_watchButtonGroup;
    [AutoBind("./16Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_16PlayerLineGroup;
    [AutoBind("./8Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_8PlayerLineGroup;
    [AutoBind("./4Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_4PlayerLineGroup;
    [AutoBind("./2Players", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_2PlayerLineGroup;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaUITask m_peakArenaUITask;
    private List<CommonUIStateController> m_16LineState;
    private List<CommonUIStateController> m_8LineState;
    private List<CommonUIStateController> m_4LineState;
    private List<CommonUIStateController> m_2LineState;
    private List<PeakArenaKnockoutPlayerInfoUIController> m_playerInfoList;
    private List<Button> replayButtonList;
    private List<Button> watchButtonList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundOneMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundTwoMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundThreeMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_roundFourMatchInfoList;
    private List<PeakArenaPlayOffMatchupInfo> m_matchInfoList;
    private int m_groupIndex;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Bind;
    private static DelegateBridge __Hotfix_UpdateData;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_DebugLog;
    private static DelegateBridge __Hotfix_UpdateViewMatchupTree;
    private static DelegateBridge __Hotfix_UpdateViewLine;
    private static DelegateBridge __Hotfix_SetMatchResultLineState;
    private static DelegateBridge __Hotfix_UpdateViewPreviousLineState;
    private static DelegateBridge __Hotfix_CalculateLineIndex;
    private static DelegateBridge __Hotfix_GetLineList;
    private static DelegateBridge __Hotfix_UpdateViewButton;
    private static DelegateBridge __Hotfix_SetGroupId;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_OnReplayClick;
    private static DelegateBridge __Hotfix_OnWatchClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaKnockoutPlanUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Bind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DebugLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewMatchupTree(List<PeakArenaPlayOffMatchupInfo> matchupInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewLine(PeakArenaPlayOffMatchupInfo matchupInfo, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMatchResultLineState(
      CommonUIStateController lineState,
      PeakArenaPlayOffMatchupInfo matchupInfo,
      bool isLeftPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewPreviousLineState(int prevMatchId, string userId, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalculateLineIndex(int matchRound, int matchIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<CommonUIStateController> GetLineList(int matchRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewButton(PeakArenaPlayOffMatchupInfo matchupInfo, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupId(int groupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplayClick(int replayIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchClick(int watchIndex)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
