﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginAnnouncement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class LoginAnnouncement
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Title;
    private static DelegateBridge __Hotfix_set_Title;
    private static DelegateBridge __Hotfix_get_Content;
    private static DelegateBridge __Hotfix_set_Content;
    private static DelegateBridge __Hotfix_get_CurrentType;
    private static DelegateBridge __Hotfix_set_CurrentType;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginAnnouncement(LoginAnnouncement.AnnounceType type, string title, string content)
    {
      // ISSUE: unable to decompile the method.
    }

    public string Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Content
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public LoginAnnouncement.AnnounceType CurrentType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum AnnounceType
    {
      Notice = 1,
      Activity = 2,
      None = 3,
    }
  }
}
