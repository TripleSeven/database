﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailJobUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailJobUIController : UIControllerBase
  {
    private Dictionary<PropertyModifyType, int> m_contributePropertyValues;
    private EquipMasterEquipType m_curEquipType;
    public EquipMasterRefineOpenPanelUIController m_equipMasterRefineOpenPanelUIController;
    private readonly Dictionary<int, EquipMasterSlotPropertyListItemUIController> m_propertyUICtrlDic;
    private readonly Dictionary<EquipMasterEquipType, EquipMasterEquipToggleUIController> m_equipToggleCtrlDic;
    private readonly List<GameObject> m_sportPropertyItemGameObjectList;
    private bool m_isEquipSlotPropetyListShowing;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterUIStateCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/MasterPanel/OpenEquipMasterButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button OpenEquipMasterButton;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueState", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterPropertyListUIStateCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterPropertyListGameObject;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterPropertyListSportsGameObject;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/Arrow_Lt", AutoBindAttribute.InitState.NotInit, false)]
    public Button EquipMasterProperytListLArrowButton;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/Arrow_Rt", AutoBindAttribute.InitState.NotInit, false)]
    public Button EquipMasterProperytListRArrowButton;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterHPValueText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterATValueText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterMagicValueText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterDFValueText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/MDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterMDFValueText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterDEXValueText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/TitleBG/TipsText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterSportPropertyDescText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterSportProperty1UIStateCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterSportProperty1UIStateCtr2;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/Proprety3", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterSportProperty1UIStateCtr3;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/Proprety4", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterSportProperty1UIStateCtr4;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ValueContent_Sports/Proprety5", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterSportProperty1UIStateCtr5;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/Toggle/Button_Weapon", AutoBindAttribute.InitState.NotInit, false)]
    public ToggleEx EquipMasterWeaponToggle;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/Toggle/Button_Armor", AutoBindAttribute.InitState.NotInit, false)]
    public ToggleEx EquipMasterArmorToggle;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/Toggle/Button_Jewelry", AutoBindAttribute.InitState.NotInit, false)]
    public ToggleEx EquipMasterJewelryToggle;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/Toggle/Button_Headwear", AutoBindAttribute.InitState.NotInit, false)]
    public ToggleEx EquipMasterHeadwearToggle;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/Toggle/Button_Special", AutoBindAttribute.InitState.NotInit, false)]
    public ToggleEx EquipMasterSpecialToggle;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipPropertyInfoClickOpenUIStateCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipPropertyInfoTitleText;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/BGButtonImage", AutoBindAttribute.InitState.NotInit, false)]
    public Button EquipPropertyInfoClickOpenBGButton;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/PropretyGroup", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterSlotPropertyGroupUIStateCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/PropretyGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSlotProperty1Go;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/PropretyGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSlotProperty2Go;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/PropretyGroup/Proprety3", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSlotProperty3Go;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/PropretyGroup/Proprety4", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSlotProperty4Go;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/PropretyGroup/Proprety5", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSlotProperty5Go;
    [AutoBind("./InfoPanel/JobUpgrade/EquipMaster/ClickOpen/Detail/Icon", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController EquipMasterClickOpenEquipTypeIconUIStateCtrl;
    [AutoBind("./RefineOpenPanel", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject RefineOpenPanelGameObject;
    [AutoBind("./EnchantSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquieMasterEnchantSuccessEffectPanelGo;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./InfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelStateCtrl;
    [AutoBind("./InfoPanel/JobDetail/JobNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobNameText;
    [AutoBind("./InfoPanel/JobDetail/JobDescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobDescText;
    [AutoBind("./InfoPanel/JobDetail/ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobArmyImage;
    [AutoBind("./InfoPanel/JobUpgrade", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobUpgradeGo;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/CurLv/LvImgs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobLv;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/CurLv/LvImgsBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobLvBg;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/NextLv", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextJobLvObj;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/NextLv/LvImgs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextJobLv;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/NextLv/LvImgsBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextJobLvBg;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/Master", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMasterImg;
    [AutoBind("./InfoPanel/JobUpgrade/JobLV/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvArrowObj;
    [AutoBind("./InfoPanel/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpGameObject;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Skill", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLearnedSkill;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Skill/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLearnedSkillIconImage;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Skill/SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobLearnedSkillNameText;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLearnedSoldier;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Soldier/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLearnedSoldierIconImage;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Soldier/SoldierNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobLearnedSoldierNameText;
    [AutoBind("./InfoPanel/JobUpgrade/Learned/Content/Master", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLearnedMaster;
    [AutoBind("./InfoPanel/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobDescContent;
    [AutoBind("./JobEffectGroup/MasterRewardPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterRewardPanel;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/JobIconGroup/JobIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_masterRewardPanelJobIcon;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/JobIconGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_masterRewardPanelNameText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propHPAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propATAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propDFAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propMagicAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propMagicDFAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propDEXAddValueText;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propHPAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propATAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propDFAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propMagicAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propMagicDFAddObj;
    [AutoBind("./JobEffectGroup/MasterRewardPanel/Property/DEX", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propDEXAddObj;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj1;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj2;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/JobMaterial3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMaterialObj3;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/UpgradeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobLvUpgradeButton;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/LvLimit", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_jobMaterialLvLimitCtrl;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/LvLimit/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobMaterialLvLimitText;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/UpgradeButton/ArtEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeBtnArtEffect;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeFrameEffect;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeFlyEffect;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect1/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon1;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect2/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon2;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect3/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon3;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/FlyEffect/FlyEffect4/ItemIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobLvUpgradeFlyEffectIcon4;
    [AutoBind("./InfoPanel/JobUpgrade/JobMaterial/EquipmentRoundUpArtEffect/StarUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvUpgradeStarUpEffect;
    [AutoBind("./JobEffectGroup/MasterRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobMasterRewardGroupGo;
    [AutoBind("./JobEffectGroup/MasterRewardGroup/ReturnBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobMasterRewardGroupGoReturnButton;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpgradeGroupGo;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/ContinueBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobUpgradeGroupGoContinueBGButton;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Hp/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelHpText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Hp/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelHpAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/AT/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelATText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/AT/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelATAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Magic/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Magic/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/DF/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDFText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/DF/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDFAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/MagicDF/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicDFText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/MagicDF/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelMagicDFAddText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Dex/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDexText;
    [AutoBind("./JobEffectGroup/JobUpgradeGroup/JobUpgradeSuccessInfoPanel/Detail/Dex/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobUpgradeSuccessInfoPanelDexAddText;
    [AutoBind("./LearnedPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanel;
    [AutoBind("./LearnedPanel/CloseImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_learnedPanelCloseBtn;
    [AutoBind("./LearnedPanel/Skill/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_learnedPanelSkillCloseBtn;
    [AutoBind("./LearnedPanel/Skill", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSkillPanel;
    [AutoBind("./LearnedPanel/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSoldierPanel;
    [AutoBind("./LearnedPanel/Skill/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_learnedPanelSkillIcon;
    [AutoBind("./LearnedPanel/Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSoldierGraphicObj;
    [AutoBind("./LearnedPanel/Skill/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_learnedPanelSkillName;
    [AutoBind("./LearnedPanel/Soldier/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_learnedPanelSoldierName;
    private Hero m_hero;
    private List<GameObject> m_jobMaterialObjList;
    private UISpineGraphic m_learndSoldierGraphic;
    private ConfigDataSkillInfo m_learnSkillInfo;
    private ConfigDataSoldierInfo m_learnSoliderInfo;
    private List<Goods> m_curjobNeedMaterials;
    private bool m_isCloseJobUpgradePanel;
    private bool m_isCloseJobMasterRewardPanel;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted_EquipMaster;
    private static DelegateBridge __Hotfix_CreateEquipMasterRefineOpenPanelUIController;
    private static DelegateBridge __Hotfix_RegisterEquipMasterUIEvent;
    private static DelegateBridge __Hotfix_OnEquipMasterProperytListLArrowButtonClick;
    private static DelegateBridge __Hotfix_OnEquipMasterProperytListRArrowButtonClick;
    private static DelegateBridge __Hotfix_OnOpenEquipMasterComplete;
    private static DelegateBridge __Hotfix_OnOpenEquipMasterButtonClick;
    private static DelegateBridge __Hotfix_OnEquipMasterToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRefineButtonClick_PropertyUICtrl;
    private static DelegateBridge __Hotfix_OnGMSetPropertyButtonClick_PropertyUICtrl;
    private static DelegateBridge __Hotfix_OnRefineButtonClick;
    private static DelegateBridge __Hotfix_ShowNormalPropertyList;
    private static DelegateBridge __Hotfix_ShowSportSlotPropertyList;
    private static DelegateBridge __Hotfix_UpdateEquipMaster;
    private static DelegateBridge __Hotfix_UpdateNormalPropertyList;
    private static DelegateBridge __Hotfix_UpdateSportSlotPropertyItem;
    private static DelegateBridge __Hotfix_UpdateSportSlotPropertyList;
    private static DelegateBridge __Hotfix_UpdateEquipPropertyInfoPanel;
    private static DelegateBridge __Hotfix_ShowEnchantSuccessEffect;
    private static DelegateBridge __Hotfix_GetPropertyContributeValue;
    private static DelegateBridge __Hotfix_CalcPropertiesContribute;
    private static DelegateBridge __Hotfix_OpenEquipPropertyInfo;
    private static DelegateBridge __Hotfix_CloseEquipPropertyInfo;
    private static DelegateBridge __Hotfix_add_EventOnRefineButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRefineButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnOpenEquipMasterButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnOpenEquipMasterButtonClick;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInJobState;
    private static DelegateBridge __Hotfix_ShowJobDetailPanel;
    private static DelegateBridge __Hotfix_ShowJobUpgradePanel;
    private static DelegateBridge __Hotfix_SetUpperLimitBreak;
    private static DelegateBridge __Hotfix_SetPropertyLearned;
    private static DelegateBridge __Hotfix_OnJobLearnedSkillClick;
    private static DelegateBridge __Hotfix_OnJobLearnedSoldierClick;
    private static DelegateBridge __Hotfix_OnJobLearnedMasterClick;
    private static DelegateBridge __Hotfix_SetJobLvMasterInfo;
    private static DelegateBridge __Hotfix_SetMasterRewardProperty;
    private static DelegateBridge __Hotfix_ShowJobMaterialPanel;
    private static DelegateBridge __Hotfix_OnJobMaterialClick;
    private static DelegateBridge __Hotfix_OnJobLvUpgradeButtonClick;
    private static DelegateBridge __Hotfix_OnJobLvUpgradeSucceed;
    private static DelegateBridge __Hotfix_JobLVUpgradeEffect;
    private static DelegateBridge __Hotfix_SetJobUpgradeInfo;
    private static DelegateBridge __Hotfix_ShowLearnedSkillAndSoldier;
    private static DelegateBridge __Hotfix_CloseLearnedSkillPanel;
    private static DelegateBridge __Hotfix_OnJobUpgradeGroupGoContinueBGButtonClick;
    private static DelegateBridge __Hotfix_OnJobMasterRewardGroupGoReturnButtonClick;
    private static DelegateBridge __Hotfix_CloseLearnedPanel;
    private static DelegateBridge __Hotfix_IsJobLvUpgradeFinished;
    private static DelegateBridge __Hotfix_StopAndCloseUIEffect;
    private static DelegateBridge __Hotfix_SetCommonUIState;
    private static DelegateBridge __Hotfix_add_EventOnJobLvUpgrade;
    private static DelegateBridge __Hotfix_remove_EventOnJobLvUpgrade;
    private static DelegateBridge __Hotfix_add_EventOnShowGetPath;
    private static DelegateBridge __Hotfix_remove_EventOnShowGetPath;
    private static DelegateBridge __Hotfix_add_EventOnUpdateView;
    private static DelegateBridge __Hotfix_remove_EventOnUpdateView;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailJobUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBindFiledsCompleted_EquipMaster()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEquipMasterRefineOpenPanelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterEquipMasterUIEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEquipMasterProperytListLArrowButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEquipMasterProperytListRArrowButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenEquipMasterComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenEquipMasterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipMasterToggleValueChanged(EquipMasterEquipType equipMasterEquipType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineButtonClick_PropertyUICtrl(int index, HeroJobSlotRefineryProperty property)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGMSetPropertyButtonClick_PropertyUICtrl(int index, int newValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineButtonClick(
      EquipMasterEquipType equipType,
      int propertyIndex,
      HeroJobSlotRefineryProperty property)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNormalPropertyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSportSlotPropertyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEquipMaster()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNormalPropertyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSportSlotPropertyItem(HeroJobSlotRefineryProperty property, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSportSlotPropertyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEquipPropertyInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnchantSuccessEffect(EquipMasterEquipType equipType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPropertyContributeValue(PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CalcPropertiesContribute()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenEquipPropertyInfo(EquipMasterEquipType equipType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipPropertyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnRefineButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnOpenEquipMasterButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInJobState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowJobDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowJobUpgradePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUpperLimitBreak()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropertyLearned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLearnedSkillClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLearnedSoldierClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLearnedMasterClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetJobLvMasterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMasterRewardProperty(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowJobMaterialPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialClick(JobMaterialEquipedUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLvUpgradeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobLvUpgradeSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator JobLVUpgradeEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetJobUpgradeInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowLearnedSkillAndSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseLearnedSkillPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobUpgradeGroupGoContinueBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMasterRewardGroupGoReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseLearnedPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobLvUpgradeFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAndCloseUIEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int, Action> EventOnJobLvUpgrade
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnShowGetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnUpdateView
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
