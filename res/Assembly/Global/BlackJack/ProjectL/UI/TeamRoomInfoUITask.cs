﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInfoUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomInfoUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TeamRoomInfoUIController m_teamRoomInfoUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private int m_nowSeconds;
    private List<TeamRoomPlayer> m_teamRoomPlayers;
    private bool m_isLeader;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private bool m_roomPlayerChangeNtfsRegistered;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_RegisterRoomPlayerChangeNtfs;
    private static DelegateBridge __Hotfix_UnregisterRoomPlayerChangeNtfs;
    private static DelegateBridge __Hotfix_InitTeamRoomInfoUIController;
    private static DelegateBridge __Hotfix_UninitTeamRoomInfoUIController;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateQuitCountdown;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_StartTeamRoomInviteUITask;
    private static DelegateBridge __Hotfix_TeamRoomInviteUITask_OnPrepareEnd;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnLeave;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnStart;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnShowChangePlayerPosition;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnChangePlayerPositionComplete;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnShowChat;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnChangeAuthority;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnShowInvite;
    private static DelegateBridge __Hotfix_TeamRoomInfoUIController_OnShowPlayerInfo;
    private static DelegateBridge __Hotfix_ShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_OnClose;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_OnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomPlayerJoinNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomPlayerQuitNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomPlayerPositionChangeNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomSelfKickOutNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomAuthorityChangeNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomInvitationRefusedNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnChatMessageNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInfoUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterRoomPlayerChangeNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnregisterRoomPlayerChangeNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTeamRoomInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTeamRoomInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateQuitCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInviteUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUITask_OnPrepareEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnLeave()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowChangePlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnChangePlayerPositionComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnChangeAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowInvite()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUIController_OnShowPlayerInfo(int index, RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPlayerSimpleInfo(RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerJoinNtf(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerQuitNtf(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerPositionChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomSelfKickOutNtf(int reason)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomAuthorityChangeNtf(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInvitationRefusedNtf(int result, string inviteeName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatMessageNtf(ChatMessage msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
