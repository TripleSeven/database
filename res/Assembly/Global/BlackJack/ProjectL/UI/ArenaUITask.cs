﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class ArenaUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ArenaUIController m_arenaUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private ArenaDefendUITask m_arenaDefendUITask;
    private List<ConfigDataRealTimePVPDanInfo> m_realTimePVPDans;
    private List<ArenaOpponent> m_arenaOpponents;
    private List<ArenaBattleReport> m_offlineArenaBattleReport;
    private List<RealTimePVPBattleReport> m_onlineArenaBattleReport;
    private List<RealTimePVPBattleReport> m_onlineArenaPromotionBattleReport;
    private OfflineArenaPanelType m_offlinePanelType;
    private OnlineArenaPanelType m_onlinePanelType;
    private ArenaUIType m_arenaUIType;
    private int m_curArenaOpponentIndex;
    private ArenaBattleReport m_curArenaBattleReport;
    private bool m_canFlushOfflineTopRankPlayers;
    private bool m_canFlushOnlineGlobalTopRankPlayers;
    private bool m_canFlushOnlineLocalTopRankPlayers;
    private bool m_isSwitchingOfflineOnline;
    private int m_nowSeconds;
    private DateTime m_matchingUIBeginTime;
    private DateTime m_matchingReqSendTime;
    private RealTimePVPMode m_matchingBattleMode;
    private DateTime m_nextCheckFlushArenaOpponentsTime;
    private bool m_isNeedCheckOnline;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectPlayerHeadAssets;
    private static DelegateBridge __Hotfix_CollectHeroModelAssets;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_InitArenaUIController;
    private static DelegateBridge __Hotfix_UninitArenaUIController;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdatePlayerInfo;
    private static DelegateBridge __Hotfix_UpdateBattlePower;
    private static DelegateBridge __Hotfix_CompareHeroBattlePower;
    private static DelegateBridge __Hotfix_StartArenaBattleReportBasicDataGetNetTask;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_ArenaUIController_OnDefend;
    private static DelegateBridge __Hotfix_ArenaDefendUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_ArenaUIController_OnReturn;
    private static DelegateBridge __Hotfix_ArenaUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_ArenaUIController_OnShowBuyArenaTicket;
    private static DelegateBridge __Hotfix_ArenaUIController_OnBuyArenaTicket;
    private static DelegateBridge __Hotfix_ArenaUIController_OnSwitchOnlineOffline;
    private static DelegateBridge __Hotfix_ChestUITask_OnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_CollectOfflineArenaAssets;
    private static DelegateBridge __Hotfix_UpdateOfflineArena;
    private static DelegateBridge __Hotfix_UpdateOfflineArenaOpponentRefreshTime;
    private static DelegateBridge __Hotfix_CompareArenaBattleReportsByCreateTime;
    private static DelegateBridge __Hotfix_CompareRealTimePVPBattleReportsByCreateTime;
    private static DelegateBridge __Hotfix_IsBattleReportAttackerGiveup;
    private static DelegateBridge __Hotfix_FlushOfflineTopRankPlayers;
    private static DelegateBridge __Hotfix_AutoGetOfflineVictoryPointReward;
    private static DelegateBridge __Hotfix_ArenaUIController_OnShowOfflinePanel;
    private static DelegateBridge __Hotfix_ArenaUIController_OnGainOfflineVictoryPointsReward;
    private static DelegateBridge __Hotfix_ArenaUIController_OnShowOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_ArenaUIController_OnAttackOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_ArenaUIController_OnShowRevengeOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_ArenaUIController_OnRevengeOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_ArenaUIController_OnOfflineBattleReportReplay;
    private static DelegateBridge __Hotfix_StartArenaBattleReplay;
    private static DelegateBridge __Hotfix_PlayerContext_OnArenaFlushOpponentsNtf;
    private static DelegateBridge __Hotfix_CollectOnlineArenaAssets;
    private static DelegateBridge __Hotfix_UpdateOnlineArena;
    private static DelegateBridge __Hotfix_UpdateOnlineMatchingTime;
    private static DelegateBridge __Hotfix_UpdateLadderMode;
    private static DelegateBridge __Hotfix_IsBattleReportPlayerGiveup;
    private static DelegateBridge __Hotfix_FlushOnlineTopRankPlayers;
    private static DelegateBridge __Hotfix_StartRealTimePVPGetInfoNetTask;
    private static DelegateBridge __Hotfix_StartRealTimePVPGetTopPlayersNetTask;
    private static DelegateBridge __Hotfix_AutoGetOnlineWeekWinReward;
    private static DelegateBridge __Hotfix_StartRealTimePVPWaitingForOpponentNetTask;
    private static DelegateBridge __Hotfix_StartMatching;
    private static DelegateBridge __Hotfix_ArenaUIController_OnShowOnlinePanel;
    private static DelegateBridge __Hotfix_ArenaUIController_OnGainOnlineWeekWinReward;
    private static DelegateBridge __Hotfix_ArenaUIController_OnOnlineBattleReportReplay;
    private static DelegateBridge __Hotfix_ArenaUIController_OnLadderChallenge;
    private static DelegateBridge __Hotfix_ArenaUIController_OnCasualChallenge;
    private static DelegateBridge __Hotfix_ArenaUIController_OnMatchingCancel;
    private static DelegateBridge __Hotfix_PlayerContext_OnRealTimePVPMatchupNtf;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPlayerHeadAssets(int playerHeadIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroModelAssets(List<BattleHero> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroBattlePower(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleReportBasicDataGetNetTask(ArenaUIType arenaUIType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnDefend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnSwitchOnlineOffline()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectOfflineArenaAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOfflineArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOfflineArenaOpponentRefreshTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareArenaBattleReportsByCreateTime(
      ArenaBattleReport r0,
      ArenaBattleReport r1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareRealTimePVPBattleReportsByCreateTime(
      RealTimePVPBattleReport r0,
      RealTimePVPBattleReport r1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsBattleReportAttackerGiveup(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushOfflineTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetOfflineVictoryPointReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowOfflinePanel(OfflineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnGainOfflineVictoryPointsReward(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowOfflineArenaOpponent(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnAttackOfflineArenaOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowRevengeOfflineArenaOpponent(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnRevengeOfflineArenaOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnOfflineBattleReportReplay(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleReplay(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnArenaFlushOpponentsNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectOnlineArenaAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOnlineArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOnlineMatchingTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLadderMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsBattleReportPlayerGiveup(
      RealTimePVPBattleReport battleReport,
      out int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushOnlineTopRankPlayers(bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPGetInfoNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPGetTopPlayersNetTask(bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoGetOnlineWeekWinReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPWaitingForOpponentNetTask(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMatching(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnShowOnlinePanel(OnlineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnGainOnlineWeekWinReward(int bonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnOnlineBattleReportReplay(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnLadderChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnCasualChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUIController_OnMatchingCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnRealTimePVPMatchupNtf(int result, RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
