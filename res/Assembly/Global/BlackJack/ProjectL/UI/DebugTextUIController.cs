﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DebugTextUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DebugTextUIController : UIControllerBase
  {
    [AutoBind("./SystemInfo/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_BgButton;
    [AutoBind("./SystemInfo/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scrollRect;
    [AutoBind("./SystemInfo/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_content;
    [AutoBind("./SystemInfo/ScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_textPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<Text> m_textList;
    private static int MaxTextCount;
    private static List<string> stringList;
    private static DebugTextUIController instance;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_AddText;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_Delay;
    private static DelegateBridge __Hotfix_OnBGClick;

    [MethodImpl((MethodImplOptions) 32768)]
    static DebugTextUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DebugTextUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddText(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Delay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
