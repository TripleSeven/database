﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BigExpressionController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [ControllerDescription("UI/Battle_ABS/Prefab/ExpressionGroupPrefab.prefab", "ExpressionGroupPrefab")]
  public class BigExpressionController : UIControllerBase
  {
    private float scrollRectLastPos;
    private int m_pageCapacity;
    private List<BigExpressionItem> bigExpressionItemList;
    private List<GameObject> m_pageList;
    private List<CommonUIStateController> m_pagePointStateControllerList;
    [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject Content;
    [AutoBind("./Scroll View/Viewport/Expression", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject Expression;
    [AutoBind("./Scroll View/Viewport/Page", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject Page;
    [AutoBind("./PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PagePointNode;
    [AutoBind("./PagePoint/PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PagePoint;
    [AutoBind("./Scroll View", AutoBindAttribute.InitState.NotInit, false)]
    public ScrollRect ScrollView;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_GetCombatExpressionInfo;
    private static DelegateBridge __Hotfix_OnExpressionClick;
    private static DelegateBridge __Hotfix_OnBGMaskClick;
    private static DelegateBridge __Hotfix_OnEndDrag;
    private static DelegateBridge __Hotfix_OnValueChange;
    private static DelegateBridge __Hotfix_SetPagePointActivity;
    private static DelegateBridge __Hotfix_add_EventOnExpressionClick;
    private static DelegateBridge __Hotfix_remove_EventOnExpressionClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public BigExpressionController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataBigExpressionInfo> GetCombatExpressionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGMaskClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndDrag(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnValueChange(Vector2 rectEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPagePointActivity(int page)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnExpressionClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
