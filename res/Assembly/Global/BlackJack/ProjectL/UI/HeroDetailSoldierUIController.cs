﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailSoldierUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailSoldierUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./SoldierInfo/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImage;
    [AutoBind("./SoldierInfo/QualityBGImage/QualityIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierQualityIcon;
    [AutoBind("./SoldierInfo/SoldierNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./SoldierInfo/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphic;
    [AutoBind("./SoldierInfo/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierFactionRangeText;
    [AutoBind("./SoldierInfo/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierFactionMoveText;
    [AutoBind("./SoldierInfo/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPValueText;
    [AutoBind("./SoldierInfo/HP/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPAddPercentText;
    [AutoBind("./SoldierInfo/HP/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPAddValueText;
    [AutoBind("./SoldierInfo/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATValueText;
    [AutoBind("./SoldierInfo/AT/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddPercentText;
    [AutoBind("./SoldierInfo/AT/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddValueText;
    [AutoBind("./SoldierInfo/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFValueText;
    [AutoBind("./SoldierInfo/DF/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddPercentText;
    [AutoBind("./SoldierInfo/DF/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddValueText;
    [AutoBind("./SoldierInfo/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFValueText;
    [AutoBind("./SoldierInfo/MagicDF/AddPercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddPercentText;
    [AutoBind("./SoldierInfo/MagicDF/AddPercentText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddValueText;
    [AutoBind("./SoldierInfo/Strong/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierStrongText;
    [AutoBind("./SoldierInfo/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierWeakText;
    [AutoBind("./SoldierInfo/DescTextScroll/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./SoldierSelet/SoldierItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_soldierSelectContentScrollRect;
    [AutoBind("./SoldierSelet/SoldierItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierSelectContentObj;
    [AutoBind("./SoldierSelet/SoldierGetCondition", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGetCondition;
    [AutoBind("./SoldierSelet/SoldierGetCondition/Texts", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierGetConditionDescStateCtrl;
    [AutoBind("./SoldierSelet/SoldierGetCondition/Texts/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierGetConditionDescJobNameText;
    [AutoBind("./SoldierSelet/SoldierGetCondition/Texts/Text4", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierGetConditionDescText;
    [AutoBind("./SoldierSelet/SoldierGetCondition/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierGetConditionGotoButton;
    [AutoBind("./Prefab/SoldierItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierItemPrefabObj;
    [AutoBind("./SoldierInfo/SkinInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skinInfoButton;
    private Hero m_hero;
    private UISpineGraphic m_soldierInfoGraphic;
    private Dictionary<int, ConfigDataJobConnectionInfo> m_soldierIDToJobInfoDict;
    private SoldierItemUIController m_lastClickSoldierItemCtrl;
    private ConfigDataSoldierInfo m_lastClickSoldierInfo;
    private List<SoldierItemUIController> m_soliderItemListCtrl;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInSoldierState;
    private static DelegateBridge __Hotfix_ShowSoldierInfoPanel;
    private static DelegateBridge __Hotfix_ShowSoldierSelectPanel;
    private static DelegateBridge __Hotfix_GetSoliderSortComparar;
    private static DelegateBridge __Hotfix_NotGetSoliderSortComparar;
    private static DelegateBridge __Hotfix_OnSoldierItemClick;
    private static DelegateBridge __Hotfix_OnSoldierGetConditionGotoButtonClick;
    private static DelegateBridge __Hotfix_OnSoldierItemAttackButtonClick;
    private static DelegateBridge __Hotfix_SetCommonUIState;
    private static DelegateBridge __Hotfix_CleanSoldierPanelDataOnHeroChanged;
    private static DelegateBridge __Hotfix_OnSkinInfoButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnSoldierAttackButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSoldierAttackButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGotoDrill;
    private static DelegateBridge __Hotfix_remove_EventOnGotoDrill;
    private static DelegateBridge __Hotfix_add_EventOnGotoJobTransfer;
    private static DelegateBridge __Hotfix_remove_EventOnGotoJobTransfer;
    private static DelegateBridge __Hotfix_add_EventOnSkinInfoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSkinInfoButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailSoldierUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInSoldierState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSoldierInfoPanel(ConfigDataSoldierInfo soldierInfo, bool isSoldierGet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSoldierSelectPanel(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSoliderSortComparar(ConfigDataSoldierInfo s1, ConfigDataSoldierInfo s2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int NotGetSoliderSortComparar(ConfigDataSoldierInfo s1, ConfigDataSoldierInfo s2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick(SoldierItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierGetConditionGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemAttackButtonClick(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CleanSoldierPanelDataOnHeroChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int, Action> EventOnSoldierAttackButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGotoDrill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataJobConnectionInfo> EventOnGotoJobTransfer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataSoldierInfo> EventOnSkinInfoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
