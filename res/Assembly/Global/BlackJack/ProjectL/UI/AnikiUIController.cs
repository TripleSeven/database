﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AnikiUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AnikiUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./SecretBlessing/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blessingButton;
    [AutoBind("./PlayerResource/DailyReward/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyRewardCountText;
    [AutoBind("./PlayerResource/DailySweep/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailySweepCountText;
    [AutoBind("./GymList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gymListGameObject;
    [AutoBind("./LevelList/NameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gymNameText;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/AnikiGymListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_anikiGymListItemPrefab;
    [AutoBind("./Prefabs/AnikiLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_anikiLevelListItemPrefab;
    private List<AnikiGymListItemUIController> m_anikiGymListItems;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_AddAnikiGymListItem;
    private static DelegateBridge __Hotfix_ClearAnikiGymListItems;
    private static DelegateBridge __Hotfix_SetAllAnikiLevelListItem;
    private static DelegateBridge __Hotfix_SetScrollRectAtFirstIn;
    private static DelegateBridge __Hotfix_SetSelectedAnikiGym;
    private static DelegateBridge __Hotfix_SetDailyRewardCount;
    private static DelegateBridge __Hotfix_SetDailySweepCount;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnAddTicketButtonClick;
    private static DelegateBridge __Hotfix_OnTeamButtonClick;
    private static DelegateBridge __Hotfix_OnBlessingButtonClick;
    private static DelegateBridge __Hotfix_AnikiGymListItem_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_AnikiLevelListItem_OnStartButtonClick;
    private static DelegateBridge __Hotfix_AnikiLevelListItem_OnLevelRaidComplete;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnAddTicket;
    private static DelegateBridge __Hotfix_remove_EventOnAddTicket;
    private static DelegateBridge __Hotfix_add_EventOnShowTeam;
    private static DelegateBridge __Hotfix_remove_EventOnShowTeam;
    private static DelegateBridge __Hotfix_add_EventOnSelectAnikiGym;
    private static DelegateBridge __Hotfix_remove_EventOnSelectAnikiGym;
    private static DelegateBridge __Hotfix_add_EventOnStartAnikiLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartAnikiLevel;
    private static DelegateBridge __Hotfix_add_EventOnLevelRaidComplete;
    private static DelegateBridge __Hotfix_remove_EventOnLevelRaidComplete;

    [MethodImpl((MethodImplOptions) 32768)]
    private AnikiUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnikiGymListItem(ConfigDataAnikiGymInfo anikiGymInfo, bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAnikiGymListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllAnikiLevelListItem(List<ConfigDataAnikiLevelInfo> levelInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScrollRectAtFirstIn(ConfigDataAnikiGymInfo anikiGymInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectedAnikiGym(ConfigDataAnikiGymInfo anikiGymInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyRewardCount(int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailySweepCount(int count, int maxNum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlessingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiGymListItem_OnToggleValueChanged(bool isOn, AnikiGymListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiLevelListItem_OnStartButtonClick(AnikiLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiLevelListItem_OnLevelRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataAnikiGymInfo> EventOnSelectAnikiGym
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataAnikiLevelInfo> EventOnStartAnikiLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLevelRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
