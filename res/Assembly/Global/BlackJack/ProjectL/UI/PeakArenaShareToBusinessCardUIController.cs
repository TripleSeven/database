﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaShareToBusinessCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaShareToBusinessCardUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiAnimation;
    [AutoBind("./BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./HeroMomentSelectPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./HeroMomentSelectPanel/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_content;
    [AutoBind("./HeroMomentSelectPanel/HeroListScrollView/Viewport/Content/Empty", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_emptyButton;
    [AutoBind("./HeroMomentSelectPanel/ShareNumber/Text_Number", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_shareCountText;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_resourceContainer;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaShareReportToBusinessCardUITask m_peakArenaShareReportToBusinessCardUITask;
    private GameObjectPool<PeakArenaReportListItemUIController> m_reportItemPool;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnShareClick;
    private static DelegateBridge __Hotfix_OnReplayReport;
    private static DelegateBridge __Hotfix_OnRenameReport;
    private static DelegateBridge __Hotfix_OnCancelShareReport;
    private static DelegateBridge __Hotfix_OnCloseClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaShareToBusinessCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplayReport(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRenameReport(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelShareReport(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
