﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActivityUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class ActivityUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private List<Goods> m_gainGoodList;
    private ActivityUIController m_activityUIController;
    public const string ParamsKey_ActivityId = "ActivityId";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectPoolItemsAssets;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_ActivityUIController_OnGainReward;
    private static DelegateBridge __Hotfix_GetGainListGoods;
    private static DelegateBridge __Hotfix_DisposeBoxOpenNetTask;
    private static DelegateBridge __Hotfix_OpenHeroSkinSelfSelectBoxNetTask;
    private static DelegateBridge __Hotfix_ActivityUIController_OnExchangeItemGroup;
    private static DelegateBridge __Hotfix_ActivityUIController_OnAddActivity;
    private static DelegateBridge __Hotfix_ActivityUIController_OnReturn;
    private static DelegateBridge __Hotfix_ActivityUIController_OnGetRewardButtonClick;
    private static DelegateBridge __Hotfix_ActivityUIController_OnGoToButtonClick;
    private static DelegateBridge __Hotfix_StartSelectCardUITask;
    private static DelegateBridge __Hotfix_SelectCardUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_ActivityUIController_OnCrystalNotEnough;
    private static DelegateBridge __Hotfix_OnNoticeCenterStateSuccess;
    private static DelegateBridge __Hotfix_OnNoticeCenterStateFailed;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public ActivityUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPoolItemsAssets(int operationalActivityItemGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUIController_OnGainReward(
      ulong activityInstanceID,
      int rewardIndex,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator GetGainListGoods(List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DisposeBoxOpenNetTask(GoodsType type, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OpenHeroSkinSelfSelectBoxNetTask(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUIController_OnExchangeItemGroup(
      ulong activityInstanceID,
      int itemGroupIndex,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUIController_OnAddActivity(string gmCmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ActivityUIController_OnGetRewardButtonClick(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ActivityUIController_OnGoToButtonClick(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSelectCardUITask(int cardPoolId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUIController_OnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeCenterStateSuccess(int status, int interval, string noticeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeCenterStateFailed(string noticeId)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
