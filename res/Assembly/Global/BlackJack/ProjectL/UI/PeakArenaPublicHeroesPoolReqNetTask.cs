﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaPublicHeroesPoolReqNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaPublicHeroesPoolReqNetTask : UINetTask
  {
    private ProjectLPlayerContext m_playerContext;
    public List<PeakArenaHeroWithEquipments> m_peakArenaHeroWithEquipments;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnMsgAck;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPublicHeroesPoolReqNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMsgAck(
      int result,
      List<PeakArenaHeroWithEquipments> peakArenaHeroWithEquipments)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
