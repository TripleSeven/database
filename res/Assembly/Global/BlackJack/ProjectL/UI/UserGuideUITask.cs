﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UserGuideUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class UserGuideUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Coroutine m_initCoroutine;
    private UserGuideUIController m_userGuideUIController;
    private ConfigDataUserGuideStep m_userGuideStepInfo;
    private const float m_waitForClickObjectTime = 0.2f;
    private const float m_maxWaitObjectTime = 20f;
    private ConfigDataBattleDialogInfo m_battleDialogInfo;
    private BattleDialogUITask m_battleDialogUITask;
    private UserGuideDialogUITask m_userGuideDialogUITask;
    private int m_pageIndex;
    private int m_nUserGuideId;
    private bool m_isDoingUpdateViewAsync;
    private List<GameObject> m_temporaryDisableObjects;
    private List<GameObject> m_temporaryDeactiveObjects;
    private bool m_isFinished;
    private bool m_isTemporaryDisableMoveBattleCamera;
    private bool m_isEnableSkip;
    private static Dictionary<int, int> m_triggerUserGuideCounts;
    private static Dictionary<int, ConfigDataUserGuide> m_userGuideConfigs;
    private static List<string[]> m_userGuideShowHideEventObjectPaths;
    private static bool m_isEnable;
    private static int m_dragHeroToBattleUserGuideID;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_AddUserGuideTriggerCount;
    private static DelegateBridge __Hotfix_GetUserGuideTriggerCount;
    private static DelegateBridge __Hotfix_SaveUserGuideProgress;
    private static DelegateBridge __Hotfix_LoadUserGuideProgress;
    private static DelegateBridge __Hotfix_get_UserGuideProgressFileName;
    private static DelegateBridge __Hotfix_IsUserGuideBattle;
    private static DelegateBridge __Hotfix_NeedSkipBattlePrepareForUserGuide;
    private static DelegateBridge __Hotfix_GetEnforceHeros;
    private static DelegateBridge __Hotfix_CollectUserGuideShowHideEventObjectPaths;
    private static DelegateBridge __Hotfix_IsArrayEqual;
    private static DelegateBridge __Hotfix_OnShowObject;
    private static DelegateBridge __Hotfix_OnHideObject;
    private static DelegateBridge __Hotfix_OnUIStateEnd;
    private static DelegateBridge __Hotfix_OnSelectBattleActor;
    private static DelegateBridge __Hotfix_OnWorldUIGetReady;
    private static DelegateBridge __Hotfix_OnServerFinishUserGuide;
    private static DelegateBridge __Hotfix_OnDeselectBattleActor;
    private static DelegateBridge __Hotfix_OnGiftStoreGoodsBuy;
    private static DelegateBridge __Hotfix_OnUITaskShow;
    private static DelegateBridge __Hotfix_OnReturnToLoginUI;
    private static DelegateBridge __Hotfix_OnPrefabAwake;
    private static DelegateBridge __Hotfix_AddShowHideEventForUserGuide;
    private static DelegateBridge __Hotfix_IsUserGuideTriggerObject;
    private static DelegateBridge __Hotfix_TriggerUserGuide;
    private static DelegateBridge __Hotfix_Trigger;
    private static DelegateBridge __Hotfix_MatchTrigger;
    private static DelegateBridge __Hotfix_CheckCondition;
    private static DelegateBridge __Hotfix_StartStep;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_ForceStop;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_AddShowHideEventForUserGuideStep;
    private static DelegateBridge __Hotfix_DoUpdateView;
    private static DelegateBridge __Hotfix_EnableInputLately;
    private static DelegateBridge __Hotfix_GetPostProcessFuncForFunctionOpenObj;
    private static DelegateBridge __Hotfix_DoUpdateViewAsync;
    private static DelegateBridge __Hotfix_OnFunctionOpenAnimEnd;
    private static DelegateBridge __Hotfix_GetCenterScreenPosition;
    private static DelegateBridge __Hotfix_IsInsideScreen;
    private static DelegateBridge __Hotfix_IsClickable;
    private static DelegateBridge __Hotfix_IsChildTransform;
    private static DelegateBridge __Hotfix_PrepareForGetUIPosition;
    private static DelegateBridge __Hotfix_ClickObject;
    private static DelegateBridge __Hotfix_Click;
    private static DelegateBridge __Hotfix_ArrayToPathString;
    private static DelegateBridge __Hotfix_DoAction;
    private static DelegateBridge __Hotfix_EnableBattleCameraTouchMove;
    private static DelegateBridge __Hotfix_FinishCurrentUserGuide;
    private static DelegateBridge __Hotfix_WaitForObjectReady;
    private static DelegateBridge __Hotfix_ScrollToItem;
    private static DelegateBridge __Hotfix_EnableObject;
    private static DelegateBridge __Hotfix_ClickBattleGrid;
    private static DelegateBridge __Hotfix_ShowCurrentPage;
    private static DelegateBridge __Hotfix_InitUserGuideUIController;
    private static DelegateBridge __Hotfix_UninitUserGuideUIController;
    private static DelegateBridge __Hotfix_StartBattleDialogUITask;
    private static DelegateBridge __Hotfix_BattleDialogUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartUserGuideDialogUITask;
    private static DelegateBridge __Hotfix_UserGuideDialogUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_OnNextButtonClicked;
    private static DelegateBridge __Hotfix_OnSkipUserGuide;
    private static DelegateBridge __Hotfix_Finish;
    private static DelegateBridge __Hotfix_Next;
    private static DelegateBridge __Hotfix_UserGuideUIController_OnNextPage;
    private static DelegateBridge __Hotfix_UserGuideUIController_OnPrevPage;
    private static DelegateBridge __Hotfix_BattleDialogUITask_OnUserGuideClose;
    private static DelegateBridge __Hotfix_UserGuideDialogUITask_OnUserGuideClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_Enable;
    private static DelegateBridge __Hotfix_set_Enable;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public UserGuideUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Initialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddUserGuideTriggerCount(int userGuideId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetUserGuideTriggerCount(int userGuideId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SaveUserGuideProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void LoadUserGuideProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    private static string UserGuideProgressFileName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsUserGuideBattle(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool NeedSkipBattlePrepareForUserGuide(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetEnforceHeros(int battleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CollectUserGuideShowHideEventObjectPaths(List<string[]> paths)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsArrayEqual<T>(T[] arr1, T[] arr2, Comparison<T> compare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnShowObject(string objectPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnHideObject(string objectPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnUIStateEnd(GameObject obj, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnSelectBattleActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool OnWorldUIGetReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnServerFinishUserGuide(int guideID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnDeselectBattleActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnGiftStoreGoodsBuy(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnUITaskShow(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnReturnToLoginUI(bool obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnPrefabAwake(GameObject awakeObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddShowHideEventForUserGuide(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsUserGuideTriggerObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TriggerUserGuide(UserGuideTrigger trigger, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Trigger(UserGuideTrigger trigger, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool MatchTrigger(
      UserGuideTrigger trigger1,
      string param1,
      UserGuideTrigger trigger2,
      string param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CheckCondition(UserGuideCondition c, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartStep(int userGuideId, int userGuideStepId, bool bSkipEnable = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ForceStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddShowHideEventForUserGuideStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator EnableInputLately(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetPostProcessFuncForFunctionOpenObj(
      ConfigDataUserGuideStep userGuideStepInfo,
      out Action<GameObject> postProcessFunc)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DoUpdateViewAsync()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFunctionOpenAnimEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool GetCenterScreenPosition(RectTransform rt, ref Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsInsideScreen(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsClickable(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsChildTransform(Transform parent, Transform child)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrepareForGetUIPosition(GameObject uiObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickObject(string[] objPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Click(GameObject o)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ArrayToPathString(string[] path)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DoAction(UserGuideAction action, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableBattleCameraTouchMove(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FinishCurrentUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitForObjectReady(string objPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ScrollToItem(string scrollObjPath, string itemObjPath)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableObject(GameObject obj, bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickBattleGrid(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCurrentPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitUserGuideUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitUserGuideUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleDialogUITask(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator StartUserGuideDialogUITask(
      ConfigDataUserGuideDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNextButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Finish(bool isClick = true, bool bForce = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Next()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideUIController_OnNextPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideUIController_OnPrevPage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnUserGuideClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UserGuideDialogUITask_OnUserGuideClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static bool Enable
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static UserGuideUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
