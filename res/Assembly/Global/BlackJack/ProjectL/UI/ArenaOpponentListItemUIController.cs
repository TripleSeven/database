﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaOpponentListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArenaOpponentListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    [AutoBind("./PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerLevel/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./ArenaPoints", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaPointsGameObject;
    [AutoBind("./ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointsText;
    [AutoBind("./VictoryPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_victoryPointsText;
    [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    private int m_index;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetArenaOpponent;
    private static DelegateBridge __Hotfix_GetIndex;
    private static DelegateBridge __Hotfix_OnAttackButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAttackButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAttackButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaOpponent(ArenaOpponent o, int idx, bool isGuardBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ArenaOpponentListItemUIController> EventOnAttackButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
