﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareStageActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class BattlePrepareStageActor
  {
    private GridPosition m_position;
    private int m_direction;
    private int m_team;
    private int m_playerIndex;
    private BattleHero m_hero;
    private int m_behaviorId;
    private int m_groupId;
    private StageActorTagType m_tagType;
    private StagePositionType m_stagePositionType;
    private bool m_isGray;
    private int m_heroHealthPoint;
    private int m_soldierHealthPoint;
    private ConfigDataSkillInfo m_extraTalentSkillInfo;
    private ClientBattle m_clientBattle;
    private GenericGraphic[] m_graphics;
    private int m_soldierCount;
    private Colori m_tweenFromColor;
    private Colori m_tweenToColor;
    private float m_tweenColorTime;
    private BattleActorUIController m_uiController;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Destroy;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_DestroyUI;
    private static DelegateBridge __Hotfix_Setup;
    private static DelegateBridge __Hotfix_GetHero;
    private static DelegateBridge __Hotfix_GetHeroId;
    private static DelegateBridge __Hotfix_GetSoldierId;
    private static DelegateBridge __Hotfix_GetSkillList;
    private static DelegateBridge __Hotfix_SetHp;
    private static DelegateBridge __Hotfix_GetHeroHp;
    private static DelegateBridge __Hotfix_GetSoldierHp;
    private static DelegateBridge __Hotfix_SetExtraTalentSkillInfo;
    private static DelegateBridge __Hotfix_GetExtraTalentSkillInfo;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_SetPosition;
    private static DelegateBridge __Hotfix_SetGray;
    private static DelegateBridge __Hotfix_SetChooseEffect;
    private static DelegateBridge __Hotfix_IsGray;
    private static DelegateBridge __Hotfix_ForceUpdateSpine;
    private static DelegateBridge __Hotfix_GetPosition;
    private static DelegateBridge __Hotfix_GetDirection;
    private static DelegateBridge __Hotfix_GetTeam;
    private static DelegateBridge __Hotfix_GetPlayerIndex;
    private static DelegateBridge __Hotfix_GetTagType;
    private static DelegateBridge __Hotfix_GetPositionType;
    private static DelegateBridge __Hotfix_GetBehaviorId;
    private static DelegateBridge __Hotfix_GetGroupId;
    private static DelegateBridge __Hotfix_IsNpc;
    private static DelegateBridge __Hotfix_IsAINpc;
    private static DelegateBridge __Hotfix_IsTeammate;
    private static DelegateBridge __Hotfix_IsEnforce;
    private static DelegateBridge __Hotfix_TweenColor;
    private static DelegateBridge __Hotfix_GetUIController;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_ComputeGraphicPosition;
    private static DelegateBridge __Hotfix_ComputeGraphicOffset;
    private static DelegateBridge __Hotfix_ComputeSoldierGraphicCount;
    private static DelegateBridge __Hotfix_UpdateGraphicVisible;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareStageActor(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(
      BattleHero hero,
      int team,
      StagePositionType posType,
      StageActorTagType tagType,
      int behaviorId,
      int groupId,
      int playerIndex,
      bool isSkipGraphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetSkillList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHp(int heroHp, int heroHpMax, int soldierHp, int soldierHpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroHp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierHp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExtraTalentSkillInfo(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetExtraTalentSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGray(bool isGray)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChooseEffect(bool canChoose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGray()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdateSpine()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDirection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StageActorTagType GetTagType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StagePositionType GetPositionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBehaviorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGroupId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAINpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeammate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnforce()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TweenColor(Colori c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorUIController GetUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeGraphicOffset(int idx, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeSoldierGraphicCount(int soldierHp, int soldierHpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGraphicVisible()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
