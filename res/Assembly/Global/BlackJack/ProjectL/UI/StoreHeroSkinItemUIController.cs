﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreHeroSkinItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class StoreHeroSkinItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_storeItemButton;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_heroSkinIcon;
    [AutoBind("./HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_heroName;
    [AutoBind("./OtherNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_skinName;
    [AutoBind("./PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_priceText;
    [AutoBind("./Tape/TSurplusText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_restTime;
    [AutoBind("./TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_timeGo;
    [AutoBind("./TimeLimit/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_timeValueText;
    [AutoBind("./Champion", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_championStateCtrl;
    [AutoBind("./Champion/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_championTitleText;
    [AutoBind("./Champion/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_championTimeValueText;
    [AutoBind("./TimeLimit/TimeTitleText", AutoBindAttribute.InitState.Inactive, false)]
    public Text m_timeTitleText;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_itemStateCtrl;
    public ConfigDataFixedStoreItemInfo m_storeItemConfig;
    public StoreType m_storeType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroSkinItemInfo;
    private static DelegateBridge __Hotfix_SetItemState;
    private static DelegateBridge __Hotfix_SetLimitTime;
    private static DelegateBridge __Hotfix_SetChampionTitle;
    private static DelegateBridge __Hotfix_OnStoreItemClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;
    private static DelegateBridge __Hotfix_add_EventOnSkinItemOutOfDate;
    private static DelegateBridge __Hotfix_remove_EventOnSkinItemOutOfDate;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreHeroSkinItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroSkinItemInfo(FixedStoreItem fixedStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChampionTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreHeroSkinItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinItemOutOfDate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
