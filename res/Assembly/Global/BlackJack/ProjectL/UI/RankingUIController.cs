﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RankingUIController : UIControllerBase
  {
    protected bool m_isDuringUpdate;
    protected bool m_isSubMenuPanelPowerShow;
    protected bool m_isSubMenuPanelRiftShow;
    protected bool m_isSubMenuPanelUnchartedShow;
    protected bool m_isSubMenuPanelPeakArenaShow;
    protected RankingUIController.MainMenuSelectState m_mainMenuSelectState;
    protected RankingListType m_currRankingType;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ReturnButton;
    [AutoBind("./SubMenumGroup/NoneSubMenuArea", AutoBindAttribute.InitState.NotInit, false)]
    public RankingSubMenuUIController SubMenuUICtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Power", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuPowerButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Power", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuPowerStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/TopHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuTopHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/TopHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuTopHeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/Top15Hero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuTop15HeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/Top15Hero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuTop15HeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/AllHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuAllHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/AllHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuAllHeroStateCtrl;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/ChampionHero", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuChampionHeroButton;
    [AutoBind("./SubMenumGroup/PowerMenulList/BGImage/BGImage1/Content/ChampionHero", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuChampionHeroStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Rift", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuRiftButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Rift", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuRiftStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftChapterStar", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuRiftChapterStarButton;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftChapterStar", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuRiftChapterStarStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftAchievement", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuRiftAchievementButton;
    [AutoBind("./SubMenumGroup/RiftMenuList/BGImage/BGImage1/Content/RiftAchievement", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuRiftAchievementStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Uncharted", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuUnchartedButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Uncharted", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuUnchartedStateCtrl;
    [AutoBind("./SubMenumGroup/UnchartedMenuList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SubMenuClimbTowerButton;
    [AutoBind("./SubMenumGroup/UnchartedMenuList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuClimbTowerStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Stage", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx MainMenuStageButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/Stage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController MainMenuStageStateCtrl;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/PeakArena", AutoBindAttribute.InitState.Inactive, false)]
    public ButtonEx MainMenuPeakArenaButton;
    [AutoBind("./MainMenuPanel/MainMenuScrollView/Viewport/Content/PeakArena", AutoBindAttribute.InitState.Inactive, false)]
    public CommonUIStateController MainMenuPeakArenaStateCtrl;
    [AutoBind("./SubMenumGroup/PeakArenaListList/BGImage/BGImage1/Content", AutoBindAttribute.InitState.NotInit, false)]
    public Transform SubMenuPeakArenaSubMemuTransform;
    [AutoBind("./SubMenumGroup/PeakArenaListList/BGImage/BGImage1/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SubMenuPeakArenaItemPrefab;
    [AutoBind("./SubMenumGroup/PowerMenulList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelPowerStateCtrl;
    [AutoBind("./SubMenumGroup/RiftMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelRiftStateCtrl;
    [AutoBind("./SubMenumGroup/UnchartedMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelUnchartedStateCtrl;
    [AutoBind("./SubMenumGroup/StageMenuList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelStageStateCtrl;
    [AutoBind("./SubMenumGroup/PeakArenaListList", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SubMenuPanelPeakArenaStateCtrl;
    private List<CommonUIStateController> PeakArenaSubMenuItemStateCtrlList;
    private int m_curSelectPeakArenaSeasonID;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateRankingUICtrl;
    private static DelegateBridge __Hotfix_GetMainMenuSelectStateFromRankingType;
    private static DelegateBridge __Hotfix_SetMainMenuSelectState;
    private static DelegateBridge __Hotfix_SetAllMainMenuUnselect;
    private static DelegateBridge __Hotfix_SetSubMenuSelectState;
    private static DelegateBridge __Hotfix_SetAllSubMenuToggleUnselect;
    private static DelegateBridge __Hotfix_ShowPowerSubMenuPanel;
    private static DelegateBridge __Hotfix_ShowRiftSubMenuPanel;
    private static DelegateBridge __Hotfix_ShowUnchartedSubMenuPanel;
    private static DelegateBridge __Hotfix_ShowPeakArenaSubMenuPanel;
    private static DelegateBridge __Hotfix_HideAllSubMenuPanel;
    private static DelegateBridge __Hotfix_CreatePeakArenaSeasonSubItem;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnNoneSubMenuAreaClick;
    private static DelegateBridge __Hotfix_OnMainMenuPowerClick;
    private static DelegateBridge __Hotfix_OnMainMenuRiftClick;
    private static DelegateBridge __Hotfix_OnMainMenuUnchartedClick;
    private static DelegateBridge __Hotfix_OnMainMenuStageClick;
    private static DelegateBridge __Hotfix_OnMainMenuPeakArenaClick;
    private static DelegateBridge __Hotfix_OnRankingTypeMenuClick;
    private static DelegateBridge __Hotfix_OnSubMenuTop15HeroClick;
    private static DelegateBridge __Hotfix_OnSubMenuTopHeroClick;
    private static DelegateBridge __Hotfix_OnSubMenuAllHeroClick;
    private static DelegateBridge __Hotfix_OnSubMenuChampionHeroClick;
    private static DelegateBridge __Hotfix_OnSubMenuRiftChapterStarClick;
    private static DelegateBridge __Hotfix_OnSubMenuRiftAchievementClick;
    private static DelegateBridge __Hotfix_OnSubMenuClimbTowerClick;
    private static DelegateBridge __Hotfix_OnSubMenuPeakArenaClick;
    private static DelegateBridge __Hotfix_add_EventOnRankingTypeMenuClick;
    private static DelegateBridge __Hotfix_remove_EventOnRankingTypeMenuClick;
    private static DelegateBridge __Hotfix_add_EventOnResetRankingType;
    private static DelegateBridge __Hotfix_remove_EventOnResetRankingType;
    private static DelegateBridge __Hotfix_add_EventOnRetunButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRetunButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingUICtrl(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected RankingUIController.MainMenuSelectState GetMainMenuSelectStateFromRankingType(
      RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetMainMenuSelectState(RankingUIController.MainMenuSelectState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetAllMainMenuUnselect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSubMenuSelectState(RankingListType rankType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetAllSubMenuToggleUnselect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowPowerSubMenuPanel(bool isshow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowRiftSubMenuPanel(bool isshow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowUnchartedSubMenuPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowPeakArenaSubMenuPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void HideAllSubMenuPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CreatePeakArenaSeasonSubItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNoneSubMenuAreaClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuPowerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuRiftClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuUnchartedClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMainMenuStageClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMainMenuPeakArenaClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankingTypeMenuClick(RankingListType rankListType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuTop15HeroClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuTopHeroClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuAllHeroClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuChampionHeroClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuRiftChapterStarClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuRiftAchievementClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuClimbTowerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubMenuPeakArenaClick(int seasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RankingListType, int> EventOnRankingTypeMenuClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnResetRankingType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRetunButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum MainMenuSelectState
    {
      None,
      Power,
      Rift,
      Uncharted,
      Stage,
      PeakArena,
    }
  }
}
