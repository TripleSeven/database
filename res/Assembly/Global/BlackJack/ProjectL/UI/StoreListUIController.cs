﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class StoreListUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_storeUIStateCtrl;
    [AutoBind("./StoreUIPanel/RefreshPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_refreshPanelUIStateCtrl;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewStoreContent;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewHeroContent;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemBuyPanelUIStateController;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/Item/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemCountBgGo;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/GoodCount", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodCountObj;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemHaveCountText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/ItemGoodsDesc/DescPanel/DescScrollView/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemPriceIcon;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPriceIconEffectGameObject;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel/Group/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemPriceText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyPanelCloseButton;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/LayoutRoot/BuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyButton;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_resonanceInfoPanel;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanelNameText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel/SuitInfo/2SuitInfo/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel2SuitInfoText;
    [AutoBind("./StoreUIPanel/StoreItemBuyPanel/ResonanceInfoPanel/SuitInfo/4SuitInfo/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel4SuitInfoText;
    [AutoBind("./StoreUIPanel/PackagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_boxBuyPanelUIStateController;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_boxIconImage;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxNameText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ExplainInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxDescGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ExplainInfoPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxDescText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_boxDescUIStateController;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ItemPanel/ItemGroup/ItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxListScrollViewContent;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button/IconImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_boxPriceIcon;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button/IconImage/Image/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxPriceIconEffectGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxPriceText;
    [AutoBind("./StoreUIPanel/PackagePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_boxBuyPanelCloseButton;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/ButtonPanel/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_boxBuyButton;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxTimeLimitGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/TimeLimit/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxTimeLimitText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/TimeLimit/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxTimeLimitTextTitle;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxBuyTimesGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/BuyTimes/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxBuyTimesText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/ValueOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boxValueOffGameObject;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/ValueOffImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxValueOffText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxCountText;
    [AutoBind("./StoreUIPanel/PackagePanel/Detail/FrameImage/PackageInfoPanel/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_boxHaveCountText;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefab/StoreBoxItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeBoxItemPrefab;
    [AutoBind("./Prefab/StoreItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeItemPrefab;
    [AutoBind("./Prefab/HeroSkinItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeHeroSkinItemPrefab;
    [AutoBind("./Prefab/StoreItemSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeSoldierSkinItemPrefab;
    [AutoBind("./StoreUIPanel/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Recharge", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rechargeToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Present", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_presentToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_crystalToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Skin", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinToggle;
    [AutoBind("./StoreUIPanel/Margin/StoreTab/Mysterious", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousToggle;
    [AutoBind("./StoreUIPanel/StorePackagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selfChoosePanel;
    [AutoBind("./StoreUIPanel/RefreshPanel/RefreshText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshTimeText;
    [AutoBind("./StoreUIPanel/RefreshPanel/PricePanel/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_refreshCurrencyTypeIcon;
    [AutoBind("./StoreUIPanel/RefreshPanel/PricePanel/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_refreshCurrencyTypeIconEffectGameObject;
    [AutoBind("./StoreUIPanel/RefreshPanel/PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshPriceText;
    [AutoBind("./StoreUIPanel/RefreshPanel/RefreshButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refreshButton;
    [AutoBind("./StoreUIPanel/RefreshPanel/TimesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshTimesText;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierSkinToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabHero", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_heroSkinToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/BlackMarket", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousBlackMarketToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Honor", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousHonorToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Medal", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousMedalToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Friendship", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousFriendshipToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Union", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousUnionToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Present", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_presentPresentToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Prerogative", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_presentPreogativeToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabHero", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinHeroToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/TabSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_skinSoldierToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Memory", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousMemoryToggle;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ToggleTab/Equipment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_mysteriousEquipmentToggle;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency1Obj;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency1Icon;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency1CountText;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1AddButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency1/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1DescriptionButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency2Obj;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency2Icon;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency2CountText;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2AddButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency2/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2DescriptionButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency3Obj;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency3Icon;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency3CountText;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3AddButton;
    [AutoBind("./StoreUIPanel/PlayerResource/Currency3/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3DescriptionButton;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/BoxGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeNameText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/BoxGoodsDesc/ItemIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rechargeIconImage;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeHaveCountText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/SureBth/SureText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargePriceText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/SureBth/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rechargePriceImage;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/ListPanel/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeGetCountText;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/ListPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rechargeGetExtraCountText;
    [AutoBind("./StoreUIPanel/RechargePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rechargePanelCloseButton;
    [AutoBind("./StoreUIPanel/RechargePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rechargePanelUIStateController;
    [AutoBind("./StoreUIPanel/RechargePanel/LayoutRoot/SureBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rechargeBuyButton;
    [AutoBind("./StoreUIPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_storePrivilegeBuyDetailPanelStateCtrl;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storePrivilegeBuyDetailPanelBGButon;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_storePrivilegeStateCtrl;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_StorePrivilegeItemName;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_StorePrivilegeItemIconImage;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemCountText;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ListPanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemDescText;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/Tape", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storePrivilegeItemLeftDayGameObject;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/Tape/TSurplusText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemLeftDay;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storePrivilegeItemBuyButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/BuyButton/SureText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storePrivilegeItemBuyPrice;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Month/Count/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_storeMonthExtraRewardCount;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/StoreMonth&SubscribePanel/ItemDesc/Subscribe/Count/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_subscribePrice;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect/AgreementButton2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subscribePanelPrivateButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect/AgreementButton1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subscribePanelCancelSubscribeButton;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect/GoogleAgreementButton2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subscribePanelGoogleButton1;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect/GoogleAgreementButton1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subscribePanelGoogleButton2;
    [AutoBind("./StoreUIPanel/MonthCardDetailPanel/SubscribePanel/ScrollView/Mask/Contect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_subscribeUIState;
    [AutoBind("./StoreUIPanel/MemoryEntranceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryEntranceButton;
    [AutoBind("./StoreUIPanel/MemoryEntranceButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryEntranceButtonStateCtrl;
    [AutoBind("./StoreUIPanel/MemoryEntranceButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryEntranceButtonRedPoint;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryExtractionPanelStateCtrl;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_memoryExtractionPanelScrollRect;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/ListScrollView/Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryExtractionPanelScrollContent;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelHelpButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/ExtractionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelExtractionButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_memoryExtractionPanelTotalValueText;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/BoxGoodsDesc/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelCloseButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryExtractionPanelBGReturnButton;
    [AutoBind("./StoreUIPanel/MemoryExtractionPanel/LayoutRoot/Empty", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryExtractionPanelEmptyTip;
    [AutoBind("./Prefab/HeroFragmentItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HeroFragmentItemPrefab;
    private bool m_needRefreshPanel;
    private bool m_isIgnoreToggleEvent;
    private StoreType m_storeType;
    private StoreId m_storeId;
    private int m_fixedStoreItemGoodsID;
    private int m_goodsID;
    private int m_index;
    private StoreId m_fixedStoreID;
    private StoreId m_randomStoreID;
    private bool m_isStaticBox;
    public int m_selfChooseItemIndex;
    private Vector3 m_scrollViewStoreContentOriginalLocalPos;
    private Vector3 m_scrollViewHeroContentOriginalLocalPos;
    private GiftStoreItem m_giftStoreItem;
    private DateTime m_giftStoreItemEndTime;
    private GameObjectPool<StoreItemUIController> m_storeUnderItemPool;
    private GameObjectPool<StoreHeroSkinItemUIController> m_storeHeroSkinItemPool;
    private GameObjectPool<StoreSoldierSkinItemUIController> m_storeSoldierSkinItemPool;
    private List<Goods> m_gainGoodsList;
    private Dictionary<StoreId, List<Toggle>> m_storeSubType2ToggleDic;
    private StoreSelfChooseUIController m_storeSelfChooseUIController;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_GetHeroSkinItemCtrlByFixedStoreItemId;
    private static DelegateBridge __Hotfix_UpdateHeroSkinItemLimitTime;
    private static DelegateBridge __Hotfix_UpdateGiftStoreRemoveTime_0;
    private static DelegateBridge __Hotfix_UpdateStore;
    private static DelegateBridge __Hotfix_UpdateRechargeStore;
    private static DelegateBridge __Hotfix_UpdateGiftStore;
    private static DelegateBridge __Hotfix_UpdateFixedStore;
    private static DelegateBridge __Hotfix_UpdateRandomStore;
    private static DelegateBridge __Hotfix_ClearPool;
    private static DelegateBridge __Hotfix_UpdateFixedStoreInfo;
    private static DelegateBridge __Hotfix_UpdateHeroSkinInfo;
    private static DelegateBridge __Hotfix_SetSkinTicketCountText;
    private static DelegateBridge __Hotfix_UpdateSoldierSkinInfo;
    private static DelegateBridge __Hotfix_UpdateRandomStoreInfo;
    private static DelegateBridge __Hotfix_UpdateRechargeStoreInfo;
    private static DelegateBridge __Hotfix_SetDefaultState;
    private static DelegateBridge __Hotfix_SetFixedStoreInfo;
    private static DelegateBridge __Hotfix_SortFixedStoreItemByUISort;
    private static DelegateBridge __Hotfix_RefreshCurrencyDisplay;
    private static DelegateBridge __Hotfix_SetRandomStoreInfo;
    private static DelegateBridge __Hotfix_SetRechargeStoreInfo;
    private static DelegateBridge __Hotfix_ShowRefreshPanel;
    private static DelegateBridge __Hotfix_HideRefreshPanel;
    private static DelegateBridge __Hotfix_SetRefreshPanel;
    private static DelegateBridge __Hotfix_IsNeedRefreshPanel;
    private static DelegateBridge __Hotfix_StoreOpenTween;
    private static DelegateBridge __Hotfix_OnStoreItemClick;
    private static DelegateBridge __Hotfix_OnHeroSkinItemClick;
    private static DelegateBridge __Hotfix_OnSkinItemOutOfDate;
    private static DelegateBridge __Hotfix_OnSoldierSkinItemClick;
    private static DelegateBridge __Hotfix_SetStoreBuyPanel;
    private static DelegateBridge __Hotfix_SetNormalItemBuyPanel_0;
    private static DelegateBridge __Hotfix_SetRechargeBuyPanel;
    private static DelegateBridge __Hotfix_SetBoxBuyPanel;
    private static DelegateBridge __Hotfix_SetItemBuyPanel_0;
    private static DelegateBridge __Hotfix_SetNormalItemBuyPanel_1;
    private static DelegateBridge __Hotfix_SetItemBuyPanel_1;
    private static DelegateBridge __Hotfix_SetEnchantStoneResonanceInfoPanel;
    private static DelegateBridge __Hotfix_SetRandomStoreCountDown;
    private static DelegateBridge __Hotfix_OpenBuyPanel;
    private static DelegateBridge __Hotfix_get_CurStoreId;
    private static DelegateBridge __Hotfix_UpdateGiftStoreInfo;
    private static DelegateBridge __Hotfix_SetGiftStoreInfo;
    private static DelegateBridge __Hotfix_SetGiftStoreBuyPanel;
    private static DelegateBridge __Hotfix_SetGiftStoreStaticBoxBuyPanel;
    private static DelegateBridge __Hotfix_SetMothCardBuyPanel;
    private static DelegateBridge __Hotfix_OnPresentPresentToggleValueChanged;
    private static DelegateBridge __Hotfix_OnPresentPreogativeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMonthCardBuyButtonClick;
    private static DelegateBridge __Hotfix_OnSubscribePrivateClick;
    private static DelegateBridge __Hotfix_OnCancelSubscribeClick;
    private static DelegateBridge __Hotfix_OnMonthCardPanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnPresentToggleValueChanged;
    private static DelegateBridge __Hotfix_SetGiftStoreBuyTimesLimit;
    private static DelegateBridge __Hotfix_SetGiftStoreTimeLimit;
    private static DelegateBridge __Hotfix_UpdateGiftStoreRemoveTime_1;
    private static DelegateBridge __Hotfix_SetGiftStoreValueOff;
    private static DelegateBridge __Hotfix_SetRecommendDesc;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnRefreshButtonClick;
    private static DelegateBridge __Hotfix_OnItemBuyPanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnItemBuyButtonClick;
    private static DelegateBridge __Hotfix_OnBoxBuyPanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnRechargePanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnBoxBuyButtonClick;
    private static DelegateBridge __Hotfix_IsStoreItemIOSSubscribeItem;
    private static DelegateBridge __Hotfix_OnMemoryEntranceButtonClick;
    private static DelegateBridge __Hotfix_SetMemoryExtractionPanelInfo;
    private static DelegateBridge __Hotfix_SortFragmentItemComparer;
    private static DelegateBridge __Hotfix_OnMemoryExtractionPanelExtractionButtonClick;
    private static DelegateBridge __Hotfix_OnMemeryExtractionHelpButtonClick;
    private static DelegateBridge __Hotfix_CloseMemoryExtractionPanel;
    private static DelegateBridge __Hotfix_FireChangeStoreEvent;
    private static DelegateBridge __Hotfix_OnRechargeBuyButtonClick;
    private static DelegateBridge __Hotfix_OnRechargeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnCystalToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSkinToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSkinTab_HeroToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSkinTab_SoldierToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousBlackMarketToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousMemoryToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousEquipmentToggleValueChange;
    private static DelegateBridge __Hotfix_OnMysteriousHonorToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousMedalToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousFriendshipToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMysteriousUnionToggleValueChanged;
    private static DelegateBridge __Hotfix_InitStoreSubType2Toggle;
    private static DelegateBridge __Hotfix_SaveOriginalScrollViewLocalPosition;
    private static DelegateBridge __Hotfix_UpdateToggles;
    private static DelegateBridge __Hotfix_FixedStoreSkinListSortFunc;
    private static DelegateBridge __Hotfix_add_EventOnChangeStore;
    private static DelegateBridge __Hotfix_remove_EventOnChangeStore;
    private static DelegateBridge __Hotfix_add_EventOnFixedStoreHeroSkinItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnFixedStoreHeroSkinItemClick;
    private static DelegateBridge __Hotfix_add_EventOnFixedStoreSoldierSkinItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnFixedStoreSoldierSkinItemClick;
    private static DelegateBridge __Hotfix_add_EventOnSkinItemOutOfDate;
    private static DelegateBridge __Hotfix_remove_EventOnSkinItemOutOfDate;
    private static DelegateBridge __Hotfix_add_EventOnFixedStoreItemBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnFixedStoreItemBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnFixedStoreBoxBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnFixedStoreBoxBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnRandomStoreItemBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnRandomStoreItemBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnRandomStoreBoxBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnRandomStoreBoxBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnRechargeStoreBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnRechargeStoreBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnGiftStoreBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnGiftStoreBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnGetRandomStore;
    private static DelegateBridge __Hotfix_remove_EventOnGetRandomStore;
    private static DelegateBridge __Hotfix_add_EventOnRefreshRandomStore;
    private static DelegateBridge __Hotfix_remove_EventOnRefreshRandomStore;
    private static DelegateBridge __Hotfix_add_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_remove_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnExtraButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnExtraButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnMemoryExtraction;
    private static DelegateBridge __Hotfix_remove_EventOnMemoryExtraction;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreHeroSkinItemUIController GetHeroSkinItemCtrlByFixedStoreItemId(
      int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateHeroSkinItemLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGiftStoreRemoveTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateStore(StoreId storeId, StoreType storeType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRechargeStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateGiftStore(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateFixedStore(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRandomStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateFixedStoreInfo(StoreId storeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateHeroSkinInfo(
      StoreId storeID,
      GameObjectPool<StoreHeroSkinItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkinTicketCountText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateSoldierSkinInfo(
      StoreId storeID,
      GameObjectPool<StoreSoldierSkinItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRandomStoreInfo(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateRechargeStoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetDefaultState(bool hideRefreshPanel = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetFixedStoreInfo(
      StoreId fixedStoreID,
      GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortFixedStoreItemByUISort(FixedStoreItem a, FixedStoreItem b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void RefreshCurrencyDisplay(List<StoreItemUIController> currencyTypeList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRandomStoreInfo(StoreId storeId, GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRechargeStoreInfo(GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowRefreshPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void HideRefreshPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRefreshPanel(StoreId randomStoreID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedRefreshPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnStoreItemClick(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroSkinItemClick(StoreHeroSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinItemOutOfDate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSoldierSkinItemClick(StoreSoldierSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetStoreBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNormalItemBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRechargeBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetBoxBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetItemBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNormalItemBuyPanel(Goods goods, StoreItemUIController storeItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemBuyPanel(Goods goods, StoreItemUIController storeItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomStoreCountDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenBuyPanel(StoreId storeId, int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    public StoreId CurStoreId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateGiftStoreInfo(StoreId storeID, GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGiftStoreInfo(StoreId storeId, GameObjectPool<StoreItemUIController> pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGiftStoreBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetGiftStoreStaticBoxBuyPanel(
      StoreItemUIController ctrl,
      ConfigDataItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetMothCardBuyPanel(StoreItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPresentPresentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPresentPreogativeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSubscribePrivateClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCancelSubscribeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPresentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGiftStoreBuyTimesLimit(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreTimeLimit(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGiftStoreRemoveTime(DateTime showEndTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreValueOff(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecommendDesc(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRefreshButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnItemBuyPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnItemBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBoxBuyPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargePanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBoxBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsStoreItemIOSSubscribeItem(StoreItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMemoryEntranceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMemoryExtractionPanelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortFragmentItemComparer(HeroFragmentBagItem f1, HeroFragmentBagItem f2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemoryExtractionPanelExtractionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemeryExtractionHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseMemoryExtractionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FireChangeStoreEvent(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCystalToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSkinToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSkinTab_HeroToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSkinTab_SoldierToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousBlackMarketToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousMemoryToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousEquipmentToggleValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousHonorToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousMedalToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousFriendshipToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMysteriousUnionToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitStoreSubType2Toggle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveOriginalScrollViewLocalPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool UpdateToggles(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int FixedStoreSkinListSortFunc(FixedStoreItem itemA, FixedStoreItem itemB)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreId> EventOnChangeStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreHeroSkinItemUIController> EventOnFixedStoreHeroSkinItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreSoldierSkinItemUIController> EventOnFixedStoreSoldierSkinItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinItemOutOfDate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event StoreListUIController.Action<StoreId, int, int, int, List<Goods>> EventOnFixedStoreItemBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId, int, int, List<Goods>> EventOnFixedStoreBoxBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId, int, List<Goods>> EventOnRandomStoreItemBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId, int, int, List<Goods>> EventOnRandomStoreBoxBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreType, int, int> EventOnRechargeStoreBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataGiftStoreItemInfo, bool, int> EventOnGiftStoreBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId> EventOnGetRandomStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreId> EventOnRefreshRandomStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType> EventOnExtraButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnMemoryExtraction
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
  }
}
