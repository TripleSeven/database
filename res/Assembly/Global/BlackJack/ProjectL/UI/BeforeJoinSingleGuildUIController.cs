﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BeforeJoinSingleGuildUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BeforeJoinSingleGuildUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildButton;
    [AutoBind("./SecletImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_guildCreateButton;
    [AutoBind("./SociatyNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sociatyNameText;
    [AutoBind("./PeopleValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peopleValueText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_LevelRequireText;
    [AutoBind("./VitalityText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_vitalityText;
    [AutoBind("./Apply", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_applyGameObject;
    private Action<BeforeJoinSingleGuildUIController> OnEventSelect;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public GuildSearchInfo m_guildSearchInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetSelect;
    private static DelegateBridge __Hotfix_SetApply;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_OnItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      GuildSearchInfo guildSearchInfo,
      Action<BeforeJoinSingleGuildUIController> selectClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelect(bool isSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetApply(bool isApply)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh(GuildSearchInfo guildSearchInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
