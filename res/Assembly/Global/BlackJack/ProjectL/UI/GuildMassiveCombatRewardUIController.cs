﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildMassiveCombatRewardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildMassiveCombatRewardUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardPanelAnimation;
    [AutoBind("./Detail/ToggleGroup/PersonalToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_personalToggle;
    [AutoBind("./Detail/ToggleGroup/GuildToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildToggle;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/Person", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_personPlane;
    [AutoBind("./Detail/Guild", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_guildPlane;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private MassiveCombatPersonRewardUIController m_personRewardUIController;
    private MassiveCombatGuildRewardUIController m_guildRewardUIController;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnEnable;
    private static DelegateBridge __Hotfix_OnTabChange;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_OnBGClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTabChange(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
