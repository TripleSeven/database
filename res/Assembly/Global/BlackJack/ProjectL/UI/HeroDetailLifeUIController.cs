﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailLifeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailLifeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ToggleGroup/ToggleLife", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggleLife;
    [AutoBind("./ToggleGroup/ToggleVoice", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggleVoice;
    [AutoBind("./ToggleGroup/ToggleLife/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleLifeRedPoint;
    [AutoBind("./ToggleGroup/ToggleVoice/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleVoiceRedPoint;
    [AutoBind("./LifeIntroductionScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_lifeScrollRect;
    [AutoBind("./VoiceScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_voiceScrollRect;
    [AutoBind("./LifeIntroductionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lifeContent;
    [AutoBind("./VoiceScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_voiceContent;
    [AutoBind("./VoiceScrollView/CV/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceCVNameText;
    private Hero m_hero;
    private ConfigDataHeroInformationInfo m_heroInformationInfo;
    private List<FettersInformationLifeItemUIController> biographyCtrlList;
    private List<FettersInformationVoiceItemUIController> voiceCtrlList;
    private bool isInVoiceTab;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_UpdateViewInLife;
    private static DelegateBridge __Hotfix_UpdateHeroLifeList;
    private static DelegateBridge __Hotfix_UpdateHeroVoiceList;
    private static DelegateBridge __Hotfix_UpdateToggleNewTag;
    private static DelegateBridge __Hotfix_OnPeofermanceVoiceButtonClick;
    private static DelegateBridge __Hotfix_OnToggleLifeValueChange;
    private static DelegateBridge __Hotfix_OnToggleVoiceValueChange;
    private static DelegateBridge __Hotfix_SetCommonUIState;
    private static DelegateBridge __Hotfix_ResetScrollViewPosition;
    private static DelegateBridge __Hotfix_add_EventOnVoiceItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnVoiceItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailLifeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInLife(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroLifeList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroVoiceList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateToggleNewTag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeofermanceVoiceButtonClick(FettersInformationVoiceItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleLifeValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleVoiceValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnVoiceItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
