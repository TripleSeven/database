﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GetVoiceContentNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class GetVoiceContentNetTask : UINetTask
  {
    private ulong m_instanceId;
    private ChatChannel m_channel;
    private ChatVoiceMessage m_voiceContent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnGetVoiceContentAck;
    private static DelegateBridge __Hotfix_get_VoiceContent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GetVoiceContentNetTask(ChatChannel channel, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetVoiceContentAck(ChatVoiceMessage voiceContent)
    {
      // ISSUE: unable to decompile the method.
    }

    public ChatVoiceMessage VoiceContent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
