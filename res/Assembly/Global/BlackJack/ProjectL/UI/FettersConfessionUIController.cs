﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersConfessionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersConfessionUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./SkillPanel/Skill5Group", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillGroupContent;
    [AutoBind("./SkillPanel/CenterEvent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_centerHeartStateCtrl;
    [AutoBind("./SkillPanel/CenterEvent", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_confessionHeartButton;
    [AutoBind("./SkillPanel/CenterEvent/SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionHeartButtonSelectImage;
    [AutoBind("./SkillPanel/CenterEvent/Female", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_confessionHeartFemaleImage;
    [AutoBind("./SkillPanel/CenterEvent/Male", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_confessionHeartMaleImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/DetailAlphaTweenState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_detailPanelTweenStateStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillDetailInfoIconImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/LockIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailInfoLockIconGo;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailInfoSkillNameText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/LvGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailInfoSkillLvText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/SkillInfoGroup/LvGroup/Max", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailInfoSkillLvMaxText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/FetterEffect/UnlockInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailDescText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailCondition1StateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConditionUnlockInfoText1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailCondition2StateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/ConditionGroup/UnlockCondition/ConditionGroup/UnlockInfo2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConditionUnlockInfoText2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/ItemDummy/RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockRewardGoods;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailUnlockButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/Button", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockButtonStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/Button/Grey", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailUnlockGreyButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/LockAndUnlockState/UnlockReward/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockRewardNameText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/NowInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockStateNowInfoText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/AfterText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockStateAfterText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/ConsumeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockStateConsumeTextStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/ConsumeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailUnlockStateConsumeValueText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockEvolutionMaterial1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockEvolutionMaterial2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockEvolutionMaterialStateCtrl1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionMaterial2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockEvolutionMaterialStateCtrl2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailUnlockStateEvolutionButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/PromoteState/AfterInfoPanel/EvolutionButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailUnlockStateEvolutionButtonStateController;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/EnhanceSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailEnhanceSuccessEffectPanelStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDetailConfessionAndRewardStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/ConditionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailConfessionAndRewardScrollViewContent;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Female/RewardGroup/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailConfessionAndRewardFemaleButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Female/RewardGroup/ItemDummy/RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailConfessionAndRewardFemaleRewardGoods;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Female/RewardGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConfessionAndRewardFemaleRewardGoodsNameText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Male/RewardGroup/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillDetailConfessionAndRewardMaleButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Male/RewardGroup/ItemDummy/RewardGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailConfessionAndRewardMaleRewardGoods;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/ConfessionAndReward/Male/RewardGroup/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDetailConfessionAndRewardMaleRewardGoodsNameText;
    [AutoBind("./Prefab/Condition", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailUnlockStateConditionPrefab;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heartFetterSexInfoStateCtrl;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/SkillLevelGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heartFetterLvText;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/SkillLevelGroup/Max", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterMaxLvTextGo;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Male", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heartFetterLvMaleHeartImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Female", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heartFetterLvFemaleHeartImage;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_heartFetterLvProgressSlider;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Skill1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterSkill1;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Skill2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterSkill2;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/HeartInfoGroup/Skill3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heartFetterSkill3;
    [AutoBind("./Margin/SkillDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heartFetterSkillDescPanelStateCtrl;
    [AutoBind("./Margin/SkillDescPanel/Detail/SkillIcon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heartFetterSkillDescPanelIconImage;
    [AutoBind("./Margin/SkillDescPanel/Detail/DescInfoScrollView/Viewport/DescInfo/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heartFetterSkillDescPanelDescText;
    [AutoBind("./ConditionPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterConditionPanelStateCtrl;
    [AutoBind("./ConditionPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockHeartFetterConditionPanelBGButton;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterCondition1StateCtrl;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition1/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition1Text;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition1/NowLevelText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition1LvText;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterCondition2StateCtrl;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition2/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition2Text;
    [AutoBind("./ConditionPanel/Detail/FrameImage/ConditionGroup/Condition2/NowLevelText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterCondition2LvText;
    [AutoBind("./SkillInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockHeartFetterSkillInfoPanelStateCtrl;
    [AutoBind("./SkillInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockHeartFetterSkillInfoPanelBGButton;
    [AutoBind("./SkillInfoPanel/Detail/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_unlockHeartFetterSkillInfoPanelIconImage;
    [AutoBind("./SkillInfoPanel/Detail/DescTextScrollView/Mask/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockHeartFetterSkillInfoPanelDescText;
    [AutoBind("./PlayerResource/Crystal/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerResourceCrystalText;
    [AutoBind("./PlayerResource/Crystal/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerResourceCrystalAddButton;
    [AutoBind("./PlayerResource/Golden/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerResourceGoldenText;
    [AutoBind("./PlayerResource/Golden/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerResourceGoldenAddButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/FastLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastLevelButton;
    [AutoBind("./Margin/SpiritEventInfoPanel/Detail/FastLevelButton/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_fastLevelButtonInputField;
    private Hero m_hero;
    private bool isFirstIn;
    private bool m_isMale;
    private int m_lastHeroHeartFetterLevel;
    private bool m_isDetailPanelInHeartState;
    private FettersConfessionSkillItemUIController m_curFetterSkillCtrl;
    private List<HeartFettersSkillItemUIController> m_heartFettersSkillItemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private HeartFettersSkillItemUIController m_curClickHeartFettersSkillItemUICtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_UpdatePlayerResource;
    private static DelegateBridge __Hotfix_UpdateViewInFettersConfession;
    private static DelegateBridge __Hotfix_SetCenterHeartFetterInfo;
    private static DelegateBridge __Hotfix_SetSkillDetailPanel;
    private static DelegateBridge __Hotfix_SetSkillInfoGroup;
    private static DelegateBridge __Hotfix_SetLockStateInfo;
    private static DelegateBridge __Hotfix_GetFettersCoditionDesc;
    private static DelegateBridge __Hotfix_SetUnlockStateInfo;
    private static DelegateBridge __Hotfix_OnFettersSkillItemClick;
    private static DelegateBridge __Hotfix_OnConfessionHeartButtonClick;
    private static DelegateBridge __Hotfix_OnUnlockCenterHeartFetterFinish;
    private static DelegateBridge __Hotfix_SetHeartFetterDetailPanel;
    private static DelegateBridge __Hotfix_OnHeartFetterSkillItemClick;
    private static DelegateBridge __Hotfix_SetUnlockHeartFetterConditionPanel;
    private static DelegateBridge __Hotfix_CloseHeartFetterUnlockConditionPanel;
    private static DelegateBridge __Hotfix_ShowHeartFetterSkillInfoPanel;
    private static DelegateBridge __Hotfix_CloseHeartFetterSkillInfoPanel;
    private static DelegateBridge __Hotfix_OnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_OnSkillUnlockButtonClick;
    private static DelegateBridge __Hotfix_OnSkillDetailUnlockGreyButtonClick;
    private static DelegateBridge __Hotfix_OnUnlockStateEvolutionButtonClcik;
    private static DelegateBridge __Hotfix_OnEvolutionHeartFetterFinished;
    private static DelegateBridge __Hotfix_ShowHeartUpdatePerformance;
    private static DelegateBridge __Hotfix_OnConfessionAndRewardButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnFastLevelButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_OnCrystalAddButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnGoldAdd;
    private static DelegateBridge __Hotfix_remove_EventOnGoldAdd;
    private static DelegateBridge __Hotfix_add_EventOnAddCrystal;
    private static DelegateBridge __Hotfix_remove_EventOnAddCrystal;
    private static DelegateBridge __Hotfix_add_EventOnHeroFetterConfess;
    private static DelegateBridge __Hotfix_remove_EventOnHeroFetterConfess;
    private static DelegateBridge __Hotfix_add_EventOnEvolutionFetterSkill;
    private static DelegateBridge __Hotfix_remove_EventOnEvolutionFetterSkill;
    private static DelegateBridge __Hotfix_add_EventOnEvolutionHeartFetter;
    private static DelegateBridge __Hotfix_remove_EventOnEvolutionHeartFetter;
    private static DelegateBridge __Hotfix_add_EventOnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_remove_EventOnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_add_EventOnSkillUnlockButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSkillUnlockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnUnlockCenterHeartFetter;
    private static DelegateBridge __Hotfix_remove_EventOnUnlockCenterHeartFetter;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersConfessionUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersConfession(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCenterHeartFetterInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillDetailPanel(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillInfoGroup(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLockStateInfo(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetFettersCoditionDesc(HeroFetterCompletionCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUnlockStateInfo(ConfigDataHeroFetterInfo heroFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFettersSkillItemClick(FettersConfessionSkillItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionHeartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockCenterHeartFetterFinish()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeartFetterDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeartFetterSkillItemClick(List<int> skillIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUnlockHeartFetterConditionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeartFetterUnlockConditionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeartFetterSkillInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeartFetterSkillInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEvolutionMaterialClick(GoodsType goodsType, int id, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillUnlockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillDetailUnlockGreyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockStateEvolutionButtonClcik()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEvolutionHeartFetterFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowHeartUpdatePerformance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionAndRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastLevelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCrystalAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGoldAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroFetterConfess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnEvolutionFetterSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnEvolutionHeartFetter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnEvolutionMaterialClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action<List<Goods>>> EventOnSkillUnlockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnUnlockCenterHeartFetter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
