﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScreenFade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ScreenFade
  {
    private float m_curTime;
    private float m_duration;
    private int m_delayFrame;
    private bool m_isFadeIn;
    private bool m_isEnd;
    private Color m_color;
    private Action m_onEnd;
    private Image m_image;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Setup;
    private static DelegateBridge __Hotfix_FadeIn;
    private static DelegateBridge __Hotfix_FadeOut;
    private static DelegateBridge __Hotfix_IsFading;
    private static DelegateBridge __Hotfix_Update;

    [MethodImpl((MethodImplOptions) 32768)]
    public ScreenFade()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
