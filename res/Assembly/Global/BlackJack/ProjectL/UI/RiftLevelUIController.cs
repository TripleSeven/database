﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RiftLevelUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ChapterBG", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chapterBGObject;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./WorldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_worldButton;
    [AutoBind("./Hard", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_hardUIStateController;
    [AutoBind("./Hard/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_hardButton;
    [AutoBind("./Hard/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_hardButtonUIStateController;
    [AutoBind("./Margin/StarReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton1GameObject;
    [AutoBind("./Margin/StarReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton2GameObject;
    [AutoBind("./Margin/StarReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton3GameObject;
    [AutoBind("./Margin/StarReward/Got/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_starRewardGotText;
    [AutoBind("./Margin/StarReward/ProgressBar/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressBarImage;
    [AutoBind("./StarRewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_starRewardPreviewUIStateController;
    [AutoBind("./StarRewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_starRewardPreviewBackgroundButton;
    [AutoBind("./StarRewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_starRewardPreviewScrollRect;
    [AutoBind("./UnlockCondition", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockConditionUIStateController;
    [AutoBind("./UnlockCondition/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockConditionBackgroundButton;
    [AutoBind("./UnlockCondition/Panel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlockConditionGroupGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RiftLevelUnlockConditionItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_riftLevelUnlockConditionItemPrefab;
    [AutoBind("./Prefabs/RiftLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_riftLevelButtonPrefab;
    [AutoBind("./Prefabs/EventRiftLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventRiftLevelButtonPrefab;
    [AutoBind("./Prefabs/ElitePoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventRiftLevelDummyPrefab;
    private List<RiftLevelButton> m_riftLevelButtons;
    private List<EventRiftLevelButton> m_eventRiftLevelButtons;
    private List<RiftLevelUnlockConditionItemUIController> m_riftLevelUnlockConditionItems;
    private GameObject m_createChapterBGObject;
    private GainRewardButton[] m_starRewardButtons;
    private ConfigDataRiftChapterInfo m_chapterInfo;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetChapter;
    private static DelegateBridge __Hotfix_SetStarRewardStatus;
    private static DelegateBridge __Hotfix_SetStarRewardProress;
    private static DelegateBridge __Hotfix_ClearRiftLevelButtons;
    private static DelegateBridge __Hotfix_ClearEventRiftLevelButtons;
    private static DelegateBridge __Hotfix_AddRiftLevelButton;
    private static DelegateBridge __Hotfix_AddEventRiftLevelButton;
    private static DelegateBridge __Hotfix_AddEventRiftLevelDummys;
    private static DelegateBridge __Hotfix_ClearRiftLevelUnlockContitionItems;
    private static DelegateBridge __Hotfix_AddRiftLevelUnlockContitionItems;
    private static DelegateBridge __Hotfix_GoToRiftLevel;
    private static DelegateBridge __Hotfix_Co_ShowRiftLevel;
    private static DelegateBridge __Hotfix_AddStarRewardPreviewGoods;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnWorldButtonClick;
    private static DelegateBridge __Hotfix_OnHardButtonClick;
    private static DelegateBridge __Hotfix_OnStarRewardButtonClick;
    private static DelegateBridge __Hotfix_OnRiftLevelButtonClick;
    private static DelegateBridge __Hotfix_OnEventRiftLevelButtonClick;
    private static DelegateBridge __Hotfix_OnUnlockConditionBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnStarRewardPreviewBackgroundButtonClick;
    private static DelegateBridge __Hotfix_SetUnlockConditionClose;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnReturnToWorld;
    private static DelegateBridge __Hotfix_remove_EventOnReturnToWorld;
    private static DelegateBridge __Hotfix_add_EventOnChangeHard;
    private static DelegateBridge __Hotfix_remove_EventOnChangeHard;
    private static DelegateBridge __Hotfix_add_EventOnSelectRiftLevel;
    private static DelegateBridge __Hotfix_remove_EventOnSelectRiftLevel;
    private static DelegateBridge __Hotfix_add_EventOnGetStarReward;
    private static DelegateBridge __Hotfix_remove_EventOnGetStarReward;
    private static DelegateBridge __Hotfix_add_EventOnGoToScenario;
    private static DelegateBridge __Hotfix_remove_EventOnGoToScenario;
    private static DelegateBridge __Hotfix_add_EventOnSelectChapter;
    private static DelegateBridge __Hotfix_remove_EventOnSelectChapter;

    [MethodImpl((MethodImplOptions) 32768)]
    private RiftLevelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapter(ConfigDataRiftChapterInfo chapterInfo, bool canSwitchHard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStarRewardStatus(int idx, GainRewardStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStarRewardProress(int star, int allStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRiftLevelButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEventRiftLevelButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRiftLevelButton(
      ConfigDataRiftLevelInfo riftLevelInfo,
      RiftLevelStatus status,
      int challengeCount,
      int challengeCountMax,
      bool challenged,
      int star,
      int achievementCount,
      int treasureCount,
      int treasureCountMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEventRiftLevelButton(
      ConfigDataRiftLevelInfo riftLevelInfo,
      RiftLevelStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEventRiftLevelDummys()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRiftLevelUnlockContitionItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddRiftLevelUnlockContitionItems(ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoToRiftLevel(int chapterID, int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowRiftLevel(
      ConfigDataRiftChapterInfo riftChapterInfo,
      ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddStarRewardPreviewGoods(ConfigDataRiftChapterInfo chapterInfo, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWorldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRiftLevelButtonClick(RiftLevelButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventRiftLevelButtonClick(EventRiftLevelButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockConditionBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnlockConditionClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturnToWorld
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChangeHard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftLevelInfo, NeedGoods> EventOnSelectRiftLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetStarReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftChapterInfo> EventOnSelectChapter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
