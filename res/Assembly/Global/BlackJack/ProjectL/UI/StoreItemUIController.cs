﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class StoreItemUIController : UIControllerBase
  {
    private PDSDKGoodType m_goodPDSDKType;
    private const string PriceState_OtherItem = "OtherItem";
    private const string PriceState_Crystal = "Crystal";
    private const string PriceState_RMBIcon = "RMB";
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storeItemButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemUIStateController;
    [AutoBind("./Item/GeneralGoods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_generalGoodObj;
    [AutoBind("./Item/GeneralGoods/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_generalGoodIconImage;
    [AutoBind("./Item/GeneralGoods/FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_generalGoodFrameImage;
    [AutoBind("./Item/GeneralGoods/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ssrEffectImage;
    [AutoBind("./Item/GeneralGoods/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_generalGoodCountText;
    [AutoBind("./Item/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_spineAnimObj;
    [AutoBind("./Item/Item", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noFrameItemObj;
    [AutoBind("./Item/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_noFrameItemIconImage;
    [AutoBind("./Item/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rechargeItemObj;
    [AutoBind("./Item/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rechargeItemIconImage;
    [AutoBind("./LimitPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_limitObj;
    [AutoBind("./LimitPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_limitText;
    [AutoBind("./DisablePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableBuyObj;
    [AutoBind("./DisablePanel/SoldOut&RefreshTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableBuyRefreshObj;
    [AutoBind("./DisablePanel/SoldOut&RefreshTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshOrRemoveTimeText;
    [AutoBind("./DisablePanel/OnlySoldOut", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableBuyOnlyObj;
    [AutoBind("./BoughtPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boughtObj;
    [AutoBind("./ExtraPresentPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_extraPresentGameObj;
    [AutoBind("./ExtraPresentPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_extraPresentText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./PresentedPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentedPanelObj;
    [AutoBind("./PresentedPanel/RemoveTimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_presentedText;
    [AutoBind("./PricePanel/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_priceIconImage;
    [AutoBind("./PricePanel/CrystalIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_priceCrystalImage;
    [AutoBind("./PricePanel/RMBIcon ", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_priceRMBImage;
    [AutoBind("./PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_priceText;
    [AutoBind("./PricePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_pricePanelStateCtrl;
    [AutoBind("./LabelPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_labelObj;
    [AutoBind("./LabelPanel/CommendLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commendLabelObj;
    [AutoBind("./LabelPanel/DiscountLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_discountLabelObj;
    [AutoBind("./LabelPanel/FirstDiscountLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstDiscountLabelObj;
    [AutoBind("./LabelPanel/LimitTimeLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_limitTimeLabelObj;
    [AutoBind("./LabelPanel/FirstLabel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstLabelObj;
    [AutoBind("./BestValue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_isBestValueObj;
    [AutoBind("./Tape", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_LeftDayGameobj;
    [AutoBind("./Tape/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_LeftDayText;
    [AutoBind("./Exclusive", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_IOSSpecificGo;
    [AutoBind("./Tape/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_LeftTitleGameobj;
    private string m_countString;
    private bool m_isSSR;
    private DateTime m_giftStoreItemEndTime;
    private int m_nowSeconds;
    public StoreType m_storeType;
    public GoodsType m_goodsType;
    public int m_fixedStoreItemGoodsID;
    public int m_goodsID;
    public int m_index;
    public bool m_haveFirstBuyReward;
    public GoodsType m_extraCurrencyType;
    public GoodsType m_currentGoodCurrencyType;
    public string m_extraCurrencyTypeIconString;
    public int m_extraCurrencyCount;
    public GiftStoreItem m_giftStoreItem;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private ClientConfigDataLoader m_clientConfigDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnStoreItemClick;
    private static DelegateBridge __Hotfix_SetFixedStoreItemInfo;
    private static DelegateBridge __Hotfix_SetRandomStoreItemInfo;
    private static DelegateBridge __Hotfix_SetRechargeStoreItemInfo;
    private static DelegateBridge __Hotfix_SetGiftStoreItemInfo;
    private static DelegateBridge __Hotfix_SetGiftStoreMothCardItemInfo;
    private static DelegateBridge __Hotfix_ShowIOSPlatformSpecificGo;
    private static DelegateBridge __Hotfix_SetLabel_1;
    private static DelegateBridge __Hotfix_SetLabel_RMB_Recharge;
    private static DelegateBridge __Hotfix_SetLabel_0;
    private static DelegateBridge __Hotfix_SetPrice_1;
    private static DelegateBridge __Hotfix_SetPrice_0;
    private static DelegateBridge __Hotfix_SetPrice_RMB;
    private static DelegateBridge __Hotfix_SetLimit_1;
    private static DelegateBridge __Hotfix_SetLimit_2;
    private static DelegateBridge __Hotfix_HaveFirstBuyReward;
    private static DelegateBridge __Hotfix_SetLimit_0;
    private static DelegateBridge __Hotfix_SetLimit_RMB;
    private static DelegateBridge __Hotfix_SetBought;
    private static DelegateBridge __Hotfix_SetSellOut_1;
    private static DelegateBridge __Hotfix_SetSellOut_3;
    private static DelegateBridge __Hotfix_SetSellOut_2;
    private static DelegateBridge __Hotfix_SetSellOut_0;
    private static DelegateBridge __Hotfix_SetRemoveTime;
    private static DelegateBridge __Hotfix_UpdateIcon;
    private static DelegateBridge __Hotfix_UpdateIcon_RMB;
    private static DelegateBridge __Hotfix_SetNoFrameInfo;
    private static DelegateBridge __Hotfix_SetGeneralGoodInfo;
    private static DelegateBridge __Hotfix_GetFrameNameAndSetSSR;
    private static DelegateBridge __Hotfix_SetRandomStoreItemNotShow;
    private static DelegateBridge __Hotfix_SetLeftDays;
    private static DelegateBridge __Hotfix_SetItemDefaultState;
    private static DelegateBridge __Hotfix_SetIsBestValue;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_UpdateRemoveTime;
    private static DelegateBridge __Hotfix_get_Count;
    private static DelegateBridge __Hotfix_set_Count;
    private static DelegateBridge __Hotfix_get_PriceIcon;
    private static DelegateBridge __Hotfix_set_PriceIcon;
    private static DelegateBridge __Hotfix_get_PriceText;
    private static DelegateBridge __Hotfix_set_PriceText;
    private static DelegateBridge __Hotfix_get_ItemNameText;
    private static DelegateBridge __Hotfix_set_ItemNameText;
    private static DelegateBridge __Hotfix_get_LimitText;
    private static DelegateBridge __Hotfix_set_LimitText;
    private static DelegateBridge __Hotfix_get_GoodPDSDKType;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStoreItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFixedStoreItemInfo(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomStoreItemInfo(RandomStoreItem item, int itemIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRechargeStoreItemInfo(
      int goodsId,
      StoreType storeType,
      bool isGoodsBought,
      double goodsPrice,
      string goodsName,
      int cystalRewardNums,
      string icon,
      bool isBestValue,
      string goodsDisCurrency = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreItemInfo(
      GiftStoreItem item,
      double price,
      PDSDKGoodType goodPDSDKType,
      string goodsDisCurrency = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGiftStoreMothCardItemInfo(
      GiftStoreItem item,
      double price,
      PDSDKGoodType goodPDSDKType,
      string goodsDisCurrency = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowIOSPlatformSpecificGo(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLabel(ConfigDataFixedStoreItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLabel_RMB_Recharge(bool isGoodsBought)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLabel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPrice(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPrice(ConfigDataRandomStoreItemInfo itemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPrice_RMB(double goodPrice, string goodsDisCurrency)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit(FixedStoreItem item, ConfigDataFixedStoreItemInfo fixedStoreItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit(GiftStoreItem item, ConfigDataGiftStoreItemInfo giftStoreItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HaveFirstBuyReward(ConfigDataFixedStoreItemInfo fixedStoreItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLimit_RMB(bool isGoodsBought, int crystalNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBought(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut(FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut(RandomStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSellOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRemoveTime(DateTime showEndTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateIcon(GoodsType goodType, int itemId, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateIcon_RMB(string icon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNoFrameInfo(GoodsType goodType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGeneralGoodInfo(int count = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetFrameNameAndSetSSR()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRandomStoreItemNotShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftDays(string remainTime = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemDefaultState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetIsBestValue(bool isBestValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemoveTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Count
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Image PriceIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Text PriceText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Text ItemNameText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Text LimitText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PDSDKGoodType GoodPDSDKType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<StoreItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
