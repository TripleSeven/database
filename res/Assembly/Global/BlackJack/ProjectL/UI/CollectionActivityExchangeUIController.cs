﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityExchangeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityExchangeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_openServiceActivityStateCtrl;
    [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImage;
    [AutoBind("./State", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_styleStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ItemList/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendButton;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Detail/TitleToggleGroup/ViewPort/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pageContent;
    [AutoBind("./Detail/QuestGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_missionsScrollRect;
    [AutoBind("./Detail/QuestGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionsScrollViewContent;
    [AutoBind("./Prafabs", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_prefabPool;
    [AutoBind("./Detail/ResidueTimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_residueTimeValueText;
    [AutoBind("./ItemList/ItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyRoot;
    [AutoBind("./Detail/RightTop/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    private IConfigDataLoader m_configDataLoader;
    private List<CollectionActivityExchangePageUIController> m_pageCtrlList;
    private List<CollectionActivityExchangeMissionItemUIController> m_missionItemCtrlList;
    private List<CollectionActivityExchangeCurrencyItemUIController> m_currencyItemCtrlList;
    private HeroCharUIController m_heroCharUIController;
    private int m_selectedPageIndex;
    private string m_showingBgResPath;
    private List<CollectionActivityPage> m_cachedPageList;
    private List<CollectionActivityCurrency> m_cachedResourceList;
    private const string c_pageItemPoolName = "DayTabItem";
    private const string c_consumeItemPoolName = "ConsumeItem";
    private const string c_missionItemPoolName = "MissionItem";
    private const string c_currencyItemPoolName = "ResourceItem";
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_InitPrefabPools;
    private static DelegateBridge __Hotfix_OnObjectCreated;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_ResetView;
    private static DelegateBridge __Hotfix_ResetPageSelect;
    private static DelegateBridge __Hotfix_OnPageButtonClick;
    private static DelegateBridge __Hotfix_SelectPage;
    private static DelegateBridge __Hotfix_SetPageCtrlSelected;
    private static DelegateBridge __Hotfix_GetPageCtrl;
    private static DelegateBridge __Hotfix_UpdatePages;
    private static DelegateBridge __Hotfix_UpdateCoverHero;
    private static DelegateBridge __Hotfix_UpdateMissionItems;
    private static DelegateBridge __Hotfix_UpdateCurrencyItems;
    private static DelegateBridge __Hotfix_OnMissionGetFromConsumeItemPool;
    private static DelegateBridge __Hotfix_OnMissionReturnFromConsumeItemPool;
    private static DelegateBridge __Hotfix_OnCheckPageCanClick;
    private static DelegateBridge __Hotfix_OnMissionGetButtonClick;
    private static DelegateBridge __Hotfix_SetResidueTime;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnRecommendButtonClicked;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnRecommendButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRecommendButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnMissionGet;
    private static DelegateBridge __Hotfix_remove_EventOnMissionGet;
    private static DelegateBridge __Hotfix_add_FuncOnCheckPageCanClick;
    private static DelegateBridge __Hotfix_remove_FuncOnCheckPageCanClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPrefabPools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnObjectCreated(string poolName, GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(CollectionActivityExchangeModel model)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetPageSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPageButtonClick(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectPage(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPageCtrlSelected(int index, bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityExchangePageUIController GetPageCtrl(
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePages()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCoverHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionItems(bool needResetScroll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrencyItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionAcitivityExchangeConsumeItemUIController OnMissionGetFromConsumeItemPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionReturnFromConsumeItemPool(
      CollectionAcitivityExchangeConsumeItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool OnCheckPageCanClick(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionGetButtonClick(
      CollectionActivityExchangeMissionItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetResidueTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRecommendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionActivityExchangeMissionItemUIController> EventOnMissionGet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<int, bool> FuncOnCheckPageCanClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
