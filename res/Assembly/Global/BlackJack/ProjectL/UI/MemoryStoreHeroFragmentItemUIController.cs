﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MemoryStoreHeroFragmentItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MemoryStoreHeroFragmentItemUIController : UIControllerBase
  {
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImg;
    [AutoBind("./NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_numberText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    private HeroFragmentBagItem m_heroFragementBagItem;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitHeroFragmentItem;
    private static DelegateBridge __Hotfix_OnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroFragmentItem(HeroFragmentBagItem heroFragementBagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
