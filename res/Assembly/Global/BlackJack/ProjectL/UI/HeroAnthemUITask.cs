﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroAnthemUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private HeroAnthemBackground3DLayerCtrl m_heroAnthemBackground3DLayerCtrl;
    private HeroAnthemUIController m_heroAnthemUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectHeroAnthemLevelAcievementsAssets;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_InitHeroAnthemBackground3DLayerCtrl;
    private static DelegateBridge __Hotfix_InitHeroAnthemUIController;
    private static DelegateBridge __Hotfix_UninitHeroAnthemUIController;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_HeroAnthemUIController_OnReturn;
    private static DelegateBridge __Hotfix_HeroAnthemUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_HeroAnthemUIController_OnGotoHeroAnthemChapter;
    private static DelegateBridge __Hotfix_HeroAnthemLevelsUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_HeroAnthemUIController_OnRankButtonClick;
    private static DelegateBridge __Hotfix_GetHeadIconAssets;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAnthemUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAnthemLevelAcievementsAssets(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitHeroAnthemBackground3DLayerCtrl()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitHeroAnthemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitHeroAnthemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnGotoHeroAnthemChapter(ConfigDataHeroAnthemInfo anthemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemLevelsUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUIController_OnRankButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<string> GetHeadIconAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
