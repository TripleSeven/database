﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SummonToggleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SummonToggleUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./ClickIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_clickIconImage;
    [AutoBind("./UnClickIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_unClickIconImage;
    [AutoBind("./MainClickText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameClickText;
    [AutoBind("./MainUnClickText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameUnClickText;
    [AutoBind("./SecondClickText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeClickText;
    [AutoBind("./SecondUnClickText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeUnClickText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetSummonToggleInfo;
    private static DelegateBridge __Hotfix_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_SetToggleIsOn;
    private static DelegateBridge __Hotfix_add_EventOnToggleClick;
    private static DelegateBridge __Hotfix_remove_EventOnToggleClick;
    private static DelegateBridge __Hotfix_get_CardPool;
    private static DelegateBridge __Hotfix_set_CardPool;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSummonToggleInfo(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleIsOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SummonToggleUIController> EventOnToggleClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CardPool CardPool
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
