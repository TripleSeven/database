﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RealTimePVPWaitingForOpponentNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class RealTimePVPWaitingForOpponentNetTask : UINetTask
  {
    private RealTimePVPMode m_mode;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnRealTimePVPWaitingForOpponentAck;
    private static DelegateBridge __Hotfix_set_ExpectedWaitingTime;
    private static DelegateBridge __Hotfix_get_ExpectedWaitingTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPWaitingForOpponentNetTask(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRealTimePVPWaitingForOpponentAck(int result, int expectedWaitingTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ExpectedWaitingTime
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
