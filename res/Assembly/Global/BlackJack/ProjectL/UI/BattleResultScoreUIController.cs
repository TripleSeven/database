﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleResultScoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleResultScoreUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/ScoreGroup/Get", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_getScoreUIStateController;
    [AutoBind("./Panel/ScoreGroup/Get/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreText;
    [AutoBind("./Panel/ScoreGroup/Get/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreText2;
    [AutoBind("./Panel/ScoreGroup/Get/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getScoreDescText;
    [AutoBind("./Panel/ScoreGroup/Have/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveScoreText;
    [AutoBind("./Panel/RewardGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardGroupScrollRect;
    [AutoBind("./Panel/NextRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_nextRewardGroupGameObject;
    [AutoBind("./Panel/NextRewardGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nextRewardScoreText;
    [AutoBind("./Panel/NextRewardGroup/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_nextRewardGroupTransform;
    private bool m_isClick;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowBattleResultScore;
    private static DelegateBridge __Hotfix_Co_ShowBattleResultScore;
    private static DelegateBridge __Hotfix_SetUnchartedScoreReward;
    private static DelegateBridge __Hotfix_SetGuildMassiveCombatScoreReward;
    private static DelegateBridge __Hotfix_SetCollectionActivityScoreReward;
    private static DelegateBridge __Hotfix_SetReward;
    private static DelegateBridge __Hotfix_Co_SetAndWaitUIState;
    private static DelegateBridge __Hotfix_Co_WaitClick;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    private BattleResultScoreUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleResultScore(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowBattleResultScore(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUnchartedScoreReward(
      BattleReward battleReward,
      ConfigDataUnchartedScoreInfo unchartedScoreInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGuildMassiveCombatScoreReward(
      BattleReward battleReward,
      ConfigDataGuildMassiveCombatLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCollectionActivityScoreReward(
      BattleReward battleReward,
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetReward(
      BattleReward battleReward,
      int curScore,
      int nextScore,
      List<Goods> nextRewardGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
