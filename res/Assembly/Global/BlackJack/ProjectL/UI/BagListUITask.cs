﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BagListUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class BagListUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public string BagMode;
    public string AlchemyMode;
    private string m_curMode;
    private BagListUIController m_bagListUIController;
    private AlchemyUIController m_alchemyUIController;
    private int m_curLayerDescIndex;
    private bool m_isNeedShowFadeIn;
    private BagListUIController.DisplayType m_displayType;
    private ulong m_clickBagItemInstanceId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_SaveUIStateToIntent;
    private static DelegateBridge __Hotfix_GetUIStateFromIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PushAndPopLayerByState;
    private static DelegateBridge __Hotfix_BagListUIController_OnReturn;
    private static DelegateBridge __Hotfix_BagListUIController_OnAddAllItem;
    private static DelegateBridge __Hotfix_BagListUIController_OnSpeedUp;
    private static DelegateBridge __Hotfix_BagListUIController_OnAddAllEquipment;
    private static DelegateBridge __Hotfix_BagListUIController_OnAddItem;
    private static DelegateBridge __Hotfix_BagListUIController_OnClearBag;
    private static DelegateBridge __Hotfix_BagListUIController_OnUseItem;
    private static DelegateBridge __Hotfix_ShowRewardGoodsUITask;
    private static DelegateBridge __Hotfix_BagListUIController_OnSellItem;
    private static DelegateBridge __Hotfix_BagListUIController_OnGotoEquipmentForge;
    private static DelegateBridge __Hotfix_EquipmentForgeUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_BagListUIController_OnAlchemyButtonClick;
    private static DelegateBridge __Hotfix_BagListUIController_OnLockButtonClick;
    private static DelegateBridge __Hotfix_BagListUIController_OnShareButtonClick;
    private static DelegateBridge __Hotfix_Bag_OnShowGetPath;
    private static DelegateBridge __Hotfix_Bag_OnGotoGetPath;
    private static DelegateBridge __Hotfix_AlchemyUIController_OnReturn;
    private static DelegateBridge __Hotfix_AlchemyUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_AlchemyUIController_OnAlchemyButtonClick;
    private static DelegateBridge __Hotfix_AlchemyUIController_OnLockButtonClick;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagListUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveUIStateToIntent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUIStateFromIntent(UIIntent uiIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushAndPopLayerByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAddAllItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnSpeedUp(
      int slot,
      ulong equipmentInstanceId,
      BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAddAllEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAddItem(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnClearBag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnUseItem(
      GoodsType type,
      int id,
      int count,
      BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRewardGoodsUITask(int itemId, int count, List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnSellItem(ulong instanceId, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnGotoEquipmentForge(
      int slot,
      ulong equipmentInstanceId,
      BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnLockButtonClick(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagListUIController_OnShareButtonClick(ulong instanceId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Bag_OnShowGetPath(BagItemBase goods, BagListUIController.DisplayType displayType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Bag_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnAlchemyButtonClick(
      List<ProGoods> proGoods,
      Action<List<Goods>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AlchemyUIController_OnLockButtonClick(ulong instanceId, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
