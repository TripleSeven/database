﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedSkinBoxItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SelfSelectedSkinBoxItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemAnimation;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./SelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectButton;
    [AutoBind("./CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public ConfigDataHeroSkinInfo m_heroSkinInfo;
    private bool m_isSelect;
    private bool m_hasOwnHeroSkin;
    private SelfSelectedSkinBoxUITask m_selfSelectedSkinBoxUITask;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetViewMode;
    private static DelegateBridge __Hotfix_ToggleSelect;
    private static DelegateBridge __Hotfix_HasSkinOwn;
    private static DelegateBridge __Hotfix_HasSkinSelect;
    private static DelegateBridge __Hotfix_SetItemNoneState;
    private static DelegateBridge __Hotfix_SetItemSelectState;
    private static DelegateBridge __Hotfix_OnSelectClick;
    private static DelegateBridge __Hotfix_OnCancelClick;
    private static DelegateBridge __Hotfix_OnItemToggleSelect;
    private static DelegateBridge __Hotfix_add_EventOnSkinItemStateChange;
    private static DelegateBridge __Hotfix_remove_EventOnSkinItemStateChange;
    private static DelegateBridge __Hotfix_add_EventOnItemToggleSelect;
    private static DelegateBridge __Hotfix_remove_EventOnItemToggleSelect;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      SelfSelectedSkinBoxUITask selfSelectedSkinBoxUITask,
      int heroSkinInfoID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetViewMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ToggleSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSkinOwn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSkinSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemNoneState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemSelectState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnSkinItemStateChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnItemToggleSelect
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
