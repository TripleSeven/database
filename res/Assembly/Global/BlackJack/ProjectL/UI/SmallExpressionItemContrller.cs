﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SmallExpressionItemContrller
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SmallExpressionItemContrller : UIControllerBase
  {
    public int m_key;
    public string m_assetPath;
    private Image m_icon;
    private Button m_button;
    private static DelegateBridge __Hotfix_CollectExpressionAsset;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_RegisterUIEvent;
    private static DelegateBridge __Hotfix_SetExpressionIcon;
    private static DelegateBridge __Hotfix_OnExpressionClick;
    private static DelegateBridge __Hotfix_add_EventOnExpressionClick;
    private static DelegateBridge __Hotfix_remove_EventOnExpressionClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CollectExpressionAsset()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterUIEvent(Action<SmallExpressionItemContrller> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExpressionIcon(Sprite sprite)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SmallExpressionItemContrller> EventOnExpressionClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
