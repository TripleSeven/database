﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class UITask : UITaskBase
  {
    protected List<string> m_assets;
    private int m_loadingCustomResCorutineCount;
    protected bool m_isOpeningUI;
    protected bool m_isNeedRegisterPlayerContextEvents;
    protected bool m_isPlayerContextEventsRegistered;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_Base_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_Base_OnStop;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_Base_OnPause;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_Base_OnResume;
    private static DelegateBridge __Hotfix_OnNewIntent;
    private static DelegateBridge __Hotfix_EnableUIInput;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_Base_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_Base_PostUpdateView;
    private static DelegateBridge __Hotfix_CollectPreloadResourceList;
    private static DelegateBridge __Hotfix_ClearAssetList;
    private static DelegateBridge __Hotfix_CollectAsset;
    private static DelegateBridge __Hotfix_CollectSpriteAsset;
    private static DelegateBridge __Hotfix_CollectFxAsset;
    private static DelegateBridge __Hotfix_StartLoadCustomRes;
    private static DelegateBridge __Hotfix_StartLoadCustomAssets;
    private static DelegateBridge __Hotfix_IsLoadingCustomAssets;
    private static DelegateBridge __Hotfix_IsPipeLineRunning;
    private static DelegateBridge __Hotfix_IsOpeningUI;
    private static DelegateBridge __Hotfix_ReturnPrevUITask;
    private static DelegateBridge __Hotfix_ClearUnusedDynamicResourceCache;
    private static DelegateBridge __Hotfix_ClearDynamicResourceCache;
    private static DelegateBridge __Hotfix_OnMemoryWarning;

    [MethodImpl((MethodImplOptions) 32768)]
    public UITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Base_OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Base_OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void EnableUIInput(bool isEnable, bool? isGlobalEnable = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Base_IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Base_PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectPreloadResourceList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearAssetList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectSpriteAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void CollectFxAsset(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLoadCustomRes(List<string> resPathList, Action onLoadCompleted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartLoadCustomAssets(Action onLoadCompleted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsLoadingCustomAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPipeLineRunning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsOpeningUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected UITaskBase ReturnPrevUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearUnusedDynamicResourceCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ClearDynamicResourceCache(List<string> assets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
