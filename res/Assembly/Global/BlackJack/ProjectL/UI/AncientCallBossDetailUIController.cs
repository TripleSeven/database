﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallBossDetailUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/BossNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Detail/BossIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Detail/BossDescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailText;
    [AutoBind("./Detail/SkillFrame/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_skillScrollRect;
    [AutoBind("./Detail/FlowFrame/DescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_flowDescText;
    [AutoBind("./Detail/FlowFrame/DescScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_descScrollRect;
    [AutoBind("./Detail/HeroFrame/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroScrollRect;
    [AutoBind("./Detail/SkillItemDetailPanel", AutoBindAttribute.InitState.Active, false)]
    private GameObject m_skillDescPanel;
    [AutoBind("./Prefabs/HeroListDetil", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroDetailPrefab;
    [AutoBind("./Prefabs/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillItemPrefab;
    private ConfigDataAncientCallBossInfo m_bossInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_ShowAncientCallBossDetail;
    private static DelegateBridge __Hotfix_OnBossSkillClick;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnAttack;
    private static DelegateBridge __Hotfix_remove_EventOnAttack;
    private static DelegateBridge __Hotfix_add_EventOnShowBossDetail;
    private static DelegateBridge __Hotfix_remove_EventOnShowBossDetail;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAncientCallBossDetail(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossSkillClick(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBossDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
