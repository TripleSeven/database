﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArenaLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_arenaLevelUIStateCtrl;
    [AutoBind("./Low", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_lowUIStateCtrl;
    [AutoBind("./Low/ArenalLevel1_3", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_lowLevelUIStateCtrl;
    [AutoBind("./Low/ArenalLevel1_3/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_lowLevelIconImage;
    [AutoBind("./Low/ArenalLevel1_3/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lowLevelText;
    [AutoBind("./Low/ArenalLevel1_3/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lowRewardGroupGameObject;
    [AutoBind("./Low/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lowArenaPointText;
    [AutoBind("./Middle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_middleUIStateCtrl;
    [AutoBind("./Middle/ArenalLevel4_6", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_middleLevelUIStateCtrl;
    [AutoBind("./Middle/ArenalLevel4_6/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_middleLevelIconImage;
    [AutoBind("./Middle/ArenalLevel4_6/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_middleLevelText;
    [AutoBind("./Middle/ArenalLevel4_6/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_middleRewardGroupGameObject;
    [AutoBind("./Middle/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_middleArenaPointText;
    [AutoBind("./High", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_highUIStateCtrl;
    [AutoBind("./High/ArenalLevel7_9", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_highLevelUIStateCtrl;
    [AutoBind("./High/ArenalLevel7_9/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_highLevelIconImage;
    [AutoBind("./High/ArenalLevel7_9/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_highLevelText;
    [AutoBind("./High/ArenalLevel7_9/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_highRewardGroupGameObject;
    [AutoBind("./High/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_highArenaPointText;
    [AutoBind("./Top", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_topUIStateCtrl;
    [AutoBind("./Top/ArenalLevel10/LevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_topLevelIconImage;
    [AutoBind("./Top/ArenalLevel10/LevelIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_topLevelText;
    [AutoBind("./Top/ArenalLevel10/Reward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topRewardGroupGameObject;
    [AutoBind("./Top/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_topArenaPointText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetArenaLevelInfo;
    private static DelegateBridge __Hotfix_SetDanInfo_1;
    private static DelegateBridge __Hotfix_SetDanInfo_0;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaLevelInfo(
      ConfigDataArenaLevelInfo arenaLevelInfo,
      GameObject rewardGoodsPrefab,
      int playerArenaLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDanInfo(
      ConfigDataRealTimePVPDanInfo danInfo,
      GameObject rewardGoodsPrefab,
      int playerDanId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDanInfo(
      ConfigDataPeakArenaDanInfo danInfo,
      GameObject rewardGoodsPrefab,
      int playerDanId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
