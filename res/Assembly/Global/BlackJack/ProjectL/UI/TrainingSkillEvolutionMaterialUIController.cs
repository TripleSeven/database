﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TrainingSkillEvolutionMaterialUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TrainingSkillEvolutionMaterialUIController : UIControllerBase
  {
    [AutoBind("./ValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_materialCountStateCtrl;
    [AutoBind("./ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_materialHaveCount;
    [AutoBind("./ValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_materialNeedCount;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_materialIconImage;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_materialFrameImage;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_materialButton;
    private GoodsType m_goodsType;
    private int m_goodsId;
    private int m_needCount;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitTrainingSkillEvolutionMaterial;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTrainingSkillEvolutionMaterial(GoodsType goodsType, int goodsId, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GoodsType, int, int> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
