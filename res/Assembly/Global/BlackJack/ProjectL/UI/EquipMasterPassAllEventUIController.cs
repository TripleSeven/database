﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterPassAllEventUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterPassAllEventUIController : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
  {
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_PassClickEventDown;
    private static DelegateBridge __Hotfix_PassEventDown;

    public event Action<List<RaycastResult>, PointerEventData, Action<List<RaycastResult>, PointerEventData>> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PassClickEventDown(List<RaycastResult> rayCastResults, PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PassEventDown<T>(
      List<RaycastResult> results,
      PointerEventData data,
      ExecuteEvents.EventFunction<T> function)
      where T : IEventSystemHandler
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
