﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GetPathItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GetPathItemUIController : UIControllerBase
  {
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Times", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_timesStateCtrl;
    [AutoBind("./Times/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timesValue;
    [AutoBind("./Times/ALL", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timesAllValue;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetGetPath;
    private static DelegateBridge __Hotfix_OnGotoButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;
    private static DelegateBridge __Hotfix_set_GetPathInfo;
    private static DelegateBridge __Hotfix_get_GetPathInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGetPath(GetPathData getPathInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GetPathData> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GetPathData GetPathInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
