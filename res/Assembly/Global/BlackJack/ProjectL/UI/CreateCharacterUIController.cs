﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CreateCharacterUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CreateCharacterUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./CharacterName/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_characterNameInputField;
    [AutoBind("./CreateButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createButton;
    [AutoBind("./AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoButton;
    [AutoBind("./Message", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageGameObject;
    [AutoBind("./Message/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    private int m_characterNameLimit;
    private float m_hideMessageTime;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetCharacterNameLimit;
    private static DelegateBridge __Hotfix_SetCharacterName;
    private static DelegateBridge __Hotfix_GetCharacterName;
    private static DelegateBridge __Hotfix_ShowMessage_1;
    private static DelegateBridge __Hotfix_ShowMessage_0;
    private static DelegateBridge __Hotfix_HideMessage;
    private static DelegateBridge __Hotfix_OnCreateButtonClick;
    private static DelegateBridge __Hotfix_OnAutoButtonClick;
    private static DelegateBridge __Hotfix_CharacterNameInputField_OnEndEdit;
    private static DelegateBridge __Hotfix_add_EventOnCreate;
    private static DelegateBridge __Hotfix_remove_EventOnCreate;
    private static DelegateBridge __Hotfix_add_EventOnAutoName;
    private static DelegateBridge __Hotfix_remove_EventOnAutoName;

    private CreateCharacterUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharacterNameLimit(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharacterName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetCharacterName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(string text, int time = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, int time = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CharacterNameInputField_OnEndEdit(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnCreate
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAutoName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
