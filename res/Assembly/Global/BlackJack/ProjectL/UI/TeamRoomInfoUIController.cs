﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./GuildOrTeamState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildOrTeamUIStateController;
    [AutoBind("./WaitStartBattle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitStartBattleGameObject;
    [AutoBind("./Panel/StartBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./Panel/StartBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_startButtonUIStateController;
    [AutoBind("./Panel/TeamLeaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_leaveButton;
    [AutoBind("./Panel/EditTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_showChangePlayerPositionButton;
    [AutoBind("./Panel/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Panel/Texts/TargetText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gameFunctionTypeNameText;
    [AutoBind("./Panel/Texts/TargetFloorText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_locationNameText;
    [AutoBind("./Panel/Texts/LVText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Panel/Texts/CountDownText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_quitCountdownText;
    [AutoBind("./Panel/Texts/ConsumeNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Panel/Texts/GuildLvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildMassiveCombatNameText;
    [AutoBind("./Panel/AuthorityDropdown", AutoBindAttribute.InitState.NotInit, false)]
    private Dropdown m_authorityDropdown;
    [AutoBind("./Panel/AuthorityDropdown/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_authorityLockButton;
    [AutoBind("./Panel/PlayerInfo0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfo0GameObject;
    [AutoBind("./Panel/PlayerInfo1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfo1GameObject;
    [AutoBind("./Panel/PlayerInfo2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfo2GameObject;
    [AutoBind("./Panel/EditTeamPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionPanelGameObject;
    [AutoBind("./Panel/EditTeamPanel/CompleteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changePlayerPositionCompleteButton;
    [AutoBind("./Panel/EditTeamPanel/PlayerInfo0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionInfo0GameObject;
    [AutoBind("./Panel/EditTeamPanel/PlayerInfo1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionInfo1GameObject;
    [AutoBind("./Panel/EditTeamPanel/PlayerInfo2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changePlayerPositionInfo2GameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/PlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfoPrefab;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private SmallExpressionParseDesc m_expressionParseDesc;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_expressionResContainer;
    private TeamRoomPlayerInfoUIController[] m_playerInfoUIControllers;
    private TeamRoomPlayerInfoUIController[] m_editPlayerInfoUIControllers;
    private TeamRoomPlayerInfoUIController m_draggingPlayerInfoUIController;
    private bool m_isIgnoreToggleEvent;
    private GameFunctionType m_gameFunctionType;
    private bool m_isOpened;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_GetPlayerInfoParent;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_OnApplicationPause;
    private static DelegateBridge __Hotfix_OnApplicationFocus;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetTeamRoomSetting;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombat;
    private static DelegateBridge __Hotfix_SetTeamRoomPlayers;
    private static DelegateBridge __Hotfix_SetBattleName;
    private static DelegateBridge __Hotfix_SetPlayerLevelRange;
    private static DelegateBridge __Hotfix_SetAuthority;
    private static DelegateBridge __Hotfix_SetQuitCountdown;
    private static DelegateBridge __Hotfix_ShowWaitStartBattle;
    private static DelegateBridge __Hotfix_ShowChangePlayerPosition;
    private static DelegateBridge __Hotfix_HideChangePlayerPosition;
    private static DelegateBridge __Hotfix_GetPlayerChangedPosition;
    private static DelegateBridge __Hotfix_ShowPlayerChat;
    private static DelegateBridge __Hotfix_ShowPlayerBigExpression;
    private static DelegateBridge __Hotfix_CreateDraggingPlayerInfoUIController;
    private static DelegateBridge __Hotfix_DestroyDragginPlayerInfoUIController;
    private static DelegateBridge __Hotfix_MoveDraggingPlayerInfoUIController;
    private static DelegateBridge __Hotfix_DropDraggingPlayerInfoUIController;
    private static DelegateBridge __Hotfix_OnLeaveButtonClick;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnShowChangePlayerPositionButtonClick;
    private static DelegateBridge __Hotfix_OnChangePlayerPositionCompleteButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnAuthorityDropdownValueChanged;
    private static DelegateBridge __Hotfix_OnAuthorityLockButtonClick;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnInviteButtonClick;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnPlayerButtonClick;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnBlessingButtonClick;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnBeginDrag;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnEndDrag;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnDrag;
    private static DelegateBridge __Hotfix_TeamRoomPlayerInfoUIController_OnDrop;
    private static DelegateBridge __Hotfix_add_EventOnLeave;
    private static DelegateBridge __Hotfix_remove_EventOnLeave;
    private static DelegateBridge __Hotfix_add_EventOnStart;
    private static DelegateBridge __Hotfix_remove_EventOnStart;
    private static DelegateBridge __Hotfix_add_EventOnShowChangePlayerPosition;
    private static DelegateBridge __Hotfix_remove_EventOnShowChangePlayerPosition;
    private static DelegateBridge __Hotfix_add_EventOnChangePlayerPositionComplete;
    private static DelegateBridge __Hotfix_remove_EventOnChangePlayerPositionComplete;
    private static DelegateBridge __Hotfix_add_EventOnShowChat;
    private static DelegateBridge __Hotfix_remove_EventOnShowChat;
    private static DelegateBridge __Hotfix_add_EventOnChangeAuthority;
    private static DelegateBridge __Hotfix_remove_EventOnChangeAuthority;
    private static DelegateBridge __Hotfix_add_EventOnShowInvite;
    private static DelegateBridge __Hotfix_remove_EventOnShowInvite;
    private static DelegateBridge __Hotfix_add_EventOnShowPlayerInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowPlayerInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Transform GetPlayerInfoParent(int idx, bool isEdit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRoomSetting(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRoomPlayers(List<TeamRoomPlayer> players, bool isLeader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleName(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerLevelRange(int levelMin, int levelMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetQuitCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitStartBattle(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChangePlayerPosition(TeamRoom teamRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideChangePlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerChangedPosition(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerChat(int playerIndex, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerBigExpression(int playerIndex, int expressionID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDraggingPlayerInfoUIController(TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyDragginPlayerInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveDraggingPlayerInfoUIController(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DropDraggingPlayerInfoUIController(Vector3 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowChangePlayerPositionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangePlayerPositionCompleteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAuthorityDropdownValueChanged(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAuthorityLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnInviteButtonClick(
      TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnPlayerButtonClick(
      TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnBlessingButtonClick(
      TeamRoomPlayerInfoUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnBeginDrag(
      TeamRoomPlayerInfoUIController ctrl,
      PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnEndDrag(
      TeamRoomPlayerInfoUIController ctrl,
      PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomPlayerInfoUIController_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TeamRoomPlayerInfoUIController_OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnLeave
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChangePlayerPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnChangePlayerPositionComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomAuthority> EventOnChangeAuthority
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowInvite
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, RectTransform> EventOnShowPlayerInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
