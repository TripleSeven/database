﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldEventListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class WorldEventListItemUIController : UIControllerBase
  {
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./Panel/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Panel/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./Panel/Energy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Panel/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Panel/Time", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./Panel/Time/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    private ConfigDataWaypointInfo m_wayPointInfo;
    private ConfigDataEventInfo m_eventInfo;
    private DateTime m_expireTime;
    private float m_updateCountdown;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetEventInfo;
    private static DelegateBridge __Hotfix_SetExpireTime;
    private static DelegateBridge __Hotfix_UpdateRemainTime;
    private static DelegateBridge __Hotfix_GetWaypointInfo;
    private static DelegateBridge __Hotfix_GetEventInfo;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldEventListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEventInfo(ConfigDataWaypointInfo waypointInfo, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExpireTime(long expireTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemainTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataWaypointInfo GetWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataEventInfo GetEventInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<WorldEventListItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
