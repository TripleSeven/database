﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaBpStageHeroItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaBpStageHeroItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./LevelBGImage/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    private Image m_grayIconImage;
    private Image m_armyIconImage;
    private int m_actorId;
    private PeakArenaBPStageHeroItemState m_state;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHero;
    private static DelegateBridge __Hotfix_SetState;
    private static DelegateBridge __Hotfix_SetSelectable;
    private static DelegateBridge __Hotfix_GetState;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_SetActorId;
    private static DelegateBridge __Hotfix_GetActorId;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(int heroId, int starLevel, int heroLevel, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetState(PeakArenaBPStageHeroItemState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectable(bool interactable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBPStageHeroItemState GetState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActorId()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PeakArenaBpStageHeroItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
