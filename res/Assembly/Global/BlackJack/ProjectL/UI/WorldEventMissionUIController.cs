﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldEventMissionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class WorldEventMissionUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./DangerOrNormal", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_dangerUIStateController;
    [AutoBind("./Mission/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Mission/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./Mission/MonsterLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_monsterLevelText;
    [AutoBind("./Mission/Energy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Mission/Reward/Goods", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGameObject;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Mission/EnterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enterButton;
    [AutoBind("./Mission/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Mission", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_missionUIStateController;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Char/0/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_charImage;
    private UISpineGraphic m_spineGraphic;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetEvent;
    private static DelegateBridge __Hotfix_SetRewards;
    private static DelegateBridge __Hotfix_CreateCharImage;
    private static DelegateBridge __Hotfix_DestroyCharImage;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnEnterButtonClick;
    private static DelegateBridge __Hotfix_OnCancelButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCheckEnterMission;
    private static DelegateBridge __Hotfix_remove_EventOnCheckEnterMission;
    private static DelegateBridge __Hotfix_add_EventOnEnterMission;
    private static DelegateBridge __Hotfix_remove_EventOnEnterMission;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    private WorldEventMissionUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEvent(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRewards(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharImage(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyCharImage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Func<bool> EventOnCheckEnterMission
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEnterMission
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
