﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginCommonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class LoginCommonUIController : UIControllerBase
  {
    [AutoBind("./Fade", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fadeImage;
    [AutoBind("./DisableInput", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableInputGameObject;
    private ScreenFade m_screenFade;
    private TouchFx m_touchFx;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitTouchFx;
    private static DelegateBridge __Hotfix_DisposeTouchFx;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_EnableInput;
    private static DelegateBridge __Hotfix_FadeIn;
    private static DelegateBridge __Hotfix_FadeOut;

    private LoginCommonUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~LoginCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisposeTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
