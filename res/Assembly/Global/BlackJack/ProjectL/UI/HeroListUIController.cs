﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroListUIController : UIControllerBase
  {
    [AutoBind("./HeroListPanel/HeroListScrollView/Viewport/Content/UnlockedHeroContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewUnlockedHeroContent;
    [AutoBind("./HeroListPanel/HeroListScrollView/Viewport/Content/LockedHeroContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewLockedHeroContent;
    [AutoBind("./HeroListPanel/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scrollView;
    [AutoBind("./HeroInfoPanel/CVPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cvText;
    [AutoBind("./HeroInfoPanel/CommentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_commentButton;
    [AutoBind("./HeroInfoPanel/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    [AutoBind("./HeroInfoPanel/DetailButton/RedPointImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailButtonRedPointImage;
    [AutoBind("./HeroInfoPanel/SummonPanel/SummonButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_summonButton;
    [AutoBind("./HeroInfoPanel/SummonPanel/SummonButton/U_SummonButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_summonButtonPressEffect;
    [AutoBind("./HeroInfoPanel/SummonPanel/SummonButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_summonButtonReadyEffect;
    [AutoBind("./HeroListPanel/FilterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterButton;
    [AutoBind("./HeroListPanel/AddHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addheroButton;
    [AutoBind("./HeroListPanel/SpeedUpHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_speedUpHeroButton;
    [AutoBind("./HeroListPanel/ResetHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_resetHeroButton;
    [AutoBind("./HeroListPanel/HeroIdInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_heroIdInputField;
    [AutoBind("./ComposeConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_composeConfirmPanel;
    [AutoBind("./ComposeConfirmPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_composeCostText;
    [AutoBind("./ComposeConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_composeCancelButton;
    [AutoBind("./ComposeConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_composeConfirmButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HeroInfoPanel/HeroName/NameTextCh", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameTextCh;
    [AutoBind("./HeroInfoPanel/HeroName/NameTextCh", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroNameStateCtrl;
    [AutoBind("./HeroInfoPanel/HeroName/NameTextEn", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameTextEn;
    [AutoBind("./HeroInfoPanel/HeroName/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroRankImage;
    [AutoBind("./HeroInfoPanel/HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./HeroInfoPanel/Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charWordGameObject;
    [AutoBind("./HeroInfoPanel/Char/U_Summonout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charSummonEffect;
    [AutoBind("./HeroInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoPanel;
    [AutoBind("./HeroInfoPanel/SkinChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroCharSkinChangeButton;
    [AutoBind("./HeroListPanel/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroJobUpButton;
    [AutoBind("./HeroInfoPanel/Equipments", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroEquipmentsButton;
    [AutoBind("./HeroInfoPanel/Equipments/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroEquipmentsContent;
    [AutoBind("./HeroInfoPanel/Equipments/RedPointImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroEquipmentsRedPointImage;
    [AutoBind("./HeroInfoPanel/PowerValue", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_powerValueStateCtrl;
    [AutoBind("./HeroInfoPanel/PowerValue/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_powerValueText;
    [AutoBind("./HeroInfoPanel/PowerValue/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_powerArrowGo;
    [AutoBind("./HeroInfoPanel/BreakPanel/BreakButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakButton;
    [AutoBind("./HeroInfoPanel/BreakPanel/BreakButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakButtonReadyEffect;
    [AutoBind("./HeroInfoPanel/BreakPanel/BreakButton/U_BreakButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakButtonPressEffect;
    [AutoBind("./HeroInfoPanel/BreakPanel/Fragment/AddImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakAddBtn;
    [AutoBind("./HeroInfoPanel/BreakPanel/Fragment/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakNumText;
    [AutoBind("./HeroInfoPanel/BreakPanel/Fragment/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakIconImage;
    [AutoBind("./HeroInfoPanel/BreakPanel/ExtractionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_extractionButton;
    [AutoBind("./HeroInfoPanel/BreakPanel/ExtractionTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_extractionTipsStateCtrl;
    [AutoBind("./HeroListPanel/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortButton;
    [AutoBind("./HeroListPanel/SortGroup/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sortButtonStateCtrl;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sortTypesPanelStateCtrl;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/BGImages/ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortTypesReturnBgImage;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Level", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesLevelToggle;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Star", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesStarToggle;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Rarity", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesRarityToggle;
    [AutoBind("./HeroListPanel/SortGroup/SortTypes/GridLayout/Power", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sortTypesPowerToggle;
    public const string LevelType = "Level";
    public const string StarLevelType = "StarLevel";
    public const string RankType = "Rank";
    public const string PowerType = "Power";
    public bool m_isUnLockHero;
    private int m_heroPowerValue;
    private int m_curHeroNeedFragmentCount;
    public int m_curSelectedHeroIndex;
    public GameObject m_curSeletedHeroList;
    private HeroCharUIController m_heroCharUIController;
    public List<Hero> m_unLockedHeroList;
    public List<Hero> m_lockedHeroList;
    public List<HeroItemUIController> m_unLockedHeroCtrlList;
    public List<HeroItemUIController> m_lockedHeroCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_UpdateViewInHeroList;
    private static DelegateBridge __Hotfix_UpdateHeroList;
    private static DelegateBridge __Hotfix_UpdateRightInfoPanel;
    private static DelegateBridge __Hotfix_SetHeroPowerValue;
    private static DelegateBridge __Hotfix_ResetHeroPowerValue;
    private static DelegateBridge __Hotfix_StopAllCoroutineInHeroList;
    private static DelegateBridge __Hotfix_SetHeroCharSkinPreview;
    private static DelegateBridge __Hotfix_Co_SetPowerValue;
    private static DelegateBridge __Hotfix_SetCurHeroList;
    private static DelegateBridge __Hotfix_SetToInitState;
    private static DelegateBridge __Hotfix_SetToAtLeftState;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_OnHeroFragmentSearchButtonClick;
    private static DelegateBridge __Hotfix_OnBreakButtonClick;
    private static DelegateBridge __Hotfix_PlayBreakButtonClickEffect;
    private static DelegateBridge __Hotfix_ComposeLockedHero;
    private static DelegateBridge __Hotfix_CloseComposeHeroPanel;
    private static DelegateBridge __Hotfix_ConfirmComposeHero;
    private static DelegateBridge __Hotfix_OnComposeHeroSucceed;
    private static DelegateBridge __Hotfix_PlayHeroGetEffect;
    private static DelegateBridge __Hotfix_OnExtractionButtonClick;
    private static DelegateBridge __Hotfix_OnEquipmentsButtonClick;
    private static DelegateBridge __Hotfix_OnHeroJobUpButtonClick;
    private static DelegateBridge __Hotfix_OnHeroCharSkinChangeButton;
    private static DelegateBridge __Hotfix_HeroCharUIController_OnClick;
    private static DelegateBridge __Hotfix_ClickHeroByIdForUserGuide;
    private static DelegateBridge __Hotfix_ClickLowLevelHeroForUserGuide;
    private static DelegateBridge __Hotfix_ClickLockedHeroForUserGuide;
    private static DelegateBridge __Hotfix_GetHeroItemPosInList;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_SetSortTypeButtonAndToggles;
    private static DelegateBridge __Hotfix_ResetData;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_OnFilterButtonClick;
    private static DelegateBridge __Hotfix_ResetScrollViewPosition;
    private static DelegateBridge __Hotfix_OnDetailButtonClick;
    private static DelegateBridge __Hotfix_OnSummonButtonClick;
    private static DelegateBridge __Hotfix_PlaySummonButtonClickEffect;
    private static DelegateBridge __Hotfix_ShowButtonGameObject;
    private static DelegateBridge __Hotfix_OnCommentButtonClick;
    private static DelegateBridge __Hotfix_OnAddHeroButtonClick;
    private static DelegateBridge __Hotfix_OnSpeedUpHeroButtonClick;
    private static DelegateBridge __Hotfix_OnResetHeroButtonClick;
    private static DelegateBridge __Hotfix_ResetPanelPos;
    private static DelegateBridge __Hotfix_InactiveCharWordPanel;
    private static DelegateBridge __Hotfix_GetCurHeroList;
    private static DelegateBridge __Hotfix_HaveHeroCanCompose;
    private static DelegateBridge __Hotfix_PlayHeroPerformance;
    private static DelegateBridge __Hotfix_OnLevelToggleValueChanged;
    private static DelegateBridge __Hotfix_OnStarLevelToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRankToggleValueChanged;
    private static DelegateBridge __Hotfix_OnPowerToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSortButtonClick;
    private static DelegateBridge __Hotfix_CloseSortTypesPanel;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnFilter;
    private static DelegateBridge __Hotfix_remove_EventOnFilter;
    private static DelegateBridge __Hotfix_add_EventOnDetail;
    private static DelegateBridge __Hotfix_remove_EventOnDetail;
    private static DelegateBridge __Hotfix_add_EventOnComment;
    private static DelegateBridge __Hotfix_remove_EventOnComment;
    private static DelegateBridge __Hotfix_add_EventOnEquipment;
    private static DelegateBridge __Hotfix_remove_EventOnEquipment;
    private static DelegateBridge __Hotfix_add_EventOnJob;
    private static DelegateBridge __Hotfix_remove_EventOnJob;
    private static DelegateBridge __Hotfix_add_EventOnHeroCharClick;
    private static DelegateBridge __Hotfix_remove_EventOnHeroCharClick;
    private static DelegateBridge __Hotfix_add_EventOnHeroBreak;
    private static DelegateBridge __Hotfix_remove_EventOnHeroBreak;
    private static DelegateBridge __Hotfix_add_EventOnAddHero;
    private static DelegateBridge __Hotfix_remove_EventOnAddHero;
    private static DelegateBridge __Hotfix_add_EventOnComposeHero;
    private static DelegateBridge __Hotfix_remove_EventOnComposeHero;
    private static DelegateBridge __Hotfix_add_EventOnSortToggleClick;
    private static DelegateBridge __Hotfix_remove_EventOnSortToggleClick;
    private static DelegateBridge __Hotfix_add_EventOnShowGetPath;
    private static DelegateBridge __Hotfix_remove_EventOnShowGetPath;
    private static DelegateBridge __Hotfix_add_EventOnInitHeroList;
    private static DelegateBridge __Hotfix_remove_EventOnInitHeroList;
    private static DelegateBridge __Hotfix_add_EventOnUpdateViewInListAndDetail;
    private static DelegateBridge __Hotfix_remove_EventOnUpdateViewInListAndDetail;
    private static DelegateBridge __Hotfix_add_EventOnHeroCharSkinChangeButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnHeroCharSkinChangeButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGoToMemoryExtractionStore;
    private static DelegateBridge __Hotfix_remove_EventOnGoToMemoryExtractionStore;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroList(bool isNeedRebuildHeroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRightInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroPowerValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetHeroPowerValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopAllCoroutineInHeroList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCharSkinPreview(string spinePath, int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetPowerValue(float newValue, float oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurHeroList(List<Hero> unlockedList, List<Hero> lockedList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToInitState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToAtLeftState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick(HeroItemUIController goCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroFragmentSearchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayBreakButtonClickEffect(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComposeLockedHero(Hero h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseComposeHeroPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ConfirmComposeHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnComposeHeroSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayHeroGetEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExtractionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentsButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroCharSkinChangeButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharUIController_OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickHeroByIdForUserGuide(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickLowLevelHeroForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ClickLockedHeroForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetHeroItemPosInList(GameObject child, List<HeroItemUIController> parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSortTypeButtonAndToggles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSummonButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlaySummonButtonClickEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowButtonGameObject(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCommentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpeedUpHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnResetHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetPanelPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InactiveCharWordPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetCurHeroList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HaveHeroCanCompose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarLevelToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPowerToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSortTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFilter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnComment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEquipment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJob
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroCharClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnHeroBreak
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnComposeHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroListUIController.HeroSortType> EventOnSortToggleClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnShowGetPath
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<Hero>, List<Hero>> EventOnInitHeroList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event HeroListUIController.Action<int, bool, bool, int, bool> EventOnUpdateViewInListAndDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroCharSkinChangeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGoToMemoryExtractionStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);

    public enum HeroSortType
    {
      Level,
      StarLevel,
      Rank,
      Power,
    }
  }
}
