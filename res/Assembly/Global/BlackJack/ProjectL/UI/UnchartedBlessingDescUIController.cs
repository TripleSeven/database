﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedBlessingDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedBlessingDescUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_panelGameObject;
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/ItemInfoGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Panel/ItemInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Panel/BGImage/FrameImage/BottomImage2/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./Panel/BGImage/FrameImage/BottomImage2/Desc/Text1", AutoBindAttribute.InitState.NotInit, false)]
    public Text SweepDescText;
    [AutoBind("./Panel/BGImage/FrameImage/SweepButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button SweepButton;
    [AutoBind("./Panel/BGImage/FrameImage/SweepButton", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SweepButtonUIStateCtrl;
    [AutoBind("./Panel/BGImage/FrameImage/SweepTimes", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SweepTimeGameObject;
    [AutoBind("./Panel/BGImage/FrameImage/SweepTimes/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text SweepTimeText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetSweepState;
    private static DelegateBridge __Hotfix_SetSweepTimes;
    private static DelegateBridge __Hotfix_SetSweepDescText;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetSkillInfo;
    private static DelegateBridge __Hotfix_GetMinYInBackgroundRect;
    private static DelegateBridge __Hotfix_SetPanelPosition;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnSweepButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnSweepButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSweepButtonClick;

    private UnchartedBlessingDescUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSweepState(BattleType battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSweepTimes(BattleType battleType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSweepDescText(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkillInfo(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMinYInBackgroundRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPanelPosition(Vector3 p, Vector2 offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSweepButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSweepButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
