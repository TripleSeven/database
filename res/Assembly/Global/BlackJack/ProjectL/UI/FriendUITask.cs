﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectLBasic;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class FriendUITask : UITask
  {
    public const string ParamKey_OpenAddFriend = "OpenAddFriend";
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private FriendUIController m_friendUIController;
    private FriendPanelType m_panelType;
    private List<string> m_invitedRecommendFriendIDList;
    private List<UserSummary> m_recommendFriendList;
    private List<UserSummary> m_findFriendList;
    private List<string> m_sendFriendshipPointUserIDList;
    private List<string> m_getFriendshipPointUserIDList;
    private string m_unblockPlayerUserID;
    private string m_kickPlayerUserID;
    private string m_kickPlayerUserName;
    private ProChatGroupInfo m_currentGroupInfo;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_UpdatePanel;
    private static DelegateBridge __Hotfix_FriendUIController_OnShowPanel;
    private static DelegateBridge __Hotfix_FriendUIController_OnReturn;
    private static DelegateBridge __Hotfix_FriendUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_FriendUIController_OnGetSocialRelation;
    private static DelegateBridge __Hotfix_FriendUIController_OnShowPlayerInfo;
    private static DelegateBridge __Hotfix_OnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_GetSocialRelation;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_OnClose;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_OnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_FriendUIController_OnAddFriend;
    private static DelegateBridge __Hotfix_FriendUIController_OnFriendInviteAceept;
    private static DelegateBridge __Hotfix_FriendUIController_OnFriendInviteDecline;
    private static DelegateBridge __Hotfix_FriendUIController_OnUnblockPlayer;
    private static DelegateBridge __Hotfix_UnblockPlayerCallback;
    private static DelegateBridge __Hotfix_FriendUIController_OnFindFriend;
    private static DelegateBridge __Hotfix_SetCurrentFindFriendList;
    private static DelegateBridge __Hotfix_FriendUIController_OnGetRecommendFriendList;
    private static DelegateBridge __Hotfix_SetCurrentRecommedFriendList;
    private static DelegateBridge __Hotfix_FriendUIController_OnGetAllGroup;
    private static DelegateBridge __Hotfix_FriendUIController_OnGroupChat;
    private static DelegateBridge __Hotfix_FriendUIController_OnWatchGroupStaff;
    private static DelegateBridge __Hotfix_FriendUIController_OnCreateNewGroup;
    private static DelegateBridge __Hotfix_FriendUIController_OnInviteFriendToGroup;
    private static DelegateBridge __Hotfix_FriendUIController_OnChangeGroupName;
    private static DelegateBridge __Hotfix_FriendUIController_OnLeaveGroup;
    private static DelegateBridge __Hotfix_OnLeaveGroupDialogBoxCallback;
    private static DelegateBridge __Hotfix_FriendUIController_OnKickGroup;
    private static DelegateBridge __Hotfix_OnKickFromGroupDialogBoxCallback;
    private static DelegateBridge __Hotfix_FriendUIController_OnChat;
    private static DelegateBridge __Hotfix_FriendUIController_OnSendFriendshipPoint;
    private static DelegateBridge __Hotfix_FriendUIController_OnGetFriendshipPoint;
    private static DelegateBridge __Hotfix_FriendUIController_OnSendAllFriendshipPoint;
    private static DelegateBridge __Hotfix_FriendUIController_OnGetAllFriendshipPoint;
    private static DelegateBridge __Hotfix_PlayerContext_OnChatGroupUpdateNtf;
    private static DelegateBridge __Hotfix_isMeInTheGroup;
    private static DelegateBridge __Hotfix_ShowErrorMessage;
    private static DelegateBridge __Hotfix_ShowMessage;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnShowPanel(FriendPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetSocialRelation(
      FriendSocialRelationFlag friendSocialRelationFlag = FriendSocialRelationFlag.All)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnShowPlayerInfo(
      UserSummary userSummy,
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowPlayerSimpleInfo(Vector3 pos, PlayerSimpleInfoUITask.PostionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_GetSocialRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnAddFriend(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnFriendInviteAceept(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnFriendInviteDecline(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnUnblockPlayer(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnblockPlayerCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnFindFriend(int bornChannelID, string partialName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrentFindFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetRecommendFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrentRecommedFriendList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetAllGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGroupChat(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnWatchGroupStaff(string groupID, bool needOpenTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnCreateNewGroup(List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnInviteFriendToGroup(string groupID, List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnChangeGroupName(string groupID, string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnLeaveGroup(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeaveGroupDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnKickGroup(
      ProChatGroupInfo chatGroupInfo,
      UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnKickFromGroupDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnChat(UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnSendFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetFriendshipPoint(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnSendAllFriendshipPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUIController_OnGetAllFriendshipPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatGroupUpdateNtf(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool isMeInTheGroup(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowErrorMessage(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMessage(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
