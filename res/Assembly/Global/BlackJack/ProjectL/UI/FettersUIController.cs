﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/Fetters", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fettersButton;
    [AutoBind("./Margin/Fetters/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fettersButtonNewImage;
    [AutoBind("./Margin/Fetters/PercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_fettersButtonPercentText;
    [AutoBind("./Margin/Information", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_informationButton;
    [AutoBind("./Margin/Information/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_informationButtonNewImage;
    [AutoBind("./Favorability/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityTotalValueText;
    [AutoBind("./HeroCharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./Margin/HeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scrollRect;
    [AutoBind("./Margin/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewContent;
    [AutoBind("./Prefab/ListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listItemPrefab;
    [AutoBind("./ChangeInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelCommonUIStateCtrl;
    private List<FettersHeroListItemUIController> m_heroCtrlList;
    private HeroCharUIController m_heroCharUIController;
    private Hero m_hero;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private bool isFirstIn;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_UpdateViewInFetters;
    private static DelegateBridge __Hotfix_OnListItemClick;
    private static DelegateBridge __Hotfix_HeroListItemCompare;
    private static DelegateBridge __Hotfix_CalcFetterFinishPercent;
    private static DelegateBridge __Hotfix_GoToInformationPanel;
    private static DelegateBridge __Hotfix_PlayHeroPerformance;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnInformationButtonClick;
    private static DelegateBridge __Hotfix_OnFettersButtonClick;
    private static DelegateBridge __Hotfix_ResetScrollViewPosition;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnListItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnListItemClick;
    private static DelegateBridge __Hotfix_add_EventOnFettersButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnFettersButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnInformationButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnInformationButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFetters(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnListItemClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemCompare(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcFetterFinishPercent(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GoToInformationPanel(bool isGotoOrReturn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInformationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFettersButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnListItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnFettersButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnInformationButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
