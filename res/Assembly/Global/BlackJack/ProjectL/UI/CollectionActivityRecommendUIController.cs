﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityRecommendUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityRecommendUIController : UIControllerBase
  {
    [AutoBind("./BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detal/RecommendHeroScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_content;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_prefabPool;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    private List<CollectionActivityRecommendItemUIController> m_recommendCtrlList;
    private const string c_recommendItemPoolName = "RecommendItem";
    private const string c_recommendHeroPoolName = "RecommendHero";
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitPrefabPools;
    private static DelegateBridge __Hotfix_OnObjectCreated;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Hide;
    private static DelegateBridge __Hotfix_OnHideStateFinished;
    private static DelegateBridge __Hotfix_OnBgBtnClicked;
    private static DelegateBridge __Hotfix_UpdateView_0;
    private static DelegateBridge __Hotfix_UpdateView_1;
    private static DelegateBridge __Hotfix_UpdateView_2;
    private static DelegateBridge __Hotfix_OnGetFromRecommendHeroPool;
    private static DelegateBridge __Hotfix_OnReturnFromRecommendHeroPool;
    private static DelegateBridge __Hotfix_add_EventOnReturnHandleEnd;
    private static DelegateBridge __Hotfix_remove_EventOnReturnHandleEnd;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityRecommendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPrefabPools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Hide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHideStateFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBgBtnClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(List<CollectionActivityCurrency> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(List<CollectionActivityCurrency> list, List<int> highlightIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(
      List<CollectionActivityCurrency> list,
      bool needHighlight,
      List<int> highlightIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityRecommendHeroUIController OnGetFromRecommendHeroPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnFromRecommendHeroPool(CollectionActivityRecommendHeroUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturnHandleEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
