﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemLevelsUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroAnthemLevelsUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./PlayerResource/Challenge/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeValueText;
    [AutoBind("./AchievementPanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_chapterAchievementText;
    [AutoBind("./ChapterPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_chapterNameText;
    [AutoBind("./ChapterPanel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_chapterImage;
    [AutoBind("./StageListPanel/StageListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./AchievementListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementListPanelStateCtrl;
    [AutoBind("./AchievementListPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementListPanelBGButton;
    [AutoBind("./AchievementListPanel/Detail/AchievementListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementListScrollRect;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefab/StageButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroAnthemLevelListItemPrefab;
    private List<HeroAnthemLevelItemUIController> m_HeroAnthemLevelListItems;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_SetHeroAnthem;
    private static DelegateBridge __Hotfix_SetAllHeroAnthemLevelListItems;
    private static DelegateBridge __Hotfix_AddHeroAnthemLevelListItem;
    private static DelegateBridge __Hotfix_ClearHeroAnthemLevelListItems;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_HeroAnthemLevelListItem_OnStartButtonClick;
    private static DelegateBridge __Hotfix_HeroAnthemLevelListItem_OnAchievementsClick;
    private static DelegateBridge __Hotfix_SetAchievementPanel;
    private static DelegateBridge __Hotfix_AddAchievementItem;
    private static DelegateBridge __Hotfix_OnAchievementListPanelBGButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnStartHeroAnthemLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartHeroAnthemLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroAnthemLevelsUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAnthem(ConfigDataHeroAnthemInfo heroAnthemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllHeroAnthemLevelListItems(List<ConfigDataHeroAnthemLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroAnthemLevelListItem(ConfigDataHeroAnthemLevelInfo levelnfo, bool opened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearHeroAnthemLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemLevelListItem_OnStartButtonClick(HeroAnthemLevelItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemLevelListItem_OnAchievementsClick(HeroAnthemLevelItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAchievementPanel(ConfigDataHeroAnthemLevelInfo anthemLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddAchievementItem(
      ConfigDataBattleAchievementInfo achievementInfo,
      List<Goods> rewards,
      GameObject prefab,
      bool complete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementListPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroAnthemLevelInfo> EventOnStartHeroAnthemLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
