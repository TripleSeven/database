﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityRecommendItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityRecommendItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./materialsItem", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_iconButton;
    [AutoBind("./materialsItem", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconBgImage;
    [AutoBind("./materialsItem/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupRoot;
    private List<CollectionActivityRecommendHeroUIController> m_heroCtrlList;
    private CollectionActivityCurrency m_currencyInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateItem;
    private static DelegateBridge __Hotfix_HandleBeforeReturnToPool;
    private static DelegateBridge __Hotfix_OnItemClicked;
    private static DelegateBridge __Hotfix_set_IsMasked;
    private static DelegateBridge __Hotfix_add_EventOnGetFromRecommendHeroPool;
    private static DelegateBridge __Hotfix_remove_EventOnGetFromRecommendHeroPool;
    private static DelegateBridge __Hotfix_add_EventOnReturnFromRecommendHeroPool;
    private static DelegateBridge __Hotfix_remove_EventOnReturnFromRecommendHeroPool;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityRecommendItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(CollectionActivityCurrency currencyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HandleBeforeReturnToPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsMasked
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Func<CollectionActivityRecommendHeroUIController> EventOnGetFromRecommendHeroPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionActivityRecommendHeroUIController> EventOnReturnFromRecommendHeroPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
