﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.Raffle3DUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class Raffle3DUIController : UIControllerBase
  {
    protected Action m_onAnimationEnd;
    protected static string AniParam_Init;
    protected static string AniParam_Level0;
    protected static string AniParam_Level1;
    protected static string AniParam_Level2;
    protected static string AniParam_Level3;
    [AutoBind("./3DBgImage", AutoBindAttribute.InitState.NotInit, false)]
    public Button ThreeDBgButton;
    [AutoBind("./3DViewImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image ThreeDViewImage;
    [AutoBind("./3DCamera", AutoBindAttribute.InitState.NotInit, false)]
    public Camera ThreeDViewCamera;
    [AutoBind("./Mesh_FX_Xionggui", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIEffectStateCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_ShowRaffle3DModel;
    private static DelegateBridge __Hotfix_ShowDrawIdleAnimation;
    private static DelegateBridge __Hotfix_ShowRaffeDrawingAnimation;
    private static DelegateBridge __Hotfix_GetAniNameByLevel;
    private static DelegateBridge __Hotfix_OnDrawAnimationEnd;
    private static DelegateBridge __Hotfix_OnDrawAnimationEndImp;
    private static DelegateBridge __Hotfix_WaitForTime;
    private static DelegateBridge __Hotfix_On3DBgButtonClick;
    private static DelegateBridge __Hotfix_CalcThreeDCameraViewRect;
    private static DelegateBridge __Hotfix_get_LayerCamera;
    private static DelegateBridge __Hotfix_set_LayerCamera;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaffle3DModel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDrawIdleAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaffeDrawingAnimation(int level, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetAniNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDrawAnimationEnd(GameObject go, string aniName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDrawAnimationEndImp()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator WaitForTime(Action action, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void On3DBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Rect CalcThreeDCameraViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    public Camera LayerCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static Raffle3DUIController()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
