﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RankingListItemUIController : UIControllerBase
  {
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    [AutoBind("./Rank/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RankValueText;
    [AutoBind("./Rank/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image RankValueImage;
    [AutoBind("./Rank/PeakArenaImage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PeakArenaRankStateCtrl;
    [AutoBind("./HeadIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image HeadIconImage;
    [AutoBind("./LevelValue", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerLevelText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerNameText;
    [AutoBind("./DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController DetailGroupStateCtrl;
    [AutoBind("./DetailGroup/Top15Hero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text Top15HeroScoreValueText;
    [AutoBind("./DetailGroup/TopHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text TopHeroScoreValueText;
    [AutoBind("./DetailGroup/AllHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text AllHeroScoreValueText;
    [AutoBind("./DetailGroup/ChampionHero/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ChampionHeroScoreTitleText;
    [AutoBind("./DetailGroup/ChampionHero/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ChampionHeroScoreValueText;
    [AutoBind("./DetailGroup/RiftChapterStar/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RiftChapterStarScoreValueText;
    [AutoBind("./DetailGroup/RiftAchievement/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RiftAchievementScoreValueText;
    [AutoBind("./DetailGroup/ClimbTower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ClimbTowerScoreValueText;
    [AutoBind("./DetailGroup/PlayerTitle/PlayerTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerTitleValueText;
    [AutoBind("./DetailGroup/PlayerTitle/PlayerTitle/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image PlayerTitleImage;
    [AutoBind("./DetailGroup/PlayerTitle", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PlayerTitleStateCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_RegisterItemClickEvent;
    private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
    private static DelegateBridge __Hotfix_GetItemIndex;
    private static DelegateBridge __Hotfix_UpdateItemInfo;
    private static DelegateBridge __Hotfix_UpdateItemInfoInPeakArenaState;
    private static DelegateBridge __Hotfix_GetRankLevelSprite;
    private static DelegateBridge __Hotfix_UpdateDetailInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetItemIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfo(
      RankingListType rankType,
      int headIconId,
      int rankLevel,
      int lv,
      string playerName,
      int score,
      string heroName = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfoInPeakArenaState(
      int headIconId,
      int rank,
      int lv,
      string playerName,
      int seasonId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateDetailInfo(RankingListType rankType, string heroName, int score)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
