﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftChapterUnlockConditionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RiftChapterUnlockConditionItemUIController : UIControllerBase
  {
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goButton;
    private int m_scenarioID;
    private static DelegateBridge __Hotfix_SetCondition;
    private static DelegateBridge __Hotfix_GoScenario;
    private static DelegateBridge __Hotfix_add_EventOnGoToScenario;
    private static DelegateBridge __Hotfix_remove_EventOnGoToScenario;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCondition(RiftChapterUnlockConditionType condition, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
