﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityExchangeMissionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityExchangeMissionItemUIController : UIControllerBase
  {
    [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemDummy;
    [AutoBind("./ConsumeItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_consumeItemGroup;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupStateCtrl;
    [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./ButtonGroup/GetButton/TimesGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_curCountText;
    [AutoBind("./ButtonGroup/GetButton/TimesGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxCountText;
    private List<CollectionAcitivityExchangeConsumeItemUIController> m_itemUICtrlList;
    private RewardGoodsUIController goodsCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateItem;
    private static DelegateBridge __Hotfix_HandleBeforeReturnToPool;
    private static DelegateBridge __Hotfix_OnGetButtonClick;
    private static DelegateBridge __Hotfix_PlayGetRewardEffect;
    private static DelegateBridge __Hotfix_add_EventOnGetFromConsumeItemPool;
    private static DelegateBridge __Hotfix_remove_EventOnGetFromConsumeItemPool;
    private static DelegateBridge __Hotfix_add_EventOnReturnFromConsumeItemPool;
    private static DelegateBridge __Hotfix_remove_EventOnReturnFromConsumeItemPool;
    private static DelegateBridge __Hotfix_add_EventOnGetButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGetButtonClick;
    private static DelegateBridge __Hotfix_set_Mission;
    private static DelegateBridge __Hotfix_get_Mission;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityExchangeMissionItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(CollectionActivityMission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HandleBeforeReturnToPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayGetRewardEffect(Action OnStateFinish, bool isMax)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Func<CollectionAcitivityExchangeConsumeItemUIController> EventOnGetFromConsumeItemPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionAcitivityExchangeConsumeItemUIController> EventOnReturnFromConsumeItemPool
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CollectionActivityExchangeMissionItemUIController> EventOnGetButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CollectionActivityMission Mission
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
