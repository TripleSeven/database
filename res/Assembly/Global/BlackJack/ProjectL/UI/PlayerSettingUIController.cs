﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerSettingUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerSettingUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_settingStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_settingPanelBGButton;
    [AutoBind("./DetailPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_settingPanelCloseButton;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/BGM/Slider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_musicVolumeSlider;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/Sound/Slider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_soundVolumeSlider;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/Voice/Slider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_voiceVolumeSlider;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/BGM/SoundOnImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_musicSoundOnState;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/BGM/SoundOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_musicSoundOffState;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/Sound/SoundOnImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soundSoundOnState;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/Sound/SoundOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soundSoundOffState;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/Voice/SoundOnImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_voiceSoundOnState;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/Voice/SoundOffImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_voiceSoundOffState;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/PowerSave/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_powerSaveModeOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/PowerSave/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_powerSaveModeOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/ChangeLanguage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_languageRoot;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/ChangeLanguage/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_languageToggleGroup;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/ResourceQuality/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_resourceLowQualityModeOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/ResourceQuality/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_resourceLowQualityModeOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BasicSettingGroup/ResourceQuality", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_resourceQualityObject;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/EndAction/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_endActionOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/EndAction/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_endActionOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/NoticeSettingGroup/RecoveryEnergy/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recoveryEnergyOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/NoticeSettingGroup/RecoveryEnergy/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recoveryEnergyOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/NoticeSettingGroup/ArenaTicket/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_arenaTicketOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/NoticeSettingGroup/ArenaTicket/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_arenaTicketOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/NoticeSettingGroup/StoreRefresh/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_storeRefreshOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/NoticeSettingGroup/StoreRefresh/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_storeRefreshOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/BattleAnimation/ToggleGroup/OnToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleAnimationOnToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/BattleAnimation/ToggleGroup/OffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleAnimationOffToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/BattleAnimation/OffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleAnimationOffButton;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/BattleAnimation/ToggleGroup/OnlyMine", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_battleAnimationOnlyMineToggle;
    [AutoBind("./DetailPanel/ScrollView/Viewport/Content/BattleSettingGroup/BattleAnimation/OnlyMineButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleAnimationOnlyMineButton;
    private ProjectLPlayerContext m_playerContext;
    private bool m_isIgnoreToggleEvent;
    private Toggle[] m_languageToggles;
    private static DelegateBridge __Hotfix_set_IsUseLowResource;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SettingPanelShow;
    private static DelegateBridge __Hotfix_OnSettingPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_InitLanguageToggles;
    private static DelegateBridge __Hotfix_UpdateSettingPanel;
    private static DelegateBridge __Hotfix_OnMusicVolumeValueChanged;
    private static DelegateBridge __Hotfix_OnSoundVolumeValueChanged;
    private static DelegateBridge __Hotfix_OnVoiceVolumeValueChanged;
    private static DelegateBridge __Hotfix_OnPowerSaveModeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnResourceLowQualityModeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnEndActionToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRecoveryEnergyToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRandomEventToggleValueChanged;
    private static DelegateBridge __Hotfix_OnArenaTicketToggleValueChanged;
    private static DelegateBridge __Hotfix_OnStoreRefreshToggleValueChanged;
    private static DelegateBridge __Hotfix_OnLanguageToggleValueChanged;
    private static DelegateBridge __Hotfix_SetBattleAnimationOn;
    private static DelegateBridge __Hotfix_SetBattleAnimationOff;
    private static DelegateBridge __Hotfix_SetBattleAnimationOnlyMine;
    private static DelegateBridge __Hotfix_ResetLanguageToggle;
    private static DelegateBridge __Hotfix_OnBattleAnimationOffButtonClick;
    private static DelegateBridge __Hotfix_OnBattleAnimationOnlyMineButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnMusicVolumeChanged;
    private static DelegateBridge __Hotfix_remove_EventOnMusicVolumeChanged;
    private static DelegateBridge __Hotfix_add_EventOnSoundVolumeChanged;
    private static DelegateBridge __Hotfix_remove_EventOnSoundVolumeChanged;
    private static DelegateBridge __Hotfix_add_EventOnVoiceVolumeChanged;
    private static DelegateBridge __Hotfix_remove_EventOnVoiceVolumeChanged;
    private static DelegateBridge __Hotfix_add_EventOnPowerSaveModeIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnPowerSaveModeIsOn;
    private static DelegateBridge __Hotfix_add_EventOnResourceLowQualityModeIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnResourceLowQualityModeIsOn;
    private static DelegateBridge __Hotfix_add_EventOnEndActionIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnEndActionIsOn;
    private static DelegateBridge __Hotfix_add_EventOnNotifyRecoveryEnergyIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnNotifyRecoveryEnergyIsOn;
    private static DelegateBridge __Hotfix_add_EventOnNotifyRandomEventIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnNotifyRandomEventIsOn;
    private static DelegateBridge __Hotfix_add_EventOnNotifyArenaTicketIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnNotifyArenaTicketIsOn;
    private static DelegateBridge __Hotfix_add_EventOnNotifyStoreRefreshIsOn;
    private static DelegateBridge __Hotfix_remove_EventOnNotifyStoreRefreshIsOn;
    private static DelegateBridge __Hotfix_add_EventOnLanguageIndexChanged;
    private static DelegateBridge __Hotfix_remove_EventOnLanguageIndexChanged;
    private static DelegateBridge __Hotfix_add_EventOnSetBattleAnimationMode;
    private static DelegateBridge __Hotfix_remove_EventOnSetBattleAnimationMode;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerSettingUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsUseLowResource
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SettingPanelShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSettingPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLanguageToggles(int index, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSettingPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMusicVolumeValueChanged(float volumn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoundVolumeValueChanged(float volumn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceVolumeValueChanged(float volumn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPowerSaveModeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnResourceLowQualityModeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndActionToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecoveryEnergyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRandomEventToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaTicketToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreRefreshToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLanguageToggleValueChanged(bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleAnimationOn(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleAnimationOff(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleAnimationOnlyMine(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetLanguageToggle(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleAnimationOffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleAnimationOnlyMineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<float> EventOnMusicVolumeChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<float> EventOnSoundVolumeChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<float> EventOnVoiceVolumeChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnPowerSaveModeIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnResourceLowQualityModeIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnEndActionIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnNotifyRecoveryEnergyIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnNotifyRandomEventIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnNotifyArenaTicketIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnNotifyStoreRefreshIsOn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLanguageIndexChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<SkipCombatMode> EventOnSetBattleAnimationMode
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
