﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossAchievementUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallBossAchievementUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementPanelUIStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementPanelBGButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementPanelCloseButton;
    [AutoBind("./Detail/AchievementScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementPanelScrollRect;
    [AutoBind("./Detail/TopDetail/DamageValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementPanelDamageValueText;
    [AutoBind("./Detail/TopDetail/EvaluateGroupContent/EvaluateLetterItem", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_achievementPanelEvaluateImage;
    [AutoBind("./Detail/TopDetail/EvaluateGroupContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementPanelEvaluateImageStateCtrl;
    [AutoBind("./Detail/TopDetail/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementPanelBestRankingValueText;
    [AutoBind("./Detail/TopDetail/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_achievementPanelBestRankValueImage;
    [AutoBind("./Detail/TopDetail/RankValueNoneImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementPanelBestRankNoneGameObject;
    [AutoBind("./Prefab/AchievementItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementItemPrefabGo;
    private ConfigDataAncientCallBossInfo m_bossInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_ShowAncientCallBossAchievement;
    private static DelegateBridge __Hotfix_ShowAchievementListPanel;
    private static DelegateBridge __Hotfix_OnGetMissionReward;
    private static DelegateBridge __Hotfix_CloseAcievementListPanel;
    private static DelegateBridge __Hotfix_add_EventOnGetMissionReward;
    private static DelegateBridge __Hotfix_remove_EventOnGetMissionReward;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAncientCallBossAchievement(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAchievementListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAcievementListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGetMissionReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
