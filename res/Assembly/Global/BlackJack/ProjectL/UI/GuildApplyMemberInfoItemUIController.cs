﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildApplyMemberInfoItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildApplyMemberInfoItemUIController : UIControllerBase
  {
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_headIconStateCtrl;
    [AutoBind("./Char/HeadIcon/HeadIconGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconGrey;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_HeadFrameDummy;
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PowerValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_powerText;
    [AutoBind("./RefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refuseButton;
    [AutoBind("./AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_agreeButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitItemInfo;
    private static DelegateBridge __Hotfix_OnAgreeClick;
    private static DelegateBridge __Hotfix_OnRefuseClick;
    private static DelegateBridge __Hotfix_add_EventOnAcceptResult;
    private static DelegateBridge __Hotfix_remove_EventOnAcceptResult;
    private static DelegateBridge __Hotfix_get_Player;
    private static DelegateBridge __Hotfix_set_Player;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitItemInfo(UserSummary player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAgreeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefuseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GuildApplyMemberInfoItemUIController, bool> EventOnAcceptResult
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public UserSummary Player
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
