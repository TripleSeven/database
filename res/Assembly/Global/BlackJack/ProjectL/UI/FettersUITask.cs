﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class FettersUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private FettersUIController m_fettersUIController;
    private FettersFavorabilityUIController m_fettersFavorabilityUIController;
    private FettersInformationUIController m_fettersInformationUIController;
    private FettersConfessionUIController m_fettersConfessionUIController;
    public const string MatthewMode = "Matthew";
    public const string FavorabilityMode = "Favorability";
    public const string InformationMode = "Information";
    public const string GiftMode = "Gift";
    public const string ConfessionMode = "Confession";
    private string m_curMode;
    private string m_lastMode;
    private int m_curLayerDescIndex;
    private Hero m_hero;
    private int m_lastHeroId;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_nowSeconds;
    private bool m_isNeedShowFadeIn;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_SaveUIStateToIntent;
    private static DelegateBridge __Hotfix_GetUIStateFromIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
    private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PushAndPopLayerByState;
    private static DelegateBridge __Hotfix_FettersUIController_OnReturn;
    private static DelegateBridge __Hotfix_FettersUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_FettersUIController_OnListItemClick;
    private static DelegateBridge __Hotfix_FettersUIController_OnInformationButtonClick;
    private static DelegateBridge __Hotfix_FettersUIController_OnFettersButtonClick;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnReturn;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnInformationButtonClick;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnGiftButtonClick;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnUseGift;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_GotoBagFullUITask;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnHeroInteract;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnMemoryButtonClick;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnFettersButtonClick;
    private static DelegateBridge __Hotfix_FettersInformationUIController_OnReturn;
    private static DelegateBridge __Hotfix_FettersInformationUIController_OnVoiceItemClick;
    private static DelegateBridge __Hotfix_FettersGiftUIController_OnReturn;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnReturn;
    private static DelegateBridge __Hotfix_FettersConfessionUIControlle_OnShowHelp;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnAddGold;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnSkillUnlockButtonClick;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnEvolutionButtonClick;
    private static DelegateBridge __Hotfix_FettersFavorabilityUIController_OnConfessionButtonClick;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnUnlockCenterHeartFetter;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnEvolutionHeartFetter;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_FettersConfessionUIController_OnGotoGetPath;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateTouchTimeText;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_add_EventOnStartHeroDungeon;
    private static DelegateBridge __Hotfix_remove_EventOnStartHeroDungeon;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveUIStateToIntent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUIStateFromIntent(UIIntent uiIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushAndPopLayerByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnListItemClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnInformationButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUIController_OnFettersButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnInformationButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnGiftButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnUseGift(
      GoodsType goodsType,
      int itemId,
      int count,
      Action<List<Goods>> Onsucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_GotoBagFullUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnHeroInteract()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnMemoryButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnFettersButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersInformationUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersInformationUIController_OnVoiceItemClick(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersGiftUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIControlle_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnSkillUnlockButtonClick(
      int heroId,
      int fetterId,
      Action<List<Goods>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnEvolutionButtonClick(
      int heroId,
      int fetterId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersFavorabilityUIController_OnConfessionButtonClick(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnUnlockCenterHeartFetter(
      int heroId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnEvolutionHeartFetter(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnEvolutionMaterialClick(
      GoodsType goodsType,
      int goodsId,
      int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersConfessionUIController_OnGotoGetPath(
      GetPathData getPath,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTouchTimeText()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero, UIIntent> EventOnStartHeroDungeon
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
