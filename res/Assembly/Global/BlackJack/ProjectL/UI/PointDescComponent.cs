﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PointDescComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  public class PointDescComponent : UIControllerBase, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
  {
    private GameObject m_checkBoundaryGo;
    private GameObject m_draggingGo;
    private bool m_isDragging;
    private GameObject emptyImageGo;
    private GameObject m_downGo;
    private GameObject m_enterGo;
    private static DelegateBridge __Hotfix_SetGameObject;
    private static DelegateBridge __Hotfix_ShowReturnBgImage;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_OnPointerDown;
    private static DelegateBridge __Hotfix_OnPointerUp;
    private static DelegateBridge __Hotfix_OnBeginDrag;
    private static DelegateBridge __Hotfix_OnEndDrag;
    private static DelegateBridge __Hotfix_OnDrag;
    private static DelegateBridge __Hotfix_CheckPositionBoundary;
    private static DelegateBridge __Hotfix_PassEvent;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_set_IsNeedExcuteEvent;
    private static DelegateBridge __Hotfix_get_IsNeedExcuteEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameObject(
      GameObject addComponentRoot,
      bool isNeedExcuteEvent,
      GameObject addReturnImageRoot = null,
      GameObject checkBoundaryGo = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReturnBgImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckPositionBoundary(PointerEventData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PassEvent<T>(PointerEventData data, ExecuteEvents.EventFunction<T> function) where T : IEventSystemHandler
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNeedExcuteEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
