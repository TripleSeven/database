﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MonthCardInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MonthCardInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController PanelStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button BackgroundButton;
    [AutoBind("./LayoutRoot/ItemDesc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text NameText;
    [AutoBind("./LayoutRoot/ItemDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image IconImage;
    [AutoBind("./LayoutRoot/ItemDesc/LeftDay", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController LeftDayStateCtrl;
    [AutoBind("./LayoutRoot/ItemDesc/LeftDay/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text LeftDays;
    [AutoBind("./LayoutRoot/ListPanel/TitleImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController MonthCardOrSubscribeStateCtrl;
    [AutoBind("./LayoutRoot/ListPanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Text DescText;
    private int m_monthCardId;
    private float m_leftTime;
    private const float m_updateInterval = 1f;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowReward;
    private static DelegateBridge __Hotfix_UpdateState;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReward(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
