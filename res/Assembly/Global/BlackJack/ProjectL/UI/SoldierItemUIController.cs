﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SoldierItemUIController : UIControllerBase
  {
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./Attacking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_attackingObj;
    [AutoBind("./NotGet", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dontGetObj;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGrapgic;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_frameImage;
    [AutoBind("./SkinIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinIconImage;
    private bool m_isSoldierGet;
    private int m_soldierSkinId;
    private UISpineGraphic m_graphic;
    public ConfigDataJobConnectionInfo JobConnectionInfo;
    public ConfigDataSoldierInfo SoldierInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSoldierItem;
    private static DelegateBridge __Hotfix_OnSoldierItemClick;
    private static DelegateBridge __Hotfix_OnAttackButtonClick;
    private static DelegateBridge __Hotfix_SetAttackingPanelActive;
    private static DelegateBridge __Hotfix_SetAttackButtonActive;
    private static DelegateBridge __Hotfix_SetFrameImgActive;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_OnDestroy;
    private static DelegateBridge __Hotfix_add_EventOnSoldierItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnSoldierItemClick;
    private static DelegateBridge __Hotfix_add_EventOnAttackButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAttackButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(
      ConfigDataSoldierInfo si,
      ConfigDataJobConnectionInfo jci,
      bool isSoliderGet,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackingPanelActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackButtonActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFrameImgActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataSoldierInfo soldierInfo,
      bool isGet,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SoldierItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataSoldierInfo> EventOnAttackButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
