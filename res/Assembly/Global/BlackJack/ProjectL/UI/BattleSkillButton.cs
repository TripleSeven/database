﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleSkillButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleSkillButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
  {
    private bool m_isPointerLongDown;
    private bool m_ignoreClick;
    private float m_pointerLongDownCountdown;
    private Text m_cooldownText;
    private Image m_iconImage;
    private Image m_frameImage;
    private GameObject m_cooldownGameObject;
    private GameObject m_selectedGameObject;
    private GameObject m_descGameObject;
    private GameObject m_costGameObject;
    private GameObject m_passiveGameObject;
    private GameObject m_combineGameObject;
    private GameObject m_banGameObject;
    private ConfigDataSkillInfo m_skillInfo;
    private int m_index;
    private bool m_isSelected;
    private bool m_isBanned;
    private bool m_isHandleLongDown;
    private CommonUIStateController m_descStateCtrl;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_SetIndex;
    private static DelegateBridge __Hotfix_GetIndex;
    private static DelegateBridge __Hotfix_SetSkillInfo;
    private static DelegateBridge __Hotfix_GetSkillInfo;
    private static DelegateBridge __Hotfix_SetCooldown;
    private static DelegateBridge __Hotfix_SetSelected;
    private static DelegateBridge __Hotfix_IsSelected;
    private static DelegateBridge __Hotfix_ShowCost;
    private static DelegateBridge __Hotfix_ShowFrame;
    private static DelegateBridge __Hotfix_SetBanned;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_SetHandleLongDown;
    private static DelegateBridge __Hotfix_SetDescGameObject;
    private static DelegateBridge __Hotfix_ShowDesc;
    private static DelegateBridge __Hotfix_HideDesc;
    private static DelegateBridge __Hotfix_OnPointerDown;
    private static DelegateBridge __Hotfix_OnPointerUp;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_InvokeClickEvent;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkillInfo(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCooldown(int cd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool selected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCost(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanned(bool banned)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHandleLongDown(bool h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDescGameObject(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InvokeClickEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
