﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomPlayerInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomPlayerInfoUIController : UIControllerBase, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler, IEventSystemHandler
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./PlayerIn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerButton;
    [AutoBind("./PlayerIn/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./PlayerIn/Name/NameWord/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PlayerIn/Name/NameWord/LVText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerIn/Name/NameBG/HeadImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconImage;
    [AutoBind("./PlayerIn/Name/NameBG/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./PlayerIn/Name", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_nameStateCtrl;
    [AutoBind("./PlayerIn/Name/NameWord/PlayerTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerTitle;
    [AutoBind("./PlayerIn/Name/NameWord/PlayerTitle/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakPlayerTitleImage;
    [AutoBind("./PlayerIn/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_graphicGameObject;
    [AutoBind("./PlayerIn/SecretBlessing", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blessingButton;
    [AutoBind("./NoPlayer/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteButton;
    [AutoBind("./Chat", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chatUIStateController;
    [AutoBind("./Chat/BGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_dialogText;
    [AutoBind("./Chat/BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_dialogBigExpression;
    private int m_index;
    private TeamRoomPlayer m_player;
    private UISpineGraphic m_spineGraphic;
    private float m_hideChatTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitEmojiText;
    private static DelegateBridge __Hotfix_SetIndex;
    private static DelegateBridge __Hotfix_GetIndex;
    private static DelegateBridge __Hotfix_SetPlayerIndex;
    private static DelegateBridge __Hotfix_SetPlayer;
    private static DelegateBridge __Hotfix_GetPlayer;
    private static DelegateBridge __Hotfix_SetName;
    private static DelegateBridge __Hotfix_SetPeakTitle;
    private static DelegateBridge __Hotfix_SetLevel;
    private static DelegateBridge __Hotfix_SetHeadIcon;
    private static DelegateBridge __Hotfix_SetBlessing;
    private static DelegateBridge __Hotfix_SetOn;
    private static DelegateBridge __Hotfix_SetEditOn;
    private static DelegateBridge __Hotfix_ShowChat;
    private static DelegateBridge __Hotfix_ShowBigExpression;
    private static DelegateBridge __Hotfix_HideChat;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_SetAnimationTime;
    private static DelegateBridge __Hotfix_GetAnimationTime;
    private static DelegateBridge __Hotfix_OnInviteButtonClick;
    private static DelegateBridge __Hotfix_OnPlayerButtonClick;
    private static DelegateBridge __Hotfix_OnBlessingButtonClick;
    private static DelegateBridge __Hotfix_OnBeginDrag;
    private static DelegateBridge __Hotfix_OnEndDrag;
    private static DelegateBridge __Hotfix_OnDrag;
    private static DelegateBridge __Hotfix_OnDrop;
    private static DelegateBridge __Hotfix_ClearEvents;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_add_EventOnInviteButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnInviteButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPlayerButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBlessingButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnBlessingButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBeginDrag;
    private static DelegateBridge __Hotfix_remove_EventOnBeginDrag;
    private static DelegateBridge __Hotfix_add_EventOnEndDrag;
    private static DelegateBridge __Hotfix_remove_EventOnEndDrag;
    private static DelegateBridge __Hotfix_add_EventOnDrag;
    private static DelegateBridge __Hotfix_remove_EventOnDrag;
    private static DelegateBridge __Hotfix_add_EventOnDrop;
    private static DelegateBridge __Hotfix_remove_EventOnDrop;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEmojiText(SmallExpressionParseDesc desc, Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer GetPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakTitle(int titleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeadIcon(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBlessing(bool blessing)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOn(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEditOn(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChat(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBigExpression(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimationTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlessingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrop(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TeamRoomPlayerInfoUIController> EventOnInviteButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController> EventOnPlayerButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController> EventOnBlessingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController, PointerEventData> EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomPlayerInfoUIController, PointerEventData> EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
