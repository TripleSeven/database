﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelUnlockConditionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RiftLevelUnlockConditionItemUIController : UIControllerBase
  {
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goButton;
    private int m_riftLevelID;
    private int m_achievementRiftLevelID;
    private int m_scenarioID;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_SetCondition;
    private static DelegateBridge __Hotfix_GoScenario;
    private static DelegateBridge __Hotfix_GoAchievement;
    private static DelegateBridge __Hotfix_GoRiftLevel;
    private static DelegateBridge __Hotfix_add_EventOnGoToScenario;
    private static DelegateBridge __Hotfix_remove_EventOnGoToScenario;
    private static DelegateBridge __Hotfix_add_EventOnGoToRiftLevel;
    private static DelegateBridge __Hotfix_remove_EventOnGoToRiftLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelUnlockConditionItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCondition(RiftLevelUnlockConditionType condition, int param1, int param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoRiftLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnGoToScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGoToRiftLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
