﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CombatCharUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CombatCharUIController : UIControllerBase
  {
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    private UISpineGraphic m_spineGraphic;
    private string m_animationName;
    private string m_loopAnimationName;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_CreateGraphic;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_SetDirection;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_HasAnimation;
    private static DelegateBridge __Hotfix_GetAnimationName;
    private static DelegateBridge __Hotfix_ForceUpdate;
    private static DelegateBridge __Hotfix_Update;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string animation, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetAnimationName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
