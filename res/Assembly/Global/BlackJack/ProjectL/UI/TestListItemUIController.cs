﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TestListItemUIController : UIControllerBase
  {
    [AutoBind("./Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Selected", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectedGameObject;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    private int m_id;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetId;
    private static DelegateBridge __Hotfix_GetId;
    private static DelegateBridge __Hotfix_SetName;
    private static DelegateBridge __Hotfix_SetSelected;
    private static DelegateBridge __Hotfix_IsSelected;
    private static DelegateBridge __Hotfix_OnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(Action<UIControllerBase> clickEvent, Action<UIControllerBase> fillEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool selected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClick(UnityAction action)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
