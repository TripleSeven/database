﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.InviteNotifyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class InviteNotifyUIController : UIControllerBase
  {
    [AutoBind("./IconButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_iconUIStateController;
    [AutoBind("./IconButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_iconButton;
    [AutoBind("./IconButton/RedPoint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_countText;
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_panelUIStateController;
    [AutoBind("./Panel/FrameImage/AcceptButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_acceptButton;
    [AutoBind("./Panel/FrameImage/RefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refuseButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/FrameImage/PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./Panel/FrameImage/PlayerInfo/LvValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Panel/FrameImage/PlayerInfo/FBNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleNameText;
    private bool m_isPanelOpened;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OpenIcon;
    private static DelegateBridge __Hotfix_OpenPanel;
    private static DelegateBridge __Hotfix_ClosePanel;
    private static DelegateBridge __Hotfix_IsPanelOpened;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetTeamRoomInviteInfo;
    private static DelegateBridge __Hotfix_SetPVPInviteInfo;
    private static DelegateBridge __Hotfix_UpdateBackgroundButtonActive;
    private static DelegateBridge __Hotfix_OnAcceptButtonClick;
    private static DelegateBridge __Hotfix_OnRefuseButtonClick;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnIconButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAccept;
    private static DelegateBridge __Hotfix_remove_EventOnAccept;
    private static DelegateBridge __Hotfix_add_EventOnRefuse;
    private static DelegateBridge __Hotfix_remove_EventOnRefuse;
    private static DelegateBridge __Hotfix_add_EventOnClickIcon;
    private static DelegateBridge __Hotfix_remove_EventOnClickIcon;

    private InviteNotifyUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPanelOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRoomInviteInfo(TeamRoomInviteInfo info, int count, bool inBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPVPInviteInfo(PVPInviteInfo info, int count, bool isBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBackgroundButtonActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAcceptButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefuseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnIconButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnAccept
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRefuse
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClickIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
