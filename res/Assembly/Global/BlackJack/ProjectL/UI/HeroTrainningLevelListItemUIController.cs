﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroTrainningLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroTrainningLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./Text/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelNameText;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Text/LVNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Text/EnergyValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./Locked", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockedButton;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGroupGameObject;
    [AutoBind("./Locked/LockText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockedText;
    [AutoBind("./SecretBlessingSmall", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blessingButton;
    private ConfigDataHeroTrainningLevelInfo m_heroTrainningLevelInfo;
    private bool m_isLocked;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroTrainningLevelInfo;
    private static DelegateBridge __Hotfix_GetHeroTrainningLevelInfo;
    private static DelegateBridge __Hotfix_SetBlessing;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_IsLocked;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnLockedButtonClick;
    private static DelegateBridge __Hotfix_OnBlessingButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnLevelRaidComplete;
    private static DelegateBridge __Hotfix_remove_EventOnLevelRaidComplete;
    private static DelegateBridge __Hotfix_add_EventOnStartButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnStartButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroTrainningLevelInfo(ConfigDataHeroTrainningLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroTrainningLevelInfo GetHeroTrainningLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBlessing(bool blessing)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool locked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockedButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlessingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnLevelRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroTrainningLevelListItemUIController> EventOnStartButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
