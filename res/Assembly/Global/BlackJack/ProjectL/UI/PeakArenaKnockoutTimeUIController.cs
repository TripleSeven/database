﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutTimeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaKnockoutTimeUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/ListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_listGroupTransform;
    [AutoBind("./Detail/ListGroup/ListItem", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_itemPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_GetSeasonTimeTable;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_OnCloseClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataPeakArenaKnockoutMatchTime> GetSeasonTimeTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private class Item : UIControllerBase
    {
      [AutoBind("./DateText", AutoBindAttribute.InitState.NotInit, false)]
      private Text m_dateText;
      [AutoBind("./TimeText", AutoBindAttribute.InitState.NotInit, false)]
      private Text m_timeText;
      [AutoBind("./MatchText", AutoBindAttribute.InitState.NotInit, false)]
      private Text m_matchText;
      [AutoBind("./TypeText", AutoBindAttribute.InitState.NotInit, false)]
      private Text m_typeText;
      private ProjectLPlayerContext m_playerContext;
      private IConfigDataLoader m_configDataLoader;
      private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
      private static DelegateBridge __Hotfix_SetData;

      [MethodImpl((MethodImplOptions) 32768)]
      protected override void OnBindFiledsCompleted()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetData(ConfigDataPeakArenaKnockoutMatchTime matchTime)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
