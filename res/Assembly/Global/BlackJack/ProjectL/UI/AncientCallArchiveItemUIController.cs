﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallArchiveItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallArchiveItemUIController : UIControllerBase
  {
    public Action<int> EventOnBossDetailClick;
    public Action<int> EventOnAchieveListClick;
    [AutoBind("./BossDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bossDetailButton;
    [AutoBind("./AchievementListButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementListButton;
    [AutoBind("./AchievementListButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redPointGameObject;
    [AutoBind("./BossNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossNameText;
    [AutoBind("./BossImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossImage;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private ConfigDataAncientCallBossInfo m_ancientCallBossInfo;
    private float m_lastUpdateTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetAncientCallBossInfo;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewRedPoint;
    private static DelegateBridge __Hotfix_OnBossDetailClick;
    private static DelegateBridge __Hotfix_OnAchievementListClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAncientCallBossInfo(ConfigDataAncientCallBossInfo ancientCallBossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossDetailClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementListClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
