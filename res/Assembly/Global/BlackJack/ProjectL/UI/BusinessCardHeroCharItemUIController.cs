﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BusinessCardHeroCharItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BusinessCardHeroCharItemUIController : UIControllerBase
  {
    private int m_positionIndex = -1;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./Empty", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_emptyGameObject;
    private BattleHero m_battleHero;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroCharItem;
    private static DelegateBridge __Hotfix_SetNoHeroChar;
    private static DelegateBridge __Hotfix_GetBattleHero;
    private static DelegateBridge __Hotfix_GetPositionIndex;
    private static DelegateBridge __Hotfix_IsEmpty;
    private static DelegateBridge __Hotfix_OnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCharItem(BattleHero battleHero, int positionIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNoHeroChar(int positionIndex, bool isMe)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero GetBattleHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPositionIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BusinessCardHeroCharItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
