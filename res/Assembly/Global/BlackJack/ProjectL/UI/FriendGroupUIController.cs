﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendGroupUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FriendGroupUIController : UIControllerBase
  {
    [AutoBind("./GroupIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_groupIconImage;
    [AutoBind("./GroupIconImage/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_groupHeadFrameTransform;
    [AutoBind("./ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./WatchStaffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchStaffButton;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlinePlayerNumText;
    [AutoBind("./GroupNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_groupNameText;
    private IConfigDataLoader m_configDataLoader;
    private ProChatGroupCompactInfo m_groupInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetGroupInfo;
    private static DelegateBridge __Hotfix_GetGroupInfo;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnWatchStaffButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnWatchGroupStaffButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnWatchGroupStaffButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnChatButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnChatButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendGroupUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupInfo(ProChatGroupCompactInfo groupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatGroupCompactInfo GetGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchStaffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FriendGroupUIController> EventOnWatchGroupStaffButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendGroupUIController> EventOnChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
