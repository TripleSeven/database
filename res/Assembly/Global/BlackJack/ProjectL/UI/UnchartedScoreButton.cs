﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedScoreButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedScoreButton : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./BGImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_imageGrey;
    [AutoBind("./InfoTextGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./InfoTextGroup/TimeTextGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeGreyText;
    [AutoBind("./InfoTextGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_numberText;
    private ConfigDataUnchartedScoreInfo m_unchartedScoreInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetUnchartedScoreInfo;
    private static DelegateBridge __Hotfix_GetUnchartedScoreInfo;
    private static DelegateBridge __Hotfix_OnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStartUnchartedScore;
    private static DelegateBridge __Hotfix_remove_EventOnStartUnchartedScore;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnchartedScoreInfo(ConfigDataUnchartedScoreInfo unchartedScoreInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataUnchartedScoreInfo GetUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<UnchartedScoreButton> EventOnStartUnchartedScore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
