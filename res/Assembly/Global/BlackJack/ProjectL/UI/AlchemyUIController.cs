﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AlchemyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using IL;
using MarchingBytes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AlchemyUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./PlayerResource/MithralStone/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_mithralStoneText;
    [AutoBind("./PlayerResource/BrillianceMithralStone/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_brillianceMithralStoneText;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Item", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Equipment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Fragment", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_fragmentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/JobMaterial", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobMaterialToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Strengthen", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_strengthToggle;
    [AutoBind("./AlchemyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_alchemyButton;
    [AutoBind("./AlchemyButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_alchemyButtonStateCtrl;
    [AutoBind("./L_gold metallurgy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectGameObject;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./BagListPanel/BagListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./BagListPanel/BagListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemTemplateRoot;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollViewBagItemContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/BgContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointBgContent;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bagListPointItem;
    [AutoBind("./RewardListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardListPanelGo;
    [AutoBind("./RewardListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardListPanelStateCtrl;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/RewardGroup/Viewport/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardsContent;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/Nothing", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardsNothingGo;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/InfoGroup/MaterialCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardsMaterialCountText;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/InfoGroup/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardsGoldenValueText;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/NoticText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardsNoticTextGo;
    [AutoBind("./RewardListPanel/BGImage/FrameImage/NoticText/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardsNoticText;
    [AutoBind("./UseItemsPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_subBagInfoPanelObj;
    [AutoBind("./UseItemsPanel/PanelDetail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_subItemNumInputField;
    [AutoBind("./UseItemsPanel/PanelDetail/UseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemUseButton;
    [AutoBind("./UseItemsPanel/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemPanelReturnButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Minus", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMinusButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Add", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemAddButton;
    [AutoBind("./UseItemsPanel/PanelDetail/Max", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_subItemMaxButton;
    [AutoBind("./UseItemsPanel/PanelDetail/AvailalbeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_subItemAvailalbeValueText;
    [AutoBind("./BagListPanel/BagListScrollView/Viewport", AutoBindAttribute.InitState.Active, false)]
    private GameObject m_bagListViewport;
    [AutoBind("./BagListPanel/BagEmptyPanel", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_bagEmptyPanel;
    [AutoBind("./Desc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descGo;
    [AutoBind("./Desc/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descReturnBgButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descLockButtonStateCtrl;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTitleText;
    [AutoBind("./Desc/Lay/FrameImage/Top/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Desc/Lay/FrameImage/Top/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Desc/Lay/FrameImage/Top/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Desc/Lay/FrameImage/Top/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descLvText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descExpText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descEquipLimitContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillContent;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descSkillContentStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descUnlockCoditionText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillNameText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillLvText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillOwnerText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillOwnerBGImage;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillDescText;
    [AutoBind("./Desc/Lay/FrameImage/Button/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descNotEquipSkillTip;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropATGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropATValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropHPGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropHPValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagiccGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagicDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDexGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDexValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    private AlchemyUIController.DisplayType m_displayType;
    private ulong m_curDescEquipmentInstanceId;
    private List<BagItemBase> m_bagItemCache;
    private Dictionary<int, List<BagItemBase>> m_enhancementItemCacheMap;
    private List<BagItemUIController> m_bagItemCtrlList;
    private Dictionary<BagItemBase, int> m_checkBagItemToCountDict;
    private BagItemBase m_lastCheckBagItem;
    private bool m_isNeedResetToTop;
    private int m_canAlchemyListIndex;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitLoopScrollViewRect;
    private static DelegateBridge __Hotfix_OnPoolObjectCreated;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_SetPointBgContent;
    private static DelegateBridge __Hotfix_IsBagItemOfDisplayType;
    private static DelegateBridge __Hotfix_BagItemComparer;
    private static DelegateBridge __Hotfix_OnBagItemClick;
    private static DelegateBridge __Hotfix_ShowRewardListPanel;
    private static DelegateBridge __Hotfix_CloseRewardListPanel;
    private static DelegateBridge __Hotfix_OnBagItemNeedFill;
    private static DelegateBridge __Hotfix_OpenSubUseItemPanel;
    private static DelegateBridge __Hotfix_SetLastAlchemyCount;
    private static DelegateBridge __Hotfix_GetLastAlchemyCount;
    private static DelegateBridge __Hotfix_OnSubItemUseItemClick;
    private static DelegateBridge __Hotfix_OnItemMinusButtonClick;
    private static DelegateBridge __Hotfix_OnItemAddButtonClick;
    private static DelegateBridge __Hotfix_OnItemMaxButtonClick;
    private static DelegateBridge __Hotfix_CloseSubItemUsePanel;
    private static DelegateBridge __Hotfix_OnInputEditEnd;
    private static DelegateBridge __Hotfix_SetEquipmentItemDesc;
    private static DelegateBridge __Hotfix_SetDescEquipmentLimit;
    private static DelegateBridge __Hotfix_SetDescEquipmentSkill;
    private static DelegateBridge __Hotfix_SetDescEquipmentEnchant;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_CloseEquipmentDescPanel;
    private static DelegateBridge __Hotfix_OnDescLockButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnAlchemyButtonClick;
    private static DelegateBridge __Hotfix_OnAlchemySucceed;
    private static DelegateBridge __Hotfix_ResetAlchemyUIView;
    private static DelegateBridge __Hotfix_GetEnhancementItemCache;
    private static DelegateBridge __Hotfix_AddEnhancementItemCache;
    private static DelegateBridge __Hotfix_RemoveEnhancementItemCache;
    private static DelegateBridge __Hotfix_HandleUseEnhancementItem;
    private static DelegateBridge __Hotfix_SortEnhancementItemCache;
    private static DelegateBridge __Hotfix_ClearEnhancementItemCacheMap;
    private static DelegateBridge __Hotfix_GetItemCount;
    private static DelegateBridge __Hotfix_IsEquipmentItem;
    private static DelegateBridge __Hotfix_IsEnhancementItem;
    private static DelegateBridge __Hotfix_IsLockedEquipmentItem;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_OnItemToggleValueChanged;
    private static DelegateBridge __Hotfix_OnJobMaterialToggleValueChanged;
    private static DelegateBridge __Hotfix_OnEquipmentToggleValueChanged;
    private static DelegateBridge __Hotfix_OnFragmentToggleValueChanged;
    private static DelegateBridge __Hotfix_OnStrengthenToggleValueChanged;
    private static DelegateBridge __Hotfix_OnToggleChanged;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnAlchemyButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAlchemyButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnLockButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnLockButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public AlchemyUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPointBgContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagItemOfDisplayType(BagItemBase itm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int BagItemComparer(BagItemBase item1, BagItemBase item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRewardListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseRewardListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenSubUseItemPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLastAlchemyCount(string inputString = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetLastAlchemyCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSubItemUseItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMinusButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSubItemUsePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInputEditEnd(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentLimit(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentSkill(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentEnchant(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipmentDescPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAlchemyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnAlchemySucceed(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetAlchemyUIView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BagItemBase> GetEnhancementItemCache(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEnhancementItemCache(int id, BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveEnhancementItemCache(int id, BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleUseEnhancementItem(BagItemBase item, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortEnhancementItemCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearEnhancementItemCacheMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetItemCount(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentItem(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEnhancementItem(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLockedEquipmentItem(BagItemBase item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFragmentToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrengthenToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<ProGoods>, Action<List<Goods>>> EventOnAlchemyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, Action> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum DisplayType
    {
      None,
      Item,
      Fragment,
      JobMaterial,
      Equipment,
      Strengthen,
    }
  }
}
