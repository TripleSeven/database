﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./TopGroup/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./TopGroup/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/LeftPanel/DetailInfoToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankButton;
    [AutoBind("./Margin/LeftPanel/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardButton;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerGroup;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup/PlayerInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_playerInfoPrefab;
    [AutoBind("./Margin/LeftPanel/NoDetail", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_rankingListNoDetailGo;
    [AutoBind("./BossListSelect", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_bossListScrollRect;
    [AutoBind("./BossListSelect", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollSnapCenter m_bossScrollSnapCenter;
    [AutoBind("./BossListSelect/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_bossListContent;
    [AutoBind("./BossListSelect/Viewport/Content/BossListSelectIcon", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_bossItemPrefab;
    [AutoBind("./Margin/TimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./Margin/BattleDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bossArchiveButton;
    [AutoBind("./Margin/BattleDetailButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_archiveButtonRedPointGameObject;
    [AutoBind("./RankingReward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankRewardPanel;
    [AutoBind("./BossBattleDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossArchivePanel;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private AncientCallUITask m_ancientCallUITask;
    private AncientCallRankRewardUIController m_rewardUIController;
    private AncientCallArchiveUIController m_ancientCallArchiveUIController;
    private GameObjectPool<AncientCallBossItemUIController> bossItemPool;
    private List<int> m_bossIdList;
    private List<AncientCallBossItemUIController> m_bossItemUIControllerList;
    private List<AncientCallPlayerInfoItemUIController> m_rankPlayerItemUIControllerList;
    private bool isFirstUpdateView;
    private int m_curBossItemIndex;
    private float m_lastUpdateTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_SetBossListScrollPos;
    private static DelegateBridge __Hotfix_UpdateData;
    private static DelegateBridge __Hotfix_UpdateDataPlayerRank;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewPlayerRank;
    private static DelegateBridge __Hotfix_UpdateViewRedPoint;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_OnBossItemClick;
    private static DelegateBridge __Hotfix_OnBossItemCenterChange;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnHelpClick;
    private static DelegateBridge __Hotfix_OnRewardClick;
    private static DelegateBridge __Hotfix_OnRankClick;
    private static DelegateBridge __Hotfix_OnBossArchiveClick;
    private static DelegateBridge __Hotfix_OnDailyFlushFinish;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBossListScrollPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDataPlayerRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewPlayerRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossItemClick(
      AncientCallBossItemUIController bossItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossItemCenterChange(int centerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossArchiveClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDailyFlushFinish()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
