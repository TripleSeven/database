﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.IOSARShowSceneController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.ProjectL.AR;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class IOSARShowSceneController : ARShowSceneController
  {
    [AutoBind("./ARCamera", AutoBindAttribute.InitState.NotInit, false)]
    private Camera m_camera;
    [AutoBind("./FocusSquare/FocusSquare", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_focusSquare;
    [AutoBind("./FocusSquare/FocusSquare/Quad", AutoBindAttribute.InitState.NotInit, false)]
    private Renderer m_focusSquareRenderer;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charNode;
    [AutoBind("./CharDraw", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charDrawNode;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGroupNode;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_CreatePlaneTrace;
    private static DelegateBridge __Hotfix_get_focusSquare;
    private static DelegateBridge __Hotfix_get_charNode;
    private static DelegateBridge __Hotfix_get_charDrawNode;
    private static DelegateBridge __Hotfix_get_charGroupNode;
    private static DelegateBridge __Hotfix_get_focusSquareRenderer;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override ARPlaneTrace CreatePlaneTrace()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override GameObject focusSquare
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override GameObject charNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override GameObject charDrawNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override GameObject charGroupNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override Renderer focusSquareRenderer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
