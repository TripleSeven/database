﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamUIStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/ExchangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exchangeButton;
    [AutoBind("./Margin/ExchangeButton/Redmark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_exchangeRedPointGameObject;
    [AutoBind("./Margin/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/ChatButton/Redmark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatRedPointGameObject;
    [AutoBind("./NameGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./LevelListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_levelListUIStateController;
    [AutoBind("./LevelListPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_levelListBackgroundButton;
    [AutoBind("./LevelListPanel/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./ItemListAndScore", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardTypeUIStateController;
    [AutoBind("./ItemListAndScore/ItemList/ItemListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyItemListGroup;
    [AutoBind("./ItemListAndScore/ScorePanel/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreRewardButton;
    [AutoBind("./ItemListAndScore/ScorePanel/RecommendHeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendHeroButton;
    [AutoBind("./ItemListAndScore/ScorePanel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./ScoreRewardPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_scoreRewardUIStateController;
    [AutoBind("./ScoreRewardPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreRewardBGButton;
    [AutoBind("./ScoreRewardPanel/Detail/ListScrollView/Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private DynamicGrid m_scoreRewardGrid;
    [AutoBind("./ScoreRewardPanel/Detail/CountScore/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreRewardScoreText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/LevelItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelItemPrefab;
    [AutoBind("./Prefabs/ResourceItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyItemPrefab;
    [AutoBind("./EnterMonster", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enterMonsterUIStateController;
    private ConfigDataCollectionActivityInfo m_collectionActivityInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetCollectionActivity;
    private static DelegateBridge __Hotfix_SetCurrencyItemList;
    private static DelegateBridge __Hotfix_SetScore;
    private static DelegateBridge __Hotfix_SetCanTeamBattle;
    private static DelegateBridge __Hotfix_ShowLevelList;
    private static DelegateBridge __Hotfix_HideLevelList;
    private static DelegateBridge __Hotfix_ClearLevelList;
    private static DelegateBridge __Hotfix_AddLevelListItem_0;
    private static DelegateBridge __Hotfix_AddLevelListItem_1;
    private static DelegateBridge __Hotfix_AddLevelListItem_2;
    private static DelegateBridge __Hotfix_ShowEnterMonster;
    private static DelegateBridge __Hotfix_Co_EnterMonster;
    private static DelegateBridge __Hotfix_SetChatUnreadCount;
    private static DelegateBridge __Hotfix_SetExchangeRedMarkActive;
    private static DelegateBridge __Hotfix_OnScoreRewardGridCellInit;
    private static DelegateBridge __Hotfix_OnScoreRewardGridCellUpdate;
    private static DelegateBridge __Hotfix_ShowScoreReward;
    private static DelegateBridge __Hotfix_HideScoreReward;
    private static DelegateBridge __Hotfix_ShowRecommendHero;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnTeamButtonClick;
    private static DelegateBridge __Hotfix_OnExchangeButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnScoreRewardButtonClick;
    private static DelegateBridge __Hotfix_OnRecommendHeroButtonClick;
    private static DelegateBridge __Hotfix_OnScoreRewardCloseButtonClick;
    private static DelegateBridge __Hotfix_OnLevelLickBackgroundButtonClick;
    private static DelegateBridge __Hotfix_CollectionActivityLevelListItem_OnStartButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnShowTeam;
    private static DelegateBridge __Hotfix_remove_EventOnShowTeam;
    private static DelegateBridge __Hotfix_add_EventOnShowExchange;
    private static DelegateBridge __Hotfix_remove_EventOnShowExchange;
    private static DelegateBridge __Hotfix_add_EventOnShowChat;
    private static DelegateBridge __Hotfix_remove_EventOnShowChat;
    private static DelegateBridge __Hotfix_add_EventOnStartScenarioLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartScenarioLevel;
    private static DelegateBridge __Hotfix_add_EventOnStartChallengeLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartChallengeLevel;
    private static DelegateBridge __Hotfix_add_EventOnStartLootLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartLootLevel;

    private CollectionActivityUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCollectionActivity(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrencyItemList(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScore(int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanTeamBattle(bool canTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowLevelList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideLevelList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLevelList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelListItem(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo,
      bool isFinished,
      int unlockDay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelListItem(
      ConfigDataCollectionActivityChallengeLevelInfo levelInfo,
      bool isFinished,
      int unlockDay,
      int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelListItem(
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      bool isFinished,
      int unlockDay,
      bool isPreLevelLocked,
      int playerLevelRequired)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnterMonster(ConfigDataBattleInfo battleInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_EnterMonster(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChatUnreadCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExchangeRedMarkActive(bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DynamicGridCellController OnScoreRewardGridCellInit(
      GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreRewardGridCellUpdate(DynamicGridCellController cell)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowScoreReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideScoreReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowRecommendHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendHeroButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelLickBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityLevelListItem_OnStartButtonClick(
      CollectionActivityLevelListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowExchange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataCollectionActivityScenarioLevelInfo> EventOnStartScenarioLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataCollectionActivityChallengeLevelInfo> EventOnStartChallengeLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataCollectionActivityLootLevelInfo> EventOnStartLootLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
