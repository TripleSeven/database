﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildStoreUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildStoreUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_storeAnimation;
    [AutoBind("./StoreUIPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./StoreUIPanel/UnderItemList/StoreList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeItemContent;
    [AutoBind("./Prefab/StoreItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_storeItem;
    [AutoBind("./StoreUIPanel/PlayerResource", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerCurrencyNode;
    [AutoBind("./StoreUIPanel/Char/CharNpc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charDummy;
    private GuildStoreUITask m_guildStoreUITask;
    private CurrencyUIController m_currencyUIController;
    private GuildFixedStoreUIComponent m_guildFixedStoreUIComponent;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SwitchTab;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnAddCurrencyClick;
    private static DelegateBridge __Hotfix_OnBuyItemSuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GuildStoreUITask guildStoreUITask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchTab(StoreId storeId, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAddCurrencyClick(GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyItemSuccess()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
