﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MarginAdjust
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class MarginAdjust : MonoBehaviour
  {
    public MarginAdjust.AdjustType adjustType;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_Start;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum AdjustType
    {
      Horizontal,
      Vertical,
    }
  }
}
