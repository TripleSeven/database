﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomListItemUIController : UIControllerBase
  {
    [AutoBind("./JoinButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_joinButton;
    [AutoBind("./NameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gameFunctionTypeNameText;
    [AutoBind("./NameGroup/LevelValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_locationNameText;
    [AutoBind("./LevelGroup/LevelValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    private GameFunctionType m_gameFunctionType;
    private int m_locationId;
    private int m_roomId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetGameFunctionTypeName;
    private static DelegateBridge __Hotfix_SetLocationName;
    private static DelegateBridge __Hotfix_SetRoomId;
    private static DelegateBridge __Hotfix_GetRoomId;
    private static DelegateBridge __Hotfix_SetGameFunctionType;
    private static DelegateBridge __Hotfix_GetGameFunctionType;
    private static DelegateBridge __Hotfix_SetLocationId;
    private static DelegateBridge __Hotfix_GetLocationIdId;
    private static DelegateBridge __Hotfix_SetPlayer;
    private static DelegateBridge __Hotfix_SetPlayerOff;
    private static DelegateBridge __Hotfix_GetPlayerInfoGameObject;
    private static DelegateBridge __Hotfix_SetPlayerLevelRange;
    private static DelegateBridge __Hotfix_OnJoinButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnJoinButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnJoinButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameFunctionTypeName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocationName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRoomId(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRoomId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameFunctionType(GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameFunctionType GetGameFunctionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocationId(int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLocationIdId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(int index, int headIconId, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerOff(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetPlayerInfoGameObject(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevelRange(int levelMin, int levelMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJoinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TeamRoomListItemUIController> EventOnJoinButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
