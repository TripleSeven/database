﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BackFlowMissonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BackFlowMissonUIController : UIControllerBase
  {
    [AutoBind("./InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoText;
    [AutoBind("./ItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemDummy;
    [AutoBind("./ProgressBarBGImage/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressBar;
    [AutoBind("./ProgressBarBGImage/ProgressBarValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_progressBarValueStateCtrl;
    [AutoBind("./ProgressBarBGImage/ProgressBarValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressHaveCount;
    [AutoBind("./ProgressBarBGImage/ProgressBarValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressNeedCount;
    [AutoBind("./ProgressBarBGImage/DoneText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressDoneText;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupStateCtrl;
    [AutoBind("./ButtonGroup/GoToButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gotoButton;
    [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitOpenServiceMissonInfo;
    private static DelegateBridge __Hotfix_SetOpenServiceMissionInfo;
    private static DelegateBridge __Hotfix_SetOpenServiceMissionState;
    private static DelegateBridge __Hotfix_OnGotoButtonClick;
    private static DelegateBridge __Hotfix_OnGetButtonClick;
    private static DelegateBridge __Hotfix_PlayGetRewardEffect;
    private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGetButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGetButtonClick;
    private static DelegateBridge __Hotfix_set_Mission;
    private static DelegateBridge __Hotfix_get_Mission;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOpenServiceMissonInfo(Mission mission, bool hasGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetOpenServiceMissionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetOpenServiceMissionState(bool hasGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayGetRewardEffect(Action OnStateFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GetPathData> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BackFlowMissonUIController> EventOnGetButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Mission Mission
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
