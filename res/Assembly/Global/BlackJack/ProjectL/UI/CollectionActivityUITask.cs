﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityUITask : UITask, ICollectionActivityWorldListener
  {
    public const string ParamKey_CollectionActivityInstanceId = "CollectionActivityInstanceId";
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private CollectionActivityUIController m_collectionActivityUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private CollectionActivityWorld m_collectionActivityWorld;
    private List<int> m_movePath;
    private int m_nowSeconds;
    private IConfigDataLoader m_configDataLoader;
    private ConfigDataCollectionActivityInfo m_collectionActivityInfo;
    private int m_needCollectionScenarioHandleLevelId;
    private bool m_isAfterBattle;
    private bool m_isEnterFirstScenarioFail;
    private bool m_needUpdateEventActors;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_CreateCollectionActivityWorld;
    private static DelegateBridge __Hotfix_DestroyCollectionActivityWorld;
    private static DelegateBridge __Hotfix_StartCollectionActivityWorld;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectPlayerAssets;
    private static DelegateBridge __Hotfix_CollectEventAssets;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_InitCollectionActivityUIController;
    private static DelegateBridge __Hotfix_UninitCollectionActivityUIController;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_CheckEnterFirstScenarioLevel;
    private static DelegateBridge __Hotfix_CheckChangeActiveScenarioLevel;
    private static DelegateBridge __Hotfix_UpdateActiveScenarioAndWaypointState;
    private static DelegateBridge __Hotfix_UpdateActiveEventState;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_OnSlowTick;
    private static DelegateBridge __Hotfix_UpdateChatUnreadCount;
    private static DelegateBridge __Hotfix_LoadAndUpdateEventActors;
    private static DelegateBridge __Hotfix_UpdateExchangeRedMark;
    private static DelegateBridge __Hotfix_StartCollectionScenarioHandleNetTask;
    private static DelegateBridge __Hotfix_StartScenarioLevel;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnReturn;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_HasUnlockLootLevel;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnShowTeam;
    private static DelegateBridge __Hotfix_TeamUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnShowExchange;
    private static DelegateBridge __Hotfix_CollectionActivityExchangeUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnShowChat;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnStartScenarioLevel;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnStartChallengeLevel;
    private static DelegateBridge __Hotfix_CollectionActivityUIController_OnStartLootLevel;
    private static DelegateBridge __Hotfix_PlayerContext_OnCollectionEventAdd;
    private static DelegateBridge __Hotfix_PlayerContext_OnCollectionEventDelete;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_StartWaypointEvent;
    private static DelegateBridge __Hotfix_OnHappeningStepEnd;
    private static DelegateBridge __Hotfix_StartWorldEventMissionUITask;
    private static DelegateBridge __Hotfix_WorldEventMissionUITask_OnClose;
    private static DelegateBridge __Hotfix_RemoveWorldEventMissionUITaskEvents;
    private static DelegateBridge __Hotfix_WorldEventMissionUITask_OnCheckEnterMission;
    private static DelegateBridge __Hotfix_WorldEventMissionUITask_OnEnterMission;
    private static DelegateBridge __Hotfix_ShowWaypointReward;
    private static DelegateBridge __Hotfix_GetRewardGoodsUITask_OnClose;
    private static DelegateBridge __Hotfix_ChestUITask_OnClose;
    private static DelegateBridge __Hotfix_OnWaypointClick;
    private static DelegateBridge __Hotfix_PlayerArriveWaypoint;
    private static DelegateBridge __Hotfix_ShowLevelList;
    private static DelegateBridge __Hotfix_StartCollectionWayPointMoveNetTask;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPlayerAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEventAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCollectionActivityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitCollectionActivityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckEnterFirstScenarioLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckChangeActiveScenarioLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActiveScenarioAndWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActiveEventState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSlowTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChatUnreadCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadAndUpdateEventActors(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateExchangeRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionScenarioHandleNetTask(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartScenarioLevel(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasUnlockLootLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowExchange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityExchangeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnStartScenarioLevel(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnStartChallengeLevel(
      ConfigDataCollectionActivityChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUIController_OnStartLootLevel(
      ConfigDataCollectionActivityLootLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnCollectionEventAdd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnCollectionEventDelete()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWaypointEvent(ConfigDataCollectionEventInfo collectinEventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHappeningStepEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWorldEventMissionUITask(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveWorldEventMissionUITaskEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool WorldEventMissionUITask_OnCheckEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowWaypointReward(BattleReward reward, bool isChest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerArriveWaypoint(
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      ConfigDataCollectionActivityWaypointInfo prevWaypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowLevelList(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionWayPointMoveNetTask(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
