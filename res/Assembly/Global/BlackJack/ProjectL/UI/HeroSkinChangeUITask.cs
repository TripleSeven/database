﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSkinChangeUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroSkinChangeUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string Mode_SkinBuy = "SkinBuyMode";
    public const string Mode_ShowSkinDetail = "ShowSkinDetailMode";
    public const string Mode_ShowOneSkin = "ShowOneSkin";
    private const string ParamKey_StoreId = "StoreId";
    private const string ParamKey_HeroSkinInfoId = "HeroSkinInfoId";
    private const string ParamKey_HeroSkinMode = "HeroSkinMode";
    private const string ParamKey_HeroObj = "HeroObj";
    private StoreId m_storeId;
    private int m_heroSkinInfoId;
    private string m_skinMode;
    private Hero m_hero;
    private HeroSkinChangeUIController m_heroSkinChangeUIController;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_HeroSkinChangeUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnNewIntent;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_HeroCharSkinChangeUIController_OnClose;
    private static DelegateBridge __Hotfix_HeroCharSkinChangeUIController_OnBuySkin;
    private static DelegateBridge __Hotfix_HeroCharSkinChangeUIController_OnTakeOffCharSkin;
    private static DelegateBridge __Hotfix_HeroCharSkinChangeUIController_OnWearCharSkin;
    private static DelegateBridge __Hotfix_HeroCharSkinChangeUIController_OnWearModelSkin;
    private static DelegateBridge __Hotfix_HeroCharSkinChangeUIController_OnTakeOffModelSkin;
    private static DelegateBridge __Hotfix_HeroSkinChangeUIController_OnSkinChangedPreview;
    private static DelegateBridge __Hotfix_HeroSkinChangeUIController_OnSkinTicketBuy;
    private static DelegateBridge __Hotfix_ClearUIControllerData;
    private static DelegateBridge __Hotfix_CloseHeroSkinChangePanelWithAnim;
    private static DelegateBridge __Hotfix_add_EventOnPause2;
    private static DelegateBridge __Hotfix_remove_EventOnPause2;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnBuySkinSucceed;
    private static DelegateBridge __Hotfix_remove_EventOnBuySkinSucceed;
    private static DelegateBridge __Hotfix_add_EventOnSkinChangedPreview;
    private static DelegateBridge __Hotfix_remove_EventOnSkinChangedPreview;
    private static DelegateBridge __Hotfix_add_EventOnAddSkinTicket;
    private static DelegateBridge __Hotfix_remove_EventOnAddSkinTicket;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSkinChangeUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroSkinChangeUITask StartUITask(
      string mode,
      StoreId storeId,
      int heroSkinInfoId = 0,
      Hero hero = null,
      UIIntent preUIIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HeroSkinChangeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnBuySkin(int skinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnTakeOffCharSkin(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnWearCharSkin(
      int heroId,
      int charSkinId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnWearModelSkin(
      int heroId,
      int jobRelatedId,
      int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCharSkinChangeUIController_OnTakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSkinChangeUIController_OnSkinChangedPreview(string spinePath, int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSkinChangeUIController_OnSkinTicketBuy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearUIControllerData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseHeroSkinChangePanelWithAnim(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPause2
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuySkinSucceed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int> EventOnSkinChangedPreview
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddSkinTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
