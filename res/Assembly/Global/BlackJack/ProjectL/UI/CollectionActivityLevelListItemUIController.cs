﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./TypeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_levelTypeUIStateController;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./TypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_typeText;
    [AutoBind("./Level", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelGameObject;
    [AutoBind("./Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Energy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_energyText;
    [AutoBind("./RewardGoodsList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsListGameObject;
    [AutoBind("./Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockButton;
    [AutoBind("./Lock/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockText;
    private ConfigDataCollectionActivityScenarioLevelInfo m_scenarioLevelInfo;
    private ConfigDataCollectionActivityChallengeLevelInfo m_challengeLevelInfo;
    private ConfigDataCollectionActivityLootLevelInfo m_lootLevelInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_AppendScoreItemToList;
    private static DelegateBridge __Hotfix_SetLevelInfo_1;
    private static DelegateBridge __Hotfix_SetLevelInfo_0;
    private static DelegateBridge __Hotfix_SetLevelInfo_2;
    private static DelegateBridge __Hotfix_SetLevelType;
    private static DelegateBridge __Hotfix_SetLevelText;
    private static DelegateBridge __Hotfix_SetRewordGoods;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_SetUnlocked;
    private static DelegateBridge __Hotfix_SetOpenDayLocked;
    private static DelegateBridge __Hotfix_SetPreLevelLocked;
    private static DelegateBridge __Hotfix_SetPlayerLevelLocked;
    private static DelegateBridge __Hotfix_SetFinished;
    private static DelegateBridge __Hotfix_GetScenarioLevelInfo;
    private static DelegateBridge __Hotfix_GetChallengeLevelInfo;
    private static DelegateBridge __Hotfix_GetLootLevelInfo;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnLockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStart;
    private static DelegateBridge __Hotfix_remove_EventOnStart;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendScoreItemToList(
      ConfigDataCollectionActivityInfo collectionActivityInfo,
      int score,
      List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelInfo(
      ConfigDataCollectionActivityScenarioLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelInfo(
      ConfigDataCollectionActivityChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelInfo(
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      bool isFirst)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevelType(StringTableId typeName, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevelText(string name, int monsterLevel, int energy, bool showLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewordGoods(List<Goods> rewardList, int displayCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLocked(bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnlocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpenDayLocked(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPreLevelLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevelLocked(int playerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityScenarioLevelInfo GetScenarioLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityChallengeLevelInfo GetChallengeLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityLootLevelInfo GetLootLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<CollectionActivityLevelListItemUIController> EventOnStart
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
