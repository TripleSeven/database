﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChestUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ChestUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./ChestObject", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chestGameObject;
    [AutoBind("./ChestObject/Chest", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chestUIStateController;
    [AutoBind("./FirstWin", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_firstWinGameObject;
    [AutoBind("./RewardGoodsGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardGoodsGroupGameObject;
    [AutoBind("./TeamRewardInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamRewardUIStateController;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friendship", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamRewardFriendshipGameObject;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friendship/ValuGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamRewardFriendshipValueText;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Team", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamRewardTeamGameObject;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Team/ValuGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamRewardTeamValueText;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamRewardFriendGameObject;
    [AutoBind("./TeamRewardInfo/Detail/RewardGroup/Friend/ValuGroup/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamRewardFriendValueText;
    [AutoBind("./DailyRewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_dailyRewardUIStateController;
    [AutoBind("./DailyRewardGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyRewardText;
    [AutoBind("./ClickScreenContinue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickScreenContinueGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/GoodsTweenDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goodsTweenDummyPrefab;
    private List<RewardGoodsUIController> m_rewardGoods;
    private bool m_isClick;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowReward;
    private static DelegateBridge __Hotfix_Co_ShowReward;
    private static DelegateBridge __Hotfix_Co_SetAndWaitUIState;
    private static DelegateBridge __Hotfix_Co_PlayAndWaitTween;
    private static DelegateBridge __Hotfix_Co_WaitClick;
    private static DelegateBridge __Hotfix_CreateGoodsTweenDummy;
    private static DelegateBridge __Hotfix_ClearGoodsDummyGroup;
    private static DelegateBridge __Hotfix_ClearRewardItems;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    private ChestUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowReward(BattleReward reward, bool isFirstWin, bool isAutoOpen)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowReward(
      BattleReward reward,
      bool isFirstWin,
      bool isAutoOpen)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayAndWaitTween(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateGoodsTweenDummy(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGoodsDummyGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRewardItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
