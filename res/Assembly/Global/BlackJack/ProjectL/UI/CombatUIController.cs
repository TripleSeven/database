﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CombatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CombatUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/AutoOffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoOffButton;
    [AutoBind("./Margin/ArenaAutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaAutoButton;
    [AutoBind("./CutsceneSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cutsceneSkillUIStateController;
    [AutoBind("./CutsceneSkill/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_cutsceneSkillIconImage;
    [AutoBind("./CutsceneSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cutsceneSkillNameText;
    [AutoBind("./Bottom/HeroHP0/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHp0Image;
    [AutoBind("./Bottom/HeroHP0/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroHp0Text;
    [AutoBind("./Bottom/HeroHP0/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroHp0FxGameObeject;
    [AutoBind("./Bottom/HeroHP1/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHp1Image;
    [AutoBind("./Bottom/HeroHP1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroHp1Text;
    [AutoBind("./Bottom/HeroHP1/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroHp1FxGameObeject;
    [AutoBind("./Bottom/SoldierHP0/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierHp0Image;
    [AutoBind("./Bottom/SoldierHP0/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHp0Text;
    [AutoBind("./Bottom/SoldierHP0/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierHp0FxGameObeject;
    [AutoBind("./Bottom/SoldierHP1/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierHp1Image;
    [AutoBind("./Bottom/SoldierHP1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHp1Text;
    [AutoBind("./Bottom/SoldierHP1/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierHp1FxGameObeject;
    [AutoBind("./Bottom/BossHP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossHpImage;
    [AutoBind("./Bottom/BossHP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossHpText;
    [AutoBind("./Bottom/BossHP/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossHpFxGameObeject;
    [AutoBind("./Bottom/Damage0", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_damage0UIStateController;
    [AutoBind("./Bottom/Damage1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_damage1UIStateController;
    [AutoBind("./Bottom/Damage0/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damage0Text;
    [AutoBind("./Bottom/Damage0/CriticalText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damage0CriticalText;
    [AutoBind("./Bottom/Damage1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damage1Text;
    [AutoBind("./Bottom/Damage1/CriticalText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damage1CriticalText;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0GameObject;
    [AutoBind("./Char/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1GameObject;
    [AutoBind("./BackgroundCanvas/Boundary", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boundaryGameObject;
    private CombatUIController.HPState[] m_hpStates;
    private FxPlayer m_fxPlayer;
    private bool m_isBoss;
    private int m_myPlayerTeam;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_ForceUpdateChar;
    private static DelegateBridge __Hotfix_PreStopCombat;
    private static DelegateBridge __Hotfix_StopCombat;
    private static DelegateBridge __Hotfix_ResetHPStates;
    private static DelegateBridge __Hotfix_TeamToSide;
    private static DelegateBridge __Hotfix_SetHealthPoint;
    private static DelegateBridge __Hotfix_UpdateHPState;
    private static DelegateBridge __Hotfix_ShowDamage;
    private static DelegateBridge __Hotfix_SetCharImageInfo;
    private static DelegateBridge __Hotfix_ShowCutsceneSkill;
    private static DelegateBridge __Hotfix_ShowCutscenePassiveSkill;
    private static DelegateBridge __Hotfix_HideCutsceneSkill;
    private static DelegateBridge __Hotfix_SetAutoBattle;
    private static DelegateBridge __Hotfix_SetArenaAutoBattle;
    private static DelegateBridge __Hotfix_OnAutoOffButtonClick;
    private static DelegateBridge __Hotfix_OnArenaAutoButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAutoBattle;
    private static DelegateBridge __Hotfix_remove_EventOnAutoBattle;

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(FxPlayer fxPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat(bool splitScreen, bool isBoss, int myPlayerTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdateChar()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetHPStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int TeamToSide(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHealthPoint(
      int team,
      int heroHp,
      int heroHpMax,
      int soldierHp,
      int soldierHpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHPState(int side, float totalHpPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDamage(
      int team,
      bool isHero,
      int hp,
      int hpMax,
      int hpModify,
      int totalDamage,
      int combatFrame,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharImageInfo(
      int team,
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCutsceneSkill(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCutscenePassiveSkill(BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideCutsceneSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaAutoBattle(bool auto)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoOffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaAutoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> EventOnAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public struct Damage
    {
      public int m_combatFrame;
      public float m_damagePercent;
    }

    public class HPState
    {
      public List<CombatUIController.Damage> m_heroDamages = new List<CombatUIController.Damage>();
      public List<CombatUIController.Damage> m_soldierDamages = new List<CombatUIController.Damage>();
      public CombatCharUIController m_combatCharUIController;
      public float m_totalHpPercent;
      public bool m_isCritical;
      private static DelegateBridge _c__Hotfix_ctor;

      public HPState()
      {
        CombatUIController.HPState._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }
  }
}
