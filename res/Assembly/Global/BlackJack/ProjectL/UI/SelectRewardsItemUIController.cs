﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelectRewardsItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SelectRewardsItemUIController : UIControllerBase, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./Frame", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frame;
    [AutoBind("./Stars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_stars;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./New", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_new;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrEffect;
    [AutoBind("./SSRPieceEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrPieceEffect;
    private Goods m_goods;
    private int m_fragmentItemID;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSelectRewardsInfo;
    private static DelegateBridge __Hotfix_OnPointerClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSelectRewardsInfo(Goods goods, bool isNew, bool isFragment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
