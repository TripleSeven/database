﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendGetUserSummaryReqNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using SLua;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [CustomLuaClass]
  public class FriendGetUserSummaryReqNetTask : UINetTask
  {
    private List<string> m_playerIdList;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendGetUserSummaryReqNetTask(List<string> playerIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetUserSummaryAck(int result, List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UserSummary> Players { get; private set; }
  }
}
