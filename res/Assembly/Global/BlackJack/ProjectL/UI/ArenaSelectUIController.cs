﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaSelectUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArenaSelectUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/OnlineButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineButton;
    [AutoBind("./Panel/OfflineButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineButton;
    [AutoBind("./Panel/TopButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_topButton;
    [AutoBind("./Panel/TopButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_topButtonStateCtrl;
    [AutoBind("./Panel/TopButton/TimeGroup/OpenTimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_topButtonOpenTimeValueText;
    [AutoBind("./Panel/TopButton/TimeGroup/NotOpenText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_topButtonNotOpenText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetTopArenaButtonState;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnOnlineButtonClick;
    private static DelegateBridge __Hotfix_OnOffineButtonClick;
    private static DelegateBridge __Hotfix_OnTopButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnShowOnlineArena;
    private static DelegateBridge __Hotfix_remove_EventOnShowOnlineArena;
    private static DelegateBridge __Hotfix_add_EventOnShowOfflineArena;
    private static DelegateBridge __Hotfix_remove_EventOnShowOfflineArena;
    private static DelegateBridge __Hotfix_add_EventOnShowTopArena;
    private static DelegateBridge __Hotfix_remove_EventOnShowTopArena;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTopArenaButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOffineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTopButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowOnlineArena
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowOfflineArena
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaPanelType> EventOnShowTopArena
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
