﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePrepareTreasure
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class BattlePrepareTreasure
  {
    private ClientBattle m_clientBattle;
    private ConfigDataBattleTreasureInfo m_battleTreasureInfo;
    private GenericGraphic m_graphic;
    private GridPosition m_position;
    private bool m_isOpened;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_GetBattleTreasureInfo;
    private static DelegateBridge __Hotfix_Destroy;
    private static DelegateBridge __Hotfix_ComputeGraphicPosition;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_SetOpened;
    private static DelegateBridge __Hotfix_IsOpened;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePrepareTreasure()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle, ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataBattleTreasureInfo GetBattleTreasureInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpened(bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
