﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Margin/LeftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_leftButton;
    [AutoBind("./Margin/RightButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rightButton;
    [AutoBind("./Margin/FilterToggles/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_infoToggle;
    [AutoBind("./Margin/FilterToggles/Job", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_jobToggle;
    [AutoBind("./Margin/FilterToggles/Job/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Job/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobToggleUnClickRedMark;
    [AutoBind("./Margin/FilterToggles/Job/JobUpButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpTip;
    [AutoBind("./Margin/FilterToggles/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierToggle;
    [AutoBind("./Margin/FilterToggles/Equip", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipToggle;
    [AutoBind("./Margin/FilterToggles/Equip/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Equip/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipToggleUnClickRedMark;
    [AutoBind("./Life", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_lifeToggle;
    [AutoBind("./Life/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lifeToggleClickRedMark;
    [AutoBind("./Margin/FilterToggles/Equip/EquipMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipMaskButton;
    [AutoBind("./Margin/JobTransferButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobTransferButton;
    [AutoBind("./Margin/JobTransferButton/RedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonRedIcon;
    [AutoBind("./Margin/JobTransferButton/U_SummonButton_ Press", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonPressEffect;
    [AutoBind("./Margin/JobTransferButton/U_Button_ready", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferButtonReadyEffect;
    private Hero m_hero;
    public int m_curHeroNum;
    public List<Hero> m_curHeroList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_PassHeroInfo;
    private static DelegateBridge __Hotfix_UpdateViewInHeroDetail;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnLeftButtonClick;
    private static DelegateBridge __Hotfix_OnRightButtonClick;
    private static DelegateBridge __Hotfix_OnJobTransferButtonClick;
    private static DelegateBridge __Hotfix_OnJobTransferButtonClickEffect;
    private static DelegateBridge __Hotfix_OnMaskButtonForUserGuideClick;
    private static DelegateBridge __Hotfix_HeroInfoToogleIsOn;
    private static DelegateBridge __Hotfix_HeroJobToggleIsOn;
    private static DelegateBridge __Hotfix_HeroEquipToggleIsOn;
    private static DelegateBridge __Hotfix_SetToggleToInfo;
    private static DelegateBridge __Hotfix_SetToggleToJob;
    private static DelegateBridge __Hotfix_SetToggleToSoldier;
    private static DelegateBridge __Hotfix_SetToggleToEquip;
    private static DelegateBridge __Hotfix_SetToggleToLife;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_OnInfoToggleValueChanged;
    private static DelegateBridge __Hotfix_OnJobToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSoldierToggleValueChanged;
    private static DelegateBridge __Hotfix_OnEquipToggleValueChanged;
    private static DelegateBridge __Hotfix_OnLifeToggleValueChanged;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnJobTransfer;
    private static DelegateBridge __Hotfix_remove_EventOnJobTransfer;
    private static DelegateBridge __Hotfix_add_EventOnSetDetailState;
    private static DelegateBridge __Hotfix_remove_EventOnSetDetailState;
    private static DelegateBridge __Hotfix_add_EventOnUpdateViewInListAndDetail;
    private static DelegateBridge __Hotfix_remove_EventOnUpdateViewInListAndDetail;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PassHeroInfo(List<Hero> hList, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroDetail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLeftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRightButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobTransferButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnJobTransferButtonClickEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMaskButtonForUserGuideClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HeroInfoToogleIsOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HeroJobToggleIsOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HeroEquipToggleIsOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToEquip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleToLife()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CreateSpineGraphic(
      ref UISpineGraphic graphic,
      string assetName,
      GameObject parent,
      int direction,
      Vector2 offset,
      float scale,
      List<ReplaceAnim> anims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroySpineGraphic(ref UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLifeToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJobTransfer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSetDetailState
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event HeroDetailUIController.Action<int, bool, bool, int, bool> EventOnUpdateViewInListAndDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public delegate void Action<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
  }
}
