﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerVoiceHandleThread
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;

namespace BlackJack.ProjectL.UI
{
  public class PlayerVoiceHandleThread
  {
    public bool m_startHandle;
    public Thread m_voiceHandleThread;
    private Queue<PlayerVoiceHandleThread.VoicePacket> m_inputQueue;
    private Queue<PlayerVoiceHandleThread.VoicePacket> m_outputQueue;
    private List<int> m_validIdList;
    private readonly object m_lock;
    private readonly object m_seclock;
    private readonly object m_idListLock;
    private static PlayerVoiceHandleThread m_instance;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Stop;
    private static DelegateBridge __Hotfix_AddDataToInputBuffer;
    private static DelegateBridge __Hotfix_AddDataContentToInputBuffer;
    private static DelegateBridge __Hotfix_AddDataHeadToInputBuffer;
    private static DelegateBridge __Hotfix_AddDataEndToInputBuffer;
    private static DelegateBridge __Hotfix_GetInputBufferData;
    private static DelegateBridge __Hotfix_AddDataToOutputBuffer;
    private static DelegateBridge __Hotfix_GetOutputBufferData;
    private static DelegateBridge __Hotfix_GetTickTime;
    private static DelegateBridge __Hotfix_VoiceThreadProc;
    private static DelegateBridge __Hotfix_ByteArrayListToByteArray;
    private static DelegateBridge __Hotfix_get_Instance;

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerVoiceHandleThread()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddDataToInputBuffer(PlayerVoiceHandleThread.VoicePacket packet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDataContentToInputBuffer(float[] audioData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDataHeadToInputBuffer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDataEndToInputBuffer(ChatVoiceInfo chatInfo, bool isValid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerVoiceHandleThread.VoicePacket GetInputBufferData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddDataToOutputBuffer(PlayerVoiceHandleThread.VoicePacket packet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerVoiceHandleThread.VoicePacket GetOutputBufferData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetTickTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void VoiceThreadProc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private byte[] ByteArrayListToByteArray(List<byte[]> list)
    {
      // ISSUE: unable to decompile the method.
    }

    public static PlayerVoiceHandleThread Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class VoicePacket
    {
      public float[] m_srcData;
      public ChatVoiceInfo m_chatInfo;
      public bool m_isCompressed;
      public DataType m_type;
      public bool m_isValid;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public VoicePacket()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
