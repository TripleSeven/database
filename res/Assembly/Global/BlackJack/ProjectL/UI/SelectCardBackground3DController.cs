﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelectCardBackground3DController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class SelectCardBackground3DController : UIControllerBase
  {
    [AutoBind("/U_Click001_effect_light", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lightEffect;
    [AutoBind("/U_Click001_effect_get", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_getEffect;
    [AutoBind("/U_Click001_effect_get_SR", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_getEffectSR;
    [AutoBind("/U_Click001_effect_get_SSR", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_getEffectSSR;
    [AutoBind("/ClockDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clockDummyGo;
    private SelectCardClock3DController m_clockCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowClockEffect;
    private static DelegateBridge __Hotfix_ShowLightEffect;
    private static DelegateBridge __Hotfix_ShowGetEffect;
    private static DelegateBridge __Hotfix_OnClockDrag;
    private static DelegateBridge __Hotfix_SetClock3dPrefab;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClockEffect(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowLightEffect(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGetEffect(bool isShow, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClockDrag(int rank, Action finishedEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClock3dPrefab(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
