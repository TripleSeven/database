﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerChatItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerChatItemUIController : UIControllerBase
  {
    private bool m_isUIEventInited;
    public bool m_isEmojiTextInit;
    public ChatComponent.ChatMessageClient m_currChatInfo;
    [AutoBind("./TimeAndPlayerName/PlayerNameText", AutoBindAttribute.InitState.Active, false)]
    public Text PlayerNameText;
    [AutoBind("./TimeAndPlayerName/SendTimeText", AutoBindAttribute.InitState.Active, false)]
    public Text SendTimeText;
    [AutoBind("./PlayerIcon/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image PlayerIconImage;
    [AutoBind("./PlayerIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    public Transform PlayerHeadFrameTransform;
    [AutoBind("./PlayerIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerLevelText;
    [AutoBind("./PlayerIcon/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Button PlayerIconButton;
    [AutoBind("./ContentBG/ContentText", AutoBindAttribute.InitState.Active, false)]
    public EmojiText ContentText;
    [AutoBind("./ContentBG", AutoBindAttribute.InitState.Active, false)]
    public GameObject ContentGo;
    [AutoBind("./ContentBG/ContentText", AutoBindAttribute.InitState.Active, false)]
    public EmojiLinkText ContentEmojiLinkText;
    [AutoBind("./Voice/Voice/SpeakImage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController VoicePlayerStateCtrl;
    [AutoBind("./Voice", AutoBindAttribute.InitState.NotInit, false)]
    public Button VoiceButton;
    [AutoBind("./Voice/Voice/TimeButton/ContentText", AutoBindAttribute.InitState.Active, false)]
    public Text VoiceLengthText;
    [AutoBind("./Voice/ContentText", AutoBindAttribute.InitState.Active, false)]
    public Text VoiceContentText;
    [AutoBind("./Voice", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject VoiceRoot;
    [AutoBind("./TimeAndPlayerName/PlayerTitle", AutoBindAttribute.InitState.Inactive, false)]
    public GameObject PlayerTitleGo;
    [AutoBind("./TimeAndPlayerName/PlayerTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerTitleText;
    [AutoBind("./TimeAndPlayerName/PlayerTitle/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image PlayerTitleImage;
    [AutoBind("./Face", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject FaceRoot;
    [AutoBind("./Face/Image", AutoBindAttribute.InitState.NotInit, false)]
    public Image FaceIcon;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController itemStateCtrl;
    [AutoBind("./BattleReport/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_contentText;
    [AutoBind("./BattleReport/BattleReportPanel/LeftPlayer/CharIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_leftPlayerIconImage;
    [AutoBind("./BattleReport/BattleReportPanel/RightPlayer/CharIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_rightPlayerIconImage;
    [AutoBind("./BattleReport/BattleReportPanel/LeftPlayer/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_leftPlayerNameText;
    [AutoBind("./BattleReport/BattleReportPanel/RightPlayer/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_rightPlayerNameText;
    [AutoBind("./BattleReport", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_battleReportStateCtrl;
    [AutoBind("./BattleReport/BattleReportPanel/LeftPlayer", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_leftStateCtrl;
    [AutoBind("./BattleReport/BattleReportPanel/RightPlayer", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_rightStateCtrl;
    [AutoBind("./BattleReport", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx m_battleReportButton;
    [AutoBind("./BattleReport/BattleReportNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_battleReportName;
    [AutoBind("./BattleReport/BattleReportPanel/MatchLevel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_battleReportMatchName;
    [AutoBind("./BattleReport/BattleReportPanel/MatchLevel/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_battleReportMatchLevelText;
    [AutoBind("./BattleReport/BattleReportPanel/MatchLevel", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_battleReportMatchGameObject;
    public ButtonEx TranslateButton;
    public ButtonEx TransBackButton;
    public Text TranslateCompleteText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateChatInfo;
    private static DelegateBridge __Hotfix_InitEmojiText;
    private static DelegateBridge __Hotfix_SetPeakArenaTitle;
    private static DelegateBridge __Hotfix_IsPlaying;
    private static DelegateBridge __Hotfix_SetPlayState;
    private static DelegateBridge __Hotfix_RegClickEvent;
    private static DelegateBridge __Hotfix_SetToVoiceMode;
    private static DelegateBridge __Hotfix_SetToTextMode;
    private static DelegateBridge __Hotfix_SetToBigExpressionMode;
    private static DelegateBridge __Hotfix_SetToBattleReportMode;
    private static DelegateBridge __Hotfix_SetChatMsgTime;
    private static DelegateBridge __Hotfix_OnVoiceButtonClick;
    private static DelegateBridge __Hotfix_OnPlayerHeadClick;
    private static DelegateBridge __Hotfix_OnHeroEquipmentLinkClick;
    private static DelegateBridge __Hotfix_OnBattleReportClick;
    private static DelegateBridge __Hotfix_OnTranslateButtonClick;
    private static DelegateBridge __Hotfix_OnTransBackButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnVoiceButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnVoiceButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPlayerHeadClick;
    private static DelegateBridge __Hotfix_remove_EventOnPlayerHeadClick;
    private static DelegateBridge __Hotfix_add_EventOnHeroEquipmentLinkClick;
    private static DelegateBridge __Hotfix_remove_EventOnHeroEquipmentLinkClick;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportClick;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportClick;
    private static DelegateBridge __Hotfix_get_TranslateButtonStateCtrl;
    private static DelegateBridge __Hotfix_set_TranslateButtonStateCtrl;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatInfo(ChatComponent.ChatMessageClient chatClientInfo, bool isPlaying = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEmojiText(SmallExpressionParseDesc expressionDesc, Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakArenaTitle(int titleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlaying()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayState(bool play)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegClickEvent(
      Action<ChatComponent.ChatMessageClient> voiceAction,
      Action<ChatMessage, GameObject> playeIconAction,
      Action<ulong, ChatBagItemMessage> heroEquipmentLinkClickAction,
      Action<ChatPeakArenaBattleReportMessage> battleReportClickAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToVoiceMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToTextMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToBigExpressionMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetToBattleReportMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string SetChatMsgTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerHeadClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroEquipmentLinkClick(string typeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTranslateButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTransBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ChatComponent.ChatMessageClient> EventOnVoiceButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ChatMessage, GameObject> EventOnPlayerHeadClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, ChatBagItemMessage> EventOnHeroEquipmentLinkClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ChatPeakArenaBattleReportMessage> EventOnBattleReportClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CommonUIStateController TranslateButtonStateCtrl
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
