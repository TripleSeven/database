﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WaitPVPInviteUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class WaitPVPInviteUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private WaitPVPInviteUIController m_waitPVPInviteUIController;
    private string m_targetUserId;
    private int m_battleInfoId;
    private PracticeMode m_practiceMode;
    private DateTime m_waitTimeout;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_InitWaitPVPInviteUIController;
    private static DelegateBridge __Hotfix_UninitWaitPVPInviteUIController;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_StartBattlePracticeInviteNetTask;
    private static DelegateBridge __Hotfix_CloseAndPause;
    private static DelegateBridge __Hotfix_WaitPVPInviteUIController_OnCancel;
    private static DelegateBridge __Hotfix_PlayerContext_OnOnBattlePracticeFailNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattlePracticeDeclinedNtf;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public WaitPVPInviteUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~WaitPVPInviteUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      string targetUserId,
      int battleInfoID = -1,
      PracticeMode practiceMode = PracticeMode.NormalPVP)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWaitPVPInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitWaitPVPInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattlePracticeInviteNetTask(
      string userId,
      int battleInfoId,
      PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAndPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WaitPVPInviteUIController_OnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnOnBattlePracticeFailNtf(PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattlePracticeDeclinedNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
