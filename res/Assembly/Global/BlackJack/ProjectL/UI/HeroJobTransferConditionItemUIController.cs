﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobTransferConditionItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroJobTransferConditionItemUIController : UIControllerBase
  {
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./FinishTag", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_finishTag;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitConditionItem;
    private static DelegateBridge __Hotfix_set_ConditionInfo;
    private static DelegateBridge __Hotfix_get_ConditionInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitConditionItem(ConfigDataJobUnlockConditionInfo condition, HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobUnlockConditionInfo ConditionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
