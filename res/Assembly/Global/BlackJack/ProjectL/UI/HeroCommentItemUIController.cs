﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCommentItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroCommentItemUIController : UIControllerBase
  {
    [AutoBind("./PraisedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerPraisedIcon;
    [AutoBind("./LVText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLvText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerNameBtn;
    [AutoBind("./CommentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerCommentText;
    [AutoBind("./PraisedNumText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerPraisedNumText;
    [AutoBind("./PraisedButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerPraisedBtn;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitCommentItem;
    private static DelegateBridge __Hotfix_OnPlayerNameButtonClick;
    private static DelegateBridge __Hotfix_OnPlayerPraisedButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnNameClick;
    private static DelegateBridge __Hotfix_remove_EventOnNameClick;
    private static DelegateBridge __Hotfix_add_EventOnPraisedBtnClick;
    private static DelegateBridge __Hotfix_remove_EventOnPraisedBtnClick;
    private static DelegateBridge __Hotfix_set_Comment;
    private static DelegateBridge __Hotfix_get_Comment;
    private static DelegateBridge __Hotfix_set_CommenterId;
    private static DelegateBridge __Hotfix_get_CommenterId;
    private static DelegateBridge __Hotfix_set_CommentInstanceId;
    private static DelegateBridge __Hotfix_get_CommentInstanceId;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitCommentItem(HeroCommentEntry comment, bool isShowPraisedIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerPraisedButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroCommentEntry> EventOnNameClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnPraisedBtnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroCommentEntry Comment
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string CommenterId
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong CommentInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
