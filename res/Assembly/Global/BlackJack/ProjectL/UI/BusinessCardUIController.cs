﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BusinessCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BusinessCardUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateController;
    [AutoBind("./PlayerInfoPanel/Detail/Title/PlayerImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeadIcon;
    [AutoBind("./PlayerInfoPanel/Detail/Title/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfoPanel/Detail/Title/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfoPanel/Detail/Title/PointValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerPointText;
    [AutoBind("./PlayerInfoPanel/Detail/Title/LikeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLikesCountText;
    [AutoBind("./PlayerInfoPanel/Detail/UnderGroup/RandomToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_randomShowToggle;
    [AutoBind("./PlayerInfoPanel/Detail/GoodImageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_likeButton;
    [AutoBind("./PlayerInfoPanel/Detail/GoodImageButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_likeUIStateController;
    [AutoBind("./PlayerInfoPanel/Detail/UnderGroup/HeroMomentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroMomentButton;
    [AutoBind("./PlayerInfoPanel/Detail/UnderGroup/PeakGloryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakGloryButton;
    [AutoBind("./PlayerInfoPanel/Detail/PlayerTitle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakPlayerTitleGo;
    [AutoBind("./PlayerInfoPanel/Detail/PlayerTitle/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerTitleText;
    [AutoBind("./PlayerInfoPanel/Detail/PlayerTitle/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakPlayerTitleImage;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teamGraphicsParentGameObject;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char2Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char3Dummy;
    [AutoBind("./PlayerInfoPanel/Detail/Graphics/4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char4Dummy;
    [AutoBind("./PersonalSign/SignDetailText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerSignText;
    [AutoBind("./PersonalSign/SignInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_playerInputField;
    [AutoBind("./PersonalSign/ChangeSignButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeSignButton;
    [AutoBind("./PersonalPanel/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupObj;
    [AutoBind("./PersonalPanel/AllHeroPower", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerAllHeroPowerText;
    [AutoBind("./PersonalPanel/ReachedAchievement", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerReachedAchievementText;
    [AutoBind("./PersonalPanel/MasterJob", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerMasterJobCountText;
    [AutoBind("./PersonalPanel/Stage", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_stageGoalText;
    [AutoBind("./PersonalPanel/Rift", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_riftGoalText;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./PlayerInfoPanel/ARButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_arButton;
    [AutoBind("./SetPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_setPanelUIStateController;
    [AutoBind("./SetPanel/BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_setPanelBGButton;
    [AutoBind("./SetPanel/HeroInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroInfoPanelUIStateController;
    [AutoBind("./SetPanel/HeroInfoPanel/SetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroInfoPanelSettingButton;
    [AutoBind("./SetPanel/HeroInfoPanel/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroInfoPanelChangeButton;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoArmyImage;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoLevelText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoNameText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Job/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoJobText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Power/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPowerText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoGraphicDummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment1Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment2Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment3Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Equip/EquipmentListGroup/Equipment4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipment4Dummy;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropHPImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropDFImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropATImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropMagicDFImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropMagicImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoPropDEXImg;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropHPValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropATValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDEXValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropHPAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDFAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropATAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicDFAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropMagicAddText;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPropDEXAddText;
    [AutoBind("./Prefab/EquipmentItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentItemPrefab;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipItemDescBGButton;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipItemDescStateController;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescTitleText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescLvText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescExpText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconImage;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconBg;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSSREffect;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescProgressImage;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescStarGroup;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescEquipLimitContent;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropContent;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropATGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropATValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDFGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropHPGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropHPValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagiccGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagicDFGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicDFValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDexGo;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDexValueText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillContent;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillNameText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillLvText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillOwnerText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/SkillContent/DescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillDescText;
    [AutoBind("./SetPanel/HeroInfoPanel/Desc/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescNotEquipSkillTip;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_talentIconImage;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_talentButton;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/HeroSkillItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroCurSkillGroup;
    [AutoBind("./SetPanel/HeroInfoPanel/HeroDetailInfo/Talent&Skill/SkillItemDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoCurSkillDescPanel;
    [AutoBind("./SetPanel/SetCharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_setCharInfoPanelUIStateController;
    [AutoBind("./SetPanel/SetCharPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_setCharInfoPanelConfirmButton;
    [AutoBind("./SetPanel/SetCharPanel/CharFace", AutoBindAttribute.InitState.NotInit, false)]
    private Dropdown m_charFaceDropdown;
    [AutoBind("./SetPanel/SetCharPanel/CharActive", AutoBindAttribute.InitState.NotInit, false)]
    private Dropdown m_charActiveDropdown;
    [AutoBind("./SetPanel/SetCharPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGraphicParentDummy;
    [AutoBind("./SetPanel/SetCharPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charNameText;
    [AutoBind("./SetPanel/HeroSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroSelectPanelUIStateController;
    [AutoBind("./SetPanel/HeroSelectPanel/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListContent;
    [AutoBind("./SetPanel/HeroSelectPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroSelectPanelConfirmButton;
    [AutoBind("./Prefab/HeroListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListItemPrefab;
    [AutoBind("./Prefab/HeroCharItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroCharItemPrefab;
    [AutoBind("./Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupItemPrefab;
    private UISpineGraphic m_currentCharSpineGraphic;
    private HeroDirectionType m_currentHeroDirectionType;
    private HeroActionType m_currentHeroAnimationType;
    private BusinessCard m_businessCard;
    private bool m_isMe;
    private int m_currentSelectPositionIndex;
    private BattleHero m_currentSelectBattleHero;
    private List<GameObject> m_skillDummyList;
    private List<BusinessCardHeroSet> m_currentHeroSetList;
    private List<BusinessCardUIController.GraphicInfo> m_graphicList;
    private List<Hero> m_heroSortList;
    private bool[] m_isSelectArray;
    private const int HeroTeamMaxCount = 5;
    private float m_fDebugReportTime;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowOpenState;
    private static DelegateBridge __Hotfix_SetPlayerInfo;
    private static DelegateBridge __Hotfix_CheckARState;
    private static DelegateBridge __Hotfix_SetPeakArenaTitle;
    private static DelegateBridge __Hotfix_GetPointText;
    private static DelegateBridge __Hotfix_GetRankStr;
    private static DelegateBridge __Hotfix_SetTeam;
    private static DelegateBridge __Hotfix_OnHeroCharClick;
    private static DelegateBridge __Hotfix_ClearTeam;
    private static DelegateBridge __Hotfix_SetRandomShowToggleOff;
    private static DelegateBridge __Hotfix_SetHeroInfoPanel;
    private static DelegateBridge __Hotfix_SetHeroTalent;
    private static DelegateBridge __Hotfix_SetHeroProperty;
    private static DelegateBridge __Hotfix_SetCurSelectSkills;
    private static DelegateBridge __Hotfix_OnSkillItemClick;
    private static DelegateBridge __Hotfix_OnTalentItemClick;
    private static DelegateBridge __Hotfix_SetEquipment;
    private static DelegateBridge __Hotfix_ClearEquipmentItem;
    private static DelegateBridge __Hotfix_SetEquipmentItemInfo;
    private static DelegateBridge __Hotfix_OnEquipmentItemClick;
    private static DelegateBridge __Hotfix_OnEquipmentItemBGButtonClick;
    private static DelegateBridge __Hotfix_SetEquipmentItemDesc;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_SetCharInfoPanel;
    private static DelegateBridge __Hotfix_OnCharDiretionValueChanged;
    private static DelegateBridge __Hotfix_OnCharAnimationValueChanged;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_SetCharAction;
    private static DelegateBridge __Hotfix_SetHeroSelectPanel;
    private static DelegateBridge __Hotfix_HeroListItemCompare;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnLikeButtonClick;
    private static DelegateBridge __Hotfix_OnHeroMomentClick;
    private static DelegateBridge __Hotfix_OnPeakGloryClick;
    private static DelegateBridge __Hotfix_OnRandowShowToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSetPanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnChangeSignButtonClick;
    private static DelegateBridge __Hotfix_OnArButtonClick;
    private static DelegateBridge __Hotfix_OnPlayerSignEditEnd;
    private static DelegateBridge __Hotfix_PlayerSignUpdateSuccess;
    private static DelegateBridge __Hotfix_PlayerSignUpdateEnd;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_OnHeroInfoPanelSettingButtonClick;
    private static DelegateBridge __Hotfix_OnHeroInfoPanelChangeButtonClick;
    private static DelegateBridge __Hotfix_OnSetCharPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnHeroSelectPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_add_EventOnArClick;
    private static DelegateBridge __Hotfix_remove_EventOnArClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnLike;
    private static DelegateBridge __Hotfix_remove_EventOnLike;
    private static DelegateBridge __Hotfix_add_EventOnHeroMoment;
    private static DelegateBridge __Hotfix_remove_EventOnHeroMoment;
    private static DelegateBridge __Hotfix_add_EventOnPeakGlory;
    private static DelegateBridge __Hotfix_remove_EventOnPeakGlory;
    private static DelegateBridge __Hotfix_add_EventOnChangeSign;
    private static DelegateBridge __Hotfix_remove_EventOnChangeSign;
    private static DelegateBridge __Hotfix_add_EventOnUpdateHeroSet;
    private static DelegateBridge __Hotfix_remove_EventOnUpdateHeroSet;

    [MethodImpl((MethodImplOptions) 32768)]
    private BusinessCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOpenState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerInfo(
      BusinessCard businessCard,
      bool isMe,
      bool canSendLike,
      bool isRandomShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckARState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakArenaTitle(int titleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetPointText(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetRankStr(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTeam(BusinessCard businessCard, bool isRandomShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroCharClick(BusinessCardHeroCharItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomShowToggleOff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroInfoPanel(BattleHero hero, bool canSetAndChange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroTalent(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroProperty(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurSelectSkills(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalentItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipment(List<BattleHeroEquipment> equipments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearEquipmentItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemInfo(BattleHeroEquipment equipment, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClick(BusinessCardHeroEquipmentItemController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(BattleHeroEquipment equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCharInfoPanel(BattleHero hero, int positionIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCharDiretionValueChanged(int direction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCharAnimationValueChanged(int animation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo heroSkinResInfo,
      bool isUIModelScale,
      HeroDirectionType directionType = HeroDirectionType.Left,
      HeroActionType actionType = HeroActionType.Idle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCharAction(
      UISpineGraphic g,
      HeroDirectionType direction,
      HeroActionType animation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroSelectPanel(BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemCompare(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLikeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroMomentClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakGloryClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRandowShowToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSetPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSignButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerSignEditEnd(InputField input)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerSignUpdateSuccess(string sign)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerSignUpdateEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick(BusinessCardHeroListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroInfoPanelSettingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroInfoPanelChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSetCharPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSelectPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnArClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnLike
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroMoment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakGlory
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnChangeSign
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<BusinessCardHeroSet>> EventOnUpdateHeroSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class GraphicInfo
    {
      public UISpineGraphic m_graphic;
      public HeroDirectionType m_directionType;
      public HeroActionType m_actionType;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public GraphicInfo(
        UISpineGraphic graphic,
        HeroDirectionType directionType,
        HeroActionType actionType)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
