﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.OfflineArenaBattleReportListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class OfflineArenaBattleReportListItemUIController : UIControllerBase
  {
    [AutoBind("./PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./ModeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_modeUIStateCtrl;
    [AutoBind("./TimeGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointsText;
    [AutoBind("./ButtonGroup/RevengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_revengeButton;
    [AutoBind("./ButtonGroup/HaveRevengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_haveRevengeButton;
    [AutoBind("./ButtonGroup/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_replayButton;
    [AutoBind("./ButtonGroup/ReplayButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_replayUIStateController;
    private ArenaBattleReport m_battleReport;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetArenaBattleReport;
    private static DelegateBridge __Hotfix_GetArenaBattleReport;
    private static DelegateBridge __Hotfix_OnRevengeButtonClick;
    private static DelegateBridge __Hotfix_OnHaveRevengeButtonClick;
    private static DelegateBridge __Hotfix_OnReplayButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnRevengeButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRevengeButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReplayButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnReplayButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleReport(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport GetArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRevengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHaveRevengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReplayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<OfflineArenaBattleReportListItemUIController> EventOnRevengeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OfflineArenaBattleReportListItemUIController> EventOnReplayButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
