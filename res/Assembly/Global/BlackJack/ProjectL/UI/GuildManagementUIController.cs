﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildManagementUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildManagementUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./InfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoButton;
    [AutoBind("./TitleImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sociatyName;
    [AutoBind("./TitleImage/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyNameChangeButton;
    [AutoBind("./MainPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_mainPanelStateCtrl;
    [AutoBind("./PlayerResource/GuildCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildCoinText;
    [AutoBind("./PlayerResource/GuildCoin/StatusButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildCoinDescButton;
    [AutoBind("./MainPanel/ListPanel/SociatyListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_sociatyListScrollRect;
    [AutoBind("./MainPanel/ListPanel/SociatyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sociatyListScrollContent;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyListPowerButton;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sociatyListPowerButtonStateCtrl;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/ActiveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyListActiveButton;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/ActiveButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sociatyListActiveButtonStateCtrl;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/OnlineButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyListOnlineButton;
    [AutoBind("./MainPanel/ListPanel/TopButtonGroup/OnlineButton/OnlineValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sociatyListOnlineValueText;
    [AutoBind("./Prefab/ListPlayerInfoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerInfoPrefabItem;
    [AutoBind("./MainPanel/InfoPanel/SociatyID/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoSociatyIDText;
    [AutoBind("./MainPanel/InfoPanel/PeopleNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoPeopleNumberText;
    [AutoBind("./MainPanel/InfoPanel/Power/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoPowerText;
    [AutoBind("./MainPanel/InfoPanel/Active/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoActiveText;
    [AutoBind("./MainPanel/InfoPanel/Coin/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoCoinText;
    [AutoBind("./MainPanel/InfoPanel/Declaration/DeclarationText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoDeclarationText;
    [AutoBind("./MainPanel/InfoPanel/Declaration/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoDeclarationButton;
    [AutoBind("./MainPanel/InfoPanel/MessageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageButton;
    [AutoBind("./MainPanel/InfoPanel/MessageButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageButtonRedPoint;
    [AutoBind("./MainPanel/InfoPanel/QuiteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_quitButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/InviteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/WealButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_wealButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/ShopButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shopButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/SociatyPlayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sociatyPlayButton;
    [AutoBind("./MainPanel/ListPanel/BottomButtonPanel/SociatyPlayButton/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sociatyPlayButtonRedPoint;
    [AutoBind("./DeclarationChangePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_declarationChangePanelStateCtrl;
    [AutoBind("./DeclarationChangePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_declarationChangePanelBackButton;
    [AutoBind("./DeclarationChangePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_declarationChangePanelCloseButton;
    [AutoBind("./DeclarationChangePanel/Detail/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_declarationChangePanelSaveButton;
    [AutoBind("./DeclarationChangePanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_declarationChangePanelInputField;
    [AutoBind("./AnnouncementChangePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_recruitPanelStateCtrl;
    [AutoBind("./AnnouncementChangePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recruitPanelBackButton;
    [AutoBind("./AnnouncementChangePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recruitPanelCloseButton;
    [AutoBind("./AnnouncementChangePanel/Detail/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recruitPanelSaveButton;
    [AutoBind("./AnnouncementChangePanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_recruitPanelInputField;
    [AutoBind("./MainPanel/InfoPanel/SetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelSetButton;
    [AutoBind("./SetPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildSetPanelStateCtrl;
    [AutoBind("./SetPanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelBackButton;
    [AutoBind("./SetPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelCloseButton;
    [AutoBind("./SetPanel/Detail/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelSaveButton;
    [AutoBind("./SetPanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildSetPanelHireDeclarationInputField;
    [AutoBind("./SetPanel/Detail/LevelInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildSetPanelLevelInputField;
    [AutoBind("./SetPanel/Detail/LevelInputField/PreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelLevelInputFieldLeftButton;
    [AutoBind("./SetPanel/Detail/LevelInputField/AftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildSetPanelLevelInputFieldRightButton;
    [AutoBind("./SetPanel/Detail/ApproveGroup/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildSetPanelApproveGroupAutoToggle;
    [AutoBind("./SetPanel/Detail/ApproveGroup/ChairmanButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildSetPanelApproveGroupChairmanToggle;
    [AutoBind("./SociatyNameChangePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildChangeNamePanelStateCtrl;
    [AutoBind("./SociatyNameChangePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildChangeNamePanelBackButton;
    [AutoBind("./SociatyNameChangePanel/Detail/ChangeNameButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildChangeNamePanelChangeNameButton;
    [AutoBind("./SociatyNameChangePanel/Detail/ChangeNameButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildChangeNamePanelChangeNameButtonStateCtrl;
    [AutoBind("./SociatyNameChangePanel/Detail/DeclarationInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildChangeNamePanelInputField;
    [AutoBind("./SociatyNameChangePanel/Detail/ChangeNameButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildChangeNamePanelValueText;
    [AutoBind("./MessagePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_messagePanelStateCtrl;
    [AutoBind("./MessagePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messagePanelBackButton;
    [AutoBind("./MessagePanel/Detail/SideToggleGroup/ApplyToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_messagePanelApplyToggle;
    [AutoBind("./MessagePanel/Detail/SideToggleGroup/JournalToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_messagePanelJournalToggle;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageApplyPanelPowerButton;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_messageApplyPanelPowerButtonStateCtrl;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/MessageListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_messageApplyPanelMessageListScrollRect;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/MessageListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageApplyPanelMessageListScrollContent;
    [AutoBind("./Prefab/MessagePlayerItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messagePlayerItemPrafeb;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/AllRefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_messageApplyPanelAllRefuseButton;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/CountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageApplyPanelApplyTotalCountValueText;
    [AutoBind("./MessagePanel/Detail/ApplyPanel/PeopleNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageApplyPanelTotalPeopleNumberValueText;
    [AutoBind("./MessagePanel/Detail/JournalPanel/JournalListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_messageJournalPanelListScrollRect;
    [AutoBind("./MessagePanel/Detail/JournalPanel/JournalListScrollView/Viewport/Content/JournalInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageJournalPanelListScrollContent;
    [AutoBind("./MessagePanel/Detail/NoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageNoItemGameObject;
    [AutoBind("./Prefab/MessageJournalItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_messageJournalItemPrafeb;
    [AutoBind("./InvitePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_invitePanelStateCtrl;
    [AutoBind("./InvitePanel/BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelBackButton;
    [AutoBind("./InvitePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelCloseButton;
    [AutoBind("./InvitePanel/Detail/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_invitePanelChangeListButton;
    [AutoBind("./InvitePanel/Detail/InfoPanel/PeopleNumber/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelPeopleNumberText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Active/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelActiveText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Level/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelLevelText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Declaration/DeclarationText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteInfoPanelDeclarationText;
    [AutoBind("./InvitePanel/Detail/InfoPanel/Declaration/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteInfoPanelDeclarationChangeButton;
    [AutoBind("./InvitePanel/Detail/ListPanel/SociatyListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_inviteListPanelSociatyListScrollRect;
    [AutoBind("./InvitePanel/Detail/ListPanel/SociatyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inviteListPanelSociatyListScrollContent;
    [AutoBind("./InvitePanel/Detail/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteListPanelPowerButton;
    [AutoBind("./InvitePanel/Detail/ListPanel/TopButtonGroup/PowerButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_inviteListPanelPowerButtonStateCtrl;
    [AutoBind("./Prefab/InvitePlayerInfoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_invitePlayerInfoItemPrefab;
    [AutoBind("./QuitDialog", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_quitDialogStateCtrl;
    [AutoBind("./QuitDialog/Panel/Title", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_quitDialogTitleText;
    [AutoBind("./QuitDialog/Panel/Tip", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_quitDialogTipText;
    [AutoBind("./QuitDialog/Panel/ButtonGroup/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_quitDialogConfirmButton;
    [AutoBind("./QuitDialog/Panel/ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_quitDialogCancelButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private const string StateName_Up = "Up";
    private const string StateName_Down = "Down";
    private const string StateName_Hide = "Hide";
    private bool m_isManageListPowerAscend;
    private bool m_isManageListActiveAscend;
    private bool m_isMessageApplyListPowerAscend;
    private bool m_isInviteListPanelPowerAscend;
    private bool m_isMessageApplyPanelSortByPower;
    private List<GuildMemberInfoItemUIController> m_playerInfoCtrlList;
    private List<GuildApplyMemberInfoItemUIController> m_applyPlayerInfoCtrlList;
    private List<GuildJournalItemUIController> m_guildJournalItemCtrlList;
    private List<GuildInviteMemberInfoItemUIController> m_inviteMemberInfoCtrlList;
    private Guild m_guild;
    private GuildTitle m_guildTitle;
    private List<UserSummary> m_canInvitePlayerList;
    private bool m_isGuildAutoJoin;
    private int m_guildJoinLevel;
    private List<UserSummary> m_messageApplyUserSummarys;
    private GuildManagementUIController.GuildListSortType m_guildListSortType;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_RefreshGuildTitle;
    private static DelegateBridge __Hotfix_SetStateByGuildTitle;
    private static DelegateBridge __Hotfix_SetSociatyInfoPanel;
    private static DelegateBridge __Hotfix_SetSociatyListPanel;
    private static DelegateBridge __Hotfix_OnGuildManagementListPlayeItemClick;
    private static DelegateBridge __Hotfix_SetMessageRedPoint;
    private static DelegateBridge __Hotfix_OnReturnImgButtonClick;
    private static DelegateBridge __Hotfix_OnInfoButtonClick;
    private static DelegateBridge __Hotfix_OnGuildCoinButtonClick;
    private static DelegateBridge __Hotfix_ClearData;
    private static DelegateBridge __Hotfix_CloseAllSmallPanel;
    private static DelegateBridge __Hotfix_OnOnlineButtonClick;
    private static DelegateBridge __Hotfix_OnActiveButtonClick;
    private static DelegateBridge __Hotfix_OnPowerButtonClick;
    private static DelegateBridge __Hotfix_SetSortButtonState;
    private static DelegateBridge __Hotfix_SortByTitle;
    private static DelegateBridge __Hotfix_SortByPower;
    private static DelegateBridge __Hotfix_SortByActive;
    private static DelegateBridge __Hotfix_SortGuildListByDefault;
    private static DelegateBridge __Hotfix_SortByDefaultType;
    private static DelegateBridge __Hotfix_SortByActiveType;
    private static DelegateBridge __Hotfix_SortByBattlePowerType;
    private static DelegateBridge __Hotfix_OnSociatyNameChangeButtonClick;
    private static DelegateBridge __Hotfix_OnGuildChangeNamePanelChangeNameButtonClick;
    private static DelegateBridge __Hotfix_OnGuildChangeNamePanelBackButtonClick;
    private static DelegateBridge __Hotfix_OnDeclarationButtonClick;
    private static DelegateBridge __Hotfix_OnDeclarationChangePanelSaveButtonClick;
    private static DelegateBridge __Hotfix_OnDeclarationChangePanelBackButtonClick;
    private static DelegateBridge __Hotfix_OnQuitButtonClick;
    private static DelegateBridge __Hotfix_OnQuitGuildConfirmButtonClick;
    private static DelegateBridge __Hotfix_CloseQuitGuildConfirmPanel;
    private static DelegateBridge __Hotfix_OnSociatyPlayButtonClick;
    private static DelegateBridge __Hotfix_OnShopButtonClick;
    private static DelegateBridge __Hotfix_OnWealButtonClick;
    private static DelegateBridge __Hotfix_OnMessageButtonClick;
    private static DelegateBridge __Hotfix_SendNtfBeforeOpenMessagePanel;
    private static DelegateBridge __Hotfix_SetMessageApplyPanel;
    private static DelegateBridge __Hotfix_OnGuildApplyMemberAcceptResult;
    private static DelegateBridge __Hotfix_SetMessageJournalPanel;
    private static DelegateBridge __Hotfix_OnMssageApplyPanelPowerButtonClick;
    private static DelegateBridge __Hotfix_OnMessagePanelBackButtonClick;
    private static DelegateBridge __Hotfix_CloseMessagePanel;
    private static DelegateBridge __Hotfix_OnMessagePanelApplyToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMessagePanelJournalToggleValueChanged;
    private static DelegateBridge __Hotfix_OnMessageApplyPanelAllRefuseButtonClick;
    private static DelegateBridge __Hotfix_OnInviteButtonClick;
    private static DelegateBridge __Hotfix_SetInvitePanel;
    private static DelegateBridge __Hotfix_SetInviteInfoPanel;
    private static DelegateBridge __Hotfix_SetInviteListPanel;
    private static DelegateBridge __Hotfix_OnInviteMemberInfoItemClick;
    private static DelegateBridge __Hotfix_OnInviteListPanelPowerButtonClick;
    private static DelegateBridge __Hotfix_OnInvitePanelChangeListButtonClick;
    private static DelegateBridge __Hotfix_OnInvitePanelBackButton;
    private static DelegateBridge __Hotfix_CloseInvitePanel;
    private static DelegateBridge __Hotfix_OnInviteInfoPanelDeclarationChangeButtonClick;
    private static DelegateBridge __Hotfix_SetRecruitChangeSubPanelInfo;
    private static DelegateBridge __Hotfix_OnRecruitPanelSaveButtonClick;
    private static DelegateBridge __Hotfix_OnRecruitPanelBackButtonClick;
    private static DelegateBridge __Hotfix_OnInvitePanelSetButtonClick;
    private static DelegateBridge __Hotfix_SetGuildSetSubPanel;
    private static DelegateBridge __Hotfix_OnGuildSetPanelChairmanToggleClick;
    private static DelegateBridge __Hotfix_OnGuildSetPanelAutoToggleClick;
    private static DelegateBridge __Hotfix_OnGuildSetPanelLevelInputFieldRightButtonClick;
    private static DelegateBridge __Hotfix_OnGuildSetPanelLevelInputFieldLeftButtonClick;
    private static DelegateBridge __Hotfix_OnGuildSetPanelLevelInputFieldEditEnd;
    private static DelegateBridge __Hotfix_OnGuildSetPanelSaveButtonClick;
    private static DelegateBridge __Hotfix_OnGuildSetPanelBackButtonClick;
    private static DelegateBridge __Hotfix_CloseGuildSetPanel;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnChangeGuildName;
    private static DelegateBridge __Hotfix_remove_EventOnChangeGuildName;
    private static DelegateBridge __Hotfix_add_EventOnGuildAnnouncementSet;
    private static DelegateBridge __Hotfix_remove_EventOnGuildAnnouncementSet;
    private static DelegateBridge __Hotfix_add_EventOnQuitGuild;
    private static DelegateBridge __Hotfix_remove_EventOnQuitGuild;
    private static DelegateBridge __Hotfix_add_EventOnGetCanInvitePlayerList;
    private static DelegateBridge __Hotfix_remove_EventOnGetCanInvitePlayerList;
    private static DelegateBridge __Hotfix_add_EventOnGuildHiringDeclarationSet;
    private static DelegateBridge __Hotfix_remove_EventOnGuildHiringDeclarationSet;
    private static DelegateBridge __Hotfix_add_EventOnGuildInfoSet;
    private static DelegateBridge __Hotfix_remove_EventOnGuildInfoSet;
    private static DelegateBridge __Hotfix_add_EventOnGetGuildJoinApply;
    private static DelegateBridge __Hotfix_remove_EventOnGetGuildJoinApply;
    private static DelegateBridge __Hotfix_add_EventOnGetGuildJournal;
    private static DelegateBridge __Hotfix_remove_EventOnGetGuildJournal;
    private static DelegateBridge __Hotfix_add_EventOnGuildJoinConfirmOrRefuse;
    private static DelegateBridge __Hotfix_remove_EventOnGuildJoinConfirmOrRefuse;
    private static DelegateBridge __Hotfix_add_EventOnGuildInviteMember;
    private static DelegateBridge __Hotfix_remove_EventOnGuildInviteMember;
    private static DelegateBridge __Hotfix_add_EventOnGuildMemberClick;
    private static DelegateBridge __Hotfix_remove_EventOnGuildMemberClick;
    private static DelegateBridge __Hotfix_add_EventOnAllRefuseButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAllRefuseButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGotoGuildStore;
    private static DelegateBridge __Hotfix_remove_EventOnGotoGuildStore;
    private static DelegateBridge __Hotfix_add_EventOnGotoGuildGameListPanel;
    private static DelegateBridge __Hotfix_remove_EventOnGotoGuildGameListPanel;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView(Guild guild)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshGuildTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetStateByGuildTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSociatyInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSociatyListPanel(List<GuildMemberCacheObject> memberList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildManagementListPlayeItemClick(GuildMemberInfoItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMessageRedPoint(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnImgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildCoinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAllSmallPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActiveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSortButtonState(
      GuildManagementUIController.GuildListSortType sortType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByTitle(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByPower(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByActive(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortGuildListByDefault(List<GuildMemberCacheObject> guildList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByDefaultType(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByActiveType(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int SortByBattlePowerType(GuildMemberCacheObject a, GuildMemberCacheObject b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSociatyNameChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildChangeNamePanelChangeNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildChangeNamePanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeclarationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeclarationChangePanelSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeclarationChangePanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuitGuildConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseQuitGuildConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSociatyPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShopButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWealButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendNtfBeforeOpenMessagePanel(bool isManager, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMessageApplyPanel(List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildApplyMemberAcceptResult(
      GuildApplyMemberInfoItemUIController ctrl,
      bool result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMessageJournalPanel(List<GuildLog> journal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMssageApplyPanelPowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessagePanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseMessagePanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessagePanelApplyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessagePanelJournalToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMessageApplyPanelAllRefuseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInvitePanel(List<UserSummary> playerList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteMemberInfoItemClick(GuildInviteMemberInfoItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteListPanelPowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvitePanelChangeListButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvitePanelBackButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseInvitePanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteInfoPanelDeclarationChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRecruitChangeSubPanelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecruitPanelSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecruitPanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvitePanelSetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGuildSetSubPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelChairmanToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelAutoToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelLevelInputFieldRightButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelLevelInputFieldLeftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelLevelInputFieldEditEnd(string inputStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildSetPanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseGuildSetPanel(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnChangeGuildName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnGuildAnnouncementSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnQuitGuild
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<List<UserSummary>>, bool> EventOnGetCanInvitePlayerList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnGuildHiringDeclarationSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool, int, string, Action> EventOnGuildInfoSet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<List<UserSummary>>> EventOnGetGuildJoinApply
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<List<GuildLog>>> EventOnGetGuildJournal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, bool, Action> EventOnGuildJoinConfirmOrRefuse
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Action> EventOnGuildInviteMember
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, Vector3, PlayerSimpleInfoUITask.PostionType> EventOnGuildMemberClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnAllRefuseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGotoGuildStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGotoGuildGameListPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum GuildListSortType
    {
      Default,
      Active,
      BattlePower,
    }
  }
}
