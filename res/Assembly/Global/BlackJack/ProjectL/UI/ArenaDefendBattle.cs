﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class ArenaDefendBattle
  {
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_mapBackground;
    private GameObject m_mapTerrainFxRoot;
    private Dictionary<GridPosition, GameObject> m_mapTerrainFxs;
    private ConfigDataArenaBattleInfo m_arenaBattleInfo;
    private BattleCamera m_camera;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_CreateMap;
    private static DelegateBridge __Hotfix_DestroyMap;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_CreateMapBackground;
    private static DelegateBridge __Hotfix_ClearMapBackground;
    private static DelegateBridge __Hotfix_CreateMapTerrainFx;
    private static DelegateBridge __Hotfix_ClearMapTerrainFx;
    private static DelegateBridge __Hotfix_CreateGraphic;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_GridPositionToWorldPosition;
    private static DelegateBridge __Hotfix_WorldPositionToGridPosition;
    private static DelegateBridge __Hotfix_ScreenPositionToWorldPosition;
    private static DelegateBridge __Hotfix_ScreenPositionToGridPosition;
    private static DelegateBridge __Hotfix_get_ArenaBattleInfo;
    private static DelegateBridge __Hotfix_get_Camera;
    private static DelegateBridge __Hotfix_get_FxPlayer;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject root)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateMap(ConfigDataArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapBackground(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapTerrainFx(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapTerrainFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GridPositionToWorldPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition WorldPositionToGridPosition(Vector2 sp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 ScreenPositionToWorldPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition ScreenPositionToGridPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataArenaBattleInfo ArenaBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleCamera Camera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer FxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
