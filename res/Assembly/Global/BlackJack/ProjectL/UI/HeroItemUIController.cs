﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroItemUIController : UIControllerBase
  {
    [AutoBind("./CollectProgressBar/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroCollectProgressImg;
    [AutoBind("./CollectProgressBar/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroCollectProgressText;
    [AutoBind("./SelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectFrameImg;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroTypeImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroTypeImg;
    [AutoBind("./HeroLvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    [AutoBind("./SSRFrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrFrameEffect;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frameImg;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_redMark;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    public HeroItemUIController.HeroItemState m_curHeroItemState;
    public Hero m_hero;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitValuesInHeroItemUnlocked;
    private static DelegateBridge __Hotfix_InitValuesInHeroItemLocked;
    private static DelegateBridge __Hotfix_InitValuesInHeroItem;
    private static DelegateBridge __Hotfix_SetUnlockCardRedPoint;
    private static DelegateBridge __Hotfix_ShowSelectFrameImage;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_add_EventOnHeroItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnHeroItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitValuesInHeroItemUnlocked(Hero hero, int heroLvText, int heroStarNum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitValuesInHeroItemLocked(
      Hero hero,
      int starNums,
      int collectCurNum,
      int collectTotalNum)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitValuesInHeroItem(
      int lvText = 1,
      int starNum = 1,
      int curNum = 0,
      int totalNum = 1,
      int state = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUnlockCardRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectFrameImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroItemUIController> EventOnHeroItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum HeroItemState
    {
      Get,
      NotGet,
      NotGetAndGlowing,
    }
  }
}
