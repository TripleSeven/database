﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/AutoOffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoOffButton;
    [AutoBind("./Margin/ArenaAutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaAutoButton;
    [AutoBind("./TopLeft", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topLeftGameObject;
    [AutoBind("./TopLeft/SimpleInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_actorSimpleInfoButton;
    [AutoBind("./TopLeft/SimpleInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actorSimpleInfoUIStateController;
    [AutoBind("./TopLeft/SimpleInfoButton/Head", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoHeadImage;
    [AutoBind("./TopLeft/SimpleInfoButton/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoNameText;
    [AutoBind("./TopLeft/SimpleInfoButton/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoLevelText;
    [AutoBind("./TopLeft/SimpleInfoButton/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoArmyImage;
    [AutoBind("./TopLeft/SimpleInfoButton/HeroHP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoHeroHPImage;
    [AutoBind("./TopLeft/SimpleInfoButton/HeroHP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoHeroHPText;
    [AutoBind("./TopLeft/SimpleInfoButton/SoldierHP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actorSimpleInfoSoldierHPImage;
    [AutoBind("./TopLeft/SimpleInfoButton/SoldierHP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actorSimpleInfoSoldierHPText;
    [AutoBind("./TopLeft/BossInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossInfoGameObject;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bossInfoButton;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bossInfoUIStateController;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/Head", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossInfoHeadImage;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossInfoNameText;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossInfoLevelText;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossInfoArmyImage;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossInfoHPImage;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossInfoHPText;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/Action/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossInfoActionImage;
    [AutoBind("./TopLeft/BossInfo/BossInfoButton/ActionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossInfoActionGroupGameObject;
    [AutoBind("./TopLeft/BossInfo/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_bossInfoDamageMarginTransform;
    [AutoBind("./TopLeft/BossInfo/Margin/DamageCount/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossInfoDamageText;
    [AutoBind("./TopLeft/BossInfo/Margin/BossPH/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossInfoPhaseText;
    [AutoBind("./TopLeft/BossInfo/ActionDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossInfoActionDescGameObject;
    [AutoBind("./TopLeft/BossInfo/ActionDesc/ActionDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bossInfoActionDescUIController;
    [AutoBind("./TopLeft/BossInfo/ActionDesc/ActionDesc/Content/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossInfoActionDescText;
    [AutoBind("./TopLeft/BossInfo/ActionDesc/ActionDesc/Content/SkillList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossInfoActionDescSkillListGameObject;
    [AutoBind("./Margin/TopLeft", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topLeftGameObject2;
    [AutoBind("./Margin/TopLeft/RegretButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regratButton;
    [AutoBind("./Margin/TopLeft/RegretButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretButtonUIStateController;
    [AutoBind("./Margin/TopLeft/RegretButton/Count/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretCountText;
    [AutoBind("./Margin/TopRight", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_topRightGameObject2;
    [AutoBind("./Margin/TopRight", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_topRightUIStateController;
    [AutoBind("./Margin/TopRight/PauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pauseButton;
    [AutoBind("./Margin/TopRight/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoButton;
    [AutoBind("./Margin/TopRight/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_autoButtonUIStateController;
    [AutoBind("./Margin/TopRight/FastButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastButton;
    [AutoBind("./Margin/TopRight/FastButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_fastButtonUIStateController;
    [AutoBind("./Margin/TopRight/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skipCombatButton;
    [AutoBind("./Margin/TopRight/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skipCombatButtonUIStateController;
    [AutoBind("./Margin/TopRight/TerrainInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_terrainInfoGameObject;
    [AutoBind("./Margin/TopRight/TerrainInfo", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_terrainInfoButton;
    [AutoBind("./Margin/TopRight/TerrainInfo/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoText;
    [AutoBind("./Margin/TopRight/TerrainInfo/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_terrainInfoImage;
    [AutoBind("./Margin/TopRight/TerrainInfo/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainInfoDefText;
    [AutoBind("./Margin/TerrainDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_terrainDescUIStateController;
    [AutoBind("./Margin/TerrainDescPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_terrainDescText;
    [AutoBind("./Margin/Bottom", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bottomGameObject;
    [AutoBind("./Margin/Bottom/DangerOnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_dangerOnButton;
    [AutoBind("./Margin/Bottom/DangerOffButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_dangerOffButton;
    [AutoBind("./Margin/Bottom/EndAllActionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_endAllActionButton;
    [AutoBind("./Margin/Bottom/UseSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_useSkillButton;
    [AutoBind("./Margin/Bottom/CancelSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelSkillButton;
    [AutoBind("./Margin/Bottom/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/Bottom/ChatButton/CountPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_chatRedPoint;
    [AutoBind("./Margin/Bottom/Skills", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillsUIStateController;
    [AutoBind("./Margin/Bottom/Skills/EndActionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_endActionButton;
    [AutoBind("./Margin/Bottom/Skills/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListGameObject;
    [AutoBind("./Margin/Bottom/Skills/List", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillListUIStateController;
    [AutoBind("./Margin/Bottom/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDescGameObject;
    [AutoBind("./Margin/Bottom/SkillHint", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillHintUIStateController;
    [AutoBind("./Margin/Bottom/SkillHint/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillHintText;
    [AutoBind("./Margin/Bottom/Status", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_statusUIStateController;
    [AutoBind("./Margin/Bottom/Status/TutnValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_statusTurnText;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusWinDescGameObject1;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_statusWinDescText1;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusWinDescGameObject2;
    [AutoBind("./Margin/Bottom/Status/ConditionGroup/Condition2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_statusWinDescText2;
    [AutoBind("./Margin/Bottom/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_danmakuStateCtrl;
    [AutoBind("./Margin/Bottom/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputBackButton;
    [AutoBind("./Margin/Bottom/DanmakuToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_danmakuToggle;
    [AutoBind("./Margin/Bottom/DanmakuToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_danmakuToggleUIStateController;
    [AutoBind("./Margin/Bottom/DanmakuToggle/InputWordButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputWordButton;
    [AutoBind("./Margin/Bottom/Danmaku/Input/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_danmakuInputField;
    [AutoBind("./Margin/Bottom/Danmaku/Input/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuSendButton;
    [AutoBind("./RegretInOrOut", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretUIStateController;
    [AutoBind("./Margin/RegretPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_regretPanelGameObject;
    [AutoBind("./Margin/RegretPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regratConfirmButton;
    [AutoBind("./Margin/RegretPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretConfirmUIStateController;
    [AutoBind("./Margin/RegretPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretCancelButton;
    [AutoBind("./Margin/RegretPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretCancelUIStateController;
    [AutoBind("./Margin/RegretPanel/BackwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretBackwardButton;
    [AutoBind("./Margin/RegretPanel/BackwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretBackwardUIStateController;
    [AutoBind("./Margin/RegretPanel/ForwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretForwardButton;
    [AutoBind("./Margin/RegretPanel/ForwardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretForwardUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/PreTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretPrevTurnButton;
    [AutoBind("./Margin/RegretPanel/RoundDetail/PreTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretPrevTurnUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/NextTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_regretNextTurnButton;
    [AutoBind("./Margin/RegretPanel/RoundDetail/NextTurnButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretNextTurnUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/RoundText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretTurnText;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretActionText;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionCountGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretTurnUIStateController;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionCountGroup/MyCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretMyCountText;
    [AutoBind("./Margin/RegretPanel/RoundDetail/Info/ActionCountGroup/EnemyCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_regretEnemyCountText;
    [AutoBind("./Margin/RegretPanel/EffectImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretEffectUIStateController;
    [AutoBind("./Margin/FXRegret", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_regretFxUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleReportPanelGameObject;
    [AutoBind("./Margin/BattlePlaybackPanel/Prefab/BattleHeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleHeroItemPrefab;
    [AutoBind("./Margin/BattlePlaybackPanel/LeftPlayer", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleReportLeftPlayerGameObject;
    [AutoBind("./Margin/BattlePlaybackPanel/RightPlayer", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleReportRightPlayerGameObject;
    [AutoBind("./Margin/BattlePlaybackPanel/ActionNoticPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleReportActionNoticeGameObject;
    [AutoBind("./Margin/BattlePlaybackPanel/ActionNoticPanel/ActionPart/Part/BlueImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleReportActionNoticePlayer0GameObject;
    [AutoBind("./Margin/BattlePlaybackPanel/ActionNoticPanel/ActionPart/Part/RedImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleReportActionNoticePlayer1GameObject;
    [AutoBind("./Margin/BattlePlaybackPanel/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportActionTimeUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/TimeGroup/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportActionTimeDetailUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/TimeGroup/Detail/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleReportActionTimeText;
    [AutoBind("./Margin/BattlePlaybackPanel/TimeGroup/Detail/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleReportActionTimeText2;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportButtonGroupUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/StartAndPauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportPauseOrPlayButton;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/LastStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportBackwardButton;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/LastStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportBackwardUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/NextStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportForwardButton;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/NextStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportForwardUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/LastRoundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportPrevTurnButton;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/LastRoundButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportPrevTurnUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/NextRoundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportNextTurnButton;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/NextRoundButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportNextTurnUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/Round", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportTurnUIStateController;
    [AutoBind("./Margin/BattlePlaybackPanel/RepeatedlyButtonGroup/Round/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleReportTurnText;
    [AutoBind("./CutsceneSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cutsceneSkillUIStateController;
    [AutoBind("./CutsceneSkill/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_cutsceneSkillIconImage;
    [AutoBind("./CutsceneSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cutsceneSkillNameText;
    [AutoBind("./Objective", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_objectiveGameObject;
    [AutoBind("./Objective/BGImage/WinDescGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_objectiveWinDescText;
    [AutoBind("./Objective/BGImage/LoseDescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_objectiveLoseDescText;
    [AutoBind("./Win", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGameObject;
    [AutoBind("./Lose", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_loseGameObject;
    [AutoBind("./TurnStart", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_turnStartGameObject;
    [AutoBind("./TurnStart/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_turnStartText;
    [AutoBind("./PlayerTurn", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerTurnGameObject;
    [AutoBind("./EnemyTurn", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyTurnGameObject;
    [AutoBind("./PeakRound", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaRoundUIStateController;
    [AutoBind("./PeakRound/Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaRoundText;
    [AutoBind("./MyAction", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_myActionUIStateController;
    [AutoBind("./MyAction/DetailPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_myActionHeadImage;
    [AutoBind("./MyAction/DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_myActionNameText;
    [AutoBind("./MyAction/DetailPanel/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_myActionPlayerTagImage;
    [AutoBind("./TeammateAction", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teammateActionUIStateController;
    [AutoBind("./TeammateAction/DetailPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teammateActionHeadImage;
    [AutoBind("./TeammateAction/DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teammateActionNameText;
    [AutoBind("./TeammateAction/DetailPanel/PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teammateActionPlayerTagImage;
    [AutoBind("./EnemyAction", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enemyActionUIStateController;
    [AutoBind("./EnemyAction/DetailPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enemyActionHeadImage;
    [AutoBind("./EnemyAction/DetailPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enemyActionNameText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/BattleSkillButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleSkillButtonPrefab;
    [AutoBind("./Prefabs/BossActionButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossActionButtonPrefab;
    [AutoBind("./Prefabs/BossActionSkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossActionSkillDescPrefab;
    private CommonUIStateController m_armyRelationButtonUIStateController;
    private CommonUIStateController m_armyRelationDescUIStateController;
    private Vector2 m_pointerDownPosition;
    private GameObjectPool<BattleSkillButton> m_battleSkillButtonPool;
    private GameObjectPool<BossActionButtonUIController> m_bossActionButtonUIControllerPool;
    private GameObjectPool<BossActionSkillDescUIController> m_bossActionSkillDescUIControllerPool;
    private BattleReportPlayerPanelUIController[] m_battleReportPlayerPanelUIControllers;
    private float m_chatRedPointLastTime;
    private float m_hideSkillHintTime;
    private bool m_isShowSkillHint;
    private bool m_isShowRegretPanel;
    private int m_developerClickCount;
    private int m_monsterTotalDamage;
    private int m_monsterTotalDamageChange;
    private float m_monsterTotalDamageTemp;
    private int m_turn;
    private string m_bossPhaseTextFormat;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_UpdateMonsterTotalDamage;
    private static DelegateBridge __Hotfix_StartBattle;
    private static DelegateBridge __Hotfix_StopBattle;
    private static DelegateBridge __Hotfix_WinOrLoseTweenFinished;
    private static DelegateBridge __Hotfix_SetWinLoseConditions;
    private static DelegateBridge __Hotfix_ShowTurnStatus;
    private static DelegateBridge __Hotfix_ShowObjecive;
    private static DelegateBridge __Hotfix_SetTurn;
    private static DelegateBridge __Hotfix_ShowTurnStart;
    private static DelegateBridge __Hotfix_ShowPlayerTurn;
    private static DelegateBridge __Hotfix_ShowEnemyTurn;
    private static DelegateBridge __Hotfix_ShowPeakAreanBoRound;
    private static DelegateBridge __Hotfix_ShowMyAction;
    private static DelegateBridge __Hotfix_ShowTeammateAction;
    private static DelegateBridge __Hotfix_ShowEnemyAction;
    private static DelegateBridge __Hotfix_HidePauseButton;
    private static DelegateBridge __Hotfix_ShowChatButton;
    private static DelegateBridge __Hotfix_SetAutoBattle;
    private static DelegateBridge __Hotfix_HideAutoBattleButton;
    private static DelegateBridge __Hotfix_SetArenaAutoBattle;
    private static DelegateBridge __Hotfix_SetFastBattle;
    private static DelegateBridge __Hotfix_SetSkipCombatMode;
    private static DelegateBridge __Hotfix_SetActorSimpleInfo;
    private static DelegateBridge __Hotfix_ShowActorSimpleInfo;
    private static DelegateBridge __Hotfix_IsShowActorSimpleInfo;
    private static DelegateBridge __Hotfix_SetBossInfo;
    private static DelegateBridge __Hotfix_SetBossInfoHp;
    private static DelegateBridge __Hotfix_SetBossInfoPhase;
    private static DelegateBridge __Hotfix_SetBossInfoActionCount;
    private static DelegateBridge __Hotfix_ShowBossInfoAndDamage;
    private static DelegateBridge __Hotfix_ShowBossInfo;
    private static DelegateBridge __Hotfix_SetMonsterTotalDamage;
    private static DelegateBridge __Hotfix_ShowBossInfoActionDesc;
    private static DelegateBridge __Hotfix_AddBossActionSkillDisc;
    private static DelegateBridge __Hotfix_HideBossInfoActionDesc;
    private static DelegateBridge __Hotfix_SetShowDanger;
    private static DelegateBridge __Hotfix_HideDangerButton;
    private static DelegateBridge __Hotfix_ShowEndAction;
    private static DelegateBridge __Hotfix_ShowEndAllAction;
    private static DelegateBridge __Hotfix_SetActionOrderType;
    private static DelegateBridge __Hotfix_RefreshChatRedState;
    private static DelegateBridge __Hotfix_ComputeActionIconPositionScale;
    private static DelegateBridge __Hotfix_ShowTopUI;
    private static DelegateBridge __Hotfix_IsShowTopUI;
    private static DelegateBridge __Hotfix_ShowBottomUI;
    private static DelegateBridge __Hotfix_IsShowBottomUI;
    private static DelegateBridge __Hotfix_ShowSkills;
    private static DelegateBridge __Hotfix_GetSkillButtonRectTransform;
    private static DelegateBridge __Hotfix_HideSkills;
    private static DelegateBridge __Hotfix_ShowUseOrCancelSkill;
    private static DelegateBridge __Hotfix_ShowUseSkill;
    private static DelegateBridge __Hotfix_SetCurrentSkill;
    private static DelegateBridge __Hotfix_ShowSkillHint;
    private static DelegateBridge __Hotfix_HideSkillHint;
    private static DelegateBridge __Hotfix_HighlightSkillHint;
    private static DelegateBridge __Hotfix_ShowTerrainInfo;
    private static DelegateBridge __Hotfix_HideTerrainInfo;
    private static DelegateBridge __Hotfix_ShowArmyRelationButton;
    private static DelegateBridge __Hotfix_HideArmyRelation;
    private static DelegateBridge __Hotfix_ShowArmyRelationDesc;
    private static DelegateBridge __Hotfix_HideArmyRelationDesc;
    private static DelegateBridge __Hotfix_IsArmyRelationDescVisible;
    private static DelegateBridge __Hotfix_ShowCutsceneSkill;
    private static DelegateBridge __Hotfix_HideCutsceneSkill;
    private static DelegateBridge __Hotfix_ClearDanmakuUIInputField;
    private static DelegateBridge __Hotfix_OnPauseButtonClick;
    private static DelegateBridge __Hotfix_OnArmyRelationButtonClick;
    private static DelegateBridge __Hotfix_OnAutoButtonClick;
    private static DelegateBridge __Hotfix_OnAutoOffButtonClick;
    private static DelegateBridge __Hotfix_OnArenaAutoButtonClick;
    private static DelegateBridge __Hotfix_OnFastButtonClick;
    private static DelegateBridge __Hotfix_OnSkipCombatButtonClick;
    private static DelegateBridge __Hotfix_OnDangerOnButtonClick;
    private static DelegateBridge __Hotfix_OnDangerOffButtonClick;
    private static DelegateBridge __Hotfix_OnEndActionButtonClick;
    private static DelegateBridge __Hotfix_OnEndAllActionButtonClick;
    private static DelegateBridge __Hotfix_OnActorSimpleInfoButtonClick;
    private static DelegateBridge __Hotfix_OnBossInfoButtonClick;
    private static DelegateBridge __Hotfix_OnUseSkillButtonClick;
    private static DelegateBridge __Hotfix_OnCancelSkillButtonClick;
    private static DelegateBridge __Hotfix_BattleSkillButton_OnClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnTerrainInfoButtonClick;
    private static DelegateBridge __Hotfix_DeveloperModeClick;
    private static DelegateBridge __Hotfix_BossActionButtonUIController_OnClick;
    private static DelegateBridge __Hotfix_ShowDanmakuToggle;
    private static DelegateBridge __Hotfix_ShowCurTurnDanmaku;
    private static DelegateBridge __Hotfix_OnDanmakuToggleValueChanged;
    private static DelegateBridge __Hotfix_OnDanmakuInputWordButtonClick;
    private static DelegateBridge __Hotfix_OnDanmakuInputBackButtonClick;
    private static DelegateBridge __Hotfix_OnDanmakuSendButtonClick;
    private static DelegateBridge __Hotfix_SetRegretCount;
    private static DelegateBridge __Hotfix_SetRegretTurnStatus;
    private static DelegateBridge __Hotfix_SetRegretButtonStatus;
    private static DelegateBridge __Hotfix_ShowRegretButton;
    private static DelegateBridge __Hotfix_ShowRegretPanel;
    private static DelegateBridge __Hotfix_ShowRegretConfirmFx;
    private static DelegateBridge __Hotfix_ResetBattleReport;
    private static DelegateBridge __Hotfix_ShowBattleReportPanel;
    private static DelegateBridge __Hotfix_ShowBattleReportPauseButton;
    private static DelegateBridge __Hotfix_SetBattleReportTurnStatus;
    private static DelegateBridge __Hotfix_SetBattleReportPlayer;
    private static DelegateBridge __Hotfix_SetBattleReportPlayerWinCount;
    private static DelegateBridge __Hotfix_SetBattleLivePlayerWinCount;
    private static DelegateBridge __Hotfix_SetBattleReportActionPlayer;
    private static DelegateBridge __Hotfix_GetBattleReportPlayerPanelUIController;
    private static DelegateBridge __Hotfix_SetBattleReportPaused;
    private static DelegateBridge __Hotfix_SetBattleReportButtonStatus;
    private static DelegateBridge __Hotfix_ShowBattleLivePanel;
    private static DelegateBridge __Hotfix_SetBattleLivePlayer;
    private static DelegateBridge __Hotfix_SetBattleLivePlayerHeros;
    private static DelegateBridge __Hotfix_ShowBattleLiveCurrentActionCountdown;
    private static DelegateBridge __Hotfix_SetBattleLiveCurrentActionCountdown;
    private static DelegateBridge __Hotfix_ShowBattleLiveActionCountdown;
    private static DelegateBridge __Hotfix_SetBattleLiveActionCountdown;
    private static DelegateBridge __Hotfix_SetBattleLivePlayerHeroAlive;
    private static DelegateBridge __Hotfix_ShowBattleLivePlayerBigExpression;
    private static DelegateBridge __Hotfix_OnRegretButtonClick;
    private static DelegateBridge __Hotfix_OnRegretConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnRegretCancelButtonClick;
    private static DelegateBridge __Hotfix_OnRegretBackwardButtonClick;
    private static DelegateBridge __Hotfix_OnRegretForwardButtonClick;
    private static DelegateBridge __Hotfix_OnRegretPrevTurnButtonClick;
    private static DelegateBridge __Hotfix_OnRegretNextTurnButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportPauseOrPlayButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportBackwardButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportForwardButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportPrevTurnButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportNextTurnButtonClick;
    private static DelegateBridge __Hotfix_OnScenePointerDown;
    private static DelegateBridge __Hotfix_OnScenePointerUp;
    private static DelegateBridge __Hotfix_OnScenePointerClick;
    private static DelegateBridge __Hotfix_OnSceneBeginDrag;
    private static DelegateBridge __Hotfix_OnSceneEndDrag;
    private static DelegateBridge __Hotfix_OnSceneDrag;
    private static DelegateBridge __Hotfix_OnScene3DTouch;
    private static DelegateBridge __Hotfix_add_EventOnPauseBattle;
    private static DelegateBridge __Hotfix_remove_EventOnPauseBattle;
    private static DelegateBridge __Hotfix_add_EventOnShowArmyRelation;
    private static DelegateBridge __Hotfix_remove_EventOnShowArmyRelation;
    private static DelegateBridge __Hotfix_add_EventOnAutoBattle;
    private static DelegateBridge __Hotfix_remove_EventOnAutoBattle;
    private static DelegateBridge __Hotfix_add_EventOnFastBattle;
    private static DelegateBridge __Hotfix_remove_EventOnFastBattle;
    private static DelegateBridge __Hotfix_add_EventOnSkipCombat;
    private static DelegateBridge __Hotfix_remove_EventOnSkipCombat;
    private static DelegateBridge __Hotfix_add_EventOnShowDanger;
    private static DelegateBridge __Hotfix_remove_EventOnShowDanger;
    private static DelegateBridge __Hotfix_add_EventOnEndAction;
    private static DelegateBridge __Hotfix_remove_EventOnEndAction;
    private static DelegateBridge __Hotfix_add_EventOnShowActorInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowActorInfo;
    private static DelegateBridge __Hotfix_add_EventOnShowBossInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowBossInfo;
    private static DelegateBridge __Hotfix_add_EventOnEndAllAction;
    private static DelegateBridge __Hotfix_remove_EventOnEndAllAction;
    private static DelegateBridge __Hotfix_add_EventOnUseSkill;
    private static DelegateBridge __Hotfix_remove_EventOnUseSkill;
    private static DelegateBridge __Hotfix_add_EventOnCancelSkill;
    private static DelegateBridge __Hotfix_remove_EventOnCancelSkill;
    private static DelegateBridge __Hotfix_add_EventOnWinOrLoseEnd;
    private static DelegateBridge __Hotfix_remove_EventOnWinOrLoseEnd;
    private static DelegateBridge __Hotfix_add_EventOnShowChat;
    private static DelegateBridge __Hotfix_remove_EventOnShowChat;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnSelectSkill;
    private static DelegateBridge __Hotfix_remove_EventOnSelectSkill;
    private static DelegateBridge __Hotfix_add_EventOnPointerDown;
    private static DelegateBridge __Hotfix_remove_EventOnPointerDown;
    private static DelegateBridge __Hotfix_add_EventOnPointerUp;
    private static DelegateBridge __Hotfix_remove_EventOnPointerUp;
    private static DelegateBridge __Hotfix_add_EventOnPointerClick;
    private static DelegateBridge __Hotfix_remove_EventOnPointerClick;
    private static DelegateBridge __Hotfix_add_EventOn3DTouch;
    private static DelegateBridge __Hotfix_remove_EventOn3DTouch;
    private static DelegateBridge __Hotfix_add_EventOnShowCurTurnDanmaku;
    private static DelegateBridge __Hotfix_remove_EventOnShowCurTurnDanmaku;
    private static DelegateBridge __Hotfix_add_EventOnCloseCurTurnDanmaku;
    private static DelegateBridge __Hotfix_remove_EventOnCloseCurTurnDanmaku;
    private static DelegateBridge __Hotfix_add_EventOnShowOneDanmaku;
    private static DelegateBridge __Hotfix_remove_EventOnShowOneDanmaku;
    private static DelegateBridge __Hotfix_add_EventOnRegretActive;
    private static DelegateBridge __Hotfix_remove_EventOnRegretActive;
    private static DelegateBridge __Hotfix_add_EventOnRegretConfirm;
    private static DelegateBridge __Hotfix_remove_EventOnRegretConfirm;
    private static DelegateBridge __Hotfix_add_EventOnRegretCancel;
    private static DelegateBridge __Hotfix_remove_EventOnRegretCancel;
    private static DelegateBridge __Hotfix_add_EventOnRegretBackward;
    private static DelegateBridge __Hotfix_remove_EventOnRegretBackward;
    private static DelegateBridge __Hotfix_add_EventOnRegretForward;
    private static DelegateBridge __Hotfix_remove_EventOnRegretForward;
    private static DelegateBridge __Hotfix_add_EventOnRegretPrevTurn;
    private static DelegateBridge __Hotfix_remove_EventOnRegretPrevTurn;
    private static DelegateBridge __Hotfix_add_EventOnRegretNextTurn;
    private static DelegateBridge __Hotfix_remove_EventOnRegretNextTurn;
    private static DelegateBridge __Hotfix_add_EventOnBattlePause;
    private static DelegateBridge __Hotfix_remove_EventOnBattlePause;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportPause;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportPause;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportPlay;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportPlay;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportBackward;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportBackward;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportForward;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportForward;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportPrevTurn;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportPrevTurn;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportNextTurn;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportNextTurn;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMonsterTotalDamage(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win, bool skipWinLoseAnim)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WinOrLoseTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWinLoseConditions(string winDesc, string loseDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTurnStatus(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowObjecive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTurn(int turn, int turnMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTurnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnemyTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPeakAreanBoRound(int round, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMyAction(BattleRoomPlayer player, int playerTagIndex, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTeammateAction(BattleRoomPlayer player, int playerIndex, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnemyAction(BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HidePauseButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChatButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideAutoBattleButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaAutoBattle(bool auto)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFastBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkipCombatMode(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorSimpleInfo(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActorSimpleInfo(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowActorSimpleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossInfo(
      BattleActor a,
      ConfigDataAncientCallBehavioralDescInfo behaviorDescInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossInfoHp(int hp, int hpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossInfoPhase(int phase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossInfoActionCount(
      int actionCount,
      int actionLoopCount,
      ClientBattleActorActionState actionState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBossInfoAndDamage(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBossInfo(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMonsterTotalDamage(int damage, bool animNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBossInfoActionDesc(
      Transform buttonTransform,
      AncientCallBossBehavioralDesc behavioralDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddBossActionSkillDisc(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBossInfoActionDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetShowDanger(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideDangerButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEndAction(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEndAllAction(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionOrderType(ActionOrderType actionOrderType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshChatRedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ComputeActionIconPositionScale(int index, out Vector2 pos, out float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTopUI(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowTopUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBottomUI(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowBottomUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkills(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetSkillButtonRectTransform(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUseOrCancelSkill(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUseSkill(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentSkill(BattleSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkillHint(StringTableId id, float hideTime = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSkillHint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HighlightSkillHint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTerrainInfo(
      ConfigDataTerrainInfo terrainInfo,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArmyRelationButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideArmyRelationDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArmyRelationDescVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCutsceneSkill(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideCutsceneSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearDanmakuUIInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPauseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArmyRelationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoOffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaAutoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipCombatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDangerOnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDangerOffButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndActionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEndAllActionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActorSimpleInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSkillButton_OnClick(BattleSkillButton sb)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTerrainInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeveloperModeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BossActionButtonUIController_OnClick(BossActionButtonUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDanmakuToggle(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCurTurnDanmaku(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputWordButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegretCount(int remainCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegretTurnStatus(int turn, int actionTeam, int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegretButtonStatus(
      bool canBackward,
      bool canForward,
      bool canPrevTurn,
      bool canNextTurn,
      bool canConfirm,
      bool canCancel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegretButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegretPanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRegretConfirmFx(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportPanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportPauseButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportTurnStatus(int turn, int turnMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportPlayer(
      int playerIndex,
      PeakArenaBattleReportPlayerSummaryInfo player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportPlayerWinCount(int playerIndex, int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLivePlayerWinCount(int playerIndex, int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportActionPlayer(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleReportPlayerPanelUIController GetBattleReportPlayerPanelUIController(
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportPaused(bool isPaused)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportButtonStatus(
      bool canBackward,
      bool canForward,
      bool canPrevTurn,
      bool canNextTurn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLivePanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLivePlayer(int playerIndex, BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLivePlayerHeros(int playerIndex, List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveCurrentActionCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLiveCurrentActionCountdown(TimeSpan ts, bool isReserve)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveActionCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLiveActionCountdown(int playerIndex, TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLivePlayerHeroAlive(int playerIndex, int actorId, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLivePlayerBigExpression(int playerIndex, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretBackwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretForwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretPrevTurnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRegretNextTurnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportPauseOrPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportBackwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportForwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportPrevTurnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportNextTurnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScenePointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSceneDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScene3DTouch(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPauseBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowArmyRelation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnFastBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<SkipCombatMode> EventOnSkipCombat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnShowDanger
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndAction
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowActorInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBossInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndAllAction
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnUseSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancelSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnWinOrLoseEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnSelectSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData.InputButton, Vector2> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Vector2> EventOn3DTouch
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnShowCurTurnDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseCurTurnDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int> EventOnShowOneDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretActive
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretBackward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretForward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretPrevTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRegretNextTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattlePause
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPause
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPlay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportBackward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportForward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPrevTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportNextTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
