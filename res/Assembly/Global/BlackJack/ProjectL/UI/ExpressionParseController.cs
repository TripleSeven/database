﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ExpressionParseController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ExpressionParseController : MonoBehaviour
  {
    private Image m_imageToClone;
    private string m_regexParamStr;
    private float m_offsetX;
    private float m_offsetY;
    private float m_fontSize;
    private int m_richTextIndexOffSet;
    private float m_gapSize;
    private string emSpace;
    private bool m_isInited;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetChatItemWithTextAndSmallExpression;
    private static DelegateBridge __Hotfix_SetUITextWithSmallExpression;
    private static DelegateBridge __Hotfix_ParseEmoji;

    [MethodImpl((MethodImplOptions) 32768)]
    public ExpressionParseController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GameObject image, int gap, float offsetX = 0.0f, float offsetY = 0.0f, float fontSize = 25f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChatItemWithTextAndSmallExpression(Text text, string inputString)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SetUITextWithSmallExpression(Text textToEdit, string inputString)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ExpressionParseController.PosStringTuple> ParseEmoji(
      string inputString,
      out string changedStr)
    {
      // ISSUE: unable to decompile the method.
    }

    public class PosStringTuple
    {
      public int pos;
      public int expressionKey;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public PosStringTuple(int p, int key)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
