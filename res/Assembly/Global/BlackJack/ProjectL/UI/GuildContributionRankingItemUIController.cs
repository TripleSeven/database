﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildContributionRankingItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildContributionRankingItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankingItemAnimation;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIcon;
    [AutoBind("./HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameDummy;
    [AutoBind("./HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./JobText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobText;
    [AutoBind("./AllContributionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_totalContributionText;
    [AutoBind("./TodayContributionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_todayContributionText;
    [AutoBind("./RankingIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankValueImage;
    [AutoBind("./RankingValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankValueText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetData;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetData(
      GuildMassiveCombatMemberInfo guildMassiveCombatMemberInfo,
      int rank)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
