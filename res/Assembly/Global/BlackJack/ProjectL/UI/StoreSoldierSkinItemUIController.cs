﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreSoldierSkinItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class StoreSoldierSkinItemUIController : UIControllerBase
  {
    public StoreType m_storeType;
    public ConfigDataFixedStoreItemInfo m_storeItemConfig;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private UISpineGraphic m_soldierGraphic;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_storeItemButton;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_spineAnimObj;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_soldierName;
    [AutoBind("./Tape /TSurplusText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_restTime;
    [AutoBind("./PricePanel/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_priceText;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_itemUIStateCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetSoldierSkinItemInfo;
    private static DelegateBridge __Hotfix_SetItemState;
    private static DelegateBridge __Hotfix_SetSoldierSpineAnim;
    private static DelegateBridge __Hotfix_OnStoreItemClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreSoldierSkinItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSoldierSkinItemInfo(FixedStoreItem soldierSkinItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSoldierSpineAnim(ConfigDataSoldierSkinInfo soldierSkinInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnStoreItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreSoldierSkinItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
