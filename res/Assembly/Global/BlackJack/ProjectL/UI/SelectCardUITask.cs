﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelectCardUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class SelectCardUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private List<int> m_heroIdList;
    private List<Goods> m_rewards;
    private List<Goods> m_extraRewards;
    private bool m_isShowRewardsPanel;
    private bool m_isRefresShareTenPanel;
    private SelectCardUIController m_selectCardUIController;
    private SelectCardBackground3DController m_selectCardBackground3DController;
    private ShareTenSelectCardUIController m_shareTenSelectCardUIController;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitlizeBeforeManagerStartIt;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnReturn;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnArvhive;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnShowSelectCardHelp;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnShowActivityDetail;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnBagCapcityNotEnough;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnCrystalNotEnough;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_SelectCardUIController_OnCardSelect;
    private static DelegateBridge __Hotfix_OnInstagramShareTenClick;
    private static DelegateBridge __Hotfix_OnTwitterShareTenClick;
    private static DelegateBridge __Hotfix_OnFacebookShareTenClick;
    private static DelegateBridge __Hotfix_OnWeiBoShareTenClick;
    private static DelegateBridge __Hotfix_OnWeChatShareTenClick;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void InitlizeBeforeManagerStartIt()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnArvhive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnShowSelectCardHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnShowActivityDetail(CardPool pool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnBagCapcityNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUIController_OnCardSelect(int cardPoolId, bool isSingleSlect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInstagramShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFacebookShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
