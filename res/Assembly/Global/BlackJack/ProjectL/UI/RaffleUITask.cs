﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Scene;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class RaffleUITask : UITask
  {
    public static string ParamKey_PoolId = "PoolId";
    protected SceneLayerBase m_rewardPanelLayer;
    protected RaffleUIController m_mainCtrl;
    protected Raffle3DUIController m_raffle3dUICtrl;
    protected RaffleRewardUIController m_rewardPanelCtrl;
    protected RafflePool m_rafflePool;
    protected RaffleUITask.DrawAnimationState m_drawAniamtionState;
    protected int m_drawnRaffleId;
    protected List<Goods> m_drawnGoods;
    public Action EventOnClose;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateDataCache;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitLayerStateOnResume;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateDrawAnimation;
    private static DelegateBridge __Hotfix_ClearContextOnPause;
    private static DelegateBridge __Hotfix_OnBgButtonClick;
    private static DelegateBridge __Hotfix_OnShowHelpButtonClick;
    private static DelegateBridge __Hotfix_OnRewardPanelButtonClick;
    private static DelegateBridge __Hotfix_OnDoRaffleButtonClick;
    private static DelegateBridge __Hotfix_StartDrawAnimation;
    private static DelegateBridge __Hotfix_OpenSrBox;
    private static DelegateBridge __Hotfix_IsSrBox;
    private static DelegateBridge __Hotfix_OnRechargeDialogResult;
    private static DelegateBridge __Hotfix_OnNotEnoughItemDialogResult;
    private static DelegateBridge __Hotfix_OnRewardPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_OnLevelEffectBgButtonClick;
    private static DelegateBridge __Hotfix_GetRewardGoodsUITask_OnClose;
    private static DelegateBridge __Hotfix_ShowErrorTipWnd;
    private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
    private static DelegateBridge __Hotfix_EnablePipelineStateMask;
    private static DelegateBridge __Hotfix_ShowNotEnoughCrystalMsgBox;
    private static DelegateBridge __Hotfix_ShowNotEoughItemMsgBox;
    private static DelegateBridge __Hotfix_get_PlayerCtx;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLayerStateOnResume()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateDrawAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearContextOnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnShowHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRewardPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDoRaffleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StartDrawAnimation(int raffleId, List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OpenSrBox(Goods goods, Action<List<Goods>> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSrBox(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeDialogResult(DialogBoxResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnNotEnoughItemDialogResult(DialogBoxResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRewardPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnLevelEffectBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowErrorTipWnd(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPipelineStateMaskNeedUpdate(RaffleUITask.PipeLineStateMaskType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EnablePipelineStateMask(RaffleUITask.PipeLineStateMaskType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEnoughCrystalMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEoughItemMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    protected ProjectLPlayerContext PlayerCtx
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum PipeLineStateMaskType
    {
      ShowRewardPanel,
      ShowDrawAnimation,
    }

    public enum DrawAnimationState
    {
      Idle,
      Drawing,
      RewardLevel,
      RewardDetail,
    }
  }
}
