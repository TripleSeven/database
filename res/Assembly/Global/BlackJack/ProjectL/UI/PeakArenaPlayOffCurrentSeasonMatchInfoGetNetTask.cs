﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask : UINetTask
  {
    private int m_matchupId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck;
    private static DelegateBridge __Hotfix_set_MatchupInfo;
    private static DelegateBridge __Hotfix_get_MatchupInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPeakArenaPlayOffCurrentSeasonMatchInfoGetAck(
      int result,
      PeakArenaPlayOffMatchupInfo matchupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public PeakArenaPlayOffMatchupInfo MatchupInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] protected set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
