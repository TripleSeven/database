﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ConfessionDialogBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ConfessionDialogBoxUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./TextGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    private bool m_isOpened;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_IsOpened;
    private static DelegateBridge __Hotfix_SetWords;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWords(string txt)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
