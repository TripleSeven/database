﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleResultBossUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleResultBossUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./AccountingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Grade", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_evaluateUIStateController;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Grade/EvaluateGroupContent/EvaluateLetterItemBig", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_evaluateImage;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/NewRecordEffect/EvaluateGroupContentEffect/EvaluateLetterItemBig", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_evaluateImage2;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/AmountHurt/HurtAltogetherText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damageText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/TopHurt/HurtAltogetherText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_oldMaxDamageText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/AmountHurt/HurtGoUpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damageDiffText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/AmountHurt/HurtGoUpText/Arrow", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_damageDiffArrowUIStateControllert;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Rank/RankNow/NumText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Rank/RankInfor/TitleTextRt/TitleNum", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText1;
    [AutoBind("./AccountingPanel/BGImages/BgImage/FrameImage/Rank/RankInfor/NumText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damageText1;
    [AutoBind("./ClickScreenContinue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickScreenContinueGameObject;
    private bool m_isClick;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowBattleResultBoss;
    private static DelegateBridge __Hotfix_Co_ShowBattleResultBoss;
    private static DelegateBridge __Hotfix_Co_TextNumberEffect;
    private static DelegateBridge __Hotfix_Co_SetAndWaitUIState;
    private static DelegateBridge __Hotfix_Co_WaitClick;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    private BattleResultBossUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleResultBoss(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowBattleResultBoss(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_TextNumberEffect(
      Text text,
      int oldValue,
      int newValue,
      float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
