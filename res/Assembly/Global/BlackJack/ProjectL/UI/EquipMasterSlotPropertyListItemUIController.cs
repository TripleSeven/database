﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterSlotPropertyListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterSlotPropertyListItemUIController : UIControllerBase
  {
    private IConfigDataLoader m_configDataLoader;
    private int m_index;
    private HeroJobSlotRefineryProperty m_property;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text NameText;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text ValueText;
    [AutoBind("./ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image ProgressBarImage;
    [AutoBind("./RefineButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RefineButton;
    [AutoBind("./ValueInputField1", AutoBindAttribute.InitState.Inactive, false)]
    public InputField GMValueInputField;
    [AutoBind("./GMRefineButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button GMRefineButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetIndex;
    private static DelegateBridge __Hotfix_GetIndex;
    private static DelegateBridge __Hotfix_UpdateProperty;
    private static DelegateBridge __Hotfix_RegistEvents;
    private static DelegateBridge __Hotfix_UnRegistEvents;
    private static DelegateBridge __Hotfix_OnRefineButtonClick;
    private static DelegateBridge __Hotfix_OnGMSetPropertyButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnRefineButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRefineButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGMSetPropertyButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGMSetPropertyButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateProperty(
      HeroJobSlotRefineryProperty heroJobSlotRefineryProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegistEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnRegistEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGMSetPropertyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, HeroJobSlotRefineryProperty> EventOnRefineButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnGMSetPropertyButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
