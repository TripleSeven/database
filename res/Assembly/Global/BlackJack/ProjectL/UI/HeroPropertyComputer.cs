﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroPropertyComputer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroPropertyComputer
  {
    private IConfigDataLoader m_configDataLoader;
    private BattleProperty m_property0;
    private BattleProperty m_property1;
    private BattleProperty m_property2;
    private BattlePropertyModifier m_propertyModifier;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_ComputeHeroProperties_1;
    private static DelegateBridge __Hotfix_ComputeHeroEquipmentProperties_1;
    private static DelegateBridge __Hotfix_ComputeHeroEquipmentProperties_2;
    private static DelegateBridge __Hotfix_ComputeSoldierCommandProperties_0;
    private static DelegateBridge __Hotfix_ComputeSoldierCommandProperties_1;
    private static DelegateBridge __Hotfix_ComputeHeroBuffProperties;
    private static DelegateBridge __Hotfix_ComputeSoldierBuffCommandProperties;
    private static DelegateBridge __Hotfix_CollectJobMasterPropertyModifier_2;
    private static DelegateBridge __Hotfix_CollectJobEquipmentRefineryPropertyModifier_3;
    private static DelegateBridge __Hotfix_CollectFetterPropertyModifier;
    private static DelegateBridge __Hotfix_CollectHeartFetterPropertyModifier;
    private static DelegateBridge __Hotfix_CollectEquipmentPropertyModifier_2;
    private static DelegateBridge __Hotfix_CollectEquipmentPropertyModifier_3;
    private static DelegateBridge __Hotfix_CollectSoldierPassiveSkillPropertyModifier;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceSkillInfos_0;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceSkillInfos_1;
    private static DelegateBridge __Hotfix_CollectSkinPropertyModifier;
    private static DelegateBridge __Hotfix_CollectBuffPropertyModifier;
    private static DelegateBridge __Hotfix_CollectBuffPropertyExchange;
    private static DelegateBridge __Hotfix_CollectJobMasterPropertyModifier_0;
    private static DelegateBridge __Hotfix_CollectJobEquipmentRefineryPropertyModifier_0;
    private static DelegateBridge __Hotfix_CollectEquipmentPropertyModifier_0;
    private static DelegateBridge __Hotfix_ComputeHeroProperties_0;
    private static DelegateBridge __Hotfix_ComputeHeroEquipmentProperties_0;
    private static DelegateBridge __Hotfix_CollectJobMasterPropertyModifier_1;
    private static DelegateBridge __Hotfix_CollectJobEquipmentRefineryPropertyModifier_1;
    private static DelegateBridge __Hotfix_CollectEquipmentPropertyModifier_1;
    private static DelegateBridge __Hotfix_CollectJobPropertyModifier;
    private static DelegateBridge __Hotfix_CollectJobEquipmentRefineryPropertyModifier_2;
    private static DelegateBridge __Hotfix_CollectPassiveSkillStaticPropertyModifier;
    private static DelegateBridge __Hotfix_CollectStaticPropertyModifier;
    private static DelegateBridge __Hotfix_get_Property0;
    private static DelegateBridge __Hotfix_get_Property1;
    private static DelegateBridge __Hotfix_get_Property2;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPropertyComputer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroProperties(
      Hero hero,
      int jobConnectionId,
      int heroLevel0 = -1,
      int heroLevel1 = -1,
      int starLevel0 = -1,
      int starLevel1 = -1,
      int jobLevel0 = -1,
      int jobLevel1 = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroEquipmentProperties(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroEquipmentProperties(Hero hero, List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierCommandProperties(Hero hero, ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierCommandProperties(
      BattleHero hero,
      ConfigDataSoldierInfo soldierInfo,
      List<TrainingTech> techs,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroBuffProperties(BattleActor a, bool isPvp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierBuffCommandProperties(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(Hero hero, int jobConnectionId, int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobEquipmentRefineryPropertyModifier(
      Hero hero,
      int jobConnectionId,
      int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectFetterPropertyModifier(Dictionary<int, int> fetters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeartFetterPropertyModifier(int heroId, int heartFetterLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(Hero hero, List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSoldierPassiveSkillPropertyModifier(
      ConfigDataSoldierInfo soldierInfo,
      List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> GetEquipmentResonanceSkillInfos(
      ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> GetEquipmentResonanceSkillInfos(
      ulong[] equipmentIds,
      List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSkinPropertyModifier(ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyModifier(BattleActor a, bool isDynamic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyExchange(BattleActor a, BattleProperty property)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobEquipmentRefineryPropertyModifier(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroProperties(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroEquipmentProperties(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobEquipmentRefineryPropertyModifier(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropertyModifier(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobPropertyModifier(ConfigDataJobInfo jobInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobEquipmentRefineryPropertyModifier(
      List<HeroJobRefineryProperty> refineryProperties)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPassiveSkillStaticPropertyModifier(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectStaticPropertyModifier(PropertyModifyType modifyType, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleProperty Property0
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty Property1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty Property2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
