﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ServerListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ServerListUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/ServerListPanel/ServerScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liServerListGroup;
    [AutoBind("./Detail/AreaListPanel/ServerScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_areaGroup;
    [AutoBind("./Detail/CharListPanel/ServerScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_headGroup;
    [AutoBind("./Detail/CharListPanel/NoChar", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noChar;
    [AutoBind("./Prefabs/ServerItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_serverListItemTemplate;
    [AutoBind("./Prefabs/CharItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_headItemTemplate;
    [AutoBind("./Prefabs/AreaItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_areaItemTemplate;
    [AutoBind("./Prefabs/ServerItem/ServerNameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_liServerNameText;
    [AutoBind("./Prefabs/ServerItem/NewBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liNewServerIcon;
    [AutoBind("./Prefabs/ServerItem/StateImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_liServerStateIcon;
    [AutoBind("./Prefabs/ServerItem/CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liServerCharIcon;
    [AutoBind("./Prefabs/ServerItem/CharGroup/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_liServerCharImage;
    [AutoBind("./Prefabs/ServerItem/CharGroup/LevelNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_liServerCharLevel;
    [AutoBind("./Prefabs/ServerItem/Maintenance", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_liMaitainServerIcon;
    private IConfigDataLoader m_configDataLoader;
    private Dictionary<string, List<LoginUITask.ServerInfo>> m_serverGroup;
    private List<LoginUITask.ServerInfo> m_servers;
    private GameObject m_selectArea;
    private int m_selectServerID;
    private List<LoginUITask.ExistCharInfo> m_existCharsInfo;
    private string m_roleListURL;
    private float m_fDebugReportTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetServerList;
    private static DelegateBridge __Hotfix_ClearServerListItem;
    private static DelegateBridge __Hotfix_RefreshServerArea;
    private static DelegateBridge __Hotfix_RefreshPlayerHead;
    private static DelegateBridge __Hotfix_AddServerListItem_1;
    private static DelegateBridge __Hotfix_AddServerListItem_0;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_LastLoginTimeDescription;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnCloseButtonClicked;
    private static DelegateBridge __Hotfix_OnAreaClick;
    private static DelegateBridge __Hotfix_OnUIClosed;
    private static DelegateBridge __Hotfix_add_EventOnClosed;
    private static DelegateBridge __Hotfix_remove_EventOnClosed;

    [MethodImpl((MethodImplOptions) 32768)]
    private ServerListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerList(
      List<LoginUITask.ServerInfo> serverList,
      List<LoginUITask.ServerInfo> recentLoginServerList,
      List<LoginUITask.ExistCharInfo> existCharsInfo,
      int selectServerID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearServerListItem(GameObject listGroup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshServerArea()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshPlayerHead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddServerListItem(GameObject listGroup, List<LoginUITask.ServerInfo> serverList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddServerListItem(GameObject listGroup, LoginUITask.ServerInfo server)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string LastLoginTimeDescription(int hour)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAreaClick(GameObject areaItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnClosed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
