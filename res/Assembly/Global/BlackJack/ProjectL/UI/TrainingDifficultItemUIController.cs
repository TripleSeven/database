﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TrainingDifficultItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TrainingDifficultItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    [AutoBind("./UnLockText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unLockText;
    [AutoBind("./NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_newImage;
    [AutoBind("./SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    private bool m_isLock;
    private bool m_isNew;
    public int DifficultLevel;
    private int m_unlockPlayerLevel;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitDifficultItem;
    private static DelegateBridge __Hotfix_OnItemClick;
    private static DelegateBridge __Hotfix_ShowSelectFrame;
    private static DelegateBridge __Hotfix_add_EventOnItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDifficultItem(int unlockPlayerLevel, int difficult)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TrainingDifficultItemUIController> EventOnItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
