﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailEquipmentUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailEquipmentUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Content/Equipment1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem;
    [AutoBind("./Content/Equipment2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem1;
    [AutoBind("./Content/Equipment3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem2;
    [AutoBind("./Content/Equipment4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem3;
    [AutoBind("./FastEquipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoEquipButton;
    [AutoBind("./FastEquipButton/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_autoEquipButtonNormalBg;
    [AutoBind("./FastRemoveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastRemoveButton;
    [AutoBind("./Desc", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_equipItemDescGo;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescTitleText;
    [AutoBind("./Desc/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescLvText;
    [AutoBind("./Desc/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescExpText;
    [AutoBind("./Desc/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconImage;
    [AutoBind("./Desc/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconBg;
    [AutoBind("./Desc/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSSREffect;
    [AutoBind("./Desc/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescProgressImage;
    [AutoBind("./Desc/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescStarGroup;
    [AutoBind("./Desc/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescEquipLimitContent;
    [AutoBind("./Desc/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropContent;
    [AutoBind("./Desc/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropATGo;
    [AutoBind("./Desc/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropATValueText;
    [AutoBind("./Desc/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDFGo;
    [AutoBind("./Desc/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDFValueText;
    [AutoBind("./Desc/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropHPGo;
    [AutoBind("./Desc/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropHPValueText;
    [AutoBind("./Desc/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagiccGo;
    [AutoBind("./Desc/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicValueText;
    [AutoBind("./Desc/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagicDFGo;
    [AutoBind("./Desc/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicDFValueText;
    [AutoBind("./Desc/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDexGo;
    [AutoBind("./Desc/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDexValueText;
    [AutoBind("./Desc/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./Desc/ValueInputField1", AutoBindAttribute.InitState.Inactive, false)]
    private InputField m_enchantmentValueInputField1;
    [AutoBind("./Desc/ValueInputField2", AutoBindAttribute.InitState.Inactive, false)]
    private InputField m_enchantmentValueInputField2;
    [AutoBind("./Desc/ValueInputField3", AutoBindAttribute.InitState.Inactive, false)]
    private InputField m_enchantmentValueInputField3;
    [AutoBind("./Desc/EnchantmentButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_enchantmentButton;
    private ConfigDataPropertyModifyInfo[] m_enchantmentValueTypes;
    private EquipmentBagItem m_slectedEquipment;
    [AutoBind("./Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillContent;
    [AutoBind("./Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipItemDescSkillContentStateCtrl;
    [AutoBind("./Desc/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillNameText;
    [AutoBind("./Desc/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillLvText;
    [AutoBind("./Desc/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescUnlockCoditionText;
    [AutoBind("./Desc/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillOwnerText;
    [AutoBind("./Desc/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillOwnerBGImage;
    [AutoBind("./Desc/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillDescText;
    [AutoBind("./Desc/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescNotEquipSkillTip;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipItemDescLockButton;
    [AutoBind("./Desc/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipItemshareButton;
    [AutoBind("./Desc/ForgeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipItemDescForgeButton;
    [AutoBind("./Desc/ExchangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipItemDescExchangeButton;
    [AutoBind("./ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_resonanceInfoPanelStateCtrl;
    [AutoBind("./ResonanceInfoPanel/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_resonanceInfoPanelBgButton;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/2SuitInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_resonanceInfoPanel2SuitStateCtrl;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/2SuitInfo/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel2SuitInfoText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/4SuitInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_resonanceInfoPanel4SuitStateCtrl;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/4SuitInfo/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel4SuitInfoText;
    [AutoBind("./ResonanceInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanelNameText;
    private int m_slot;
    private Hero m_hero;
    private ulong m_equipmentInstanceId;
    private List<GameObject> m_equipmentGos;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInEquipmentState;
    private static DelegateBridge __Hotfix_CreateEquipmentList;
    private static DelegateBridge __Hotfix_OnEquipmentIconClick;
    private static DelegateBridge __Hotfix_OnEquipmentAddButtonClick;
    private static DelegateBridge __Hotfix_HaveEquipmentInBag;
    private static DelegateBridge __Hotfix_OnRuneIconButtonClick;
    private static DelegateBridge __Hotfix_SetResonanceInfoPanel;
    private static DelegateBridge __Hotfix_CloseResonanceInfoPanel;
    private static DelegateBridge __Hotfix_SetEquipmentItemDesc;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_OnForgeButtonClick;
    private static DelegateBridge __Hotfix_OnEnchantmentButtonClick;
    private static DelegateBridge __Hotfix_OnExchangeButtonClick;
    private static DelegateBridge __Hotfix_OnAutoEquipButtonClick;
    private static DelegateBridge __Hotfix_OnFastRemoveButtonClick;
    private static DelegateBridge __Hotfix_DisableAutoEquipButton;
    private static DelegateBridge __Hotfix_CloseEquipemntItemDescGo;
    private static DelegateBridge __Hotfix_OnEquipemntLockButtonClick;
    private static DelegateBridge __Hotfix_OnEquipemntShareButtonClick;
    private static DelegateBridge __Hotfix_SetCommonUIState;
    private static DelegateBridge __Hotfix_add_EventOnAutoRemove;
    private static DelegateBridge __Hotfix_remove_EventOnAutoRemove;
    private static DelegateBridge __Hotfix_add_EventOnAutoEquip;
    private static DelegateBridge __Hotfix_remove_EventOnAutoEquip;
    private static DelegateBridge __Hotfix_add_EventOnGotoEquipmentForge;
    private static DelegateBridge __Hotfix_remove_EventOnGotoEquipmentForge;
    private static DelegateBridge __Hotfix_add_EventOnGotoEquipmentStore;
    private static DelegateBridge __Hotfix_remove_EventOnGotoEquipmentStore;
    private static DelegateBridge __Hotfix_add_EventOnLockButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnLockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShareButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnShareButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailEquipmentUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInEquipmentState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEquipmentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentIconClick(HeroEquipItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentAddButtonClick(HeroEquipItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HaveEquipmentInBag(HeroEquipItemUIController equipmentCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRuneIconButtonClick(HeroEquipItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetResonanceInfoPanel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseResonanceInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(HeroEquipItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnForgeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoEquipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastRemoveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DisableAutoEquipButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipemntItemDescGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipemntLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipemntShareButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnAutoRemove
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnAutoEquip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong> EventOnGotoEquipmentForge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong> EventOnGotoEquipmentStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, Action> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, string> EventOnShareButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
