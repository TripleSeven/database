﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceRecordButtonListerner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class VoiceRecordButtonListerner : MonoBehaviour
  {
    private Button TalkButton;
    private Coroutine m_coroutineTalkButtonTestHolding;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_OnExitTalkButton;
    private static DelegateBridge __Hotfix_OnEnterTalkButton;
    private static DelegateBridge __Hotfix_OnTalkButtonClickDown;
    private static DelegateBridge __Hotfix_OnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_OnTalkButtonTestHoding;
    private static DelegateBridge __Hotfix_SetTalkButtonSize;
    private static DelegateBridge __Hotfix_add_EventOnTalkButtonHold;
    private static DelegateBridge __Hotfix_remove_EventOnTalkButtonHold;
    private static DelegateBridge __Hotfix_add_EventOnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_remove_EventOnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_add_EventOnExitTalkButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnExitTalkButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnEnterTalkButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnEnterTalkButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitTalkButton(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterTalkButton(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickDown(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickUp(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnTalkButtonTestHoding()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTalkButtonSize(Vector2 size)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnTalkButtonHold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTalkButtonClickUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExitTalkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEnterTalkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
