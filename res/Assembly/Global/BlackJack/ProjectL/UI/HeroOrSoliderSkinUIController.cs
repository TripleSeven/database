﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroOrSoliderSkinUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroOrSoliderSkinUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Select", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./LimitTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGo;
    [AutoBind("./LimitTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeValueText;
    private UISpineGraphic m_playerHeroGraphic;
    private UISpineGraphic m_soldierInfoGraphic;
    private Hero m_hero;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitHeroSkinItem;
    private static DelegateBridge __Hotfix_InitDefaultHeroSkinItem;
    private static DelegateBridge __Hotfix_InitSoldierSkinItem;
    private static DelegateBridge __Hotfix_InitDefaultSoldierSkinItem;
    private static DelegateBridge __Hotfix_ShowSelectImage;
    private static DelegateBridge __Hotfix_OnHeroJobCardItemClick;
    private static DelegateBridge __Hotfix_SetLimitTime;
    private static DelegateBridge __Hotfix_add_EventOnItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnItemClick;
    private static DelegateBridge __Hotfix_set_JobConnectionInfo;
    private static DelegateBridge __Hotfix_get_JobConnectionInfo;
    private static DelegateBridge __Hotfix_set_HeroSkinInfo;
    private static DelegateBridge __Hotfix_get_HeroSkinInfo;
    private static DelegateBridge __Hotfix_set_ModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_get_ModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_get_SoldierInfo;
    private static DelegateBridge __Hotfix_set_SoldierInfo;
    private static DelegateBridge __Hotfix_get_SoldierSkinInfo;
    private static DelegateBridge __Hotfix_set_SoldierSkinInfo;
    private static DelegateBridge __Hotfix_set_IsHeroSkin;
    private static DelegateBridge __Hotfix_get_IsHeroSkin;
    private static DelegateBridge __Hotfix_set_IsDefaultSkin;
    private static DelegateBridge __Hotfix_get_IsDefaultSkin;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroSkinItem(
      Hero hero,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int skinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDefaultHeroSkinItem(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierSkinItem(
      Soldier2SkinResource item,
      ConfigDataSoldierSkinInfo soldierSkinInfo,
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDefaultSoldierSkinItem(Hero hero, ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobCardItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroOrSoliderSkinUIController> EventOnItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroSkinInfo HeroSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataModelSkinResourceInfo ModelSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierSkinInfo SoldierSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsHeroSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsDefaultSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
