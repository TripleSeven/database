﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BusinessCardHeroEquipmentItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BusinessCardHeroEquipmentItemController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentItemButton;
    [AutoBind("./BGFrame", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipmentItemBGImage;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentSSREffectGameObject;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipmentItemIcon;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentItemStarGroup;
    private BattleHeroEquipment m_equipment;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetEquipmentInfo;
    private static DelegateBridge __Hotfix_GetEquipment;
    private static DelegateBridge __Hotfix_OnItemClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEquipmentInfo(BattleHeroEquipment equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroEquipment GetEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BusinessCardHeroEquipmentItemController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
