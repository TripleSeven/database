﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedItemBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SelfSelectedItemBoxUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./LayoutRoot/Prefab/Item", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemTemplate;
    [AutoBind("./LayoutRoot/ListScrollView /Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_content;
    [AutoBind("./LayoutRoot/ItemDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemDescPanel;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private SelfSelectedItemBoxUITask m_selfSelectedItemBoxUITask;
    public SelfSelectedItemDetailUIController m_itemDetailUIController;
    private List<GoodsUIController> m_goodUIControllerList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitBuyMode;
    private static DelegateBridge __Hotfix_InitUseMode;
    private static DelegateBridge __Hotfix_SetSelfChooseItemBuyPlane;
    private static DelegateBridge __Hotfix_OnGoodsClick;
    private static DelegateBridge __Hotfix_PlayCloseAnimation;
    private static DelegateBridge __Hotfix_OnBGClick;
    private static DelegateBridge __Hotfix_add_EventOnCloseClick;
    private static DelegateBridge __Hotfix_remove_EventOnCloseClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedItemBoxUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBuyMode(
      SelfSelectedItemBoxUITask selfSelectedItemBoxUITask,
      StoreId storeId,
      int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitUseMode(
      SelfSelectedItemBoxUITask selfSelectedItemBoxUITask,
      int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelfChooseItemBuyPlane(int itemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoodsClick(GoodsUIController goodsUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayCloseAnimation(Action OnFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnCloseClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
