﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierManualItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SoldierManualItemUIController : UIControllerBase
  {
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./ClickImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_clickImage;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockObj;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphic;
    private UISpineGraphic m_graphic;
    public ConfigDataSoldierInfo SoldierInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSoldierItem;
    private static DelegateBridge __Hotfix_OnSoldierItemClick;
    private static DelegateBridge __Hotfix_SetClickImageActive;
    private static DelegateBridge __Hotfix_add_EventOnSoldierItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnSoldierItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierItem(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClickImageActive(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SoldierManualItemUIController> EventOnSoldierItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
