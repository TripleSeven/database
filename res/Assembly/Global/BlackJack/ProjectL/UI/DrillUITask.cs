﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class DrillUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public static string DrillMode;
    public static string ManualMode;
    public static string TrainingMode;
    public static string TutorMode;
    private static string m_curMode;
    private DrillUIController m_drillUIController;
    private DrillSoldierManualUIController m_drillSoldierManualUIController;
    private DrillTrainingUIController m_drillTrainingUIController;
    private DrillTutorUIController m_drillTutorUIController;
    private static string m_trainingModeFrom;
    private static int m_courseId;
    private int m_curLayerDescIndex;
    private List<ConfigDataSoldierInfo> m_totalSoldierInfoList;
    private List<ConfigDataSoldierInfo> m_curSoldierInfoList;
    private int m_totalSoldierManualPage;
    private int m_curSoldierManualPage;
    private int m_soldierFilterRank;
    private ArmyTag m_armyTag;
    private int m_nowSeconds;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_slot;
    private static int m_getSoldierTechId;
    private bool m_isNeedShowFadeIn;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_DrillUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_SetTotalSoldierInfoListWithFilter;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
    private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PushAndPopLayerByState;
    private static DelegateBridge __Hotfix_DrillUIController_OnReturn;
    private static DelegateBridge __Hotfix_DrillUIController_OnManualButtonClick;
    private static DelegateBridge __Hotfix_DrillUIController_OnTrainingButtonClick;
    private static DelegateBridge __Hotfix_DrillUIController_OnAssistanceTrainingButtonClick;
    private static DelegateBridge __Hotfix_DrillUIController_OnAssistanceStop;
    private static DelegateBridge __Hotfix_DrillUIController_OnAssistanceGetReward;
    private static DelegateBridge __Hotfix_DrillSoldierManualUIController_OnReturn;
    private static DelegateBridge __Hotfix_DrillUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_DrillSoldierManualUIController_OnNeedUpdateView;
    private static DelegateBridge __Hotfix_DrillSoldierManualUIController_OnGoToTraingTech;
    private static DelegateBridge __Hotfix_DrillTrainingUIController_OnReturn;
    private static DelegateBridge __Hotfix_DrillTrainingUIController_OnAddGold;
    private static DelegateBridge __Hotfix_DrillTrainingUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_DrillTrainingUIController_OnTechLevelup;
    private static DelegateBridge __Hotfix_DrillTrainingUIController_OnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_DrillTrainingUIController_OnGotoGetPath;
    private static DelegateBridge __Hotfix_DrillTutorUIController_OnReturn;
    private static DelegateBridge __Hotfix_DrillTutorUIController_OnShowTutorHelp;
    private static DelegateBridge __Hotfix_DrillTutorUIController_OnConfirmAssistant;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateTimeText;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(UIIntent uiIntent, int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DrillUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTotalSoldierInfoListWithFilter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushAndPopLayerByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnManualButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnTrainingButtonClick(int courseId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnAssistanceTrainingButtonClick(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnAssistanceStop(int taskId, int slot, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnAssistanceGetReward(int taskId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillSoldierManualUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillSoldierManualUIController_OnNeedUpdateView(
      int curPage,
      int rank,
      ArmyTag armyTag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillSoldierManualUIController_OnGoToTraingTech(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnTechLevelup(int techId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnEvolutionMaterialClick(
      GoodsType goodsType,
      int goodsId,
      int needCount,
      int courseId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTrainingUIController_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTutorUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTutorUIController_OnShowTutorHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillTutorUIController_OnConfirmAssistant(
      int taskId,
      List<int> heroId,
      int workSeconds,
      int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTimeText()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static DrillUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
