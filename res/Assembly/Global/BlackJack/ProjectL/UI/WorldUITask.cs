﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class WorldUITask : UITask, IClientWorldListener
  {
    private static bool s_isShowWorldUI = true;
    private static bool s_showMainButtonBar = true;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private static WorldUITask s_instance;
    private WorldUIController m_worldUIController;
    private WorldMapUIController m_worldMapUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private RiftUITask m_riftUITask;
    private UnchartedUITask m_unchartedUITask;
    private ArenaSelectUITask m_arenaSelectUITask;
    private ClientWorld m_clientWorld;
    private IConfigDataLoader m_configDataLoader;
    private int m_slotTick;
    private int m_nowSeconds;
    private bool m_isResuming;
    private bool m_needUpdateEventActors;
    private ResumeNeedNextWorldStepType m_resumeNeedNextWorldStep;
    private List<int> m_movePath;
    private int m_noticeCenterButtonRedMarkCheckInterval;
    private bool m_noticeCenterEnable;
    private bool m_isNeedShowWebActivityRedMark;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectPlayerAssets;
    private static DelegateBridge __Hotfix_CollectEventAssets;
    private static DelegateBridge __Hotfix_CollectMonthCardAssets;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_HideAllView;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_InitWorldMapUIController;
    private static DelegateBridge __Hotfix_UninitWorldMapUIController;
    private static DelegateBridge __Hotfix_InitWorldUIController;
    private static DelegateBridge __Hotfix_UninitWorldUIController;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateRedMarks;
    private static DelegateBridge __Hotfix_UpdatePlayerInfo;
    private static DelegateBridge __Hotfix_UpdateActiveScenario;
    private static DelegateBridge __Hotfix_UpdateHeroRedMark;
    private static DelegateBridge __Hotfix_UpdateMissionRedMark;
    private static DelegateBridge __Hotfix_UpdateActivityRedMark;
    private static DelegateBridge __Hotfix_UpdateActivityRedMarkForAllWebActivity;
    private static DelegateBridge __Hotfix_UpdateFriendRedMark;
    private static DelegateBridge __Hotfix_UpdateDrillRedMark;
    private static DelegateBridge __Hotfix_UpdateFetterRedMark;
    private static DelegateBridge __Hotfix_UpdateGuildRedMark;
    private static DelegateBridge __Hotfix_UpdateWorldEventRedMark;
    private static DelegateBridge __Hotfix_UpdateInvestigationButton;
    private static DelegateBridge __Hotfix_UpdateOpenServiceActivityButton;
    private static DelegateBridge __Hotfix_UpdateBackFlowActivityButton;
    private static DelegateBridge __Hotfix_UpdateCooperateBattleButton;
    private static DelegateBridge __Hotfix_UpdateMonthCardInfo;
    private static DelegateBridge __Hotfix_UpdatePeakArenaButton;
    private static DelegateBridge __Hotfix_LoadAndUpdateEventActors;
    private static DelegateBridge __Hotfix_OnSlowTick;
    private static DelegateBridge __Hotfix_ShowEventList;
    private static DelegateBridge __Hotfix_HasRandomEvent;
    private static DelegateBridge __Hotfix_UpdateNewChatCount;
    private static DelegateBridge __Hotfix_UpdateNewMailCount;
    private static DelegateBridge __Hotfix_CreateClientWorld;
    private static DelegateBridge __Hotfix_DestroyClientWorld;
    private static DelegateBridge __Hotfix_StartClientWorld;
    private static DelegateBridge __Hotfix_NextWorldStep;
    private static DelegateBridge __Hotfix_WorldUIGetReady;
    private static DelegateBridge __Hotfix_ClickFirstWaypoint;
    private static DelegateBridge __Hotfix_StartSetProtagonistNetTask;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowMainButtonBar;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowPlayerInfo;
    private static DelegateBridge __Hotfix_WorldUIController_OnCompass;
    private static DelegateBridge __Hotfix_WorldUIController_OnCurrentScenario;
    private static DelegateBridge __Hotfix_WorldUIController_OnUnlockScenarioGotoRiftLevel;
    private static DelegateBridge __Hotfix_WorldUIController_OnGotoEvent;
    private static DelegateBridge __Hotfix_WorldUIController_OnStartPastScenario;
    private static DelegateBridge __Hotfix_WorldUIController_OnClosePastScenarioList;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowEvent;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowHero;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowBag;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowMission;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowFetter;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowStore;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowSelectCard;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowDrill;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowFriend;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowGuild;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowUncharted;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowArena;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowRift;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowTest;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowTest2;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowCooperateBattle;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowRecommendGiftBox;
    private static DelegateBridge __Hotfix_RecommendGiftBoxUITask_Close;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowEventGiftBox;
    private static DelegateBridge __Hotfix_EventGiftBoxUITask_Close;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowMail;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowChat;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowRanking;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowActivity;
    private static DelegateBridge __Hotfix_OnNeedUpdateWebInfoRedMard;
    private static DelegateBridge __Hotfix_OnNoticeCenterStateSuccess;
    private static DelegateBridge __Hotfix_OnNoticeCenterStateFailed;
    private static DelegateBridge __Hotfix_UpdateNoticeCenterButton;
    private static DelegateBridge __Hotfix_UpdateNoticeCenterRedMarkState;
    private static DelegateBridge __Hotfix_WorldUIController_OnNoticeCenterButtonClick;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowOpenServiceActivity;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowBackFlowActivity;
    private static DelegateBridge __Hotfix_WorldUIController_OnPeakArenaButtonClick;
    private static DelegateBridge __Hotfix_WorldUIController_OnMonthCardButtonClick;
    private static DelegateBridge __Hotfix_WorldUIController_OnRefreshMonthCardPanel;
    private static DelegateBridge __Hotfix_WorldUIController_OnMonthCardItemClick;
    private static DelegateBridge __Hotfix_WorldUIController_OnMonthCardItemBuyClick;
    private static DelegateBridge __Hotfix_WorldUIController_OnOpenWebInvestigation;
    private static DelegateBridge __Hotfix_WorldUIController_OnYYBButtonClick;
    private static DelegateBridge __Hotfix_WorldUIController_OnOppoButtonClick;
    private static DelegateBridge __Hotfix_PDSDK_OnDoQuestionSucceed;
    private static DelegateBridge __Hotfix_PDSDK_OnDoQuestionFailed;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowSign;
    private static DelegateBridge __Hotfix_WorldUIController_OnShowActivityNotice;
    private static DelegateBridge __Hotfix_WorldUIController_ShowHide;
    private static DelegateBridge __Hotfix_ShowWorld;
    private static DelegateBridge __Hotfix_WorldMapUIController_OnPointerDown;
    private static DelegateBridge __Hotfix_WorldMapUIController_OnPointerUp;
    private static DelegateBridge __Hotfix_WorldMapUIController_OnPointerClick;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddGold;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_PlayerContext_OnMailsChangedNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnFriendInviteNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnFriendInviteAcceptNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnFriendshipPointsReceivedNtf;
    private static DelegateBridge __Hotfix_PlayerContext_EventOnGiftStoreBuyGoodsNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnChatMessageNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnMailReadAck;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoChangeNtf;
    private static DelegateBridge __Hotfix_LogBattleRandomSeed;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomInviteNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattlePracticeInvitedNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomTeamBattleJoinNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPVPBattleJoinNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomRealTimePVPBattleJoinNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomGuildMassiveCombatBattleJoinNtf;
    private static DelegateBridge __Hotfix_PlayerContext_EventOnRandomEventUpdate;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_GetClientWorld;
    private static DelegateBridge __Hotfix_StartScenario;
    private static DelegateBridge __Hotfix_StartWaypointEvent;
    private static DelegateBridge __Hotfix_StartBattleRoomBattle;
    private static DelegateBridge __Hotfix_StartBattleLiveRoomBattle;
    private static DelegateBridge __Hotfix__StartBattleLiveRoomBattle;
    private static DelegateBridge __Hotfix_StartBattleHappening;
    private static DelegateBridge __Hotfix__StartBattleHappening;
    private static DelegateBridge __Hotfix_StartHappening;
    private static DelegateBridge __Hotfix_NextHappeningStep;
    private static DelegateBridge __Hotfix_HandleAttackFailResult;
    private static DelegateBridge __Hotfix_StartLevelWayPointMoveNetTask;
    private static DelegateBridge __Hotfix_StartLevelScenarioHandleNetTask;
    private static DelegateBridge __Hotfix_OnLevelWayPointMoveNetTaskResult;
    private static DelegateBridge __Hotfix_ShowWaypointReward;
    private static DelegateBridge __Hotfix_GetRewardGoodsUITask_OnClose;
    private static DelegateBridge __Hotfix_ChestUITask_OnClose;
    private static DelegateBridge __Hotfix_CheckChangeActiveScenario;
    private static DelegateBridge __Hotfix_Co_ChangeActiveScenario;
    private static DelegateBridge __Hotfix_Co_ChangeActiveScenario35;
    private static DelegateBridge __Hotfix_Co_PlayWaypointEffect;
    private static DelegateBridge __Hotfix_CheckOpenRiftChapter;
    private static DelegateBridge __Hotfix_CheckOrderReward;
    private static DelegateBridge __Hotfix_CheckReturnToBeforeBattleUI;
    private static DelegateBridge __Hotfix_CheckTeamRoomInviteAgain;
    private static DelegateBridge __Hotfix_CheckOpenTeamRoomInfoUI;
    private static DelegateBridge __Hotfix_StartTeamRoomInfoUITask;
    private static DelegateBridge __Hotfix_CheckInBattleRoom;
    private static DelegateBridge __Hotfix_CheckInPeakArenaMatch;
    private static DelegateBridge __Hotfix_ShowPeakArenaMatch;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnPeakArenaContinue;
    private static DelegateBridge __Hotfix_ShowMatchingNowUITask;
    private static DelegateBridge __Hotfix_CheckInPeakArenaKnockOutUI;
    private static DelegateBridge __Hotfix_MoveToPrevWaypoint;
    private static DelegateBridge __Hotfix_StartBattleArmyRandomSeedGetNetTask;
    private static DelegateBridge __Hotfix_StartDanmakuGetNetTaskNetTask;
    private static DelegateBridge __Hotfix_OnWaypointClick;
    private static DelegateBridge __Hotfix_PlayerArriveWaypoint;
    private static DelegateBridge __Hotfix_ShowPastScenarioList;
    private static DelegateBridge __Hotfix_OnScreenEffect;
    private static DelegateBridge __Hotfix_StartShowFadeOutLoadingFadeIn;
    private static DelegateBridge __Hotfix_HideFadeOutLoadingFadeIn;
    private static DelegateBridge __Hotfix_StartPlayerInfoUITask;
    private static DelegateBridge __Hotfix_PlayerInfoUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_PlayerInfoUITask_OnClose;
    private static DelegateBridge __Hotfix_StartAppScoreUITask;
    private static DelegateBridge __Hotfix_AppScoreUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_AppScoreUITask_OnClose;
    private static DelegateBridge __Hotfix_StartSignUITask;
    private static DelegateBridge __Hotfix_SignUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartActivityNoticeUITask;
    private static DelegateBridge __Hotfix_ActivityNoticeUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartHeroUITask;
    private static DelegateBridge __Hotfix_HeroUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartGuildStoreUITask;
    private static DelegateBridge __Hotfix_GuildStoreUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartBagUITask;
    private static DelegateBridge __Hotfix_BagUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartMissionUITask;
    private static DelegateBridge __Hotfix_MissionUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartPlayerInfoUITaskByPath;
    private static DelegateBridge __Hotfix_PlayerInfoUITask_OnLoadAllResCompletedByPath;
    private static DelegateBridge __Hotfix_StartOpenServiceActivityUITask;
    private static DelegateBridge __Hotfix_OpenServiceActivityUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartBackFlowActivityUITask;
    private static DelegateBridge __Hotfix_BackFlowActivityUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartFettersUITask;
    private static DelegateBridge __Hotfix_FettersUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_FetterUITask_StartHeroDungeon;
    private static DelegateBridge __Hotfix_StartArenaSelectUITask;
    private static DelegateBridge __Hotfix_ArenaSelectUITask_OnStop;
    private static DelegateBridge __Hotfix_ArenaSelectUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_ArenaSelectUITask_OnShowArena;
    private static DelegateBridge __Hotfix_ArenaSelectUITask_OnShowPeakArena;
    private static DelegateBridge __Hotfix_StartArenaUITask;
    private static DelegateBridge __Hotfix_ArenaUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAndStartArenaUITask;
    private static DelegateBridge __Hotfix_SendPeakArenaSeasonSimpleInfoNetTask;
    private static DelegateBridge __Hotfix_SendGetSeasonInfoNetTask;
    private static DelegateBridge __Hotfix_StartPeakArenaUITask;
    private static DelegateBridge __Hotfix_PeakArenaUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartArenaBattleReportBasicDataGetNetTask;
    private static DelegateBridge __Hotfix_StartStoreUITask;
    private static DelegateBridge __Hotfix_StoreUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartSelectCardUITask;
    private static DelegateBridge __Hotfix_SelectCardUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartDrillUITask;
    private static DelegateBridge __Hotfix_DrillUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartFriendUITask;
    private static DelegateBridge __Hotfix_StartGuildUITask;
    private static DelegateBridge __Hotfix_StartGuildSubUITask;
    private static DelegateBridge __Hotfix_GuildUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_GuildManagementUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_FriendUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartMailUITask;
    private static DelegateBridge __Hotfix_MailUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartRankingUITask;
    private static DelegateBridge __Hotfix_StartActivityUITask;
    private static DelegateBridge __Hotfix_ActivityUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartTeamUITask;
    private static DelegateBridge __Hotfix_TeamUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartTestUITask;
    private static DelegateBridge __Hotfix_TestUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_TestUITask_OnPauseOrStop;
    private static DelegateBridge __Hotfix_TestUITask_OnStartBattle;
    private static DelegateBridge __Hotfix_StartUnchartedUITask;
    private static DelegateBridge __Hotfix_UnchartedUITask_OnStop;
    private static DelegateBridge __Hotfix_UnchartedUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartUnchartedSubUITask;
    private static DelegateBridge __Hotfix__StartUnchartedSubUITask;
    private static DelegateBridge __Hotfix_StartRiftUITask;
    private static DelegateBridge __Hotfix_RiftUITask_OnStop;
    private static DelegateBridge __Hotfix_RiftUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_RiftUITask_GoToScenario;
    private static DelegateBridge __Hotfix_StartHeroDungeonUITask;
    private static DelegateBridge __Hotfix_HeroDungeonUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartHeroPhantomUITask;
    private static DelegateBridge __Hotfix_HeroPhantomUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartThearchyUITask;
    private static DelegateBridge __Hotfix_ThearchyUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartAnikiUITask;
    private static DelegateBridge __Hotfix_AnikiUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartTreasureMapUITask;
    private static DelegateBridge __Hotfix_TreasureMapUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartMemoryCorridorUITask;
    private static DelegateBridge __Hotfix_MemoryCorridorUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartHeroTrainningUITask;
    private static DelegateBridge __Hotfix_HeroTrainningUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartCooperateBattleUITask;
    private static DelegateBridge __Hotfix_CooperateBattleUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartUnchartedScoreUITask;
    private static DelegateBridge __Hotfix_UnchartedScoreUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartClimbTowerUITask;
    private static DelegateBridge __Hotfix_ClimbTowerUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartEternalShrineUITask;
    private static DelegateBridge __Hotfix_EternalShrineUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartHeroAnthemUITask;
    private static DelegateBridge __Hotfix_HeroAnthemUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartCollectionActivityUITask;
    private static DelegateBridge __Hotfix_CollectionActivityUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartWorldEventMissionUITask;
    private static DelegateBridge __Hotfix_WorldEventMissionUITask_OnClose;
    private static DelegateBridge __Hotfix_RemoveWorldEventMissionUITaskEvents;
    private static DelegateBridge __Hotfix_WorldEventMissionUITask_OnCheckEnterMission;
    private static DelegateBridge __Hotfix_WorldEventMissionUITask_OnEnterMission;
    private static DelegateBridge __Hotfix_StartGoddessDialogUITask;
    private static DelegateBridge __Hotfix_GoddessDialogUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_GoddessDialogUITask_OnClose;
    private static DelegateBridge __Hotfix_StartDialogUITask;
    private static DelegateBridge __Hotfix_DialogUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_DialogUITask_OnClose;
    private static DelegateBridge __Hotfix_StartBattleUITask;
    private static DelegateBridge __Hotfix_HideInviteAndChatUITask;
    private static DelegateBridge __Hotfix_UnloadAssetsAndStartBattleUITask;
    private static DelegateBridge __Hotfix_Co_UnloadAssetsAndStartBattleUITask;
    private static DelegateBridge __Hotfix_BattleUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartArenaBattleUITask;
    private static DelegateBridge __Hotfix__StartArenaBattleUITask;
    private static DelegateBridge __Hotfix_IsBattleReportVersionValid;
    private static DelegateBridge __Hotfix_StartArenaBattleReport;
    private static DelegateBridge __Hotfix_StartArenaReplayBattleUITask;
    private static DelegateBridge __Hotfix_StartRealTimePVPBattleReport;
    private static DelegateBridge __Hotfix_StartRealTimePVPReplayBattleUITask;
    private static DelegateBridge __Hotfix_StartPeakArenaBattleReport;
    private static DelegateBridge __Hotfix__StartPeakArenaBattleReport;
    private static DelegateBridge __Hotfix_StartPeakArenaReplayBattleUITask;
    private static DelegateBridge __Hotfix_FadeOutAndStartBattleUITask;
    private static DelegateBridge __Hotfix_CanGotoGetPath;
    private static DelegateBridge __Hotfix_StartGetPathTargetUITask;
    private static DelegateBridge __Hotfix__StartGetPathTargetUITask;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~WorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPlayerAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEventAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectMonthCardAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWorldMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitWorldMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWorldUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitWorldUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRedMarks()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActivityRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateActivityRedMarkForAllWebActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFriendRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDrillRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFetterRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGuildRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWorldEventRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateInvestigationButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOpenServiceActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBackFlowActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCooperateBattleButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMonthCardInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadAndUpdateEventActors(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSlowTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEventList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNewChatCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNewMailCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextWorldStep(bool checkWorldUIGetReady = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIGetReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClickFirstWaypoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSetProtagonistNetTask(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowMainButtonBar(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnCompass()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnCurrentScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnUnlockScenarioGotoRiftLevel(
      ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnGotoEvent(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnStartPastScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnClosePastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowBag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowFetter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowSelectCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowDrill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowUncharted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowRift()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowTest()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowTest2()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowCooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowRecommendGiftBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RecommendGiftBoxUITask_Close(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowEventGiftBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EventGiftBoxUITask_Close(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowRanking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNeedUpdateWebInfoRedMard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeCenterStateSuccess(int status, int interval, string noticeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeCenterStateFailed(string noticeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNoticeCenterButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateNoticeCenterRedMarkState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnNoticeCenterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowOpenServiceActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnShowBackFlowActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnPeakArenaButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnMonthCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnRefreshMonthCardPanel(bool curIsOpenState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnMonthCardItemClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnMonthCardItemBuyClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnOpenWebInvestigation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnYYBButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUIController_OnOppoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnDoQuestionSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnDoQuestionFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WorldUIController_OnShowSign()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WorldUIController_OnShowActivityNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void WorldUIController_ShowHide(bool isShow, bool saveShowState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldMapUIController_OnPointerDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldMapUIController_OnPointerUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldMapUIController_OnPointerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnMailsChangedNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnFriendInviteNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnFriendInviteAcceptNtf(UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnFriendshipPointsReceivedNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGiftStoreBuyGoodsNtf(string OrderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatMessageNtf(ChatMessage msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnMailReadAck(int obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LogBattleRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInviteNtf(TeamRoomInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattlePracticeInvitedNtf(PVPInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomTeamBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomRealTimePVPBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomGuildMassiveCombatBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnRandomEventUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorld GetClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartScenario(
      ConfigDataScenarioInfo scenarioInfo,
      ConfigDataWaypointInfo prevWaypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWaypointEvent(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataWaypointInfo prevWaypointInfo,
      ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartBattleLiveRoomBattle(int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartBattleLiveRoomBattle(int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartBattleHappening(
      BattleType battleType,
      int locationId,
      ulong instanceId = 0,
      Action onStepEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool _StartBattleHappening(
      BattleType battleType,
      int levelId,
      ulong instanceId = 0,
      Action onStepEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHappening(
      BattleType battleType,
      ConfigDataBattleInfo battleInfo,
      int monsterLevel,
      ConfigDataDialogInfo dialogBefore = null,
      ConfigDataDialogInfo dialogAfter = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextHappeningStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HandleAttackFailResult(int result, UIIntent currIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelWayPointMoveNetTask(int waypointId, Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelScenarioHandleNetTask(int scenarioId, Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelWayPointMoveNetTaskResult(
      int result,
      BattleReward reward,
      bool isScenario,
      Action<int> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowWaypointReward(BattleReward reward, bool isChest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChestUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckChangeActiveScenario(bool checkWorldUIGetReady = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeActiveScenario(
      ConfigDataScenarioInfo scenarioInfo,
      bool checkWorldUIGetReady)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeActiveScenario35()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayWaypointEffect(ClientWorldWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenRiftChapter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckOrderReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckReturnToBeforeBattleUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckTeamRoomInviteAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenTeamRoomInfoUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckInBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckInPeakArenaMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPeakArenaMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPeakArenaContinue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMatchingNowUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckInPeakArenaKnockOutUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveToPrevWaypoint(bool moveCamera = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartBattleArmyRandomSeedGetNetTask(int battleId, Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartDanmakuGetNetTaskNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerArriveWaypoint(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataWaypointInfo prevWaypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ShowPastScenarioList(ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartShowFadeOutLoadingFadeIn(Action fadeOutEnd, FadeStyle style = FadeStyle.Black)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideFadeOutLoadingFadeIn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPlayerInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAppScoreUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppScoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppScoreUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSignUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SignUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartActivityNoticeUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityNoticeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildStoreUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildStoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBagUITask(BagListUIController.DisplayType displayType = BagListUIController.DisplayType.None, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BagUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMissionUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MissionUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPlayerInfoUITaskByPath(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerInfoUITask_OnLoadAllResCompletedByPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartOpenServiceActivityUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenServiceActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBackFlowActivityUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BackFlowActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartFettersUITask(
      ConfigDataHeroDungeonLevelInfo heroDungeonLevelInfo = null,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FettersUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FetterUITask_StartHeroDungeon(Hero hero, UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaSelectUITask(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnShowArena(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaSelectUITask_OnShowPeakArena(
      PeakArenaPanelType peakArenaPanelType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaUITask(ArenaUIType arenaUIType, int panelType, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitAndStartArenaUITask(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent = null,
      int matchGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendPeakArenaSeasonSimpleInfoNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGetSeasonInfoNetTask(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaUITask(
      UIIntent fromIntent = null,
      PeakArenaPanelType tabType = PeakArenaPanelType.Clash,
      int matchGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleReportBasicDataGetNetTask(
      ArenaUIType arenaUIType,
      int panelType,
      UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartStoreUITask(StoreId storeId = StoreId.StoreId_None, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartSelectCardUITask(int cardPoolId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectCardUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDrillUITask(int drillState = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrillUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartFriendUITask(UIIntent fromIntent = null, bool openAddFriend = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildUITask(ulong guildMassiveCombatInstanceId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildSubUITask(GetPathType UIName, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FriendUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMailUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MailUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRankingUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartActivityUITask(int activityId = 0, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamUITask(
      GameFunctionType gameFunctionType = GameFunctionType.GameFunctionType_None,
      int chapterId = 0,
      int locationId = 0,
      int collectionActivityId = 0,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTestUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUITask_OnPauseOrStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUITask_OnStartBattle(ConfigDataBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedUITask(
      BattleType battleType = BattleType.None,
      int chapterId = 0,
      int tabIndex = 0,
      bool isAfterBattle = false,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUnchartedSubUITask(
      BattleType battleType,
      int chapterId,
      bool isAfterBattle,
      UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartUnchartedSubUITask(
      BattleType battleType,
      int chapterId,
      bool isAfterBattle,
      UIIntent fromIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftUITask(
      ConfigDataRiftLevelInfo levelInfo,
      bool openLevelInfo,
      bool needReturnToChapter = true,
      UIIntent fromIntent = null,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftUITask_GoToScenario(int scenarioID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonUITask(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      bool openLevelInfo,
      Hero hero = null,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDungeonUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroPhantomUITask(
      ConfigDataHeroPhantomInfo heroPhantomInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroPhantomUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartThearchyUITask(
      ConfigDataThearchyTrialInfo thearchyTrialInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ThearchyUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAnikiUITask(ConfigDataAnikiGymInfo anikiGymInfo, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AnikiUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTreasureMapUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TreasureMapUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMemoryCorridorUITask(
      ConfigDataMemoryCorridorInfo memoryCorridorInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MemoryCorridorUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroTrainningUITask(
      ConfigDataHeroTrainningInfo heroTrainningInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroTrainningUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCooperateBattleUITask(
      ConfigDataCooperateBattleInfo cooperateBattleInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CooperateBattleUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedScoreUITask(
      ConfigDataUnchartedScoreInfo unchartedScoreInfo,
      BattleType battleType,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedScoreUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerUITask(UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClimbTowerUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartEternalShrineUITask(
      ConfigDataEternalShrineInfo eternalShrineInfo,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroAnthemUITask(ConfigDataHeroAnthemInfo heroAnthemInfo, UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionActivityUITask(
      ConfigDataCollectionActivityInfo collectionActivityInfo,
      bool isAfterBattle = false,
      UIIntent fromIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectionActivityUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWorldEventMissionUITask(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveWorldEventMissionUITaskEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool WorldEventMissionUITask_OnCheckEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventMissionUITask_OnEnterMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGoddessDialogUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GoddessDialogUITask_OnClose(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDialogUITask(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUITask_OnClose(bool isSkip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleUITask(ConfigDataBattleInfo battleInfo, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void HideInviteAndChatUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void UnloadAssetsAndStartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void BattleUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartArenaBattleUITask(bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartArenaBattleUITask(bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsBattleReportVersionValid(int battleReportVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartArenaBattleReport(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaReplayBattleUITask(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartRealTimePVPBattleReport(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPReplayBattleUITask(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaBattleReport(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType,
      int matchGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void _StartPeakArenaBattleReport(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType,
      int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaReplayBattleUITask(
      PeakArenaLadderBattleReport battleReport,
      string userId,
      PeakArenaBattleReportSourceType sourceType,
      int matchGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeOutAndStartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CanGotoGetPath(GetPathData getPathInfo, ref FadeStyle fadeStyle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartGetPathTargetUITask(
      GetPathData getPath,
      UIIntent fromIntent,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _StartGetPathTargetUITask(
      GetPathData getPath,
      UIIntent fromIntent,
      NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
