﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatExpressionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ChatExpressionUIController : UIControllerBase
  {
    public BigExpressionInChatController m_bigExpressionInChatController;
    private float m_emptySpaceNormalRatio;
    private int m_currSmallPageIndex;
    private List<CommonUIStateController> m_smallExpressionPagePointList;
    private List<KeyValuePair<float, float>> m_smallExpressionPageSubSectionList;
    private List<SmallExpressionItemContrller> m_smallExpressionCtrlList;
    [AutoBind("./Detail/ExpressionToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle SmallExpressionToggle;
    [AutoBind("./ExpressionBGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ExpressionBgButton;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PanelStateCtrl;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionRoot;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionContent;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView/Viewport/Content/OnePage/ExpressionItem", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionItem;
    [AutoBind("./Detail/ExpressionGroup/ExpressionScrollView/Viewport/Content/OnePage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SmallExpressionPage;
    [AutoBind("./Detail/ExpressionGroup/PagePoint/PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ExpressionPagePoint;
    [AutoBind("./Detail/ExpressionGroup/PagePoint", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ExpressionPagePointRoot;
    [AutoBind("./Detail/BigExressionGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject BigExpressionGroupDummy;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ExpressionDetailRoot;
    private const int SmallExpressionCount4OnePage = 24;
    private const int ExpressionPageEmptySpaceWidth = 100;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_ShowOrHideExpressionPanel;
    private static DelegateBridge __Hotfix_SwitchExpressionPanel;
    private static DelegateBridge __Hotfix_DelayInstancePrefab;
    private static DelegateBridge __Hotfix_OnPointerUp4ExpressionScrollRect;
    private static DelegateBridge __Hotfix_OnSmallExpressionClick;
    private static DelegateBridge __Hotfix_OnExpressionBgButtonClick;
    private static DelegateBridge __Hotfix_OnSmallExpressionToggleValueChanged;
    private static DelegateBridge __Hotfix_OnBigExpressionClick;
    private static DelegateBridge __Hotfix_InitExpressionPageAndItem;
    private static DelegateBridge __Hotfix_SetSmallExpressionPageImmediate;
    private static DelegateBridge __Hotfix_SetSmallExpressionPage;
    private static DelegateBridge __Hotfix_add_EventOnSmallExpressionClick;
    private static DelegateBridge __Hotfix_remove_EventOnSmallExpressionClick;
    private static DelegateBridge __Hotfix_add_EventOnExpressionBgButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnExpressionBgButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatExpressionUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHideExpressionPanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchExpressionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator DelayInstancePrefab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPointerUp4ExpressionScrollRect(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSmallExpressionClick(SmallExpressionItemContrller uCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionBgButtonClick(UIControllerBase uCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSmallExpressionToggleValueChanged(bool isSelected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator InitExpressionPageAndItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSmallExpressionPageImmediate(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SetSmallExpressionPage(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<SmallExpressionItemContrller> EventOnSmallExpressionClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExpressionBgButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
