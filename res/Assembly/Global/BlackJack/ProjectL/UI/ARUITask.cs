﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class ARUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_iosLayerDescArray;
    private UITaskBase.LayerDesc[] m_hwLayerDescArray;
    private static readonly UITaskBase.LayerDesc m_uiLayerDesc;
    private static readonly UITaskBase.LayerDesc m_iosSceneLayerDesc;
    private static readonly UITaskBase.LayerDesc m_hwSceneLayerDesc;
    private UITaskBase.UIControllerDesc[] m_iosCtrlDescArray;
    private UITaskBase.UIControllerDesc[] m_hwCtrlDescArray;
    private static readonly UITaskBase.UIControllerDesc m_uiCtrlDesc;
    private static readonly UITaskBase.UIControllerDesc m_iosSceneCtrlDesc;
    private static readonly UITaskBase.UIControllerDesc m_hwSceneCtrlDesc;
    private ARShowUIController m_arShowUIController;
    private ARShowSceneController m_arShowSceneController;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_CustomLoadAsset;
    private static DelegateBridge __Hotfix_CollectAssetWrap;
    private static DelegateBridge __Hotfix_ReturnLastTask;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public ARUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CustomLoadAsset(Action collectionCallback, Action finishCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectAssetWrap(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReturnLastTask()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static ARUITask()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
