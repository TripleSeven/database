﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARHeroCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ARHeroCardUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroItemButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroItemUIStateController;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroTypeImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroTypeImg;
    [AutoBind("./HeroLvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    [AutoBind("./SSRFrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSSRFrameEffect;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroFrameImg;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./ARSelectFrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSelectFrame;
    [AutoBind("./ARSelectFrontImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSelectFront;
    public ARHeroListUIController.HeroWrap m_heroWrap;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroListItemInfo;
    private static DelegateBridge __Hotfix_RefreshHeroSelect;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_add_EventOnSelectHeroItem;
    private static DelegateBridge __Hotfix_remove_EventOnSelectHeroItem;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListItemInfo(ARHeroListUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshHeroSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ARHeroCardUIController> EventOnSelectHeroItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
