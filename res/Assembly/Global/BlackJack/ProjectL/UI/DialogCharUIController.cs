﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogCharUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DialogCharUIController : UIControllerBase
  {
    private CommonUIStateController m_uiStateController;
    private Image m_image;
    private ConfigDataCharImageInfo m_charImageInfo;
    private UISpineGraphic m_spineGraphic;
    private Color m_curColor;
    private Color m_tweenColor;
    private float m_tweenColorTime;
    private string m_idleAnimationName;
    private string m_idleFacialAnimationName;
    private float m_voiceTime;
    private float m_blinkTime;
    private bool m_isPlayingPreAnimation;
    private bool m_isPlayingEyeAnimation;
    private bool m_isPlayingMouthAnimation;
    private const int EYE_ANIMATION_TRACK = 1;
    private const int MOUTH_ANIMATION_TRACK = 2;
    private const string EYE_ANIMATION = "_eye";
    private const string EYE_STILL_ANIMATION = "_eye_still";
    private const string MOUTH_ANIMATION = "_mouth";
    private const string MOUTH_STILL_ANIMATION = "_mouth_still";
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_CreateGraphic;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_SetScaleOffet;
    private static DelegateBridge __Hotfix_SetDirection;
    private static DelegateBridge __Hotfix_SetColor;
    private static DelegateBridge __Hotfix_TweenColor;
    private static DelegateBridge __Hotfix_GetCharImageInfo;
    private static DelegateBridge __Hotfix_StartFacialAnimation;
    private static DelegateBridge __Hotfix_Enter;
    private static DelegateBridge __Hotfix_Leave;
    private static DelegateBridge __Hotfix_Co_TweenColor;
    private static DelegateBridge __Hotfix_SetAnimation;
    private static DelegateBridge __Hotfix_StopMouthAnimation;
    private static DelegateBridge __Hotfix_SetNextBlinkTime;
    private static DelegateBridge __Hotfix_GetAnimationDuration;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_StopAnimation;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_PlayEyeAnimation;
    private static DelegateBridge __Hotfix_PlayEyeStillAnimation;
    private static DelegateBridge __Hotfix_PlayMouthAnimation;
    private static DelegateBridge __Hotfix_PlayMouthStillAnimation;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScaleOffet(float scale, float yOffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetColor(Color c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TweenColor(Color c, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFacialAnimation(float voiceTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Enter(int enterType, Action onEndAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Leave(int leaveType, Action onEndAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_TweenColor(
      Color fromColor,
      Color toColor,
      float time,
      Action onEndAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimation(
      string preAnimation,
      string preFacialAnimation,
      string idleAnimation,
      string idleFacialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopMouthAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNextBlinkTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayEyeAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayEyeStillAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayMouthAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayMouthStillAnimation(string facialAnimation)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
