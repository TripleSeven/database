﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class StoreUITask : UITask
  {
    private static List<PDSDKGood> m_pdsdkGoods = new List<PDSDKGood>();
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private StoreListUIController m_storeListUIController;
    private int m_nowSecond;
    private ProjectLPlayerContext m_playerContext;
    private ClientConfigDataLoader m_configdataLoader;
    private string m_curBuyingGoodsId;
    public const string ParamsKey_StoreInfoId = "StoreInfoID";
    public const string ParamsKey_StoreItemId = "StoreItemId";
    public const string ParamsKey_IsShowExtractionPanel = "IsShowExtractionPanel";
    private StoreId m_storeId;
    public static GetRewardGoodsUITask m_orderGetRewardGoodsUITask;
    public static Action m_orderGetRewardUICloseCallback;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_SpecialStartStoreUITask;
    private static DelegateBridge __Hotfix_StoreUIUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_PDSDKRequestBuyGoods;
    private static DelegateBridge __Hotfix_PDSDKGetGoods;
    private static DelegateBridge __Hotfix_GetPDSDKGoodByGiftId;
    private static DelegateBridge __Hotfix_PDSDKRequestGoods;
    private static DelegateBridge __Hotfix_OnPDSDKPayCancel;
    private static DelegateBridge __Hotfix_OnPDSDKPayFailed;
    private static DelegateBridge __Hotfix_OnPDSDKPaySuccess;
    private static DelegateBridge __Hotfix_OnPDSDKReqGoodsAck;
    private static DelegateBridge __Hotfix_GetStoreCurrencyInfo;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnNewIntent;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_GetStoreTypeByStoreId;
    private static DelegateBridge __Hotfix_StoreUIController_OnClose;
    private static DelegateBridge __Hotfix_StoreUIController_OnChangeStore;
    private static DelegateBridge __Hotfix_StoreUIController_OnFixedStoreHeroSkinItemClick;
    private static DelegateBridge __Hotfix_StoreUIController_OnFixedStoreSoldierSkinItemClick;
    private static DelegateBridge __Hotfix_StoreUIController_OnFixedStoreItemBuy;
    private static DelegateBridge __Hotfix_StoreUIController_OnFixedStoreBoxBuy;
    private static DelegateBridge __Hotfix_StoreUIController_OnRandomStoreItemBuy;
    private static DelegateBridge __Hotfix_StoreUIController_OnRandomStoreBoxBuy;
    private static DelegateBridge __Hotfix_StoreUIController_GetRandomStore;
    private static DelegateBridge __Hotfix_StoreUIController_RefreshRandomStore;
    private static DelegateBridge __Hotfix_StoreUIController_OMemoryExtractionButtonClick;
    private static DelegateBridge __Hotfix_StoreListUIController_OnSkinItemOutOfDate;
    private static DelegateBridge __Hotfix_StoreUIController_OnRechargeStoreBuy;
    private static DelegateBridge __Hotfix_SendStoreItemCancelBuyReq;
    private static DelegateBridge __Hotfix_SendCheckOnlineReq;
    private static DelegateBridge __Hotfix_ClearBuyingGoodsCache;
    private static DelegateBridge __Hotfix_StoreUIController_OnGiftStoreBuy;
    private static DelegateBridge __Hotfix_SendGiftStoreBuyStoreItemReq;
    private static DelegateBridge __Hotfix_SendGiftStoreAppleSubscribeItemReq;
    private static DelegateBridge __Hotfix_SendRechargeStoreBuyStoreItemReq;
    private static DelegateBridge __Hotfix_PlayerContex_OnGiftStoreBuyNtf;
    private static DelegateBridge __Hotfix_PlayerContex_OnGiftStoreOperationalGoodsUpdateNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_HandleBoxOpenNetTask;
    private static DelegateBridge __Hotfix_PlayerContext_OnRechargeNoughtSuccessNtf;
    private static DelegateBridge __Hotfix_StoreListUIController_OpenAddGoldStore;
    private static DelegateBridge __Hotfix_StoreListUIController_OpenAddCrystalStore;
    private static DelegateBridge __Hotfix_StoreUIController_CrystalNotEnough;
    private static DelegateBridge __Hotfix_StoreUIController_OnExtraCurrencyBuy;
    private static DelegateBridge __Hotfix_UpdateViewAndOpenRewardGoodsUI;
    private static DelegateBridge __Hotfix_CheckOrderReward;
    private static DelegateBridge __Hotfix_SendPullOrderRewardReq;
    private static DelegateBridge __Hotfix_ProcessOrderRewards;
    private static DelegateBridge __Hotfix_ProcessOrderRewardsSub;
    private static DelegateBridge __Hotfix_HandleOneGiftStoreBuyItems;
    private static DelegateBridge __Hotfix_StaticGetRewardGoodsUITask_OnStop;
    private static DelegateBridge __Hotfix_StaticGetRewardGoodsUITask_OnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_add_m_onBuyGiftStoreGoodsEvent;
    private static DelegateBridge __Hotfix_remove_m_onBuyGiftStoreGoodsEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(
      UIIntent fromIntent,
      StoreId storeId,
      int? itemId = null,
      bool isNeedOpenMemoryExtractionPanel = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SpecialStartStoreUITask(UIIntent uiIntent, StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StoreUIUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDKRequestBuyGoods(string goodID, PDSDKGoodType goodType, int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PDSDKGood> PDSDKGetGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PDSDKGood GetPDSDKGoodByGiftId(int giftId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDKRequestGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPDSDKPayCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPDSDKPayFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPDSDKPaySuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnPDSDKReqGoodsAck(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataStoreCurrencyInfo GetStoreCurrencyInfo(
      string currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StoreType GetStoreTypeByStoreId(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnChangeStore(StoreId storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StoreUIController_OnFixedStoreHeroSkinItemClick(
      StoreHeroSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void StoreUIController_OnFixedStoreSoldierSkinItemClick(
      StoreSoldierSkinItemUIController itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnFixedStoreItemBuy(
      StoreId fixedStoreID,
      int fixedStoreItemGoodsID,
      int goodsId,
      int selfChooseItemIndex,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnFixedStoreBoxBuy(
      StoreId fixedStoreID,
      int fixedStoreItemGoodsID,
      int goodsID,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnRandomStoreItemBuy(
      StoreId randomStoreId,
      int index,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnRandomStoreBoxBuy(
      StoreId randomStoreId,
      int index,
      int goodsID,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_GetRandomStore(StoreId randomStoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_RefreshRandomStore(StoreId randomStoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OMemoryExtractionButtonClick(Action OnSucced)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreListUIController_OnSkinItemOutOfDate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnRechargeStoreBuy(
      StoreType storeType,
      int itemGoodsId,
      int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendStoreItemCancelBuyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendCheckOnlineReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBuyingGoodsCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnGiftStoreBuy(
      ConfigDataGiftStoreItemInfo giftStoreItem,
      bool isfristBuy,
      int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGiftStoreBuyStoreItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGiftStoreAppleSubscribeItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendRechargeStoreBuyStoreItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContex_OnGiftStoreBuyNtf(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContex_OnGiftStoreOperationalGoodsUpdateNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnRechargeNoughtSuccessNtf(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreListUIController_OpenAddGoldStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreListUIController_OpenAddCrystalStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_CrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_OnExtraCurrencyBuy(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewAndOpenRewardGoodsUI(List<Goods> rewardGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckOrderReward(Action OnGetReward = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SendPullOrderRewardReq(
      string orderId,
      Action successedCallback = null,
      Action failedCallback = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator ProcessOrderRewards(
      List<Goods> rewardList,
      Action successedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator ProcessOrderRewardsSub(
      List<Goods> rewardsDisplay,
      List<Goods> rewardList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HandleOneGiftStoreBuyItems(List<Goods> rewards, Action successedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StaticGetRewardGoodsUITask_OnStop(Task task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StaticGetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<int> m_onBuyGiftStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
