﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallBossItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./NormalPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_clickButton;
    [AutoBind("./NormalPanel/BossNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossNameText;
    [AutoBind("./NormalPanel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossIconImage;
    [AutoBind("./NormalPanel/TimeLayGroup/TextValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_closeTimeText;
    [AutoBind("./NormalPanel/AdvantageGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_campGroupTransform;
    [AutoBind("./NormalPanel/AdvantageGroup/Group/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_campGameObject;
    [AutoBind("./LockPanel/OpenTime/TextTimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_openTimeText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public int m_bossId;
    private int m_index;
    private bool m_center;
    private ConfigDataAncientCallBossInfo m_ancientCallBossInfo;
    private GameObjectPool m_campPool;
    private float m_lastUpdateTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetBossID;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewTime;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_add_EventOnBossClick;
    private static DelegateBridge __Hotfix_remove_EventOnBossClick;
    private static DelegateBridge __Hotfix_get_Index;
    private static DelegateBridge __Hotfix_set_Index;
    private static DelegateBridge __Hotfix_get_Center;
    private static DelegateBridge __Hotfix_set_Center;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossID(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<AncientCallBossItemUIController> EventOnBossClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Index
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Center
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
