﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private UnchartedUIController m_unchartedUIController;
    private int m_nowSeconds;
    private int m_tabType;
    private DateTime m_nextCheckClimbTowerFlushTime;
    private List<ConfigDataCooperateBattleInfo> m_ooperateBattleInfos;
    private List<ConfigDataHeroPhantomInfo> m_heroPhantomInfos;
    private List<ConfigDataUnchartedScoreInfo> m_unchartedScoreInfos;
    private List<ConfigDataCollectionActivityInfo> m_collectionActivityInfos;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_InitUnchartedUIController;
    private static DelegateBridge __Hotfix_UninitUnchartedUIController;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateClimbTowerFlushTime;
    private static DelegateBridge __Hotfix_FlushClimbTower;
    private static DelegateBridge __Hotfix_FadeAndStartUnchartedSubUITask;
    private static DelegateBridge __Hotfix_UnchartedUIController_OnShowUncharted;
    private static DelegateBridge __Hotfix_UnchartedUIController_OnShowTeam;
    private static DelegateBridge __Hotfix_TeamUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_UnchartedUIController_OnReturn;
    private static DelegateBridge __Hotfix_UnchartedUIController_OnChangeUnchartedTab;
    private static DelegateBridge __Hotfix_UnchartedUIController_OnStoreClick;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitUnchartedUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitUnchartedUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateClimbTowerFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushClimbTower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeAndStartUnchartedSubUITask(
      BattleType battleType,
      int chapterId,
      bool isAfterBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUIController_OnShowUncharted(BattleType battleType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUIController_OnShowTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUIController_OnChangeUnchartedTab(int tabType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedUIController_OnStoreClick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
