﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentArchiveUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipmentArchiveUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prefabAnimation;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./EquipmentInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentInfoPanel;
    [AutoBind("./EquipmentInfoPanel/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/All", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_aLLItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Weapon", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_weaponItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Armour", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_armorItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Helmet", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_helmetItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Ornament", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_ornamentItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/GridLayout/Other", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_otherItemToggle;
    [AutoBind("./EquipmentList/EquipmentFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterText;
    [AutoBind("./EquipmentList/EquipmentFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipmentFilterButton;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterPanel;
    [AutoBind("./EquipmentList/EquipmentFilter/SortTypes/BGImages/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterBGButton;
    [AutoBind("./EquipmentList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_itemContentInfinityGrid;
    [AutoBind("./EquipmentList/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemContent;
    [AutoBind("./Prefab/ItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPrefab;
    [AutoBind("./CountLimit/CountGroup/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currentItemCountText;
    [AutoBind("./CountLimit/CountGroup/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxItemCountText;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_selectBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_allBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_armorBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_helmetBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_ornamentBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_weaponBagItemList;
    private List<EquipmentArchiveUIController.EquipmentInfoWrap> m_otherBagItemList;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<ArchiveItemUIController> m_itemUIControllerList;
    private EquipmentArchiveUIController.EquipmentInfoWrap m_selectEquipmentInfoWrap;
    private ArchiveItemDetailInfoController m_itemDetailInfoController;
    private int m_allOwnItemCount;
    private int m_armorOwnItemCount;
    private int m_helmetOwnItemCount;
    private int m_ornamentOwnItemCount;
    private int m_weaponOwnItemCount;
    private int m_otherOwnItemCount;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetTask;
    private static DelegateBridge __Hotfix_EnterController;
    private static DelegateBridge __Hotfix_ItemListCompare;
    private static DelegateBridge __Hotfix_RefreshItem;
    private static DelegateBridge __Hotfix_RefreshItemCount;
    private static DelegateBridge __Hotfix_CreateItemList;
    private static DelegateBridge __Hotfix_DestroyItemBagList;
    private static DelegateBridge __Hotfix_RefreshItemBagWithHeroData;
    private static DelegateBridge __Hotfix_OnReturnClick;
    private static DelegateBridge __Hotfix_OnItemClick;
    private static DelegateBridge __Hotfix_OnEquipmentFilterSwitchClick;
    private static DelegateBridge __Hotfix_OnEquipmentFilterClick;
    private static DelegateBridge __Hotfix_OnFilterBGClick;
    private static DelegateBridge __Hotfix_OnGetItemPathClick;
    private static DelegateBridge __Hotfix_UpdateChildrenCallbackDelegate;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentArchiveUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTask(ArchiveUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ItemListCompare(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap1,
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentWrap2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItem(ArchiveItemUIController archiveItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItemCount(int currentCount, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateItemList(
      List<EquipmentArchiveUIController.EquipmentInfoWrap> equipmentInfoWrapList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyItemBagList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshItemBagWithHeroData(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick(ArchiveItemUIController archiveItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentFilterSwitchClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentFilterClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetItemPathClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildrenCallbackDelegate(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    public class EquipmentInfoWrap
    {
      public ConfigDataEquipmentInfo equipmentInfo;
      public bool isUnlocked;
      public bool isSelect;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public EquipmentInfoWrap()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
