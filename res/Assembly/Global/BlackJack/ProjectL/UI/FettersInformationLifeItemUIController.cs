﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersInformationLifeItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersInformationLifeItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateCtrl;
    [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockContentText;
    [AutoBind("./Title/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockTitleText;
    [AutoBind("./Lock/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lockText;
    public ConfigDataHeroBiographyInfo m_heroBiographyInfo;
    public bool? IsNew;
    private bool m_isLock;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitLifeItem;
    private static DelegateBridge __Hotfix_UpdateIsNewValue;
    private static DelegateBridge __Hotfix_IsNewTagActive;
    private static DelegateBridge __Hotfix_IsBiographyRead;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLifeItem(int biographyId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateIsNewValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNewTagActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBiographyRead(int id)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
