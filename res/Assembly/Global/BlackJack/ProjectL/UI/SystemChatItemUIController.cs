﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SystemChatItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SystemChatItemUIController : UIControllerBase
  {
    [AutoBind("./BgImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text ContentText;
    private static DelegateBridge __Hotfix_UpdateChatInfo;
    private static DelegateBridge __Hotfix_FakePlayerCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatInfo(ChatComponent.ChatMessageClient chatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FakePlayerCount(int trueCount)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
