﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCommentUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroCommentUIController : UIControllerBase
  {
    private int m_curShowedNormalCommentCount = 10;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Normal", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_commentNormalToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Normal/UnClick", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commentNormalToggleUnClick;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Normal/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commentNormalToggleClick;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Hot", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_commentHotToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Hot/UnClick", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commentHotToggleUnClick;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Hot/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commentHotToggleClick;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/RankingList", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_commentRankingToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/RankingList/UnClick", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commnetRankingUnClick;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/RankingList/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commnetRankingClick;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/RankingList/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankingListLockButton;
    [AutoBind("./NoCommentPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noCommentPanelObj;
    [AutoBind("./CommentList/Viewport/Content/HotContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_hotCommentListObj;
    [AutoBind("./CommentList/Viewport/Content/NormalContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_normalCommentListObj;
    [AutoBind("./CommentList/Viewport/Content/MoreCommentText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_moreCommentTextObj;
    [AutoBind("./CommentList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_commentListScrollRect;
    [AutoBind("./InputComment", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inputCommentGo;
    [AutoBind("./InputComment/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_commentInputField;
    [AutoBind("./InputComment/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_commentSendButton;
    [AutoBind("./Prefab/CommentItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_commentItemPrefabObj;
    [AutoBind("./Prefab/MoreCommentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_moreCommentButton;
    [AutoBind("./RankingListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankingListGroup;
    [AutoBind("./RankingListGroup", AutoBindAttribute.InitState.NotInit, false)]
    public RankingListUIController m_rankingListUICtrl;
    [AutoBind("./PlayerHeroDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeroDetailPanelObj;
    [AutoBind("./PlayerHeroDetailPanel/GrayBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerHeroDetailCloseImage;
    [AutoBind("./PlayerHeroDetailPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerHeroDetailCloseBtn;
    [AutoBind("./PlayerHeroDetailPanel/Detail/HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeroLvStarsObj;
    [AutoBind("./PlayerHeroDetailPanel/Detail/JobName/JobNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroJobNameText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/JobLV/LvImgs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeroJobLvStarsObj;
    [AutoBind("./PlayerHeroDetailPanel/Detail/JobLV/MasterImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroJobMasterImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/HeroHalfBodyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroHalfBodyImage;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroPropHPImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroPropDFImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroPropATImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroPropMagicDFImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroPropMagicImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroPropDEXImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropHPValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropDFValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropATValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropMagicDFValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropMagicValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropDEXValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropHPAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropDFAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropATAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropMagicDFAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropMagicAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroPropDEXAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/NameTitle/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/HeroName/LvValue/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroLvValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/HeroName/Name/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroNameText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/HeroName/SoldierIconImg/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroSoldierIconImg;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Equipments/1", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroEquipments1;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Equipments/2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroEquipments2;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Equipments/3", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroEquipments3;
    [AutoBind("./PlayerHeroDetailPanel/Detail/Equipments/4", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroEquipments4;
    [AutoBind("./PlayerHeroDetailPanel/Detail/TalentIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroTalentIcon;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillContent/1", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroSkillIcon1;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillContent/2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroSkillIcon2;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillContent/3", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeroSkillIcon3;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeroSkillDescObj;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/TitleNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroSkillDescNameText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerHeroSkillDescCostObj;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/SkillType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroSkillDescText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/Type/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroSkillDescTypeText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/CD/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroSkillDescCDText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/Distance/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroSkillDescDistanceText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SkillDesc/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerHeroSkillDescRangeText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicObj;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFValueText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierATAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDFAddText;
    [AutoBind("./PlayerHeroDetailPanel/Detail/SoldierInfo/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private Hero m_hero;
    private UISpineGraphic m_soldierInfoGraphic;
    private HeroCharUIController m_heroCharUIController;
    private HeroCommentUIController.ToggleState m_curToggleState;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_HeroCommentUpdateView;
    private static DelegateBridge __Hotfix_UpdateRankingListLock;
    private static DelegateBridge __Hotfix_CreateComments;
    private static DelegateBridge __Hotfix_UpdateRankingList;
    private static DelegateBridge __Hotfix_GetNormalCommentsInAllComments;
    private static DelegateBridge __Hotfix_CommentComparerByTime;
    private static DelegateBridge __Hotfix_CommentComparerByPraisedNum;
    private static DelegateBridge __Hotfix_GetCommentsWithCount;
    private static DelegateBridge __Hotfix_CreateCommentByList;
    private static DelegateBridge __Hotfix_ShowPlayerHeroDetailPanel;
    private static DelegateBridge __Hotfix_OnSkillItemClick;
    private static DelegateBridge __Hotfix_SetSkillDescPanel;
    private static DelegateBridge __Hotfix_SetSoldierPropAddText;
    private static DelegateBridge __Hotfix_ClosePlayerHeroDetailPanel;
    private static DelegateBridge __Hotfix_OnCommentItemNameClick;
    private static DelegateBridge __Hotfix_OnCommentItemPraisedBtnClick;
    private static DelegateBridge __Hotfix_OnMoreCommentButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnAllCommentToggleClick;
    private static DelegateBridge __Hotfix_OnHotCommentToggleClick;
    private static DelegateBridge __Hotfix_OnRankingListCommentToggleClick;
    private static DelegateBridge __Hotfix_OnRankingListLockButtonClick;
    private static DelegateBridge __Hotfix_OnCommentSendButtonClick;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnCommentSend;
    private static DelegateBridge __Hotfix_remove_EventOnCommentSend;
    private static DelegateBridge __Hotfix_add_EventOnNameClick;
    private static DelegateBridge __Hotfix_remove_EventOnNameClick;
    private static DelegateBridge __Hotfix_add_EventOnPraisedBtnClick;
    private static DelegateBridge __Hotfix_remove_EventOnPraisedBtnClick;
    private static DelegateBridge __Hotfix_add_EventOnGetHeroComments;
    private static DelegateBridge __Hotfix_remove_EventOnGetHeroComments;
    private static DelegateBridge __Hotfix_add_EventOnRankingQurey;
    private static DelegateBridge __Hotfix_remove_EventOnRankingQurey;
    private static DelegateBridge __Hotfix_add_EventOnRankingListLockClick;
    private static DelegateBridge __Hotfix_remove_EventOnRankingListLockClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCommentUpdateView(Hero hero, bool isNeedResetScrollViewPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingListLock(bool isLock)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateComments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingList(RankingListInfo rankInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<HeroCommentEntry> GetNormalCommentsInAllComments(
      List<HeroCommentEntry> allComments,
      List<HeroCommentEntry> hotComments,
      int needShowedCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CommentComparerByTime(HeroCommentEntry c1, HeroCommentEntry c2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CommentComparerByPraisedNum(HeroCommentEntry c1, HeroCommentEntry c2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<HeroCommentEntry> GetCommentsWithCount(
      List<HeroCommentEntry> commentList,
      int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCommentByList(
      List<HeroCommentEntry> commentList,
      GameObject content,
      bool isShowPraisedIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerHeroDetailPanel(HeroCommentEntry commentEntry, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillDescPanel(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierPropAddText(int oldValue, int newValue, Text txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePlayerHeroDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCommentItemNameClick(HeroCommentEntry comment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCommentItemPraisedBtnClick(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMoreCommentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllCommentToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHotCommentToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankingListCommentToggleClick(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankingListLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCommentSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ref UISpineGraphic graphic,
      string assetName,
      GameObject parent,
      int direction,
      Vector2 offset,
      float scale,
      List<ReplaceAnim> anims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(ref UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string> EventOnCommentSend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, HeroCommentEntry> EventOnNameClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong> EventOnPraisedBtnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool> EventOnGetHeroComments
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnRankingQurey
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRankingListLockClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum ToggleState
    {
      Hot,
      All,
      Ranking,
    }
  }
}
