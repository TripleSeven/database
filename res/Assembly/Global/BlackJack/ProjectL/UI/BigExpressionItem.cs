﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BigExpressionItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BigExpressionItem
  {
    private Action<int> m_onClick;
    public ConfigDataBigExpressionInfo m_bigExpressionInfo;
    public GameObject m_expression;
    private Image imageIcon;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public BigExpressionItem(
      ConfigDataBigExpressionInfo bigExpressionInfo,
      GameObject expression,
      Action<int> click)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
