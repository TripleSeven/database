﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleReportPlayerPanelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Battle;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleReportPlayerPanelUIController : UIControllerBase
  {
    [AutoBind("./PlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerUIStateController;
    [AutoBind("./PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfo/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeadIconImage;
    [AutoBind("./PlayerInfo/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerHeadFrameTransform;
    [AutoBind("./PlayerInfo/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfo/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGroupGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject2;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText2;
    [AutoBind("./PlayerInfo/Emotion", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_expressionUIStateController;
    [AutoBind("./PlayerInfo/Emotion/EmotionImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_expressionImage;
    [AutoBind("./WinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_winGroupUIStateController;
    [AutoBind("./WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin1GameObject;
    [AutoBind("./WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin2GameObject;
    [AutoBind("./IntoBattleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroupGameObject;
    [AutoBind("./IntoBattleGroup/MyHeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_heroGroupTransform;
    private float m_hideExpressionTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetPlayer;
    private static DelegateBridge __Hotfix_SetPlayerActive;
    private static DelegateBridge __Hotfix_ShowCountdown;
    private static DelegateBridge __Hotfix_SetCountdown;
    private static DelegateBridge __Hotfix_ShowWinCount;
    private static DelegateBridge __Hotfix_ShowHeros;
    private static DelegateBridge __Hotfix_ClearHeros;
    private static DelegateBridge __Hotfix_AddHero;
    private static DelegateBridge __Hotfix_SetHeroAlive;
    private static DelegateBridge __Hotfix_ShowBigExpression;
    private static DelegateBridge __Hotfix_HideBigExpression;
    private static DelegateBridge __Hotfix_Update;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCountdown(TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWinCount(int winCount, bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHeros(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(GameObject prefab, BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAlive(int actorId, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBigExpression(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBigExpression()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
