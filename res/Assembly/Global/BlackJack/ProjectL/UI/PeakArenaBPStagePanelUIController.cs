﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaBPStagePanelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaBPStagePanelUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./PlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerUIStateController;
    [AutoBind("./PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./PlayerInfo/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerHeadIconImage;
    [AutoBind("./PlayerInfo/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerHeadFrameTransform;
    [AutoBind("./PlayerInfo/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./PlayerInfo/OffLineImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_playerOfflineGameObject;
    [AutoBind("./PlayerInfo/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGroupGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./PlayerInfo/TimeGroup/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject2;
    [AutoBind("./PlayerInfo/TimeGroup/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText2;
    [AutoBind("./PlayerInfo/WinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_winGroupUIStateController;
    [AutoBind("./PlayerInfo/WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin1GameObject;
    [AutoBind("./PlayerInfo/WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winGroupWin2GameObject;
    [AutoBind("./PlayerInfo/OpponentHideOrShow", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerUnknownUIStateController;
    [AutoBind("./HeroList/MyHeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroScrollRect;
    [AutoBind("./HeroList/EnemyHeroListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_heroScrollRect2;
    [AutoBind("./HeroList/EnemyActiveDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activeDescUIStateController;
    [AutoBind("./HeroList/KickOutHero/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_banHeroGroupTransform;
    [AutoBind("./HeroList/KickOutHero", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_banHeroUIStateController;
    [AutoBind("./IntoBattleGroup/MyHeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_pickHeroGroupTransform;
    [AutoBind("./IntoBattleGroup/EnemyHeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_pickHeroGroupTransform2;
    [AutoBind("./IntoBattleGroup/MyText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pickHeroTitleGameObject;
    [AutoBind("./IntoBattleGroup/EnemyText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pickHeroTitleGameObject2;
    [AutoBind("./ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arrowGameObject;
    private CommonUIStateController[] m_waitPickHeroEffectUIStateControllers;
    private int m_pickHeroCount;
    private bool m_isBpPanelActive;
    private GameObjectPool<PeakArenaBpStageHeroItemUIController> m_heroItemPool;
    private GameObjectPool<PeakArenaBpStageHeroItemUIController> m_heroItemPool2;
    private GameObjectPool<PeakArenaBpStageHeroItemUIController> m_banHeroItemPool;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_ActiveBpPanel;
    private static DelegateBridge __Hotfix_ShowBpPanelActiveArrow;
    private static DelegateBridge __Hotfix_SetPlayer;
    private static DelegateBridge __Hotfix_SetPlayerUnknown;
    private static DelegateBridge __Hotfix_SetPlayerActive;
    private static DelegateBridge __Hotfix_SetPlayerStatus;
    private static DelegateBridge __Hotfix_ClearHeros;
    private static DelegateBridge __Hotfix_AddHero;
    private static DelegateBridge __Hotfix_ShowPickHeroEffects;
    private static DelegateBridge __Hotfix_ShowWaitPickHeroEffect;
    private static DelegateBridge __Hotfix_SetWaitPickHeroEffect;
    private static DelegateBridge __Hotfix_ShowBanHeroEffects;
    private static DelegateBridge __Hotfix_HideBanHeroEffects;
    private static DelegateBridge __Hotfix_ShowEnemyActiveDesc;
    private static DelegateBridge __Hotfix_HideEnemyActiveDesc;
    private static DelegateBridge __Hotfix_ShowBattleReportActiveDesc;
    private static DelegateBridge __Hotfix_HideBattleReportActiveDesc;
    private static DelegateBridge __Hotfix_SetPickHeroTitle;
    private static DelegateBridge __Hotfix_ShowCountdown;
    private static DelegateBridge __Hotfix_SetCountdown;
    private static DelegateBridge __Hotfix_ShowWinCount;
    private static DelegateBridge __Hotfix_PeakArenaBanPickHeroItemUIController_OnClick;
    private static DelegateBridge __Hotfix_add_EventOnHeroItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnHeroItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBPStagePanelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActiveBpPanel(bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpPanelActiveArrow(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerUnknown(bool unknown)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerStatus(PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHero(
      BPStageHero hero,
      int actorId,
      PeakArenaBPStageHeroItemState state,
      bool showJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPickHeroEffects(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitPickHeroEffect(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetWaitPickHeroEffect(int idx, bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanHeroEffects(List<BPStageHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBanHeroEffects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnemyActiveDesc(BPStageCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideEnemyActiveDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportActiveDesc(BPStageCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleReportActiveDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPickHeroTitle(bool isEnemy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCountdown(TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWinCount(int winCount, bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBanPickHeroItemUIController_OnClick(
      PeakArenaBpStageHeroItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PeakArenaBpStageHeroItemUIController> EventOnHeroItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
