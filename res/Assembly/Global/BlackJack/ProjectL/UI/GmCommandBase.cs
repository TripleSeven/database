﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GmCommandBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public abstract class GmCommandBase
  {
    protected string m_type;
    protected string m_param;
    protected string[] m_paramArray;
    protected int m_result;
    protected ProjectLPlayerContext m_playerContext;
    protected IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Enter;
    private static DelegateBridge __Hotfix_Excute;
    private static DelegateBridge __Hotfix_Exit;
    private static DelegateBridge __Hotfix_Run;

    [MethodImpl((MethodImplOptions) 32768)]
    protected GmCommandBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(string type, string param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void Enter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void Excute()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void Exit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Run()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
