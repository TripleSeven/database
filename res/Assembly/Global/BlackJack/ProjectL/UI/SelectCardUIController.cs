﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelectCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SelectCardUIController : UIControllerBase
  {
    [AutoBind("./MainUI/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./MainUI", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mainUIGo;
    [AutoBind("./MainUI/Currencys/NormalCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_normalCardNumText;
    [AutoBind("./MainUI/Currencys/DiamondCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_diamondsCardNumText;
    [AutoBind("./MainUI/Currencys/Diamond/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_diamondsAddBtn;
    [AutoBind("./MainUI/Currencys/Diamond/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_diamondsNumText;
    [AutoBind("./MainUI/SummonPanel/Ads/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_adsImage;
    [AutoBind("./MainUI/SummonPanel/Ads/Image/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_adsTimeText;
    [AutoBind("./MainUI/SummonPanel/Ads/Image/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_activityDetailButton;
    [AutoBind("./MainUI/SummonPanel/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_cardPoolToggleGroupGo;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools/TicketSummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_ticketSummonToggle;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools/MagicStoneSummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_magicStoneSummonToggle;
    [AutoBind("./MainUI/SummonPanel/Margin/RightToggleGroup/CardPoolsScrollView/Viewport/CardPools/EquipSummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_equipSummonToggle;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_singleSelectBtn;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton/DetailGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectBtnIconImg;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton/DetailGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_singleSelectBtnNumText;
    [AutoBind("./MainUI/SummonPanel/SingleSelectButton/DetailGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_singleSelectBtnTitleText;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tenSelectBtn;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/DetailGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tenSelectBtnIconImg;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/DetailGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tenSelectBtnNumText;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/DetailGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tenSelectBtnTitleText;
    [AutoBind("./MainUI/SummonPanel/TenSelectButton/U_TenSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenSelectBtn3DEffect;
    [AutoBind("./MainUI/BuyDiamondButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyDiamondButton;
    [AutoBind("./Prefab/SummonToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_summonToggleItemPrefab;
    [AutoBind("./AfterSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_afterSelectPanel;
    [AutoBind("./AfterSelectPanel/ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectArrowButton;
    [AutoBind("./AfterSelectPanel/OpenNoticePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectOpenNoticePanelGo;
    [AutoBind("./AfterSelectPanel/OpenNoticePanel/ArrowImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectOpenNoticePanelImage;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectPanel;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleHeroWordPanel;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/ContinueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_singleSelectContinueImage;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/NewText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectNewTextObj;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/GetText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectGetTextObj;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleSelectStarGroupObj;
    [AutoBind("./AfterSelectPanel/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectSkipButton;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/ItemImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectItemImage;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/FragmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectFragmentIconImg;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/FragmentIcon/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_singleSelectFragmentCount;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/SSR", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectFragmentSSRImg;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/SR", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_singleSelectFragmentSRImg;
    [AutoBind("./AfterSelectPanel/SingleSelectPanel/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleShareButtonDummy;
    [AutoBind("./AfterSelectPanel/SharePhotoDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./AfterSelectPanel/TenSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenSelectPanel;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/ItemShowPanel/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenSelectContentObj;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_afterSelectConfirmButton;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectAgainButton;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/Single", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectAgainButtonSingleBgObj;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/Ten", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectAgainButtonTenBgObj;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/DetailGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_selectAgainBtnIconImg;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/DetailGroup/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectAgainBtnNumText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/SelectAgainButton/DetailGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectAgainBtnTitleText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tenShareButtonDummy;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/Diamond/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_diamondsAddBtn2;
    [AutoBind("./ChangeCrystalToTicketPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeCrystalToTicketPanel;
    [AutoBind("./ChangeCrystalToTicketPanel/Detail/Tip", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_changeCrystalToTicketPanelTip;
    [AutoBind("./ChangeCrystalToTicketPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeCrystalToTicketPanelCancelButton;
    [AutoBind("./ChangeCrystalToTicketPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeCrystalToTicketPanelConfirmButton;
    [AutoBind("./CrystalAndTicketNotEnoughPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_crystalAndTicketNotEnoughPanel;
    [AutoBind("./CrystalAndTicketNotEnoughPanel/Detail/Tip", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_crystalAndTicketNotEnoughPanelTip;
    [AutoBind("./CrystalAndTicketNotEnoughPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_crystalAndTicketNotEnoughPanelCancelButton;
    [AutoBind("./CrystalAndTicketNotEnoughPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_crystalAndTicketNotEnoughPanelConfirmButton;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/NormalCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_afterSelectPanelNormalCardNumText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/DiamondCard/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_afterSelectPanelDiamondsCardNumText;
    [AutoBind("./AfterSelectPanel/TenSelectPanel/Currencys/Diamond/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_afterSelectPanelDiamondsNumText;
    [AutoBind("./MainUI/SummonPanel/Margin/ArchiveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_archiveButton;
    [AutoBind("/MainUI/DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailButton;
    [AutoBind("/DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailPanelObj;
    [AutoBind("/DetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_detailPanelStateCtrl;
    [AutoBind("/DetailPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailCloseButton;
    [AutoBind("/DetailPanel/BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_detailReturnBgButton;
    [AutoBind("/DetailPanel/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_detailScrollRect;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailWeightTitleText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/QualityGroup/SSR/WeightText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailSSRWeightText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/QualityGroup/SR/WeightText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailSRWeightText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/ProbabilityGroup/WeightInfo/QualityGroup/R/WeightText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailRWeightText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/InfoGroup/Title/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_detailScrollViewContentTitleText;
    [AutoBind("/DetailPanel/Detail/ScrollView/Viewport/Content/InfoGroup/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailScrollViewContent;
    [AutoBind("/Prefab/PoolItemWithRank", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_poolItemWithRankPrefab;
    private Button m_weiBoShareOneButton;
    private Button m_weChatShareOneButton;
    private Button m_fbShareOneButton;
    private Button m_twitterShareOneButton;
    private Button m_instagramShareOneButton;
    private Button m_weiBoShareTenButton;
    private Button m_weChatShareTenButton;
    private Button m_fbShareTenButton;
    private Button m_twitterShareTenButton;
    private Button m_instagramShareTenButton;
    private Text m_playerNameText;
    private Text m_playerLvText;
    private Text m_serverNameText;
    private Text m_heroNameText;
    private int m_nToShareHeroId;
    private int m_cardPoolId;
    private CardPool m_cardPool;
    private bool m_isClockHold;
    private bool m_isSingleSelect;
    private bool m_isContinueButtonClick;
    private List<Goods> m_selectRewards;
    private const string PoolItemNameText = "NameText";
    private const int NormalCardId = 4000;
    private const int CrystalCardId = 4001;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private HeroCharUIController m_heroCharUIController;
    private SelectCardBackground3DController m_selectCardBackground3DController;
    private ShareTenSelectCardUIController m_shareTenSelectCardUIController;
    private bool m_hasShowClockEffect;
    private List<SummonToggleUIController> m_toggleCtrlList;
    private List<Goods> m_extraRewardsCache;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInSelectCard;
    private static DelegateBridge __Hotfix_UpdateSharePlayerInfo;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetShareTenSelectCardUIController;
    private static DelegateBridge __Hotfix_ShowSelectCardRewards;
    private static DelegateBridge __Hotfix_PlayRewardsEffect;
    private static DelegateBridge __Hotfix_PlaySingleRewardEffect;
    private static DelegateBridge __Hotfix_StopClockEffectSoundOnSkipButtonClick;
    private static DelegateBridge __Hotfix_AfterShowClockEffectFinished;
    private static DelegateBridge __Hotfix_OnClockButtonDrag;
    private static DelegateBridge __Hotfix_OnContinueButtonClick;
    private static DelegateBridge __Hotfix_PlayTotalRewardsEffect;
    private static DelegateBridge __Hotfix_SetAfterSelectCurrencyPanel;
    private static DelegateBridge __Hotfix_SetSingleSelectRewardInfo;
    private static DelegateBridge __Hotfix_SetTenSelectRewardsInfo;
    private static DelegateBridge __Hotfix_CreateCardPoolTabs;
    private static DelegateBridge __Hotfix_SummonToggleCtrl_EventOnToggleClick;
    private static DelegateBridge __Hotfix_SetValuesAfterToggleChanged;
    private static DelegateBridge __Hotfix_SetSelectButton;
    private static DelegateBridge __Hotfix_OnActivityDetailButtonClick;
    private static DelegateBridge __Hotfix_OnSingleSelectButtonClick;
    private static DelegateBridge __Hotfix_OnTenSelectButtonClick;
    private static DelegateBridge __Hotfix_OnSelectAgainButtonClick;
    private static DelegateBridge __Hotfix_CloseTenSelectPanel;
    private static DelegateBridge __Hotfix_ShowChangeCrystalToTicketPanel;
    private static DelegateBridge __Hotfix_ChangeCrystalToTicketPanelCancelButtonClick;
    private static DelegateBridge __Hotfix_CloseChangeCrystalToTicketPanel;
    private static DelegateBridge __Hotfix_ChangeCrystalToTicketPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_ShowCrystalAndTicketNotEnoughPanel;
    private static DelegateBridge __Hotfix_CrystalAndTicketNotEnoughPanelCancelButtonClick;
    private static DelegateBridge __Hotfix_CloseCrystalAndTicketNotEnoughPanel;
    private static DelegateBridge __Hotfix_CrystalAndTicketNotEnoughPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnBuyDiamondButtonClick;
    private static DelegateBridge __Hotfix_OnAddCrystalBtnClick;
    private static DelegateBridge __Hotfix_OnAddCrystal;
    private static DelegateBridge __Hotfix_SkipShowRewardEffect;
    private static DelegateBridge __Hotfix_Close3DUIEffect;
    private static DelegateBridge __Hotfix_CloseShowSelectRewardsPanel;
    private static DelegateBridge __Hotfix_IsShowSelectCardResult;
    private static DelegateBridge __Hotfix_ShowDetailScrollView;
    private static DelegateBridge __Hotfix_CreatePoolItemPrefab;
    private static DelegateBridge __Hotfix_OnDetailButtonClick;
    private static DelegateBridge __Hotfix_OnCloseDetailPanelButtonClick;
    private static DelegateBridge __Hotfix_OnArchiveButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnInstagramShareOnceClick;
    private static DelegateBridge __Hotfix_OnTwitterShareOnceClick;
    private static DelegateBridge __Hotfix_OnFacebookShareOnceClick;
    private static DelegateBridge __Hotfix_OnWeiBoShareOnceClick;
    private static DelegateBridge __Hotfix_OnWeChatShareOnceClick;
    private static DelegateBridge __Hotfix_OnInstagramShareTenClick;
    private static DelegateBridge __Hotfix_OnTwitterShareTenClick;
    private static DelegateBridge __Hotfix_OnFacebookShareTenClick;
    private static DelegateBridge __Hotfix_OnWeiBoShareTenClick;
    private static DelegateBridge __Hotfix_OnWeChatShareTenClick;
    private static DelegateBridge __Hotfix_CaptureOnceFrame;
    private static DelegateBridge __Hotfix_CaptureTenFrame;
    private static DelegateBridge __Hotfix_add_EventOnInstagramShareTen;
    private static DelegateBridge __Hotfix_remove_EventOnInstagramShareTen;
    private static DelegateBridge __Hotfix_add_EventOnTwitterShareTen;
    private static DelegateBridge __Hotfix_remove_EventOnTwitterShareTen;
    private static DelegateBridge __Hotfix_add_EventOnFacebookShareTen;
    private static DelegateBridge __Hotfix_remove_EventOnFacebookShareTen;
    private static DelegateBridge __Hotfix_add_EventOnWeiBoShareTen;
    private static DelegateBridge __Hotfix_remove_EventOnWeiBoShareTen;
    private static DelegateBridge __Hotfix_add_EventOnWeChatShareTen;
    private static DelegateBridge __Hotfix_remove_EventOnWeChatShareTen;
    private static DelegateBridge __Hotfix_add_EventOnArchive;
    private static DelegateBridge __Hotfix_remove_EventOnArchive;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnSelectCard;
    private static DelegateBridge __Hotfix_remove_EventOnSelectCard;
    private static DelegateBridge __Hotfix_add_EventOnShowSelectCardHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowSelectCardHelp;
    private static DelegateBridge __Hotfix_add_EventOnShowActivityDetail;
    private static DelegateBridge __Hotfix_remove_EventOnShowActivityDetail;
    private static DelegateBridge __Hotfix_add_EventOnBagCapcityNotEnough;
    private static DelegateBridge __Hotfix_remove_EventOnBagCapcityNotEnough;
    private static DelegateBridge __Hotfix_add_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_remove_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_add_EventOnAddCrystal;
    private static DelegateBridge __Hotfix_remove_EventOnAddCrystal;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInSelectCard(int cardPoolId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSharePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetShareTenSelectCardUIController(ShareTenSelectCardUIController controller)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectCardRewards(
      List<Goods> rewards,
      List<Goods> extraRewards,
      SelectCardBackground3DController selectCardBackground3DController)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayRewardsEffect(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlaySingleRewardEffect(
      Goods goods,
      bool isNeedClockEffect,
      int rank,
      int maxRank,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopClockEffectSoundOnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AfterShowClockEffectFinished(Goods goods, int index, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClockButtonDrag(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayTotalRewardsEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAfterSelectCurrencyPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSingleSelectRewardInfo(Goods goods, int index, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTenSelectRewardsInfo(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCardPoolTabs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SummonToggleCtrl_EventOnToggleClick(SummonToggleUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetValuesAfterToggleChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSelectButton(Image iconImage, Text titleText, Text numText, bool isSingle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActivityDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSingleSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTenSelectButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectAgainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseTenSelectPanel(Action action = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowChangeCrystalToTicketPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeCrystalToTicketPanelCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseChangeCrystalToTicketPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeCrystalToTicketPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCrystalAndTicketNotEnoughPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CrystalAndTicketNotEnoughPanelCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseCrystalAndTicketNotEnoughPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CrystalAndTicketNotEnoughPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyDiamondButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddCrystalBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkipShowRewardEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Close3DUIEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseShowSelectRewardsPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsShowSelectCardResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDetailScrollView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreatePoolItemPrefab(PoolContentItemData item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseDetailPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArchiveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInstagramShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFacebookShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatShareOnceClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInstagramShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFacebookShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatShareTenClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureOnceFrame(int sharePlatform)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureTenFrame(int sharePlatform)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnInstagramShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTwitterShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnFacebookShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnWeiBoShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnWeChatShareTen
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnArchive
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, bool> EventOnSelectCard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSelectCardHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<CardPool> EventOnShowActivityDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBagCapcityNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
