﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildCreateReqNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class GuildCreateReqNetTask : UINetTask
  {
    private string m_guildName;
    private string m_hiringDeclaration;
    private string m_announcement;
    private bool m_autoJoin;
    private int m_joinLevel;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnMsgAck;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildCreateReqNetTask(
      string guildName,
      string hiringDeclaration,
      string announcement,
      bool autoJoin,
      int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMsgAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
