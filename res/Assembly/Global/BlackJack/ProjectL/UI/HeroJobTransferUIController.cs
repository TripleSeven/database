﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobTransferUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroJobTransferUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/TransferPanel/TransferList/Rank1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankLayout1;
    [AutoBind("./Margin/TransferPanel/TransferList/Rank2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankLayout2;
    [AutoBind("./Margin/TransferPanel/TransferList/Rank3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankLayout3;
    [AutoBind("./Margin/TransferPanel/TransferList/Rank4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankLayout4;
    [AutoBind("./Margin/TransferPanel/TransferList/Line", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankLine1;
    [AutoBind("./Margin/TransferPanel/TransferList/Line/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine1Image;
    [AutoBind("./Margin/TransferPanel/TransferList/Line2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine2Image;
    [AutoBind("./Margin/TransferPanel/TransferList/Line2/Image1", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine2Image1;
    [AutoBind("./Margin/TransferPanel/TransferList/Line2/Image2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine2Image2;
    [AutoBind("./Margin/TransferPanel/TransferList/Line3/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine3Image;
    [AutoBind("./Margin/TransferPanel/TransferList/Line3/Image1", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine3Image1;
    [AutoBind("./Margin/TransferPanel/TransferList/Line3/Image2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankLine3Image2;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./Gold/NumText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldNumText;
    [AutoBind("./Gold/AddBtn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddBtn;
    [AutoBind("./MagicStone/NumText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_magicStoneNumText;
    [AutoBind("./MagicStone/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_magicStoneBtn;
    [AutoBind("./HeroJobInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroJobInfoPanelObj;
    [AutoBind("./HeroJobInfo/JobBaseInfo/JobNameGroup/JobNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobNameText;
    [AutoBind("./HeroJobInfo/JobBaseInfo/JobNameGroup/JobNameEnText/JobNameEnText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobNameEnText;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTypeBgImage;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTypeBgImage2;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Faction/TypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTypeText;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobMoveValueText;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobRangeValueText;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Advantage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobAdvantageText;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Advantage/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobAdvantageText1;
    [AutoBind("./HeroJobInfo/JobBaseInfo/Advantage/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobAdvantageText2;
    [AutoBind("./HeroJobInfo/HeroGraphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGraphicObj;
    [AutoBind("./HeroJobInfo/JobBaseInfo/JobSoldierTypeImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobSoldierTypeImage;
    [AutoBind("./HeroJobInfo/JobTransferButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobTransferButton;
    [AutoBind("./HeroJobInfo/JobUnlockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobUnlockButton;
    [AutoBind("./Margin/TransferPanel/MasterButtonGroup/MasterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_masterButton;
    [AutoBind("./Margin/TransferPanel/MasterButtonGroup/AlreadyMaster/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_alreadyMasterValueText;
    [AutoBind("./MasterInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterInfoPanel;
    [AutoBind("./MasterInfoPanel/BGButtonImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_masterInfoPanelReturnButton;
    [AutoBind("./MasterInfoPanel/Detail/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterInfoPanelScrollViewContent;
    [AutoBind("./Prefab/JobInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterItemPrefab;
    [AutoBind("./HeroJobInfo/JobBaseInfo/JobLv/BreakBGImageGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvBgImagesGroup;
    [AutoBind("./HeroJobInfo/JobBaseInfo/JobLv/BreakIconImageGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobLvImagesGroup;
    [AutoBind("./HeroJobInfo/LearnedSkill/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedSkillIconImg;
    [AutoBind("./HeroJobInfo/LearnedSkill/SkillIconImage2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedSkillIconImg2;
    [AutoBind("./HeroJobInfo/SkillDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailPanel;
    [AutoBind("./HeroJobInfo/LearnedSoldier/GraphicParent/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedSoldierGraphicObj;
    [AutoBind("./HeroJobInfo/LearnedSoldier/GraphicParent/Graphic2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedSoldierGraphicObj2;
    [AutoBind("./HeroJobInfo/LearnedSoldier/GraphicParent/Graphic/GraphicBgImg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedSoldierGraphicBg;
    [AutoBind("./HeroJobInfo/LearnedSoldier/GraphicParent/Graphic2/Graphic2BgImg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedSoldierGraphicBg2;
    [AutoBind("./HeroJobInfo/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailPanel;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/TitleText2", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterBenefitTipText;
    [AutoBind("./HeroJobInfo/EquipMaster", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_equipMasterGameObject;
    [AutoBind("./HeroJobInfo/EquipMaster/Button", AutoBindAttribute.InitState.NotInit, false)]
    public Button m_equipMasterButton;
    [AutoBind("./HeroJobInfo/EquipMaster/Button", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_equipMasterButtonUIStateCtrl;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_quipMasterClickOpenPanelUIStateCtrl;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/BGButtonImage", AutoBindAttribute.InitState.NotInit, false)]
    public Button EquipMasterClickOpenPanelBGButton;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterHPValueText;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterATValueText;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterMagicValueText;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterDFValueText;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent/MDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterMDFValueText;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterDEXValueText;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/TitleText4_1", AutoBindAttribute.InitState.NotInit, false)]
    public Text EquipMasterSportSlotPropertyDesc;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent_Sports/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSportSlotProperty1;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent_Sports/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSportSlotProperty2;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent_Sports/Proprety3", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSportSlotProperty3;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent_Sports/Proprety4", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSportSlotProperty4;
    [AutoBind("./HeroJobInfo/EquipMaster/ClickOpen/Detail/ValueContent_Sports/Proprety5", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EquipMasterSportSlotProperty5;
    [AutoBind("./HeroJobInfo/LockedPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_transferLockPanel;
    [AutoBind("./HeroJobInfo/LockedPanel/BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_transferLockPanelReturnBg;
    [AutoBind("./HeroJobInfo/LockedPanel/QuesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_transferLockQuesText;
    [AutoBind("./HeroJobInfo/LockedPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_transferLockConfirmButton;
    [AutoBind("./HeroJobInfo/ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_transferConfirmPanel;
    [AutoBind("./HeroJobInfo/ConfirmPanel/JobNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confirmPanelJobNameText;
    [AutoBind("./HeroJobInfo/ConfirmPanel/GoldCostValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldCostValueText;
    [AutoBind("./HeroJobInfo/ConfirmPanel/MagicStoneCostValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_magicStoneCostValueText;
    [AutoBind("./HeroJobInfo/ConfirmPanel/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmBgButton;
    [AutoBind("./HeroJobInfo/ConfirmPanel/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmReturnButton;
    [AutoBind("./HeroJobInfo/ConfirmPanel/TransferButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmTransferButton;
    [AutoBind("./HeroJobInfo/ConfirmPanel/NoticeText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmTransferNoticeTextStateCtrl;
    [AutoBind("./HeroJobInfo/UnLockHeroJobPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unLockHeroJobPanel;
    [AutoBind("./HeroJobInfo/UnLockHeroJobPanel/JobNameText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unLockHeroJobPanelJobNameText;
    [AutoBind("./HeroJobInfo/UnLockHeroJobPanel/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unLockHeroJobConditionsContentObj;
    [AutoBind("./HeroJobInfo/Prefab/UnlcokConditionItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlcokJobConditionPrefab;
    [AutoBind("./HeroJobInfo/UnLockHeroJobPanel/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unLockHeroJobReturnButton;
    [AutoBind("./HeroJobInfo/UnLockHeroJobPanel/UnlockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unLockHeroJobUnlockButton;
    [AutoBind("./HeroJobInfo/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobTransferEffectSkipBtn;
    [AutoBind("./HeroJobInfo/BgEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_JobTransferEffectBgObj;
    [AutoBind("./HeroJobInfo/BgEffect/BlackImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_JobTransferEffectBlackImage;
    [AutoBind("./HeroJobInfo/WhiteImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_JobTransferEffectWhiteImage;
    [AutoBind("./HeroJobInfo/BgEffect/BgImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_JobTransferEffectBgImage;
    [AutoBind("./HeroJobInfo/BgEffect/ContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jobTransferEffectContinueButton;
    [AutoBind("./HeroJobInfo/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroATGraphicObj;
    [AutoBind("./HeroJobInfo/GroundImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobTransferGroundImage;
    [AutoBind("./HeroJobInfo/JobChangeSucceedText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobChangeSucceedText;
    [AutoBind("./HeroJobInfo/Graphic/UI_JobChange_1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroATGraphicJobChangeEffectObj1;
    [AutoBind("./HeroJobInfo/Graphic/UI_JobChange_2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroATGraphicJobChangeEffectObj2;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobUpgradeInfo;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/Hp/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTransHpText;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTransATText;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTransDFText;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTransMagicText;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTransMagicDFText;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jobTransDEXText;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/Hp/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobTransHpImage;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/AT/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobTransATImage;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/DF/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobTransDFImage;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/Magic/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobTransMagicImage;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/MagicDF/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobTransMagicDFImage;
    [AutoBind("./HeroJobInfo/JobUpgradeInfo/RightPanel/DEX/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_jobTransDEXImage;
    [AutoBind("./HeroJobInfo/LearnedPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanel;
    [AutoBind("./HeroJobInfo/LearnedPanel/CloseImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_learnedPanelCloseBtn;
    [AutoBind("./HeroJobInfo/LearnedPanel/Skill/ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_learnedPanelSkillCloseBtn;
    [AutoBind("./HeroJobInfo/LearnedPanel/Skill", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSkillPanel;
    [AutoBind("./HeroJobInfo/LearnedPanel/Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSoldierPanel;
    [AutoBind("./HeroJobInfo/LearnedPanel/Skill/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_learnedPanelSkillIcon;
    [AutoBind("./HeroJobInfo/LearnedPanel/Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_learnedPanelSoldierGraphicObj;
    [AutoBind("./HeroJobInfo/LearnedPanel/Skill/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_learnedPanelSkillName;
    [AutoBind("./HeroJobInfo/LearnedPanel/Soldier/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_learnedPanelSoldierName;
    private UISpineGraphic m_heroGraphic;
    private UISpineGraphic m_heroATGraphic;
    private UISpineGraphic m_soldierGraphicL;
    private UISpineGraphic m_soldierGraphicR;
    private UISpineGraphic m_learndSoldierGraphic;
    private Hero m_hero;
    private List<int> m_jobConnectionIds;
    private List<int> m_oldSkillInfoList;
    private List<int> m_oldSoldierInfoList;
    private HeroCharUIController m_heroCharUIController;
    private HeroPropertyComputer m_curComputer;
    private HeroPropertyComputer m_oldComputer;
    private ConfigDataJobConnectionInfo m_curJobConnectionInfo;
    private ConfigDataJobConnectionInfo m_oldJobConnectionInfo;
    private Dictionary<int, HeroJobCardItemUIController> m_jobConnectionIdToJobCardCtrlDict;
    private bool m_isJobLocked;
    private bool m_isNeedMagicStone;
    private bool m_isShowLearnSkill;
    private bool m_isShowLearnSoldier;
    private bool m_isJobHasTransfered;
    private bool m_isJobTransferContinue;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHero;
    private static DelegateBridge __Hotfix_UpdateViewInHeroJobTransfer;
    private static DelegateBridge __Hotfix_SetEquipMasterButtonState;
    private static DelegateBridge __Hotfix_SortJobConnectionIdByUISort;
    private static DelegateBridge __Hotfix_DrawLines;
    private static DelegateBridge __Hotfix_DynamicSetTransferLines;
    private static DelegateBridge __Hotfix_SetLineImageColor;
    private static DelegateBridge __Hotfix_ClearGridLayout;
    private static DelegateBridge __Hotfix_GetGridLayoutByRank;
    private static DelegateBridge __Hotfix_OnJobCardItemClick;
    private static DelegateBridge __Hotfix_SetHeroInfo;
    private static DelegateBridge __Hotfix_SetCanLearnSkills;
    private static DelegateBridge __Hotfix_SetCanLearnSoldiers;
    private static DelegateBridge __Hotfix_CloseHeroJobInfo;
    private static DelegateBridge __Hotfix_OnJobTransferButtonClick;
    private static DelegateBridge __Hotfix_CloseJobTransferConfirmPanel;
    private static DelegateBridge __Hotfix_CloseJobTransferLockedPanel;
    private static DelegateBridge __Hotfix_UpdateEquipMasterNormalPropertyList;
    private static DelegateBridge __Hotfix_UpdateSportSlotPropertyItem;
    private static DelegateBridge __Hotfix_UpdateEquipMasterSportSlotPropertyList;
    private static DelegateBridge __Hotfix_UpdateEquipMasterClickOpenPanel;
    private static DelegateBridge __Hotfix_ShowEquipMasterClickOpenPanel;
    private static DelegateBridge __Hotfix_CloseEquipMasterClickOpenPanel;
    private static DelegateBridge __Hotfix_OnJobUnlockButtonClick;
    private static DelegateBridge __Hotfix_OnUnLockHeroJobReturnButtonClick;
    private static DelegateBridge __Hotfix_OnUnLockHeroJobUnlockButtonClick;
    private static DelegateBridge __Hotfix_OnJobTransferConfirmButtonClick;
    private static DelegateBridge __Hotfix_GetCanLearnSkillAndSoldier;
    private static DelegateBridge __Hotfix_OnHeroJobTransferSucceed;
    private static DelegateBridge __Hotfix_OnSkipBtnClick;
    private static DelegateBridge __Hotfix_PlayJobTransfetEffectStep1;
    private static DelegateBridge __Hotfix_PlayJobTransfetEffectStep2;
    private static DelegateBridge __Hotfix_ShowLearnedSkillAndSoldier;
    private static DelegateBridge __Hotfix_CloseLearnedSkillPanel;
    private static DelegateBridge __Hotfix_CloseLearnedPanel;
    private static DelegateBridge __Hotfix_OnJobTransferEffectContinueButtonClick;
    private static DelegateBridge __Hotfix_SetTransferSucceedInfo;
    private static DelegateBridge __Hotfix_PropertyToString;
    private static DelegateBridge __Hotfix_SetImageState;
    private static DelegateBridge __Hotfix_OnMasterRewardButtonClick;
    private static DelegateBridge __Hotfix_CloseMasterInfoPanel;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_CreateSoldierGraphic;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_OnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_OnMagicStoneClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnAddGold;
    private static DelegateBridge __Hotfix_remove_EventOnAddGold;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobUnlock;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobUnlock;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobTransferFinishedForUserGuide;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobTransferFinishedForUserGuide;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobTransfer;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobTransfer;
    private static DelegateBridge __Hotfix_add_EventOnHeroJobCardClick;
    private static DelegateBridge __Hotfix_remove_EventOnHeroJobCardClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(Hero hero, ConfigDataJobConnectionInfo jci = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroJobTransfer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipMasterButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int SortJobConnectionIdByUISort(int j1, int j2)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DrawLines(List<int> Ids)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DynamicSetTransferLines(List<int> Ids)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLineImageColor(Image img, bool canTransfer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGridLayout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetGridLayoutByRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobCardItemClick(HeroJobCardItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroInfo(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCanLearnSkills(List<ConfigDataSkillInfo> skillInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCanLearnSoldiers(List<ConfigDataSoldierInfo> soldierInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeroJobInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobTransferButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseJobTransferConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseJobTransferLockedPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEquipMasterNormalPropertyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSportSlotPropertyItem(HeroJobSlotRefineryProperty property, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEquipMasterSportSlotPropertyList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEquipMasterClickOpenPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEquipMasterClickOpenPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipMasterClickOpenPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobUnlockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnLockHeroJobReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnLockHeroJobUnlockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobTransferConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetCanLearnSkillAndSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHeroJobTransferSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipBtnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayJobTransfetEffectStep1()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator PlayJobTransfetEffectStep2()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowLearnedSkillAndSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseLearnedSkillPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseLearnedPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobTransferEffectContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTransferSucceedInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string PropertyToString(int value0, int value1, Image img)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetImageState(Image img, int t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMasterRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseMasterInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(
      ref UISpineGraphic graphic,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      GameObject graphice,
      float scaleNum,
      string anim,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSoldierGraphic(
      ref UISpineGraphic graphic,
      ConfigDataSoldierInfo soldierInfo,
      GameObject graphice,
      float scales,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphic(ref UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMagicStoneClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddGold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnHeroJobUnlock
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroJobTransferFinishedForUserGuide
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnHeroJobTransfer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataJobConnectionInfo> EventOnHeroJobCardClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
