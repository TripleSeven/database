﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemLevelItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroAnthemLevelItemUIController : UIControllerBase
  {
    [AutoBind("./StageNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./AchievementCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementCountText;
    [AutoBind("./AchievementCount", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementCountButton;
    [AutoBind("./Locked", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockedButtonGo;
    [AutoBind("./StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./FirstClean", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardStateCtrl;
    [AutoBind("./FirstClean/RewardDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardDummy;
    private ConfigDataHeroAnthemLevelInfo m_heroAnthemLevelInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroAnthemLevelInfo;
    private static DelegateBridge __Hotfix_GetHeroAnthemLevelInfo;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_OnLockedButtonClick;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnAchievementCountButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStartButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnStartButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAchievementsClick;
    private static DelegateBridge __Hotfix_remove_EventOnAchievementsClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAnthemLevelInfo(ConfigDataHeroAnthemLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroAnthemLevelInfo GetHeroAnthemLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool locked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockedButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementCountButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroAnthemLevelItemUIController> EventOnStartButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<HeroAnthemLevelItemUIController> EventOnAchievementsClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
