﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreSelfChooseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class StoreSelfChooseUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./LayoutRoot/Prefab/Item", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemTemplate;
    [AutoBind("./LayoutRoot/ListScrollView /Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_content;
    private IConfigDataLoader m_configDataLoader;
    private List<GoodsUIController> m_goodUIController;
    private StoreListUIController m_storeListUIController;
    private StoreItemUIController m_storeItemUIController;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetSelfChooseItemID;
    private static DelegateBridge __Hotfix_OnBgClick;
    private static DelegateBridge __Hotfix_OnGoodsClick;
    private static DelegateBridge __Hotfix_OnFixedStoreItemBuyClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreSelfChooseUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(StoreListUIController storeListUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelfChooseItemID(StoreId storeID, StoreItemUIController storeItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBgClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoodsClick(GoodsUIController goodsUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFixedStoreItemBuyClick(
      StoreId fixedStoreID,
      int fixedStoreItemGoodsID,
      int goodsID,
      int selfChooseItemIndex,
      List<Goods> gainGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
