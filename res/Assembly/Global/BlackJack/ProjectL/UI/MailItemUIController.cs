﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MailItemUIController : UIControllerBase
  {
    public Mail CurrentMailInfo;
    private ProjectLPlayerContext m_playerCtx;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public Button MailItemButton;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_uiStateCtrl;
    [AutoBind("./MailTitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text MailTitleText;
    [AutoBind("./SendTimeText", AutoBindAttribute.InitState.NotInit, false)]
    public Text SendTimeText;
    [AutoBind("./SelectedImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image SelectedImage;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateMailInfo;
    private static DelegateBridge __Hotfix_SetStateIconSpriteByMailState;
    private static DelegateBridge __Hotfix_OnMailItemClick;
    private static DelegateBridge __Hotfix_add_EventOnMailItemButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnMailItemButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMailInfo(Mail mailInfo, bool isSelected = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetStateIconSpriteByMailState(Mail mailInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMailItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<MailItemUIController> EventOnMailItemButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
