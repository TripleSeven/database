﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionAcitivityExchangeConsumeItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionAcitivityExchangeConsumeItemUIController : UIControllerBase
  {
    [AutoBind("./IconDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_iconDummy;
    [AutoBind("./ValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tarCountText;
    [AutoBind("./ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_curCountText;
    [AutoBind("./ValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    private RewardGoodsUIController goodsCtrl;
    private static DelegateBridge __Hotfix_UpdateItem;

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(CollectionActivityItem item)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
