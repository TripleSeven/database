﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BoxItemInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BoxItemInfoUIController : UIControllerBase
  {
    [AutoBind("./ItemGoodsDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodsDummy;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./PresentImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentLogo;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetBoxItemInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public BoxItemInfoUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBoxItemInfo(Goods good, bool isPresent)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
