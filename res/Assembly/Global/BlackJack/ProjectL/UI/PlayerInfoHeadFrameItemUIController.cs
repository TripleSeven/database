﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoHeadFrameItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerInfoHeadFrameItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    public int HeadFrameId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateItemInfo;
    private static DelegateBridge __Hotfix_OnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_add_EventOnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_remove_EventOnSelectToggleValueChanged;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItemInfo(int headFrameId, bool isLock, bool isSelected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, PlayerInfoHeadFrameItemUIController> EventOnSelectToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
