﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleUIState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.UI
{
  public enum BattleUIState
  {
    None,
    SelectActionActor,
    Move,
    ExtraMove,
    SelectSkillTarget,
    ConfirmSkill,
    SelectTeleportPosition,
    WaitOtherPlayer,
    Regret,
    BattleReportRegret,
    BattleLiveRoom,
  }
}
