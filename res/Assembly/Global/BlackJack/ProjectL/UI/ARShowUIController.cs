﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARShowUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ARShowUIController : UIControllerBase
  {
    private ARShowUIController.UIState uiState;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_resourceContainer;
    [AutoBind("./PhotographButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_photographButton;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./SummonButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_summonButton;
    [AutoBind("./ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exitButton;
    [AutoBind("./PhotographPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_roleControlPanel;
    [AutoBind("./PhotographPanel/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/SideInToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charActionChoose;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Idle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charIdleToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Attack", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charAttackToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/SuperAttack", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charSuperAttackToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charMagicToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Death", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charDeathToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Sing", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charSingToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Faint", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charFaintToggle;
    [AutoBind("./PhotographPanel/Margin/ActionChoosenPanel/Content/Run", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_charRunToggle;
    [AutoBind("./PhotographPanel/DirectionToggleGroup/TureLeftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_turnLeftToggle;
    [AutoBind("./PhotographPanel/DirectionToggleGroup/TureRightButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_turnRightToggle;
    [AutoBind("./SharePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePanel;
    [AutoBind("./SharePanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBackgroundButton;
    [AutoBind("./SharePanel/ShareImage", AutoBindAttribute.InitState.NotInit, false)]
    private RawImage m_shareImage;
    [AutoBind("./SharePanel/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareReturnButton;
    [AutoBind("./SharePanel/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shareButtonDummy;
    [AutoBind("./SharePanel/SharePhotoDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./DevelopPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_developPanel;
    [AutoBind("./DevelopPanel/ScrollbarScale", AutoBindAttribute.InitState.NotInit, false)]
    private Scrollbar m_scaleScrollbar;
    [AutoBind("./DevelopPanel/ScrollbarDistance", AutoBindAttribute.InitState.NotInit, false)]
    private Scrollbar m_distanceScrollbar;
    [AutoBind("./DevelopPanel/TextScale", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scaleText;
    [AutoBind("./DevelopPanel/TextDistance", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_distanceText;
    private GameObject m_sharePhotoUIPanel;
    private Text m_playerNameText;
    private Text m_playerLvText;
    private Text m_serverNameText;
    private GameObject m_shareSubPanel;
    private Button m_weiBoButton;
    private Button m_weChatButton;
    private Button m_fbButton;
    private Button m_twitterButton;
    private Button m_instagramButton;
    private ARUITask m_task;
    private ARShowSceneController m_arShowSceneController;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private Texture2D m_captureTexture;
    private int m_arShowType;
    private int m_selectHeroID;
    public const int BattleShow = 1;
    public const int HeroDrawShow = 2;
    public const int TeamShow = 3;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_UpdateDeveloperMode;
    private static DelegateBridge __Hotfix_SetUITask;
    private static DelegateBridge __Hotfix_LoadData;
    private static DelegateBridge __Hotfix_SetARShowSceneController;
    private static DelegateBridge __Hotfix_OnPhotographClick;
    private static DelegateBridge __Hotfix_OnReturnClick;
    private static DelegateBridge __Hotfix_OnSummonClick;
    private static DelegateBridge __Hotfix_OnExitClick;
    private static DelegateBridge __Hotfix_OnCharActionClick;
    private static DelegateBridge __Hotfix_OnCharDirectionClick;
    private static DelegateBridge __Hotfix_OnShareReturnClick;
    private static DelegateBridge __Hotfix_OnFacebookClick;
    private static DelegateBridge __Hotfix_OnInstagramClick;
    private static DelegateBridge __Hotfix_OnTwitterClick;
    private static DelegateBridge __Hotfix_OnWeiBoClick;
    private static DelegateBridge __Hotfix_OnWeChatClick;
    private static DelegateBridge __Hotfix_OnShareBackgroundClick;
    private static DelegateBridge __Hotfix_OnScaleScrollBarValueChange;
    private static DelegateBridge __Hotfix_OnDistanceScrollBarValueChange;
    private static DelegateBridge __Hotfix_Photograph;
    private static DelegateBridge __Hotfix_ShareImage;
    private static DelegateBridge __Hotfix_CopyTexture2D;
    private static DelegateBridge __Hotfix_SaveTextureToPNG;

    private ARShowUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDeveloperMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUITask(ARUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetARShowSceneController(ARShowSceneController arShowSceneController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPhotographClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSummonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnExitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharActionClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharDirectionClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnShareReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFacebookClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInstagramClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnWeiBoClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnWeChatClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnShareBackgroundClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnScaleScrollBarValueChange(float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDistanceScrollBarValueChange(float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator Photograph()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected IEnumerator ShareImage(int sharePlatform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Texture2D CopyTexture2D(Texture2D texture)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveTextureToPNG(Texture2D texture, string path, string pngName)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum UIState
    {
      RecognizePanel,
      Summon,
      Show,
      Photo,
    }
  }
}
