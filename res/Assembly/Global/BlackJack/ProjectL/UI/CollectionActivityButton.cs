﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityButton : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./BGImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_imageGrey;
    [AutoBind("./InfoTextGroup/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./InfoTextGroup/TimeTextGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeGreyText;
    [AutoBind("./InfoTextGroup/TimeText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText2;
    [AutoBind("./InfoTextGroup/TimeTextGrey2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeGreyText2;
    private ConfigDataCollectionActivityInfo m_collectionActivityInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetCollectionActivityInfo;
    private static DelegateBridge __Hotfix_GetCollectionActivityInfo;
    private static DelegateBridge __Hotfix_OnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStartCollectionActivity;
    private static DelegateBridge __Hotfix_remove_EventOnStartCollectionActivity;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCollectionActivityInfo(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<CollectionActivityButton> EventOnStartCollectionActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
