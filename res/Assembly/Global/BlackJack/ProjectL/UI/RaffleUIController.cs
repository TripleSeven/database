﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RaffleUIController : UIControllerBase
  {
    public Action EventOnShowHelpButtonClick;
    public Action EvetnOnRewardPanelButtonClick;
    public Action EventOnDoRaffleButtonClick;
    public Action EventOnBgButtonClick;
    public Action EventOnLevelEffectBgButtonClick;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button BgButton;
    [AutoBind("./LayoutRoot/Detail/WatchRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RewardPanelButton;
    [AutoBind("./LayoutRoot/Detail/DescButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ShowHelpButton;
    [AutoBind("./LayoutRoot/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button DoRaffleButton;
    [AutoBind("./LayoutRoot/Detail/Tip", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController CostPanelStateCtrl;
    [AutoBind("./LayoutRoot/Detail/Tip/FreeCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text FreeCountValueText;
    [AutoBind("./LayoutRoot/Detail/Tip/Crystal", AutoBindAttribute.InitState.NotInit, false)]
    public Image CostImage;
    [AutoBind("./LayoutRoot/Detail/Tip/CrystalText", AutoBindAttribute.InitState.NotInit, false)]
    public Text CostTileText;
    [AutoBind("./LayoutRoot/Detail/Tip/CrystalText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text CostValueText;
    [AutoBind("./LayoutRoot/Detail/Tip/HaveTicketText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text HaveMoneyValueText;
    [AutoBind("./LayoutRoot/3DViewRect", AutoBindAttribute.InitState.NotInit, false)]
    public RectTransform ThreeDViewRect;
    [AutoBind("./EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController LevelEffectStateCtrl;
    [AutoBind("./EffectGroup/Special", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SpecialLevelStateCtrl;
    [AutoBind("./EffectGroup/First", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController FirstLevelStateCtrl;
    [AutoBind("./EffectGroup/Second", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SecondLevelStateCtrl;
    [AutoBind("./EffectGroup/Third", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController ThirdLevelStateCtrl;
    [AutoBind("./EffectGroup/ClickScreenContinue/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button LevelEffectBgButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateRafflePanel;
    private static DelegateBridge __Hotfix_Get3DViewRectTransfrom;
    private static DelegateBridge __Hotfix_ShowDrawRewardLevelEffect;
    private static DelegateBridge __Hotfix_GetLevelEffectNameByLevel;
    private static DelegateBridge __Hotfix_GetLevelEffectStateCtrlByLevel;
    private static DelegateBridge __Hotfix_UpdateCostPanelState;
    private static DelegateBridge __Hotfix_OnBgButtonClick;
    private static DelegateBridge __Hotfix_OnShowHelpButtonClick;
    private static DelegateBridge __Hotfix_OnRewardPanelButtonClick;
    private static DelegateBridge __Hotfix_OnDoRaffleButtonClick;
    private static DelegateBridge __Hotfix_OnLevelEffectBgButtonClick;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRafflePanel(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform Get3DViewRectTransfrom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDrawRewardLevelEffect(int rewardLevel, bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetLevelEffectNameByLevel(int rewardLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected CommonUIStateController GetLevelEffectStateCtrlByLevel(
      int rewardLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateCostPanelState(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnShowHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRewardPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnDoRaffleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnLevelEffectBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
