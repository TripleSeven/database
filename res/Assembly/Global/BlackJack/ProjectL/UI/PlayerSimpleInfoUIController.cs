﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerSimpleInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerSimpleInfoUIController : UIControllerBase
  {
    private BusinessCard m_playerInfo;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PanelRoot;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image PlayerIcon;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/PlayerIconImage/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    public Transform m_headFrameTransform;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/GoodImageButton", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_likeUIStateController;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    public Text LevelText;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerNameText;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/GoodCount", AutoBindAttribute.InitState.NotInit, false)]
    public Text LikeNumberText;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/WatchButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button WatchButton;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/FightButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button FightButton;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/FightGreyButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button FightGreyButton;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button DeleteFriendButton;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ChatButton;
    [AutoBind("./Detail/BGImage/FrameImage/PlayerPanel/GoodImageButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button LikeButton;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button AddFriendButton;
    [AutoBind("./Detail/BGImage/FrameImage/ButtonGroup/ShieldButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ShieldButton;
    [AutoBind("./Detail/BGImage/FrameImage/PeakArenaButton", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject peakArenaGameObject;
    [AutoBind("./Detail/BGImage/FrameImage/PeakArenaButton/Button", AutoBindAttribute.InitState.NotInit, false)]
    public Button peakArenaFightButton;
    [AutoBind("./Detail/BGImage/FrameImage/PeakArenaButton/Button", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController peakArenaFightButtonAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button BGButton;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PanelOpenOrCloseStateCtrl;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController PanelShowStateCtrl;
    [AutoBind("./Detail/BGImage/FrameImage/SociatyButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SociatyButtonGroupStateCtrl;
    [AutoBind("./Detail/BGImage/FrameImage/SociatyButtonGroup/SociatyButtonGroup/ChairmanRelieveButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ChairmanRelieveButton;
    [AutoBind("./Detail/BGImage/FrameImage/SociatyButtonGroup/SociatyButtonGroup/ChairmanMoveButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ChairmanMoveButton;
    [AutoBind("./Detail/BGImage/FrameImage/SociatyButtonGroup/SociatyButtonGroup/VChairmanButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button VChairmanButton;
    [AutoBind("./Detail/BGImage/FrameImage/SociatyButtonGroup/SociatyButtonGroup/VChairmanChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button VChairmanChangeButton;
    [AutoBind("./Detail/BGImage/FrameImage/SociatyButtonGroup/SociatyButtonGroup/ExpelButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ExpelButton;
    private StringTableId m_canNotFightStringTableId;
    private StringTableId m_canNotPeakArenaFightStringTableId;
    private const int MaxLevel = 60;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdatePlayerInfo;
    private static DelegateBridge __Hotfix_ShowOrHide;
    private static DelegateBridge __Hotfix_GetViewRect;
    private static DelegateBridge __Hotfix_GetPanelSize;
    private static DelegateBridge __Hotfix_SetPanelPosition;
    private static DelegateBridge __Hotfix_UpdateFightButtonState;
    private static DelegateBridge __Hotfix_UpdatePeakArenaFightButtonState;
    private static DelegateBridge __Hotfix_SetGuildButtonGroupStateByMode;
    private static DelegateBridge __Hotfix_SetGuildButtonGroupListener;
    private static DelegateBridge __Hotfix_OnExpelButtonClick;
    private static DelegateBridge __Hotfix_OnVChairmanChangeButtonClick;
    private static DelegateBridge __Hotfix_OnVChairmanButtonClick;
    private static DelegateBridge __Hotfix_OnChairmanMoveButtonClick;
    private static DelegateBridge __Hotfix_OnChairmanRelieveButtonClick;
    private static DelegateBridge __Hotfix_OnWatchCardButtonClick;
    private static DelegateBridge __Hotfix_OnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_OnPKButtonClick;
    private static DelegateBridge __Hotfix_OnPKGreyButton;
    private static DelegateBridge __Hotfix_OnPeakArenaFightClick;
    private static DelegateBridge __Hotfix_OnDeleteFriendButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendButtonClick;
    private static DelegateBridge __Hotfix_OnBlockButtonClick;
    private static DelegateBridge __Hotfix_OnLikeButtonClick;
    private static DelegateBridge __Hotfix_OnBGButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnWatchCardButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnWatchCardButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPKButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPKButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaPkButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaPkButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnDeleteFriendButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnDeleteFriendButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAddFriendButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAddFriendButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBlockButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnBlockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnLikeButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnLikeButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBGButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnBGButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnChairmanRelieveButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnChairmanRelieveButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnChairmanMoveButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnChairmanMoveButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnVChairmanChangeButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnVChairmanChangeButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnExpelButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnExpelButtonClick;
    private static DelegateBridge __Hotfix_get_PanelSize;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerInfo(
      BusinessCard playerInfo,
      bool isFriend,
      bool canSendLike,
      bool isInBattle,
      bool isInTeamRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHide(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Rect GetViewRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GetPanelSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPanelPosition(Vector3 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFightButtonState(
      BusinessCard playerInfo,
      bool isFriend,
      bool isInBattle,
      bool isInTeamRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaFightButtonState(
      BusinessCard playerInfo,
      bool isFriend,
      bool isInBattle,
      bool isInTeamRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuildButtonGroupStateByMode(string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGuildButtonGroupListener()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVChairmanChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVChairmanButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChairmanMoveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChairmanRelieveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPrivateChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPKButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPKGreyButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaFightClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDeleteFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLikeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<string> EventOnWatchCardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPrivateChatButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnPKButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnPeakArenaPkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BusinessCard> EventOnDeleteFriendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddFriendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnBlockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnLikeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBGButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnChairmanRelieveButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnChairmanMoveButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, bool> EventOnVChairmanChangeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnExpelButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Vector2 PanelSize
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
