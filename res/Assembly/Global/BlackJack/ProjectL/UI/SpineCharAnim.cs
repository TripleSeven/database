﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SpineCharAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class SpineCharAnim
  {
    public const string Idle_Normal = "idle_Normal";
    public const string Idle_Angry = "idle_Angry";
    public const string Idle_Injury = "idle_Injury";
    public const string Idle_Sad = "idle_Sad";
    public const string Idle_Shy = "idle_Shy";
    public const string Idle_Sigh = "idle_Sigh";
    public const string Idle_Smile = "idle_Smile";
    public const string Idle_Battle_Normal = "idle_Battle_Normal";
    public const string Idle_Battle_Mighty = "idle_Battle_Mighty";
    public const string Idle_Battle_Weak = "idle_Battle_Weak";
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public SpineCharAnim()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
