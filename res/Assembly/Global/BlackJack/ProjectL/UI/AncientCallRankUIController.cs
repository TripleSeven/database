﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRankUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallRankUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankState;
    [AutoBind("./Detail/RefreshTimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_refreshTimeText;
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./Detail/Filter/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectRankButton;
    [AutoBind("./Detail/Filter/Button/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterNameText;
    [AutoBind("./Detail/ToggleGroup/ALLToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_allToggle;
    [AutoBind("./Detail/ToggleGroup/SocialToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_socialToggle;
    [AutoBind("./Detail/Filter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sortTypeState;
    [AutoBind("./Detail/Filter/SortTypes/ScrollView/Viewport/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_filterContent;
    [AutoBind("./Detail/Filter/SortTypes/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterBgButton;
    [AutoBind("./Detail/TotalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_totalLoopScrollRect;
    [AutoBind("./Detail/TotalScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_totalObjectPool;
    [AutoBind("./Detail/SingleBossScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_bossLoopScrollRect;
    [AutoBind("./Detail/SingleBossScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_bossObjectPool;
    [AutoBind("./Detail/NoDetail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankingListNoDetailGo;
    [AutoBind("./Detail/BossRank", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_singleBossRankGameObject;
    [AutoBind("./Detail/AllRank", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allRankGameObject;
    [AutoBind("./TeamDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teamDetailPanelStateCtrl;
    [AutoBind("./TeamDetailPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamDetailPanelBackgroundButton;
    [AutoBind("./TeamDetailPanel/Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_teamDetailPanelHeroScrollRect;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teamDetailPanelPlayerIconImage;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_teamDetailPanelHeadFrameDummy;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamDetailPanelLevelText;
    [AutoBind("./TeamDetailPanel/Panel/PlayerInfo/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamDetailPanelPlayerNameText;
    [AutoBind("./TeamDetailPanel/Panel/HeroPower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_teamDetailPanelHeroPowerText;
    [AutoBind("./Prefab/BossName", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_togglePrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private AncientCallRankingListUITask m_ancientCallRankingListUITask;
    private AncientCallSelfRankAllUIController m_selfRankAllUIController;
    private AncientCallSelfRankSingleBossUIController m_selfRankSingleBossUIController;
    private GameObjectPool m_filterTogglePool;
    private Dictionary<int, Toggle> m_filterToggleDic;
    private int m_selectBossId;
    private RankingListInfo m_rankingListInfo;
    private Toggle m_selectSocialToggle;
    private const int AllBoss = 0;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnTotalBossPoolObjCreate;
    private static DelegateBridge __Hotfix_OnSingleBossPoolObjCreate;
    private static DelegateBridge __Hotfix_CloseTeamDetailPanel;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetRank;
    private static DelegateBridge __Hotfix_UpdateVeiwRank;
    private static DelegateBridge __Hotfix_SendAncientCallRankReq;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnTotalBossListItemFill;
    private static DelegateBridge __Hotfix_OnSingleBossListItemFill;
    private static DelegateBridge __Hotfix_OnBattleArraryClick;
    private static DelegateBridge __Hotfix_OnSelfBattleArrayClick;
    private static DelegateBridge __Hotfix_OnSelectRankClick;
    private static DelegateBridge __Hotfix_OnSelectSocialRelationClick;
    private static DelegateBridge __Hotfix_OnSelectRankToggle;
    private static DelegateBridge __Hotfix_OnFilterBgClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallRankUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnTotalBossPoolObjCreate(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSingleBossPoolObjCreate(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseTeamDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(List<int> bossIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRank(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateVeiwRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SendAncientCallRankReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTotalBossListItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnSingleBossListItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleArraryClick(RankingTargetPlayerInfo info, int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelfBattleArrayClick(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectRankClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectSocialRelationClick(Toggle selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectRankToggle(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterBgClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
