﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EternalShrineUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EternalShrineUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./SecretBlessing/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blessingButton;
    [AutoBind("./PlayerResource/Times/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailyChallengeCountText;
    [AutoBind("./PlayerResource/DailySweep/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dailySweepCountText;
    [AutoBind("./LevelList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelListScrollRect;
    [AutoBind("./BossInfoPanel/TitleGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_chapterNameText;
    [AutoBind("./BossInfoPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGo;
    [AutoBind("./RecommendPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroTagPanelGameObject;
    [AutoBind("./RecommendPanel/FrameImage/CampGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_heroTagPanelGroup;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/EternalShrineLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eternalShrineLevelListItemPrefab;
    [AutoBind("./Prefabs/CampIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroTagPrefab;
    private List<EternalShrineLevelListItemUIController> m_eternalShrineLevelListItems;
    private UISpineGraphic m_graphic;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_SetEternalShrine;
    private static DelegateBridge __Hotfix_SetAllEternalShrineLevelListItems;
    private static DelegateBridge __Hotfix_AddEternalShrineLevelListItem;
    private static DelegateBridge __Hotfix_SetScrollRectAtFirstIn;
    private static DelegateBridge __Hotfix_ClearEternalShrineLevelListItems;
    private static DelegateBridge __Hotfix_EternalShrineHeroTagUIController_OnClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnAddTicketButtonClick;
    private static DelegateBridge __Hotfix_OnBlessingButtonClick;
    private static DelegateBridge __Hotfix_EternalShrineLevelListItem_OnStartButtonClick;
    private static DelegateBridge __Hotfix_EternalShrineLevelListItem_OnLevelRaidComplete;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_SetDailyChallengeCount;
    private static DelegateBridge __Hotfix_SetDailySweepCount;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnAddTicket;
    private static DelegateBridge __Hotfix_remove_EventOnAddTicket;
    private static DelegateBridge __Hotfix_add_EventOnStartEternalShrineLevel;
    private static DelegateBridge __Hotfix_remove_EventOnStartEternalShrineLevel;
    private static DelegateBridge __Hotfix_add_EventOnLevelRaidComplete;
    private static DelegateBridge __Hotfix_remove_EventOnLevelRaidComplete;

    [MethodImpl((MethodImplOptions) 32768)]
    private EternalShrineUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEternalShrine(ConfigDataEternalShrineInfo eternalShrineInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAllEternalShrineLevelListItems(List<ConfigDataEternalShrineLevelInfo> levelInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEternalShrineLevelListItem(
      ConfigDataEternalShrineLevelInfo levelnfo,
      bool opened,
      bool blessing)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScrollRectAtFirstIn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearEternalShrineLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineHeroTagUIController_OnClick(EternalShrineHeroTagUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBlessingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineLevelListItem_OnStartButtonClick(
      EternalShrineLevelListItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EternalShrineLevelListItem_OnLevelRaidComplete(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(ConfigDataEternalShrineInfo eternalShrineInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyChallengeCount(int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailySweepCount(int count, int maxNum)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataEternalShrineLevelInfo> EventOnStartEternalShrineLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnLevelRaidComplete
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
