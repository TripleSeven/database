﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ItemPay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class ItemPay : IDisposable
  {
    private static List<PDSDKGood> m_pdsdkGoods = new List<PDSDKGood>();
    private ProjectLPlayerContext m_playerContext;
    private ClientConfigDataLoader m_configdataLoader;
    private string m_curBuyingGoodsId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_PDSDKRequestGoods;
    private static DelegateBridge __Hotfix_PDSDKGetGoods;
    private static DelegateBridge __Hotfix_OnPDSDKReqGoodsAck;
    private static DelegateBridge __Hotfix_GiftStoreBuy;
    private static DelegateBridge __Hotfix_SendGiftStoreBuyStoreItemReq;
    private static DelegateBridge __Hotfix_SendGiftStoreAppleSubscribeItemReq;
    private static DelegateBridge __Hotfix_PDSDKRequestBuyGoods;
    private static DelegateBridge __Hotfix_OnPDSDKPayCancel;
    private static DelegateBridge __Hotfix_OnPDSDKPayFailed;
    private static DelegateBridge __Hotfix_OnPDSDKPaySuccess;
    private static DelegateBridge __Hotfix_SendStoreItemCancelBuyReq;
    private static DelegateBridge __Hotfix_SendCheckOnlineReq;
    private static DelegateBridge __Hotfix_ClearBuyingGoodsCache;
    private static DelegateBridge __Hotfix_PlayerContex_OnGiftStoreBuyNtf;
    private static DelegateBridge __Hotfix_CheckOrderReward;
    private static DelegateBridge __Hotfix_SendPullOrderRewardReq;
    private static DelegateBridge __Hotfix_ProcessOrderRewards;
    private static DelegateBridge __Hotfix_ProcessOrderRewardsSub;
    private static DelegateBridge __Hotfix_HandleOneGiftStoreBuyItems;
    private static DelegateBridge __Hotfix_add_EventOnPaySuccess;
    private static DelegateBridge __Hotfix_remove_EventOnPaySuccess;
    private static DelegateBridge __Hotfix_add_EventOnRequestGoodsListSuccess;
    private static DelegateBridge __Hotfix_remove_EventOnRequestGoodsListSuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    public ItemPay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDKRequestGoods(Action<List<PDSDKGood>> OnFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PDSDKGood> PDSDKGetGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void OnPDSDKReqGoodsAck(bool isSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GiftStoreBuy(GiftStoreItem giftStoreItem, int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGiftStoreBuyStoreItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGiftStoreAppleSubscribeItemReq(
      StoreType storeType,
      int goodsId,
      int number,
      PDSDKGoodType pdsdkGoodType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDKRequestBuyGoods(string goodID, PDSDKGoodType goodType, int number)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPDSDKPayCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPDSDKPayFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPDSDKPaySuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendStoreItemCancelBuyReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendCheckOnlineReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBuyingGoodsCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContex_OnGiftStoreBuyNtf(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckOrderReward(Action OnGetReward = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendPullOrderRewardReq(
      string orderId,
      Action successedCallback = null,
      Action failedCallback = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ProcessOrderRewards(
      List<Goods> rewardList,
      Action successedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ProcessOrderRewardsSub(
      List<Goods> rewardsDisplay,
      List<Goods> rewardList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleOneGiftStoreBuyItems(List<Goods> rewards, Action successedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPaySuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static event Action<List<PDSDKGood>> EventOnRequestGoodsListSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
