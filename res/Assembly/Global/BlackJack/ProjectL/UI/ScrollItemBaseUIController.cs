﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScrollItemBaseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class ScrollItemBaseUIController : UIControllerBase
  {
    public Action<UIControllerBase> EventOnUIItemNeedFill;
    public Action<UIControllerBase> EventOnUIItemClick;
    public Action<UIControllerBase> EventOnUIItemDoubleClick;
    public Action<UIControllerBase> EventOnUIItem3DTouch;
    public int m_itemIndex;
    public UIControllerBase m_ownerCtrl;
    private ThreeDTouchEventListener m_3dTouchEventTrigger;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_ScrollCellIndex;
    private static DelegateBridge __Hotfix_OnScrollItemClick;
    private static DelegateBridge __Hotfix_OnScrollItemDoubleClick;
    private static DelegateBridge __Hotfix_OnScrollItem3DTouch;
    private static DelegateBridge __Hotfix_get_ItemIndex;
    private static DelegateBridge __Hotfix_set_ItemIndex;

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(UIControllerBase ownerCtrl, bool isCareItemClick = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ScrollCellIndex(int itemIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItemDoubleClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScrollItem3DTouch()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ItemIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
