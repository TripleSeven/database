﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.InfinityGridLayoutGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  [RequireComponent(typeof (GridLayoutGroup))]
  [RequireComponent(typeof (ContentSizeFitter))]
  public class InfinityGridLayoutGroup : MonoBehaviour
  {
    [SerializeField]
    private int m_minAmount;
    [SerializeField]
    private int amount;
    private RectTransform rectTransform;
    private GridLayoutGroup gridLayoutGroup;
    private ContentSizeFitter contentSizeFitter;
    private ScrollRect scrollRect;
    private List<RectTransform> children;
    private Vector2 startPosition;
    public InfinityGridLayoutGroup.UpdateChildrenCallbackDelegate updateChildrenCallback;
    private int realIndex;
    private int realIndexUp;
    private bool hasInit;
    private Vector2 gridLayoutSize;
    private Vector2 gridLayoutPos;
    private Dictionary<Transform, Vector2> childsAnchoredPosition;
    private Dictionary<Transform, int> childsSiblingIndex;
    private static DelegateBridge __Hotfix_get_MinAmount;
    private static DelegateBridge __Hotfix_set_MinAmount;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_InitChildren;
    private static DelegateBridge __Hotfix_ScrollCallback;
    private static DelegateBridge __Hotfix_UpdateChildren;
    private static DelegateBridge __Hotfix_UpdateChildrenCallback;
    private static DelegateBridge __Hotfix_SetAmount;

    [MethodImpl((MethodImplOptions) 32768)]
    public InfinityGridLayoutGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    public int MinAmount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator InitChildren()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ScrollCallback(Vector2 data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildren()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChildrenCallback(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAmount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate void UpdateChildrenCallbackDelegate(int index, Transform trans);
  }
}
