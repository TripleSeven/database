﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SelfSelectedSkinBoxUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SelfSelectedSkinBoxUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_selfSelectedSkinBoxAnimation;
    [AutoBind("./FacelifPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmButtonAnimation;
    [AutoBind("./FacelifPanel/Detail/Prefab/SkinItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinItem;
    [AutoBind("./FacelifPanel/Detail/SkinScroll", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_skinScrollRect;
    [AutoBind("./FacelifPanel/Detail/SkinScroll/Viewport", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_skinScrollViewportTrans;
    [AutoBind("./FacelifPanel/Detail/SkinScroll/Viewport/SkinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_skinGroupTrans;
    [AutoBind("./FacelifPanel/Detail/SkinScroll/Viewport/SkinGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GridLayoutGroup m_skinGroupGrid;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/InfoGroup/CountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectSkinCount;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/InfoGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectSkinTotalCountLabelText;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skinPanelAnimation;
    [AutoBind("./FacelifPanel/Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/InfoGroup/CountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectCountText;
    [AutoBind("./FacelifPanel/Detail/ConfirmPanel/BottomText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skinBoxDescText;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/JobScrollView/Viewport/JobGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jobGroupDummy;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_smallHeroDummy;
    [AutoBind("./FacelifPanel/Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigHeroDummy;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private SelfSelectedSkinBoxUITask.SelfSelectedSkinBoxMode m_selfSelectedSkinBoxMode;
    private UISpineGraphic m_smallHeroGraphic;
    private UISpineGraphic m_bigHeroGraphic;
    private List<SelfSelectedSkinBoxItemUIController> m_skinItemUIControllerList;
    private SelfSelectedSkinBoxUITask m_selfSelectedSkinBoxUITask;
    private ConfigDataItemInfo m_itemInfo;
    private float m_defaultSpace;
    private int m_heroSkinID;
    private const int maxDisplaySkinCount = 4;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetViewMode;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_RefreshConfirmState;
    private static DelegateBridge __Hotfix_SetSubItemNoneState;
    private static DelegateBridge __Hotfix_SetSubItemSelectStete;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnConfirmClick;
    private static DelegateBridge __Hotfix_OnSkinItemStateChange;
    private static DelegateBridge __Hotfix_OnItemToggleSelect;
    private static DelegateBridge __Hotfix_OnJobClick;
    private static DelegateBridge __Hotfix_add_EventOnConfirmClick;
    private static DelegateBridge __Hotfix_remove_EventOnConfirmClick;
    private static DelegateBridge __Hotfix_add_EventOnCancelClick;
    private static DelegateBridge __Hotfix_remove_EventOnCancelClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelfSelectedSkinBoxUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(
      SelfSelectedSkinBoxUITask selfSelectedSkinBoxUITask,
      int selfSelectedSkinBoxID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetViewMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Refresh(int heroSkinID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshConfirmState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSubItemNoneState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSubItemSelectStete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateSpineGraphic(
      ref UISpineGraphic graphic,
      string assetName,
      GameObject parent,
      int direction,
      Vector2 offset,
      float scale,
      List<ReplaceAnim> anims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinItemStateChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemToggleSelect(int heroSkinID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobClick(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<int>> EventOnConfirmClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancelClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
