﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaBattlePrepareUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaBattlePrepareUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGCover", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageBGGameObject;
    [AutoBind("./RightTop/PauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pauseButton;
    [AutoBind("./Panel/FirstOrAfter", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_orderUIStateController;
    [AutoBind("./Panel/ShowFirst", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_orderFirstGameObject;
    [AutoBind("./Panel/ShowAfter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_orderAfterGameObject;
    [AutoBind("./Panel/BattleReportShowFirstAndAfter", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportOrderUIStateController;
    [AutoBind("./Panel/AutoToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_bpStageAutoToggle;
    [AutoBind("./Panel/PrepareState/DetailGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTipUIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure/EffecctImage/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageTipText;
    [AutoBind("./Panel/PrepareState/DetailGroup/TextAndFigure/EffecctImage/FirstAndAfterImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTipFlagUIStateController;
    [AutoBind("./Panel/PrepareState/DetailGroup/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bpStageConfirmButton;
    [AutoBind("./Panel/PrepareState/DetailGroup/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageConfirmButtonUIStateController;
    [AutoBind("./Panel/PrepareState/ActivePlayer/MyActive", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageMyActiveUIStateController;
    [AutoBind("./Panel/PrepareState/ActivePlayer/EnemyActive", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageEnemyActiveUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTimeUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageTimeDetailUIStateController;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail/SurplusTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageTimeText;
    [AutoBind("./Panel/PrepareState/TimeGroup/Detail/ReserveTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageTimeText2;
    [AutoBind("./ArenaBeginEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareCompleteCountdownEffectUIStateController;
    [AutoBind("./ArenaBeginEffectNoNum", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_prepareCompleteNoCountdownEffectUIStateController;
    [AutoBind("./WatchPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battleLivePanelGameObject;
    [AutoBind("./WatchPanel/WatchTip", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleLiveTipUIStateController;
    [AutoBind("./WatchPanel/WatchTip/WaitBegin/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleLiveWaitBeginTimeText;
    [AutoBind("./WatchPanel/DanmakuToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_danmakuToggle;
    [AutoBind("./WatchPanel/DanmakuToggle/InputWordButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputWordButton;
    [AutoBind("./WatchPanel/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_danmakuStateCtrl;
    [AutoBind("./WatchPanel/Danmaku", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuInputBackButton;
    [AutoBind("./WatchPanel/Danmaku/Input/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_danmakuInputField;
    [AutoBind("./WatchPanel/Danmaku/Input/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_danmakuSendButton;
    [AutoBind("./BanHeroMarginPanel", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_bpStagePanelMarginTransform;
    [AutoBind("./BanHeroMarginPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStagePanelUIStateController;
    [AutoBind("./BanHeroMarginPanel/LeftGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageLeftPanelGameObject;
    [AutoBind("./BanHeroMarginPanel/RightGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageRightPanelGameObject;
    [AutoBind("./BanHeroMarginPanel/MyKickOutHero", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageBanHeroUIStateController;
    [AutoBind("./BanHeroMarginPanel/MyKickOutHero/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_bpStageBanHeroGroupTransform;
    [AutoBind("./BanHeroMarginPanel/AttackButton/ButtonImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./BanHeroMarginPanel/AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageAttackButtonUIStateController;
    [AutoBind("./BanHeroMarginPanel/BattleMapPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bpStageMapPreviewUIStateController;
    [AutoBind("./BanHeroMarginPanel/BattleMapPanel/Text/", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bpStageMapPreviewName;
    [AutoBind("./BanHeroMarginPanel/BattleMapPanel/Image/", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bpStageMapPreviewImage;
    [AutoBind("./BanHeroMarginPanel/ExpressionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_expressionButton;
    [AutoBind("./BanHeroMarginPanel/ExpressionButton/ExpressionGroupDummy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_expressionGroupUIStateController;
    [AutoBind("./BanHeroMarginPanel/ExpressionButton/ExpressionGroupDummy/BGMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_expressionGroupBGButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportButtonGroupUIStateController;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportSkipButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/StartAndPauseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportPauseOrPlayButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/LastStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportBackwardButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/LastStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportBackwardUIStateController;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/NextStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleReportForwardButton;
    [AutoBind("./BanHeroMarginPanel/RepeatedlyButtonGroup/NextStepButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battleReportForwardUIStateController;
    [AutoBind("./ExpressionMargin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_expressionMargionTransform;
    [AutoBind("./ExpressionMargin/Chat1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageLeftChatGameObject;
    [AutoBind("./ExpressionMargin/Chat2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bpStageRightChatGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroItemPrefab;
    private PeakArenaBPStagePanelUIController[] m_bpStagePanelUIControllers;
    private PeakArenaBPStageChatUIController[] m_bpStageChatUIControllers;
    private CommonUIStateController[] m_bpStageTipFigureUIStateControllers;
    private BigExpressionController m_bigExpressionCtrl;
    private bool m_isClosingBpStageTime;
    private TimeSpan m_closingBpStageCountdown1;
    private TimeSpan m_closingBpStageCountdown2;
    private bool m_closingBpStageCountdownIsReserve;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowBpStagteOpen;
    private static DelegateBridge __Hotfix_ShowOrder;
    private static DelegateBridge __Hotfix_SetBPStagePanelFinished;
    private static DelegateBridge __Hotfix_ShowPrepareCompleteCountdown;
    private static DelegateBridge __Hotfix_ShowBpStageMapPreview;
    private static DelegateBridge __Hotfix_HideBpStageMapPreview;
    private static DelegateBridge __Hotfix_ShowExpressionButton;
    private static DelegateBridge __Hotfix_GetBPStagePanelUIController;
    private static DelegateBridge __Hotfix_SetPlayer_0;
    private static DelegateBridge __Hotfix_SetPlayer_1;
    private static DelegateBridge __Hotfix_SetPlayerUnknown;
    private static DelegateBridge __Hotfix_SetPlayerWinCount;
    private static DelegateBridge __Hotfix_SetPlayerStatus;
    private static DelegateBridge __Hotfix_ShowPlayerBigExpression;
    private static DelegateBridge __Hotfix_ActiveBpStagePanel;
    private static DelegateBridge __Hotfix_ShowBpStagePanelActiveArrow;
    private static DelegateBridge __Hotfix_SetBpStageHeros;
    private static DelegateBridge __Hotfix_AddHeros;
    private static DelegateBridge __Hotfix_ShowBpStageActivePlayerTip;
    private static DelegateBridge __Hotfix_HideBpStageActivePlayerTip;
    private static DelegateBridge __Hotfix_ShowBpStageTip;
    private static DelegateBridge __Hotfix_SetBPStageTip;
    private static DelegateBridge __Hotfix_HideBpStageTip;
    private static DelegateBridge __Hotfix_SetBpStageTipFigureCount;
    private static DelegateBridge __Hotfix_ShowBpStageConfirmButton;
    private static DelegateBridge __Hotfix_SetBpStageConfirmButtonState;
    private static DelegateBridge __Hotfix_HideBpStageEnemyActiveDesc;
    private static DelegateBridge __Hotfix_ShowPickHeroEffects;
    private static DelegateBridge __Hotfix_ShowBpStageWaitPickHeroEffect;
    private static DelegateBridge __Hotfix_ShowBanMyHeroEffects;
    private static DelegateBridge __Hotfix_SetAttackButtonReadyState;
    private static DelegateBridge __Hotfix_ShowBpStageCountdown;
    private static DelegateBridge __Hotfix_HideBpStageCountdown;
    private static DelegateBridge __Hotfix_SetBpStageCountdown_1;
    private static DelegateBridge __Hotfix_SetBpStageCountdown_0;
    private static DelegateBridge __Hotfix_ResetBattleReport;
    private static DelegateBridge __Hotfix_ShowBattleReportButtonGroup;
    private static DelegateBridge __Hotfix_SetBattleReportPaused;
    private static DelegateBridge __Hotfix_SetBattleReportButtonStatus;
    private static DelegateBridge __Hotfix_ShowBattleReportOrder;
    private static DelegateBridge __Hotfix_ShowBattleReportMapPreview;
    private static DelegateBridge __Hotfix_HideBattleReportMapPreview;
    private static DelegateBridge __Hotfix_ShowBattleReportTip;
    private static DelegateBridge __Hotfix_ShowBattleReportBanHeroEffects;
    private static DelegateBridge __Hotfix_ShowBattleReportPrepareComplete;
    private static DelegateBridge __Hotfix_ShowBattleLivePanel;
    private static DelegateBridge __Hotfix_ShowBattleLiveWaitPlayer;
    private static DelegateBridge __Hotfix_ShowBattleLiveWaitBeginCountdown;
    private static DelegateBridge __Hotfix_HideBattleLiveTip;
    private static DelegateBridge __Hotfix_ShowBattleLiveBpStageCountdown;
    private static DelegateBridge __Hotfix_SetBattleLiveBpStageCountdown;
    private static DelegateBridge __Hotfix_ClearDanmakuUIInputField;
    private static DelegateBridge __Hotfix_IsBpStageAuto;
    private static DelegateBridge __Hotfix_OnPauseButtonClick;
    private static DelegateBridge __Hotfix_OnExpressionButtonClick;
    private static DelegateBridge __Hotfix_OnBigExpressionClick;
    private static DelegateBridge __Hotfix_OnBigExpressionBGClick;
    private static DelegateBridge __Hotfix_ShowExpressionGroup;
    private static DelegateBridge __Hotfix_OnBpStageAutoToggleValueChanged;
    private static DelegateBridge __Hotfix_OnBPStageConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnAttackButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportSkipButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportPauseOrPlayButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportBackwardButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportForwardButtonClick;
    private static DelegateBridge __Hotfix_OnDanmakuToggleValueChanged;
    private static DelegateBridge __Hotfix_OnDanmakuInputWordButtonClick;
    private static DelegateBridge __Hotfix_OnDanmakuInputBackButtonClick;
    private static DelegateBridge __Hotfix_OnDanmakuSendButtonClick;
    private static DelegateBridge __Hotfix_PeakArenaBanPickPanelUIController_OnHeroItemClick;
    private static DelegateBridge __Hotfix_add_EventOnPauseBattle;
    private static DelegateBridge __Hotfix_remove_EventOnPauseBattle;
    private static DelegateBridge __Hotfix_add_EventOnBpStageConfirm;
    private static DelegateBridge __Hotfix_remove_EventOnBpStageConfirm;
    private static DelegateBridge __Hotfix_add_EventOnBpStageAuto;
    private static DelegateBridge __Hotfix_remove_EventOnBpStageAuto;
    private static DelegateBridge __Hotfix_add_EventOnAttack;
    private static DelegateBridge __Hotfix_remove_EventOnAttack;
    private static DelegateBridge __Hotfix_add_EventOnBpStageHeroClick;
    private static DelegateBridge __Hotfix_remove_EventOnBpStageHeroClick;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportSkip;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportSkip;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportPause;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportPause;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportPlay;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportPlay;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportBackward;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportBackward;
    private static DelegateBridge __Hotfix_add_EventOnBattleReportForward;
    private static DelegateBridge __Hotfix_remove_EventOnBattleReportForward;
    private static DelegateBridge __Hotfix_add_EventOnSendDanmaku;
    private static DelegateBridge __Hotfix_remove_EventOnSendDanmaku;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStagteOpen(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrder(bool isFirst)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBPStagePanelFinished(int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPrepareCompleteCountdown(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageMapPreview(ConfigDataPeakArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageMapPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExpressionButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PeakArenaBPStagePanelUIController GetBPStagePanelUIController(
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(int playerIndex, BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(int playerIndex, PeakArenaBattleReportPlayerSummaryInfo player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerUnknown(int playerIndex, bool unknown)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerWinCount(int playerIndex, int winCount, bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerStatus(int playerIndex, PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerBigExpression(int playerIndex, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActiveBpStagePanel(int playerIndex, bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStagePanelActiveArrow(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBpStageHeros(int playerIndex, BPStage bpStage, bool showJob, bool selectable = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeros(
      PeakArenaBPStagePanelUIController ctrl,
      List<BPStageHeroSetupInfo> heroSetups,
      PeakArenaBPStageHeroItemState state,
      bool showJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageActivePlayerTip(
      int activePlayerIndex,
      int myPlayerIndex,
      BPStageCommandType bpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageActivePlayerTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageTip(
      string txt,
      int figureCount,
      int selectedFigureCount,
      BPStageCommandType bpType,
      bool isTurnChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBPStageTip(
      string text,
      int figureCount,
      int selectedFigureCount,
      BPStageCommandType bpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBpStageTipFigureCount(int count, int selectedCount, string selectedStateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBpStageConfirmButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBpStageConfirmButtonState(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBpStageEnemyActiveDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPickHeroEffects(int playerIndex, List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageWaitPickHeroEffect(int playerIndex, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanMyHeroEffects(List<BPStageHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackButtonReadyState(bool isReady)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBpStageCountdown(bool isTurnChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBpStageCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBpStageCountdown(TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBpStageCountdown(TimeSpan ts, bool isReserve)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportButtonGroup(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportPaused(bool isPaused)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleReportButtonStatus(bool canBackward, bool canForward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportMapPreview(ConfigDataPeakArenaBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleReportMapPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportTip(int activePlayerIndex, string txt, BPStageCommandType bpType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportBanHeroEffects(
      int playerIndex,
      List<BPStageHeroSetupInfo> heroSetups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleReportPrepareComplete(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLivePanel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveWaitPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveWaitBeginCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleLiveTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleLiveBpStageCountdown(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleLiveBpStageCountdown(int playerIndex, TimeSpan ts, TimeSpan ts2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearDanmakuUIInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBpStageAuto()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPauseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionClick(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBigExpressionBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowExpressionGroup(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBpStageAutoToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBPStageConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportPauseOrPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportBackwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportForwardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputWordButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuInputBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDanmakuSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBanPickPanelUIController_OnHeroItemClick(
      PeakArenaBpStageHeroItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPauseBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBpStageConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnBpStageAuto
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaBpStageHeroItemUIController> EventOnBpStageHeroClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPause
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportPlay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportBackward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBattleReportForward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSendDanmaku
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
