﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentForgeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipmentForgeUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/FilterToggles/EnhanceToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_enhanceToggle;
    [AutoBind("./Margin/FilterToggles/BreakToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_breakToggle;
    [AutoBind("./Margin/FilterToggles/EnchantmentToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_enchantmentToggle;
    [AutoBind("./Margin/FilterToggles/EnchantmentToggle/EnchantLockMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantLockMaskButton;
    [AutoBind("./PlayerResource/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./PlayerResource/Gold/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddButton;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./EquipmentList/ListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemRoot;
    [AutoBind("./EquipmentList/EnchantmentFilter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentFilter;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentfilterSortButton;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentfilterSortTypeText;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentfilterSortTypesGo;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentfilterSortTypesGridLayout;
    [AutoBind("./EquipmentList/EnchantmentFilter/SortTypes/AllButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentfilterSortTypesAllToggle;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceAndBreakFilter;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortButton;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterSortTypeText;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortOrderButton;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGo;
    [AutoBind("./EquipmentList/EnhanceAndBreakFilter/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGridLayout;
    [AutoBind("./EquipmentList/NotBreakItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listNoBreakItemGo;
    [AutoBind("./EquipmentList/NoEnchantmentText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listNoEnchantmentItemGo;
    [AutoBind("./EquipmentList/Title", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_listTitleStateCtrl;
    [AutoBind("./EquipmentList/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listLongPressTipGo;
    [AutoBind("./Desc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descGo;
    [AutoBind("./Desc/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descReturnBgButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descLockButtonStateCtrl;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTitleText;
    [AutoBind("./Desc/Lay/FrameImage/Top/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Desc/Lay/FrameImage/Top/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Desc/Lay/FrameImage/Top/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Desc/Lay/FrameImage/Top/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descLvText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descExpText;
    [AutoBind("./Desc/Lay/FrameImage/Top/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descEquipLimitContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillContent;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descSkillContentStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descUnlockCoditionText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillNameText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillLvText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillOwnerText;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillOwnerBGImage;
    [AutoBind("./Desc/Lay/FrameImage/Button/SkillContent/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillDescText;
    [AutoBind("./Desc/Lay/FrameImage/Button/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descNotEquipSkillTip;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropContent;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropATGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropATValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropHPGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropHPValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagiccGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagicDFGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicDFValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDexGo;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDexValueText;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceTitleText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceIcon;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceIconBg;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceIconSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/Lv/CurLv", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceCurLv;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/Lv/CurLv1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceCurLv1;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/Lv/MaxLV", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceMaxLV;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceProgressImage;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ProgressGroup/WillGetExpImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enhanceWillGetExpImage;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ExpValueText/CurValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceExpCurValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ExpValueText/AddValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceExpAddValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/ExpValueText/NextLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceExpNextLvText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/MaterialsContent/MaterialsGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceMaterialsContent;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/EnhanceGold/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceGoldText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/EnhanceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceButton;
    [AutoBind("./EnhanceSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceSuccessEffectPanel;
    [AutoBind("./EnhanceSuccessEffectPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceSuccessEffectPanelCloseButton;
    [AutoBind("./ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceConfirmPanel;
    [AutoBind("./ConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceConfirmButton;
    [AutoBind("./ConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceCancelButton;
    [AutoBind("./ConfirmPanel/Detail/Tip1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceConfirmPanelTip1;
    [AutoBind("./ConfirmPanel/Detail/Tip2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceConfirmPanelTip2;
    [AutoBind("./EnhaneConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhanceItemSRConfirmPanel;
    [AutoBind("./EnhaneConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceItemSRConfirmButton;
    [AutoBind("./EnhaneConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceItemSRCancelButton;
    [AutoBind("./EnhaneConfirmPanel/Detail/Tip2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhanceItemSRTip;
    [AutoBind("./IntensifySuccessPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_intensifySuccessPanelStateCtrl;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_intensifySuccessPanelInfoStateCtrl;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/Equipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_intensifySuccessPanelInfoIconBg;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/Equipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_intensifySuccessPanelInfoIcon;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/Equipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_intensifySuccessPanelInfoStarGroup;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup/CurLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelCurLvText;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup/NextLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelNextLvText;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/LvGroup/SkillNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelSkillNameText;
    [AutoBind("./IntensifySuccessPanel/BreakInfoGroup/DescTextScrollView/Mask/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_intensifySuccessPanelSkillDescText;
    [AutoBind("./IntensifySuccessPanel/BlackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_intensifySuccessPanelBlackButton;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropGroupGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropHpGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/HP/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropHpOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/HP/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropHpNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropATGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/AT/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropATOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/AT/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropATNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropDFGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DF/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDFOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DF/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDFNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropMagicGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/Magic/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/Magic/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropMagicDFGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/MagicDF/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicDFOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/MagicDF/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropMagicDFNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DEX", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enhancePropDEXGo;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DEX/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDEXOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/PropGroup/DEX/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropDEXNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enhancePropSkillStateCtrl;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillNameText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/OldValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillOldValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/NewValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillNewValueText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/PropContent/SkillGroup/SkillChangeItem/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enhancePropSkillConditionText;
    [AutoBind("./EquipmentInfo/EquipmentEnhance/FastAddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enhanceFastAddButton;
    [AutoBind("./EquipmentInfo/EquipmentBreak", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakGo;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakTitleText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCurLvEquipmentIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCurLvEquipmentIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakCurLvEquipmentIconSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakCurLvEquipmentStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/LvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakCurLvEquipmentOldLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/CurLvEquipment/LvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakCurLvEquipmentMaxLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakNextLvEquipmentIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakNextLvEquipmentIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakNextLvEquipmentIconSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakNextLvEquipmentStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/LvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakNextLvEquipmentOldLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/NextLvEquipment/LvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakNextLvEquipmentMaxLvText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakMaterialIconGo;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakMaterialIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakMaterialIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/MaterialIcon/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakMaterialSSREffect;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/ItemsContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakItemsContent;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/BreakGold/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakGoldText;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanBreak/BreakButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakButton;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/CurLvEquipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCantIcon;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/CurLvEquipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakCantIconBg;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/CurLvEquipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakCantStarGroup;
    [AutoBind("./EquipmentInfo/EquipmentBreak/CanNotBreak/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakCantTitleText;
    [AutoBind("./BreakSuccessPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakSuccessPanel;
    [AutoBind("./BreakSuccessPanel/BlackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_breakSuccessPanelBlackButton;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/Equipment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakSuccessInfoIconBg;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/Equipment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_breakSuccessInfoIcon;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/Equipment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_breakSuccessInfoStarGroup;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/CurLvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoCurOldLvText;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/CurLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoCurMaxLvText;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/NextLvGroup/OldLvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoNextOldLvText;
    [AutoBind("./BreakSuccessPanel/BreakInfoGroup/LvGroup/NextLvGroup/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_breakSuccessInfoNextMaxLvText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentItemNameText;
    [AutoBind("./EquipmentInfo/Enchantment/LevelGroup/LevelValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentItemLevelValueText;
    [AutoBind("./EquipmentInfo/Enchantment/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentIcon;
    [AutoBind("./EquipmentInfo/Enchantment/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentIconBg;
    [AutoBind("./EquipmentInfo/Enchantment/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentIconSSREffect;
    [AutoBind("./EquipmentInfo/Enchantment/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentStarGroup;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/EnchantPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentEnchantPropertyGroup;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/EnchantPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentEnchantPropertyGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoNowEffectStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/RuneIcon", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoNowEffectRuneIconStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/RuneIcon/Active", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentSuitInfoNowEffectRuneActiveIcon;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/RuneIcon/Unactive", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentSuitInfoNowEffectRuneUnactiveIcon;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoNowEffectRuneNameText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/Scrollrect/Viewport/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoNowEffectRune2SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/NowEffect/Scrollrect/Viewport/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoNowEffectRune4SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentSuitInfoAfterEffectStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/RuneIcon/Unactive", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentSuitInfoAfterEffectRuneUnactiveIcon;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoAfterEffectRuneNameText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/Scrollrect/Viewport/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoAfterEffectRune2SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/ItemPropertyGroup/SuitInfo/InfoGroup/AfterEffect/Scrollrect/Viewport/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentSuitInfoAfterEffectRune4SuitInfoText;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentMaterialGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/IconBG", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentStoneIconBg;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentStoneIcon;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/StoneNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentStoneNameText;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentStoneDescText;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/ValueGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentStoneValueGroupStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/MaterialGroup/ItemIcon/ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentStoneHaveCount;
    [AutoBind("./EquipmentInfo/Enchantment/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentGoldenValueText;
    [AutoBind("./EquipmentInfo/Enchantment/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentGoldenValueTextStateCtrl;
    [AutoBind("./EquipmentInfo/Enchantment/EnchantmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentButton;
    [AutoBind("./EquipmentInfo/Enchantment/KeepPropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantKeepPropertyGroup;
    [AutoBind("./EquipmentInfo/Enchantment/EnchantSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantSuccessEffectPanel;
    [AutoBind("./EnchantmentResultPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelCloseButton;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/PropretyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enchantmentResultPanelOldPropretyGroup;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/Scrollrect/Viewport/SuitInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelOldSuitInfoStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/Scrollrect/Viewport/SuitInfoGroup/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelOldSuitInfoNameText;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/Scrollrect/Viewport/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelOld2SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/OldPropretyGroup/Scrollrect/Viewport/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelOld4SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelNewProprety1StateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelNewProprety1ProgressBar;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety1NameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety1/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety1ValueText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelNewProprety2StateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelNewProprety2ProgressBar;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety2NameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety2/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety2ValueText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelNewProprety3StateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelNewProprety3ProgressBar;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety3NameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/PropretyGroup/Proprety3/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewProprety3ValueText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/Scrollrect/Viewport/SuitInfoGroup/EffectTitleText/EffectNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNewSuitInfoNameText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/Scrollrect/Viewport/SuitInfoGroup/2Suit/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNew2SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/NewPropretyGroup/Scrollrect/Viewport/SuitInfoGroup/4Suit/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNew4SuitInfoText;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/EnchantmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelEnchantmentAgainButton;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/EnchantmentButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelEnchantmentAgainButtonStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/ItemNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelNumberText;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/GoldenNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enchantmentResultPanelGoldenNumberText;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/GoldenNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enchantmentResultPanelGoldenNumberStateCtrl;
    [AutoBind("./EnchantmentResultPanel/Detail/EnchantmentAgain/ItemImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_enchantmentResultPanelItemImage;
    [AutoBind("./EnchantmentResultPanel/Detail/SaveProprety/SavePropretyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelSavePropretyButton;
    [AutoBind("./EnchantmentResultPanel/ContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_enchantmentResultPanelContinueButton;
    [AutoBind("./SavePropretyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_savePropretyPanelStateCtrl;
    [AutoBind("./SavePropretyPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_savePropretyPanelCancelButton;
    [AutoBind("./SavePropretyPanel/Detail/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_savePropretyPanelComfirmButton;
    [AutoBind("./SavePropretyPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_savePropretyPanelToggle;
    [AutoBind("./CancelPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cancelPanelStateCtrl;
    [AutoBind("./CancelPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelPanelCancelButton;
    [AutoBind("./CancelPanel/Detail/ComfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelPanelComfirmButton;
    [AutoBind("./CancelPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_cancelPanelToggle;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_slot;
    private int m_isAscend;
    private bool m_isFirstIn;
    private bool m_isToggleChanged;
    private EquipmentForgeUIController.ForgeState m_curForgeState;
    private ulong m_curEquipmentInstanceId;
    private ulong m_curDescEquipmentInstanceId;
    private ulong m_curBreakMaterialEquipmentId;
    private ulong m_curAddEnhanceEquipmentInstanceId;
    private BagItemBase m_curSelectEnchantStoneItem;
    private ConfigDataEnchantStoneInfo m_lastSelectEnchantStoneInfo;
    private List<ulong> m_enhanceEquipmentInstanceIds;
    private List<EquipmentBagItem> m_equipmentItemCache;
    private List<EnchantStoneBagItem> m_enchantStoneItemCache;
    private List<EquipmentDepotListItemUIController> m_equipmentForgeCtrlList;
    private EquipmentDepotUIController.EquipmentSortTypeState m_curEquipmentSortType;
    private int m_curEnchantmentSortTypeId;
    private List<CommonBattleProperty> m_properties;
    private bool m_isAfter3DTouch;
    private string m_oldSkillLevelStr;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSortTypePanel;
    private static DelegateBridge __Hotfix_InitLoopScrollRect;
    private static DelegateBridge __Hotfix_OnPoolObjectCreated;
    private static DelegateBridge __Hotfix_UpdateEquipmentForgeView;
    private static DelegateBridge __Hotfix_CreateEquipmentList;
    private static DelegateBridge __Hotfix_SortEquipmentListByType;
    private static DelegateBridge __Hotfix_DefaultEquipmentItemComparer;
    private static DelegateBridge __Hotfix_CollectEquipmentPropValueAndSort;
    private static DelegateBridge __Hotfix_OnEquipmentListItemClick;
    private static DelegateBridge __Hotfix_OnEquipmentItemClickInEnhance;
    private static DelegateBridge __Hotfix_OnEquipmentItemClickInBreak;
    private static DelegateBridge __Hotfix_OnEquipmentItemClickInEnchantment;
    private static DelegateBridge __Hotfix_OnEquipmentListItemNeedFill;
    private static DelegateBridge __Hotfix_CloseEquipmentDescPanel;
    private static DelegateBridge __Hotfix_OnEquipmentListItem3DTouch;
    private static DelegateBridge __Hotfix_OnFilterSortButtonClick;
    private static DelegateBridge __Hotfix_OnEnchantmentFilterSortButtonClick;
    private static DelegateBridge __Hotfix_OnCloseFilterSortTypeGo;
    private static DelegateBridge __Hotfix_OnCloseEnchantmentFilterSortTypeGo;
    private static DelegateBridge __Hotfix_OnFilterSortOrderButtonClick;
    private static DelegateBridge __Hotfix_OnFilterTypeButtonClick;
    private static DelegateBridge __Hotfix_OnEnchantmentFilterTypeButtonClick;
    private static DelegateBridge __Hotfix_SetEquipmentItemDesc;
    private static DelegateBridge __Hotfix_SetDescEquipmentLimit;
    private static DelegateBridge __Hotfix_SetDescEquipmentSkill;
    private static DelegateBridge __Hotfix_SetDescEquipmentEnchant;
    private static DelegateBridge __Hotfix_SetPropItemColor;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_OnDescLockButtonClick;
    private static DelegateBridge __Hotfix_UpdateViewInEnhanceState;
    private static DelegateBridge __Hotfix_SetEnhancePropItems;
    private static DelegateBridge __Hotfix_ResetEnhancePropItemActive;
    private static DelegateBridge __Hotfix_CalculateEquipmentEnhanceAddLvByExp;
    private static DelegateBridge __Hotfix_OnEnhanceButtonClick;
    private static DelegateBridge __Hotfix_SendEnhanceEquipmentMsg;
    private static DelegateBridge __Hotfix_OnEnhanceSucceed;
    private static DelegateBridge __Hotfix_StopEnhanceSucceedEffect;
    private static DelegateBridge __Hotfix_SetSkillLevelUpEffect;
    private static DelegateBridge __Hotfix_DelayActiveIntensifyContinueButton;
    private static DelegateBridge __Hotfix_OnIntensifySuccessPanelBlackButtonClick;
    private static DelegateBridge __Hotfix_ShowEnhanceItemSRConfirmPanel;
    private static DelegateBridge __Hotfix_ShowEnhanceItemsConfirmPanel;
    private static DelegateBridge __Hotfix_OnEnhanceItemSRConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnEnhanceItemSRCancelButtonClick;
    private static DelegateBridge __Hotfix_OnEnhanceConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnEnhanceCancelButtonClick;
    private static DelegateBridge __Hotfix_OnEnhanceFastAddButtonClick;
    private static DelegateBridge __Hotfix_IsEquipmentAddExpAtMaxLevelMaxExp;
    private static DelegateBridge __Hotfix_UpdateViewInBreakState;
    private static DelegateBridge __Hotfix_OnBreakButtonClick;
    private static DelegateBridge __Hotfix_OnBreakSucceed;
    private static DelegateBridge __Hotfix_DelayActiveBreakContinueButton;
    private static DelegateBridge __Hotfix_OnBreakSuccessPanelContinueButtonClick;
    private static DelegateBridge __Hotfix_OnEquipmentBreakNeedItemClick;
    private static DelegateBridge __Hotfix_UpdateViewInEnchantmentState;
    private static DelegateBridge __Hotfix_OnEnchantmentButtonClick;
    private static DelegateBridge __Hotfix_ShowEnchantmentResultPanel;
    private static DelegateBridge __Hotfix_SetEnchantmentResultPanelInfo;
    private static DelegateBridge __Hotfix_Co_PlayNewPropertyEffect;
    private static DelegateBridge __Hotfix_Co_DynamicSetPropertyValue;
    private static DelegateBridge __Hotfix_CalcPropertyPercent;
    private static DelegateBridge __Hotfix_OnEnchantmentResultPanelContinueButtonClick;
    private static DelegateBridge __Hotfix_CloseEnchantmentResultPanel;
    private static DelegateBridge __Hotfix_OnEnchantmentResultSaveButtonClick;
    private static DelegateBridge __Hotfix_OnEnchantmentAgainButtonClick;
    private static DelegateBridge __Hotfix_ShowSavePanel;
    private static DelegateBridge __Hotfix_CloseSavePanel;
    private static DelegateBridge __Hotfix_OnSavePanlConfirmClick;
    private static DelegateBridge __Hotfix_PlaySucceedEffect;
    private static DelegateBridge __Hotfix_OnCloseEnchantResultPanelButtonClick;
    private static DelegateBridge __Hotfix_ShowCancelPanel;
    private static DelegateBridge __Hotfix_CloseCancelPanel;
    private static DelegateBridge __Hotfix_OnCancelPanlConfirmClick;
    private static DelegateBridge __Hotfix_OnSavePropertyToggleValueChanged;
    private static DelegateBridge __Hotfix_OnCancelPanelToggleValueChanged;
    private static DelegateBridge __Hotfix_OnEnhanceToggleValueChanged;
    private static DelegateBridge __Hotfix_OnBreakToggleValueChanged;
    private static DelegateBridge __Hotfix_OnEnchantmentToggleValueChanged;
    private static DelegateBridge __Hotfix_OnEnchantLockMaskButtonClick;
    private static DelegateBridge __Hotfix_OnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_SetPlayerResourceGold;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_GetFirstEquipmentGoInListForUserGuide;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnEnhanceButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnEnhanceButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBreakButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnBreakButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnEnchantButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnEnchantButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBreakNeedItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnBreakNeedItemClick;
    private static DelegateBridge __Hotfix_add_EventOnEnchantSaveButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnEnchantSaveButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_remove_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_add_EventOnLockButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnLockButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentForgeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitSortTypePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEquipmentForgeView(
      EquipmentForgeUIController.ForgeState forgeState,
      int slot,
      ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEquipmentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortEquipmentListByType(List<EquipmentBagItem> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int DefaultEquipmentItemComparer(EquipmentBagItem e1, EquipmentBagItem e2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropValueAndSort(
      List<EquipmentBagItem> list,
      PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClickInEnhance(EquipmentDepotListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClickInBreak(EquipmentDepotListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentItemClickInEnchantment(EquipmentDepotListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEquipmentDescPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItem3DTouch(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentFilterSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseFilterSortTypeGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseEnchantmentFilterSortTypeGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentLimit(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentSkill(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDescEquipmentEnchant(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItemColor(Text oldText, Text newText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewInEnhanceState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnhancePropItems(
      PropertyModifyType type,
      int originValue,
      int perAddValue,
      int curLv,
      int addLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetEnhancePropItemActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalculateEquipmentEnhanceAddLvByExp(
      EquipmentBagItem equipment,
      int enhanceExp,
      int curLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendEnhanceEquipmentMsg()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceSucceed(string oldSkillLevelStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopEnhanceSucceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillLevelUpEffect(
      EquipmentBagItem equipment,
      string oldLv,
      string newLv,
      int newSkillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayActiveIntensifyContinueButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnIntensifySuccessPanelBlackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnhanceItemSRConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnhanceItemsConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceItemSRConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceItemSRCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEnhanceFastAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentAddExpAtMaxLevelMaxExp(
      EquipmentBagItem equipment,
      int addLv,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewInBreakState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakSucceed()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayActiveBreakContinueButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakSuccessPanelContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentBreakNeedItemClick(GoodsType type, int id, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewInEnchantmentState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowEnchantmentResultPanel(
      int newResonanceId,
      List<CommonBattleProperty> properties,
      bool isFirstShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnchantmentResultPanelInfo(
      int newResonanceId,
      List<CommonBattleProperty> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayNewPropertyEffect(List<CommonBattleProperty> properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_DynamicSetPropertyValue(
      Text valueText,
      Image progressBar,
      int value,
      PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float CalcPropertyPercent(PropertyModifyType propertyTypeId, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentResultPanelContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseEnchantmentResultPanel(Action succeedEffectEvent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentResultSaveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentAgainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSavePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSavePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSavePanlConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySucceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseEnchantResultPanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCancelPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseCancelPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelPanlConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSavePropertyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelPanelToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnhanceToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBreakToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantmentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantLockMaskButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerResourceGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetFirstEquipmentGoInListForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, EquipmentForgeUIController.ForgeState> EventOnGoldAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, List<ulong>, Action, int> EventOnEnhanceButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, ulong, Action, int> EventOnBreakButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, ulong, Action<int, List<CommonBattleProperty>>> EventOnEnchantButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnBreakNeedItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action> EventOnEnchantSaveButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, EquipmentForgeUIController.ForgeState, Action> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum ForgeState
    {
      Enhance,
      Break,
      Enchantment,
    }
  }
}
