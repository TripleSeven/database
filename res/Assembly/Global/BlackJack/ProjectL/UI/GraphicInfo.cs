﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GraphicInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using SLua;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class GraphicInfo
  {
    [DoNotToLua]
    private GraphicInfo.LuaExportHelper luaExportHelper;

    [DoNotToLua]
    public GraphicInfo.LuaExportHelper m_luaExportHelper
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      set
      {
        this.luaExportHelper = value;
      }
    }

    public class LuaExportHelper
    {
      private GraphicInfo m_owner;

      public LuaExportHelper(GraphicInfo owner)
      {
        this.m_owner = owner;
      }
    }
  }
}
