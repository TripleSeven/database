﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersHeroListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersHeroListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroLvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    [AutoBind("./SSRFrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrFrameEffect;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frameImg;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMark;
    [AutoBind("./HeartImage/ValueTextRed", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityValueTextRed;
    [AutoBind("./HeartImage/ValueTextPink", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityValueTextPink;
    public Hero m_hero;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitFettersHeroListItem;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFettersHeroListItem(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<Hero> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
