﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleRewardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using MarchingBytes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RaffleRewardUIController : UIControllerBase
  {
    protected Dictionary<int, List<RaffleItem>> m_raffleRewardLevelInfoDict;
    protected List<RaffleRewardItemUIController> m_rewardItemCtrlList;
    public Action EventOnCloseButtonClick;
    protected bool m_isShow;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./PrefabRoot", AutoBindAttribute.InitState.NotInit, false)]
    public Transform PrefabRoot;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool EasyPool;
    [AutoBind("./LayoutRoot/RewardScrollView/Viewport/RewardItemLayContent", AutoBindAttribute.InitState.NotInit, false)]
    public Transform RewardItemRoot;
    [AutoBind("./LayoutRoot/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseButton;
    [AutoBind("./LayoutRoot/RewardScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public ScrollRect RewardScrollRect;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button BgButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitRaffleRewardItemPool;
    private static DelegateBridge __Hotfix_UpdateRaffleRewardPanel;
    private static DelegateBridge __Hotfix_ShowRaffleRewardPanel;
    private static DelegateBridge __Hotfix_IsRaffleRewardPanelShow;
    private static DelegateBridge __Hotfix_GainRaffleRewardLevelInfo;
    private static DelegateBridge __Hotfix_AllocRewardItemCtrl;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleRewardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitRaffleRewardItemPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRaffleRewardPanel(RafflePool rafflePool, bool refreshAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRaffleRewardPanel(bool isShow, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRaffleRewardPanelShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void GainRaffleRewardLevelInfo(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AllocRewardItemCtrl(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
