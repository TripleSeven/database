﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class ArenaDefendActor
  {
    private GridPosition m_position;
    private int m_direction;
    private int m_soldierCount;
    private BattleHero m_hero;
    private ArenaDefendBattle m_battle;
    private Colori m_tweenFromColor;
    private Colori m_tweenToColor;
    private float m_tweenColorTime;
    private GenericGraphic[] m_graphics;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Destroy;
    private static DelegateBridge __Hotfix_DestroyGraphics;
    private static DelegateBridge __Hotfix_SetHero;
    private static DelegateBridge __Hotfix_UpdateGraphics;
    private static DelegateBridge __Hotfix_UpdateGraphic;
    private static DelegateBridge __Hotfix_GetHero;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_SetPosition;
    private static DelegateBridge __Hotfix_GetPosition;
    private static DelegateBridge __Hotfix_GetDirection;
    private static DelegateBridge __Hotfix_TweenColor;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_ComputeGraphicPosition;
    private static DelegateBridge __Hotfix_GetGraphicOffset;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendActor(ArenaDefendBattle uiTask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyGraphics()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGraphics()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool UpdateGraphic(
      int idx,
      string assetName,
      float scale,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDirection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TweenColor(Colori c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GetGraphicOffset(int idx, int dir)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
