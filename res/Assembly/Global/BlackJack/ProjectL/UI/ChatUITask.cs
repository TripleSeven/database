﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class ChatUITask : UITask
  {
    private bool m_isTalkButtonDown;
    private bool m_isExitTalkButtonRect;
    private ProjectLPlayerContext m_playerCtx;
    private List<ChatComponent.ChatMessageClient> m_currentChatList;
    private ChatChannel m_currentTableType;
    private string m_chatTagetID;
    private string m_groupChatID;
    private List<ChatUITask.ShareGoodsData> m_shareGoodsDataList;
    private IConfigDataLoader m_configDataLoader;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private bool m_isPrivateShowHistoryRecord;
    private bool m_isDragSrcowView;
    private bool m_isGroupShowHistoryRecord;
    private ChatUIController m_chatUICtrl;
    private ChatExpressionUIController m_expressionCtrl;
    private string m_currentChatUserID;
    public const string ParamsKey_AppointChannel = "AppointEnterChannel";
    public const string ParamsKey_ChatUserId = "PrivateChatPlayerGameUserId";
    public const string ParamsKey_GroupChatId = "GroupChatId";
    public const string ExpressionMark = "#";
    public const string ExpressionMark2 = "＃";
    public const int EquipmentShareToChatListMaxCount = 8;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartChatUITaskWithNormalMode;
    private static DelegateBridge __Hotfix_StartChatUITaskToEnterAppointChannel;
    private static DelegateBridge __Hotfix_ShareItemToChatByInstanceId;
    private static DelegateBridge __Hotfix_ShareItemToChat;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnNewIntent;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnGetChatTargetInfoEndFroResume;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_OnSmallExpressionClick;
    private static DelegateBridge __Hotfix_OnExpressionBgButtonClick;
    private static DelegateBridge __Hotfix_OnExpressionFunctionButtonClick;
    private static DelegateBridge __Hotfix_OnRoomIdInputEnd;
    private static DelegateBridge __Hotfix_OnItemVoiceButtonClick;
    private static DelegateBridge __Hotfix_OnPlayerIconClick;
    private static DelegateBridge __Hotfix_ShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_OnTalkButtonHold;
    private static DelegateBridge __Hotfix_CheckMicrophonePermission;
    private static DelegateBridge __Hotfix_OnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_OnExitTalkButton;
    private static DelegateBridge __Hotfix_OnEnterTalkButton;
    private static DelegateBridge __Hotfix_OnTableChange;
    private static DelegateBridge __Hotfix_OnChatTargetChanged;
    private static DelegateBridge __Hotfix_OnSendButtonClick;
    private static DelegateBridge __Hotfix_RemoveEmojiInMessage;
    private static DelegateBridge __Hotfix_SendChatContent;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnVoiceRecordTimeout;
    private static DelegateBridge __Hotfix_OnGoodsLinkClick;
    private static DelegateBridge __Hotfix_OnBattleReportClick;
    private static DelegateBridge __Hotfix_GetDataFromIntent;
    private static DelegateBridge __Hotfix_TryGetPrivateChatPlayerInfoList;
    private static DelegateBridge __Hotfix_TryGetPrivateChatPlayerInfoListFilterBlacklist;
    private static DelegateBridge __Hotfix_TryGetChatGroupInfoList;
    private static DelegateBridge __Hotfix_OnChatMessageNtf;
    private static DelegateBridge __Hotfix_OnChatMessageAck;
    private static DelegateBridge __Hotfix_OnChatGroupUpdateNtf;
    private static DelegateBridge __Hotfix_EventOnFriendGetSocialRelationNtf;
    private static DelegateBridge __Hotfix_IsPipelineStateMaskNeedUpdate;
    private static DelegateBridge __Hotfix_EnablePipelineStateMask;
    private static DelegateBridge __Hotfix_GetCurrentChatListByTableType;
    private static DelegateBridge __Hotfix_TickForVoiceRecordTime;
    private static DelegateBridge __Hotfix_TickForHistoryRecordShow;
    private static DelegateBridge __Hotfix_TickForVoiceAnim;
    private static DelegateBridge __Hotfix_PlayVoiceAnim;
    private static DelegateBridge __Hotfix_StopAllVoiceAnim;
    private static DelegateBridge __Hotfix_OnChatVoiceStartPlay;
    private static DelegateBridge __Hotfix_DelayInstancePrefab;
    private static DelegateBridge __Hotfix_RefreshRedState;
    private static DelegateBridge __Hotfix_ChackAndReturnIsEquipmentShareChat;
    private static DelegateBridge __Hotfix_GetEquipmentShareToChatInstanceIdList;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ChatUITask StartChatUITaskWithNormalMode(
      UIIntent returnToIntent = null,
      Action redirectPipLineOnLoadAllResCompleted = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ChatUITask StartChatUITaskToEnterAppointChannel(
      ChatChannel channel,
      UIIntent returnToIntent = null,
      string id = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ShareItemToChatByInstanceId(ulong instanceId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShareItemToChat(ulong instanceId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool OnGetChatTargetInfoEndFroResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSmallExpressionClick(SmallExpressionItemContrller ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionFunctionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnRoomIdInputEnd(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemVoiceButtonClick(ChatComponent.ChatMessageClient voiceInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerIconClick(ChatMessage chatInfo, GameObject playerIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPlayerSimpleInfo(GameObject playerIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonHold()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckMicrophonePermission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitTalkButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterTalkButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTableChange(ChatChannel currentTableType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatTargetChanged(ChatChannel channel, string id, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClick(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string RemoveEmojiInMessage(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendChatContent(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceRecordTimeout(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoodsLinkClick(ulong instanceId, ChatBagItemMessage chatGoodsMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportClick(
      ChatPeakArenaBattleReportMessage chatPeakArenaBattleReportMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetDataFromIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryGetPrivateChatPlayerInfoList(List<string> playerIdList, Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryGetPrivateChatPlayerInfoListFilterBlacklist(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryGetChatGroupInfoList(Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatMessageNtf(ChatMessage info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatMessageAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatGroupUpdateNtf(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EventOnFriendGetSocialRelationNtf(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPipelineStateMaskNeedUpdate(ChatUITask.PipeLineStateMaskType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EnablePipelineStateMask(ChatUITask.PipeLineStateMaskType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ChatComponent.ChatMessageClient> GetCurrentChatListByTableType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForVoiceRecordTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForHistoryRecordShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForVoiceAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayVoiceAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopAllVoiceAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatVoiceStartPlay(ChatComponent.ChatMessageClient voiceMsg)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayInstancePrefab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshRedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ChackAndReturnIsEquipmentShareChat(
      string content,
      List<ChatUITask.ShareGoodsData> equipmentShareToChatList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ulong> GetEquipmentShareToChatInstanceIdList(
      List<ChatUITask.ShareGoodsData> equipmentShareToChatList)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum PipeLineStateMaskType
    {
      IsInit,
      NewChatInfo,
      TableChange,
      ChooseNewChatTarget,
      ShowHistoryRecord,
      ChatTargetRefresh,
    }

    public struct ShareGoodsData
    {
      public ulong m_instanceId;
      public string m_name;

      public ShareGoodsData(ulong instanceId, string name)
      {
        this.m_instanceId = instanceId;
        this.m_name = name;
      }
    }
  }
}
