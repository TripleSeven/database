﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroListUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroListUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private HeroListUIController m_heroListUIController;
    private HeroDetailUIController m_heroDetailUIController;
    private HeroDetailJobUIController m_heroDetailJobUIController;
    private HeroDetailInfoUIController m_heroDetailInfoUIController;
    private HeroDetailAddExpUIController m_heroDetailAddExpUIController;
    private HeroDetailSoldierUIController m_heroDetailSoldierUIController;
    private HeroDetailLifeUIController m_heroDetailLifeUIController;
    private HeroDetailEquipmentUIController m_heroDetailEquipmentUIController;
    private HeroDetailSelectSkillUIController m_heroDetailSelectSkillUIController;
    private HeroJobTransferUITask m_heroJobTransferUITask;
    private HeroSkinChangeUITask m_heroCharChangeTask;
    private StoreSoldierSkinDetailUITask m_storeSoldierSkinDetailUITask;
    public const string ListMode = "List";
    public const string DetailInfoMode = "DetailInfo";
    public const string DetailJobMode = "DetailJob";
    public const string DetailSoldierMode = "DetailSoldier";
    public const string DetailEquipmentMode = "DetailEquipment";
    public const string DetailLifeMode = "DetailLife";
    public const string DetailSelectSkillMode = "DetailSelectSkill";
    public const string DetailAddExpMode = "DetailAddExp";
    private string m_stateName;
    private string m_lastMode;
    private string m_curMode;
    private List<Hero> m_lockedList;
    private List<Hero> m_curHeroList;
    private List<Hero> m_unlockedList;
    private int m_curHeroPos;
    private int m_curLayerDescIndex;
    private int m_lastHeroId;
    private bool m_isUnlockHero;
    private bool m_isDetailLayerOpen;
    private bool m_isNeedRefreshHeroList;
    private HeroListUIController.HeroSortType m_curHeroSortType;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_SaveUIStateToIntent;
    private static DelegateBridge __Hotfix_GetUIStateFromIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectExpItemAssets;
    private static DelegateBridge __Hotfix_CollectHeroEquipmentAssets;
    private static DelegateBridge __Hotfix_UpdateHeroListData;
    private static DelegateBridge __Hotfix_UpdateHeroCardData;
    private static DelegateBridge __Hotfix_UpdateCurrentHeroData;
    private static DelegateBridge __Hotfix_UpdateSoldierModeData;
    private static DelegateBridge __Hotfix_UpdateJobModeData;
    private static DelegateBridge __Hotfix_HeroListItemCompare;
    private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
    private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
    private static DelegateBridge __Hotfix_CreateLayerDescByIndex;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateViewOnHeroChanged;
    private static DelegateBridge __Hotfix_StartUpdatePiplineInHeroListTask;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PushLayerByCurState;
    private static DelegateBridge __Hotfix_PushLayerByIndex;
    private static DelegateBridge __Hotfix_CheckConflictBetweenLayer;
    private static DelegateBridge __Hotfix_HeroDetailJobUIController_OnOpenEquipMasterButtonClick;
    private static DelegateBridge __Hotfix_HeroDetailJobUIController_OnEquipMasterRefineButtonClick;
    private static DelegateBridge __Hotfix_HeroDetailJobUIController_OnEquipMasterConserveButtonClick;
    private static DelegateBridge __Hotfix_HeroDetailUIController_OnSetDetailState;
    private static DelegateBridge __Hotfix_SetTabCommonUIStateByName;
    private static DelegateBridge __Hotfix_HeroListUIController_SetHeroList;
    private static DelegateBridge __Hotfix_HeroListUIController_OnReturn;
    private static DelegateBridge __Hotfix_HeroListUIController_OnAddHero;
    private static DelegateBridge __Hotfix_HeroListUIController_OnHeroCompose;
    private static DelegateBridge __Hotfix_HeroDetailLifeUIController_OnVoiceItemClick;
    private static DelegateBridge __Hotfix_HeroDetailSelectSkillUIController_OnHeroSkillsSelect;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnHeroSoldierSelect;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnGotoDrill;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnGotoJobTransfer;
    private static DelegateBridge __Hotfix_HeroDetailJobUIController_OnJobLvUpgrade;
    private static DelegateBridge __Hotfix_HeroDetailAddExpUIController_OnHeroAddExp;
    private static DelegateBridge __Hotfix_HeroDetailAddExpUIController_OnHeroMaxLevelUseExpItem;
    private static DelegateBridge __Hotfix_HeroDetailAddExpUIController_OnReturn;
    private static DelegateBridge __Hotfix_HeroListUIController_OnDetail;
    private static DelegateBridge __Hotfix_HeroDetailUIController_OnReturn;
    private static DelegateBridge __Hotfix_HeroListUIController_OnHeroBreak;
    private static DelegateBridge __Hotfix_HeroDetailUIController_OnJobTransfer;
    private static DelegateBridge __Hotfix_HeroJobTransferUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_HeroListUIController_OnComment;
    private static DelegateBridge __Hotfix_HeroCommentUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnGotoEquipmentDepot;
    private static DelegateBridge __Hotfix_EquipmentDepotUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnGotoEquipmentForge;
    private static DelegateBridge __Hotfix_EquipmentForgeUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnEquipmentTakeOff;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnLockButtonClick;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnShareButtonClick;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnAutoEquip;
    private static DelegateBridge __Hotfix_HeroDetailEquipmentUIController_OnAutoRemove;
    private static DelegateBridge __Hotfix_Hero_OnShowGetPath;
    private static DelegateBridge __Hotfix_HeroListUIController_OnGotoEquipmentTab;
    private static DelegateBridge __Hotfix_HeroListUIController_OnGotoJobTab;
    private static DelegateBridge __Hotfix_HeroListUIController_OnSortToggleClick;
    private static DelegateBridge __Hotfix_CloseHeroCharChangeTask;
    private static DelegateBridge __Hotfix_HeroListUIController_OnHeroCharSkinChangeButtonClick;
    private static DelegateBridge __Hotfix_HeroListUIController_OnHeroCharClick;
    private static DelegateBridge __Hotfix_HeroListUIController_OnGoToMemoryExtractionStore;
    private static DelegateBridge __Hotfix_HeroCharChangeTask_OnSkinChangedPreview;
    private static DelegateBridge __Hotfix_HeroCharChangeTask_EventOnClose;
    private static DelegateBridge __Hotfix_HeroCharChangeTask_OnAddSkinTicket;
    private static DelegateBridge __Hotfix_Hero_OnGotoGetPath;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnSkinInfoButtonClick;
    private static DelegateBridge __Hotfix_HeroDetailInfoUIController_OnSkinInfoButtonClick;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUITask_EventOnReturn;
    private static DelegateBridge __Hotfix_HeroDetailInfoUIController_OnJobUpButtonClick;
    private static DelegateBridge __Hotfix_StoreSoldierSkinDetailUITask_EventOnBuySuccessEnd;
    private static DelegateBridge __Hotfix_StoreSoldierSkinDetailUITask_EventOnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroListUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveUIStateToIntent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUIStateFromIntent(UIIntent uiIntent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectExpItemAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroEquipmentAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroListData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroCardData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrentHeroData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierModeData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobModeData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HeroListItemCompare(Hero h1, Hero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UITaskBase.LayerDesc CreateLayerDescByIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewOnHeroChanged(
      int pos,
      bool isUnlockHero,
      bool isNeedStopCoroutine,
      int lastHeroId,
      bool isNeedRefreshHeroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUpdatePiplineInHeroListTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushLayerByCurState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PushLayerByIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckConflictBetweenLayer(int layerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailJobUIController_OnOpenEquipMasterButtonClick(Action onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailJobUIController_OnEquipMasterRefineButtonClick(
      int heroId,
      int jobConnectionId,
      int slotId,
      int index,
      int stoneId,
      Action<HeroJobSlotRefineryProperty> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailJobUIController_OnEquipMasterConserveButtonClick(Action<int> onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailUIController_OnSetDetailState(string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTabCommonUIStateByName(string modeName, string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_SetHeroList(List<Hero> unlockedList, List<Hero> lockedList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnAddHero(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroCompose(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailLifeUIController_OnVoiceItemClick(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSelectSkillUIController_OnHeroSkillsSelect(
      int heroId,
      List<int> skillIds,
      bool isSkillChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnHeroSoldierSelect(
      int heroId,
      int soldierId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoDrill(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoJobTransfer(
      ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailJobUIController_OnJobLvUpgrade(
      int heroId,
      int jobConnectionId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailAddExpUIController_OnHeroAddExp(
      int heroId,
      BagItemBase bagItem,
      int count,
      Action OnFinished)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailAddExpUIController_OnHeroMaxLevelUseExpItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailAddExpUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnDetail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroBreak(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailUIController_OnJobTransfer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroJobTransferUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnComment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroCommentUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnGotoEquipmentDepot(
      int slot,
      ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentDepotUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnGotoEquipmentForge(
      int slot,
      ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnEquipmentTakeOff(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnLockButtonClick(
      ulong instanceId,
      int slot,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnShareButtonClick(ulong instanceId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnAutoEquip(int heroId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailEquipmentUIController_OnAutoRemove(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Hero_OnShowGetPath(GoodsType goodsType, int goodsId, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnGotoEquipmentTab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnGotoJobTab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnSortToggleClick(HeroListUIController.HeroSortType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseHeroCharChangeTask(Action closeFinisdhAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroCharSkinChangeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnHeroCharClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroListUIController_OnGoToMemoryExtractionStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCharChangeTask_OnSkinChangedPreview(string spinePath, int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCharChangeTask_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroCharChangeTask_OnAddSkinTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Hero_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnSkinInfoButtonClick(
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailInfoUIController_OnSkinInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUITask_EventOnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailInfoUIController_OnJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreSoldierSkinDetailUITask_EventOnBuySuccessEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreSoldierSkinDetailUITask_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
