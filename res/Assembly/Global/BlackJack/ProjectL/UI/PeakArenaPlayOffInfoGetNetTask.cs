﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaPlayOffInfoGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaPlayOffInfoGetNetTask : UINetTask
  {
    private int m_sequenceId;
    private int m_season;
    public long m_version;
    public static PeakArenaPlayOffInfoGetNetTask m_curRunningTask;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnPeakArenaPlayOffInfoGetAck;
    private static DelegateBridge __Hotfix_GetUserSummary;
    private static DelegateBridge __Hotfix_SendRequest;
    private static DelegateBridge __Hotfix_FinishTransaction;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffInfoGetNetTask(int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnPeakArenaPlayOffInfoGetAck(
      int result,
      int sequenceId,
      int nextMatchIndex,
      long version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void GetUserSummary()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool SendRequest()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FinishTransaction(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
