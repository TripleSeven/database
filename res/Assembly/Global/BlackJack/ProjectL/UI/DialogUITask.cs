﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class DialogUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private DialogUIController m_dialogUIController;
    private ConfigDataDialogInfo m_dialogInfo;
    private bool m_needBGChangeTween;
    private bool m_isSkipClose;
    private List<DialogDetailUITask.LogData> m_dialogDataList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_InitDialogUIController;
    private static DelegateBridge __Hotfix_UninitDialogUIController;
    private static DelegateBridge __Hotfix_DialogUIController_OnSkip;
    private static DelegateBridge __Hotfix_DialogUIController_OnAutoShowDialogOption;
    private static DelegateBridge __Hotfix_OpenDialogOptionUITask;
    private static DelegateBridge __Hotfix_SkipDialogDialogBoxCallback;
    private static DelegateBridge __Hotfix_IsDialogGroupHasReward;
    private static DelegateBridge __Hotfix_IsDialogGroupHasOptionDialog;
    private static DelegateBridge __Hotfix_DialogUIController_OnNext;
    private static DelegateBridge __Hotfix_CloseDialog;
    private static DelegateBridge __Hotfix_SendGetDialogRewardNetTask;
    private static DelegateBridge __Hotfix_OnRewardTaskClose;
    private static DelegateBridge __Hotfix_StartDialogUITask;
    private static DelegateBridge __Hotfix_DialogOptionUITask_OnClose;
    private static DelegateBridge __Hotfix_DialogUIController_OnClose;
    private static DelegateBridge __Hotfix_DialogUIController_OnLog;
    private static DelegateBridge __Hotfix_SetDialogDataList;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUIController_OnSkip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUIController_OnAutoShowDialogOption(ConfigDataDialogInfo di)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenDialogOptionUITask(ConfigDataDialogInfo di)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkipDialogDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsDialogGroupHasReward(out int optionDialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsDialogGroupHasOptionDialog(out int optionDialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUIController_OnNext(bool needBGChangeTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendGetDialogRewardNetTask(int dialogId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardTaskClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDialogUITask(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogOptionUITask_OnClose(int choiceId, int nextDialogID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DialogUIController_OnLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDialogDataList(DialogDetailUITask.LogData.LogDataType logDataType, string data)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
