﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatBagItemHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace BlackJack.ProjectL.UI
{
  public class ChatBagItemHelper
  {
    private const string m_shareNameMark = "[{0}]";
    private static readonly Regex s_nameRegex;
    private static readonly StringBuilder s_TextBuilder;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ChatTextNameToInstanceId;
    private static DelegateBridge __Hotfix_ChatTextInstanceIdToName;
    private static DelegateBridge __Hotfix_ChatTextToChatLinkText;
    private static DelegateBridge __Hotfix_GetBagItemChatName;
    private static DelegateBridge __Hotfix_TryGetChatBagItemDataWithInstanceId;
    private static DelegateBridge __Hotfix_TryGetBagItemNameChatLinkText;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatBagItemHelper()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ChatTextNameToInstanceId(
      string chatText,
      List<ChatUITask.ShareGoodsData> shareGoodsDataList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ChatTextInstanceIdToName(
      string chatText,
      List<ChatBagItemMessage.ChatBagItemData> chatBagItemDataList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ChatTextToChatLinkText(
      string chatText,
      List<ChatBagItemMessage.ChatBagItemData> chatBagItemDataList,
      int linkCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetBagItemChatName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool TryGetChatBagItemDataWithInstanceId(
      ulong instanceId,
      List<ChatBagItemMessage.ChatBagItemData> chatBagItemDataList,
      out ChatBagItemMessage.ChatBagItemData chatBagItemData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool TryGetBagItemNameChatLinkText(
      string name,
      string itemChatName,
      ChatBagItemMessage.ChatBagItemData item,
      out string linkName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static ChatBagItemHelper()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
