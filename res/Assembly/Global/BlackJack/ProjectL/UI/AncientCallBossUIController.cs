﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallBossUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./TopGroup/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./TopGroup/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/BossDetailPanel/EvaluateGroupContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bossEvaluateImageStateCtrl;
    [AutoBind("./Margin/BossDetailPanel/EvaluateGroupContent/EvaluateLetterItem", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bossEvaluateImage;
    [AutoBind("./Margin/BossDetailPanel/HarmValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bestDamageValueText;
    [AutoBind("./Margin/BossDetailPanel/HarmText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_harmText;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/RewardDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardDetailButton;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/BossDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bossDetailButton;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/AchievementListButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementListButton;
    [AutoBind("./Margin/BossDetailPanel/ButtonGroup/AchievementListButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_achievementListButtonRedMark;
    [AutoBind("./AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackButton;
    [AutoBind("./Background/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bossCharGameObejct;
    [AutoBind("./Background/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fxGameObject;
    [AutoBind("./Margin/BossDetailPanel/AdvantageGroup/Group", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_bossAdvantageGroup;
    [AutoBind("./Margin/BossDetailPanel/BossNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_bossNameText;
    [AutoBind("./Margin/LeftPanel/BGImages/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankTitleText;
    [AutoBind("./Margin/LeftPanel/NoDetail", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_rankingListNoDetailGo;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_playerGroup;
    [AutoBind("./Margin/LeftPanel/PlayerGroupScollView/ViewPort/PlayerGroup/PlayerInfo", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_playerInfoPrefab;
    [AutoBind("./Margin/LeftPanel/DetailInfoToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankDetailButton;
    [AutoBind("./Margin/LeftPanel/RuleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreRuleButton;
    [AutoBind("./ScoreDescPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_scoreDescPanelUIStateCtrl;
    [AutoBind("./ScoreDescPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreDescPanelBGButton;
    [AutoBind("./ScoreDescPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scoreDescPanelCloseButton;
    [AutoBind("./ScoreDescPanel/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_scoreDescPanelScrollRect;
    [AutoBind("./ScoreDescPanel/Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreDescPanelDescText;
    [AutoBind("./RewardInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardInfoPanelUIStateCtrl;
    [AutoBind("./RewardInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardInfoPanelBGButton;
    [AutoBind("./RewardInfoPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardInfoPanelCloseButton;
    [AutoBind("./RewardInfoPanel/Detail/RewardListItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardInfoPanelScrollRect;
    [AutoBind("./RewardInfoPanel/Detail/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rewardInfoPanelDescText;
    [AutoBind("./Prefab/RewardListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardListItemPrefab;
    private UISpineGraphic m_graphic;
    private GameObject m_backgroundFxGameObject;
    private string m_backgroundFxAssetName;
    private ConfigDataAncientCallBossInfo m_bossInfo;
    private List<AncientCallPlayerInfoItemUIController> m_rankPlayerItemUIControllerList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_ShowAncientCallBoss;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_SetBossDetailPanel;
    private static DelegateBridge __Hotfix_SetTagInfoPanel;
    private static DelegateBridge __Hotfix_OnScoreRuleButtonClick;
    private static DelegateBridge __Hotfix_ShowScoreRuleDescPanel;
    private static DelegateBridge __Hotfix_CloseScoreRulePanel;
    private static DelegateBridge __Hotfix_OnRewardDetailButtonClick;
    private static DelegateBridge __Hotfix_ShowAchievementPanel;
    private static DelegateBridge __Hotfix_CloseAchievementPanel;
    private static DelegateBridge __Hotfix_OnRankDetailButtonClick;
    private static DelegateBridge __Hotfix_SetBossRankList;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_CreateBackgroundFx;
    private static DelegateBridge __Hotfix_DestroyBackgroundFx;
    private static DelegateBridge __Hotfix_OpenTagPanel;
    private static DelegateBridge __Hotfix_OnBossDetailButtonClick;
    private static DelegateBridge __Hotfix_OnAchievementListButtonClick;
    private static DelegateBridge __Hotfix_OnAttackButtonClick;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnAttack;
    private static DelegateBridge __Hotfix_remove_EventOnAttack;
    private static DelegateBridge __Hotfix_add_EventOnShowRankList;
    private static DelegateBridge __Hotfix_remove_EventOnShowRankList;
    private static DelegateBridge __Hotfix_add_EventOnShowBossDetail;
    private static DelegateBridge __Hotfix_remove_EventOnShowBossDetail;
    private static DelegateBridge __Hotfix_add_EventOnShowBossAchievement;
    private static DelegateBridge __Hotfix_remove_EventOnShowBossAchievement;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAncientCallBoss(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBossDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTagInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScoreRuleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowScoreRuleDescPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseScoreRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAchievementPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseAchievementPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBossRankList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackgroundFx(ConfigDataAncientCallBossInfo bossInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyBackgroundFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenTagPanel(ConfigDataHeroTagInfo tagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBossDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementListButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRankList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBossDetail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBossAchievement
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
