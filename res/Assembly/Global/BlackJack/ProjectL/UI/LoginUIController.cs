﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class LoginUIController : UIControllerBase
  {
    private int m_showServerButtonClickCount = 10;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./AnnouncePanel", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_announcePanelUIStateController;
    [AutoBind("./AnnouncePanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeAnnounceButton;
    [AutoBind("./AnnouncePanel/BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_announceBGButton;
    [AutoBind("./AnnouncePanel/Detail/LeftToggleScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_announceListScrollRect;
    [AutoBind("./AnnouncePanel/Detail/LeftToggleScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announceListContent;
    [AutoBind("./AnnouncePanel/Detail/LeftToggleScrollView/Viewport/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announceListPointItem;
    [AutoBind("./AnnouncePanel/Detail/LeftToggleScrollView/Viewport/BGContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announceListPointBGContent;
    [AutoBind("./AnnouncePanel/Prefab/ToggleItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleItemPrefab;
    [AutoBind("./AnnouncePanel/Prefab/ToggleItem/ItemContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./AnnouncePanel/Prefab/ToggleItem/ItemContent/Logo", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tabLogo;
    [AutoBind("./AnnouncePanel/Detail/InfoPanel/DetailScrollView/Viewport/Content/Text/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_announceContentText;
    [AutoBind("./AnnouncePanel/Detail/InfoPanel/DetailScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_detailScrollView;
    [AutoBind("./Margin/OpenAnnounceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_openAnnounceButton;
    [AutoBind("./Server", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_serverGameObject;
    [AutoBind("./Server/ButtonPanel/FastEnterGame", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_fastEnterGameToggle;
    [AutoBind("./Server/ButtonPanel/TestFirstBattle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_testFirstBattleToggle;
    [AutoBind("./Server/ButtonPanel/SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_saveServerConfigButton;
    [AutoBind("./ShowServerButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_showServerButton;
    [AutoBind("./Account/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_accountInputField;
    [AutoBind("./Password/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_passwordInputField;
    [AutoBind("./LoginButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_loginButton;
    [AutoBind("./SDKLoginButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sdkLoginButton;
    [AutoBind("./Message", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_messageGameObject;
    [AutoBind("./Account", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_accountGameObject;
    [AutoBind("./Password", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_passwordGameObject;
    [AutoBind("./SelectServers", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectServerButton;
    [AutoBind("./SelectServers/ServersNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectServerName;
    [AutoBind("./Message/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    [AutoBind("./Margin/ClientVersionText", AutoBindAttribute.InitState.Active, false)]
    private Text m_clientVersionText;
    [AutoBind("./Margin/SwitchUserButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_switchUserButton;
    [AutoBind("./Margin/LoginPCClient/Button", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_loginPCClientButton;
    [AutoBind("./Login/LogoImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_logoAnimController;
    [AutoBind("./Login", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_swordAnimController;
    private float m_hideMessageTime;
    private CommonUIStateController m_networkWatingStateController;
    private bool m_isStartAnimShowed;
    public static string m_accountName;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_OnEnable;
    private static DelegateBridge __Hotfix_SetServerInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnAccountTextChanged;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_ShowWaitingNet;
    private static DelegateBridge __Hotfix_OnLoginPCClientButtonClick;
    private static DelegateBridge __Hotfix_OnOpenUserCenterButtonClick;
    private static DelegateBridge __Hotfix_OnSelectServerButtonClick;
    private static DelegateBridge __Hotfix_get_IsLoginButtonClicked;
    private static DelegateBridge __Hotfix_set_IsLoginButtonClicked;
    private static DelegateBridge __Hotfix_OnLoginButtonClick;
    private static DelegateBridge __Hotfix_OnSaveServerConfigClick;
    private static DelegateBridge __Hotfix_OnToggleFastEnterGame;
    private static DelegateBridge __Hotfix_OnToggleTestFirstBattle;
    private static DelegateBridge __Hotfix_ShowStartAnimation;
    private static DelegateBridge __Hotfix_SetAccountName;
    private static DelegateBridge __Hotfix_EnablePDSDKLoginMode;
    private static DelegateBridge __Hotfix_ShowLoginedButton;
    private static DelegateBridge __Hotfix_GetAccountName;
    private static DelegateBridge __Hotfix_SetPassword;
    private static DelegateBridge __Hotfix_GetPassword;
    private static DelegateBridge __Hotfix_ShowMessage_0;
    private static DelegateBridge __Hotfix_ShowMessage_1;
    private static DelegateBridge __Hotfix_SetClientVersion;
    private static DelegateBridge __Hotfix_HideMessage;
    private static DelegateBridge __Hotfix_ShowAnnouncePanel;
    private static DelegateBridge __Hotfix_SetAnnounce_1;
    private static DelegateBridge __Hotfix_SetAnnounce_0;
    private static DelegateBridge __Hotfix_set_EnableLoginPCClientButton;
    private static DelegateBridge __Hotfix_OnOpenAnnouncePanel;
    private static DelegateBridge __Hotfix_OnCloseAnnouncePanel;
    private static DelegateBridge __Hotfix_OnShowServerButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCloseAnnouncePanel;
    private static DelegateBridge __Hotfix_remove_EventOnCloseAnnouncePanel;
    private static DelegateBridge __Hotfix_add_EventOnOpenAnnouncePanel;
    private static DelegateBridge __Hotfix_remove_EventOnOpenAnnouncePanel;
    private static DelegateBridge __Hotfix_add_EventOnLogin;
    private static DelegateBridge __Hotfix_remove_EventOnLogin;
    private static DelegateBridge __Hotfix_add_EventOnSaveServerConfig;
    private static DelegateBridge __Hotfix_remove_EventOnSaveServerConfig;
    private static DelegateBridge __Hotfix_add_EventSelectServer;
    private static DelegateBridge __Hotfix_remove_EventSelectServer;
    private static DelegateBridge __Hotfix_add_EventOpenUserCenter;
    private static DelegateBridge __Hotfix_remove_EventOpenUserCenter;
    private static DelegateBridge __Hotfix_add_EventLoginPCClient;
    private static DelegateBridge __Hotfix_remove_EventLoginPCClient;
    private static DelegateBridge __Hotfix_add_EventAccountTextChanged;
    private static DelegateBridge __Hotfix_remove_EventAccountTextChanged;
    private static DelegateBridge __Hotfix_set_IsTestFirstBattle;
    private static DelegateBridge __Hotfix_get_IsTestFirstBattle;

    private LoginUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerInfo(LoginUITask.ServerInfo serverInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAccountTextChanged(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitingNet(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLoginPCClientButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenUserCenterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectServerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsLoginButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLoginButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSaveServerConfigClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleFastEnterGame(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleTestFirstBattle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStartAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAccountName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnablePDSDKLoginMode(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowLoginedButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetAccountName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPassword(string password)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPassword()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, int time = 0, bool isOverride = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(string text, int time = 0, bool isOverride = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClientVersion(string ver)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAnnouncePanel(bool isShow, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnnounce(string announceText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAnnounce(List<LoginAnnouncement> announceList)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool EnableLoginPCClientButton
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowServerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnCloseAnnouncePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOpenAnnouncePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLogin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSaveServerConfig
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventSelectServer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOpenUserCenter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventLoginPCClient
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventAccountTextChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static bool IsTestFirstBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
