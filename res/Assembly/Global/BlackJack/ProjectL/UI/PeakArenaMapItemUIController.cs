﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaMapItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaMapItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_itemToggle;
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public ConfigDataPeakArenaBattleInfo m_peakArenaBattleInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetData;
    private static DelegateBridge __Hotfix_OnSelected;
    private static DelegateBridge __Hotfix_add_EventOnItemSelected;
    private static DelegateBridge __Hotfix_remove_EventOnItemSelected;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetData(ConfigDataPeakArenaBattleInfo peakArenaBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PeakArenaMapItemUIController> EventOnItemSelected
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
