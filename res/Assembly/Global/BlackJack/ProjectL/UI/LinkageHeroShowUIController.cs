﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LinkageHeroShowUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class LinkageHeroShowUIController : UIControllerBase
  {
    [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backButton;
    [AutoBind("./HeroShow/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImage;
    [AutoBind("./HeroShow/Heros/KongGui", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_kongGuiGameObject;
    [AutoBind("./HeroShow/Heros/Sakura", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sakuraGameObject;
    [AutoBind("./ToggleScrollView/Mask/ToggleGroup/ToggleKongGui", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_kongGuiToggle;
    [AutoBind("./ToggleScrollView/Mask/ToggleGroup/ToggleSakura", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_sakuraToggle;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_copyrightText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private LinkageHeroShowUITask m_linkageHeroShowUITask;
    private HeroShowComponent m_heroShowComponent;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnClose;
    private static DelegateBridge __Hotfix_OnHeroToggleClick;
    private static DelegateBridge __Hotfix_LoadHeroTeam;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroToggleClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadHeroTeam(GameObject teamGameObject, HeroBelongProduction heroBelongProduction)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
