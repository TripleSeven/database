﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceRecordHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class VoiceRecordHelper
  {
    private int m_recordIdSeed;
    private bool m_isRecording;
    private bool m_isCancelRecord;
    private int m_preRecordPos;
    private AudioClip m_recordClip;
    private int m_frequency;
    private DateTime m_startTime;
    private DateTime m_endTime;
    private string m_deviceName;
    private DateTime m_recordNextTickTime;
    private ChatChannel? m_currRecordChannel;
    private int VoiceMaxLength;
    private int VoiceMinLength;
    private int EncodeAndDecodeFrameSize;
    private bool m_isXFRecognizing;
    private LinkedList<string> m_xfWaitForRecognizeFilePathList;
    private LinkedList<string> m_xfRecognizedResultList;
    private XunfeiSDKWrapper m_xfWrapper;
    private static VoiceRecordHelper m_instance;
    private bool m_isRunningStartRecord;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_InitXFSDKManager;
    private static DelegateBridge __Hotfix_StartRecord;
    private static DelegateBridge __Hotfix_IsMicrophoneAvailable;
    private static DelegateBridge __Hotfix_OnStopRecord;
    private static DelegateBridge __Hotfix_StopRecord;
    private static DelegateBridge __Hotfix_IsRecording;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_IsArrivalMaxLength;
    private static DelegateBridge __Hotfix_IsTooShort;
    private static DelegateBridge __Hotfix_GetRecordLength;
    private static DelegateBridge __Hotfix_CancelRecord;
    private static DelegateBridge __Hotfix_GetSaveAudioFullPath;
    private static DelegateBridge __Hotfix_TickForRecordVoice;
    private static DelegateBridge __Hotfix_StartXFRecognize;
    private static DelegateBridge __Hotfix_OnXFSpeechRecognizedSuccess;
    private static DelegateBridge __Hotfix_OnXFSpeechRecognizeFailed;
    private static DelegateBridge __Hotfix_DeleteAudioTempFile;
    private static DelegateBridge __Hotfix_ClearRecord;
    private static DelegateBridge __Hotfix_AllocRecordId;
    private static DelegateBridge __Hotfix_add_EventOnVoiceRecordTimeout;
    private static DelegateBridge __Hotfix_remove_EventOnVoiceRecordTimeout;
    private static DelegateBridge __Hotfix_get_XFRecognizedResultList;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_Instance;
    private static DelegateBridge __Hotfix_add_m_eventStopRecord;
    private static DelegateBridge __Hotfix_remove_m_eventStopRecord;

    [MethodImpl((MethodImplOptions) 32768)]
    private VoiceRecordHelper(string deviceName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitXFSDKManager(XunfeiSDKWrapper wrapper)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartRecord(ChatChannel channel, Action onMicrophoneStarted)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMicrophoneAvailable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStopRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRecording()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArrivalMaxLength()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTooShort()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetRecordLength()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSaveAudioFullPath(int voiceRecordId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickForRecordVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartXFRecognize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnXFSpeechRecognizedSuccess(string returnTxt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnXFSpeechRecognizeFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeleteAudioTempFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AllocRecordId()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ChatChannel> EventOnVoiceRecordTimeout
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public LinkedList<string> XFRecognizedResultList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static VoiceRecordHelper Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action m_eventStopRecord
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
