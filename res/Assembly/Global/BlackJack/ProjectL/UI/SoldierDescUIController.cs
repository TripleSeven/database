﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SoldierDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SoldierDescUIController : UIControllerBase
  {
    [AutoBind("./Faction/JobImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailGraphic;
    [AutoBind("./Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierRangeText;
    [AutoBind("./Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMoveText;
    [AutoBind("./Faction/TypeBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImg;
    [AutoBind("./Faction/TypeBgImage2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_typeBgImg2;
    [AutoBind("./TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierTitleText;
    [AutoBind("./Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierWeakText;
    [AutoBind("./Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierStrongText;
    [AutoBind("./HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPValueText;
    [AutoBind("./DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFValueText;
    [AutoBind("./AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATValueText;
    [AutoBind("./MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFValueText;
    [AutoBind("./HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPAddText;
    [AutoBind("./DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFAddText;
    [AutoBind("./AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATAddText;
    [AutoBind("./MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFAddText;
    private UISpineGraphic m_soldierGraphic;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSoldierDesc_0;
    private static DelegateBridge __Hotfix_InitSoldierDesc_1;
    private static DelegateBridge __Hotfix_SetSoldierDetailPanel;
    private static DelegateBridge __Hotfix_CalcPropValue;
    private static DelegateBridge __Hotfix_ShowPanel;
    private static DelegateBridge __Hotfix_ClosePanel;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierDesc(ConfigDataSoldierInfo soldierInfo, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSoldierDesc(
      ConfigDataSoldierInfo soldierInfo,
      BattleHero hero,
      List<TrainingTech> techs,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel(
      ConfigDataSoldierInfo soldierInfo,
      HeroPropertyComputer computer,
      string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropValue(int v0, int v1, bool isAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      int team,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
