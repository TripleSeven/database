﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DataNoticeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DataNoticeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/Scroll View/Viewport/Content/LinkTextButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_privacyWebLinkButton;
    [AutoBind("./Detail/DisagreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_disagreeButton;
    [AutoBind("./Detail/AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_agreeButton;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnCloseButtonClicked;
    private static DelegateBridge __Hotfix_OnPrivacyWebLinkButtonClicked;
    private static DelegateBridge __Hotfix_OnDisagreeButtonClicked;
    private static DelegateBridge __Hotfix_OnAgreeButtonClicked;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_OnUIClosed;
    private static DelegateBridge __Hotfix_add_EventOnClosed;
    private static DelegateBridge __Hotfix_remove_EventOnClosed;
    private static DelegateBridge __Hotfix_add_EventOnPrivacyWebLinkButtonClicked;
    private static DelegateBridge __Hotfix_remove_EventOnPrivacyWebLinkButtonClicked;
    private static DelegateBridge __Hotfix_add_EventOnDisagreeButtonClicked;
    private static DelegateBridge __Hotfix_remove_EventOnDisagreeButtonClicked;
    private static DelegateBridge __Hotfix_add_EventOnAgreeButtonClicked;
    private static DelegateBridge __Hotfix_remove_EventOnAgreeButtonClicked;

    private DataNoticeUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~DataNoticeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPrivacyWebLinkButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisagreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAgreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClosed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPrivacyWebLinkButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnDisagreeButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAgreeButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
