﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaMapChooseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaMapChooseUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_mapChooseAnimation;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./InviteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteButton;
    [AutoBind("./InviteButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_inviteButtonAnimation;
    [AutoBind("./Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_mapContent;
    [AutoBind("./Scroll View/Viewport/Content/MapInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaMapChooseUITask m_peakArenaMapChooseUITask;
    private string m_userID;
    private PeakArenaMapItemUIController m_selectMapItem;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_OnBGButtonClick;
    private static DelegateBridge __Hotfix_OnInviteButtonClick;
    private static DelegateBridge __Hotfix_OnMapClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMapClick(PeakArenaMapItemUIController itemUIController)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
