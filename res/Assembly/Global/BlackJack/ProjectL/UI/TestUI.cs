﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TestUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class TestUI : MonoBehaviour
  {
    private ClientBattle m_clientBattle;
    private ClientWorld m_clientWorld;
    private CollectionActivityWorld m_collectionActivityWorld;
    private int m_updateCount;
    private float m_fps;
    private float m_lastFpsResetTime;
    private float m_lastMemoryWarningTime;
    private float m_timeScale;
    private bool m_isTimePaused;
    private bool m_isShowTestToolToggle;
    private bool m_isShowTestToolBar;
    private bool m_isShowTouches;
    private bool m_isLowResolution;
    private bool m_isFrameRateLimit;
    private bool m_isShowTime;
    private bool m_isShowMemory;
    private bool m_isShowTrigger;
    private bool m_isShowAchievement;
    private bool m_isShowGMCommand;
    private int m_initScreenWidth;
    private int m_initScreenHeight;
    private string m_GMCommandStr;
    private int m_unit;
    private GUIStyle m_buttonStyle;
    private GUIStyle m_textFieldStyle;
    private GUIStyle m_textStyle;
    private GUIStyle m_textStyleGray;
    private GUIStyle m_textStyleSmall;
    private GUIStyle m_textStyleSmallGray;
    private GUIStyle m_textStyleSmallCenter;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_InitializeBattle;
    private static DelegateBridge __Hotfix_UninitializeBattle;
    private static DelegateBridge __Hotfix_InitializeWorld;
    private static DelegateBridge __Hotfix_UninitializeWorld;
    private static DelegateBridge __Hotfix_InitializeCollectionActivityWorld;
    private static DelegateBridge __Hotfix_UninitializeCollectionActivityWorld;
    private static DelegateBridge __Hotfix_SetTimeScale;
    private static DelegateBridge __Hotfix_ToolButton;
    private static DelegateBridge __Hotfix_ToolToggle;
    private static DelegateBridge __Hotfix_ToolTextField;
    private static DelegateBridge __Hotfix_TextLine;
    private static DelegateBridge __Hotfix_GUIBattleTestTools;
    private static DelegateBridge __Hotfix_GUIWorldTestTools;
    private static DelegateBridge __Hotfix_GUIGMReloginButton;
    private static DelegateBridge __Hotfix_GUIToolToggle;
    private static DelegateBridge __Hotfix_GUISpeedToggle;
    private static DelegateBridge __Hotfix_GUITriggerToggle;
    private static DelegateBridge __Hotfix_GUIAchievementToggle;
    private static DelegateBridge __Hotfix_GUIAudioToggle;
    private static DelegateBridge __Hotfix_GUITouchToggle;
    private static DelegateBridge __Hotfix_GUIMultiTouchToggle;
    private static DelegateBridge __Hotfix_GUIResolutionToggle;
    private static DelegateBridge __Hotfix_GUIFrameRateToggle;
    private static DelegateBridge __Hotfix_GUIStopInBackgroundToggle;
    private static DelegateBridge __Hotfix_GUITimeToggle;
    private static DelegateBridge __Hotfix_GUIMemoryToggle;
    private static DelegateBridge __Hotfix_GUILowMemoryTest;
    private static DelegateBridge __Hotfix_GUIGCTest;
    private static DelegateBridge __Hotfix_GUIHud;
    private static DelegateBridge __Hotfix_GUITextPanel;
    private static DelegateBridge __Hotfix_UnloadAllAbTest;
    private static DelegateBridge __Hotfix_GUIGMCommandToggle;
    private static DelegateBridge __Hotfix_GUIGMCommand;
    private static DelegateBridge __Hotfix_LogResources;
    private static DelegateBridge __Hotfix_CompareObjectRuntimeMemroySize;
    private static DelegateBridge __Hotfix_GridPositionToScreenPosition;
    private static DelegateBridge __Hotfix_GUIBattleActor;
    private static DelegateBridge __Hotfix_GUIBattleActors;
    private static DelegateBridge __Hotfix_GUIBattleMap;
    private static DelegateBridge __Hotfix_CombatPositionToScreenPosition;
    private static DelegateBridge __Hotfix_GUICombatActor;
    private static DelegateBridge __Hotfix_GUICombatActors;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_TestMemoryWarnning;
    private static DelegateBridge __Hotfix_ComputeFps;
    private static DelegateBridge __Hotfix_UpdateTestUIBackground;
    private static DelegateBridge __Hotfix_OnGUI;
    private static DelegateBridge __Hotfix_InitStyle;
    private static DelegateBridge __Hotfix_PreProcessGMCommand;
    private static DelegateBridge __Hotfix_SendGMCommand;
    private static DelegateBridge __Hotfix_add_EventOnExitBattle;
    private static DelegateBridge __Hotfix_remove_EventOnExitBattle;
    private static DelegateBridge __Hotfix_add_EventOnRestartBattle;
    private static DelegateBridge __Hotfix_remove_EventOnRestartBattle;
    private static DelegateBridge __Hotfix_add_EventOnReplayBattle;
    private static DelegateBridge __Hotfix_remove_EventOnReplayBattle;
    private static DelegateBridge __Hotfix_add_EventOnPrepareBattle;
    private static DelegateBridge __Hotfix_remove_EventOnPrepareBattle;
    private static DelegateBridge __Hotfix_add_EventOnStopBattle;
    private static DelegateBridge __Hotfix_remove_EventOnStopBattle;

    [MethodImpl((MethodImplOptions) 32768)]
    public TestUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeBattle(ClientBattle clientBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitializeBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeWorld(ClientWorld clientWorld)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitializeWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeCollectionActivityWorld(CollectionActivityWorld world)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitializeCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTimeScale(float ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ToolButton(ref int x, ref int y, float size, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ToolToggle(ref int x, ref int y, float size, string text, bool oldValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string ToolTextField(ref int x, ref int y, float size, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TextLine(int x, ref int y, string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleTestTools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIWorldTestTools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGMReloginButton(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIToolToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUISpeedToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITriggerToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIAchievementToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIAudioToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITouchToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIMultiTouchToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIResolutionToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIFrameRateToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIStopInBackgroundToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITimeToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIMemoryToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUILowMemoryTest(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGCTest(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIHud()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUITextPanel(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnloadAllAbTest(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGMCommandToggle(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIGMCommand(ref int x, ref int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LogResources()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareObjectRuntimeMemroySize(
      TestUI.ObjectAndSize os1,
      TestUI.ObjectAndSize os2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToScreenPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUIBattleMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 CombatPositionToScreenPosition(Vector2i p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUICombatActor(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GUICombatActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestMemoryWarnning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeFps()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTestUIBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitStyle(int unit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool PreProcessGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SendGMCommand(string cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnExitBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRestartBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReplayBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPrepareBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnStopBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public struct ObjectAndSize
    {
      public UnityEngine.Object m_object;
      public int m_size;
    }
  }
}
