﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSkinChangeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroSkinChangeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./FacelifPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_facelifPanel;
    [AutoBind("./FacelifPanel/ReturnBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./FacelifPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_blackBgButton;
    [AutoBind("./FacelifPanel/Detail/SkinSelectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_skinsScrollRect;
    [AutoBind("./FacelifPanel/Detail/SkinSelectPanel/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinSelectContent;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/JobScrollView/Viewport/JobGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinJobGroup;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/JobScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_infoHeroJobScrollRect;
    [AutoBind("./FacelifPanel/Detail/InfoPanel/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skinCharGo;
    [AutoBind("./FacelifPanel/Detail/Scrollrect/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skinDescText;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupStateCtrl;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/FromText/Title/DetailText ", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_fromText;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/GetOn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSwitchButton;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/Buy", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoBuyButton;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/Buy/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoBuyValueText;
    [AutoBind("./FacelifPanel/Detail/ButtonGroupState/Buy/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currencyImage;
    [AutoBind("./FacelifPanel/PlayerResource", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currencyGameObject;
    [AutoBind("./FacelifPanel/PlayerResource/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skinTicketCountText;
    [AutoBind("./FacelifPanel/PlayerResource/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skinTicketAddButton;
    [AutoBind("./FacelifPanel/PlayerResource/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_totalCurrencyImage;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private Hero m_hero;
    private string m_mode;
    private int m_heroSkinInfoId;
    private int m_curCharSkinIndex;
    private ConfigDataHeroInfo m_heroInfo;
    private UISpineGraphic m_playerHeroGraphic;
    private ScrollSnapCenter m_skinsScrollSnapCenter;
    private HeroCharUIController m_heroCharUIController;
    private HeroCharSkinItemUIController m_curSelectHeroCharSkinItemCtrl;
    private List<HeroCharSkinItemUIController> m_charSkinCtrlsList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInHeroCharSkin;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_SetSelectSkinPanelInfo;
    private static DelegateBridge __Hotfix_CreateHeroDefaultSkin;
    private static DelegateBridge __Hotfix_SetSkinInfoPanel;
    private static DelegateBridge __Hotfix_SetNotDefaultSkinInfoPanel;
    private static DelegateBridge __Hotfix_OnHeroSkinJobItemClick;
    private static DelegateBridge __Hotfix_RefreshCurrencyState;
    private static DelegateBridge __Hotfix_SetButtonState;
    private static DelegateBridge __Hotfix_SetHeroSkinCharAndSpine;
    private static DelegateBridge __Hotfix_SetDefaultSkinInfoPanel;
    private static DelegateBridge __Hotfix_OnHeroCharSkinItemClick;
    private static DelegateBridge __Hotfix_SkinsScrollSnapCenter_OnEndDrag;
    private static DelegateBridge __Hotfix_OnInfoBuyButtonClick;
    private static DelegateBridge __Hotfix_OnInfoSwitchButtonClick;
    private static DelegateBridge __Hotfix_ShowSwitchHeroSpinePanel;
    private static DelegateBridge __Hotfix_ShowSwitchDefaultHeroSpinePanel;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnSkinTicketButtonClick;
    private static DelegateBridge __Hotfix_CloseHeroSkinChangePanel;
    private static DelegateBridge __Hotfix_OnBuyButtonClick;
    private static DelegateBridge __Hotfix_ClearData;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnBuySkin;
    private static DelegateBridge __Hotfix_remove_EventOnBuySkin;
    private static DelegateBridge __Hotfix_add_EventOnWearCharSkin;
    private static DelegateBridge __Hotfix_remove_EventOnWearCharSkin;
    private static DelegateBridge __Hotfix_add_EventOnTakeOffCharSkin;
    private static DelegateBridge __Hotfix_remove_EventOnTakeOffCharSkin;
    private static DelegateBridge __Hotfix_add_EventOnSkinChangedPreview;
    private static DelegateBridge __Hotfix_remove_EventOnSkinChangedPreview;
    private static DelegateBridge __Hotfix_add_EventOnSkinTicketButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSkinTicketButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnWearModelSkin;
    private static DelegateBridge __Hotfix_remove_EventOnWearModelSkin;
    private static DelegateBridge __Hotfix_add_EventOnTakeOffModelSkin;
    private static DelegateBridge __Hotfix_remove_EventOnTakeOffModelSkin;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSkinChangeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroCharSkin(int heroSkinInfoId, string mode, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSelectSkinPanelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateHeroDefaultSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkinInfoPanel(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetNotDefaultSkinInfoPanel(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSkinJobItemClick(ConfigDataJobConnectionInfo heroJobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshCurrencyState(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetButtonState(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroSkinCharAndSpine(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDefaultSkinInfoPanel(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroCharSkinItemClick(HeroCharSkinItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkinsScrollSnapCenter_OnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoSwitchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchHeroSpinePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchDefaultHeroSpinePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseHeroSkinChangePanel(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuySkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnWearCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnTakeOffCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, int> EventOnSkinChangedPreview
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinTicketButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int> EventOnWearModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnTakeOffModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
