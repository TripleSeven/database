﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MusicAppreciateUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MusicAppreciateUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Disc1", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_disc1Toggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Disc2", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_disc2Toggle;
    [AutoBind("./BGImage/Rotation", AutoBindAttribute.InitState.NotInit, false)]
    private Animator m_rotationAnimator;
    [AutoBind("./BGImage/Play", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playState;
    [AutoBind("./Margin/MusicList/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_musicItemContentTransform;
    [AutoBind("./Margin/MusicList/ScrollView/Viewport/Content/MusicItem", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_musicItemPrefab;
    private ProjectLPlayerContext m_playerContext;
    private ClientConfigDataLoader m_configDataLoader;
    private MusicAppreciateUITask m_musicAppreciateUITask;
    private GameObjectPool<MusicAppreciateItemUIController> m_musicItemPool;
    private Toggle m_selectToggle;
    private List<MusicAppreciateItemUIController> m_musicItemUIControllerList;
    private ConfigDataMusicAppreciateTable m_playMusicInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewMusicList;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnMusicClick;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnDiscToggleClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public MusicAppreciateUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewMusicList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMusicClick(MusicAppreciateItemUIController itemUIController, bool isPlay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDiscToggleClick(Toggle selectToggle)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
