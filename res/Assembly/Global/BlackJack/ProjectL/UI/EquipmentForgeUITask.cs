﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentForgeUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class EquipmentForgeUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Hero m_hero;
    private int m_slot;
    private ulong m_instanceId;
    private EquipmentForgeUIController.ForgeState m_forgeState;
    private EquipmentForgeUIController m_equipmentForgeUIController;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnReturn;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnEquipmentEnhance;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnEquipmentStarLevelUp;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnEnhanceNeedItemClick;
    private static DelegateBridge __Hotfix_EquipmentForge_OnGotoGetPath;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnEnchantButtonClick;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnEnchantSaveButtonClick;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnCrystalNotEnough;
    private static DelegateBridge __Hotfix_EquipmentForgeUIController_OnLockButtonClick;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentForgeUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEquipmentEnhance(
      ulong instanceId,
      List<ulong> materialIds,
      Action OnSucceed,
      int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEquipmentStarLevelUp(
      ulong instanceId,
      ulong materialId,
      Action OnSucceed,
      int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnGoldAddButtonClick(
      ulong instanceId,
      int slot,
      EquipmentForgeUIController.ForgeState forgeState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEnhanceNeedItemClick(
      GoodsType goodsType,
      int goodsId,
      int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForge_OnGotoGetPath(GetPathData getPath, NeedGoods needGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEnchantButtonClick(
      ulong instanceId,
      ulong stoneInstanceId,
      Action<int, List<CommonBattleProperty>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnEnchantSaveButtonClick(Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EquipmentForgeUIController_OnLockButtonClick(
      ulong instanceId,
      EquipmentForgeUIController.ForgeState forgeState,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
