﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ShareTenSelectCardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ShareTenSelectCardUIController : UIControllerBase
  {
    [AutoBind("./HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroGroup;
    [AutoBind("./Share/PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./Share/PlayerInfo/Lvbg/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLvText;
    [AutoBind("./Share/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverNameText;
    private List<Transform> m_heroDummyParentList;
    private List<GameObject> m_heroGameObject;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_UpdateSharePlayerInfo;
    private static DelegateBridge __Hotfix_Compare;

    [MethodImpl((MethodImplOptions) 32768)]
    public ShareTenSelectCardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh(List<int> m_heroIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSharePlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int Compare(int leftHeroID, int rightHeroID)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
