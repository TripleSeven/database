﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BeforeJoinGuildInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BeforeJoinGuildInfoUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelAnimation;
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charLevelText;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_charIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameDummy;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_charName;
    [AutoBind("./BGImages/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildNameText;
    [AutoBind("./Declaration/DeclarationText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_guildDeclarationText;
    [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildDetailButton;
    [AutoBind("./DetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildDetailButtonAnimation;
    [AutoBind("./JoinInButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildJoinButton;
    [AutoBind("./JoinInButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guildJoinButtonAnimation;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GuildSearchInfo m_selectGuildInfo;
    private BeforeJoinGuildUIController m_guildUIController;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetInfoPanelState;
    private static DelegateBridge __Hotfix_SetSelectGuildInfo;
    private static DelegateBridge __Hotfix_OnGuildJoinClick;
    private static DelegateBridge __Hotfix_OnGuildDetailClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(BeforeJoinGuildUIController guildUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetInfoPanelState(bool hasData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectGuildInfo(GuildSearchInfo guildSearchInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildJoinClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildDetailClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
