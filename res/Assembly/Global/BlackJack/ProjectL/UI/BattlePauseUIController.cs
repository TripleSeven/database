﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattlePauseUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattlePauseUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Objective/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Objective/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exitButton;
    [AutoBind("./Objective/AchievementButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementButton;
    [AutoBind("./Objective/SetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_settingButton;
    [AutoBind("./Objective/StrategyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_strategyButton;
    [AutoBind("./Objective/AchievementButton/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementButtonText;
    [AutoBind("./Objective/Turn", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_turnGameObject;
    [AutoBind("./Objective/Turn/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_turnText;
    [AutoBind("./Objective/WinDesc/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionGroupGameObject;
    [AutoBind("./Objective/LoseDesc/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_loseConditionGroupGameObject;
    [AutoBind("./Objective/StarDesc/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starConditionGroupGameObject;
    [AutoBind("./Objective/Map", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_mapTransform;
    [AutoBind("./Objective/Map/Terrain", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapTerrainGameObject;
    [AutoBind("./Objective/Map/Region", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapRegionGameObject;
    [AutoBind("./Objective/Map/Actor", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActorGameObject;
    [AutoBind("./Achievement", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_achievementUIStateController;
    [AutoBind("./Achievement/Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_achievementScrollRect;
    [AutoBind("./Achievement/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_achievementBackgroundButton;
    [AutoBind("./Strategy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_strategyUIStateController;
    [AutoBind("./Strategy/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_strategyBackgroundButton;
    [AutoBind("./Strategy/Panel/Left/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_strategyContentText;
    [AutoBind("./Strategy/Panel/Right/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_danmakuContent;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/Terrain", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapTerrainPrefab;
    [AutoBind("./Prefabs/Actor0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActor0Prefab;
    [AutoBind("./Prefabs/Actor1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActor1Prefab;
    [AutoBind("./Prefabs/Actor2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapActor2Prefab;
    [AutoBind("./Prefabs/Reach", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mapReachPrefab;
    [AutoBind("./Prefabs/ConditionInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_conditionPrefab;
    [AutoBind("./Prefabs/DanmakuItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_danmakuItemPrefab;
    private List<DanmakuItemUIController> m_danmakuItemUICtrlList;
    private List<BattleAchievementItemUIController> m_achievementItems;
    private int m_achievementCountMax;
    private int m_mapWidth;
    private int m_mapHeight;
    private Vector2 m_mapTileSize;
    private bool m_isOpened;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_IsOpened;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_ShowStrategyButton;
    private static DelegateBridge __Hotfix_SetWinLoseConditions;
    private static DelegateBridge __Hotfix_SetConditionInfo;
    private static DelegateBridge __Hotfix_AddCondition;
    private static DelegateBridge __Hotfix_SetStarCondition;
    private static DelegateBridge __Hotfix_SetAchievement;
    private static DelegateBridge __Hotfix_SetAchievementCount;
    private static DelegateBridge __Hotfix_ShowAchievement;
    private static DelegateBridge __Hotfix_HideAchievement;
    private static DelegateBridge __Hotfix_AddAchievementItem;
    private static DelegateBridge __Hotfix_ClearAchievementItems;
    private static DelegateBridge __Hotfix_SetTurn;
    private static DelegateBridge __Hotfix_SetMap;
    private static DelegateBridge __Hotfix_ClearMap;
    private static DelegateBridge __Hotfix_SetMapActors;
    private static DelegateBridge __Hotfix_CreateMapActors;
    private static DelegateBridge __Hotfix_ClearMapActors;
    private static DelegateBridge __Hotfix_SetMapReachRegion;
    private static DelegateBridge __Hotfix_ClearMapReachRegion;
    private static DelegateBridge __Hotfix_ClearMapAll;
    private static DelegateBridge __Hotfix_GridPositionToMapPosition;
    private static DelegateBridge __Hotfix_OnExitButtonClick;
    private static DelegateBridge __Hotfix_OnAchievementButtonClick;
    private static DelegateBridge __Hotfix_OnStrategyButtonClick;
    private static DelegateBridge __Hotfix_UpdateStrategyPanel;
    private static DelegateBridge __Hotfix_Co_StartDanmakuItemsTweenPos;
    private static DelegateBridge __Hotfix_OnStrategyBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnSetButtonClick;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnAchievementBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnExit;
    private static DelegateBridge __Hotfix_remove_EventOnExit;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnShowPlayerSetting;
    private static DelegateBridge __Hotfix_remove_EventOnShowPlayerSetting;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattlePauseUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStrategyButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWinLoseConditions(string winDesc, string loseDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConditionInfo(GameObject parent, string conditionStrs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCondition(GameObject parent, string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStarCondition(BattleType battleType, int starTurnMax, int starDeadMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievement(BattleLevelAchievement[] achievements, BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievementCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideAchievement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddAchievementItem(
      ConfigDataBattleAchievementInfo achievementInfo,
      List<Goods> rewards,
      GameObject prefab,
      bool complete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAchievementItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTurn(int turn, int turnMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMap(BattleMap map)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMapActors(
      List<GridPosition> team0,
      List<GridPosition> team1,
      List<GridPosition> teamNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapActors(List<GridPosition> positions, GameObject prefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMapReachRegion(List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapReachRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearMapAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GridPositionToMapPosition(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrategyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStrategyPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_StartDanmakuItemsTweenPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStrategyBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAchievementBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnExit
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowPlayerSetting
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
