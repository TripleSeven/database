﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MailListUIController : UIControllerBase
  {
    private List<MailItemUIController> m_itemUICtrlList;
    public MailDetailUIController m_mailDetailUICtrl;
    public MailItemUIController m_curMailItemCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./MailPanel/ListScrollView/Viewport/Content/MailItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mailItemPrefab;
    [AutoBind("./MailPanel/MailDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mailDetailGo;
    [AutoBind("./MailPanel/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_listScrollRect;
    [AutoBind("./MailPanel/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mailListContent;
    [AutoBind("./MailPanel/ListScrollView/BGViewport/BGContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mailListPointBgContent;
    [AutoBind("./MailPanel/ListScrollView/BGViewport/BGContent/Point", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mailListPointItem;
    [AutoBind("./MailPanel/CloseButton ", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./MailPanel/GetAllButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getAllButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateMailInfoList;
    private static DelegateBridge __Hotfix_ReSetScrollViewToTop;
    private static DelegateBridge __Hotfix_UpdateCurrentSelectedMailDetail;
    private static DelegateBridge __Hotfix_OnMailItemButtonClick;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnGetAllButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnMailItemButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnMailItemButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGetAllAttachments;
    private static DelegateBridge __Hotfix_remove_EventOnGetAllAttachments;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMailInfoList(
      List<Mail> mailInfoList,
      Mail selectedMail = null,
      bool needReSetScrollView = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReSetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCurrentSelectedMailDetail(Mail mailInfo, bool listIsEmpty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMailItemButtonClick(MailItemUIController obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<Mail> EventOnMailItemButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetAllAttachments
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
