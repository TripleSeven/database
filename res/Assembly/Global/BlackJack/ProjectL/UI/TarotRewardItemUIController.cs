﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TarotRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TarotRewardItemUIController : UIControllerBase
  {
    [AutoBind("./TarotIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tarotIconImage;
    [AutoBind("./RewardItemDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardItemDummy;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private GameObject m_rewardPrefab;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(Goods goods, int tarotID, GameObject rewardPrefab, bool hasGot)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
