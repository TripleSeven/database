﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInviteUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomInviteUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_friendToggle;
    [AutoBind("./Panel/ToggleGroup/Nearly", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recentToggle;
    [AutoBind("./Panel/ToggleGroup/Sociaty", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_guildToggle;
    [AutoBind("./Panel/ToggleGroup/Sociaty/UnopenMask", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildToggleUnopenMask;
    [AutoBind("./Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    [AutoBind("./Panel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_inviteScrollRect;
    [AutoBind("./Panel/InvitedCount/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteCountTitleText;
    [AutoBind("./Panel/InvitedCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteCountValueText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/PlayerListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_invitePlayerListItemPrefab;
    private List<TeamRoomInvitePlayerListItemUIController> m_playerListItems;
    private bool m_isIgnoreToggleEvent;
    private bool m_isOpened;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetPlayerType;
    private static DelegateBridge __Hotfix_SetPlayers;
    private static DelegateBridge __Hotfix_IsRecentTeamBattlePlayer;
    private static DelegateBridge __Hotfix_AddPlayerListItem;
    private static DelegateBridge __Hotfix_ClearPlayerListItems;
    private static DelegateBridge __Hotfix_UpdatePlayerStatus;
    private static DelegateBridge __Hotfix_ShowGildToggleUnopenMask;
    private static DelegateBridge __Hotfix_OnFriendToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRecentToggleValueChanged;
    private static DelegateBridge __Hotfix_OnGuildToggleValueChanged;
    private static DelegateBridge __Hotfix_OnGuildUnopenMaskClick;
    private static DelegateBridge __Hotfix_OnConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnCancelButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnConfirm;
    private static DelegateBridge __Hotfix_remove_EventOnConfirm;
    private static DelegateBridge __Hotfix_add_EventOnCancel;
    private static DelegateBridge __Hotfix_remove_EventOnCancel;
    private static DelegateBridge __Hotfix_add_EventOnChangePlayerType;
    private static DelegateBridge __Hotfix_remove_EventOnChangePlayerType;

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerType(TeamRoomInvitePlayerType playerType, bool canChangePlayerType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayers(List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRecentTeamBattlePlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPlayerListItem(
      string userId,
      string name,
      int headIconId,
      int level,
      bool isRecent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearPlayerListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerStatus(string userId, TeamRoomPlayerInviteState inviteState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGildToggleUnopenMask(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecentToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildUnopenMaskClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<List<string>> EventOnConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TeamRoomInvitePlayerType> EventOnChangePlayerType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
