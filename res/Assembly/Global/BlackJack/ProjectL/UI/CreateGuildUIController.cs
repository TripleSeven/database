﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CreateGuildUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CreateGuildUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_createGuildPanelAnimation;
    [AutoBind("./Detail/SociatyNameInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildNameInputField;
    [AutoBind("./Detail/SociatyDeclarationInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_guildDeclarationInputField;
    [AutoBind("./Detail/LevelInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_levelInputField;
    [AutoBind("./Detail/LevelInputField/PreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_forwardLevelButton;
    [AutoBind("./Detail/LevelInputField/AftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backwardLevelButton;
    [AutoBind("./Detail/ValueGroup/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_requireItemIamge;
    [AutoBind("./Detail/ApproveGroup/AutoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_approveAutoToggle;
    [AutoBind("./Detail/ApproveGroup/ChairmanButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_approveChairmanToggle;
    [AutoBind("./Detail/GreatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_colseButton;
    [AutoBind("./BackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_requireLevel;
    private GuildUITask m_guildUITask;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_CreateGuild;
    private static DelegateBridge __Hotfix_RefreshLevelDisplay;
    private static DelegateBridge __Hotfix_OnCreateGuildClick;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnForwardLevelClick;
    private static DelegateBridge __Hotfix_OnBackwardLevelClick;
    private static DelegateBridge __Hotfix_OnBGClick;
    private static DelegateBridge __Hotfix_OnLevelInputFiledEndEdit;
    private static DelegateBridge __Hotfix_OnGuildDeclaratioInputFiledEndEdit;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshLevelDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateGuildClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnForwardLevelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackwardLevelClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelInputFiledEndEdit(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildDeclaratioInputFiledEndEdit(string content)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
