﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RewardHeroUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RewardHeroUIController : UIControllerBase
  {
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./LevelAndExp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelAndExpGameObject;
    [AutoBind("./LevelAndExp/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./LevelAndExp/EXPNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_expText;
    [AutoBind("./LevelAndExp/ExpProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_expImage;
    [AutoBind("./LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpGameObject;
    [AutoBind("./EffectRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_effectRoot;
    private float m_heroExpTotalWidth;
    private Hero m_hero;
    private float m_singleAddExp;
    private float m_beforeHeroExp;
    private int m_finalHeroExp;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHero;
    private static DelegateBridge __Hotfix_ShowExpAndLevel;
    private static DelegateBridge __Hotfix_AddExpBarWidth;
    private static DelegateBridge __Hotfix_UpdateTextValue;
    private static DelegateBridge __Hotfix_SetLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExpAndLevel(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AddExpBarWidth(float intervalTime = 2f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTextValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
