﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CurrencyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CurrencyUIController : UIControllerBase
  {
    [AutoBind("./Currency1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency1Obj;
    [AutoBind("./Currency1/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency1Icon;
    [AutoBind("./Currency1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency1CountText;
    [AutoBind("./Currency1/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1AddButton;
    [AutoBind("./Currency1/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency1DescriptionButton;
    [AutoBind("./Currency2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency2Obj;
    [AutoBind("./Currency2/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency2Icon;
    [AutoBind("./Currency2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency2CountText;
    [AutoBind("./Currency2/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2AddButton;
    [AutoBind("./Currency2/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency2DescriptionButton;
    [AutoBind("./Currency3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_currency3Obj;
    [AutoBind("./Currency3/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currency3Icon;
    [AutoBind("./Currency3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currency3CountText;
    [AutoBind("./Currency3/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3AddButton;
    [AutoBind("./Currency3/DescriptionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currency3DescriptionButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private Dictionary<int, GoodsType> m_currencyDic;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetDefaultState;
    private static DelegateBridge __Hotfix_SetCurrency;
    private static DelegateBridge __Hotfix_RefreshDisplay;
    private static DelegateBridge __Hotfix_SetAddButtonState;
    private static DelegateBridge __Hotfix_add_EventOnAddButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAddButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public CurrencyUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefaultState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrency(int pos, GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAddButtonState(
      GameObject currencyGameObject,
      Image icon,
      Text countText,
      Button descriptionButton,
      Button addButton,
      GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GoodsType> EventOnAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
