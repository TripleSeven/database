﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityWaypointUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityWaypointUIController : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./TestText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_testText;
    [AutoBind("./WorldHitImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_hitGameObject;
    private Text m_nameText;
    private Image m_nameBackgroundImage;
    private GameObject m_redPointGameObject;
    private Text m_redPointCountText;
    private ConfigDataCollectionActivityWaypointInfo m_waypointInfo;
    private bool m_isPointerDown;
    private bool m_ignoreClick;
    private float m_initNameBackgroundWidth;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetWaypoint;
    private static DelegateBridge __Hotfix_SetState;
    private static DelegateBridge __Hotfix_SetRedPointCount;
    private static DelegateBridge __Hotfix_SetCanClick;
    private static DelegateBridge __Hotfix_CanClick;
    private static DelegateBridge __Hotfix_IgnoreClick;
    private static DelegateBridge __Hotfix_GetClickTransform;
    private static DelegateBridge __Hotfix_OnPointerDown;
    private static DelegateBridge __Hotfix_OnPointerUp;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_add_EventOnPointerDown;
    private static DelegateBridge __Hotfix_remove_EventOnPointerDown;
    private static DelegateBridge __Hotfix_add_EventOnPointerUp;
    private static DelegateBridge __Hotfix_remove_EventOnPointerUp;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    private CollectionActivityWaypointUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWaypoint(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetState(CollectionActivityWaypointStateType state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRedPointCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetClickTransform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
