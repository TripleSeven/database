﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobMasterJobItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroJobMasterJobItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./JobIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Property", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyContent;
    [AutoBind("./Property/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyHP;
    [AutoBind("./Property/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyHPValueText;
    [AutoBind("./Property/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyAT;
    [AutoBind("./Property/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyATValueText;
    [AutoBind("./Property/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyDF;
    [AutoBind("./Property/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyDFValueText;
    [AutoBind("./Property/DEX", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyDEX;
    [AutoBind("./Property/DEX/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyDEXValueText;
    [AutoBind("./Property/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyMagicDF;
    [AutoBind("./Property/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyMagicDFValueText;
    [AutoBind("./Property/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_propertyMagic;
    [AutoBind("./Property/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_propertyMagicValueText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitJobMasterItem;
    private static DelegateBridge __Hotfix_SetMasterRewardProperty;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitJobMasterItem(ConfigDataJobInfo jobInfo, bool isMaster)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMasterRewardProperty(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
