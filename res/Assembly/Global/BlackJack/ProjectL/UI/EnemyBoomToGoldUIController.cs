﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EnemyBoomToGoldUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EnemyBoomToGoldUIController : UIControllerBase
  {
    private int m_heroId;
    [AutoBind("./Enemy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_enemyGameObject;
    [AutoBind("./Gold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goldGameObject;
    [AutoBind("./Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./DieEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dieEffectGameObject;
    private UISpineGraphic m_spineGraphic;
    private static DelegateBridge __Hotfix_SetBattleActor;
    private static DelegateBridge __Hotfix_Co_ShowBoomAndGold;
    private static DelegateBridge __Hotfix_GetEnemyGoldCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator Co_ShowBoomAndGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEnemyGoldCount()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
