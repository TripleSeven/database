﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BossActionButtonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BossActionButtonUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Skill", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillIconImage;
    private AncientCallBossBehavioralDesc m_behavioralDesc;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetBehavioralDesc;
    private static DelegateBridge __Hotfix_GetBehavioralDesc;
    private static DelegateBridge __Hotfix_SetState;
    private static DelegateBridge __Hotfix_OnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBehavioralDesc(AncientCallBossBehavioralDesc behaviorDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossBehavioralDesc GetBehavioralDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BossActionButtonUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
