﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerInfoHeadIconUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerInfoHeadIconUIController : UIControllerBase
  {
    [AutoBind("./ListPanel/ToggleGroup/HeadFrameToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_headFrameToggle;
    [AutoBind("./ListPanel/ToggleGroup/HeadToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_headPortraitToggle;
    [AutoBind("./ListPanel/HeadListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeHeadPortraitScrollViewContent;
    [AutoBind("./ListPanel/HeadFrameListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_changeHeadFrameScrollViewContent;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_changeIconStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeIconPanelBGButton;
    [AutoBind("./ListPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmChangeIconButton;
    [AutoBind("./PreviewPanel/PlayerHeadImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_previewHeadPortraitImage;
    [AutoBind("./PreviewPanel/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_previewHeadFrameTransform;
    [AutoBind("./PreviewPanel/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewLevelText;
    [AutoBind("./PreviewPanel/GetCondition/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewFrameUnlockText;
    [AutoBind("./PreviewPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewPortraitNameText;
    [AutoBind("./PreviewPanel/TitleText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_previewFrameNameText;
    private GameObject m_headPortraitItemPrefab;
    private GameObject m_headFrameItemPrefab;
    private List<Hero> m_heroList;
    private PlayerInfoHeadPortraitItemUIController m_curSelectedHeadPortraitItem;
    private PlayerInfoHeadFrameItemUIController m_curSelectedHeadFrameItem;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowChangeHeadIconPanel;
    private static DelegateBridge __Hotfix_ChangeHeadIconTogglePanel;
    private static DelegateBridge __Hotfix_OnChangeIconPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_UpdateChangHeadIconPreview;
    private static DelegateBridge __Hotfix_OnConfirmChangeIconButtonClick;
    private static DelegateBridge __Hotfix_GetCurPanelType;
    private static DelegateBridge __Hotfix_OnHeadPortraitToggleValueChanged;
    private static DelegateBridge __Hotfix_OnHeadFrameToggleValueChanged;
    private static DelegateBridge __Hotfix_OnHeadFrameValueChanged;
    private static DelegateBridge __Hotfix_OnHeadPortraitValueChanged;
    private static DelegateBridge __Hotfix_CheckHeadFrameValid;
    private static DelegateBridge __Hotfix_add_EventOnChangeHeadPortraitAndHeadFrame;
    private static DelegateBridge __Hotfix_remove_EventOnChangeHeadPortraitAndHeadFrame;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoHeadIconUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChangeHeadIconPanel(
      GameObject headPortraitItemPrefab,
      GameObject headFrameItemPrefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeHeadIconTogglePanel(PlayerInfoHeadIconPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnChangeIconPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateChangHeadIconPreview()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmChangeIconButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PlayerInfoHeadIconPanelType GetCurPanelType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadPortraitToggleValueChanged(bool isOn, UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadFrameToggleValueChanged(bool isOn, UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadFrameValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeadPortraitValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckHeadFrameValid()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnChangeHeadPortraitAndHeadFrame
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
