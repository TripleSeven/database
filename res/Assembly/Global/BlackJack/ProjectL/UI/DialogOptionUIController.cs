﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogOptionUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DialogOptionUIController : UIControllerBase
  {
    [AutoBind("./Choice", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Choice/1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_choiceButton1;
    [AutoBind("./Choice/2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_choiceButton2;
    [AutoBind("./Choice/3", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_choiceButton3;
    [AutoBind("./Choice/1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_choiceText1;
    [AutoBind("./Choice/2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_choiceText2;
    [AutoBind("./Choice/3/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_choiceText3;
    [AutoBind("./Time/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./Time", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGo;
    private ConfigDataDialogInfo m_dialogInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetChoicesData;
    private static DelegateBridge __Hotfix_SetTimeText;
    private static DelegateBridge __Hotfix_add_EventOnChoiceClick;
    private static DelegateBridge __Hotfix_remove_EventOnChoiceClick;

    private DialogOptionUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChoicesData(ConfigDataDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTimeText(string timeTxt)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> EventOnChoiceClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
