﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRankRewardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallRankRewardUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_explainText;
    [AutoBind("./Detail/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_itemContent;
    [AutoBind("./Detail/ScrollView/Viewport/Content/RewardListItem", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_itemPrefab;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_prefabResourceContainer;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private AncientCallUITask m_ancientCallUITask;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_OnCloseClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    private class RewardItem : UIControllerBase
    {
      [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
      private CommonUIStateController m_uiState;
      [AutoBind("./NoRewardText", AutoBindAttribute.InitState.NotInit, false)]
      private GameObject m_explainGameObject;
      [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
      private Text m_rankRangeText;
      [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
      private Transform m_rewardGroup;
      private ProjectLPlayerContext m_playerContext;
      private IConfigDataLoader m_configDataLoader;
      private GameObject m_rewardGoodsPrefab;
      public int m_index;
      private ConfigDataAncientCallTotalBossPointsInfo m_afterBossPointsInfo;
      private ConfigDataAncientCallTotalBossPointsInfo m_bossPointsInfo;
      private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
      private static DelegateBridge __Hotfix_Init;
      private static DelegateBridge __Hotfix_SetData;
      private static DelegateBridge __Hotfix_UpdateView;

      [MethodImpl((MethodImplOptions) 32768)]
      protected override void OnBindFiledsCompleted()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void Init(GameObject rewardGoodsPrefab)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void SetData(
        ConfigDataAncientCallTotalBossPointsInfo bossPointsInfo,
        ConfigDataAncientCallTotalBossPointsInfo afterBossPointsInfo,
        int index)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private void UpdateView()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
