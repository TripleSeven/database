﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillTrainingUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DrillTrainingUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./PlayerResource/Crystal/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_crystalText;
    [AutoBind("./PlayerResource/Crystal/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_crystalAddButton;
    [AutoBind("./PlayerResource/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./PlayerResource/Gold/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddButton;
    [AutoBind("./TrainingNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingNameText;
    [AutoBind("./TitleInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_roomInfoImageStateCtrl;
    [AutoBind("./TitleInfoGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_roomInfoLvValueText;
    [AutoBind("./TitleInfoGroup/ExpValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_roomInfoExpValueText;
    [AutoBind("./TitleInfoGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_roomInfoProgressBar;
    [AutoBind("./TrainingEventScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trainingCourseScrollViewContent;
    [AutoBind("./SkillPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillPanelGameObject;
    [AutoBind("./Prefab/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillItemPrefab;
    [AutoBind("./SkillItemInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillItemInfoPanelStateCtrl;
    [AutoBind("./SkillItemInfoPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelBGButton;
    [AutoBind("./SkillItemInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelCloseButton;
    [AutoBind("./SkillItemInfoPanel/FastMaxButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelFastMaxButton;
    [AutoBind("./SkillItemInfoPanel/FastMaxButton/FastLevelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillItemInfoPanelFastLevelButton;
    [AutoBind("./SkillItemInfoPanel/FastMaxButton/FastLevelButton/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_skillItemInfoPanelFastLevelInputField;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailPanel;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/TypeAndQuality/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailPanelSoldierTypeIcon;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/TypeAndQuality/QualityIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierDetailPanelSoldierQualityIcon;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/TypeAndQuality/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelSoldierNameText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupHPValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupATValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupDFValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupMagicDFValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupRangeValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Move/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupMoveValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupRestrainValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Week/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupWeakValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/PorpretyGroup/Type", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelPorpretyGroupTypeStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/SoldierDetail/Desc/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDetailPanelPorpretyGroupDescText;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/HeroList", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelHeroListStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierDetailPanel/HeroList/HeroList/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDetailPanelHeroListContent;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierAndSkillInfoSkillIconImage;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SkillGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoSkillNameText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SoldierGroup/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoSoldierGraphic;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SoldierGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoSoldierNameText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/SkillAndSoldierShowPanel/SkillGroup/FrameImage/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoSkillUpdateEffectGroup;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierAndSkillInfoPanelStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/WatchDetailButton/WatchDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierAndSkillInfoPanelWatchDetailButton;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/WatchDetailButton/WatchDetailButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelDetailStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/NowEffectGroup/NowLvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelNowLvValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/NowEffectGroup/NowEffectTextScroll/Mask/NowEffectText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelNowEffectText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/IntensifyEffectGroup/IntensifyLvValueTexxt", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelIntensifyLvValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/IntensifyEffectGroup/NowEffectTextScroll/Mask/NowEffectText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelIntensifyNowEffectText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/AfterIntensifyEffectGroup/AfterIntensifyLvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelAfterIntensifyLvValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/AfterIntensifyEffectGroup/TextScroll/Mask/AfterEffectText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelAfterIntensifyEffectText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/ItemGroup/ItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoPanelItemGroup;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/PromoteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierAndSkillInfoPanelPromoteButton;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/PromoteButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierAndSkillInfoPanelPromoteButtonStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAndSkillInfoPanelGoldenValueText;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/GoldenValueText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierAndSkillInfoPanelGoldenStateCtrl;
    [AutoBind("./SkillItemInfoPanel/SoldierAndSkillInfoPanel/InfoPanel/BGImage/FrameImage/PromoteGroup/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierAndSkillInfoPanelConditionGroup;
    [AutoBind("./SkillItemInfoPanel/EnhanceSuccessEffectPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enhanceSuccessEffectPanelStateCtrl;
    private bool m_isFirstIn;
    private UISpineGraphic m_soldierInfoGraphic;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private TrainingRoom m_curTrainingRoom;
    private TrainingCourse m_curTrainingCourse;
    private TrainingSkillItemUIController m_curTrainingSkillItemCtrl;
    private CourseItemUIController m_lastCourseItemCtrl;
    private List<TrainingSkillItemUIController> m_trainingSkillItemUICtrlList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInDrillTraining;
    private static DelegateBridge __Hotfix_ShowTechClickPanelByTechId;
    private static DelegateBridge __Hotfix_SetRoomCoachInfoPanel;
    private static DelegateBridge __Hotfix_SetCoursesListPanel;
    private static DelegateBridge __Hotfix_OnCourseItemClick;
    private static DelegateBridge __Hotfix_SetSkillPanel;
    private static DelegateBridge __Hotfix_OnTrainingSkillItemClick;
    private static DelegateBridge __Hotfix_SetSkillItemInfoPanel;
    private static DelegateBridge __Hotfix_OnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_SetSoldierDetailPanel;
    private static DelegateBridge __Hotfix_OnInfoPanelPromoteButtonClick;
    private static DelegateBridge __Hotfix_OnShowSoldierDetailButtonClick;
    private static DelegateBridge __Hotfix_OnSkillItemInfoPanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_OnCrystalAddButtonClick;
    private static DelegateBridge __Hotfix_OnSkillItemInfoPanelFastMaxButtonClick;
    private static DelegateBridge __Hotfix_OnSkillItemInfoPanelFastLevelButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnAddGold;
    private static DelegateBridge __Hotfix_remove_EventOnAddGold;
    private static DelegateBridge __Hotfix_add_EventOnAddCrystal;
    private static DelegateBridge __Hotfix_remove_EventOnAddCrystal;
    private static DelegateBridge __Hotfix_add_EventOnTechLevelup;
    private static DelegateBridge __Hotfix_remove_EventOnTechLevelup;
    private static DelegateBridge __Hotfix_add_EventOnEvolutionMaterialClick;
    private static DelegateBridge __Hotfix_remove_EventOnEvolutionMaterialClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillTrainingUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInDrillTraining(int courseId, int techId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTechClickPanelByTechId(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRoomCoachInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCoursesListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCourseItemClick(CourseItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingSkillItemClick(TrainingSkillItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillItemInfoPanel(TrainingSkillItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEvolutionMaterialClick(GoodsType goodsType, int id, int needCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailPanel(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoPanelPromoteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowSoldierDetailButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemInfoPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCrystalAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemInfoPanelFastMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemInfoPanelFastLevelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddGold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, Action> EventOnTechLevelup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int, int> EventOnEvolutionMaterialClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
