﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BagItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BagItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_stateCtrl;
    [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_iconImage;
    [AutoBind("./SelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_selectFrameImage;
    [AutoBind("./FragmentSelectFrameImg", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_fragmentSelectFrameImg;
    [AutoBind("./SelectFrameImg2", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_checkFrameImage;
    [AutoBind("./BgImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_bgImage;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_ssrEffect;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_lockImage;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_lvValueText;
    [AutoBind("./EquipingTag", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_equipingTag;
    [AutoBind("./EquipingTag/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_equipingTagHeadIcon;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_starGroup;
    [AutoBind("./GreyMaskItem", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_greyMaskItem;
    [AutoBind("./GreyMaskPiece", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_greyMaskPiece;
    [AutoBind("./EnchantmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_enchantmentIcon;
    public ConfigDataItemInfo m_itemInfo;
    public ConfigDataJobMaterialInfo m_jobMaterialInfo;
    public ConfigDataEquipmentInfo m_equipmentInfo;
    public ConfigDataEnchantStoneInfo m_enchantStoneInfo;
    public ConfigDataRefineryStoneInfo m_refineryStoneInfo;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetBagItemInfo;
    private static DelegateBridge __Hotfix_SetItemInfoByType;
    private static DelegateBridge __Hotfix_SetItemStateByType;
    private static DelegateBridge __Hotfix_ShowFrame;
    private static DelegateBridge __Hotfix_ShowCheckFrame;
    private static DelegateBridge __Hotfix_ShowGreyMask;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_RegisterItemClickEvent;
    private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
    private static DelegateBridge __Hotfix_set_BagItem;
    private static DelegateBridge __Hotfix_get_BagItem;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBagItemInfo(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemInfoByType(GoodsType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemStateByType(GoodsType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCheckFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGreyMask(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    public BagItemBase BagItem
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
