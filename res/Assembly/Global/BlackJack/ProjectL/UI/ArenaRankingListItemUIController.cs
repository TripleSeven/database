﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaRankingListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArenaRankingListItemUIController : UIControllerBase
  {
    [AutoBind("./RankingIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankingImage;
    [AutoBind("./RankingText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankingText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaPointsText;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_backgroundGameObject;
    private Image m_playerIconImage;
    private Transform m_playerHeadFrameTransform;
    private Image m_arenaLevelIconImage;
    private Text m_playerLevelText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetRanking;
    private static DelegateBridge __Hotfix_SetIndex;
    private static DelegateBridge __Hotfix_SetPlayer_0;
    private static DelegateBridge __Hotfix_SetPlayer_1;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRanking(int ranking)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(ProArenaTopRankPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayer(ProRealTimePVPLeaderboardPlayerInfo player, ProUserSummary userSummary)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
