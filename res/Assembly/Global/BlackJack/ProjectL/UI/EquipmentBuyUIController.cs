﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentBuyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipmentBuyUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipmentBuyUIAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Detail/EquipType", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipTypeAnimation;
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./Detail/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvText;
    [AutoBind("./Detail/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_expText;
    [AutoBind("./Detail/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Detail/Item/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Detail/Item/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Detail/Item/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Detail/Item/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Detail/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipLimitAnimation;
    [AutoBind("./Detail/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipLimitContent;
    [AutoBind("./Detail/EquipGroup/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Detail/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HPGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_HPText;
    [AutoBind("./Detail/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ATGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ATText;
    [AutoBind("./Detail/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DFGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DFText;
    [AutoBind("./Detail/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicDFGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicDFText;
    [AutoBind("./Detail/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicText;
    [AutoBind("./Detail/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DexGameObject;
    [AutoBind("./Detail/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DexText;
    [AutoBind("./Detail/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillDescAnimation;
    [AutoBind("./Detail/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_notEquipSkillGameObject;
    [AutoBind("./Detail/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillNameText;
    [AutoBind("./Detail/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDescText;
    [AutoBind("./Detail/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillUnlockConditionText;
    [AutoBind("./Detail/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillBelongBGText;
    [AutoBind("./Detail/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillBelongText;
    [AutoBind("./Detail/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyButton;
    [AutoBind("./Detail/BuyButton/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_currencyImage;
    [AutoBind("./Detail/BuyButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currencyCount;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private EquipmentBuyUITask m_equipmentBuyUITask;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PlayCloseAnimation;
    private static DelegateBridge __Hotfix_GetEquipmentTypeAnimationName;
    private static DelegateBridge __Hotfix_SetSkillDescription;
    private static DelegateBridge __Hotfix_SetEquipCondition;
    private static DelegateBridge __Hotfix_ClosePropDisplay;
    private static DelegateBridge __Hotfix_SetEquipmentPropItem;
    private static DelegateBridge __Hotfix_OnBGClick;
    private static DelegateBridge __Hotfix_OnBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnCloseClick;
    private static DelegateBridge __Hotfix_remove_EventOnCloseClick;
    private static DelegateBridge __Hotfix_add_EventOnBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnBuyClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayCloseAnimation(Action onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetEquipmentTypeAnimationName(EquipmentType equipmentType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSkillDescription(ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipCondition(ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePropDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropItem(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBGClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnCloseClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Goods> EventOnBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
