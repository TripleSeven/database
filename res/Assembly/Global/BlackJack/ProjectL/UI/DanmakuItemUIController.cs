﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DanmakuItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DanmakuItemUIController : UIControllerBase
  {
    [AutoBind("./TurnText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_turnText;
    [AutoBind("./TextMask/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_contentText;
    [AutoBind("./TextMask/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private TweenPos m_contentTextTweenPos;
    [AutoBind("./TextMask", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_textMaskRectTransform;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitDanmakuItem;
    private static DelegateBridge __Hotfix_PlayTextTweenPos;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDanmakuItem(string content, int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayTextTweenPos()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
