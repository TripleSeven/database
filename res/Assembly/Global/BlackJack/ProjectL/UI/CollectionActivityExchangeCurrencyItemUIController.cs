﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CollectionActivityExchangeCurrencyItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CollectionActivityExchangeCurrencyItemUIController : UIControllerBase
  {
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_countText;
    private static DelegateBridge __Hotfix_UpdateItem_0;
    private static DelegateBridge __Hotfix_UpdateItem_1;

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(CollectionActivityCurrency currencyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateItem(int itemId, int count)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
