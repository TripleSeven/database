﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.WorldUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class WorldUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Fog", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fogImage;
    [AutoBind("./Player", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerButton;
    [AutoBind("./Player/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./Player/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerLevelText;
    [AutoBind("./Player/Vip/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerVipText;
    [AutoBind("./Player/Exp/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerExpText;
    [AutoBind("./Player/Exp/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerExpImage;
    [AutoBind("./Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./CompassButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_compassButton;
    [AutoBind("./CurrentScenario", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_currentScenarioButton;
    [AutoBind("./CurrentScenario/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_currentScenarioNameText;
    [AutoBind("./NewScenario", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_newScenarioUIStateController;
    [AutoBind("./NewScenario/NameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_newScenarioNameText;
    [AutoBind("./EnterScenario", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enterScenarioUIStateController;
    [AutoBind("./EnterScenario/Image/Chapter", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enterScenarioChapterText;
    [AutoBind("./EnterScenario/Image/Name", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_enterScenarioNameText;
    [AutoBind("./EnterMonster", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_enterMonsterUIStateController;
    [AutoBind("./EventList", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_eventListTransform;
    [AutoBind("./EventList", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_eventListUIStateController;
    [AutoBind("./EventList/RandomEventPanel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_randomEventScrollRect;
    [AutoBind("./EventList/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eventListBackgroundButton;
    [AutoBind("./PastScenarioList", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_pastScenarioListUIStateController;
    [AutoBind("./PastScenarioList/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pastScenarioListBackgroundButton;
    [AutoBind("./PastScenarioList/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_pastScenarioListScrollRect;
    [AutoBind("./UnlockScenario", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockScenarioUIStateController;
    [AutoBind("./UnlockScenario/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockScenarioBackgroundButton;
    [AutoBind("./UnlockScenario/Frame/Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockScenarioTitleText;
    [AutoBind("./UnlockScenario/Frame/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockScenarioText;
    [AutoBind("./UnlockScenario/Frame/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockScenarioGotoButton;
    [AutoBind("./MainButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_mainButton;
    [AutoBind("./MainButton/ON", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mainButtonOnGameObject;
    [AutoBind("./MainButton/OFF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_mainButtonOffGameObject;
    [AutoBind("./Margin1", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_margin1Transform;
    [AutoBind("./Margin1/MainButtonBar", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_mainButtonBarUIStateController;
    [AutoBind("./Margin1/MainButtonBar/HeroButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroButton;
    [AutoBind("./Margin1/MainButtonBar/HeroButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroButtonRedMarkGameObject;
    [AutoBind("./Margin1/MainButtonBar/BagButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bagButton;
    [AutoBind("./Margin1/MainButtonBar/MissionButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_missionButton;
    [AutoBind("./Margin1/MainButtonBar/MissionButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionButtonRedMarkGameObject;
    [AutoBind("./Margin1/MainButtonBar/FetterButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fetterButton;
    [AutoBind("./Margin1/MainButtonBar/FetterButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fetterButtonRedMarkGameObject;
    [AutoBind("./Margin1/MainButtonBar/StoreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storeButton;
    [AutoBind("./Margin1/MainButtonBar/SelectCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectCardButton;
    [AutoBind("./Margin1/MainButtonBar/TrainingHouseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_drillButton;
    [AutoBind("./Margin1/MainButtonBar/TrainingHouseButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_drillButtonRedMark;
    [AutoBind("./Margin1/MainButtonBar/SociatyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_guildButton;
    [AutoBind("./Margin1/MainButtonBar/SociatyButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sociatyButtonRedMark;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/Right/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightGameObject;
    [AutoBind("./Margin/Right/EventButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eventButton;
    [AutoBind("./Margin/Right/EventButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventButtonRedMarkGameObject;
    [AutoBind("./Margin/Right/UnchartedButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unchartedButton;
    [AutoBind("./Margin/Right/ArenaButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaButton;
    [AutoBind("./Margin/Right/RiftButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_riftButton;
    [AutoBind("./Margin/Right/TestButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_testButton;
    [AutoBind("./Margin/Right/TestButton2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_testButton2;
    [AutoBind("./Margin/Right/ActiveButtonGroup/CooperateBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cooperateBattleButton;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_recommendGiftBoxButton;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_recommendGiftBoxButtonAnimation;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_recommendGiftBoxRedMark;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitPackageButton/LimitPackage/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_recommendGiftBoxTimeText;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TopArenaButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaButtonStateCtrl;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TopArenaButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaButton;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitEventButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eventGiftBoxButton;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitEventButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_eventGiftBoxButtonAnimation;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitEventButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_eventGiftBoxRedMark;
    [AutoBind("./Margin/Right/ActiveButtonGroup/TimeLimitEventButton/LimitEvent/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_eventGiftBoxTimeText;
    [AutoBind("./Margin/Left/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftGameObject;
    [AutoBind("./Margin/Left/FriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_friendButton;
    [AutoBind("./Margin/Left/FriendButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendButtonRedMark;
    [AutoBind("./Margin/Left/MailButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_mailButton;
    [AutoBind("./Margin/Left/MailButton/CountPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unreadMailCountText;
    [AutoBind("./Margin/Left/ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./Margin/Left/ChatButton/CountPanel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_newChatCountText;
    [AutoBind("./Margin/Left/RankButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankButton;
    [AutoBind("./Margin/Left/ActivityButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_activityButton;
    [AutoBind("./Margin/Left/ActivityButton/CountPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRedMarkGameObject;
    [AutoBind("./Margin/Left/InvestigationButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_investigationButton;
    [AutoBind("./Margin/Left/InvestigationButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_investigationButtonRedMark;
    [AutoBind("./Margin/Left/WebInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_noticeCenterButton;
    [AutoBind("./Margin/Left/WebInfoButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeCenterButtonRedMark;
    [AutoBind("./Margin/YYBButton/", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_YYBButton;
    [AutoBind("./Margin/OppoButton/", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_OppoButton;
    [AutoBind("./OpenServiceButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_openServiceActivityButton;
    [AutoBind("./OpenServiceButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_openServiceActivityButtonRedMark;
    [AutoBind("./BackflowButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backflowButtonActivityButton;
    [AutoBind("./BackflowButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_backflowButtonActivityButtonRedMark;
    [AutoBind("./MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_monthCardButton;
    [AutoBind("./MonthCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_monthCardButtonStateCtrl;
    [AutoBind("./MonthCardButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monthCardRedMarkObj;
    [AutoBind("./MonthCardBuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_monthCardPanelUIStateController;
    [AutoBind("./MonthCardBuyPanel/Detail/FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monthCardItemGroupObj;
    [AutoBind("./MonthCardBuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_monthCardCloseButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/RandomEventListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_randomEventListItemPrefab;
    [AutoBind("./Prefabs/PastScenarioListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pastScenarioListItemPrefab;
    [AutoBind("./Prefabs/MonthCardDetail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_monthCardItemPrefab;
    [AutoBind("./WorldTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ServerTimeGO;
    [AutoBind("./WorldTime/TextTime", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ServerTimeText;
    private ConfigDataRiftLevelInfo m_gotoRiftLevelInfo;
    private int m_developerClickCount;
    private Color m_fogColor;
    private List<PastScenarioListItemUIController> m_pastScenarioListItems;
    private List<GameObject> m_userGuideDialogHideGameObjects;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private bool m_alwaysShowMonthCardButton;
    private List<ConfigDataMonthCardInfo> m_allMonthCardList;
    private ConfigDataGiftStoreItemInfo m_strongRecommendGiftStoreItem;
    private DateTime m_strongRecommendGiftStoreItemEndTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnEnable;
    private static DelegateBridge __Hotfix_UpdateDeveloperMode;
    private static DelegateBridge __Hotfix_ShowOrHide;
    private static DelegateBridge __Hotfix_UpdateGameFunctionOpen;
    private static DelegateBridge __Hotfix_SetPlayerName;
    private static DelegateBridge __Hotfix_SetPlayerLevel;
    private static DelegateBridge __Hotfix_SetPlayerVip;
    private static DelegateBridge __Hotfix_SetPlayerExp;
    private static DelegateBridge __Hotfix_SetPlayerHeadIcon;
    private static DelegateBridge __Hotfix_ShowMainButtonBar;
    private static DelegateBridge __Hotfix_SetActiveScenario;
    private static DelegateBridge __Hotfix_ShowNewScenario;
    private static DelegateBridge __Hotfix_ShowUnlockScenario;
    private static DelegateBridge __Hotfix_ShowEnterScenario;
    private static DelegateBridge __Hotfix_ShowEnterMonster;
    private static DelegateBridge __Hotfix_Co_EnterMonster;
    private static DelegateBridge __Hotfix_SetFog;
    private static DelegateBridge __Hotfix_AddRandomEvent;
    private static DelegateBridge __Hotfix_ClearRandomEventList;
    private static DelegateBridge __Hotfix_ShowEventList;
    private static DelegateBridge __Hotfix_HideEventList;
    private static DelegateBridge __Hotfix_IsEventListVisible;
    private static DelegateBridge __Hotfix_AddPastScenario;
    private static DelegateBridge __Hotfix_ClearPastScenarioList;
    private static DelegateBridge __Hotfix_ShowPastScenarioList;
    private static DelegateBridge __Hotfix_HidePastScenarioList;
    private static DelegateBridge __Hotfix_ShowCooperateBattleButton;
    private static DelegateBridge __Hotfix_ShowHeroRedMark;
    private static DelegateBridge __Hotfix_ShowDrillRedMark;
    private static DelegateBridge __Hotfix_ShowFetterRedMark;
    private static DelegateBridge __Hotfix_ShowNoticeCenterButtonRedMark;
    private static DelegateBridge __Hotfix_ShowNoticeCenterButton;
    private static DelegateBridge __Hotfix_ShowGuildRedMark;
    private static DelegateBridge __Hotfix_ShowEventRedMark;
    private static DelegateBridge __Hotfix_ShowMissionRedMark;
    private static DelegateBridge __Hotfix_ShowActivityRedMark;
    private static DelegateBridge __Hotfix_ShowFriendRedMark;
    private static DelegateBridge __Hotfix_UpdateNewChatCount;
    private static DelegateBridge __Hotfix_UpdateUnreadMailCount;
    private static DelegateBridge __Hotfix_GetUserGuideDialogHideGameObjects;
    private static DelegateBridge __Hotfix_UpdateMonthCardOpen;
    private static DelegateBridge __Hotfix_SetMonthCardRedMark;
    private static DelegateBridge __Hotfix_UpdateRecommendGiftBoxRedMark;
    private static DelegateBridge __Hotfix_UpdateRecommendGiftBoxTime;
    private static DelegateBridge __Hotfix_UpdateRecommendGiftBoxIconState;
    private static DelegateBridge __Hotfix_UpdateEventGiftBoxRedMark;
    private static DelegateBridge __Hotfix_UpdateEventGiftBoxTime;
    private static DelegateBridge __Hotfix_UpdateEventGiftBoxIconState;
    private static DelegateBridge __Hotfix_UpdateSystemTime;
    private static DelegateBridge __Hotfix_SetMonthCardButtonState;
    private static DelegateBridge __Hotfix_OpenMonthCardPanel;
    private static DelegateBridge __Hotfix_OnMonthCardSort;
    private static DelegateBridge __Hotfix_CloseMonthCardPanel;
    private static DelegateBridge __Hotfix_DeveloperModeClick;
    private static DelegateBridge __Hotfix_OnCompassButtonClick;
    private static DelegateBridge __Hotfix_OnCurrentScenarioButtonClick;
    private static DelegateBridge __Hotfix_OnUnlockScenarioBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnUnlockScenarioGotoButtonClick;
    private static DelegateBridge __Hotfix_OnMainButtonClick;
    private static DelegateBridge __Hotfix_OnPlayerButtonClick;
    private static DelegateBridge __Hotfix_OnHeroButtonClick;
    private static DelegateBridge __Hotfix_OnBagButtonClick;
    private static DelegateBridge __Hotfix_OnSelectCardButtonClick;
    private static DelegateBridge __Hotfix_OnMissionButtonClick;
    private static DelegateBridge __Hotfix_OnFetterButtonClick;
    private static DelegateBridge __Hotfix_OnStoreButtonClick;
    private static DelegateBridge __Hotfix_OnDrillButtonClick;
    private static DelegateBridge __Hotfix_OnFriendButtonClick;
    private static DelegateBridge __Hotfix_OnGuildButtonClick;
    private static DelegateBridge __Hotfix_OnEventButtonClick;
    private static DelegateBridge __Hotfix_OnUnchartedButtonClick;
    private static DelegateBridge __Hotfix_OnArenaButtonClick;
    private static DelegateBridge __Hotfix_OnRiftButtonClick;
    private static DelegateBridge __Hotfix_OnTestButtonClick;
    private static DelegateBridge __Hotfix_OnTestButton2Click;
    private static DelegateBridge __Hotfix_OnCooperateBattleButtonClick;
    private static DelegateBridge __Hotfix_OnRecommendGiftBoxButtonClick;
    private static DelegateBridge __Hotfix_OnEventGiftBoxButtonClick;
    private static DelegateBridge __Hotfix_OnMailButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnRankButtonClick;
    private static DelegateBridge __Hotfix_OnActivityButtonClick;
    private static DelegateBridge __Hotfix_OnOpenServiceActivityButtonClick;
    private static DelegateBridge __Hotfix_OnBackflowButtonActivityButtonClick;
    private static DelegateBridge __Hotfix_OnOppoButtonClick;
    private static DelegateBridge __Hotfix_OnYingYongBaoButtonClick;
    private static DelegateBridge __Hotfix_OnNoticeCenterButtonClick;
    private static DelegateBridge __Hotfix_OnInvestigationButtonClick;
    private static DelegateBridge __Hotfix_UpdateInvestigationButton;
    private static DelegateBridge __Hotfix_UpdatePeakArenaButton;
    private static DelegateBridge __Hotfix_UpdatePeakArenaButtonState;
    private static DelegateBridge __Hotfix_UpdateOpenServiceActivityButton;
    private static DelegateBridge __Hotfix_UpdateBackFlowActivityButton;
    private static DelegateBridge __Hotfix_SaveState;
    private static DelegateBridge __Hotfix_WorldEventListItem_OnButtonClick;
    private static DelegateBridge __Hotfix_OnEventListBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnPastScenarioListBackgroundButtonClick;
    private static DelegateBridge __Hotfix_PastScenarioListItem_OnStartButtonClick;
    private static DelegateBridge __Hotfix_OnPeakArenaButtonClick;
    private static DelegateBridge __Hotfix_OnMonthCardButtonClick;
    private static DelegateBridge __Hotfix_OnMonthCardCloseButtonClick;
    private static DelegateBridge __Hotfix_OnMonthCardItemItemClick;
    private static DelegateBridge __Hotfix_OnMonthCardBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnShowPlayerInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowPlayerInfo;
    private static DelegateBridge __Hotfix_add_EventOnCompass;
    private static DelegateBridge __Hotfix_remove_EventOnCompass;
    private static DelegateBridge __Hotfix_add_EventOnCurrentScenario;
    private static DelegateBridge __Hotfix_remove_EventOnCurrentScenario;
    private static DelegateBridge __Hotfix_add_EventOnShowMainButtonBar;
    private static DelegateBridge __Hotfix_remove_EventOnShowMainButtonBar;
    private static DelegateBridge __Hotfix_add_EventOnUnlockScenarioGotoRiftLevel;
    private static DelegateBridge __Hotfix_remove_EventOnUnlockScenarioGotoRiftLevel;
    private static DelegateBridge __Hotfix_add_EventOnGotoEvent;
    private static DelegateBridge __Hotfix_remove_EventOnGotoEvent;
    private static DelegateBridge __Hotfix_add_EventOnStartPastScenario;
    private static DelegateBridge __Hotfix_remove_EventOnStartPastScenario;
    private static DelegateBridge __Hotfix_add_EventOnClosePastScenarioList;
    private static DelegateBridge __Hotfix_remove_EventOnClosePastScenarioList;
    private static DelegateBridge __Hotfix_add_EventOnShowHero;
    private static DelegateBridge __Hotfix_remove_EventOnShowHero;
    private static DelegateBridge __Hotfix_add_EventOnShowBag;
    private static DelegateBridge __Hotfix_remove_EventOnShowBag;
    private static DelegateBridge __Hotfix_add_EventOnShowSelectCard;
    private static DelegateBridge __Hotfix_remove_EventOnShowSelectCard;
    private static DelegateBridge __Hotfix_add_EventOnShowMisision;
    private static DelegateBridge __Hotfix_remove_EventOnShowMisision;
    private static DelegateBridge __Hotfix_add_EventOnShowFetter;
    private static DelegateBridge __Hotfix_remove_EventOnShowFetter;
    private static DelegateBridge __Hotfix_add_EventOnShowStore;
    private static DelegateBridge __Hotfix_remove_EventOnShowStore;
    private static DelegateBridge __Hotfix_add_EventOnShowDrill;
    private static DelegateBridge __Hotfix_remove_EventOnShowDrill;
    private static DelegateBridge __Hotfix_add_EventOnShowFriend;
    private static DelegateBridge __Hotfix_remove_EventOnShowFriend;
    private static DelegateBridge __Hotfix_add_EventOnShowGuild;
    private static DelegateBridge __Hotfix_remove_EventOnShowGuild;
    private static DelegateBridge __Hotfix_add_EventOnShowEvent;
    private static DelegateBridge __Hotfix_remove_EventOnShowEvent;
    private static DelegateBridge __Hotfix_add_EventOnShowUncharted;
    private static DelegateBridge __Hotfix_remove_EventOnShowUncharted;
    private static DelegateBridge __Hotfix_add_EventOnShowArena;
    private static DelegateBridge __Hotfix_remove_EventOnShowArena;
    private static DelegateBridge __Hotfix_add_EventOnShowRift;
    private static DelegateBridge __Hotfix_remove_EventOnShowRift;
    private static DelegateBridge __Hotfix_add_EventOnShowTest;
    private static DelegateBridge __Hotfix_remove_EventOnShowTest;
    private static DelegateBridge __Hotfix_add_EventOnShowTest2;
    private static DelegateBridge __Hotfix_remove_EventOnShowTest2;
    private static DelegateBridge __Hotfix_add_EventOnShowCooperateBattle;
    private static DelegateBridge __Hotfix_remove_EventOnShowCooperateBattle;
    private static DelegateBridge __Hotfix_add_EventOnRecommendGiftBox;
    private static DelegateBridge __Hotfix_remove_EventOnRecommendGiftBox;
    private static DelegateBridge __Hotfix_add_EventOnEventGiftBox;
    private static DelegateBridge __Hotfix_remove_EventOnEventGiftBox;
    private static DelegateBridge __Hotfix_add_EventOnShowMail;
    private static DelegateBridge __Hotfix_remove_EventOnShowMail;
    private static DelegateBridge __Hotfix_add_EventOnShowChat;
    private static DelegateBridge __Hotfix_remove_EventOnShowChat;
    private static DelegateBridge __Hotfix_add_EventOnShowRanking;
    private static DelegateBridge __Hotfix_remove_EventOnShowRanking;
    private static DelegateBridge __Hotfix_add_EventOnShowAnnouncement;
    private static DelegateBridge __Hotfix_remove_EventOnShowAnnouncement;
    private static DelegateBridge __Hotfix_add_EventOnOpenWebInvestigation;
    private static DelegateBridge __Hotfix_remove_EventOnOpenWebInvestigation;
    private static DelegateBridge __Hotfix_add_EventOnNoticeCenterButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnNoticeCenterButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowOpenServiceActivity;
    private static DelegateBridge __Hotfix_remove_EventOnShowOpenServiceActivity;
    private static DelegateBridge __Hotfix_add_EventOnShowBackFlowActivity;
    private static DelegateBridge __Hotfix_remove_EventOnShowBackFlowActivity;
    private static DelegateBridge __Hotfix_add_EventOnMonthCardButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnMonthCardButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnRefreshMonthCardPanel;
    private static DelegateBridge __Hotfix_remove_EventOnRefreshMonthCardPanel;
    private static DelegateBridge __Hotfix_add_EventOnMonthCardItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnMonthCardItemClick;
    private static DelegateBridge __Hotfix_add_EventOnMonthCardItemBuyClick;
    private static DelegateBridge __Hotfix_remove_EventOnMonthCardItemBuyClick;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnYYBButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnYYBButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnOppoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnOppoButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    private WorldUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDeveloperMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHide(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGameFunctionOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerVip(int vip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerExp(int exp, int expMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeadIcon(int playerHeadIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMainButtonBar(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActiveScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNewScenario(ConfigDataScenarioInfo scenarioInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUnlockScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnterScenario(ConfigDataScenarioInfo scenarioInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEnterMonster(ConfigDataBattleInfo battleInfo, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_EnterMonster(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFog(float fog)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEvent(ConfigDataWaypointInfo waypointInfo, RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRandomEventList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEventList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideEventList(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventListVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddPastScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearPastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HidePastScenarioList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCooperateBattleButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHeroRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDrillRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFetterRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNoticeCenterButtonRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNoticeCenterButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGuildRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowEventRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMissionRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowActivityRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFriendRedMark(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateNewChatCount(int newCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateUnreadMailCount(int newCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GameObject> GetUserGuideDialogHideGameObjects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMonthCardOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetMonthCardRedMark(bool flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRecommendGiftBoxRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRecommendGiftBoxTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRecommendGiftBoxIconState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEventGiftBoxRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEventGiftBoxTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEventGiftBoxIconState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSystemTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMonthCardButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenMonthCardPanel(List<MonthCard> allCardList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int OnMonthCardSort(MonthCard x, MonthCard y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseMonthCardPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeveloperModeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompassButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCurrentScenarioButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockScenarioBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockScenarioGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMainButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBagButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectCardButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFetterButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDrillButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGuildButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnchartedButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRiftButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTestButton2Click(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCooperateBattleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecommendGiftBoxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventGiftBoxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMailButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnActivityButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOpenServiceActivityButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackflowButtonActivityButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOppoButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnYingYongBaoButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeCenterButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInvestigationButtonClick(UIControllerBase ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInvestigationButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePeakArenaButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaButtonState(EPeakArenaDurringStateType stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOpenServiceActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBackFlowActivityButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldEventListItem_OnButtonClick(WorldEventListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEventListBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPastScenarioListBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PastScenarioListItem_OnStartButtonClick(PastScenarioListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardItemItemClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardBuyClick(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnShowPlayerInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCompass
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCurrentScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnShowMainButtonBar
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataRiftLevelInfo> EventOnUnlockScenarioGotoRiftLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataWaypointInfo, ConfigDataEventInfo> EventOnGotoEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataScenarioInfo> EventOnStartPastScenario
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClosePastScenarioList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHero
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowSelectCard
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowMisision
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowFetter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowStore
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowDrill
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowGuild
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowUncharted
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowArena
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRift
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTest
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTest2
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowCooperateBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRecommendGiftBox
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEventGiftBox
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowMail
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowRanking
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowAnnouncement
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOpenWebInvestigation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNoticeCenterButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowOpenServiceActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBackFlowActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMonthCardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnRefreshMonthCardPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnMonthCardItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnMonthCardItemBuyClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnYYBButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnOppoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
