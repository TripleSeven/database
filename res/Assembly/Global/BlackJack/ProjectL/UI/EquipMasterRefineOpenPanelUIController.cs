﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipMasterRefineOpenPanelUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipMasterRefineOpenPanelUIController : UIControllerBase
  {
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private bool m_isPlayingNewPropertyAnim;
    private Hero m_hero;
    private EquipMasterEquipType m_equipType;
    private int m_propertyIndex;
    private HeroJobSlotRefineryProperty m_originProperty;
    private HeroJobSlotRefineryProperty m_newProperty;
    private List<RefineryStoneBagItem> m_refineryStoneList;
    private EquipMasterEquipPropertyFilterType m_curFilterType;
    private int m_lastClickRefineryStoneIndex;
    private EquipmentDepotListItemUIController m_lastClickRefineryStoneUICtrl;
    private EquipMasterPassAllEventUIController m_passAllEventUICtrl;
    private bool m_isFilterTypePanelOpen;
    private Action<bool> m_onReturn;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RefineOpenPanelUIStateCtrl;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool RefineStoneListObjectPool;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public LoopScrollRect RefineStoneListScrollRect;
    [AutoBind("./EquipmentList/ListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ItemRoot;
    [AutoBind("./EquipmentList/Filter", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject FilterGameObject;
    [AutoBind("./EquipmentList/Filter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button FilterButton;
    [AutoBind("./EquipmentList/Filter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    public Text FilterTypeText;
    [AutoBind("./EquipmentList/Filter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController FilterTypesUIStateCtrl;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/All", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeAllToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/Hp", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeHPToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/AT", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeATToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/DF", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeDFToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/MagicAT", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeMagicATToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeMagicDFToggle;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout/Dex", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle FilterTypeDexToggle;
    [AutoBind("./RefineInfoPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    public Text RefineInfoTitleText;
    [AutoBind("./RefineInfoPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseButton;
    [AutoBind("./RefineInfoPanel/Button", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RefineButtonUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Button/RefineButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RefineButton;
    [AutoBind("./RefineInfoPanel/Button/AgainButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button RefineAgainButton;
    [AutoBind("./RefineInfoPanel/Button/AgainButton", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RefineAgainButtonUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Button/ConserveButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ConserveButton;
    [AutoBind("./RefineInfoPanel/StoneValue/Value", AutoBindAttribute.InitState.NotInit, false)]
    public Text RefineStoneValueText;
    [AutoBind("./RefineInfoPanel/GoldValue/Value", AutoBindAttribute.InitState.NotInit, false)]
    public Text GoldConsumeText;
    [AutoBind("./RefineInfoPanel/Proprety_Original", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController OriginalPropertyUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Proprety_Original/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text OriginPropertyNameText;
    [AutoBind("./RefineInfoPanel/Proprety_Original/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text OriginPropertyValueText;
    [AutoBind("./RefineInfoPanel/Proprety_Original/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image OriginPropertyProgressImage;
    [AutoBind("./RefineInfoPanel/Proprety_New", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController NewPropertyUIStateCtrl;
    [AutoBind("./RefineInfoPanel/Proprety_New/NameText", AutoBindAttribute.InitState.NotInit, false)]
    public Text NewPropertyNameText;
    [AutoBind("./RefineInfoPanel/Proprety_New/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text NewPropertyValueText;
    [AutoBind("./RefineInfoPanel/Proprety_New/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image NewPropertyProgressImage;
    [AutoBind("./CancelConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController CancelConfirmPanelUIStateCtrl;
    [AutoBind("./CancelConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button CancelConfirmCancelButton;
    [AutoBind("./CancelConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button CancelConfirmConfirmButton;
    [AutoBind("./CancelConfirmPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle CancelConfirmDontshowToggle;
    [AutoBind("./SaveConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController SaveConfirmPanelUIStateCtrl;
    [AutoBind("./SaveConfirmPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button SaveConfirmCancelButton;
    [AutoBind("./SaveConfirmPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button SaveConfirmConfirmButton;
    [AutoBind("./SaveConfirmPanel/Detail/ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    public Toggle SaveConfirmDontshowToggle;
    [AutoBind("./FullScreenButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button FullScreenButton;
    public GameObject m_refineStoneListItemTemplate;
    private const string RefineStoneListObjectPoolName = "EquipMasterRefineryStoneListItem";
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_UpdateUI;
    private static DelegateBridge __Hotfix_UpdateRefineStoneList;
    private static DelegateBridge __Hotfix_UpdateSingleRefineryStone;
    private static DelegateBridge __Hotfix_UpdatePropertyValue;
    private static DelegateBridge __Hotfix_UpdateRefineInfo;
    private static DelegateBridge __Hotfix_UpdateRefineInfoOnStoneChanged;
    private static DelegateBridge __Hotfix_Co_DynamicSetPropertyValue;
    private static DelegateBridge __Hotfix_Co_RandomSetPropertyValue;
    private static DelegateBridge __Hotfix_UpdateRefineInfoOnRefineComplete;
    private static DelegateBridge __Hotfix_ShowCancelConfirmPanel;
    private static DelegateBridge __Hotfix_CloseCloseConfirmPanel;
    private static DelegateBridge __Hotfix_ShowSaveConfirmPanel;
    private static DelegateBridge __Hotfix_CloseSaveConfirmPanel;
    private static DelegateBridge __Hotfix_ShowFilterTypesPanel;
    private static DelegateBridge __Hotfix_CloseFilterTypesPanel;
    private static DelegateBridge __Hotfix_OnFilterButtonClick;
    private static DelegateBridge __Hotfix_OnFilterTypeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRefineStoneListItemFill;
    private static DelegateBridge __Hotfix_OnRefineStoneChanged;
    private static DelegateBridge __Hotfix_OnRefineStoneListItemClick;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_CanDoEquipMasterRefine;
    private static DelegateBridge __Hotfix_OnRefineButtonClick;
    private static DelegateBridge __Hotfix_OnRefineComplete;
    private static DelegateBridge __Hotfix_OnFullScreenButtonClick;
    private static DelegateBridge __Hotfix_OnFullScreenButtonClick_NewPropertyAnim;
    private static DelegateBridge __Hotfix_SaveRefineProperty;
    private static DelegateBridge __Hotfix_OnSaveRefinePropertyComplete;
    private static DelegateBridge __Hotfix_OnConserveButtonClick;
    private static DelegateBridge __Hotfix_CreateRefineStoneListPool;
    private static DelegateBridge __Hotfix_GetFilterButtonName;
    private static DelegateBridge __Hotfix_GetRefineryStoneList;
    private static DelegateBridge __Hotfix_IsRefineryStoneFitToEquipType;
    private static DelegateBridge __Hotfix_IsRefineryStonePropertyHasExistInOtherEquipSlot;
    private static DelegateBridge __Hotfix_IsFilterByArmyType;
    private static DelegateBridge __Hotfix_IsFilterByEquipMasterEquipPropertyFilterType;
    private static DelegateBridge __Hotfix_RefineryStoneSorter;
    private static DelegateBridge __Hotfix_GetCurrentSlectedRefineryStone;
    private static DelegateBridge __Hotfix_add_EventOnRefineButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRefineButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnConserveButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnConserveButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipMasterRefineOpenPanelUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show(
      Hero hero,
      EquipMasterEquipType equipType,
      int propertyIndex,
      HeroJobSlotRefineryProperty originProperty,
      Action<bool> onReturn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(bool refineSuccess)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRefineStoneList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateSingleRefineryStone(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePropertyValue(
      Text text,
      Image progressImage,
      HeroJobSlotRefineryProperty property)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRefineInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRefineInfoOnStoneChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_DynamicSetPropertyValue(
      Text valueText,
      Image progressBar,
      PropertyModifyType type,
      int value,
      int maxValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_RandomSetPropertyValue(
      Text valueText,
      Image progressBar,
      PropertyModifyType type,
      int value,
      int maxValue,
      Action onFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRefineInfoOnRefineComplete()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCancelConfirmPanel(
      EquipMasterRefineOpenPanelUIController.TempPropertyCancelType cancelType,
      Action onCanel,
      Action onClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseCloseConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSaveConfirmPanel(Action onCanel, Action onSave)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSaveConfirmPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowFilterTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseFilterTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterTypeToggleValueChanged(
      bool value,
      EquipMasterEquipPropertyFilterType filterType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineStoneListItemFill(UIControllerBase uiCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineStoneChanged(UIControllerBase uiCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineStoneListItemClick(UIControllerBase uiCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanDoEquipMasterRefine(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefineComplete(HeroJobSlotRefineryProperty newProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFullScreenButtonClick(
      List<RaycastResult> raycastResults,
      PointerEventData eventData,
      Action<List<RaycastResult>, PointerEventData> passEventDown)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFullScreenButtonClick_NewPropertyAnim()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveRefineProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSaveRefinePropertyComplete(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConserveButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateRefineStoneListPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetFilterButtonName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<RefineryStoneBagItem> GetRefineryStoneList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRefineryStoneFitToEquipType(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRefineryStonePropertyHasExistInOtherEquipSlot(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFilterByArmyType(RefineryStoneBagItem refineryStone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFilterByEquipMasterEquipPropertyFilterType(RefineryStoneBagItem bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RefineryStoneSorter(RefineryStoneBagItem item1, RefineryStoneBagItem item2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private RefineryStoneBagItem GetCurrentSlectedRefineryStone()
    {
      // ISSUE: unable to decompile the method.
    }

    public event HeroJobRefineDelegate EventOnRefineButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Action<int>> EventOnConserveButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum TempPropertyCancelType
    {
      ClosePanel,
      ClickStone,
    }
  }
}
