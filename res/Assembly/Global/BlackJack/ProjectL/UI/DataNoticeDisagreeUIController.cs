﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DataNoticeDisagreeUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DataNoticeDisagreeUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exitButton;
    [AutoBind("./Detail/BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backButton;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnCloseButtonClicked;
    private static DelegateBridge __Hotfix_OnExitButtonClicked;
    private static DelegateBridge __Hotfix_OnBackButtonClicked;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_OnUIClosed;
    private static DelegateBridge __Hotfix_add_EventOnClosed;
    private static DelegateBridge __Hotfix_remove_EventOnClosed;
    private static DelegateBridge __Hotfix_add_EventOnExitButtonClicked;
    private static DelegateBridge __Hotfix_remove_EventOnExitButtonClicked;
    private static DelegateBridge __Hotfix_add_EventOnBackButtonClicked;
    private static DelegateBridge __Hotfix_remove_EventOnBackButtonClicked;

    private DataNoticeDisagreeUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~DataNoticeDisagreeUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClosed
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExitButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBackButtonClicked
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
