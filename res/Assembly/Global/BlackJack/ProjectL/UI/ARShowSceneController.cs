﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARShowSceneController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.AR;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public abstract class ARShowSceneController : UIControllerBase
  {
    protected SkeletonAnimation m_roleSkeleton;
    protected List<SkeletonAnimation> m_roleSkeletonList;
    protected ARUITask m_task;
    protected ProjectLPlayerContext m_playerContext;
    protected ARPlaneTrace m_arPlaneTrace;
    protected bool isPlaneTrace;
    protected string[] m_roleAnimationList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnCharClick;
    private static DelegateBridge __Hotfix_SetUITask;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_SummonHeroBattleShow;
    private static DelegateBridge __Hotfix_SummonHeroDrawShow;
    private static DelegateBridge __Hotfix_SummonHeroTeamShow;
    private static DelegateBridge __Hotfix_PlaySingleCharAnimation;
    private static DelegateBridge __Hotfix_PlayAnimation_1;
    private static DelegateBridge __Hotfix_PlayAnimation_0;
    private static DelegateBridge __Hotfix_SetCharDirection_0;
    private static DelegateBridge __Hotfix_SetCharDirection_1;
    private static DelegateBridge __Hotfix_SetCharScale;
    private static DelegateBridge __Hotfix_SetTeamDistance;
    private static DelegateBridge __Hotfix_DestroyChar;
    private static DelegateBridge __Hotfix_PlaneTraceEnable;
    private static DelegateBridge __Hotfix_IsFindPlane;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnResume;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ARShowSceneController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCharClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUITask(ARUITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroBattleShow(int selectHeroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroDrawShow(int selectHeroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonHeroTeamShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySingleCharAnimation(string animationName, bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(
      SkeletonAnimation skeletonAnimation,
      string animationName,
      bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(
      SkeletonAnimation skeletonAnimation,
      HeroActionType actionType,
      bool isLoop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharDirection(bool isLookRight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharDirection(GameObject charObj, bool isLookRight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCharScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamDistance(float distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyChar()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaneTraceEnable(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFindPlane()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnResume()
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract GameObject focusSquare { get; }

    protected abstract GameObject charNode { get; }

    protected abstract GameObject charDrawNode { get; }

    protected abstract GameObject charGroupNode { get; }

    protected abstract Renderer focusSquareRenderer { get; }

    protected abstract ARPlaneTrace CreatePlaneTrace();
  }
}
