﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TarotUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TarotUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tarotAnimation;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./Detail/DescButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descButton;
    [AutoBind("./Detail/WatchRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchRewardButton;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmButton;
    [AutoBind("./Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmButtonAnimation;
    [AutoBind("./Detail/TicketGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_ticketGroupAnimation;
    [AutoBind("./Detail/TicketGroup/IconImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Detail/TicketGroup/ConsumeTitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_consumeTitleText;
    [AutoBind("./Detail/TicketGroup/ConsumeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_consumeValueText;
    [AutoBind("./Detail/TicketGroup/HaveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveValueText;
    [AutoBind("./Detail/TicketGroup/FreeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_freeValueText;
    [AutoBind("./Detail/CardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cardGroupAnimation;
    [AutoBind("./Detail/CardGroup/RotationPoint", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cardGroupRotationAnimation;
    [AutoBind("./RewardListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardListAnimation;
    [AutoBind("./CardShowPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_cardShowAnimation;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<Toggle> m_cardList;
    private TarotUITask m_tarotUITask;
    private TarotRewardListPanelUIController m_tarotRewardListPanelUIController;
    private TarotCardShowPanelUIController m_tarotCardShowPanelUIController;
    private List<Goods> m_rewardGoods;
    private int m_poolID;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_RemainRaffleCount;
    private static DelegateBridge __Hotfix_IsCardSelected;
    private static DelegateBridge __Hotfix_IsSrBox;
    private static DelegateBridge __Hotfix_OpenSrBox;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_ShowCard;
    private static DelegateBridge __Hotfix_ClearCardSelectedState;
    private static DelegateBridge __Hotfix_UpdateCostPanel;
    private static DelegateBridge __Hotfix_ShowNotEnoughCrystalMsgBox;
    private static DelegateBridge __Hotfix_ShowNotEoughItemMsgBox;
    private static DelegateBridge __Hotfix_OnRechargeDialogResult;
    private static DelegateBridge __Hotfix_OnWatchRewardClick;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnBackgroundClick;
    private static DelegateBridge __Hotfix_OnConfirmClick;
    private static DelegateBridge __Hotfix_OnDescriptionClick;
    private static DelegateBridge __Hotfix_OnCardShowPanelCloseClick;
    private static DelegateBridge __Hotfix_OnRewardGoodsUIClose;
    private static DelegateBridge __Hotfix_OnCardSelect;

    [MethodImpl((MethodImplOptions) 32768)]
    public TarotUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(int poolID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RemainRaffleCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsCardSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSrBox(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OpenSrBox(Goods goods, Action<List<Goods>> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCard(int displayCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearCardSelectedState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCostPanel(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEnoughCrystalMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ShowNotEoughItemMsgBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRechargeDialogResult(DialogBoxResult result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchRewardClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescriptionClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCardShowPanelCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardGoodsUIClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCardSelect()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
