﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRecordRewardListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallRecordRewardListItemUIController : UIControllerBase
  {
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_bgImage;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_targetDamageText;
    [AutoBind("./CommonRewardItemGroup", AutoBindAttribute.InitState.NotInit, false)]
    public Transform m_rewardGroup;
    [AutoBind("./EvaluateGroupContent/EvaluateLetterItemBig", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_evaluateImage;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetAncientCallRecordReward;
    private static DelegateBridge __Hotfix_SetBackgroundImage;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAncientCallRecordReward(
      ConfigDataAncientCallRecordRewardInfo rewardInfo,
      GameObject rewardGoodsPrefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBackgroundImage(int index)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
