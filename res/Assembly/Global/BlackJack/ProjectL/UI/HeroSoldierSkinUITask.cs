﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSoldierSkinUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroSoldierSkinUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private const string ParamKey_SoldierInfo = "SoldierInfo";
    private const string ParamKey_IsFromHeroInfoTab = "IsFromHeroInfoTab";
    private const string ParamKey_HeroObject = "HeroObj";
    private Hero m_hero;
    private bool m_isFromHeroInfoTab;
    private ConfigDataSoldierInfo m_curSoldierInfo;
    private HeroSoldierSkinUIController m_heroSoldierSkinUIController;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectSoldiersAssets;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnNewIntent;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnReturn;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnBuyHeroSkin;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnCloseHeroCharChangeTask;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnBuySoldierSkin;
    private static DelegateBridge __Hotfix_StoreSoldierSkinDetailUITask_EventOnBuySuccessEnd;
    private static DelegateBridge __Hotfix_StoreSoldierSkinDetailUITask_EventOnClose;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnWearModelSkin;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnTakeOffModelSkin;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnWearSoldierSkin;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnTakeOffSoldierSkin;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnTakeOffCharSkin;
    private static DelegateBridge __Hotfix_HeroSoldierSkinUIController_OnWearCharSkin;
    private static DelegateBridge __Hotfix_BringLayerToTop;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSoldierSkinUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroSoldierSkinUITask StartUITask(
      Hero hero,
      bool isFromHeroInfoTab,
      ConfigDataSoldierInfo curSoldierInfo,
      UIIntent preUIIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void HeroSoldierSkinUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSoldiersAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool OnNewIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnReturn(bool isFromHeroInfoTab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnBuyHeroSkin(int heroSkinInfoId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroSoldierSkinUIController_OnCloseHeroCharChangeTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnBuySoldierSkin(int soldierSkinInfoId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreSoldierSkinDetailUITask_EventOnBuySuccessEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StoreSoldierSkinDetailUITask_EventOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnWearModelSkin(
      int heroId,
      int jobRelatedId,
      int modelSkinId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnTakeOffModelSkin(
      int heroId,
      int jobRelatedId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnWearSoldierSkin(
      int heroId,
      int soldierId,
      int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnTakeOffSoldierSkin(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnTakeOffCharSkin(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroSoldierSkinUIController_OnWearCharSkin(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BringLayerToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
