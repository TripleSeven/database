﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillSoldierManualUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DrillSoldierManualUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./SoldierListPanel/PageButtonGroup/PointGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pageButtonGroup;
    [AutoBind("./SoldierListPanel/PageButtonGroup/PrevButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pagePrevButton;
    [AutoBind("./SoldierListPanel/PageButtonGroup/NextButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pageNextButton;
    [AutoBind("./SoldierListPanel/ToggleGroup/AllToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_allRankToggle;
    [AutoBind("./SoldierListPanel/ToggleGroup/Level1", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rank1Toggle;
    [AutoBind("./SoldierListPanel/ToggleGroup/Level2", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rank2Toggle;
    [AutoBind("./SoldierListPanel/ToggleGroup/Level3", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_rank3Toggle;
    [AutoBind("./SoldierListPanel/SoldierList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierListContent;
    [AutoBind("./Prefab/SoldierItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierItemPrefab;
    [AutoBind("./SoldierListPanel/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortButton;
    [AutoBind("./SoldierListPanel/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_sortButtonTypeText;
    [AutoBind("./SoldierListPanel/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sortTypes;
    [AutoBind("./SoldierListPanel/SortTypes/ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sortTypesReturnBgImage;
    [AutoBind("./SoldierListPanel/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sortTypesGridLayout;
    [AutoBind("./SoldierInfoPanel/TypeAndQuality/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierTypeIcon;
    [AutoBind("./SoldierInfoPanel/TypeAndQuality/QualityIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierQualityIcon;
    [AutoBind("./SoldierInfoPanel/TypeAndQuality/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHpValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropRangeValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/Move/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMoveValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropRestrainValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropWeakValueText;
    [AutoBind("./SoldierInfoPanel/PorpretyGroup/Type", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierPropTypeStateCtrl;
    [AutoBind("./SoldierInfoPanel/Desc/DescTextScroll/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./SoldierInfoPanel/Desc/ExpressionToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierSkillToggle;
    [AutoBind("./SoldierInfoPanel/Desc/ShortcutToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_soldierDescToggle;
    [AutoBind("./FrontSoldierShow/Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphic;
    [AutoBind("./GetSoldierNotic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_getSoldierNotic;
    [AutoBind("./GetSoldierNotic/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_getSoldierNoticText;
    [AutoBind("./GetSoldierNotic/GoToButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getSoldierNoticGoToButton;
    private ProjectLPlayerContext m_playerContext;
    private int m_curPage;
    private SoldierManualItemUIController m_curSoldierInfoCtrl;
    private UISpineGraphic m_soldierInfoGraphic;
    private List<ConfigDataSoldierInfo> m_soldierInfoList;
    private List<SoldierManualItemUIController> m_soliderItemListCtrl;
    private int m_totalPageCount;
    private int m_curRank;
    private ArmyTag m_curArmyTag;
    private bool m_isFirstIn;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_AddListenerToSortToggles;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInDrillSoliderManual;
    private static DelegateBridge __Hotfix_OnSoldierItemClick;
    private static DelegateBridge __Hotfix_SetSoldierInfoDetailPanel;
    private static DelegateBridge __Hotfix_OnPageNextButtonClick;
    private static DelegateBridge __Hotfix_OnPagePrevButtonClick;
    private static DelegateBridge __Hotfix_OnAllRankToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRank1ToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRank2ToggleValueChanged;
    private static DelegateBridge __Hotfix_OnRank3ToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSoldierSkillToggleValueChanged;
    private static DelegateBridge __Hotfix_OnSoldierDescToggleValueChanged;
    private static DelegateBridge __Hotfix_OnGetSoldierNoticGoToButtonClick;
    private static DelegateBridge __Hotfix_OnSortButtonClick;
    private static DelegateBridge __Hotfix_CloseSortTypesPanel;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_ResetData;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnNeedUpdateView;
    private static DelegateBridge __Hotfix_remove_EventOnNeedUpdateView;
    private static DelegateBridge __Hotfix_add_EventOnGoToTraingTech;
    private static DelegateBridge __Hotfix_remove_EventOnGoToTraingTech;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillSoldierManualUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddListenerToSortToggles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInDrillSoliderManual(
      List<ConfigDataSoldierInfo> soldierList,
      int totalPageCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierItemClick(SoldierManualItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierInfoDetailPanel(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPageNextButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPagePrevButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllRankToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRank1ToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRank2ToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRank3ToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierSkillToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierDescToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetSoldierNoticGoToButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSortTypesPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetData()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, ArmyTag> EventOnNeedUpdateView
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGoToTraingTech
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
