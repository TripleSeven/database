﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendInviteToGroupGetNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class FriendInviteToGroupGetNetTask : UINetTask
  {
    private List<string> m_userIDList;
    private string m_chatGroupID;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnInviteToChatGroupAck;
    private static DelegateBridge __Hotfix_set_ChatGroupInfo;
    private static DelegateBridge __Hotfix_get_ChatGroupInfo;
    private static DelegateBridge __Hotfix_set_FailedUser;
    private static DelegateBridge __Hotfix_get_FailedUser;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendInviteToGroupGetNetTask(string chatGroupID, List<string> userIDList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnInviteToChatGroupAck(
      int result,
      ProChatGroupInfo chatGroupInfo,
      ProChatUserInfo userInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public ProChatGroupInfo ChatGroupInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ProChatUserInfo FailedUser
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
