﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCharUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroCharUIController : UIControllerBase, IPointerClickHandler, IEventSystemHandler
  {
    private static List<ConfigDataHeroPerformanceInfo> HeroPerformanceInfoList = new List<ConfigDataHeroPerformanceInfo>();
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charRoot;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_root;
    [AutoBind("./Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordGo;
    [AutoBind("./Word/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_text;
    [AutoBind("./Char/0/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    private UISpineGraphic m_spineGraphic;
    private int m_performanceIndex;
    private int m_heroPerformanceId;
    private int m_heroCharSkinId;
    private ConfigDataCharImageInfo m_charImageInfo;
    private bool m_canClick;
    private bool m_isAnimationPlayed;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private HeroCharUIController.PerformanceState m_curState;
    private IAudioPlayback m_audioPlayback;
    private Hero m_hero;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_CreateGraphic_2;
    private static DelegateBridge __Hotfix_UpdateHeroPerformanceInfoList;
    private static DelegateBridge __Hotfix_CreateGraphic_0;
    private static DelegateBridge __Hotfix_CreateGraphic_1;
    private static DelegateBridge __Hotfix_GetCharImageInfo;
    private static DelegateBridge __Hotfix_GetHeroCharSkinId;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_PlayAnimation_0;
    private static DelegateBridge __Hotfix_PlayAnimation_1;
    private static DelegateBridge __Hotfix_PlayVoice;
    private static DelegateBridge __Hotfix_ShowWords;
    private static DelegateBridge __Hotfix_CloseWordPanel;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_SetSpineColor;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_IsClickInCharRoot;
    private static DelegateBridge __Hotfix_SetCanClick;
    private static DelegateBridge __Hotfix_PlayOneRandomPerformance;
    private static DelegateBridge __Hotfix_CanPlayRandomHeroPerformancesIndex;
    private static DelegateBridge __Hotfix_PlayOneSpecifiedPerformance;
    private static DelegateBridge __Hotfix_GetPerformanceIdByState;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(
      Hero hero,
      HeroCharUIController.PerformanceState performanceState = HeroCharUIController.PerformanceState.All,
      bool canClick = true,
      string assetPath = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateHeroPerformanceInfoList(
      Hero hero,
      HeroCharUIController.PerformanceState performanceState = HeroCharUIController.PerformanceState.All)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGraphic(ConfigDataHeroInfo heroInfo, string spinePath, int heroSkinId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageInfo GetCharImageInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroCharSkinId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float PlayAnimation(int idx, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string animName, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IAudioPlayback PlayVoice(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float ShowWords(int idx, float autoCloseTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CloseWordPanel(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSpineColor(Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsClickInCharRoot(GameObject clickGameObject)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float PlayOneRandomPerformance(bool canInterrupt = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanPlayRandomHeroPerformancesIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float PlayOneSpecifiedPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPerformanceIdByState(
      int favorabilityLevel,
      Hero hero,
      HeroCharUIController.PerformanceState performanceState)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum PerformanceState
    {
      All,
      List,
      Break,
      Summon,
      JobTransfer,
    }
  }
}
