﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class TeamUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TeamUIController m_teamUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    private CreateTeamRoomUIController m_createTeamRoomUIController;
    private TeamRoomInfoUITask m_teamRoomInfoUITask;
    private GameFunctionType m_lastRefreshGameFunctionType;
    private int m_lastRefreshChapterId;
    private int m_lastRefreshLocationId;
    private DateTime m_lastRefreshTeamRoomTime;
    private int m_collectionActivityId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_CreateIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_InitTeamUIController;
    private static DelegateBridge __Hotfix_UninitTeamUIController;
    private static DelegateBridge __Hotfix_InitCreateTeamRoomUIController;
    private static DelegateBridge __Hotfix_UninitCreateTeamRoomUIController;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_StartTeamRoomInviteNetTask;
    private static DelegateBridge __Hotfix_CheckTeamRoomInviteAgain;
    private static DelegateBridge __Hotfix_CheckOpenTeamRoomInfoUI;
    private static DelegateBridge __Hotfix_StartTeamRoomInfoUITask;
    private static DelegateBridge __Hotfix_TeamRoomInfoUITask_OnClose;
    private static DelegateBridge __Hotfix_RefreshTeamRoom;
    private static DelegateBridge __Hotfix_TeamUIController_OnReturn;
    private static DelegateBridge __Hotfix_TeamUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_TeamUIController_OnRefreshTeamRoom;
    private static DelegateBridge __Hotfix_TeamUIController_OnShowCreateTeamRoom;
    private static DelegateBridge __Hotfix_GetMaxFinishedLocationId;
    private static DelegateBridge __Hotfix_TeamUIController_OnAutoMatch;
    private static DelegateBridge __Hotfix_TeamUIController_OnAutoMatchCancel;
    private static DelegateBridge __Hotfix_AutoMatchCancelDialogBoxCallback;
    private static DelegateBridge __Hotfix_TeamUIController_OnSelectGameFunctionTypeAndLocation;
    private static DelegateBridge __Hotfix_TeamUIController_OnJoinTeamRoom;
    private static DelegateBridge __Hotfix_HandleJoinTeamRoomResultError;
    private static DelegateBridge __Hotfix_CreateTeamRoomUIController_OnCreateTeamRoom;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomAutoMatchInfoNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UIIntentReturnable CreateIntent(
      UIIntent fromIntent,
      GameFunctionType gameFunctionType = GameFunctionType.GameFunctionType_None,
      int chapterId = 0,
      int locationId = 0,
      int collectionActivityId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCreateTeamRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitCreateTeamRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInviteNetTask(List<string> userIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckTeamRoomInviteAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckOpenTeamRoomInfoUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RefreshTeamRoom(bool checkRefreshTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnRefreshTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnShowCreateTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMaxFinishedLocationId(GameFunctionType gameFuncitonType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnAutoMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnAutoMatchCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AutoMatchCancelDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnSelectGameFunctionTypeAndLocation(
      GameFunctionType gameFunctionType,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamUIController_OnJoinTeamRoom(
      int roomId,
      GameFunctionType gameFunctionType,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleJoinTeamRoomResultError(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateTeamRoomUIController_OnCreateTeamRoom(TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomPlayerLeaveWaitingListAndJoinRoomNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomAutoMatchInfoNtf(int frontOfYouWaitingNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
