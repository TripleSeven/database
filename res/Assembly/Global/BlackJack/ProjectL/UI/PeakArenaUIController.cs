﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaUIController : UIControllerBase
  {
    private int m_knockoutGroupId = -1;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./ScoreText", AutoBindAttribute.InitState.Inactive, false)]
    private Text m_scoreText;
    [AutoBind("./QuitCountText", AutoBindAttribute.InitState.Inactive, false)]
    private Text m_quitCountText;
    [AutoBind("./PlayerResource/Gold", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_goldGameObject;
    [AutoBind("./PlayerResource/ArenaCoin", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaCoinGameObject;
    [AutoBind("./PlayerResource/ArenaCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaCoinText;
    [AutoBind("./PlayerResource/IdolsTicket", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_jettonGameObject;
    [AutoBind("./PlayerResource/IdolsTicket/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jettonText;
    [AutoBind("./PlayerResource/IdolsTicket/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_jettonButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_managementTeamButton;
    [AutoBind("./TeamCompiledButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_managementTeamButtonAnimation;
    [AutoBind("./Detail/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_peakMarginTransform;
    [AutoBind("./Detail/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakClashToggle;
    [AutoBind("./Detail/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakBattleReportToggle;
    [AutoBind("./Detail/Margin/Tabs/ArenaLevelToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakDanToggle;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakLocalRankingToggle;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakKnockoutToggle;
    [AutoBind("./Detail/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakClashToggleState;
    [AutoBind("./Detail/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakBattleReportToggleState;
    [AutoBind("./Detail/Margin/Tabs/ArenaLevelToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakDanToggleState;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakLocalRankingToggleState;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakKnockoutToggleState;
    [AutoBind("./Detail/Margin/Tabs/ClashToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakClashLockButton;
    [AutoBind("./Detail/Margin/Tabs/BattleReportToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakBattleReportLockButton;
    [AutoBind("./Detail/Margin/Tabs/ArenaLevelToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakDanLockButton;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakLocalRankingLockButton;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle/Lock", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakKnockoutLockButton;
    [AutoBind("./Detail/Margin/Tabs/LocalRankingToggle/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakLocalRankingClickGameObject;
    [AutoBind("./Detail/Margin/Tabs/MatchToggle/Click", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakKnockoutClickGameObject;
    [AutoBind("./Detail/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakClashLadderChallengeButton;
    [AutoBind("./Detail/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakClashLadderChallengeButtonUIStateController;
    [AutoBind("./Detail/Panels/Clash/CasualMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakClashCasualChallengeButton;
    [AutoBind("./Detail/Panels/Clash/LadderMode/AreaGroup/AreaNameGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderModeAreaNameText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakClashLadderModeUIStateController;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/RestPanel/BeginTimeGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderRestPanelRestModeBeginTimeTitleText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/RestPanel/BeginTimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderRestPanelRestModeBeginTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchBeginTimePanel/TopNotceGroup/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderBeginTimeModeTopTitleText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchBeginTimePanel/NoticGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderBeginTimeModeOpenTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/NoticGroup/TimesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeBattleTimesText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/NoticGroup/WinText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeBattleWinText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/NoticGroup/FailedText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeBattleFailedText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/WinRate/WinRateValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderMatchModeWinRateValueText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/KnockOutMatchOverTimePanel/LevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakClashLadderMatchModeLevelImage;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/NoticGroup/GradingMatchText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeBattleTGradingMatchText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/NoticGroup/WinText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeBattleWinText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/NoticGroup/FailedText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchModeBattleFailedText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCompetitionInfo/CompetitionRoundGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakClashLadderGradingMatchModeCompetitionRoundGameObject;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakClashLadderOpponentModeOpponentHeadIcon;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_peakClashLadderOpponentModeOpponentHeadFrameDummy;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeOpponentLevelText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeOpponentNameText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_peakClashLadderOpponentModeOpponentInfoToggle;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/TimeLimit", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakClashLadderOpponentModeOpponentInfoTimeLimitGo;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentPanel/OpponentInfo/TimeLimit/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentModeOpponentInfoTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeEndTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/NoticGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/NoticGroup/TimesText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordTimesText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/NoticGroup/WinText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/NoticGroup/FailedText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordFailedText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/WinRate/WinRateValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderGradingMatchCloseTimeModeMatchRecordWinRateValueText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/GradingMatchCloseTimePanelPanel/MatchRecord/LevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakClashLadderGradingMatchCloseTimeModeMatchRecordLevelImage;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/WaitOpponentPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderWaitOpponentModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentAbstentionPanel/TopNotceGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentAbstentionModeTimeText;
    [AutoBind("./Detail/Panels/Clash/LadderMode/StateGroup/OpponentAbstentionPanel/NoticGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakClashLadderOpponentAbstentionModeNoticeText;
    [AutoBind("./Detail/Panels/BattleReport/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakBattleReportScrollRect;
    [AutoBind("./Detail/Panels/BattleReport/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakNoBattleReportGameObject;
    [AutoBind("./Detail/Panels/ArenaLevel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakArenaLevelScrollRect;
    [AutoBind("./Prefabs/ArenaLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakLevelListItemPrefab;
    [AutoBind("./Detail/Panels/GetLevelRewardNotice/FrameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaLevelGetRewardNoticeText;
    [AutoBind("./Detail/Panels/Ranking/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakRankingScrollRect;
    [AutoBind("./Prefabs/ArenaRankingListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakRankingListItemPrefab;
    [AutoBind("./Detail/Panels/Ranking/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingPlayerNameText;
    [AutoBind("./Detail/Panels/Ranking/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakRankingPlayerRankingUIStateController;
    [AutoBind("./Detail/Panels/Ranking/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingPlayerRankingText;
    [AutoBind("./Detail/Panels/Ranking/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakRankingPlayerRankingImage;
    [AutoBind("./Detail/Panels/Ranking/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingArenaPointsText;
    [AutoBind("./Detail/Panels/Ranking/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakRankingPlayerIconImage;
    [AutoBind("./Detail/Panels/Ranking/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_peakRankingPlayerHeadFrameTransform;
    [AutoBind("./Detail/Panels/Ranking/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakRankingPlayerLevelText;
    [AutoBind("./Detail/Panels/Match", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_knockoutGameObject;
    [AutoBind("./Detail/Margin/Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakPlayerIconImage;
    [AutoBind("./Detail/Margin/Player/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerNameText;
    [AutoBind("./Detail/Margin/Player/ArenaLevel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakDanStateCtrl;
    [AutoBind("./Detail/Margin/Player/ArenaLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakDanText;
    [AutoBind("./Detail/Margin/Player/ArenaLevel/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakDanIconImage;
    [AutoBind("./Detail/Margin/Player/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakPlayerLevelText;
    [AutoBind("./Detail/Margin/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaPointsText;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaPointsUpText1;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaPointsUpText2;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakArenaPointsUpBarImage;
    [AutoBind("./Detail/Margin/Player/Player", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaKnockOutPlayerTip;
    [AutoBind("./Detail/Margin/Player/ArenaPointsUp", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaArenaPointsUpStateCtrl;
    [AutoBind("./Detail/Margin/Player/ArenaGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaMatchGroupTextStateCtrl;
    [AutoBind("./Detail/Margin/Player/ArenaGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaKnockOutGroupText;
    [AutoBind("./Detail/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaRankingRewardButton;
    [AutoBind("./Detail/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaRankingRewardButtonStateCtrl;
    [AutoBind("./ArenaRankingReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakArenaRankingRewardUIStateCtrl;
    [AutoBind("./ArenaRankingReward/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaRankingRewardBGButton;
    [AutoBind("./ArenaRankingReward/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaRankingRewardCloseButton;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_peakArenaRankingRewardScrollRect;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView/Viewport/Content/PointRaceGroup/PointRaceTitle/ExplainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakArenaRankingRewardPointRaceExplainText;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView/Viewport/Content/PointRaceContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaRankingRewardPointRaceContent;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView/Viewport/Content/KnockoutGroupContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaRankingRewardKnockoutGroupContent;
    [AutoBind("./Prefabs/ArenaSeasonRewardListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakRankingRewardListItemPrefab;
    [AutoBind("./PromoteBattleTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteBattleUIStateController;
    [AutoBind("./PromoteBattleTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleBGButton;
    [AutoBind("./PromoteBattleTips/Panel/TipsText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteBattleText;
    [AutoBind("./PromoteBattleTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleConfirmButton;
    [AutoBind("./PromoteSucceedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteSucceedUIStateController;
    [AutoBind("./PromoteSucceedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedBGButton;
    [AutoBind("./PromoteSucceedTips/Panel/ArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteSucceedDanImage;
    [AutoBind("./PromoteSucceedTips/Panel/Info/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteSucceedDanText;
    [AutoBind("./PromoteSucceedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedConfirmButton;
    [AutoBind("./PromoteFailedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteFailedUIStateController;
    [AutoBind("./PromoteFailedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteFailedBGButton;
    [AutoBind("./PromoteFailedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteFailedConfirmButton;
    [AutoBind("./PromoteFailedTips/Panel/NowArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteFailedNowDanImage;
    [AutoBind("./PromoteFailedTips/Panel/BeforeArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteFailedOldDanImage;
    [AutoBind("./PromoteFailedTips/Panel/BeforeArenaLevelImage/Grey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteFailedOldDanGreyImage;
    [AutoBind("./PromoteSucceedGroupTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakKnockOutMatchPromoteSuccessUIStateController;
    [AutoBind("./PromoteSucceedGroupTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakKnockOutMatchPromoteSuccessBGButton;
    [AutoBind("./PromoteSucceedGroupTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakKnockOutMatchPromoteSuccessConfirmButton;
    [AutoBind("./PromoteSucceedGroupTips/Panel/ArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakKnockOutMatchPromoteSuccessDanImage;
    [AutoBind("./PromoteSucceedGroupTips/Panel/Title/WordImage", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakKnockOutMatchPromoteSuccessTitleText;
    [AutoBind("./PromoteSucceedGroupTips/Panel/Info/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakKnockOutMatchPromoteSuccessDescText;
    [AutoBind("./PromoteFailedGroupTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakKnockOutMatchPromoteFailedUIStateController;
    [AutoBind("./PromoteFailedGroupTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakKnockOutMatchPromoteFailedBGButton;
    [AutoBind("./PromoteFailedGroupTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakKnockOutMatchPromoteFailedConfirmButton;
    [AutoBind("./PromoteFailedGroupTips/Panel/NowArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakKnockOutMatchPromoteFailedDanImage;
    [AutoBind("./PromoteFailedGroupTips/Panel/BeforeArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakKnockOutMatchPromoteFailedBeforeDanImage;
    [AutoBind("./PromoteFailedGroupTips/Panel/BeforeArenaLevelImage/Grey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_peakKnockOutMatchPromoteFailedBeforeDanGreyImage;
    [AutoBind("./PromoteFailedGroupTips/Panel/Info/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_peakKnockOutMatchPromoteFailedDescText;
    [AutoBind("./MatchingFailedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingFailedUIStateController;
    [AutoBind("./MatchingFailedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedBGButton;
    [AutoBind("./MatchingFailedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedConfirmButton;
    [AutoBind("./MatchingNow", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingNowUIStateController;
    [AutoBind("./MatchingNow/Panel/Detail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_matchingNowPredictTimeGameObject;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowPredictTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingNowCancelButton;
    [AutoBind("./MatchingPlayerInfo", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingPlayerInfoStateController;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/Player/PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_matchingPlayerInfoOpponentHeadIcon;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/Player/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_matchingPlayerInfoOpponentHeadFrameDummy;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/Player/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingPlayerInfoOpponentLevelText;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/OpponentInfo/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingPlayerInfoOpponentNameText;
    [AutoBind("./MatchingPlayerInfo/Panel/Detail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingPlayerInfoTimeText;
    [AutoBind("./GradingMatchOver", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_gradingMatchOverStateCtrl;
    [AutoBind("./GradingMatchOver/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gradingMatchOverBGButton;
    [AutoBind("./GradingMatchOver/Panel/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gradingMatchOverConfirmButton;
    [AutoBind("./GradingMatchOver/Panel/LevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_gradingMatchOverDanImage;
    [AutoBind("./GradingMatchOver/Panel/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_gradingMatchOverDanText;
    [AutoBind("./Detail/Panels/Clash/FirstWinButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakFirstWinButton;
    [AutoBind("./Detail/Panels/Clash/FirstWinButton/Chest", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_peakFirstWinChestStateCtrl;
    [AutoBind("./RewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardPreviewUIStateController;
    [AutoBind("./RewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardPreviewBackgroundButton;
    [AutoBind("./RewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardPreviewScrollRect;
    [AutoBind("./ShareBattleReportPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shareBattleReportPanelUIStateCtrl;
    [AutoBind("./ShareBattleReportPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelBackgroundButton;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/NameCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelNameCardButtonn;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/NameCardButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shareBattleReportPanelNameCardButtonState;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/FriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelFriendButton;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/TopAreanButton", AutoBindAttribute.InitState.Inactive, false)]
    private Button m_shareBattleReportPanelTopAreanButton;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/WorldChannelButon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelWorldChannelButon;
    [AutoBind("./ShareBattleReportPanel/Detail/ButtonGroup/GuildButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shareBattleReportPanelGuildButton;
    private bool m_isMatchingNow;
    private PeakArenaPanelType m_curPeakArenaPanelType;
    private List<ConfigDataPeakArenaDanInfo> m_peakArenaDans;
    private GameObjectPool<ArenaLevelListItemUIController> m_peakArenaLevelPool;
    private GameObjectPool<PeakArenaRankingListItemUIController> m_peakArenaRankingPool;
    private GameObjectPool<PeakArenaReportListItemUIController> m_peakArenaReportPool;
    private ulong m_curBattleReportInstanceId;
    private PeakArenaKnockoutUIController m_peakArenaKnockoutUIController;
    private GameObject m_opponentFrameCloneGameObject;
    private GameObject m_waitOpponentFrameCloneGameObject;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetUIListeners;
    private static DelegateBridge __Hotfix_SetGameObjectPools;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateToggleState;
    private static DelegateBridge __Hotfix_UpdatePeakArena;
    private static DelegateBridge __Hotfix_UpdateCurrencyState;
    private static DelegateBridge __Hotfix_UpdateClashStateSeasonTimeState;
    private static DelegateBridge __Hotfix_ShareBattleReportPanelClose;
    private static DelegateBridge __Hotfix_SetKnockoutGroupId;
    private static DelegateBridge __Hotfix_SetEditorModeUI;
    private static DelegateBridge __Hotfix_SetLeftPlayerInfo;
    private static DelegateBridge __Hotfix_SetRewardButtonUIState;
    private static DelegateBridge __Hotfix_SetClashState;
    private static DelegateBridge __Hotfix_SetBattleReportState;
    private static DelegateBridge __Hotfix_IComparorPeakBattleReport;
    private static DelegateBridge __Hotfix_SetKnockoutState;
    private static DelegateBridge __Hotfix_RefreshBattleReportName;
    private static DelegateBridge __Hotfix_SetArenaLevelState;
    private static DelegateBridge __Hotfix_SetRankingState;
    private static DelegateBridge __Hotfix_GetLadderModeStateName;
    private static DelegateBridge __Hotfix_SetPeakClashLadderRestMode;
    private static DelegateBridge __Hotfix_SetPeakClashLadderBeginTimeMode;
    private static DelegateBridge __Hotfix_SetPeakClashLadderMatchMode;
    private static DelegateBridge __Hotfix_SetPeakClashLadderGradingMatchMode;
    private static DelegateBridge __Hotfix_SetPeakClashLadderGradingMatchCloseTimeMode;
    private static DelegateBridge __Hotfix_SetPeakClashLadderOpponentMode;
    private static DelegateBridge __Hotfix_GetKnockOutMatchReadyLastTime;
    private static DelegateBridge __Hotfix_ShowKnockOutMatchingPlayerInfoPanel;
    private static DelegateBridge __Hotfix_SetKnockOutMatchingPlayerInfoPanel;
    private static DelegateBridge __Hotfix_UpdateKnockOutMatchingPlayerInfoPanelTimeText;
    private static DelegateBridge __Hotfix_CloseKnockOutMatchingPlayerInfoPanel;
    private static DelegateBridge __Hotfix_SetPeakClashLadderWaitOpponentMode;
    private static DelegateBridge __Hotfix_SetPeakClashLadderOpponentAbstentionMode;
    private static DelegateBridge __Hotfix_GetKnockOutMatchTitleTipText;
    private static DelegateBridge __Hotfix_GetSeasonBeginTime;
    private static DelegateBridge __Hotfix_GetSeasonBeginTimeTitleText;
    private static DelegateBridge __Hotfix_GetSeasonPointMatchEndTime;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleSeason;
    private static DelegateBridge __Hotfix_ShowArenaTopPromotionTipDialog;
    private static DelegateBridge __Hotfix_GetField;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnManagementTeamClick;
    private static DelegateBridge __Hotfix_OnPeakClashCasualChallengeButtonClick;
    private static DelegateBridge __Hotfix_OnPeakClashLadderChallengeButtonClick;
    private static DelegateBridge __Hotfix_OnReportShareButtonClick;
    private static DelegateBridge __Hotfix_OnReportReplyButtonClick;
    private static DelegateBridge __Hotfix_OnBattleReportNameChangeButtonClick;
    private static DelegateBridge __Hotfix_OnShareBattleReportPanelGuildButtonClick;
    private static DelegateBridge __Hotfix_OnShareBattleReportPanelWorldChannelButtonClick;
    private static DelegateBridge __Hotfix_OnShareBattleReportPanelTopAreanButtonClick;
    private static DelegateBridge __Hotfix_OnShareBattleReportPanelFriendButtonClick;
    private static DelegateBridge __Hotfix_OnShareBattleReportPanelNameCardButtonClick;
    private static DelegateBridge __Hotfix_OnShareBattleReportPanelBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnPeakClashToggle;
    private static DelegateBridge __Hotfix_OnPeakBattleReportToggle;
    private static DelegateBridge __Hotfix_OnPeakDanToggle;
    private static DelegateBridge __Hotfix_OnPeakLocalRankingToggle;
    private static DelegateBridge __Hotfix_OnPeakKnockoutToggle;
    private static DelegateBridge __Hotfix_OnToggleLockClick;
    private static DelegateBridge __Hotfix_OnPeakArenaRankingRewardButtonClick;
    private static DelegateBridge __Hotfix_ShowPromoteBattle;
    private static DelegateBridge __Hotfix_ShowPromoteSucceed;
    private static DelegateBridge __Hotfix_ShowMatchingFailed;
    private static DelegateBridge __Hotfix_ShowMatchingNow;
    private static DelegateBridge __Hotfix_HideMatchingNow;
    private static DelegateBridge __Hotfix_SetMatchingNowTime;
    private static DelegateBridge __Hotfix_SetMatchingPredictTime;
    private static DelegateBridge __Hotfix_IsMatchingNowPredictTimeActive;
    private static DelegateBridge __Hotfix_OnMatchingNowCancelButtonClick;
    private static DelegateBridge __Hotfix_SetLadderWeekWinRewardStatus;
    private static DelegateBridge __Hotfix_OnPeakFirstWinButtonClick;
    private static DelegateBridge __Hotfix_ShowGradingMatchOverPanel;
    private static DelegateBridge __Hotfix_ShowPromoteFailed;
    private static DelegateBridge __Hotfix_ShowPeakKnockOutMatchPromoteSuccessPanel;
    private static DelegateBridge __Hotfix_ShowPeakKnockOutMatchPromoteFailedPanel;
    private static DelegateBridge __Hotfix_OnPeakKnockOutMatchPromoteSuccessBGButton;
    private static DelegateBridge __Hotfix_OnPeakKnockOutMatchPromoteFailedBGButton;
    private static DelegateBridge __Hotfix_OnPromoteFailedBGButtonClick;
    private static DelegateBridge __Hotfix_OnGradingMatchOverConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnRewardPreviewBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnPromoteBattleConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnPromoteSucceedConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnMatchingFailedConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnPeakArenaRankingRewardCloseButtonClick;
    private static DelegateBridge __Hotfix_OnAddJettonTicketClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnManagementTeam;
    private static DelegateBridge __Hotfix_remove_EventOnManagementTeam;
    private static DelegateBridge __Hotfix_add_EventOnMatchingCancel;
    private static DelegateBridge __Hotfix_remove_EventOnMatchingCancel;
    private static DelegateBridge __Hotfix_add_EventOnAddJettonTicket;
    private static DelegateBridge __Hotfix_remove_EventOnAddJettonTicket;
    private static DelegateBridge __Hotfix_add_EventOnReportReply;
    private static DelegateBridge __Hotfix_remove_EventOnReportReply;
    private static DelegateBridge __Hotfix_add_EventOnReportNameChange;
    private static DelegateBridge __Hotfix_remove_EventOnReportNameChange;
    private static DelegateBridge __Hotfix_add_EventOnPeakClashCasualChallengeButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPeakClashCasualChallengeButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPeakClashLadderChallengeButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnPeakClashLadderChallengeButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowPeakPanel;
    private static DelegateBridge __Hotfix_remove_EventOnShowPeakPanel;
    private static DelegateBridge __Hotfix_add_EventOnShareBattleReport;
    private static DelegateBridge __Hotfix_remove_EventOnShareBattleReport;

    private PeakArenaUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIListeners()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGameObjectPools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateToggleState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePeakArena(PeakArenaPanelType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateCurrencyState(PeakArenaPanelType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateClashStateSeasonTimeState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShareBattleReportPanelClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetKnockoutGroupId(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEditorModeUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardButtonUIState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetClashState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleReportState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int IComparorPeakBattleReport(BattleReportHead r1, BattleReportHead r2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetKnockoutState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshBattleReportName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetArenaLevelState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetLadderModeStateName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderRestMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderBeginTimeMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderMatchMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderGradingMatchMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderGradingMatchCloseTimeMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderOpponentMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TimeSpan GetKnockOutMatchReadyLastTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowKnockOutMatchingPlayerInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetKnockOutMatchingPlayerInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateKnockOutMatchingPlayerInfoPanelTimeText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseKnockOutMatchingPlayerInfoPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderWaitOpponentMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakClashLadderOpponentAbstentionMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetKnockOutMatchTitleTipText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSeasonBeginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSeasonBeginTimeTitleText()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetSeasonPointMatchEndTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetPeakArenaBattleSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowArenaTopPromotionTipDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T GetField<T>(object instance, string fieldname)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnManagementTeamClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportShareButtonClick(ulong reportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReportReplyButtonClick(ulong reportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleReportNameChangeButtonClick(ulong reportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelGuildButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelWorldChannelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelTopAreanButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelNameCardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShareBattleReportPanelBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakClashToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakBattleReportToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakDanToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakLocalRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakKnockoutToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleLockClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaRankingRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteBattle(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteSucceed(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingNowTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingPredictTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMatchingNowPredictTimeActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingNowCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderWeekWinRewardStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakFirstWinButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGradingMatchOverPanel(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteFailed(int oldDanId, int nowDanId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPeakKnockOutMatchPromoteSuccessPanel(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPeakKnockOutMatchPromoteFailedPanel(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakKnockOutMatchPromoteSuccessBGButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakKnockOutMatchPromoteFailedBGButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteFailedBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGradingMatchOverConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteBattleConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteSucceedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingFailedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaRankingRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddJettonTicketClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnManagementTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMatchingCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddJettonTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnReportReply
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong> EventOnReportNameChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakClashCasualChallengeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakClashLadderChallengeButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaPanelType> EventOnShowPeakPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PeakArenaBattleReportType, ulong> EventOnShareBattleReport
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
