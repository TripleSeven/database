﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UIAsset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class UIAsset
  {
    public const string UISpineMaterial = "UI/Common_New_ABS/Material/UISpine.mat";
    public const string UISpine2Material = "UI/Common_New_ABS/Material/UISpine2.mat";
    public const string UISpineMaterial_SepAlpha = "UI/Common_New_ABS/Material/UISpine_SepAlpha.mat";
    public const string UISpine2Material_SepAlpha = "UI/Common_New_ABS/Material/UISpine2_SepAlpha.mat";
    public const string HeroFragmentIconMaterial = "UI/Icon/Piece_ABS/Material/PieceIcon.mat";
    public const string HeroSummonIconMaterial = "UI/Icon/Card_ABS/Material/CardIcon.mat";
    public const string HeroSkinIconMaterial = "UI/Icon/HeroSkin_ABS/Material/HeroSkinIcon.mat";
    public const string SoldierSkinIconMaterial = "UI/Icon/SoldierSkin_ABS/Material/SoldierSkinIcon.mat";
    public const string TeamIcon0 = "UI/Common_New_ABS/Icon/Bead_Blue.png";
    public const string TeamIcon1 = "UI/Common_New_ABS/Icon/Bead_Green.png";
    public const string HpBar0 = "UI/Common_New_ABS/Pattern_Blood_Green.png";
    public const string HpBar1 = "UI/Common_New_ABS/Pattern_Blood_Red.png";
    public const string HpBarNpc = "UI/Common_New_ABS/Pattern_Blood_Blue.png";
    public const string ActorInfoHpBar0 = "UI/Common_New_ABS/Pattern_Blood_Green01.png";
    public const string ActorInfoHpBar1 = "UI/Common_New_ABS/Pattern_Blood_Red01.png";
    public const string BattlePlayerTag1 = "UI/Common_New_ABS/Font/Word_1P.png";
    public const string BattlePlayerTag2 = "UI/Common_New_ABS/Font/Word_2P.png";
    public const string BattlePlayerTag3 = "UI/Common_New_ABS/Font/Word_3P.png";
    public const string GoldIcon = "UI/Icon/Item_ABS/Item_Gold.png";
    public const string ExpIcon = "UI/Icon/Item_ABS/Item_PlayerExp.png";
    public const string CrystalIcon = "UI/Icon/Item_ABS/Item_MagicCrystal.png";
    public const string EnergyIcon = "UI/Icon/Item_ABS/Item_Bread.png";
    public const string FriendShipPointIcon = "UI/Common_New_ABS/Icon/Icon_Friendship_1.png";
    public const string ArenaTicketIcon = "UI/Icon/Item_ABS/Item_PVPTicket.png";
    public const string ArenaHonourIcon = "UI/Icon/Item_ABS/Item_PvpCoin.png";
    public const string SkinTicketIcon = "UI/Icon/Item_ABS/Item_ETicket-God.png";
    public const string MemoryEssenceIcon = "UI/Icon/Item_ABS/Item_MemoryEssence.png";
    public const string MithralStoneIcon = "UI/Icon/Item_ABS/Icon_DragonScale_01.png";
    public const string BrillianceMithralStoneIcon = "UI/Icon/Item_ABS/Icon_DragonScale_02.png";
    public const string GuildMedalIcon = "UI/Common_New_ABS/Icon/Icon_GuildCoin.png";
    public const string ChallengePointIcon = "UI/Common_New_ABS/Icon/Icon_Challenge.png";
    public const string FashionPointIcon = "UI/Common_New_ABS/Icon/Icon_Skin.png";
    public const string ItemIconRank0Frame = "UI/Common_New_ABS/Border_Icon_Copper.png";
    public const string ItemIconRank1Frame = "UI/Common_New_ABS/Border_Icon_Copper.png";
    public const string ItemIconRank2Frame = "UI/Common_New_ABS/Border_Icon_Silver.png";
    public const string ItemIconRank3Frame = "UI/Common_New_ABS/Border_Icon_Gold.png";
    public const string ItemIconRank4Frame = "UI/Common_New_ABS/Border_Icon_Colour.png";
    public const string ItemIconRank5Frame = "UI/Common_New_ABS/Border_Icon_Colour.png";
    public const string ItemIconRank9Frame = "UI/Common_New_ABS/Border_Icon_Gray.png";
    public const string RankNImage = "UI/Common_New_ABS/Font/Rank_N.png";
    public const string RankRImage = "UI/Common_New_ABS/Font/Rank_R.png";
    public const string RankSRImage = "UI/Common_New_ABS/Font/Rank_SR.png";
    public const string RankSSRImage = "UI/Common_New_ABS/Font/Rank_SSR.png";
    public const string PeakArenaPromoteImage1 = "UI/Arena_ABS/PeakRankIcon/PeakArena_Champion.png";
    public const string PeakArenaPromoteImage2 = "UI/Arena_ABS/PeakRankIcon/PeakArena_Secondplace.png";
    public const string PeakArenaPromoteImage8 = "UI/Arena_ABS/PeakRankIcon/PeakArena_TheGreat8.png";
    public const string PeakArenaPromoteImage16 = "UI/Arena_ABS/PeakRankIcon/PeakArena_TheGreat16.png";
    public const string PeakArenaPromoteImage256 = "UI/Arena_ABS/PeakRankIcon/PeakArena_TheGreat256.png";
    public const string SoldierRank1Image = "UI/Common_New_ABS/Font/Word_1.png";
    public const string SoldierRank2Image = "UI/Common_New_ABS/Font/Word_2.png";
    public const string SoldierRank3Image = "UI/Common_New_ABS/Font/Word_3.png";
    public const string PropertyRankSIcon = "UI/Common_New_ABS/Font/Font_S.png";
    public const string PropertyRankAIcon = "UI/Common_New_ABS/Font/Font_A.png";
    public const string PropertyRankBIcon = "UI/Common_New_ABS/Font/Font_B.png";
    public const string PropertyRankCIcon = "UI/Common_New_ABS/Font/Font_C.png";
    public const string PropertyRankDIcon = "UI/Common_New_ABS/Font/Font_D.png";
    public const string PropertyRankEIcon = "UI/Common_New_ABS/Font/Font_E.png";
    public const string HeroListRank1Frame = "UI/HeadFrame_ABS/Card_Thumbnail_04.png";
    public const string HeroListRank2Frame = "UI/HeadFrame_ABS/Card_Thumbnail_03.png";
    public const string HeroListRank3Frame = "UI/HeadFrame_ABS/Card_Thumbnail_02.png";
    public const string HeroListRank4Frame = "UI/HeadFrame_ABS/Card_Thumbnail_01.png";
    public const string HeroListRank5Frame = "UI/HeadFrame_ABS/Card_Thumbnail_01.png";
    public const string HeroJobTransferRank1Img = "UI/JobTransfer_ABS/Pattern_Job_01.png";
    public const string HeroJobTransferRank2Img = "UI/JobTransfer_ABS/Pattern_Job_02.png";
    public const string HeroJobTransferRank3Img = "UI/JobTransfer_ABS/Pattern_Job_03.png";
    public const string HeroJobTransferRank4Img = "UI/JobTransfer_ABS/Pattern_Job_04.png";
    public const string HeroJobTransferRank5Img = "UI/JobTransfer_ABS/Pattern_Job_05.png";
    public const string HeroJobTransferValUp = "UI/Common_New_ABS/Icon_Up.png";
    public const string HeroJobTransferValSame = "UI/Common_New_ABS/Icon_Minus.png";
    public const string HeroJobTransferValDown = "UI/Common_New_ABS/Icon_Down.png";
    public const string Ranking1Image = "UI/Common_New_ABS/Icon/Icon_One.png";
    public const string Ranking2Image = "UI/Common_New_ABS/Icon/Icon_Two.png";
    public const string Ranking3Image = "UI/Common_New_ABS/Icon/Icon_Three.png";
    public const string ActivityPlusIcon = "UI/Common_New_ABS/Icon/Icon_Plus_01.png";
    public const string ActivityEqualIcon = "UI/Common_New_ABS/Icon/Icon_Equal_01.png";
    public const string ActivityIcon = "UI/Activity_ABS/Icon_activity.png";
    public const string AnnouncementIcon = "UI/Activity_ABS/Icon_Repair.png";
    public const string LoginNoticeLabel = "UI/TextImages/English_Login_ABS/Label_Notice.png";
    public const string LoginActivityLabel = "UI/TextImages/English_Login_ABS/Label_Activity.png";
    public const string TouchHitFx = "FX/UI_ABS/UI_Fx/U_click_down.prefab";
    public const string TouchHitFx2 = "FX/UI_ABS/UI_Fx/U_click_down.prefab";
    public const string TouchLoopFx = "FX/UI_ABS/UI_Fx/U_click.prefab";
    public const string TouchLoopFx2 = "FX/UI_ABS/UI_Fx/U_click_open.prefab";
    public const string HPBreakFxLarge = "FX/UI_ABS/UIFX_HPBarBreak_L_Pfb.prefab";
    public const string HPBreakFxSmall = "FX/UI_ABS/UIFX_HPBarBreak_S_Pfb.prefab";
    public const string TreasureMapCharSpine1 = "Spine/General/Goblin_ABS/Goblin_1_Prefab.prefab";
    public const string TreasureMapCharSpine2 = "Spine/General/Goblin_ABS/Goblin_2_Prefab.prefab";
    public const string TreasureMapCharSpine3 = "Spine/General/Goblin_ABS/Goblin_3_Prefab.prefab";
    public const string TreasureMapCharSpine4 = "Spine/General/Goblin_ABS/Goblin_4_Prefab.prefab";
    public const string StorePrivilegeIconFrame = "UI/Common_New_ABS/Border_Icon_Colour.png";
    public const string DrillBgSlot0 = "UI/Drill_ABS/1/1.png";
    public const string DrillBgSlot1 = "UI/Drill_ABS/1/2.png";
    public const string DrillBgSlot2 = "UI/Drill_ABS/1/3.png";
    public const string WeaponTypeIcon = "UI/Common_New_ABS/Icon/Icon_Weapon.png";
    public const string ArmorTypeIcon = "UI/Common_New_ABS/Icon/Icon_Equipment.png";
    public const string HelmetTypeIcon = "UI/Common_New_ABS/Icon/Icon_Shoes.png";
    public const string OrnamentTypeIcon = "UI/Common_New_ABS/Icon/Icon_Ornaments.png";
    public static readonly string[] GoldNumbers;
    public static readonly string[] ServerStateSpritePath;
    public const string DialogBox = "Assets/GameProject/RuntimeAssets/UI/Common_ABS/Prefab/DialogBox.prefab";
    public const string RealtimePVPNoticePrefab = "UI/UserGuide_ABS/Prefab/UserGuide_17UIPrefab.prefab";
    public const string DefaultSelectCardClockPrefab = "UI/SelectCard_ABS/Prefab/Clock.prefab";
    public const string SkillCutscenePrefab = "Cutscene/Skill_ABS/CutsceneSkill1Prefab.prefab";
    public const string SkillCutsceneCameraAnimation = "CutsceneCameraSkill";
    public const string SkillCutscene2CameraAnimation = "Cutscene2CameraSkill";
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIAsset()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static UIAsset()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
