﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./OrganizeTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_teamButton;
    [AutoBind("./StoreButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_storeButton;
    [AutoBind("./Margin/Toggle/DailyToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_dailyToggle;
    [AutoBind("./Margin/Toggle/LimitTimeToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_limitTimeToggle;
    [AutoBind("./Margin/Toggle/ChallengeToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_challengeToggle;
    [AutoBind("./UnchartedList/DailyUnchartedList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_dailyScrollRect;
    [AutoBind("./UnchartedList/LimitTimeUnchartedList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_limitTimeScrollRect;
    [AutoBind("./UnchartedList/ChallengeUnchartedList", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_challengeScrollRect;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Aniki", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_anikiButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Aniki", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_anikiUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Aniki/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_anikiDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Thearchy", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_thearchyButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Thearchy", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_thearchyUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Thearchy/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_thearchyDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/TreasureMap", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_treasureMapButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/TreasureMap", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_treasureMapUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/TreasureMap/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_treasureMapChallengeCountText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/MemoryCorridor", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryCorridorButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/MemoryCorridor", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryCorridorUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/MemoryCorridor/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_memoryCorridorDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Training", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroTrainingButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Training", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroTrainingUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/Training/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroTrainingDailyRewardText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_climbTowerButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/ClimbTower", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_climbTowerUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/ClimbTower/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_climbTowerFlushtText;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/EternalShrine", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_eternalShrineButton;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/EternalShrine", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_eternalShrineUIStateController;
    [AutoBind("./UnchartedList/DailyUnchartedList/Viewport/Content/EternalShrine/DailyReward/NumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_eternalShrineChallengeCountText;
    [AutoBind("./UnchartedList/ChallengeUnchartedList/Viewport/Content/HeroAnthem", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroAnthemButton;
    [AutoBind("./UnchartedList/ChallengeUnchartedList/Viewport/Content/HeroAnthem", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroAnthemUIStateController;
    [AutoBind("./UnchartedList/ChallengeUnchartedList/Viewport/Content/AncientCall", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ancientCallGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/HeroPhantom", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroPhantomButtonPrefab;
    [AutoBind("./Prefabs/CooperateBattle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_cooperateBattleButtonPrefab;
    [AutoBind("./Prefabs/UnchartedScore", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unchartedScoreButtonPrefab;
    [AutoBind("./Prefabs/CollectionActivity", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_collectionActivityButtonPrefab;
    private bool m_isIgnoreToggleEvent;
    private AncientCallActivityButton m_ancientCallActivityButton;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SetTabType;
    private static DelegateBridge __Hotfix_SetDailyItems;
    private static DelegateBridge __Hotfix_SetLimitTimeItems;
    private static DelegateBridge __Hotfix_SetChallengeItems;
    private static DelegateBridge __Hotfix_SetClimbTowerFlushTime;
    private static DelegateBridge __Hotfix_SetButtonOpened;
    private static DelegateBridge __Hotfix_CheckButtonOpened;
    private static DelegateBridge __Hotfix_FireEventOnShowUncharted;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnTeamButtonClick;
    private static DelegateBridge __Hotfix_OnStoreButtonClick;
    private static DelegateBridge __Hotfix_OnDailyToggleValueChanged;
    private static DelegateBridge __Hotfix_OnLimitTimeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnChallengeToggleValueChanged;
    private static DelegateBridge __Hotfix_OnAnikiButtonClick;
    private static DelegateBridge __Hotfix_OnThearchyButtonClick;
    private static DelegateBridge __Hotfix_OnTreasureMapButtonClick;
    private static DelegateBridge __Hotfix_OnMemoryCorridorButtonClick;
    private static DelegateBridge __Hotfix_OnHeroTrainingButtonClick;
    private static DelegateBridge __Hotfix_OnClimbTowerButtonClick;
    private static DelegateBridge __Hotfix_OnEternalShrineButtonClick;
    private static DelegateBridge __Hotfix_OnHeroAnthemButtonClick;
    private static DelegateBridge __Hotfix_OnAncientCallButtonClick;
    private static DelegateBridge __Hotfix_OnHeroPhantomButtonClick;
    private static DelegateBridge __Hotfix_OnCooperateBattleButtonClick;
    private static DelegateBridge __Hotfix_OnUnchartedScoreButtonClick;
    private static DelegateBridge __Hotfix_OnCollectionButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowUncharted;
    private static DelegateBridge __Hotfix_remove_EventOnShowUncharted;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowTeam;
    private static DelegateBridge __Hotfix_remove_EventOnShowTeam;
    private static DelegateBridge __Hotfix_add_EventOnStoreClick;
    private static DelegateBridge __Hotfix_remove_EventOnStoreClick;
    private static DelegateBridge __Hotfix_add_EventOnChangeTabType;
    private static DelegateBridge __Hotfix_remove_EventOnChangeTabType;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTabType(int tabIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTimeItems(
      List<ConfigDataCooperateBattleInfo> cooperateBattleInfos,
      List<ConfigDataHeroPhantomInfo> heroPhantomInfos,
      List<ConfigDataUnchartedScoreInfo> unchartedScoreInfos,
      List<ConfigDataCollectionActivityInfo> collectionActivityInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengeItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClimbTowerFlushTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetButtonOpened(CommonUIStateController ctrl, GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckButtonOpened(GameFunctionType gameFuncType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FireEventOnShowUncharted(BattleType battleType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeamButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStoreButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDailyToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLimitTimeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChallengeToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAnikiButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnThearchyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTreasureMapButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemoryCorridorButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroTrainingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClimbTowerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEternalShrineButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroAnthemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAncientCallButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroPhantomButtonClick(HeroPhantomButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCooperateBattleButtonClick(CooperateBattleButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnchartedScoreButtonClick(UnchartedScoreButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCollectionButtonClick(CollectionActivityButton ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int> EventOnShowUncharted
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStoreClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnChangeTabType
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
