﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MusicAppreciateItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MusicAppreciateItemUIController : UIControllerBase
  {
    public Action<MusicAppreciateItemUIController, bool> EventOnMusicClick;
    [AutoBind("./MusicNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_musicButton;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_musicButtonState;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private ConfigDataMusicAppreciateTable m_musicAppreciateTable;
    private bool m_isPaly;
    private int m_index;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_StopMusic;
    private static DelegateBridge __Hotfix_PlayMusic;
    private static DelegateBridge __Hotfix_SetPlayMusicState;
    private static DelegateBridge __Hotfix_SetMusicData;
    private static DelegateBridge __Hotfix_GetMusicInfo;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnMusicClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayMusic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayMusicState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMusicData(
      ConfigDataMusicAppreciateTable musicAppreciateTable,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataMusicAppreciateTable GetMusicInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMusicClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
