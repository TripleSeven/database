﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomInviteUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomInviteUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private TeamRoomInviteUIController m_teamRoomInviteUIController;
    private TeamRoomInvitePlayerType m_playerType;
    private List<UserSummary> m_playerList;
    private List<string> m_updateStatusUserIds;
    private List<TeamRoomInviteUITask.OnlinePlayerStatus> m_onlinePlayersStatus;
    private GameFunctionType m_onlinePlayersStatusGameFunctionType;
    private int m_onlinePlayersStatusLocationId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_PrepareForStartOrResume;
    private static DelegateBridge __Hotfix_GetGuildInfo;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_InitTeamRoomInviteUIController;
    private static DelegateBridge __Hotfix_UninitTeamRoomInviteUIController;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_StartTeamRoomInviteeInfoGetNetTask;
    private static DelegateBridge __Hotfix_IsUserInTeamRoom;
    private static DelegateBridge __Hotfix_ComparePlayerLevel;
    private static DelegateBridge __Hotfix_CloseAndReturnPrevUITask;
    private static DelegateBridge __Hotfix_UpdateOnlinePlayerStatus;
    private static DelegateBridge __Hotfix_GetOnlinePlayerStatus;
    private static DelegateBridge __Hotfix_TeamRoomInviteUIController_OnConfirm;
    private static DelegateBridge __Hotfix_TeamRoomInviteUIController_OnCancel;
    private static DelegateBridge __Hotfix_TeamRoomInviteUIController_OnChangePlayerType;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomInviteeInfoNtf;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomInviteUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PrepareForStartOrResume(UIIntent intent, Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetGuildInfo(Action<bool> onPrepareEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitTeamRoomInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitTeamRoomInviteUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInviteeInfoGetNetTask(
      List<string> userIds,
      TeamRoomInviteeInfoType infoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsUserInTeamRoom(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComparePlayerLevel(UserSummary u1, UserSummary u2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAndReturnPrevUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateOnlinePlayerStatus(string userId, TeamRoomPlayerInviteState inviteState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamRoomInviteUITask.OnlinePlayerStatus GetOnlinePlayerStatus(
      string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUIController_OnConfirm(List<string> playerUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUIController_OnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomInviteUIController_OnChangePlayerType(TeamRoomInvitePlayerType playerType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInviteeInfoNtf(
      string inviteeUserId,
      int levelUnlocked,
      int guildMassiveCombatAvailable)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class OnlinePlayerStatus
    {
      public DateTime UpdateTime;
      public string UserId;
      public TeamRoomPlayerInviteState InviteState;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public OnlinePlayerStatus()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
