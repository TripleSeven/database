﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersConfessionSkillItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersConfessionSkillItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private ToggleEx m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./SkillGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvValueText;
    [AutoBind("./SkillGroup/RecPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redPoint;
    [AutoBind("./BlackBG/UnlockConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockConditionText;
    [AutoBind("./SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillIconImage;
    [AutoBind("./SkillGroup/UnlockSelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_unlockSelectImage;
    [AutoBind("./LockSelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockSelectImage;
    private bool m_hasUnLocked;
    private int m_unlockFavoribilityLevel;
    private int m_curFavoribilityLevel;
    private int m_heroId;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private int m_skillLv;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitFettersConfessionSkillItem;
    private static DelegateBridge __Hotfix_OnSkillToggleValueChanged;
    private static DelegateBridge __Hotfix_PlayUnlockEffect;
    private static DelegateBridge __Hotfix_PlayPromoteEffect;
    private static DelegateBridge __Hotfix_ShowSelectImage;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;
    private static DelegateBridge __Hotfix_set_HeroFetterInfo;
    private static DelegateBridge __Hotfix_get_HeroFetterInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFettersConfessionSkillItem(
      int fetterId,
      int curFavoribilityLevel,
      bool hasUnLock,
      int skillLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayUnlockEffect(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPromoteEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FettersConfessionSkillItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroFetterInfo HeroFetterInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
