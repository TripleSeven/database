﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DialogDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public sealed class DialogDetailUIController : UIControllerBase
  {
    private List<DialogDetailCharDialogInfoUIController> m_dialogDetailCharDialogInfoUIControllerList;
    private List<DialogDetailChoiceUIController> m_dialogDetailChoiceUIController;
    private List<DialogDetailCharDialogInfoUIController> m_unusedDialogDetailCharDialogInfoUIControllerList;
    private List<DialogDetailChoiceUIController> m_unusedDialogDetailChoiceUIController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx BGButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController DialogDetailUIPrefabCommonUIStateController;
    [AutoBind("./Detail/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject ContentGameObject;
    [AutoBind("./Detail/BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx BackButton;
    [AutoBind("./Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect ScrollView;
    [AutoBind("./Prefab/CharDialogInfo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject CharDialogInfoGameObject;
    [AutoBind("./Prefab/Choice", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject ChoiceGameObject;
    [AutoBind("./Prefab/ChoiceItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject ChoiceItemGameObject;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_SetLogDataList;
    private static DelegateBridge __Hotfix_DeactiveDialogInfoList;
    private static DelegateBridge __Hotfix_GetDialogDetailCharDialogInfo;
    private static DelegateBridge __Hotfix_GetDialogDetailChoice;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_PanelShow;
    private static DelegateBridge __Hotfix_OnPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    public DialogDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLogDataList(List<DialogDetailUITask.LogData> logDataList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DeactiveDialogInfoList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DialogDetailCharDialogInfoUIController GetDialogDetailCharDialogInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DialogDetailChoiceUIController GetDialogDetailChoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PanelShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
