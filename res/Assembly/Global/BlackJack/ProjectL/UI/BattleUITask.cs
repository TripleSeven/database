﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.Scene;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  public class BattleUITask : UITask, IClientBattleListener
  {
    private const int BattleDanmakuUICtrlIndex = 0;
    private const int BattleRoomUICtrlIndex = 1;
    private const int BattleUICtrlIndex = 2;
    private const int BattleActorInfoUICtrlIndex = 3;
    private const int BattlePrepareUICtrlIndex = 4;
    private const int PVPBattlePrepareUICtrlIndex = 5;
    private const int PeakArenaBattlePrepareUICtrlIndex = 6;
    private const int BattlePrepareActorInfoUICtrlIndex = 7;
    private const int ActionOrderUICtrlIndex = 8;
    private const int BattleTreasureDialogUICtrlIndex = 9;
    private const int CombatUICtrlIndex = 10;
    private const int PreCombatUICtrlIndex = 11;
    private const int BattleDanmakuLiveUICtrlIndex = 12;
    private const int BattlePauseUICtrlIndex = 13;
    private const int BattleCommonUICtrlIndex = 14;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private BattleUIController m_battleUIController;
    private BattleDanmakuUIController m_battleDanmakuUIController;
    private BattleActorInfoUIController m_battleActorInfoUIController;
    private BattlePrepareUIController m_battlePrepareUIController;
    private PVPBattlePrepareUIController m_pvpBattlePrepareUIController;
    private PeakArenaBattlePrepareUIController m_peakArenaBattlePrepareUIController;
    private BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController;
    private BattleDanmakuUIController m_battleDanmakuLiveUIController;
    private ActionOrderUIController m_actionOrderUIController;
    private BattlePauseUIController m_battlePauseUIController;
    private BattleTreasureDialogUIController m_battleTreasureDialogUIController;
    private CombatUIController m_combatUIController;
    private PreCombatUIController m_preCombatUIController;
    private BattleRoomUIController m_battleRoomUIController;
    private BattleCommonUIController m_battleCommonUIController;
    private BattleMapUIController m_battleMapUIController;
    private BattleSceneUIController m_battleSceneUIController;
    private CombatSceneUIController m_combatSceneUIController;
    private ClientBattle m_clientBattle;
    private IConfigDataLoader m_configDataLoader;
    private BattleUIState m_uiState;
    private ClientBattleActor m_activeActor;
    private ClientBattleActor m_currentBossActor;
    private GridPosition m_activeActorInitPosition;
    private int m_activeActorInitDirection;
    private int m_skillIndex;
    private GridPosition m_skillTargetPosition;
    private GridPosition m_skillTargetPosition2;
    private GridPosition m_combatStartPosition;
    private GridPosition m_combatTargetPosition;
    private bool m_isBattleCutsceneFade;
    private bool m_isCombatCutsceneFade;
    private DateTime m_battleMapClickTime;
    private int m_showDangerRegionTeam;
    private List<int> m_showDangerRegionActorIds;
    private BattleActor m_preCombatTargetActor;
    private bool m_saveShowTopUI;
    private bool m_saveShowBottomUI;
    private bool m_disableSaveProcessingBattle;
    private BattleTeamSetup m_battleTeamSetup0;
    private BattleTeamSetup m_battleTeamSetup1;
    private List<GridPosition> m_teamPositions0;
    private List<GridPosition> m_teamPositions1;
    private List<GridPosition> m_teamNpcPositions0;
    private List<BattleHero> m_playerBattleHeros;
    private List<int> m_tempIntList;
    private List<string> m_tempStringList;
    private HashSet<GridPosition> m_dangerRegion;
    private List<TrainingTech> m_trainingTechs;
    private List<TrainingTech> m_tempTrainingTechs;
    private List<Goods> m_tempGoodsList;
    private List<int> m_userGuideEnforceHeroIds;
    private List<int> m_arenaAttackerHeroIds;
    private List<int> m_myBattleHeroIds;
    private List<int> m_enemyBattleHeroIds;
    private List<int> m_myHireBattleHeroIds;
    private HeroPropertyComputer m_heroPropertyComputer;
    private HeroPropertyComputer m_soldierPropertyComputer;
    private BattleLoadState m_loadState;
    private BattlePerformState m_battlePerformState;
    private int m_nowSeconds;
    private DateTime m_myActionTimeout;
    private DateTime m_otherActionTimeout;
    private bool m_isMyActionTimeoutActive;
    private bool m_isActionTimeoutAutoBattle;
    private bool m_isAutoBattleOnce;
    private DateTime m_battleStartTime;
    private List<int> m_pendingHeroSetupNtfs;
    private GridPosition m_selectProtectHeroPos;
    private GridPosition m_selectBanHeroPos;
    private List<object> m_collectAssetObjects;
    private List<string> m_collectedCombatAssets;
    private RandomNumber m_armyRandomNumber;
    private ProBattleReport m_battleReport;
    private int m_battleStopTurn;
    private bool m_isStartInBattleRoom;
    private bool m_isStartInBattleLiveRoom;
    private bool m_isStartBattleAutoBattle;
    private bool m_isLiveRoomWaitFinishNtf;
    private bool m_isExitingBattleReturnToWorld;
    private bool m_isShowingMessageExitBattle;
    private bool m_isTestPrepareRestoreHeros;
    private int m_fastReconnectBattleRoomCount;
    private DateTime m_lastReconectBattleRoomTime;
    private DateTime m_lastDanmakuSendTime;
    private List<ConfigDataBattleDialogInfo> m_dialogBeginInfoList;
    private List<int> m_bpStateSelectedActorIds;
    private int m_uiBPStageCurrentTurn;
    private BPStage m_battleReportPeakArenaBPStage;
    private BPStagePeakArenaRule m_battleReportPeakArenaBPStageRule;
    private DateTime m_nextBpStageBattleReportAutoPlayTime;
    private int m_peakArenaBattleReportBoRound;
    private List<RegretStep> m_regretSteps;
    private List<BattleCommand> m_regretBattleCommands;
    private List<BattleCommand> m_battleReportRegretBattleCommands;
    private int m_regretFinalStep;
    private int m_regretCurrentStep;
    private int m_regretCancelStep;
    private int m_regretFinalTurn;
    private int m_regretCurrentTurn;
    private int m_regretCameraFocusActorId;
    private bool m_isBattleLoseRegret;
    private bool m_isTryActivateRegret;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
    private static DelegateBridge __Hotfix_IsNeedLoadActionOrderUILayer;
    private static DelegateBridge __Hotfix_IsNeedLoadBattleRoomUILayer;
    private static DelegateBridge __Hotfix_IsNeedLoadPVPBattlePrepareUILayer;
    private static DelegateBridge __Hotfix_IsNeedLoadPeakArenaBattlePrepareUILayer;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_LogCurrentBattleId;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_HideAllView;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_IsLoading;
    private static DelegateBridge __Hotfix_CreateClientBattle;
    private static DelegateBridge __Hotfix_DestroyClientBattle;
    private static DelegateBridge __Hotfix_PrepareClientBattle;
    private static DelegateBridge __Hotfix_StartClientBattle;
    private static DelegateBridge __Hotfix_RebuildClientBattle;
    private static DelegateBridge __Hotfix_StopBattle;
    private static DelegateBridge __Hotfix_SaveAutoBattle;
    private static DelegateBridge __Hotfix_SendBattleRoomInitLog;
    private static DelegateBridge __Hotfix_GetBattleTeamSetup;
    private static DelegateBridge __Hotfix_GetTeamPositions;
    private static DelegateBridge __Hotfix_SetCombatHp;
    private static DelegateBridge __Hotfix_ExitBattleReturnToWorld;
    private static DelegateBridge __Hotfix_UnloadAssetsAndStartWorldUITask;
    private static DelegateBridge __Hotfix_Co_UnloadAssetsAndStartWorldUITask;
    private static DelegateBridge __Hotfix_WorldUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_ShowErrorMessageAndExitBattle_1;
    private static DelegateBridge __Hotfix_ShowErrorMessageAndExitBattle_0;
    private static DelegateBridge __Hotfix_SaveProcessingBattle;
    private static DelegateBridge __Hotfix_BattlePrepareBeforeShowResult;
    private static DelegateBridge __Hotfix_IsMeArenaBattleTeam1;
    private static DelegateBridge __Hotfix_CanUseChat;
    private static DelegateBridge __Hotfix_CanUseDanmaku;
    private static DelegateBridge __Hotfix_CanChangeActionOrder;
    private static DelegateBridge __Hotfix_IsCurrentBattleType;
    private static DelegateBridge __Hotfix_get_DialogBeginInfoList;
    private static DelegateBridge __Hotfix_ClearDialogBeginIDList;
    private static DelegateBridge __Hotfix_LowMemoryGC;
    private static DelegateBridge __Hotfix_TeamChatMessage;
    private static DelegateBridge __Hotfix_get_ClientBattle;
    private static DelegateBridge __Hotfix_TestUI_ExitBattle;
    private static DelegateBridge __Hotfix_TestUI_RestartBattle;
    private static DelegateBridge __Hotfix_TestUI_ReplayBattle;
    private static DelegateBridge __Hotfix_TestUI_PrepareBattle;
    private static DelegateBridge __Hotfix_TestUI_StopBattle;
    private static DelegateBridge __Hotfix_SendBattleCheatGMCommand;
    private static DelegateBridge __Hotfix_SendBattleCheatBuffGMCommand;
    private static DelegateBridge __Hotfix_PlayerContext_OnChatMessageNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnTeamRoomInviteNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattlePracticeInvitedNtf;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_CollectBattlePrepareAssets;
    private static DelegateBridge __Hotfix_CollectBattleAssets;
    private static DelegateBridge __Hotfix_CollectBattlePrepareHeros;
    private static DelegateBridge __Hotfix_CollectBattlefieldAssets;
    private static DelegateBridge __Hotfix_CollectTerrainAssets;
    private static DelegateBridge __Hotfix_CollectTerrainEffectAssets;
    private static DelegateBridge __Hotfix_CollectCombatTerrainAsset;
    private static DelegateBridge __Hotfix_CollectCharImageAssets;
    private static DelegateBridge __Hotfix_CollectCharSpineAssets;
    private static DelegateBridge __Hotfix_CollectHeadImageAssets;
    private static DelegateBridge __Hotfix_CollectHeroAssets;
    private static DelegateBridge __Hotfix_CollectSoldierAssets;
    private static DelegateBridge __Hotfix_CollectSkinAssets;
    private static DelegateBridge __Hotfix_CollectSkillAssets;
    private static DelegateBridge __Hotfix_CollectBuffAssets;
    private static DelegateBridge __Hotfix_CollectDefaultHeroAssets;
    private static DelegateBridge __Hotfix_CollectBattleTreasureAssets;
    private static DelegateBridge __Hotfix_CollectBattleEventAssets;
    private static DelegateBridge __Hotfix_CollectBattleEventActionAssets;
    private static DelegateBridge __Hotfix_CollectBattlePerformAssets;
    private static DelegateBridge __Hotfix__CollectBattlePerformAssets;
    private static DelegateBridge __Hotfix_CollectTrainingTechAssets;
    private static DelegateBridge __Hotfix_IsAlreadyCollectAsset;
    private static DelegateBridge __Hotfix_ClearAlreadyCollectAssets;
    private static DelegateBridge __Hotfix_StartBattleUI;
    private static DelegateBridge __Hotfix_ClientActorTryMoveTo;
    private static DelegateBridge __Hotfix_AddCommandIfMoved;
    private static DelegateBridge __Hotfix_FindAttackPosition;
    private static DelegateBridge __Hotfix_CanAction;
    private static DelegateBridge __Hotfix_GetMapBattleActor;
    private static DelegateBridge __Hotfix_ResetActorEffect;
    private static DelegateBridge __Hotfix_CanUseSkillAtPosition;
    private static DelegateBridge __Hotfix_ShowMoveAndAttackRegion_1;
    private static DelegateBridge __Hotfix_ShowMoveAndAttackRegion_0;
    private static DelegateBridge __Hotfix_ShowMoveRegion_1;
    private static DelegateBridge __Hotfix_ShowMoveRegion_0;
    private static DelegateBridge __Hotfix_UpdateDangerRegion;
    private static DelegateBridge __Hotfix_ShowDangerTeamRegion;
    private static DelegateBridge __Hotfix_ShowDangerRegion;
    private static DelegateBridge __Hotfix_ShowTryMovePath;
    private static DelegateBridge __Hotfix_GetSummonMoveType;
    private static DelegateBridge __Hotfix_ShowSkillTargetRegion_1;
    private static DelegateBridge __Hotfix_AddValidPositionToList;
    private static DelegateBridge __Hotfix_ShowSkillTargetRegion_0;
    private static DelegateBridge __Hotfix_ShowSkillAttackRegion;
    private static DelegateBridge __Hotfix_ShowSkillAttackRange;
    private static DelegateBridge __Hotfix_ShowTeleportRegion_1;
    private static DelegateBridge __Hotfix_ShowTeleportRegion_0;
    private static DelegateBridge __Hotfix_ShowAttackTargets;
    private static DelegateBridge __Hotfix_ShowSkillTargets_1;
    private static DelegateBridge __Hotfix_ShowSkillTargets_0;
    private static DelegateBridge __Hotfix_ShowBattleTreasureDialog;
    private static DelegateBridge __Hotfix_ShowBattleTreasureReward;
    private static DelegateBridge __Hotfix_ShowPreCombat;
    private static DelegateBridge __Hotfix_ShowFastCombat;
    private static DelegateBridge __Hotfix_ShowActorInfo;
    private static DelegateBridge __Hotfix_HideActorInfo;
    private static DelegateBridge __Hotfix_ShowSelectionMarkAndTerrain;
    private static DelegateBridge __Hotfix_SetUIStateNone;
    private static DelegateBridge __Hotfix_SetUIStateSelectActionActor;
    private static DelegateBridge __Hotfix_SetUIStateMove;
    private static DelegateBridge __Hotfix_ShowSkills;
    private static DelegateBridge __Hotfix_SetUIStateExtraMove;
    private static DelegateBridge __Hotfix_SetUIStateSelectSkillTarget;
    private static DelegateBridge __Hotfix_SetUIStateConfirmSkill;
    private static DelegateBridge __Hotfix_SetUIStateSelectTeleportPosition1;
    private static DelegateBridge __Hotfix_SetUIStateSelectTeleportPosition2;
    private static DelegateBridge __Hotfix_SetUIStateWaitOtherPlayer;
    private static DelegateBridge __Hotfix_SetUIStateBattleLiveRoom;
    private static DelegateBridge __Hotfix_ShowCanActionActorsUI;
    private static DelegateBridge __Hotfix_HideCanActionActorsUI;
    private static DelegateBridge __Hotfix_CancelActiveActor;
    private static DelegateBridge __Hotfix_DoAutoBattle;
    private static DelegateBridge __Hotfix_SetAutoBattle;
    private static DelegateBridge __Hotfix_CameraFocusActor;
    private static DelegateBridge __Hotfix_UpdateAncientCallUIs;
    private static DelegateBridge __Hotfix_UpdateCurrentBossActor;
    private static DelegateBridge __Hotfix_UpdateBossSimpleInfo;
    private static DelegateBridge __Hotfix_UpdateMonsterTotalDamage;
    private static DelegateBridge __Hotfix_UpdateBossPhase;
    private static DelegateBridge __Hotfix_UpdateBossActionCount;
    private static DelegateBridge __Hotfix_BattleUIController_OnAutoBattle;
    private static DelegateBridge __Hotfix_BattleUIController_OnFastBattle;
    private static DelegateBridge __Hotfix_BattleUIController_OnSkipCombat;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowDanger;
    private static DelegateBridge __Hotfix_BattleUIController_OnEndAllAction;
    private static DelegateBridge __Hotfix_EndAllActionDialogBoxCallback;
    private static DelegateBridge __Hotfix_BattleUIController_OnEndAction;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowActorInfo;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowBossInfo;
    private static DelegateBridge __Hotfix_BattleUIController_OnSelectSkill;
    private static DelegateBridge __Hotfix_BattleUIController_OnUseSkill;
    private static DelegateBridge __Hotfix_BattleUIController_OnCancelSkill;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowChat;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_BattleUIController_OnPauseBattle;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowArmyRelation;
    private static DelegateBridge __Hotfix_BattleUIController_OnPointerDown;
    private static DelegateBridge __Hotfix_BattleUIController_OnPointerUp;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowCurTurnDanmaku;
    private static DelegateBridge __Hotfix_BattleUIController_OnCloseCurTurnDanmaku;
    private static DelegateBridge __Hotfix_BattleUIController_OnShowOneDanmaku;
    private static DelegateBridge __Hotfix_ShowOneDanmaku;
    private static DelegateBridge __Hotfix_SetDanmakuUIActive;
    private static DelegateBridge __Hotfix_ClearDanmakuUIInputField;
    private static DelegateBridge __Hotfix_BattleUIController_OnPointerClick;
    private static DelegateBridge __Hotfix_OnBattleMapClick;
    private static DelegateBridge __Hotfix_ShouldShowActorInfo;
    private static DelegateBridge __Hotfix_BattleUIController_On3DTouch;
    private static DelegateBridge __Hotfix_PlaySelectHeroSound;
    private static DelegateBridge __Hotfix_BattleDialogUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleTreasureDialogUIController_OnClose;
    private static DelegateBridge __Hotfix_GetRewardGoodsUITask_OnClose;
    private static DelegateBridge __Hotfix_CombatUIController_OnAutoBattle;
    private static DelegateBridge __Hotfix_PreCombatUIController_OnOk;
    private static DelegateBridge __Hotfix_PreCombatUIController_OnCancel;
    private static DelegateBridge __Hotfix_PreCombatUIController_OnStop;
    private static DelegateBridge __Hotfix_BattleSceneUIController_OnPointerDown;
    private static DelegateBridge __Hotfix_BattleSceneUIController_OnPointerUp;
    private static DelegateBridge __Hotfix_BattleSceneUIController_OnPointerClick;
    private static DelegateBridge __Hotfix_BattleSceneUIController_OnBeginDrag;
    private static DelegateBridge __Hotfix_BattleSceneUIController_OnEndDrag;
    private static DelegateBridge __Hotfix_BattleSceneUIController_OnDrag;
    private static DelegateBridge __Hotfix_BattleSceneUIController_On3DTouch;
    private static DelegateBridge __Hotfix_SendBattleCommands;
    private static DelegateBridge __Hotfix_UpdateBattleRoomPlayerHeroAlive;
    private static DelegateBridge __Hotfix_UpdateBattleLiveRoomPlayerAndHeros;
    private static DelegateBridge __Hotfix_UpdateBattleLiveRoomPlayerHeroAlive;
    private static DelegateBridge __Hotfix_StartBattleRoomMyActionCountdown;
    private static DelegateBridge __Hotfix_StopBattleRoomMyActionCountdown;
    private static DelegateBridge __Hotfix_ActivateBattleRoomMyActionCountdown;
    private static DelegateBridge __Hotfix_UpdateBattleRoomMyActionCountdown;
    private static DelegateBridge __Hotfix_BattleRoomMyActionTimeout;
    private static DelegateBridge __Hotfix_StartBattleRoomOtherActionCountdown;
    private static DelegateBridge __Hotfix_StopBattleRoomOtherActionCountdown;
    private static DelegateBridge __Hotfix_ActivateBattleRoomOtherActionCountdown;
    private static DelegateBridge __Hotfix_UpdateBattleRoomOtherActionCountdown;
    private static DelegateBridge __Hotfix_SetBattleLiveActionCount;
    private static DelegateBridge __Hotfix_BattleRoomSetAutoBattle;
    private static DelegateBridge __Hotfix_ProcessBattlePendingNtfs;
    private static DelegateBridge __Hotfix_ProcessingPendingPlayerQuitNtfs;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPlayerStatusChangedNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomQuitNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomBattleCommandExecuteNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomTeamBattleFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomGuildMassiveCombatBattleFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPVPBattleFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomRealTimePVPBattleFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPeakArenaBattleFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPeakArenaMatchFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPeakArenaPlayerForfeitNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleLiveRoomFinishNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPeakArenaDanmakuNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_CheckReconnectBattleRoom;
    private static DelegateBridge __Hotfix_ReconnectBattleRoom;
    private static DelegateBridge __Hotfix_ReconnectBattleLiveRoom;
    private static DelegateBridge __Hotfix_AfterReconnectRebuildBattle;
    private static DelegateBridge __Hotfix_GetBattleRoom;
    private static DelegateBridge __Hotfix_UpdateBattleRoomPlayerHeroCount;
    private static DelegateBridge __Hotfix_UpdateBattleRoomPrepareCountdown;
    private static DelegateBridge __Hotfix_UpdateRealtimePVPBattlePrepareCountdown;
    private static DelegateBridge __Hotfix_UpdateRealtimePVPBattlePrepareStatus;
    private static DelegateBridge __Hotfix_UpdateStageActorTag;
    private static DelegateBridge __Hotfix_ProcessBattlePreparePendingNtfs;
    private static DelegateBridge __Hotfix_LoadAndUpdateBattleRoomStageActors;
    private static DelegateBridge __Hotfix_UpdateBattleRoomStageActors;
    private static DelegateBridge __Hotfix_BattleRoomBattleStart;
    private static DelegateBridge __Hotfix_PVPBattlePrepareUIController_OnPrepareConfirm;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomHeroSetupNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomDataChangeNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomTeamBattleStartNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPVPBattleStartNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomRealTimePVPBattleStartNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPeakArenaBattleStartNtf;
    private static DelegateBridge __Hotfix_ShowBattleResult;
    private static DelegateBridge __Hotfix_ShowBattleLose;
    private static DelegateBridge __Hotfix_ShowBattleResultScore;
    private static DelegateBridge __Hotfix_ShowBattleResultBoss;
    private static DelegateBridge __Hotfix_ShowBattleReportEnd_0;
    private static DelegateBridge __Hotfix_ShowBattleReportEnd_1;
    private static DelegateBridge __Hotfix_ShowBattleReportEnd_3;
    private static DelegateBridge __Hotfix_ShowBattleReportEnd_2;
    private static DelegateBridge __Hotfix_BuildBattleReport;
    private static DelegateBridge __Hotfix_CheckBattleResult;
    private static DelegateBridge __Hotfix_BattleResultEnd;
    private static DelegateBridge __Hotfix_BattleRoomInviteTeammateDialogBoxCallback;
    private static DelegateBridge __Hotfix_SetPeakArenaBattleReportBoRound;
    private static DelegateBridge __Hotfix_BattleUIController_OnWinOrLoseEnd;
    private static DelegateBridge __Hotfix_BattleResultUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleResultScoreUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleResultBossUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleLoseUITask_OnClose;
    private static DelegateBridge __Hotfix_AddBattleReportEndUITaskEvents;
    private static DelegateBridge __Hotfix_RemoveBattleReportEndUITaskEvents;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnClose;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnPlayAgain;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnNextRound;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnPeakArenaConfirm;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnPeakArenaContinue;
    private static DelegateBridge __Hotfix_ShowMatchingNowUITask;
    private static DelegateBridge __Hotfix_BattleReportEndUITask_OnPeakArenaLiveExit;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_InitBattleSceneUIController;
    private static DelegateBridge __Hotfix_UninitBattleSceneUIController;
    private static DelegateBridge __Hotfix_InitBattleUIController;
    private static DelegateBridge __Hotfix_UninitBattleUIController;
    private static DelegateBridge __Hotfix_InitCombatUIController;
    private static DelegateBridge __Hotfix_UninitCombatUIController;
    private static DelegateBridge __Hotfix_InitPreCombatUIController;
    private static DelegateBridge __Hotfix_UninitPreCombatUIController;
    private static DelegateBridge __Hotfix_InitBattleRoomUIController;
    private static DelegateBridge __Hotfix_UninitBattleRoomUIController;
    private static DelegateBridge __Hotfix_InitBattleCommonUIController;
    private static DelegateBridge __Hotfix_UninitBattleCommonUIController;
    private static DelegateBridge __Hotfix_InitBattlePrepareUIController;
    private static DelegateBridge __Hotfix_UninitBattlePrepareUIController;
    private static DelegateBridge __Hotfix_OnStartBattle;
    private static DelegateBridge __Hotfix_OnStopBattle;
    private static DelegateBridge __Hotfix_BattleLoseUseRegretDialogBoxCallback;
    private static DelegateBridge __Hotfix_OnShowBattleWinCondition;
    private static DelegateBridge __Hotfix_OnHideBattleWinCondition;
    private static DelegateBridge __Hotfix_OnShowBattleLoseCondition;
    private static DelegateBridge __Hotfix_OnHideBattleLoseCondition;
    private static DelegateBridge __Hotfix_OnNextTurn;
    private static DelegateBridge __Hotfix_OnNextTurnAnimationEnd;
    private static DelegateBridge __Hotfix_OnNextTeam;
    private static DelegateBridge __Hotfix_OnNextPlayer;
    private static DelegateBridge __Hotfix_OnNextActor;
    private static DelegateBridge __Hotfix_OnClientActorActive;
    private static DelegateBridge __Hotfix_OnClientActorActionBegin;
    private static DelegateBridge __Hotfix_OnClientActorActionEnd;
    private static DelegateBridge __Hotfix_OnClientActorMove;
    private static DelegateBridge __Hotfix_OnClientActorTryMove;
    private static DelegateBridge __Hotfix_OnClientActorNoAct;
    private static DelegateBridge __Hotfix_OnClientActorTarget;
    private static DelegateBridge __Hotfix_OnClientActorSkill;
    private static DelegateBridge __Hotfix_OnClientActorSkillEnd;
    private static DelegateBridge __Hotfix_OnClientActorSkillHit;
    private static DelegateBridge __Hotfix_OnClientActorBuffHit;
    private static DelegateBridge __Hotfix_OnClientActorTerrainHit;
    private static DelegateBridge __Hotfix_OnClientActorImmune;
    private static DelegateBridge __Hotfix_OnClientActorPassiveSkill;
    private static DelegateBridge __Hotfix_OnClientActorGuard;
    private static DelegateBridge __Hotfix_OnClientActorDie;
    private static DelegateBridge __Hotfix_OnClientActorAppear;
    private static DelegateBridge __Hotfix_OnClientActorDisappear;
    private static DelegateBridge __Hotfix_OnClientActorReplace;
    private static DelegateBridge __Hotfix_OnClientActorHealthPointChange;
    private static DelegateBridge __Hotfix_OnCancelCombat;
    private static DelegateBridge __Hotfix_OnPreStartCombat;
    private static DelegateBridge __Hotfix_OnLoadCombatAssets;
    private static DelegateBridge __Hotfix_OnStartCombat;
    private static DelegateBridge __Hotfix_OnPreStopCombat;
    private static DelegateBridge __Hotfix_OnStopCombat;
    private static DelegateBridge __Hotfix_OnPrepareFastCombat;
    private static DelegateBridge __Hotfix_OnStartFastCombat;
    private static DelegateBridge __Hotfix_OnCombatActorHit;
    private static DelegateBridge __Hotfix_OnStartSkillCutscene;
    private static DelegateBridge __Hotfix_OnStartPassiveSkillCutscene;
    private static DelegateBridge __Hotfix_OnStopSkillCutscene;
    private static DelegateBridge __Hotfix_OnStartBattleDialog;
    private static DelegateBridge __Hotfix_BattleDialogUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_OnStartBattleTreasureDialog;
    private static DelegateBridge __Hotfix_OnShowBattleTreasureReward;
    private static DelegateBridge __Hotfix_OnStartBattlePerform;
    private static DelegateBridge __Hotfix_OnStopBattlePerform;
    private static DelegateBridge __Hotfix_OnScreenEffect;
    private static DelegateBridge __Hotfix_OnBossPhase;
    private static DelegateBridge __Hotfix_add_m_onActiveActorEvent;
    private static DelegateBridge __Hotfix_remove_m_onActiveActorEvent;
    private static DelegateBridge __Hotfix_add_m_onDeactiveActorEvent;
    private static DelegateBridge __Hotfix_remove_m_onDeactiveActorEvent;
    private static DelegateBridge __Hotfix_StartBattleTeamSetNetTask;
    private static DelegateBridge __Hotfix_StartLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartLevelWayPointMoveNetTask;
    private static DelegateBridge __Hotfix_StartLevelScenarioHandleNetTask;
    private static DelegateBridge __Hotfix_StartRiftLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartHeroDungeonLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartAnikiLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartThearchyLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartMemoryCorridorLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartHeroTrainningLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartHeroPhantomLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartTreasureLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartUnchartedScoreLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartUnchartedChallengeLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartClimbTowerLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartEternalShrineLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartHeroAnthemLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartAncientCallBossAttackNetTask;
    private static DelegateBridge __Hotfix_StartCollectionLevelAttackNetTask;
    private static DelegateBridge __Hotfix_StartCollectionEventAttackNetTask;
    private static DelegateBridge __Hotfix_StartGuildMassiveCombatAttackNetTask;
    private static DelegateBridge __Hotfix_StartArenaOpponentAttackFightingNetTask;
    private static DelegateBridge __Hotfix_HandleLevelAttackNetTaskResult;
    private static DelegateBridge __Hotfix_StartLevelFinishedNetTask;
    private static DelegateBridge __Hotfix_StartWayPointBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartRiftLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartHeroDungeonLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartAnikiLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartThearchyLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartTreasureLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartMemoryCorridorLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartHeroTrainningLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartHeroPhantomLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartUnchartedScoreLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartUnchartedChallengeLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartClimbTowerLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartEternalShrineLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartHeroAnthemLevelBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartAncientCallBossBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartCollectionLevelFinishNetTask;
    private static DelegateBridge __Hotfix_StartCollectionEventBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_StartGuildMassiveCombatAttackFinishedNetTask;
    private static DelegateBridge __Hotfix_StartArenaBattleFinishedNetTask;
    private static DelegateBridge __Hotfix_HandleBattleFinishedNetTaskResult;
    private static DelegateBridge __Hotfix_StartBattleCancelNetTask;
    private static DelegateBridge __Hotfix_StartDanmakuPostNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomHeroSetupNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomHeroSwapNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomHeroSetoffNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomPlayerStatusChangeNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomPlayerStatusChangeNetTaskAndAutoBattle;
    private static DelegateBridge __Hotfix_StartBattleRoomQuitNetTask;
    private static DelegateBridge __Hotfix_StartPeakArenaBattleLiveRoomQuitNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomEndCurrentBPTurnNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomPlayerActionBeginNetTask;
    private static DelegateBridge __Hotfix_StarBattleRoomBPStageCommandExecuteNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomBattleCommandExecuteNetTask;
    private static DelegateBridge __Hotfix_StartPeakArenaDanmakuNetTask;
    private static DelegateBridge __Hotfix_GetWinConditionTargetPosition;
    private static DelegateBridge __Hotfix_SetupReachRegion;
    private static DelegateBridge __Hotfix_SetupBattlePauseUIController;
    private static DelegateBridge __Hotfix_SetupBattlePauseUIAchievements;
    private static DelegateBridge __Hotfix_BattlePauseUIController_OnClose;
    private static DelegateBridge __Hotfix_BattlePauseUIController_OnShowPlayerSetting;
    private static DelegateBridge __Hotfix_BattlePauseUIController_OnExit;
    private static DelegateBridge __Hotfix_ExitBattleDialogBoxCallback;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStage;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStagePlayerAndHeros;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStageHeros;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStageTips;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStageCountdown;
    private static DelegateBridge __Hotfix_PeakArenaBpStageAutoBanPick;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStageBattleReport;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStageBattleReportPlayerAndHeros;
    private static DelegateBridge __Hotfix_UpdatePeakArenaBPStageBattleReportTips;
    private static DelegateBridge __Hotfix_InitPeakArenaBPStageBattleReport;
    private static DelegateBridge __Hotfix_PeakArenaBPStageBattleReportGotoTurn;
    private static DelegateBridge __Hotfix_PeakArenaBPStageBattleReportNextTurn;
    private static DelegateBridge __Hotfix_PeakArenaBPStageBattleReportStartBattle;
    private static DelegateBridge __Hotfix_TickPeakArenaBpStageBattleReport;
    private static DelegateBridge __Hotfix_StopPeakArenaBPStageBattleReportAutoPlay;
    private static DelegateBridge __Hotfix_IsPeakArenaBpStageBattleReportPaused;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomBPStageCommandExecuteNtf;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnPauseBattle;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBpStageHeroClick;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBpStageConfirm;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBpStageAuto;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnAttack;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBattleReportSkip;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBattleReportPause;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBattleReportPlay;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBattleReportBackward;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnBattleReportForward;
    private static DelegateBridge __Hotfix_PeakArenaBattlePrepareUIController_OnSendDanmaku;
    private static DelegateBridge __Hotfix_PlayerContext_OnBattleRoomPlayerBPStageInfoInitNtf;
    private static DelegateBridge __Hotfix_PrepareBattleUI;
    private static DelegateBridge __Hotfix_GetMyStageActorCountMax;
    private static DelegateBridge __Hotfix_SetupMyHeros;
    private static DelegateBridge __Hotfix_FillMyHeros;
    private static DelegateBridge __Hotfix_SetupStageActors;
    private static DelegateBridge __Hotfix_AddUseableHeros;
    private static DelegateBridge __Hotfix_GetUnchartedScoreInfo;
    private static DelegateBridge __Hotfix_GetHeroUnchartedScoreBonus;
    private static DelegateBridge __Hotfix_GetTowerBattleRuleInfo;
    private static DelegateBridge __Hotfix_GetTowerBonusHeroGroupInfo;
    private static DelegateBridge __Hotfix_IsTowerPowerUpHero;
    private static DelegateBridge __Hotfix_GetGuildPlayerMassiveCombatInfo;
    private static DelegateBridge __Hotfix_GetGuildMassiveCombatPreferredHeroTagIds;
    private static DelegateBridge __Hotfix_IsGuildMassiveCombatCampUpHero;
    private static DelegateBridge __Hotfix_IsEternalShrineCampUpHero;
    private static DelegateBridge __Hotfix_IsHeroAnthemPowerUpHero;
    private static DelegateBridge __Hotfix_IsAncientCallPowerUpHero;
    private static DelegateBridge __Hotfix_IsCollectionActivityDropUpHero;
    private static DelegateBridge __Hotfix_GetCollectionActivityInfo;
    private static DelegateBridge __Hotfix_GetHeroCollectionActivityScoreBonus;
    private static DelegateBridge __Hotfix_GetHeroTagType;
    private static DelegateBridge __Hotfix_LoadArenaAttackerHeroActionValue;
    private static DelegateBridge __Hotfix_CompareHero;
    private static DelegateBridge __Hotfix_CompareHeroUnchartdScoreBonus;
    private static DelegateBridge __Hotfix_CompareHeroCollectionActivityScoreBonus;
    private static DelegateBridge __Hotfix_ShowStagePositions;
    private static DelegateBridge __Hotfix_GetStagePositionCenter;
    private static DelegateBridge __Hotfix_SetupBattlePrepareTreasures;
    private static DelegateBridge __Hotfix_IsListElementsEqual;
    private static DelegateBridge __Hotfix_BuildBattleTeamSetups;
    private static DelegateBridge __Hotfix_ModifyBattleTeamSetups;
    private static DelegateBridge __Hotfix_BuildBattleTeamSetup;
    private static DelegateBridge __Hotfix_GetSoldierCount;
    private static DelegateBridge __Hotfix_SetTeamAndStartBattle;
    private static DelegateBridge __Hotfix_StartBattlePrepareLoadState;
    private static DelegateBridge __Hotfix_StartBattleLoadState;
    private static DelegateBridge __Hotfix_ShowMoveAndAttackRegion_Prepare;
    private static DelegateBridge __Hotfix_UpdateArenaAttackerHeroIds;
    private static DelegateBridge __Hotfix_CompareHeroActionValue;
    private static DelegateBridge __Hotfix_GetPlayerTrainingTechs;
    private static DelegateBridge __Hotfix_GetPlayerLevel;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleReportPlayer;
    private static DelegateBridge __Hotfix_GetPlayerSessionId;
    private static DelegateBridge __Hotfix_GetMyPlayerIndex;
    private static DelegateBridge __Hotfix_GetPeakArenaBORound;
    private static DelegateBridge __Hotfix_BattlePrepareCanChangeSkill;
    private static DelegateBridge __Hotfix_IsBattlePrepareDisableCameraMove;
    private static DelegateBridge __Hotfix_GetBattlePrepareChangeCameraSize;
    private static DelegateBridge __Hotfix_UpdateBattlePower;
    private static DelegateBridge __Hotfix_IsShowRecommendHeroButton;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnPauseBattle;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnShowArmyRelation;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnStart;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnShowRecommendHero;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnShowActionOrder;
    private static DelegateBridge __Hotfix_HeroNotFullDialogBoxCallback;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnTestOnStage;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnShowMyActorInfo;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnHideActorInfo;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnStageActorChange;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnHeroOnStage;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnStageActorOffStage;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnStageActorMove;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnStageActorSwap;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnUpdateBattlePower;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnShowChat;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnPointerDown;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnPointerUp;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnPointerClick;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnBeginDragHero;
    private static DelegateBridge __Hotfix_BattlePrepareUIController_OnEndDragHero;
    private static DelegateBridge __Hotfix_GetHeroFromBattleHero;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnShowSelectSkillPanel;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnChangeSkill;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnChangeSoldier;
    private static DelegateBridge __Hotfix_ActionOrderUIController_OnConfirm;
    private static DelegateBridge __Hotfix_SetUIStateRegret;
    private static DelegateBridge __Hotfix_UpdateRegretButton;
    private static DelegateBridge __Hotfix_ClearRegret;
    private static DelegateBridge __Hotfix_IsRegretActive;
    private static DelegateBridge __Hotfix_CanUseRegret;
    private static DelegateBridge __Hotfix_RegretGotoStep;
    private static DelegateBridge __Hotfix_FindRegretStepIndexByStep;
    private static DelegateBridge __Hotfix_FindRegretStepIndexByTurn;
    private static DelegateBridge __Hotfix_ActivateRegret;
    private static DelegateBridge __Hotfix_BuildBattleReportRegretData;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretActive;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretConfirm;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretCancel;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretBackward;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretForward;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretPrevTurn;
    private static DelegateBridge __Hotfix_BattleUIController_OnRegretNextTurn;
    private static DelegateBridge __Hotfix_CanUseBattleReportRegret;
    private static DelegateBridge __Hotfix_UpdateBattleReportPlayers;
    private static DelegateBridge __Hotfix_BattleUIController_OnBattleReportPause;
    private static DelegateBridge __Hotfix_BattleUIController_OnBattleReportPlay;
    private static DelegateBridge __Hotfix_BattleUIController_OnBattleReportBackward;
    private static DelegateBridge __Hotfix_BattleUIController_OnBattleReportForward;
    private static DelegateBridge __Hotfix_BattleUIController_OnBattleReportPrevTurn;
    private static DelegateBridge __Hotfix_BattleUIController_OnBattleReportNextTurn;
    private static DelegateBridge __Hotfix_UserGuide_GetEnforceHeros;
    private static DelegateBridge __Hotfix_SetUserGuideBattleSettings;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~BattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedLoadActionOrderUILayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedLoadBattleRoomUILayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedLoadPVPBattlePrepareUILayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedLoadPeakArenaBattlePrepareUILayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LogCurrentBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void HideAllView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLoading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroyClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PrepareClientBattle(bool prepareBattleUI = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RebuildClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBattle(bool win, bool abnormal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveAutoBattle(bool win, bool isAutoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleRoomInitLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleTeamSetup GetBattleTeamSetup(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GridPosition> GetTeamPositions(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCombatHp(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExitBattleReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void UnloadAssetsAndStartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void WorldUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowErrorMessageAndExitBattle(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowErrorMessageAndExitBattle(StringTableId strId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareBeforeShowResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsMeArenaBattleTeam1()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanChangeActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsCurrentBattleType(BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<ConfigDataBattleDialogInfo> DialogBeginInfoList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearDialogBeginIDList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LowMemoryGC()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamChatMessage(ChatMessage msg, ProjectLPlayerContext playerContext)
    {
      // ISSUE: unable to decompile the method.
    }

    public ClientBattle ClientBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_ExitBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_RestartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_ReplayBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_PrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestUI_StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleCheatGMCommand(bool isCheat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleCheatBuffGMCommand(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnChatMessageNtf(ChatMessage msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnTeamRoomInviteNtf(TeamRoomInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattlePracticeInvitedNtf(PVPInviteInfo inviteInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlePrepareAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlePrepareHeros(List<BattleHero> heros0, List<BattleHero> heros1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectTerrainAssets(ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectTerrainEffectAssets(ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectCombatTerrainAsset(ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectCharImageAssets(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectCharSpineAssets(
      ConfigDataCharImageInfo charImageInfo,
      ConfigDataCharImageSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAssets(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo heroSkinResInfo,
      int heroStar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSoldierAssets(
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataModelSkinResourceInfo soldierSkinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSkinAssets(ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectSkillAssets(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffAssets(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectDefaultHeroAssets(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleTreasureAssets(List<int> treasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleEventAssets(List<int> triggerIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattleEventActionAssets(ConfigDataBattleEventActionInfo eventActionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlePerformAssets(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _CollectBattlePerformAssets(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectTrainingTechAssets(List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAlreadyCollectAsset(object obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAlreadyCollectAssets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClientActorTryMoveTo(ClientBattleActor ca, GridPosition p, int finalDir = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCommandIfMoved(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindAttackPosition(int attackDistance, GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor GetMapBattleActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetActorEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseSkillAtPosition(
      ClientBattleActor ca,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveAndAttackRegion(BattleActor actor, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveAndAttackRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveRegion(BattleActor actor, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDangerRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDangerTeamRegion(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDangerRegion(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTryMovePath(ClientBattleActor ca, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MoveType GetSummonMoveType(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargetRegion(ClientBattleActor ca, int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddValidPositionToList(int x, int y, List<GridPosition> positionList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargetRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillAttackRegion(
      ClientBattleActor ca,
      int skillIndex,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillAttackRange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTeleportRegion(ClientBattleActor ca, int skillIndex, GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowTeleportRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAttackTargets(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargets(ClientBattleActor ca, int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPreCombat(
      BattleActor a0,
      BattleActor a1,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowActorInfo(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSelectionMarkAndTerrain(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateNone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectActionActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateExtraMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectSkillTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateConfirmSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectTeleportPosition1()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateSelectTeleportPosition2()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateWaitOtherPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateBattleLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCanActionActorsUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideCanActionActorsUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CancelActiveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoAutoBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CameraFocusActor(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateAncientCallUIs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrentBossActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBossSimpleInfo(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMonsterTotalDamage(int damage, bool animNumber = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBossPhase(int phase = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBossActionCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnFastBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnSkipCombat(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowDanger(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnEndAllAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EndAllActionDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnEndAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowBossInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnSelectSkill(int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnUseSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnCancelSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowCurTurnDanmaku(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnCloseCurTurnDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnShowOneDanmaku(string text, int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowOneDanmaku(string text, string userId, bool isWitness)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDanmakuUIActive(bool bIsShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearDanmakuUIInputField()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleMapClick(GridPosition p, bool isDoubleClick = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ShouldShowActorInfo(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_On3DTouch(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySelectHeroSound(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnClose(bool isSkip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleTreasureDialogUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetRewardGoodsUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CombatUIController_OnAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreCombatUIController_OnOk()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreCombatUIController_OnCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreCombatUIController_OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleSceneUIController_On3DTouch(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomPlayerHeroAlive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleLiveRoomPlayerAndHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleLiveRoomPlayerHeroAlive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBattleRoomMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivateBattleRoomMyActionCountdown(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomMyActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomMyActionTimeout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomOtherActionCountdown(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopBattleRoomOtherActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivateBattleRoomOtherActionCountdown(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomOtherActionCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBattleLiveActionCount(
      int playerIndex,
      TimeSpan ts,
      TimeSpan ts2,
      TimeSpan ts3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomSetAutoBattle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessBattlePendingNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessingPendingPlayerQuitNtfs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPlayerStatusChangedNtf(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomQuitNtf(int playerIndex, BattleRoomQuitReason reason)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomBattleCommandExecuteNtf(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomTeamBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomGuildMassiveCombatBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomRealTimePVPBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPeakArenaBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPeakArenaMatchFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPeakArenaPlayerForfeitNtf(string forfeitUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleLiveRoomFinishNtf(BattleLiveRoomFinishReason reason)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPeakArenaDanmakuNtf(string text, string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckReconnectBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReconnectBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReconnectBattleLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AfterReconnectRebuildBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleRoom GetBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomPlayerHeroCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomPrepareCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRealtimePVPBattlePrepareCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRealtimePVPBattlePrepareStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageActorTag(BattlePrepareStageActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessBattlePreparePendingNtfs(bool playFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadAndUpdateBattleRoomStageActors(List<int> posList, bool playFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleRoomStageActors(int posIdx, BattleHero hero, bool playFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PVPBattlePrepareUIController_OnPrepareConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomHeroSetupNtf(List<int> posList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomDataChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomTeamBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPVPBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomRealTimePVPBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPeakArenaBattleStartNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleResult(
      BattleType battleType,
      int stars,
      BattleReward battleReward,
      bool isFirstWin,
      List<int> gotAchievements,
      BattleLevelAchievement[] achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleLose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleResultScore(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleResultBoss(BattleReward battleReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleReportEnd(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleReportEnd(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleReportEnd(
      PeakArenaLadderBattleReport battleReport,
      int battleReportBoRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowBattleReportEnd(BattleRoom battleRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ProBattleReport BuildBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckBattleResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleRoomInviteTeammateDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPeakArenaBattleReportBoRound(int boRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnWinOrLoseEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultScoreUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleResultBossUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLoseUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddBattleReportEndUITaskEvents(BattleReportEndUITask battleReportEndUITask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveBattleReportEndUITaskEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPlayAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnNextRound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPeakArenaConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPeakArenaContinue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMatchingNowUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleReportEndUITask_OnPeakArenaLiveExit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPeakArenaBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPreCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitPreCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleRoomUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattleCommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitBattlePrepareUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopBattle(bool win, bool abnormal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleLoseUseRegretDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnShowBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHideBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnShowBattleLoseCondition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHideBattleLoseCondition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextTurnAnimationEnd(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextTeam(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextPlayer(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnNextActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorActive(ClientBattleActor a, bool newStep, int step, int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorActionBegin(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorActionEnd(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorMove(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorTryMove(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorNoAct(ClientBattleActor a, int endStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorTarget(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition pos,
      int armyRelationValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorSkill(ClientBattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorSkillEnd(ClientBattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorSkillHit(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      int monsterTotalDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorBuffHit(
      ClientBattleActor a,
      ConfigDataBuffInfo buffInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      int monsterTotalDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorTerrainHit(
      ClientBattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorImmune(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorPassiveSkill(ClientBattleActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorGuard(ClientBattleActor a, ClientBattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorDie(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorAppear(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorDisappear(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorReplace(ClientBattleActor a, ClientBattleActor newActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientActorHealthPointChange(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCancelCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPreStartCombat(BattleActor a, BattleActor b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnLoadCombatAssets(BattleActor a, BattleActor b, Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartCombat(BattleActor a, BattleActor b, bool splitScreen)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPrepareFastCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartFastCombat(FastCombatActorInfo a, FastCombatActorInfo b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopSkillCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleDialogUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnShowBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBossPhase(int phase)
    {
      // ISSUE: unable to decompile the method.
    }

    public static event Action<ClientBattleActor> m_onActiveActorEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static event Action<ClientBattleActor> m_onDeactiveActorEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleTeamSetNetTask(
      BattleType battleType,
      int battleId,
      int levelId,
      List<int> heros)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelAttackNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelWayPointMoveNetTask(ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelScenarioHandleNetTask(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftLevelAttackNetTask(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelAttackNetTask(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAnikiLevelAttackNetTask(ConfigDataAnikiLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartThearchyLevelAttackNetTask(ConfigDataThearchyTrialLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMemoryCorridorLevelAttackNetTask(ConfigDataMemoryCorridorLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroTrainningLevelAttackNetTask(ConfigDataHeroTrainningLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroPhantomLevelAttackNetTask(ConfigDataHeroPhantomLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTreasureLevelAttackNetTask(ConfigDataTreasureLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedScoreLevelAttackNetTask(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedChallengeLevelAttackNetTask(ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerLevelAttackNetTask(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartEternalShrineLevelAttackNetTask(ConfigDataEternalShrineLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroAnthemLevelAttackNetTask(ConfigDataHeroAnthemLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAncientCallBossAttackNetTask(ConfigDataAncientCallBossInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionLevelAttackNetTask(GameFunctionType levelType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionEventAttackNetTask(ConfigDataCollectionEventInfo collectionEventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildMassiveCombatAttackNetTask(
      ConfigDataGuildMassiveCombatLevelInfo levelInfo,
      List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaOpponentAttackFightingNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleLevelAttackNetTaskResult(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartLevelFinishedNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWayPointBattleFinishedNetTask(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRiftLevelBattleFinishedNetTask(ConfigDataRiftLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroDungeonLevelBattleFinishedNetTask(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAnikiLevelBattleFinishedNetTask(ConfigDataAnikiLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartThearchyLevelBattleFinishedNetTask(ConfigDataThearchyTrialLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTreasureLevelBattleFinishedNetTask(ConfigDataTreasureLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMemoryCorridorLevelBattleFinishedNetTask(
      ConfigDataMemoryCorridorLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroTrainningLevelBattleFinishedNetTask(
      ConfigDataHeroTrainningLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroPhantomLevelBattleFinishedNetTask(ConfigDataHeroPhantomLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedScoreLevelBattleFinishedNetTask(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedChallengeLevelBattleFinishedNetTask(
      ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartClimbTowerLevelBattleFinishedNetTask(ConfigDataTowerFloorInfo floorInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartEternalShrineLevelBattleFinishedNetTask(
      ConfigDataEternalShrineLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartHeroAnthemLevelBattleFinishedNetTask(ConfigDataHeroAnthemLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartAncientCallBossBattleFinishedNetTask(ConfigDataAncientCallBossInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionLevelFinishNetTask(GameFunctionType levelType, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCollectionEventBattleFinishedNetTask(
      ConfigDataCollectionEventInfo collectionEventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartGuildMassiveCombatAttackFinishedNetTask(
      ConfigDataGuildMassiveCombatLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleFinishedNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBattleFinishedNetTaskResult(
      int result,
      bool isWin,
      BattleReward reward,
      int stars = 0,
      bool isFirstWin = false,
      List<int> gotAchievements = null,
      BattleLevelAchievement[] achievements = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleCancelNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartDanmakuPostNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomHeroSetupNetTask(int heroId, int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomHeroSwapNetTask(int position1, int position2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomHeroSetoffNetTask(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomPlayerStatusChangeNetTask(PlayerBattleStatus status, Action onOk)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomPlayerStatusChangeNetTaskAndAutoBattle(bool isAutoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomQuitNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaBattleLiveRoomQuitNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomEndCurrentBPTurnNetTask(Action onOk)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomPlayerActionBeginNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StarBattleRoomBPStageCommandExecuteNetTask(BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleRoomBattleCommandExecuteNetTask(LinkedList<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaDanmakuNetTask(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetWinConditionTargetPosition(
      ConfigDataBattleWinConditionInfo winConditionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GridPosition> SetupReachRegion(bool isSkipBattlePrepare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupBattlePauseUIController(List<GridPosition> reachRegion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupBattlePauseUIAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePauseUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePauseUIController_OnShowPlayerSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePauseUIController_OnExit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExitBattleDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStagePlayerAndHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStageHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStageTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStageCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBpStageAutoBanPick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStageBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStageBattleReportPlayerAndHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaBPStageBattleReportTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPeakArenaBPStageBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBPStageBattleReportGotoTurn(int turn, bool showBpEffect = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBPStageBattleReportNextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBPStageBattleReportStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickPeakArenaBpStageBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopPeakArenaBPStageBattleReportAutoPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsPeakArenaBpStageBattleReportPaused()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomBPStageCommandExecuteNtf(
      int playerIndex,
      ProBPStageCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBpStageHeroClick(
      PeakArenaBpStageHeroItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBpStageConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBpStageAuto(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBattleReportSkip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBattleReportPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBattleReportPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBattleReportBackward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnBattleReportForward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaBattlePrepareUIController_OnSendDanmaku(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnBattleRoomPlayerBPStageInfoInitNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattleUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMyStageActorCountMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> SetupMyHeros(
      ConfigDataBattleInfo battleInfo,
      int number,
      BattleType battleType,
      int levelId,
      bool isSkipBattlePrepare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FillMyHeros(List<int> heroIds, int count, List<int> disableHeroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupStageActors(bool isSkipBattlePrepare)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddUseableHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ConfigDataUnchartedScoreInfo GetUnchartedScoreInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetHeroUnchartedScoreBonus(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataTowerBattleRuleInfo GetTowerBattleRuleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataTowerBonusHeroGroupInfo GetTowerBonusHeroGroupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTowerPowerUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GuildPlayerMassiveCombatInfo GetGuildPlayerMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> GetGuildMassiveCombatPreferredHeroTagIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGuildMassiveCombatCampUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEternalShrineCampUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeroAnthemPowerUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAncientCallPowerUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsCollectionActivityDropUpHero(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ConfigDataCollectionActivityInfo GetCollectionActivityInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetHeroCollectionActivityScoreBonus(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private StageActorTagType GetHeroTagType(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoadArenaAttackerHeroActionValue(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CompareHero(BattleHero h1, BattleHero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroUnchartdScoreBonus(BattleHero h1, BattleHero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroCollectionActivityScoreBonus(BattleHero h1, BattleHero h2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetStagePositionCenter(StagePositionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupBattlePrepareTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsListElementsEqual(List<int> list0, List<int> list1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BuildBattleTeamSetups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ModifyBattleTeamSetups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BuildBattleTeamSetup(int team, bool saveHeroList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSoldierCount(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTeamAndStartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattlePrepareLoadState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleLoadState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowMoveAndAttackRegion_Prepare(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateArenaAttackerHeroIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<TrainingTech> GetPlayerTrainingTechs(
      int team,
      int playerIndex,
      bool isNpc = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPlayerLevel(int team, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private PeakArenaBattleReportPlayerSummaryInfo GetPeakArenaBattleReportPlayer(
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ulong GetPlayerSessionId(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMyPlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetPeakArenaBORound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool BattlePrepareCanChangeSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBattlePrepareDisableCameraMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float GetBattlePrepareChangeCameraSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsShowRecommendHeroButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnPauseBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowArmyRelation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowRecommendHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroNotFullDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnTestOnStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowMyActorInfo(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnHideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnHeroOnStage(
      BattleHero hero,
      GridPosition pos,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorOffStage(BattlePrepareStageActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorMove(
      BattlePrepareStageActor sa,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnStageActorSwap(
      BattlePrepareStageActor sa1,
      BattlePrepareStageActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnUpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattlePrepareUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnBeginDragHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareUIController_OnEndDragHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Hero GetHeroFromBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSkillPanel(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSkill(
      BattleHero battleHero,
      List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSoldier(
      BattleHero battleHero,
      int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionOrderUIController_OnConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUIStateRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRegretButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRegretActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegretGotoStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindRegretStepIndexByStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindRegretStepIndexByTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActivateRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BuildBattleReportRegretData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretBackward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretForward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretPrevTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnRegretNextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanUseBattleReportRegret()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleReportPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnBattleReportPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnBattleReportPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnBattleReportBackward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnBattleReportForward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnBattleReportPrevTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUIController_OnBattleReportNextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> UserGuide_GetEnforceHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetUserGuideBattleSettings()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
