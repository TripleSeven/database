﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildManagementUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class GuildManagementUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string ParamKey_JumpUI = "JumpUI";
    private GuildManagementUIController m_guildManagementUIController;
    private static string m_guildId;
    private PlayerSimpleInfoUITask m_playerSimpleInfoUITask;
    private float m_lastUpdateTime;
    private static Guild m_guild;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_GuildManagementUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnClose;
    private static DelegateBridge __Hotfix_PlayerContext_EventOnGuildUpdateInfo;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnQuitGuild;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGetCanInvitePlayerList;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGuildHiringDeclarationSet;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGuildInfoSet;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGuildAnnouncementSet;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGetGuildJoinApply;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGuildJoinConfirmOrRefuse;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnChangeGuildName;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGetGuildJournal;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGuildInviteMember;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnAllRefuseButtonClick;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGuildMemberClick;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGotoGuildStore;
    private static DelegateBridge __Hotfix_GuildManagementUIController_OnGotoGuildGameListPanel;
    private static DelegateBridge __Hotfix_GuildGameListUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_OnClose;
    private static DelegateBridge __Hotfix_PlayerSimpleInfoUITask_OnPrivateChatButtonClick;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_SendRefreshGuildReq;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildManagementUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildManagementUITask StartUITask(
      string guildId = null,
      UIIntent intent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GuildManagementUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnQuitGuild(Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGetCanInvitePlayerList(
      Action<List<UserSummary>> OnSucceed,
      bool isShowSucceedTip)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildHiringDeclarationSet(
      string hiringDeclaration,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildInfoSet(
      bool autoJoin,
      int joinLevel,
      string announcement,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildAnnouncementSet(
      string announcement,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGetGuildJoinApply(Action<List<UserSummary>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildJoinConfirmOrRefuse(
      string userId,
      bool isAccept,
      Action OnEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnChangeGuildName(string name, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGetGuildJournal(Action<List<GuildLog>> OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildInviteMember(string userId, Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnAllRefuseButtonClick(Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGuildMemberClick(
      string userId,
      Vector3 pos,
      PlayerSimpleInfoUITask.PostionType posType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGotoGuildStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildManagementUIController_OnGotoGuildGameListPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildGameListUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerSimpleInfoUITask_OnPrivateChatButtonClick(BusinessCard playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendRefreshGuildReq()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
