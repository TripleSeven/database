﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleSceneUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleSceneUIController : UIControllerBase, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
  {
    [AutoBind("./ActionMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionMarkGameObject;
    [AutoBind("./ActionMark2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actionMark2GameObject;
    [AutoBind("./WinCondition", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_winConditionUIStateController;
    [AutoBind("./WinCondition/Defeat", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionKillGameObject;
    [AutoBind("./WinCondition/Protect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_winConditionProtectGameObject;
    [AutoBind("./SelectionMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectionMarkGameObject;
    [AutoBind("./PassiveSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_passiveSkillUIStateController;
    [AutoBind("./PassiveSkill/Panel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_passiveSkillIconImage;
    [AutoBind("./PassiveSkill/Panel/TalentIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_passiveSkillTalentIconImage;
    [AutoBind("./PassiveSkill/Panel/EquipIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_passiveSkillEquipIconGameObject;
    [AutoBind("./PassiveSkill/Panel/EquipIconImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_passiveSkillEquipIconImage;
    [AutoBind("./PassiveSkill/Panel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_passiveSkillNameText;
    [AutoBind("./Guard", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_guardUIStateController;
    [AutoBind("./ActionArrowGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_protectBanGroupUIStateController;
    [AutoBind("./ActionArrowGroup/Protect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_protectUIStateController;
    [AutoBind("./ActionArrowGroup/KickOut", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_banUIStateController;
    [AutoBind("./DamageNumbers", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumbersGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/DamageNumber", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberPrefab;
    [AutoBind("./Prefabs/DamageNumberWeak", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberWeakPrefab;
    [AutoBind("./Prefabs/DamageNumberStrong", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberStrongPrefab;
    [AutoBind("./Prefabs/DamageNumberCritical", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberCriticalPrefab;
    [AutoBind("./Prefabs/DamageNumberSoldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_damageNumberSoldierPrefab;
    [AutoBind("./Prefabs/HealNumber", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_healNumberPrefab;
    [AutoBind("./Prefabs/Immune", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_immunePrefab;
    [AutoBind("./Prefabs/Target1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target1Prefab;
    [AutoBind("./Prefabs/Target2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target2Prefab;
    [AutoBind("./Prefabs/Target3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target3Prefab;
    [AutoBind("./Prefabs/Target4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target4Prefab;
    [AutoBind("./Prefabs/Target5", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target5Prefab;
    [AutoBind("./Prefabs/Target6", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target6Prefab;
    [AutoBind("./Prefabs/Target7", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_target7Prefab;
    [AutoBind("./Prefabs/CanAction", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_canActionPrefab;
    private ClientBattle m_clientBattle;
    private GameObjectPool<DamageNumber> m_damageNumberPool;
    private GameObjectPool<DamageNumber> m_damageNumberWeakPool;
    private GameObjectPool<DamageNumber> m_damageNumberStrongPool;
    private GameObjectPool<DamageNumber> m_damageNumberCriticalPool;
    private GameObjectPool<DamageNumber> m_damageNumberSoldierPool;
    private GameObjectPool<DamageNumber> m_healNumberPool;
    private GameObjectPool<DamageNumber> m_immunePool;
    private GameObjectPool[] m_targetIconPools;
    private GameObjectPool m_canActionIconPool;
    private float m_hideTargetIconTime;
    private const int InvalidPointerID = -1000;
    private int m_downPointerId;
    private int m_clickPointerId;
    private int m_dragPointerId;
    private bool m_is3DTouchTriggered;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_PrepareBattle;
    private static DelegateBridge __Hotfix_StartBattle;
    private static DelegateBridge __Hotfix_StopBattle;
    private static DelegateBridge __Hotfix_ShowBattleWinCondition;
    private static DelegateBridge __Hotfix_HideBattleWinCondition;
    private static DelegateBridge __Hotfix_OnActorSkillHit;
    private static DelegateBridge __Hotfix_OnActorBuffHit;
    private static DelegateBridge __Hotfix_OnActorTerrainHit;
    private static DelegateBridge __Hotfix_ShowDamageNumbers;
    private static DelegateBridge __Hotfix_OnActorAttachImmune;
    private static DelegateBridge __Hotfix_OnActorPassiveSkill;
    private static DelegateBridge __Hotfix_OnActorGuard;
    private static DelegateBridge __Hotfix_OnActorCombatDamage;
    private static DelegateBridge __Hotfix_ShowDamangeNumber;
    private static DelegateBridge __Hotfix_SetActiveActor;
    private static DelegateBridge __Hotfix_ShowSelectionMark;
    private static DelegateBridge __Hotfix_HideSelectionMark;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_Update3DTouch;
    private static DelegateBridge __Hotfix_GetActorGraphicCenterPosition;
    private static DelegateBridge __Hotfix_ShowAttackTargetIcons;
    private static DelegateBridge __Hotfix_ShowSkillTargetIcons;
    private static DelegateBridge __Hotfix_ShowSkillTargetIcon_1;
    private static DelegateBridge __Hotfix_ShowAttackTargetIcon;
    private static DelegateBridge __Hotfix_ShowSkillTargetIcon_0;
    private static DelegateBridge __Hotfix_HideTargetIcons;
    private static DelegateBridge __Hotfix_AllocateAttackTargetIcon;
    private static DelegateBridge __Hotfix_AllocateSkillTargetIcon;
    private static DelegateBridge __Hotfix_ShowCanActionIcons;
    private static DelegateBridge __Hotfix_HideCanActionIcons;
    private static DelegateBridge __Hotfix_ShowProtectIndicator;
    private static DelegateBridge __Hotfix_ShowBanIndicator;
    private static DelegateBridge __Hotfix_HideBanProtectIndicator;
    private static DelegateBridge __Hotfix_GridPositionToMapPosition;
    private static DelegateBridge __Hotfix_OnPointerDown;
    private static DelegateBridge __Hotfix_OnPointerUp;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_OnBeginDrag;
    private static DelegateBridge __Hotfix_OnEndDrag;
    private static DelegateBridge __Hotfix_OnDrag;
    private static DelegateBridge __Hotfix_add_EventOnPointerDown;
    private static DelegateBridge __Hotfix_remove_EventOnPointerDown;
    private static DelegateBridge __Hotfix_add_EventOnPointerUp;
    private static DelegateBridge __Hotfix_remove_EventOnPointerUp;
    private static DelegateBridge __Hotfix_add_EventOnPointerClick;
    private static DelegateBridge __Hotfix_remove_EventOnPointerClick;
    private static DelegateBridge __Hotfix_add_EventOnBeginDrag;
    private static DelegateBridge __Hotfix_remove_EventOnBeginDrag;
    private static DelegateBridge __Hotfix_add_EventOnEndDrag;
    private static DelegateBridge __Hotfix_remove_EventOnEndDrag;
    private static DelegateBridge __Hotfix_add_EventOnDrag;
    private static DelegateBridge __Hotfix_remove_EventOnDrag;
    private static DelegateBridge __Hotfix_add_EventOn3DTouch;
    private static DelegateBridge __Hotfix_remove_EventOn3DTouch;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleSceneUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle clientBattle, BattleMapUIController battleMapUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBattleWinCondition(
      ConfigDataBattleWinConditionInfo winConditionInfo,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorSkillHit(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorBuffHit(
      ClientBattleActor a,
      ConfigDataBuffInfo buffInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorTerrainHit(
      ClientBattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDamageNumbers(
      ClientBattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorAttachImmune(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorPassiveSkill(ClientBattleActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorGuard(ClientBattleActor a, ClientBattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCombatDamage(ClientBattleActor a, int heroDamage, int soldierDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDamangeNumber(
      ClientBattleActor a,
      int hpModify,
      DamageNumberType damageNumberType,
      bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActiveActor(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectionMark(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideSelectionMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update3DTouch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GetActorGraphicCenterPosition(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAttackTargetIcons(
      List<GridPosition> positions,
      List<int> armyRelationValues,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkillTargetIcons(
      List<GridPosition> positions,
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSkillTargetIcon(
      GameObject go,
      GridPosition pos,
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowAttackTargetIcon(GridPosition pos, int armyRelationValue, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSkillTargetIcon(
      GridPosition pos,
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos,
      float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideTargetIcons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject AllocateAttackTargetIcon(int armyRelationValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject AllocateSkillTargetIcon(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCanActionIcons(List<GridPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideCanActionIcons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowProtectIndicator(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBanIndicator(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBanProtectIndicator()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToMapPosition(GridPosition p, float z)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<PointerEventData> EventOnPointerDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnPointerUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnPointerClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnBeginDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<PointerEventData> EventOnDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Vector2> EventOn3DTouch
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
