﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaEquipmentDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaEquipmentDetailUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipItemDescStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_equipBGButton;
    [AutoBind("./Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescTitleText;
    [AutoBind("./Detail/Lay/FrameImage/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescLvText;
    [AutoBind("./Detail/Lay/FrameImage/Top/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescExpText;
    [AutoBind("./Detail/Lay/FrameImage/Top/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconImage;
    [AutoBind("./Detail/Lay/FrameImage/Top/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescIconBg;
    [AutoBind("./Detail/Lay/FrameImage/Top/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSSREffect;
    [AutoBind("./Detail/Lay/FrameImage/Top/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_equipItemDescProgressImage;
    [AutoBind("./Detail/Lay/FrameImage/Top/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescStarGroup;
    [AutoBind("./Detail/Lay/FrameImage/Top/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescEquipLimitContent;
    [AutoBind("./Detail/Lay/FrameImage/Top/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropContent;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropATGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropATValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDFGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDFValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropHPGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropHPValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagiccGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropMagicDFGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropMagicDFValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescPropDexGo;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescPropDexValueText;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Detail/Lay/FrameImage/Top/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillContent;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillNameText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillLvText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescSkillOwnerGameObject;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Top/BelongBGImage/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillOwnerText;
    [AutoBind("./Detail/Lay/FrameImage/Button/SkillContent/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipItemDescSkillDescText;
    [AutoBind("./Detail/Lay/FrameImage/Button/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemDescNotEquipSkillTip;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_PlayShowAnimation;
    private static DelegateBridge __Hotfix_SetEquipmentItemDesc;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_SetPropEnchantment;
    private static DelegateBridge __Hotfix_OnCloseClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayShowAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEquipmentItemDesc(
      EquipmentBagItem equipment,
      Hero hero,
      List<EquipmentBagItem> equipmentList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropEnchantment(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
