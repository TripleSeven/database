﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroAnthemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroAnthemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./RankButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankButton;
    [AutoBind("./PlayerResource/Challenge/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeValueText;
    [AutoBind("./AchievementButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_AchievementValueText;
    [AutoBind("./Chapters/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_chapterListScrollRect;
    [AutoBind("./RankPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankPanelStateCtrl;
    [AutoBind("./RankPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rankPanelBgButton;
    [AutoBind("./RankPanel/Detail/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private DynamicGrid m_rankPanelGrid;
    [AutoBind("./RankPanel/Detail/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rankPanelPlayerRankingStateCtrl;
    [AutoBind("./RankPanel/Detail/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankPanelPlayerRankImage;
    [AutoBind("./RankPanel/Detail/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerRankText;
    [AutoBind("./RankPanel/Detail/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerNameText;
    [AutoBind("./RankPanel/Detail/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankPanelPlayerHeadIcon;
    [AutoBind("./RankPanel/Detail/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rankPanelPlayerHeadFrameDummy;
    [AutoBind("./RankPanel/Detail/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerLevelText;
    [AutoBind("./RankPanel/Detail/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankPanelPlayerArenaPointsText;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefab/HeroAnthemButton", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HeroAnthemChapterItemPrefab;
    [AutoBind("./Prefab/RankListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rankListItemPrefab;
    private List<HeroAnthemChapterItemUIController> m_heroAnthemChapterItemList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private ScrollSnapCenter m_chaptersScrollSnapCenter;
    private HeroAnthemChapterItemUIController m_curHeroAnthemChapterItemCtrl;
    private int m_curChapterCtrlIndex;
    private List<RankingTargetPlayerInfo> m_rankPlayerList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_SetHeroAnthemChapters;
    private static DelegateBridge __Hotfix_SetCurChapterAchievements;
    private static DelegateBridge __Hotfix_ChaptersScrollSnapCenter_OnEndDrag;
    private static DelegateBridge __Hotfix_AddHeroAnthemChapterItem;
    private static DelegateBridge __Hotfix_SetCurrentChapter;
    private static DelegateBridge __Hotfix_ClearHeroAnthemChapterListItems;
    private static DelegateBridge __Hotfix_OnRankButtonClick;
    private static DelegateBridge __Hotfix_ShowRankPanel;
    private static DelegateBridge __Hotfix_SetRankPanel;
    private static DelegateBridge __Hotfix_SetPlayerRankInfo;
    private static DelegateBridge __Hotfix_CloseRanlPanel;
    private static DelegateBridge __Hotfix_OnRankPanelGridCellInit;
    private static DelegateBridge __Hotfix_OnRankPanelGridCellUpdate;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_HeroAnthemChapterItem_OnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnRankButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnRankButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGotoHeroAnthemChapter;
    private static DelegateBridge __Hotfix_remove_EventOnGotoHeroAnthemChapter;

    [MethodImpl((MethodImplOptions) 32768)]
    private HeroAnthemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAnthemChapters(List<ConfigDataHeroAnthemInfo> heroAnthemInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurChapterAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChaptersScrollSnapCenter_OnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroAnthemChapterItem(
      ConfigDataHeroAnthemInfo anthemInfo,
      bool isLocked,
      bool isNew)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentChapter(ConfigDataHeroAnthemInfo heroAnthemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearHeroAnthemChapterListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRankPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRankPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlayerRankInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseRanlPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DynamicGridCellController OnRankPanelGridCellInit(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankPanelGridCellUpdate(DynamicGridCellController cell)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAnthemChapterItem_OnButtonClick(HeroAnthemChapterItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRankButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroAnthemInfo> EventOnGotoHeroAnthemChapter
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
