﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildMassiveCombatUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class GuildMassiveCombatUITask : UITask
  {
    public const string ParamKey_GuildMassiveCombatInstanceId = "GuildMassiveCombatInstanceId";
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private GuildMassiveCombatUIController m_guildMassiveCombatUIController;
    private ulong m_guildMassiveCombatInstanceId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_PlayerContext_EventOnGuildUpdateInfo;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_StartTeamRoomInfoUITask;
    private static DelegateBridge __Hotfix_CloseAllSubWindow;
    private static DelegateBridge __Hotfix_OnClose;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnGiveUpButtonClick;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnCompleteButtonClick;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnChatButtonClick;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnReturnToWorld;
    private static DelegateBridge __Hotfix_CheckAttackStronghold;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnSelectStronghold;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnSingleBattle;
    private static DelegateBridge __Hotfix_GuildMassiveCombatUIController_OnTeamBattle;
    private static DelegateBridge __Hotfix_ShowCombatResultEffectAndClose;
    private static DelegateBridge __Hotfix_PlayerContext_OnGuildMassiveCombatNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnGuildMassiveCombatSurrenderNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnGuildMassiveCombatConqueredNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartTeamRoomInfoUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAllSubWindow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnGiveUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnCompleteButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnReturnToWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckAttackStronghold(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnSelectStronghold(
      GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnSingleBattle(
      GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GuildMassiveCombatUIController_OnTeamBattle(GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCombatResultEffectAndClose(
      GuildMassiveCombatInfo combatInfo,
      bool isWin,
      float delay = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnGuildMassiveCombatNtf(GuildMassiveCombatGeneral combat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnGuildMassiveCombatSurrenderNtf(
      GuildMassiveCombatInfo lastFinishedCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnGuildMassiveCombatConqueredNtf(
      GuildMassiveCombatInfo lastFinishedCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
