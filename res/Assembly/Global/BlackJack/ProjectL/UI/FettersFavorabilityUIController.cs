﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FettersFavorabilityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FettersFavorabilityUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateCtrl;
    [AutoBind("./MaleOrFemale", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sexStateCtrl;
    [AutoBind("./TouchOrNot", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchInOutStateCtrl;
    [AutoBind("./LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_levelUpStateCtrl;
    [AutoBind("./TouchEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchStateCtrl;
    [AutoBind("./InfoPanelEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoStateCtrl;
    [AutoBind("./PresentEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentStateCtrl;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_resourceContainer;
    [AutoBind("./EditModeUI", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_editModeGo;
    [AutoBind("./EditModeUI/BattleValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battleValueText;
    [AutoBind("./EditModeUI/UnlockValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockValueText;
    [AutoBind("./EditModeUI/MaxValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_maxValueText;
    [AutoBind("./Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButton;
    [AutoBind("./Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionButtonStateCtrl;
    [AutoBind("./Margin/Confession/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionButtonNewImage;
    [AutoBind("./Confession", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionStateCtrl;
    [AutoBind("./ConfessionUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionPanelGo;
    [AutoBind("./ConfessionUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionPanelStateCtrl;
    [AutoBind("./ConfessionUIPrefab/Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionLetterButton;
    [AutoBind("./ConfessionUIPrefab/Margin/Confession", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionLetterButtonStateCtrl;
    [AutoBind("./ConfessBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_confessBGImage;
    [AutoBind("./ConfessionUIShowBg", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionUIShowBgButton;
    [AutoBind("./ConfessionUIPrefab/PenButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelPenButton;
    [AutoBind("./ConfessionUIPrefab/UnderGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionPanelUnderTextPanel;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionButtonListPanelStateCtrl;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButtonListPanelButton1;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButtonListPanelButton2;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionButtonListPanelButton3;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button1/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionButtonListPanelButton1Text;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button2/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionButtonListPanelButton2Text;
    [AutoBind("./ConfessionUIPrefab/ButtonListPanel/Button3/Image/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionButtonListPanelButton3Text;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionLetterPanelStateCtrl;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ConfessionImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionHeroCharGo;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ConfessionImage/Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_confessionHeroCharTransform;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionLetterTitleText;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/MainText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionLetterMainText;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/UnderPart/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_confessionLetterLastText;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/InfoScrollView/Viewport/Content/UnderPart/PenButton/Effect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionLetterPenButtonGo;
    [AutoBind("./ConfessionUIPrefab/ConfessionwordPanel/ReelMiddle/LetterContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionLetterPenButton;
    [AutoBind("./RightTop", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_confessionPanelRightTopGo;
    [AutoBind("./RightTop/ShareButton/HideButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelHideButton;
    [AutoBind("./RightTop/ShareButton/HideButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confessionPanelHideButtonStateCtrl;
    [AutoBind("./RightTop/ShareButton/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelBackButton;
    [AutoBind("./RightTop/ShareButton/ShareButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionPanelShareButton;
    [AutoBind("./ConfessionUIPrefab/ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shareButtonDummy;
    [AutoBind("./ConfessionUIPrefab/ShareButtonDummy/ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confessionSharePanelReturnButton;
    [AutoBind("./ConfessionUIPrefab/SharePhotpDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ReturnButton/HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Margin/Favorability/ValueTextGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_favorabilityValueTextStateCtrl;
    [AutoBind("./Margin/Favorability/ValueTextGroup/ValueNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityValueText;
    [AutoBind("./Margin/Favorability/UpArrow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_favorabilityAddUpArrowGo;
    [AutoBind("./Margin/Favorability/UpArrowText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityAddValueText;
    [AutoBind("./Margin/Favorability/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityHeroNameText;
    [AutoBind("./Margin/Favorability/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_favorabilityHeroNameStateCtrl;
    [AutoBind("./Margin/Favorability/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorabilityLvText;
    [AutoBind("./Margin/Favorability/MaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_favorabilityMaleSlider;
    [AutoBind("./Margin/Favorability/FemaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_favorabilityFemaleSlider;
    [AutoBind("./Margin/Favorability/LevelUpEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpEffectGroupGameObject;
    [AutoBind("./FemaleQinMiDu", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_femaleQinMiDuGameObject;
    [AutoBind("./MaleQinMiDu", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_maleQinMiDuGameObject;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Char/0/Happy_Effects", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charConfessEffectGameObject;
    [AutoBind("./Char/0/Love_Effects", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charConfessSucceedEffectGameObject;
    [AutoBind("./Word", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordGameObject;
    [AutoBind("./ContinueButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_charContinueButton;
    [AutoBind("./Margin/Touch", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchNumberStateCtrl;
    [AutoBind("./Margin/Touch/CountGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_touchCountStateCtrl;
    [AutoBind("./Margin/Touch/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_touchTimeText;
    [AutoBind("./ProgressGroup/EffectSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressEffectSlider;
    [AutoBind("./ProgressGroup/MaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressMaleSlider;
    [AutoBind("./ProgressGroup/FrmaleSlider", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressFemaleSlider;
    [AutoBind("./ProgressGroup/EffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressEffectGroup;
    [AutoBind("./MaleBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_maleBGImage;
    [AutoBind("./FemaleBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_femaleBGImage;
    [AutoBind("./LevelUpStarEffectGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_levelUpStarEffectGroup;
    [AutoBind("./Margin/Information", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_informationButton;
    [AutoBind("./Margin/Information/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_informationButtonNewImage;
    [AutoBind("./Margin/ToggleGroup/Fetters", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fettersButton;
    [AutoBind("./Margin/ToggleGroup/Fetters/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_fettersButtonRedMark;
    [AutoBind("./Margin/ToggleGroup/Fetters/PercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_fettersButtonPercentText;
    [AutoBind("./Margin/ToggleGroup/Memory", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_memoryButton;
    [AutoBind("./Margin/ToggleGroup/Memory", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_memoryButtonStateCtrl;
    [AutoBind("./Margin/ToggleGroup/Memory/NewImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_memoryButtonNewImage;
    [AutoBind("./Margin/ToggleGroup/Memory/PercentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_memoryButtonPercentText;
    [AutoBind("./Margin/ToggleGroup/Present", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_presentButton;
    [AutoBind("./TouchImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_touchImage;
    [AutoBind("./UnlockEvent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_unlockEventStateCtrl;
    [AutoBind("./UnlockEvent/Detail/Panel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockEventTitleText;
    [AutoBind("./UnlockEvent/Detail/Panel/InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_unlockEventContentText;
    [AutoBind("./UnlockEvent/BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unlockEventBackButton;
    [AutoBind("./PresentPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentPanel;
    [AutoBind("./PresentPanel/ItemScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentContentStateCtrl;
    [AutoBind("./PresentPanel/ItemScrollView/Scroll View", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_presentScrollRect;
    [AutoBind("./PresentPanel/ItemScrollView/Scroll View/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentContent;
    [AutoBind("./PresentPanel/ItemCount", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentItemCountShowUIStateCtrl;
    [AutoBind("./PresentPanel/ItemCount/ContinuousTouch", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentContinuousTouchUIStateCtrl;
    [AutoBind("./PresentPanel/ItemCount/Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_presentItemCountText;
    [AutoBind("./PresentPanel/ItemCount/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_presentItemIconImage;
    [AutoBind("./PresentPanel/ItemCount/IconImage/IconImageEffect", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_presentItemIconEffectImage;
    [AutoBind("./PresentExpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentExpEffect;
    [AutoBind("./PresentPanel/ItemScrollView/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_presentSendButton;
    [AutoBind("./PresentPanel/ItemScrollView/Button", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_presentSendButtonStateCtrl;
    [AutoBind("./PresentPanel/ItemScrollView/ButtonMaskImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentSendButtonMaskImage;
    [AutoBind("./Prefab/ItemPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_presentItemPrefab;
    private bool m_isMale;
    private bool m_isInGiftMode;
    private bool m_isFirstInFavorability;
    private Hero m_hero;
    private HeroCharUIController m_heroCharUIController;
    private List<Goods> m_heroFavoribilityLevelUpRewards;
    private int m_heroFavoribilityOldLevel;
    private int m_curUnlockEventIndex;
    private List<string> m_unlockEventTitleList;
    private List<string> m_unlockEventContentList;
    private int m_tempAddExp;
    private int m_tempFavorabilityExp;
    private int m_totalUseExpItemCount;
    private int m_tempFavorabilityLevel;
    private FettersGiftItemUIController m_curGiftBagItemCtrl;
    private FettersGiftItemUIController m_sendButtonDownGiftBagItemCtrl;
    private List<BagItemBase> m_giftBagItemCache;
    private ConfessionDialogBoxUIController m_dialogBoxUIController;
    private ConfigDataConfessionDialogInfo m_curDialogInfo;
    private IAudioPlayback m_currentAudio;
    private FettersFavorabilityUIController.ConfessionProgressState m_confessionProgressState;
    private HeroCharUIController m_confessionHeroCharUIController;
    private ConfessionLetterTextUIController m_confessionLetterTitleCtrl;
    private ConfessionLetterTextUIController m_confessionLetterContentCtrl;
    private ConfessionLetterTextUIController m_confessionLetterSignatureCtrl;
    private Text m_confessionShareNameText;
    private Text m_confessionShareLvText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private bool m_hasConfessed;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetConfessionShareGameObjects;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_UpdateViewInFettersFavorability;
    private static DelegateBridge __Hotfix_SetEditModeUIGameObjects;
    private static DelegateBridge __Hotfix_OpenFettersFavorability;
    private static DelegateBridge __Hotfix_SetFavorabilityValue;
    private static DelegateBridge __Hotfix_SetTouchCountState;
    private static DelegateBridge __Hotfix_SetTouchRecoverTime;
    private static DelegateBridge __Hotfix_DragTrigger_EventOnSliderFull;
    private static DelegateBridge __Hotfix_DragTrigger_EventOnClick;
    private static DelegateBridge __Hotfix_DragTrigger_EventOnEndDrag;
    private static DelegateBridge __Hotfix_DragTrigger_EventOnBeginDrag;
    private static DelegateBridge __Hotfix_DragTrigger_EventOnSliderEmpty;
    private static DelegateBridge __Hotfix_SetTouchStateCtrlOut;
    private static DelegateBridge __Hotfix_SetCharGoTweenPos;
    private static DelegateBridge __Hotfix_OnHeroInteractSucceed;
    private static DelegateBridge __Hotfix_AfterHeroInteractionResultEffect;
    private static DelegateBridge __Hotfix_PlayFavorbilityLevelUp;
    private static DelegateBridge __Hotfix_Co_PlayHeroInteractEffect;
    private static DelegateBridge __Hotfix_OnCharContinueButtonClick;
    private static DelegateBridge __Hotfix_ShowUnlockEvents;
    private static DelegateBridge __Hotfix_GetUnlockEvents;
    private static DelegateBridge __Hotfix_OnUnlockEventBackButtonClick;
    private static DelegateBridge __Hotfix_UpdateViewInFettersGift;
    private static DelegateBridge __Hotfix_OpenFettersGift;
    private static DelegateBridge __Hotfix_SetPresentContent;
    private static DelegateBridge __Hotfix_GiftBagItemComparer;
    private static DelegateBridge __Hotfix_OnSendButtonClickDown;
    private static DelegateBridge __Hotfix_SendUseGiftEvent;
    private static DelegateBridge __Hotfix_WaitTimeThenDoEvent;
    private static DelegateBridge __Hotfix_OnUseGiftItemSucceed;
    private static DelegateBridge __Hotfix_OnGiftItemClick;
    private static DelegateBridge __Hotfix_LocalAddExpTick;
    private static DelegateBridge __Hotfix_Co_SetFavorabilityValue;
    private static DelegateBridge __Hotfix_SetItemCountPanel;
    private static DelegateBridge __Hotfix_ResetGiftScrollViewPosition;
    private static DelegateBridge __Hotfix_StartHeroConfessionProgress;
    private static DelegateBridge __Hotfix_ShowDialog;
    private static DelegateBridge __Hotfix_ShowNextDialog;
    private static DelegateBridge __Hotfix_OnConfessionPanelHideButtonClick;
    private static DelegateBridge __Hotfix_OnConfessionUIShowBgButtonClick;
    private static DelegateBridge __Hotfix_OnConfessionPanelPenButtonClick;
    private static DelegateBridge __Hotfix_ShowConfessAnswerButtonList;
    private static DelegateBridge __Hotfix_ShowDialogAfterAnswer;
    private static DelegateBridge __Hotfix_OnConfessionLetterButtonClick;
    private static DelegateBridge __Hotfix_ShowConfessionLetter;
    private static DelegateBridge __Hotfix_ShowLetterWords;
    private static DelegateBridge __Hotfix_OnConfessionLetterPenButtonClick;
    private static DelegateBridge __Hotfix_OnConfessionPanelBackButtonClick;
    private static DelegateBridge __Hotfix_CloseConfessionPanel;
    private static DelegateBridge __Hotfix_ShowConfessSucceedEffect;
    private static DelegateBridge __Hotfix_C_ShowConfessSicceedEffect;
    private static DelegateBridge __Hotfix_SendConfesionReq;
    private static DelegateBridge __Hotfix_OnConfessionPanelShareButtonClick;
    private static DelegateBridge __Hotfix_OnConfessionSharePanelReturnButtonClick;
    private static DelegateBridge __Hotfix_OnWeiBoClick;
    private static DelegateBridge __Hotfix_OnWeChatClick;
    private static DelegateBridge __Hotfix_OnFacebookClick;
    private static DelegateBridge __Hotfix_OnTwitterClick;
    private static DelegateBridge __Hotfix_OnInstagramClick;
    private static DelegateBridge __Hotfix_Share;
    private static DelegateBridge __Hotfix_CaptureFrame;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_SetConfessionButtonState;
    private static DelegateBridge __Hotfix_Co_OnAnimationFinishedEnableInput;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnInformationButtonClick;
    private static DelegateBridge __Hotfix_OnFettersButtonClick;
    private static DelegateBridge __Hotfix_OnMemoryButtonClick;
    private static DelegateBridge __Hotfix_OnGiftButtonClick;
    private static DelegateBridge __Hotfix_OnConfessionButtonClick;
    private static DelegateBridge __Hotfix_InactiveWordGameObject;
    private static DelegateBridge __Hotfix_PlayHeroPerformance;
    private static DelegateBridge __Hotfix_GoToInformationPanel;
    private static DelegateBridge __Hotfix_SetIsFirstInFavorabilityTag;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnSliderFull;
    private static DelegateBridge __Hotfix_remove_EventOnSliderFull;
    private static DelegateBridge __Hotfix_add_EventOnGiftButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGiftButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnMemoryButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnMemoryButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnFettersButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnFettersButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnInformationButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnInformationButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnConfessionButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnConfessionButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnUseGift;
    private static DelegateBridge __Hotfix_remove_EventOnUseGift;
    private static DelegateBridge __Hotfix_add_EventOnGotoBagFullPanel;
    private static DelegateBridge __Hotfix_remove_EventOnGotoBagFullPanel;

    [MethodImpl((MethodImplOptions) 32768)]
    public FettersFavorabilityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConfessionShareGameObjects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersFavorability(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEditModeUIGameObjects()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenFettersFavorability(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFavorabilityValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTouchCountState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchRecoverTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnSliderFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnEndDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnBeginDrag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DragTrigger_EventOnSliderEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTouchStateCtrlOut(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCharGoTweenPos(bool isCharAtInState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHeroInteractSucceed(
      List<Goods> rewards,
      int heroPerformanceId,
      HeroInteractionResultType heroInteractionResultType,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AfterHeroInteractionResultEffect(string stateName, int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFavorbilityLevelUp(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayHeroInteractEffect(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCharContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowUnlockEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetUnlockEvents(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnlockEventBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInFettersGift(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenFettersGift(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPresentContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GiftBagItemComparer(BagItemBase a, BagItemBase b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClickDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendUseGiftEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitTimeThenDoEvent(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseGiftItemSucceed(List<Goods> rewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftItemClick(FettersGiftItemUIController ctrl, bool isNeedShowDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocalAddExpTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetFavorabilityValue(
      int curFavoribilityLv,
      int addLv,
      float finalSliderValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetItemCountPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetGiftScrollViewPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartHeroConfessionProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialog(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelHideButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionUIShowBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelPenButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowConfessAnswerButtonList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialogAfterAnswer(int dialoId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionLetterButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowConfessionLetter(int letterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowLetterWords(int letterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionLetterPenButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelBackButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseConfessionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowConfessSucceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator C_ShowConfessSicceedEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendConfesionReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionPanelShareButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionSharePanelReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFacebookClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInstagramClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Share(int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConfessionButtonState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_OnAnimationFinishedEnableInput(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInformationButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFettersButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMemoryButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfessionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InactiveWordGameObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayHeroPerformance(int heroPerformanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GoToInformationPanel(bool isGotoOrReturn, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsFirstInFavorabilityTag(bool v)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSliderFull
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnGiftButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnMemoryButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnFettersButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnInformationButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Hero> EventOnConfessionButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int, Action<List<Goods>>> EventOnUseGift
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGotoBagFullPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum ConfessionProgressState
    {
      Begin,
      Question,
      Letter,
      End,
    }
  }
}
