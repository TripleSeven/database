﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleReportEndUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleReportEndUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_leftPlayerIcon;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerLV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerLevel;
    [AutoBind("./Panel/LeftPlayer/Player/PlayerName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_leftPlayerName;
    [AutoBind("./Panel/RightPlyer/Player/PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rightPlayerIcon;
    [AutoBind("./Panel/RightPlyer/Player/PlayerLV/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerLevel;
    [AutoBind("./Panel/RightPlyer/Player/PlayerName", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rightPlayerName;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention/Win", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftWinGameObject;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention/Lose", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftLoseGameObject;
    [AutoBind("./Panel/LeftWinOrLoseOrAbstention/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftAbstentionGameObject;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention/Win", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightWinGameObject;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention/Lose", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightLoseGameObject;
    [AutoBind("./Panel/RightWinOrLoseOrAbstention/Text", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightAbstentionGameObject;
    [AutoBind("./Panel/LeftMatchGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftMatchGameObject;
    [AutoBind("./Panel/LeftMatchGroup/WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftMatchWin1GameObject;
    [AutoBind("./Panel/LeftMatchGroup/WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftMatchWin2GameObject;
    [AutoBind("./Panel/RightMatchGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightMatchGameObject;
    [AutoBind("./Panel/RightMatchGroup/WinGroup/Win1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightMatchWin1GameObject;
    [AutoBind("./Panel/RightMatchGroup/WinGroup/Win2/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightMatchWin2GameObject;
    [AutoBind("./Panel/PeakArenaPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaPanelGameObject;
    [AutoBind("./Panel/PeakArenaPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaConfirmButton;
    [AutoBind("./Panel/PeakArenaPanel/ContinueFightButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaContinueButton;
    [AutoBind("./Panel/LivePanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaLivePanelGameObject;
    [AutoBind("./Panel/LivePanel/Wait", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_peakArenaLiveWaitGameObject;
    [AutoBind("./Panel/LivePanel/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_peakArenaLiveExitButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Panel/PlayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playButton;
    [AutoBind("./Panel/ExitButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_exitButton;
    [AutoBind("./Panel/TestCountdownText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_testCountdownText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open_0;
    private static DelegateBridge __Hotfix_Open_1;
    private static DelegateBridge __Hotfix_Open_3;
    private static DelegateBridge __Hotfix_Open_2;
    private static DelegateBridge __Hotfix_Co_WaitAndNextRound;
    private static DelegateBridge __Hotfix_SetLeftPlayer;
    private static DelegateBridge __Hotfix_SetRightPlayer;
    private static DelegateBridge __Hotfix_SetLeftWinOrLoseOrAbstention;
    private static DelegateBridge __Hotfix_SetRightWinOrLoseOrAbstention;
    private static DelegateBridge __Hotfix_SetLeftWinCount;
    private static DelegateBridge __Hotfix_SetRightWinCount;
    private static DelegateBridge __Hotfix_SetTestCountdown;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnPlayButtonClick;
    private static DelegateBridge __Hotfix_OnExitButtonClick;
    private static DelegateBridge __Hotfix_OnPeakArenaConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnPeakArenaContinueButtonClick;
    private static DelegateBridge __Hotfix_OnPeakArenaLiveExitButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnPlayAgain;
    private static DelegateBridge __Hotfix_remove_EventOnPlayAgain;
    private static DelegateBridge __Hotfix_add_EventOnNextRound;
    private static DelegateBridge __Hotfix_remove_EventOnNextRound;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaConfirm;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaConfirm;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaContinue;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaContinue;
    private static DelegateBridge __Hotfix_add_EventOnPeakArenaLiveExit;
    private static DelegateBridge __Hotfix_remove_EventOnPeakArenaLiveExit;

    private BattleReportEndUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(RealTimePVPBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(PeakArenaLadderBattleReport battleReport, int battleReportBoRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(BattleRoom battleRoom)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitAndNextRound(float waitTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRightPlayer(string name, int level, int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftWinOrLoseOrAbstention(int i)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRightWinOrLoseOrAbstention(int i)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetLeftWinCount(int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRightWinCount(int winCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaContinueButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPeakArenaLiveExitButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnPlayAgain
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNextRound
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaConfirm
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaContinue
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPeakArenaLiveExit
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
