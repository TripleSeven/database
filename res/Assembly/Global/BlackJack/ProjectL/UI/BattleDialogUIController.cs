﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleDialogUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleDialogUIController : UIControllerBase
  {
    [AutoBind("./SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skipButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./WordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_wordsGameObject;
    [AutoBind("./Char/0", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char0GameObject;
    [AutoBind("./Char/1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_char1GameObject;
    private DialogCharUIController[] m_dialogCharUIControllers;
    private BattleDialogBoxUIController m_dialogBoxUIController;
    private IAudioPlayback m_currentAudio;
    private ConfigDataBattleDialogInfo m_dialogInfo;
    private float m_showDialogBoxTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_SetDialog;
    private static DelegateBridge __Hotfix_CloseDialog;
    private static DelegateBridge __Hotfix_Co_CloseDialog;
    private static DelegateBridge __Hotfix_ShowDialogBox;
    private static DelegateBridge __Hotfix_PlayVoice;
    private static DelegateBridge __Hotfix_StopVoice;
    private static DelegateBridge __Hotfix_Co_NextDialog;
    private static DelegateBridge __Hotfix_SetDialogBoxLogButton;
    private static DelegateBridge __Hotfix_OnSkipButtonClick;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnLogButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnSkip;
    private static DelegateBridge __Hotfix_remove_EventOnSkip;
    private static DelegateBridge __Hotfix_add_EventOnNext;
    private static DelegateBridge __Hotfix_remove_EventOnNext;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnLog;
    private static DelegateBridge __Hotfix_remove_EventOnLog;

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleDialogUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_CloseDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private IAudioPlayback PlayVoice(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_NextDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDialogBoxLogButton(bool bShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLogButtonClick(UIControllerBase uIControllerBase)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNext
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLog
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
