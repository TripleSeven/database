﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RewardGoodsDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RewardGoodsDescUIController : UIControllerBase
  {
    private GoodsType m_goodsType;
    private int m_goodsId;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    private GameObject HPGo;
    private GameObject ATGo;
    private GameObject DFGo;
    private GameObject MagicGo;
    private GameObject MagicDFGo;
    private GameObject DEXGo;
    private Text HPText;
    private Text ATText;
    private Text DFText;
    private Text MagicText;
    private Text MagicDFText;
    private Text DEXText;
    private RectTransform m_backgroundTransform;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitRewardGoodsDescInfo;
    private static DelegateBridge __Hotfix_SetEquipmentPropInfo;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_GetBackgroundTransform;
    private static DelegateBridge __Hotfix_ShowRewardGoodsDesc;
    private static DelegateBridge __Hotfix_Co_ShowRewardGoodsDesc;
    private static DelegateBridge __Hotfix_ShowPanel;
    private static DelegateBridge __Hotfix_ClosePanel;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRewardGoodsDescInfo(GoodsType goodsType, int goodsId, bool isNeedAutoClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropInfo(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetBackgroundTransform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowRewardGoodsDesc(
      PrefabControllerBase ctrl,
      GoodsType goodsType,
      int goodsId,
      int alignType = 0,
      GameObject gameObjectForPosCalc = null,
      bool isNeedAutoClose = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_ShowRewardGoodsDesc(
      RewardGoodsDescUIController descCtrl,
      GameObject gameObjectForPosCalc,
      int alignType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
