﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReportShareFriendItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ReportShareFriendItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendOnlineUIStateController;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendOnlineLongText;
    [AutoBind("./PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconImage;
    [AutoBind("./PlayerIconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconGreyImage;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_friendHeadFrameTransform;
    [AutoBind("./SelectToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_selectToggle;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendLevelText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendNameText;
    [AutoBind("./ServerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverText;
    private string m_userID;
    private UserSummary m_userSummy;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetFriendInfo;
    private static DelegateBridge __Hotfix_OnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_add_EventOnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_remove_EventOnSelectToggleValueChanged;

    [MethodImpl((MethodImplOptions) 32768)]
    public ReportShareFriendItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendInfo(UserSummary userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, string> EventOnSelectToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
