﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UIUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UIUtility
  {
    public const float PointerLongDownTime = 0.5f;
    public const float PointerClickTorrent = 0.02f;
    public const float PointerDragTorrent = 0.01f;
    private static Dictionary<string, string> m_translateTable;
    private const float MarginFixX = 88f;
    private const float MarginFixY = 40f;
    private static int s_DefaultPixelDragThreshold;
    private static int s_longFrameCountdown;
    private static float s_defaultMaximumDeltaTime;
    public static Color MyGreenColor;
    public static Color MyGrayColor;
    public static Color MyBlueColor;
    public const float MAX_DEBUG_REPORT_TIME = 10f;
    public const string STR_UPDATE_REPORT_DESC = "UploadLogByTouches";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Translate;
    private static DelegateBridge __Hotfix_LoadTranslateTable;
    private static DelegateBridge __Hotfix_WorldToLocalPosition;
    private static DelegateBridge __Hotfix_ScreenToLocalPosition;
    private static DelegateBridge __Hotfix_AddColorTag;
    private static DelegateBridge __Hotfix_AddSizeTag;
    private static DelegateBridge __Hotfix_ActivateLayer;
    private static DelegateBridge __Hotfix_IsLayerActive;
    private static DelegateBridge __Hotfix_ClickButton;
    private static DelegateBridge __Hotfix_ClickToggle;
    private static DelegateBridge __Hotfix_TimeSpanToString;
    private static DelegateBridge __Hotfix_TimeSpanToString2;
    private static DelegateBridge __Hotfix_TimeSpanToString3;
    private static DelegateBridge __Hotfix_DateTimeToString;
    private static DelegateBridge __Hotfix_TimeSpanToCountdownSecond;
    private static DelegateBridge __Hotfix_TimeSpanToString4;
    private static DelegateBridge __Hotfix_DateTimeToGMTString;
    private static DelegateBridge __Hotfix_SetDailyRewardCount;
    private static DelegateBridge __Hotfix_SetDailySweepCount;
    private static DelegateBridge __Hotfix_SetDailyChallengeCount;
    private static DelegateBridge __Hotfix_ResetTween;
    private static DelegateBridge __Hotfix_ReplayTween;
    private static DelegateBridge __Hotfix_ReversePlayTween;
    private static DelegateBridge __Hotfix_SetTweenFinished;
    private static DelegateBridge __Hotfix_RemoveTweenFinished;
    private static DelegateBridge __Hotfix_SetTweenIgnoreTimeScale;
    private static DelegateBridge __Hotfix_SetUIStateOpen;
    private static DelegateBridge __Hotfix_SetUIStateClose;
    private static DelegateBridge __Hotfix_SetUIStateOpenAndClose;
    private static DelegateBridge __Hotfix_SetUIState;
    private static DelegateBridge __Hotfix_GetUIStateCurrentStateName;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_ShowGameObjectChildrenByColor;
    private static DelegateBridge __Hotfix_SetGameObjectChildrenActiveCount;
    private static DelegateBridge __Hotfix_ReverseShowGameObjectChildrenByActive;
    private static DelegateBridge __Hotfix_GetHeroItemFrameNameByRank;
    private static DelegateBridge __Hotfix_GetGoodsFrameNameByRank;
    private static DelegateBridge __Hotfix_GetPropertyRatingImageName;
    private static DelegateBridge __Hotfix_GetGoodsName;
    private static DelegateBridge __Hotfix_GetGoodsRank;
    private static DelegateBridge __Hotfix_GetGoodsIconName;
    private static DelegateBridge __Hotfix_GetGoodsIconMaterialName;
    private static DelegateBridge __Hotfix_GetGoodsFrameName;
    private static DelegateBridge __Hotfix_IsGoodsHeroFragment;
    private static DelegateBridge __Hotfix_IsGoodsGoblin;
    private static DelegateBridge __Hotfix_GetGoodsDesc;
    private static DelegateBridge __Hotfix_GetGuildTitleText;
    private static DelegateBridge __Hotfix_IsRankSSR;
    private static DelegateBridge __Hotfix_GetBagItemAlchemyGold;
    private static DelegateBridge __Hotfix_GetBagItemDropID;
    private static DelegateBridge __Hotfix_GetBagItemDropDisplayCount;
    private static DelegateBridge __Hotfix_GoodTypeHaveID;
    private static DelegateBridge __Hotfix_AppendRandomDropRewardGoodsToList;
    private static DelegateBridge __Hotfix_GetGoodsCount;
    private static DelegateBridge __Hotfix_GetCurrencyCount;
    private static DelegateBridge __Hotfix_GetPlayerHeadIconImageName;
    private static DelegateBridge __Hotfix_GetPlayerSmallHeadIconImageName;
    private static DelegateBridge __Hotfix_GetPlayerRoundHeadIconImageName;
    private static DelegateBridge __Hotfix_GetPlayerHeadIconCharImageInfo;
    private static DelegateBridge __Hotfix_GetPlayerHeadFrameImageName;
    private static DelegateBridge __Hotfix_SetPlayerHeadFrame;
    private static DelegateBridge __Hotfix_GetBattlePlayerTagImageName;
    private static DelegateBridge __Hotfix_GetSoldierCurSkillDesc_1;
    private static DelegateBridge __Hotfix_GetSoldierCurSkillDesc_0;
    private static DelegateBridge __Hotfix_GetSkillIdFromEquipment;
    private static DelegateBridge __Hotfix_GetRankImage;
    private static DelegateBridge __Hotfix_GetPeakArenaPromoteImage;
    private static DelegateBridge __Hotfix_GetSoldierRankImage;
    private static DelegateBridge __Hotfix_GetHeroCharAssetPath;
    private static DelegateBridge __Hotfix_GetHeroModelAssetPath_1;
    private static DelegateBridge __Hotfix_GetSoldierModelAssetPath;
    private static DelegateBridge __Hotfix_GetHeroModelAssetPath_0;
    private static DelegateBridge __Hotfix_GetSelectedSoldierModelAssetPath;
    private static DelegateBridge __Hotfix_GetCharImageInfoRelatedToCharSkin_1;
    private static DelegateBridge __Hotfix_GetCharImageInfoRelatedToCharSkin_0;
    private static DelegateBridge __Hotfix_GetHeroInformationInfoRelatedToCharSkin_1;
    private static DelegateBridge __Hotfix_GetHeroInformationInfoRelatedToCharSkin_0;
    private static DelegateBridge __Hotfix_GetMemberOnlineText;
    private static DelegateBridge __Hotfix_GetSepAlphaTextureName;
    private static DelegateBridge __Hotfix_GetGameFunctionOpenMessage;
    private static DelegateBridge __Hotfix_GetLeaderboardRankingImageName;
    private static DelegateBridge __Hotfix_GetDescText;
    private static DelegateBridge __Hotfix_MarginAdjustHorizontal;
    private static DelegateBridge __Hotfix_MarginAdjustVertical;
    private static DelegateBridge __Hotfix_SetLongFrame;
    private static DelegateBridge __Hotfix_CheckLongFrame;
    private static DelegateBridge __Hotfix_TweenHorizontalScrollRecPosition;
    private static DelegateBridge __Hotfix_GetGiftItemPdsdkGoodType_0;
    private static DelegateBridge __Hotfix_GetGiftItemPdsdkGoodType_1;
    private static DelegateBridge __Hotfix_SetStoreCurrencyImage;
    private static DelegateBridge __Hotfix_GetInputFieldCharacterLimit;
    private static DelegateBridge __Hotfix_GetRichTextLength;
    private static DelegateBridge __Hotfix_FindUITaskWithType_1;
    private static DelegateBridge __Hotfix_FindUITaskWithType_0;
    private static DelegateBridge __Hotfix_IsUITaskRunning;
    private static DelegateBridge __Hotfix_PauseUITask;
    private static DelegateBridge __Hotfix_StopUITask;
    private static DelegateBridge __Hotfix_SetDefaultPixelDragThreshold;
    private static DelegateBridge __Hotfix_GetDefaultPixelDragThreshold;
    private static DelegateBridge __Hotfix_IsIosSubscribe_0;
    private static DelegateBridge __Hotfix_IsIosSubscribe_1;
    private static DelegateBridge __Hotfix_IsYYBChannel;
    private static DelegateBridge __Hotfix_IsZiLongChannel;
    private static DelegateBridge __Hotfix_IsOppoChannel;
    private static DelegateBridge __Hotfix_GetUIPrefabPath;
    private static DelegateBridge __Hotfix_CreateSubUI;
    private static DelegateBridge __Hotfix_Share;
    private static DelegateBridge __Hotfix_JumpToStore;
    private static DelegateBridge __Hotfix_LocalizedString;
    private static DelegateBridge __Hotfix_GetTextPreferredHeight;
    private static DelegateBridge __Hotfix_GetEquipMasterPropertyGoldIconName;
    private static DelegateBridge __Hotfix_GetEquipMasterPropertyIconName;
    private static DelegateBridge __Hotfix_GetEquipMasterPropertyGrayIconName;
    private static DelegateBridge __Hotfix_GetAllEquipMasterPropertyIconAssetPath;
    private static DelegateBridge __Hotfix_HasVoicePermission;

    [MethodImpl((MethodImplOptions) 32768)]
    public UIUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Translate(string srcStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LoadTranslateTable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2 WorldToLocalPosition(
      Vector3 p,
      Camera worldCam,
      RectTransform rt,
      Camera uiCam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2 ScreenToLocalPosition(Vector2 p, RectTransform rt, Camera uiCam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string AddColorTag(string txt, Color c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string AddSizeTag(string txt, int size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ActivateLayer(UIControllerBase controller, bool a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLayerActive(UIControllerBase controller)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClickButton(UIControllerBase controller, string buttonName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClickToggle(UIControllerBase controller, string toggleName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString2(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString3(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string DateTimeToString(DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int TimeSpanToCountdownSecond(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string TimeSpanToString4(TimeSpan timeSpan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string DateTimeToGMTString(DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDailyRewardCount(Text text, int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDailySweepCount(Text text, int count, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDailyChallengeCount(Text text, int restCount, int allCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ResetTween(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReplayTween(GameObject go, Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReversePlayTween(GameObject go, Action onFinished = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool SetTweenFinished(TweenMain[] tweens, Action onFinished)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void RemoveTweenFinished(TweenMain[] tweens)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetTweenIgnoreTimeScale(GameObject go, bool ignore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpen(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      bool disableInput = false,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateClose(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      bool disableInput = false,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIStateOpenAndClose(
      CommonUIStateController ctrl,
      string openStateName,
      string closeStateName,
      Action onEnd = null,
      bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetUIState(
      CommonUIStateController ctrl,
      string stateName,
      Action onEnd = null,
      bool disableInput = false,
      bool allowToRefreshSameState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetUIStateCurrentStateName(CommonUIStateController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PlayAnimation(GameObject go, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ShowGameObjectChildrenByColor(GameObject obj, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetGameObjectChildrenActiveCount(GameObject obj, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReverseShowGameObjectChildrenByActive(GameObject obj, int num)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroItemFrameNameByRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsFrameNameByRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPropertyRatingImageName(char rating)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetGoodsRank(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsIconName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsIconMaterialName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsFrameName(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsGoodsHeroFragment(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsGoodsGoblin(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGoodsDesc(GoodsType goodsType, int goodsId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGuildTitleText(GuildTitle title)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRankSSR(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetBagItemAlchemyGold(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetBagItemDropID(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetBagItemDropDisplayCount(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GoodTypeHaveID(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AppendRandomDropRewardGoodsToList(int dropId, List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetGoodsCount(GoodsType goodsType, int goodsId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetCurrencyCount(GoodsType currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerHeadIconImageName(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerSmallHeadIconImageName(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerRoundHeadIconImageName(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ConfigDataCharImageInfo GetPlayerHeadIconCharImageInfo(
      int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPlayerHeadFrameImageName(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject SetPlayerHeadFrame(
      Transform parent,
      int headFrameId,
      bool ingoreDefaultHeadFame = true,
      string uiState = "Normal")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetBattlePlayerTagImageName(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierCurSkillDesc(
      ConfigDataSoldierInfo soldierInfo,
      List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierCurSkillDesc(ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetSkillIdFromEquipment(
      ConfigDataEquipmentInfo equipmentInfo,
      int equipmentLevel,
      ref string skillLv)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetRankImage(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetPeakArenaPromoteImage(int round, bool isChampion = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierRankImage(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroCharAssetPath(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroModelAssetPath(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSoldierModelAssetPath(Hero hero, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetHeroModelAssetPath(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSelectedSoldierModelAssetPath(BattleHero hero, int team = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataCharImageInfo GetCharImageInfoRelatedToCharSkin(
      int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataCharImageInfo GetCharImageInfoRelatedToCharSkin(
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataHeroInformationInfo GetHeroInformationInfoRelatedToCharSkin(
      int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataHeroInformationInfo GetHeroInformationInfoRelatedToCharSkin(
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetMemberOnlineText(bool isOnline, DateTime logountTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetSepAlphaTextureName(string colorTextureName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGameFunctionOpenMessage(GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLeaderboardRankingImageName(int ranking)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetDescText(string format, List<string> args)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MarginAdjustHorizontal(RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool MarginAdjustVertical(RectTransform rt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLongFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckLongFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator TweenHorizontalScrollRecPosition(
      ScrollRect scrollRect,
      float from,
      float to,
      float duration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PDSDKGoodType GetGiftItemPdsdkGoodType(
      ConfigDataGiftStoreItemInfo giftInfo,
      bool isfristBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PDSDKGoodType GetGiftItemPdsdkGoodType(
      double firstPrice,
      double normalPrice,
      bool isfristBuy,
      bool IsAppleSubscribe)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetStoreCurrencyImage(Image image, string currencyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetInputFieldCharacterLimit(int limit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRichTextLength(string richText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UITaskBase FindUITaskWithType(System.Type taskType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindUITaskWithType<T>() where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsUITaskRunning(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PauseUITask(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StopUITask(System.Type type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetDefaultPixelDragThreshold(int t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetDefaultPixelDragThreshold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsIosSubscribe(ConfigDataGiftStoreItemInfo giftItemInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsIosSubscribe(bool isSubscribe)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsYYBChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsZiLongChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsOppoChannel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetUIPrefabPath<T>() where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T CreateSubUI<T>(Transform parent) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Share(int sharePlatform, int heroId = 0, int archiveId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void JumpToStore(UIIntent intent, StoreId storeId, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LocalizedString(GameObject goRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float GetTextPreferredHeight(string str, Text text, float maxWidth)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetEquipMasterPropertyGoldIconName(PropertyModifyType propertyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetEquipMasterPropertyIconName(PropertyModifyType propertyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetEquipMasterPropertyGrayIconName(PropertyModifyType propertyType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetAllEquipMasterPropertyIconAssetPath()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HasVoicePermission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static UIUtility()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
