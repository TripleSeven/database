﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PDSDKGood
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.UI
{
  public struct PDSDKGood
  {
    public string m_ID;
    public string m_registerID;
    public string m_name;
    public double m_price;
    public PDSDKGoodType m_type;
    public string m_desc;
    public string m_disCurrency;
    public double m_disPrice;
  }
}
