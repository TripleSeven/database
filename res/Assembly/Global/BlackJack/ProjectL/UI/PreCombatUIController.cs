﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PreCombatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PreCombatUIController : UIControllerBase
  {
    [AutoBind("./BackgroundImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./BattlePreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_battlePreviewStateCtrl;
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./Margin/TerrainInfo0/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0TerrainText;
    [AutoBind("./Margin/TerrainInfo0/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0TerrainDefText;
    [AutoBind("./Margin/TerrainInfo0/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0TerrainImage;
    [AutoBind("./Margin/Actor0", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0UIStateController;
    [AutoBind("./Margin/Actor0/CharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0CharUIStateController;
    [AutoBind("./Margin/Actor0/CharPanel/CharIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0CharIcon;
    [AutoBind("./Margin/Actor0/CharInfoGroup/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0SkillGo;
    [AutoBind("./Margin/Actor0/CharInfoGroup/SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0SkillIconImage;
    [AutoBind("./Margin/Actor0/CharInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0TypeIcon;
    [AutoBind("./Margin/Actor0/CharInfoGroup/HpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0HpText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/HpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0HpProgressBar;
    [AutoBind("./Margin/Actor0/CharInfoGroup/HpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0HeroHpFxGameObeject;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0Proprety1NameText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0Proprety1ValueText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0Proprety2NameText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/Proprety2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0Proprety2ValueText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0NameText;
    [AutoBind("./Margin/Actor0/CharInfoGroup/LvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0LvValueText;
    [AutoBind("./Margin/Actor0/SituationGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0SituationGroup;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0SoldierGroupGameObject;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0SoldierTypeIcon;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0SoldierNameText;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/SoldierHpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0SoldierHpProgressBar;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/SoldierHpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0SoldierHpText;
    [AutoBind("./Margin/Actor0/SoldierInfoGroup/SoldierHpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0SoldierHpFxGameObeject;
    [AutoBind("./Margin/Actor0/Damage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0DamageUIStateController;
    [AutoBind("./Margin/Actor0/Damage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0DamageText;
    [AutoBind("./Margin/Actor0/Damage/CriticalText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0DamageCriticalText;
    [AutoBind("./Margin/Actor0/PassiveSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor0PassiveSkillUIStateController;
    [AutoBind("./Margin/Actor0/PassiveSkill/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0PassiveSkillIconImage;
    [AutoBind("./Margin/Actor0/PassiveSkill/TalentIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0PassiveSkillTalentIconImage;
    [AutoBind("./Margin/Actor0/PassiveSkill/EquipIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor0PassiveSkillEquipIconGameObject;
    [AutoBind("./Margin/Actor0/PassiveSkill/EquipIconImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor0PassiveSkillEquipIconImage;
    [AutoBind("./Margin/Actor0/PassiveSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor0PassiveSkillNameText;
    [AutoBind("./Margin/TerrainInfo1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1TerrainText;
    [AutoBind("./Margin/TerrainInfo1/Def/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1TerrainDefText;
    [AutoBind("./Margin/TerrainInfo1/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1TerrainImage;
    [AutoBind("./Margin/Actor1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1UIStateController;
    [AutoBind("./Margin/Actor1/CharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1CharUIStateController;
    [AutoBind("./Margin/Actor1/CharPanel/CharIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1CharIcon;
    [AutoBind("./Margin/Actor1/CharInfoGroup/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1SkillGo;
    [AutoBind("./Margin/Actor1/CharInfoGroup/SkillGroup/SkillIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1SkillIconImage;
    [AutoBind("./Margin/Actor1/CharInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1TypeIcon;
    [AutoBind("./Margin/Actor1/CharInfoGroup/HpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1HpText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/HpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1HpProgressBar;
    [AutoBind("./Margin/Actor1/CharInfoGroup/HpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1HeroHpFxGameObeject;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety1", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1Proprety1NameText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety1/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1Proprety1ValueText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety2", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1Proprety2NameText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/Proprety2/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1Proprety2ValueText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1NameText;
    [AutoBind("./Margin/Actor1/CharInfoGroup/LvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1LvValueText;
    [AutoBind("./Margin/Actor1/SituationGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1SituationGroup;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1SoldierGroupGameObject;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/TypeIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1SoldierTypeIcon;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1SoldierNameText;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/SoldierHpGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1SoldierHpProgressBar;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/SoldierHpGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1SoldierHpText;
    [AutoBind("./Margin/Actor1/SoldierInfoGroup/SoldierHpGroup/Fx", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1SoldierHpFxGameObeject;
    [AutoBind("./Margin/Actor1/Damage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1DamageUIStateController;
    [AutoBind("./Margin/Actor1/Damage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1DamageText;
    [AutoBind("./Margin/Actor1/Damage/CriticalText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1DamageCriticalText;
    [AutoBind("./Margin/Actor1/PassiveSkill", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_actor1PassiveSkillUIStateController;
    [AutoBind("./Margin/Actor1/PassiveSkill/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1PassiveSkillIconImage;
    [AutoBind("./Margin/Actor1/PassiveSkill/TalentIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1PassiveSkillTalentIconImage;
    [AutoBind("./Margin/Actor1/PassiveSkill/EquipIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_actor1PassiveSkillEquipIconGameObject;
    [AutoBind("./Margin/Actor1/PassiveSkill/EquipIconImage/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_actor1PassiveSkillEquipIconImage;
    [AutoBind("./Margin/Actor1/PassiveSkill/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_actor1PassiveSkillNameText;
    [AutoBind("./ButtonGroup/OkButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_okButton;
    [AutoBind("./ButtonGroup/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_cancelButton;
    private int m_armyRelationValue;
    private int m_totalDamage0;
    private int m_totalDamage1;
    private FxPlayer m_fxPlayer;
    private bool m_isOpened;
    private bool m_isFastCombat;
    private HeroPropertyComputer m_propertyComputer;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_IsOpened;
    private static DelegateBridge __Hotfix_IsFastCombat;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_ShowOkCancelButton;
    private static DelegateBridge __Hotfix_SetBattleActorInfo;
    private static DelegateBridge __Hotfix_SetHeroHp;
    private static DelegateBridge __Hotfix_SetSoldierHp;
    private static DelegateBridge __Hotfix_OpenAndShowFastCombat;
    private static DelegateBridge __Hotfix_Co_OpenAndFastCombat;
    private static DelegateBridge __Hotfix_ShowFastCombat;
    private static DelegateBridge __Hotfix_Co_FastCombat;
    private static DelegateBridge __Hotfix_Co_PlayDamage_0;
    private static DelegateBridge __Hotfix_Co_PlayDamage_1;
    private static DelegateBridge __Hotfix_PlayDamageFx;
    private static DelegateBridge __Hotfix_OnActorPassiveSkill;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_OnOkButtonClick;
    private static DelegateBridge __Hotfix_OnCancelButtonClick;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnOk;
    private static DelegateBridge __Hotfix_remove_EventOnOk;
    private static DelegateBridge __Hotfix_add_EventOnCancel;
    private static DelegateBridge __Hotfix_remove_EventOnCancel;
    private static DelegateBridge __Hotfix_add_EventOnStop;
    private static DelegateBridge __Hotfix_remove_EventOnStop;

    [MethodImpl((MethodImplOptions) 32768)]
    private PreCombatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(FxPlayer fxPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFastCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowOkCancelButton(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleActorInfo(
      BattleActor a0,
      BattleActor a1,
      ConfigDataTerrainInfo terrain0,
      ConfigDataTerrainInfo terrain1,
      bool isMagicAttack0,
      bool isMagicAttack1,
      int armyRelationValue,
      ConfigDataSkillInfo attackerSkillInfo,
      int attackerSide,
      bool isPvp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroHp(int side, int hp, int hpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierHp(int side, int hp, int hpMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenAndShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_OpenAndFastCombat(
      FastCombatActorInfo a0,
      FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_FastCombat(FastCombatActorInfo a0, FastCombatActorInfo a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayDamage(int side, FastCombatActorInfo a)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_PlayDamage(
      GameObject fxParent,
      int side,
      int beforeHp,
      int afterHp,
      int hpMax,
      bool isHero,
      int totalDamage,
      bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayDamageFx(
      GameObject fxParent,
      int side,
      int hp,
      int hpMax,
      int damage,
      bool isLargeFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorPassiveSkill(int side, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOkButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnOk
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnStop
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
