﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentDepotUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipmentDepotUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./PlayerResource/Gold/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_goldText;
    [AutoBind("./PlayerResource/Gold/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goldAddButton;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private LoopVerticalScrollRect m_loopScrollView;
    [AutoBind("./EquipmentList/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private EasyObjectPool m_listItemPool;
    [AutoBind("./EquipmentList/ListScrollView/ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listScrollViewItemRoot;
    [AutoBind("./EquipmentList/Filter/SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortButton;
    [AutoBind("./EquipmentList/Filter/SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_filterSortTypeText;
    [AutoBind("./EquipmentList/Filter/SortOrderButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_filterSortOrderButton;
    [AutoBind("./EquipmentList/Filter/SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGo;
    [AutoBind("./EquipmentList/Filter/SortTypes/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_filterSortTypesGridLayout;
    [AutoBind("./EquipmentList/NoItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_listNotItemGo;
    [AutoBind("./Desc/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descTitleText;
    [AutoBind("./Desc/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIcon;
    [AutoBind("./Desc/IconBg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descIconBg;
    [AutoBind("./Desc/SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSSREffect;
    [AutoBind("./Desc/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descStarGroup;
    [AutoBind("./Desc/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descLvText;
    [AutoBind("./Desc/ExpText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descExpText;
    [AutoBind("./Desc/ProgressImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descProgressImage;
    [AutoBind("./Desc/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descEquipLimitContent;
    [AutoBind("./Desc/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillContent;
    [AutoBind("./Desc/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descSkillContentStateCtrl;
    [AutoBind("./Desc/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillNameText;
    [AutoBind("./Desc/SkillContent/LvText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillLvText;
    [AutoBind("./Desc/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descUnlockCoditionText;
    [AutoBind("./Desc/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillOwnerText;
    [AutoBind("./Desc/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descSkillOwnerBGImage;
    [AutoBind("./Desc/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descSkillDescText;
    [AutoBind("./Desc/NotEquipSkillTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descNotEquipSkillTip;
    [AutoBind("./Desc/LockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descLockButton;
    [AutoBind("./Desc/EquipButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descEquipButton;
    [AutoBind("./Desc/UnloadButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descUnloadButton;
    [AutoBind("./Desc/PropGroup/PropContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropContent;
    [AutoBind("./Desc/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropATGo;
    [AutoBind("./Desc/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropATValueText;
    [AutoBind("./Desc/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDFGo;
    [AutoBind("./Desc/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDFValueText;
    [AutoBind("./Desc/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropHPGo;
    [AutoBind("./Desc/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropHPValueText;
    [AutoBind("./Desc/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagiccGo;
    [AutoBind("./Desc/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicValueText;
    [AutoBind("./Desc/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropMagicDFGo;
    [AutoBind("./Desc/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropMagicDFValueText;
    [AutoBind("./Desc/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropDexGo;
    [AutoBind("./Desc/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropDexValueText;
    [AutoBind("./Desc/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropGroupStateCtrl;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/PropertyGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_descPropEnchantmentGroup;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance/RuneIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_descPropEnchantmentGroupRuneIconImage;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descPropEnchantmentGroupRuneNameText;
    [AutoBind("./Desc/PropGroup/EnchantmentGroup/Resonance", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_descPropEnchantmentGroupRuneStateCtrl;
    [AutoBind("./Desc/PropertyComparison", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_PropertyComparisonGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescPropGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Hp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescHpGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Hp/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescHpOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Hp/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescHpNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescATGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/AT/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescATOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/AT/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescATNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescDFGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/DF/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDFOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/DF/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDFNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescMagicDFGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/MagicDF/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicDFOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/MagicDF/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicDFNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescMagicGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Magic/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Magic/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescMagicNewText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_allDescDexGo;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Dex/FirstNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDexOldText;
    [AutoBind("./Desc/PropertyComparison/PropGroup/Dex/LastValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_allDescDexNewText;
    [AutoBind("./BattlePower", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_battlePowerGo;
    [AutoBind("./BattlePower/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_battlePowerValueText;
    [AutoBind("./ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_confirmPanelStateCtrl;
    [AutoBind("./ConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmPanelCancelButton;
    [AutoBind("./ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_confirmPanelConfirmButton;
    private int m_isAscend;
    private EquipmentDepotUIController.EquipmentSortTypeState m_curEquipmentSortType;
    private Hero m_hero;
    private int m_slot;
    private int m_canUseEquipmentCount;
    private ulong m_curEquipmentInstanceId;
    private bool m_isFirstIn;
    private List<EquipmentBagItem> m_equipItemCache;
    private List<EquipmentDepotListItemUIController> m_equipmentDepotCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSortTypePanel;
    private static DelegateBridge __Hotfix_InitLoopScrollRect;
    private static DelegateBridge __Hotfix_OnPoolObjectCreated;
    private static DelegateBridge __Hotfix_UpdateEquipmentDepotView;
    private static DelegateBridge __Hotfix_CreateEquipmentList;
    private static DelegateBridge __Hotfix_SortEquipmentListByType;
    private static DelegateBridge __Hotfix_DefaultEquipmentItemComparer;
    private static DelegateBridge __Hotfix_CollectEquipmentPropValueAndSort;
    private static DelegateBridge __Hotfix_OnEquipmentListItemClick;
    private static DelegateBridge __Hotfix_OnEquipmentListItemNeedFill;
    private static DelegateBridge __Hotfix_OnFilterSortButtonClick;
    private static DelegateBridge __Hotfix_OnCloseFilterSortTypeGo;
    private static DelegateBridge __Hotfix_OnFilterSortOrderButtonClick;
    private static DelegateBridge __Hotfix_OnFilterTypeButtonClick;
    private static DelegateBridge __Hotfix_SetEquipmentItemDesc;
    private static DelegateBridge __Hotfix_SetEquipmentComparison;
    private static DelegateBridge __Hotfix_ResetEquipmentComparisonPropValue;
    private static DelegateBridge __Hotfix_SetEquipmentComparisonProp;
    private static DelegateBridge __Hotfix_SetPropItemColor;
    private static DelegateBridge __Hotfix_SetPropItems;
    private static DelegateBridge __Hotfix_OnDescEquipButtonClick;
    private static DelegateBridge __Hotfix_OnConfirmPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnConfirmPanelCancelButtonClick;
    private static DelegateBridge __Hotfix_OnDescUnloadButtonClick;
    private static DelegateBridge __Hotfix_OnDescLockButtonClick;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_ResetScrollViewToTop;
    private static DelegateBridge __Hotfix_GetFirstEquipmentGoInListForUserGuide;
    private static DelegateBridge __Hotfix_ClickEquipmentListItemForUserGuide;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGoldAddButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnTakeOffEquipment;
    private static DelegateBridge __Hotfix_remove_EventOnTakeOffEquipment;
    private static DelegateBridge __Hotfix_add_EventOnLockButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnLockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnEquipButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnEquipButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public EquipmentDepotUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitSortTypePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoopScrollRect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEquipmentDepotView(Hero hero, int slot, ulong equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateEquipmentList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortEquipmentListByType(List<EquipmentBagItem> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int DefaultEquipmentItemComparer(EquipmentBagItem e1, EquipmentBagItem e2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectEquipmentPropValueAndSort(
      List<EquipmentBagItem> list,
      PropertyModifyType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentListItemNeedFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseFilterSortTypeGo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterSortOrderButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFilterTypeButtonClick(EquipmentSortItemUIController ctrl, bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentItemDesc(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentComparison(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetEquipmentComparisonPropValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentComparisonProp(PropertyModifyType type, int value, bool isDressed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItemColor(Text oldText, Text newText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropItems(PropertyModifyType type, int value, int addValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescEquipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConfirmPanelCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescUnloadButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoldAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetScrollViewToTop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetFirstEquipmentGoInListForUserGuide()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickEquipmentListItemForUserGuide(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGoldAddButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnTakeOffEquipment
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int> EventOnLockButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, ulong, int> EventOnEquipButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum EquipmentSortTypeState
    {
      Lv,
      StarLv,
      Rank,
      Hp,
      AT,
      DF,
      MagicAT,
      MagicDF,
      Dex,
    }
  }
}
