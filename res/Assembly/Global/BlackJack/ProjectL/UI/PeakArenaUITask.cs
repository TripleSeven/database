﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public PeakArenaUIController m_peakArenaUIController;
    private PlayerResourceUIController m_playerResourceUIController;
    public PeakArenaKnockoutTimeUIController m_knockoutTimeUIController;
    public PeakArenaBuyJettonUIController m_buyJettonUIController;
    public PeakArenaUseJettonUIController m_useJettonUIController;
    private int m_nowSeconds;
    private int m_knockoutGroupId;
    private PeakArenaPanelType m_peakPanelType;
    private DateTime m_matchingUIBeginTime;
    private DateTime m_matchingReqSendTime;
    private RealTimePVPMode m_matchingBattleMode;
    private bool m_canFlushOfflineTopRankPlayers;
    private PeakArenaPlayOffInfoGetNetTask m_curNetTask;
    private float m_lastUpdateTime;
    public const string ParamsKey_knockoutGroupId = "KnockoutGroupId";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_StartUITaskInteral;
    private static DelegateBridge __Hotfix_SendGetSeasonInfoNetTask;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_InitPeakArenaUIController;
    private static DelegateBridge __Hotfix_UninitPeakArenaUIController;
    private static DelegateBridge __Hotfix_RegisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_UnregisterPlayerContextEvents;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_SendSeasonGetNetTaskInClashState;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_LoadRes;
    private static DelegateBridge __Hotfix_UpdatePeakArenaPopWindow;
    private static DelegateBridge __Hotfix_UpdatePeakMatchingTime;
    private static DelegateBridge __Hotfix_StartRealTimePVPWaitingForOpponentNetTask;
    private static DelegateBridge __Hotfix_StartMatching;
    private static DelegateBridge __Hotfix_PlayerContext_OnRealTimePVPMatchupNtf;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddCrystal;
    private static DelegateBridge __Hotfix_PlayerResourceUIController_OnAddGold;
    private static DelegateBridge __Hotfix_OnAddJettonTicketClick;
    private static DelegateBridge __Hotfix_OnBuyJettonSuccess;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnReturn;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnShowHelp;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnManagementTeam;
    private static DelegateBridge __Hotfix_PeakArenaUIController_EventOnShowPeakPanel;
    private static DelegateBridge __Hotfix_GetPeakArenaWeekFirstWinRewardsAndShow;
    private static DelegateBridge __Hotfix_OnGetRewardGoodsUITaskClose;
    private static DelegateBridge __Hotfix_FlushOfflineTopRankPlayers;
    private static DelegateBridge __Hotfix_GetSeasonKnockInfoInfo;
    private static DelegateBridge __Hotfix_GetPeakArenaBattleReports;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnPeakClashCasualChallengeButtonClick;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnPeakClashLadderChallengeButtonClick;
    private static DelegateBridge __Hotfix_StartPeakArenaPlayOffBattleSetReadyNetTask;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnMatchingCancel;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnShareBattleReport;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnReportNameChange;
    private static DelegateBridge __Hotfix_PeakArenaUIController_OnReportReply;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartUITask(Action<PeakArenaUITask> OnFinish = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartUITaskInteral(Action<PeakArenaUITask> OnFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void SendGetSeasonInfoNetTask(Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPeakArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitPeakArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterPlayerContextEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendSeasonGetNetTaskInClashState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadRes(List<string> assetList, Action onComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakArenaPopWindow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePeakMatchingTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartRealTimePVPWaitingForOpponentNetTask(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartMatching(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnRealTimePVPMatchupNtf(int result, RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerResourceUIController_OnAddGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddJettonTicketClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyJettonSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnShowHelp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnManagementTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_EventOnShowPeakPanel(PeakArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetPeakArenaWeekFirstWinRewardsAndShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRewardGoodsUITaskClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushOfflineTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetSeasonKnockInfoInfo(Action onFinish = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetPeakArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnPeakClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnPeakClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartPeakArenaPlayOffBattleSetReadyNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnMatchingCancel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnShareBattleReport(
      PeakArenaBattleReportType type,
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnReportNameChange(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PeakArenaUIController_OnReportReply(ulong reportId)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
