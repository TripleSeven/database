﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.OpenServiceActivityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class OpenServiceActivityUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_openServiceActivityStateCtrl;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./CharGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./Detail/TitleToggleGroup/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_daysButtonContent;
    [AutoBind("./Detail/QuestGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_missionsScrollRect;
    [AutoBind("./Detail/QuestGroup/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missionsScrollViewContent;
    [AutoBind("./Prefab/MissonItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_missonItemPrefab;
    [AutoBind("./Detail/UnderCountGroup/CountGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_integralContentGroup;
    [AutoBind("./Detail/UnderCountGroup/NowCountGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralGroupCountValueText;
    [AutoBind("./Detail/ResidueTimeGroup/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_residueTimeValueText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_curSelectday;
    private HeroCharUIController m_heroCharUIController;
    private DayButtonUIController m_curSelectDayButtonCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInOpenServiceActivity;
    private static DelegateBridge __Hotfix_InitDayButtons;
    private static DelegateBridge __Hotfix_OnDayButtonClick;
    private static DelegateBridge __Hotfix_InitMissionPanel;
    private static DelegateBridge __Hotfix_OnMissionGotoButtonClick;
    private static DelegateBridge __Hotfix_OnMissionGetButtonClick;
    private static DelegateBridge __Hotfix_InitIntegralPanel;
    private static DelegateBridge __Hotfix_OnIntegralGoodsClick;
    private static DelegateBridge __Hotfix_SetResidueTime;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnMissionGoto;
    private static DelegateBridge __Hotfix_remove_EventOnMissionGoto;
    private static DelegateBridge __Hotfix_add_EventOnMissionGet;
    private static DelegateBridge __Hotfix_remove_EventOnMissionGet;
    private static DelegateBridge __Hotfix_add_EventOnIntegralGoodsClick;
    private static DelegateBridge __Hotfix_remove_EventOnIntegralGoodsClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInOpenServiceActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDayButtons()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDayButtonClick(DayButtonUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitMissionPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionGotoButtonClick(GetPathData pathInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionGetButtonClick(OpenServiceMissonUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitIntegralPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnIntegralGoodsClick(IntegralGoodsUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetResidueTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GetPathData> EventOnMissionGoto
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OpenServiceMissonUIController> EventOnMissionGet
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<IntegralGoodsUIController> EventOnIntegralGoodsClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
