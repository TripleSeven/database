﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RiftLevelButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RiftLevelButton : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./NameNum/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameNumText;
    [AutoBind("./ChallengeCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeCountText;
    [AutoBind("./StarsandCup/Star1/Star", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star1GameObject;
    [AutoBind("./StarsandCup/Star2/Star", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star2GameObject;
    [AutoBind("./StarsandCup/Star3/Star", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star3GameObject;
    [AutoBind("./StarsandCup/Cup/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_achievementCountText;
    [AutoBind("./StoryImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./StoryNormalImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image1;
    private ConfigDataRiftLevelInfo m_riftLevelInfo;
    private RiftLevelStatus m_status;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetRiftLevelInfo;
    private static DelegateBridge __Hotfix_GetRiftLevel;
    private static DelegateBridge __Hotfix_SetStatus;
    private static DelegateBridge __Hotfix_GetStatus;
    private static DelegateBridge __Hotfix_SetStar;
    private static DelegateBridge __Hotfix_SetChallengeCount;
    private static DelegateBridge __Hotfix_SetAchievementCount;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRiftLevelInfo(ConfigDataRiftLevelInfo riftLevelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataRiftLevelInfo GetRiftLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(RiftLevelStatus status, bool isNew, bool isClear, bool isAllTreasureGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelStatus GetStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStar(int star)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengeCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAchievementCount(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RiftLevelButton> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
