﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UIGroup4LowMem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.UI
{
  public enum UIGroup4LowMem
  {
    Entry,
    Login,
    ReLogin,
    Preload,
    Common,
    World,
    Battle,
    Hero,
    SelectCard,
    Mission,
    Drill,
    Fetters,
    Rift,
    Arena,
    Uncharted,
    Activity,
    Friend,
    Guild,
  }
}
