﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FixedStoreBuyStoreItemNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class FixedStoreBuyStoreItemNetTask : UINetTask
  {
    private int m_fixedStoreID;
    private int m_fixedStoreItemGoodsID;
    private int m_selfChooseItemIndex;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnFixedStoreBuyStoreItemAck;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreBuyStoreItemNetTask(
      int fixedStoreID,
      int fixedStoreItemGoodsID,
      int selfChoooseItemIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFixedStoreBuyStoreItemAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
