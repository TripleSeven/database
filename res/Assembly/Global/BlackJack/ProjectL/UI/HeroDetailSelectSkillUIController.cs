﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailSelectSkillUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailSelectSkillUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoSelectSkillPanel;
    [AutoBind("./ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoCloseSelectSkillButton;
    [AutoBind("./SelectSkill/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoSelectSkillsCost;
    [AutoBind("./SelectSkill/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoSelectSkillsContent;
    [AutoBind("./SkillItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoAllSkillsContent;
    [AutoBind("./SaveButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSaveSelectSkillsButton;
    [AutoBind("./SkillItemDetailPanel/CommonSkillDesc/Lay/FrameImage/LoadButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSkillDescLoadButton;
    [AutoBind("./SkillItemDetailPanel/CommonSkillDesc/Lay/FrameImage/UnLoadButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSkillDescUnLoadButton;
    [AutoBind("./SkillItemDetailPanel/CommonSkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoSkillDescGo;
    private Hero m_hero;
    private List<int> m_curSelectSkillIds;
    private HeroSkillItemUIController m_curSelectedSkillCtrl;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInSelectSkillState;
    private static DelegateBridge __Hotfix_ShowSelectSkillContent;
    private static DelegateBridge __Hotfix_OnSkillItemClick;
    private static DelegateBridge __Hotfix_OnLoadOrUnLoadButtonClick;
    private static DelegateBridge __Hotfix_OnSaveSelectSkillsButtonClick;
    private static DelegateBridge __Hotfix_CloseSkillDesc;
    private static DelegateBridge __Hotfix_CloseSelectSkillPanel;
    private static DelegateBridge __Hotfix_CalcTotalCostFromSkillList;
    private static DelegateBridge __Hotfix_add_EventOnHeroSkillsSelect;
    private static DelegateBridge __Hotfix_remove_EventOnHeroSkillsSelect;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInSelectSkillState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSelectSkillContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLoadOrUnLoadButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSaveSelectSkillsButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSkillDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseSelectSkillPanel(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CalcTotalCostFromSkillList(List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, List<int>, bool> EventOnHeroSkillsSelect
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
