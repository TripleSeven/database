﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroBreakEffectUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroBreakEffectUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroCharGameObject;
    [AutoBind("./Char/0/U_BreakSuccess", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroCharBreakSuccessEffect;
    [AutoBind("./ClickScreenContinue", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroBreakEffectReturnButton;
    [AutoBind("./BreakSuccessInfoPanel/Detail/Hp/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoHpText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/Hp/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoHpAddText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/AT/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoATText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/AT/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoATAddText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/Magic/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoMagicText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/Magic/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoMagicAddText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/DF/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoDFText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/DF/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoDFAddText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/MagicDF/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoMagicDFText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/MagicDF/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoMagicDFAddText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/Dex/NowNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoDexText;
    [AutoBind("./BreakSuccessInfoPanel/Detail/Dex/AddNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroBreakSuccessInfoDexAddText;
    [AutoBind("./UnderGroup/StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStarGroup;
    [AutoBind("./BigStar/BigStarEffect/U_StarGold_3.8", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigStarEffect;
    [AutoBind("./BigStar/U_StarGold_trail", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_bigStarTrailEffect;
    [AutoBind("./TalentLevelUpPanel/FrameImage/Talent1/BgImage/InfoGroup/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_talent1Icon;
    [AutoBind("./TalentLevelUpPanel/FrameImage/Talent1/BgImage/InfoGroup/Desc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talent1NameText;
    [AutoBind("./TalentLevelUpPanel/FrameImage/Talent1/BgImage/InfoGroup/Desc/DescScrollView/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talent1DescText;
    [AutoBind("./TalentLevelUpPanel/FrameImage/Talent2/BgImage/InfoGroup/Icon/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_talent2Icon;
    [AutoBind("./TalentLevelUpPanel/FrameImage/Talent2/BgImage/InfoGroup/Desc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talent2NameText;
    [AutoBind("./TalentLevelUpPanel/FrameImage/Talent2/BgImage/InfoGroup/Desc/DescScrollView/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_talent2DescText;
    private Hero m_hero;
    private HeroCharUIController m_heroCharUIController;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_HeroBreakEffectUpdateView;
    private static DelegateBridge __Hotfix_SetBreakHeroInfo;
    private static DelegateBridge __Hotfix_ShowStarEffect;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HeroBreakEffectUpdateView(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBreakHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowStarEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
