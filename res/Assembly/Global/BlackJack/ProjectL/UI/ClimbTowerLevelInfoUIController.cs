﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClimbTowerLevelInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ClimbTowerLevelInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Panel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Panel/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_image;
    [AutoBind("./Panel/Hard/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_hardText;
    [AutoBind("./Panel/StartButton/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_costEnergyText;
    [AutoBind("./Panel/WinCondition/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_winConditionGroupTransform;
    [AutoBind("./Panel/Rule/Scrollrect/Viewport/ConditionGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_ruleConditionGroupTransform;
    [AutoBind("./Panel/RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rewardGroupTransform;
    [AutoBind("./RecommendPanel/HeroScrollView/Viewport/HeroGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_recommendHeroGroupTransform;
    [AutoBind("./RecommendPanel/InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_recommendHeroDescText;
    [AutoBind("./Panel/StartButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_startButton;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/ConditionText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_conditionPrefab;
    [AutoBind("./Prefabs/RecommendHeroItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_recommendHeroItemPrefab;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SetTowerLevel;
    private static DelegateBridge __Hotfix_SetRecommendHeros;
    private static DelegateBridge __Hotfix_SetConditions;
    private static DelegateBridge __Hotfix_AddCondition;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnStartButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStartBattle;
    private static DelegateBridge __Hotfix_remove_EventOnStartBattle;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    private ClimbTowerLevelInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(bool hasRecommendHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTowerLevel(
      ConfigDataTowerFloorInfo floorInfo,
      ConfigDataTowerLevelInfo levelInfo,
      ConfigDataTowerBattleRuleInfo ruleInfo,
      ConfigDataTowerBonusHeroGroupInfo bonusHeroGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRecommendHeros(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetConditions(Transform parent, string conditionStrs, int stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCondition(Transform parent, string str, int stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnStartBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
