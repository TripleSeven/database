﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DrillUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./FastMaxButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_fastMaxButton;
    [AutoBind("./TrainingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingStateCtrl;
    [AutoBind("./TrainingPanel/LeftToggleGroup/Toggles", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trainingPanelLeftToggleGroup;
    [AutoBind("./TrainingPanel/DetailPanel/TitleInfoGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelTitleInfoLvValueText;
    [AutoBind("./TrainingPanel/DetailPanel/TitleInfoGroup/ExpValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelTitleInfoExpValueText;
    [AutoBind("./TrainingPanel/DetailPanel/TitleInfoGroup/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_trainingPanelTitleInfoProgressBar;
    [AutoBind("./TrainingPanel/DetailPanel/TrainingEventScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trainingPanelTrainingEventScrollViewContent;
    [AutoBind("./TrainingPanel/DetailPanel/TrainingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_trainingPanelTrainingButton;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupHPAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupHPAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupAttackAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupAttackAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefenseAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupDefenseAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefenseAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupDefenseAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFAdd", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupMagicDFAddStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFAdd/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupMagicDFAddValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPPer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupHPPerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/HPPer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupHPPerValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackPer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupAttackPerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/AttackPer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupAttackPerValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefensePer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupDefensePerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/DefensePer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupDefensePerValue;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFPer", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_trainingPanelPropretyGroupMagicDFPerStateCtrl;
    [AutoBind("./TrainingPanel/DetailPanel/PropretyGroup/Detail/MagicDFPer/Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trainingPanelPropretyGroupMagicDFPerValue;
    [AutoBind("./Teaching", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_teachingStateCtrl;
    [AutoBind("./Teaching", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingChar;
    [AutoBind("./Teaching/Detail/Team1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingDetailTeam1;
    [AutoBind("./Teaching/Detail/Team2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingDetailTeam2;
    [AutoBind("./Teaching/Detail/Team3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingDetailTeam3;
    [AutoBind("./Teaching/TodayReward/IconGroup/ArmyIcon1", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teachingTodayRewardArmyIcon1;
    [AutoBind("./Teaching/TodayReward/IconGroup/ArmyIcon2", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_teachingTodayRewardArmyIcon2;
    [AutoBind("./Margin/RightToggle/TrainingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_trainingToggle;
    [AutoBind("./Margin/RightToggle/TeachingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_teachingToggle;
    [AutoBind("./Margin/RightToggle/TeachingToggle/Click/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingToggleClickRedMark;
    [AutoBind("./Margin/RightToggle/TeachingToggle/UnClick/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_teachingToggleUnClickRedMark;
    [AutoBind("./SoldierInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierInfoButton;
    [AutoBind("./StopTeachingPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stopTeachingPanelStateCtrl;
    [AutoBind("./StopTeachingPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopTeachingPanelBackButton;
    [AutoBind("./StopTeachingPanel/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopTeachingPanelConfirmButton;
    [AutoBind("./StopTeachingPanel/Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopTeachingPanelCancelButton;
    private DrillUIController.DrillState m_curDrillState;
    private TrainingRoom m_curTrainingRoom;
    private List<AssistanceTeamUIController> m_assistanceTeamUICtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int[] m_buffPropArr;
    private HeroCharUIController m_heroCharUIController;
    private AssistanceTeamUIController m_stopHeroAssistantsTaskCtrl;
    private List<CommonUIStateController> propStateCtrlList;
    private List<Text> propGoList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInDrill;
    private static DelegateBridge __Hotfix_SetTrainingDetailPanel;
    private static DelegateBridge __Hotfix_OnCourseItemClick;
    private static DelegateBridge __Hotfix_OnRoomToggleClick;
    private static DelegateBridge __Hotfix_SetPropertyAddition;
    private static DelegateBridge __Hotfix_ResetAllPropertyState;
    private static DelegateBridge __Hotfix_CalcSoldierPropertyModifityAddtion;
    private static DelegateBridge __Hotfix_SetTeachingDetailPanel;
    private static DelegateBridge __Hotfix_SetTeamTime;
    private static DelegateBridge __Hotfix_OnAssistanceTeamTrainingButtonClick;
    private static DelegateBridge __Hotfix_OnAssistanceTeamStopButtonClick;
    private static DelegateBridge __Hotfix_OpenStopTeachingPanel;
    private static DelegateBridge __Hotfix_CloseStopTeachingPanel;
    private static DelegateBridge __Hotfix_OnStopTeachingPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnAssistanceTeamGetRewardButtonClick;
    private static DelegateBridge __Hotfix_OnTrainingButtonClick;
    private static DelegateBridge __Hotfix_OnSoldierInfoButtonClick;
    private static DelegateBridge __Hotfix_OnTrainingToggleValueChanged;
    private static DelegateBridge __Hotfix_OnTeachingToggleValueChanged;
    private static DelegateBridge __Hotfix_OnFastMaxButtonClick;
    private static DelegateBridge __Hotfix_ClearDataCache;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnManualButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnManualButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnTrainingButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnTrainingButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAssistanceTrainingButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAssistanceTrainingButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAssistanceStopButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAssistanceStopButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnAssistanceGetRewardButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnAssistanceGetRewardButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public DrillUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInDrill(int drillState = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTrainingDetailPanel(TrainingRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCourseItemClick(CourseItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRoomToggleClick(DrillRoomToggleUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPropertyAddition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetAllPropertyState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CalcSoldierPropertyModifityAddtion(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTeachingDetailPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamTime(int slot, TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssistanceTeamTrainingButtonClick(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssistanceTeamStopButtonClick(AssistanceTeamUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenStopTeachingPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseStopTeachingPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStopTeachingPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssistanceTeamGetRewardButtonClick(AssistanceTeamUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSoldierInfoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTeachingToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFastMaxButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnManualButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTrainingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnAssistanceTrainingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnAssistanceStopButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnAssistanceGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum DrillState
    {
      Training,
      Teaching,
    }
  }
}
