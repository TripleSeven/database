﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoryUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public sealed class StoryUIController : UIControllerBase
  {
    private StoryUITask.StateType m_stateType;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_settingStateCtrl;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_panelBGButton;
    [AutoBind("./SkipButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx SkipButton;
    [AutoBind("./Detail/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text StoryText;
    [AutoBind("./BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image StoryImage;
    [AutoBind("./Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image SpaceImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController StoryUIPrefabCommonUIStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button BGButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_SetStory;
    private static DelegateBridge __Hotfix_PanelShow;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_GetTweenPosToVector3;
    private static DelegateBridge __Hotfix_GetTweenPosduration;
    private static DelegateBridge __Hotfix_NormalPlay;
    private static DelegateBridge __Hotfix_QuickPlay;
    private static DelegateBridge __Hotfix_OnSkipButtonClick;
    private static DelegateBridge __Hotfix_SkipDialogDialogBoxCallback;
    private static DelegateBridge __Hotfix_StateClose;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStory(ConfigDataStoryOutlineInfo info, StoryUITask.StateType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PanelShow(StoryUITask.StateType stateType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GetTweenPosToVector3(float height)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private float GetTweenPosduration(float height, bool isSpeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NormalPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void QuickPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkipButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkipDialogDialogBoxCallback(DialogBoxResult r)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateClose()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
