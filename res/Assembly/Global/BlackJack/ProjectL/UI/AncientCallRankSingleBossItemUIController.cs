﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRankSingleBossItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallRankSingleBossItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./DamageValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damageValueText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText;
    [AutoBind("./RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankImage;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./HeadIcon/FriendImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendImageGameObject;
    [AutoBind("./HeadIcon/GuildImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_guildImageGameObject;
    [AutoBind("./BattleArrayButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_battleArrayButton;
    private ProjectLPlayerContext m_playerContext;
    private ClientConfigDataLoader m_configDataLoader;
    private RankingTargetPlayerInfo m_rankingTargetPlayerInfo;
    private int m_rank;
    private bool m_isSocialRanking;
    private int m_bossId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetRankPlayerInfo;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_GetRankLevelSprite;
    private static DelegateBridge __Hotfix_OnBattleArraryClick;
    private static DelegateBridge __Hotfix_add_EventOnBattleArraryClick;
    private static DelegateBridge __Hotfix_remove_EventOnBattleArraryClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRankPlayerInfo(
      RankingTargetPlayerInfo rankingTargetPlayerInfo,
      int rank,
      int bossId,
      bool isSocialRanking = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattleArraryClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<RankingTargetPlayerInfo, int> EventOnBattleArraryClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
