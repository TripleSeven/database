﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutPlayerInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaKnockoutPlayerInfoUIController : UIControllerBase
  {
    private int GroupStateFinalRound = 4;
    [AutoBind("./BGType", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_bgState;
    [AutoBind("./State", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_playerItemState;
    [AutoBind("./State/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./State/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverNameText;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_playerIconButton;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./HeadIcon/HeadIconGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconGreyImage;
    [AutoBind("./HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./State/Idols", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_JettonButton;
    [AutoBind("./State/Idols", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_JettonButtonState;
    [AutoBind("./State/Idols/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_jettonValueText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private CommonUIStateController m_headFrameState;
    private PeakArenaUITask m_peakArenaUITask;
    private PeakArenaPlayOffPlayerInfo m_knockoutPlayerInfo;
    private PeakArenaPlayOffMatchupInfo m_matchupInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetPlayerDefaultState;
    private static DelegateBridge __Hotfix_SetByeState;
    private static DelegateBridge __Hotfix_SetMatchupInfo;
    private static DelegateBridge __Hotfix_UpdateViewPlayerInfo;
    private static DelegateBridge __Hotfix_UpdateViewGreyState;
    private static DelegateBridge __Hotfix_UpdateViewFinishMatchState;
    private static DelegateBridge __Hotfix_UpdateViewJetton;
    private static DelegateBridge __Hotfix_GetPlayerUserid;
    private static DelegateBridge __Hotfix_OnPlayerHeadClick;
    private static DelegateBridge __Hotfix_OnUseJettonClick;
    private static DelegateBridge __Hotfix_OnUseJettonSuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerDefaultState(
      PeakArenaPlayOffMatchupInfo matchupInfo,
      PeakArenaPlayOffPlayerInfo playerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetByeState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchupInfo(PeakArenaPlayOffMatchupInfo matchupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewGreyState(bool isGrey)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewFinishMatchState(PeakArenaPlayOffMatchupInfo matchupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewJetton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerUserid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerHeadClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseJettonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseJettonSuccess()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
