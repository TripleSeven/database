﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailAddExpUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailAddExpUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rootGo;
    [AutoBind("./ReturnBgImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeAddExpButton;
    [AutoBind("./Detail/ExpItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpScrollViewContent;
    [AutoBind("./Detail/HeroInfoGroup/Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelLvValueText;
    [AutoBind("./Detail/HeroInfoGroup/Lv/ValueText/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelLvValueMaxText;
    [AutoBind("./Detail/HeroInfoGroup/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelHeroNameText;
    [AutoBind("./Detail/LevelUpEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpPanelLevelUpEffect;
    [AutoBind("./Detail/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_addExpPanelHeroIconImg;
    [AutoBind("./Detail/Exp/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpPanelExpValueText;
    [AutoBind("./Detail/Exp/ProgressImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_addExpPanelExpProgressImg;
    [AutoBind("./Detail/U_LevelUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpPanelLevelUp3DEffect;
    [AutoBind("./Detail/NoExpItemTip", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpPanelNoExpItemTip;
    [AutoBind("./Detail/ConfirmPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addExpConfirmPanel;
    [AutoBind("./Detail/ConfirmPanel/Tip1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpConfirmPanelTip1;
    [AutoBind("./Detail/ConfirmPanel/Tip2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addExpConfirmPanelTip2;
    [AutoBind("./Detail/ConfirmPanel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addExpConfirmPanelConfirmButton;
    [AutoBind("./Detail/ConfirmPanel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addExpConfirmPanelCancelButton;
    [AutoBind("./Detail/ConfirmPanel/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addExpConfirmPanelBgButton;
    private bool m_isHeroLvUp;
    private bool m_isFirstIn;
    private bool m_canLastClickExpItemUse;
    private Hero m_hero;
    private float m_expProgressBarWidth2;
    private int m_totalUseExpItemCount;
    private int m_tempHeroLevel;
    private int m_tempHeroExp;
    private int m_tempAddExp;
    private int m_heroLastLevel;
    private HeroExpItemUIController m_curExpBagItemCtrl;
    public List<HeroExpItemUIController> m_expCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private int m_addImageCoroutineCount;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInAddExpState;
    private static DelegateBridge __Hotfix_UpdateAddExpPanel;
    private static DelegateBridge __Hotfix_HeroAddExpLvUpEffect;
    private static DelegateBridge __Hotfix_OnExpItemClick;
    private static DelegateBridge __Hotfix_LocalAddExpTick;
    private static DelegateBridge __Hotfix_AddProgressImageWidth;
    private static DelegateBridge __Hotfix_OnLocalAddExpFinishedSendReq;
    private static DelegateBridge __Hotfix_WaitTimeThenDoEvent;
    private static DelegateBridge __Hotfix_CanExpItemUse;
    private static DelegateBridge __Hotfix_OnAddExpConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnAddExpCanelButtonClick;
    private static DelegateBridge __Hotfix_CloseAddExpPanel;
    private static DelegateBridge __Hotfix_UseFirstExpBagItemForUserGuide;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnHeroMaxLevelUseExpItem;
    private static DelegateBridge __Hotfix_remove_EventOnHeroMaxLevelUseExpItem;
    private static DelegateBridge __Hotfix_add_EventOnHeroAddExp;
    private static DelegateBridge __Hotfix_remove_EventOnHeroAddExp;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDetailAddExpUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInAddExpState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateAddExpPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroAddExpLvUpEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpItemClick(HeroExpItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LocalAddExpTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator AddProgressImageWidth(
      float totalWidth,
      float DesWidth,
      float intervalTime = 0.1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLocalAddExpFinishedSendReq(int usedCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WaitTimeThenDoEvent(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanExpItemUse(int heroLv, int herpExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddExpConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddExpCanelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseAddExpPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UseFirstExpBagItemForUserGuide(HeroExpItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnHeroMaxLevelUseExpItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, BagItemBase, int, Action> EventOnHeroAddExp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
