﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MailDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MailDetailUIController : UIControllerBase
  {
    private Mail m_currentMailInfo;
    private ProjectLPlayerContext m_playerContext;
    private List<RewardGoodsUIController> m_attachmentUICtrlList;
    private GameObject ItemUICtrlPrefab;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController BgUIState;
    [AutoBind("./MailDetail_Title/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text TitleText;
    [AutoBind("./TimeOutTextPanel/TimeOutText", AutoBindAttribute.InitState.NotInit, false)]
    public Text TimeOutText;
    [AutoBind("./MailDetail/ListScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text ContextText;
    [AutoBind("./MailDetail/ListScrollViewSmall/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text SmallContextText;
    [AutoBind("./MailDetail/RewardItem/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject RewardRoot;
    [AutoBind("./MailDetail/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx GetButton;
    [AutoBind("./MailDetail/GotoButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button GotoButton;
    [AutoBind("./NoMailPanel/ChooseText", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ChooseTextGo;
    [AutoBind("./NoMailPanel/NoneText", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject NoneTextGo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateMailDetail;
    private static DelegateBridge __Hotfix_ShowAttachmentsUI;
    private static DelegateBridge __Hotfix_OnGotoButtonClick;
    private static DelegateBridge __Hotfix_OnGetAttachmentButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGetAttachmentButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGetAttachmentButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public MailDetailUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMailDetail(Mail mailInfo, bool haveMail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAttachmentsUI(Mail mailInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAttachmentButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<Mail> EventOnGetAttachmentButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<Mail> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
