﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EquipmentDepotListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EquipmentDepotListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController m_stateCtrl;
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_icon;
    [AutoBind("./BgFrame", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_bgFrame;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_SSREffect;
    [AutoBind("./CheckImageMask", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_checkImageMask;
    [AutoBind("./StarGroup", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_starGroup;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_lvValueText;
    [AutoBind("./EquipingTag/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_equipingTagHeadIcon;
    [AutoBind("./LockImage", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_lockImage;
    [AutoBind("./SelectFrame", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_selectFrame;
    [AutoBind("./Mask", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject m_mask;
    [AutoBind("./EnchantmentIcon", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_enchantmentIcon;
    [AutoBind("./CountText", AutoBindAttribute.InitState.NotInit, false)]
    public Text m_countText;
    public bool IsEquiped;
    public ulong EquipmentInstanceId;
    public BagItemBase BagItem;
    public ScrollItemBaseUIController ScrollItemBaseUICtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitEquipmentDepotListItemInfo;
    private static DelegateBridge __Hotfix_SetToDarkState;
    private static DelegateBridge __Hotfix_ShowCheckImage;
    private static DelegateBridge __Hotfix_ShowSelectFrame;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_RegisterItemClickEvent;
    private static DelegateBridge __Hotfix_RegisterItemNeedFillEvent;
    private static DelegateBridge __Hotfix_RegisterItem3DTouchEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEquipmentDepotListItemInfo(BagItemBase bagItem, bool canWear, bool isInDepot = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToDarkState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCheckImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectFrame(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemClickEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItemNeedFillEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterItem3DTouchEvent(Action<UIControllerBase> action)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
