﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallBossDetailHeroItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallBossDetailHeroItemUIController : UIControllerBase
  {
    [AutoBind("./HeroHead", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./DescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetAncientCallBossHero;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAncientCallBossHero(AncientRecommendHeros hero)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
