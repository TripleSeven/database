﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SignRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SignRewardItemUIController : UIControllerBase, IPointerClickHandler, IEventSystemHandler
  {
    [AutoBind("./ItemGroup/RewardGoodsDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_signItemGoodsDummy;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    public Image m_itemFrameImage;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_signItemUIStateCtrl;
    [AutoBind("./DayText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dayText;
    private SignRewardItemUIController.SignState m_signState;
    private GoodsType m_goodsType;
    private int m_goodsId;
    private int m_goodsCount;
    private ConfigDataItemInfo m_itemInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnPointerClick;
    private static DelegateBridge __Hotfix_SetReward;
    private static DelegateBridge __Hotfix_SetDay;
    private static DelegateBridge __Hotfix_PlaySignAnimation;
    private static DelegateBridge __Hotfix_Co_ChangeStateToSigning;
    private static DelegateBridge __Hotfix_TodayAutoSign;
    private static DelegateBridge __Hotfix_GetGoodsType;
    private static DelegateBridge __Hotfix_GetGoodsId;
    private static DelegateBridge __Hotfix_GetGoodsCount;
    private static DelegateBridge __Hotfix_add_EventOnSignTodayItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnSignTodayItemClick;
    private static DelegateBridge __Hotfix_add_EventOnSignTodayBoxOpenClick;
    private static DelegateBridge __Hotfix_remove_EventOnSignTodayBoxOpenClick;
    private static DelegateBridge __Hotfix_add_EventOnShowBoxRewards;
    private static DelegateBridge __Hotfix_remove_EventOnShowBoxRewards;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReward(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDay(int day)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySignAnimation(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ChangeStateToSigning(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TodayAutoSign()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GoodsType GetGoodsType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGoodsId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGoodsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnSignTodayItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GoodsType, int, int> EventOnSignTodayBoxOpenClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<Goods>, SignRewardItemUIController> EventOnShowBoxRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum SignState
    {
      Signed,
      NeedSign,
      NotSign,
    }
  }
}
