﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CommonUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class CommonUITask : UITask
  {
    private static CommonUITask s_instance;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private CommonUIController m_commonUIController;
    private float m_sendHeartBeatTime;
    private DateTime m_nextCheckOnlineTime;
    private StringTableId m_disconnectedByServerMessageId;
    private float m_lastCheckClientVersionTime;
    private int m_netTaskRunningCount;
    public CommonUITask.CommonUIState m_state;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_InitNetReq;
    private static DelegateBridge __Hotfix_TickCheckClientVersion;
    private static DelegateBridge __Hotfix_TickChatVoice;
    private static DelegateBridge __Hotfix_TickClientHeartBeat;
    private static DelegateBridge __Hotfix_TickCheckOnline;
    private static DelegateBridge __Hotfix__TickCheckOnline;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_Application_LogMessageReceived;
    private static DelegateBridge __Hotfix_PlayerContext_OnNewMarqueeNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnServerDisconnectNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnClientCheatNtf;
    private static DelegateBridge __Hotfix_PlayerContext_OnOnBattlePracticeFailNtf;
    private static DelegateBridge __Hotfix_PlayerContext_EventOnGuildUpdateInfo;
    private static DelegateBridge __Hotfix_EventOnSDKPromotingPlaySuccess;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;
    private static DelegateBridge __Hotfix_StartWorldUITask;
    private static DelegateBridge __Hotfix_WorldUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartBattleUITask;
    private static DelegateBridge __Hotfix_BattleUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_StartArenaBattleUITask;
    private static DelegateBridge __Hotfix_CheckEnterBattle;
    private static DelegateBridge __Hotfix_EnterBattleRoomBattle;
    private static DelegateBridge __Hotfix_EnterBattleLiveRoomBattle;
    private static DelegateBridge __Hotfix_EnterArenaBattle;
    private static DelegateBridge __Hotfix_RebuildBattle;
    private static DelegateBridge __Hotfix_RebuildBattleHappening;
    private static DelegateBridge __Hotfix_RebuildArenaBattle;
    private static DelegateBridge __Hotfix_RebuildBattleRoom;
    private static DelegateBridge __Hotfix_RebuildBattleLiveRoom;
    private static DelegateBridge __Hotfix_RebuildBattleRoomAndStartUITask;
    private static DelegateBridge __Hotfix_RebuildBattleLiveRoomAndStartUITask;
    private static DelegateBridge __Hotfix_StartBattleCancelNetTask;
    private static DelegateBridge __Hotfix_StartArenaBattleCancelNetTaskAndStartWorldUITask;
    private static DelegateBridge __Hotfix_StartArenaPlayerInfoGetNetTask;
    private static DelegateBridge __Hotfix_StartArenaBattleReconnectNetTask;
    private static DelegateBridge __Hotfix_StartBattleRoomGetNetTask;
    private static DelegateBridge __Hotfix_StartPeakArenaBattleLiveRoomJoinNetTask;
    private static DelegateBridge __Hotfix_StartRealTimePVPGetInfoNetTask;
    private static DelegateBridge __Hotfix_StartPeakArenaPlayerInfoGetNetTask;
    private static DelegateBridge __Hotfix_StartPeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~CommonUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitNetReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickCheckClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickChatVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientHeartBeat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void TickCheckOnline()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _TickCheckOnline()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Application_LogMessageReceived(string logString, string stackTrace, LogType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnNewMarqueeNtf(Marquee marquee)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnServerDisconnectNtf(int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnClientCheatNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnOnBattlePracticeFailNtf(PracticeMode practiceMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_EventOnGuildUpdateInfo(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EventOnSDKPromotingPlaySuccess(string goodsRegisterID)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void WorldUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattleUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool StartArenaBattleUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckEnterBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnterBattleRoomBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnterBattleLiveRoomBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnterArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleHappening(BattleType battleType, int levelId, ulong instanceId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleRoomAndStartUITask(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RebuildBattleLiveRoomAndStartUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartBattleCancelNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartArenaBattleCancelNetTaskAndStartWorldUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartArenaPlayerInfoGetNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void StartArenaBattleReconnectNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartBattleRoomGetNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaBattleLiveRoomJoinNetTask(
      int matchId,
      ulong roomId,
      Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartRealTimePVPGetInfoNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaPlayerInfoGetNetTask(Action<int> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StartPeakArenaPlayOffCurrentSeasonMatchInfoGetNetTask(
      BattleRoom battleRoom,
      Action<PeakArenaPlayOffMatchupInfo> onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum CommonUIState
    {
      Init,
      BeginLoadAsset,
      EndLoadAsset,
      WorldOrBattle,
    }
  }
}
