﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ReloginUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ReloginUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./DialogBox", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_dialogBoxGameObject;
    [AutoBind("./DialogBox/Buttons/RetryButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button RetryLoginButton;
    [AutoBind("./DialogBox/Buttons/LoginButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button ReturnToLoginButton;
    [AutoBind("./Waiting", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_waitingGameObject;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_ShowWaitForReloginConfirm;
    private static DelegateBridge __Hotfix_ShowWaitForReloginProcessing;
    private static DelegateBridge __Hotfix_ShowWaitReturnToLoginConfirm;

    private ReloginUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitForReloginConfirm()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitForReloginProcessing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitReturnToLoginConfirm()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
