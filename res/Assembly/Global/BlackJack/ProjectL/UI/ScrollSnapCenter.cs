﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ScrollSnapCenter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ScrollSnapCenter : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
  {
    public float m_itemSize;
    public ScrollSnapDirection m_scrollSnapDir;
    public ScrollSnapPageType m_scrollSnapPageType;
    public float m_dragPercentage;
    private bool m_isSnaping;
    private Vector2 m_snapPosition;
    private int m_itemCount;
    private ScrollRect m_scrollRect;
    private int m_currentCenterIndex;
    private Vector2 m_beginDragPosition;
    private Vector2 m_endFragPosition;
    private static DelegateBridge __Hotfix_Awake;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_SetItemCount;
    private static DelegateBridge __Hotfix_GetItemCount;
    private static DelegateBridge __Hotfix_ComputeItemPosition;
    private static DelegateBridge __Hotfix_GetCenterItemIndexUnclamped;
    private static DelegateBridge __Hotfix_GetCenterItemIndex;
    private static DelegateBridge __Hotfix_Snap_1;
    private static DelegateBridge __Hotfix_GetItemIndexByDragDistance;
    private static DelegateBridge __Hotfix_ComputeSnapCenterPosition;
    private static DelegateBridge __Hotfix_Snap_0;
    private static DelegateBridge __Hotfix_OnBeginDrag;
    private static DelegateBridge __Hotfix_OnEndDrag;
    private static DelegateBridge __Hotfix_add_EventOnCenterItemChanged;
    private static DelegateBridge __Hotfix_remove_EventOnCenterItemChanged;
    private static DelegateBridge __Hotfix_add_EventOnEndDrag;
    private static DelegateBridge __Hotfix_remove_EventOnEndDrag;

    [MethodImpl((MethodImplOptions) 32768)]
    public ScrollSnapCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Awake()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetItemCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetItemCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 ComputeItemPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCenterItemIndexUnclamped()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCenterItemIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Snap(int idx, bool smooth = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetItemIndexByDragDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeSnapCenterPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Snap(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnCenterItemChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEndDrag
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
