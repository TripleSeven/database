﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDungeonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDungeonUIController : UIControllerBase
  {
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./HeroCharPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObjectRoot;
    [AutoBind("./EventGroup/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_levelsScrollRect;
    [AutoBind("./ChallengeCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_challengeCountValueText;
    [AutoBind("./Progress", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressGameObject;
    [AutoBind("./Progress/VictoryPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressText;
    [AutoBind("./Progress/VictoryPointsReward/BarFrame/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_progressImage;
    [AutoBind("./Progress/VictoryPointsReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton1GameObject;
    [AutoBind("./Progress/VictoryPointsReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton2GameObject;
    [AutoBind("./Progress/VictoryPointsReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_starRewardButton3GameObject;
    [AutoBind("./StarRewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_starRewardPreviewUIStateController;
    [AutoBind("./StarRewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_starRewardPreviewBackgroundButton;
    [AutoBind("./StarRewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_starRewardPreviewScrollRect;
    [AutoBind("./EventGroup/TurntableImage/Circle1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_circle1GameObject;
    [AutoBind("./EventGroup/TurntableImage/Circle2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_circle2GameObject;
    [AutoBind("./Prefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_prefabGameObject;
    [AutoBind("./Prefab/HeroDungeonLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroDungeonLevelListItemPrefab;
    [AutoBind("./EventGroup/TurntableImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ellipseImage;
    private List<HeroDungeonLevelListItemUIController> m_heroDungeonLevelListItems;
    private const int gearRollThreasholdValue = 50;
    private float m_rotationCount;
    private int m_curLevelIndex;
    private float dy2;
    private GainRewardButton[] m_starRewardButtons;
    private HeroCharUIController m_heroCharUIController;
    private ConfigDataHeroInformationInfo m_heroDungeonInfo;
    private ScrollSnapCenter m_levelsScrollViewVerticalSnapCenter;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_GetLineOnEllipseIntersection;
    private static DelegateBridge __Hotfix_isPointInLine;
    private static DelegateBridge __Hotfix_GetLineLineIntersection;
    private static DelegateBridge __Hotfix_SetCircleRotation;
    private static DelegateBridge __Hotfix_AddHeroDungeonLevelListItem;
    private static DelegateBridge __Hotfix_ClearHeroDungeonLevelListItems;
    private static DelegateBridge __Hotfix_UpdateDungeonLevelListItem;
    private static DelegateBridge __Hotfix_SetCurrentDungeonLevel;
    private static DelegateBridge __Hotfix_SetHeroChar;
    private static DelegateBridge __Hotfix_SetDungeonStarsProgress;
    private static DelegateBridge __Hotfix_OnStarRewardButtonClick;
    private static DelegateBridge __Hotfix_AddStarRewardPreviewGoods;
    private static DelegateBridge __Hotfix_SetChallengeCountValueText;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnHeroDungeonLevelButtonClick;
    private static DelegateBridge __Hotfix_OnStarRewardPreviewBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnGetStarReward;
    private static DelegateBridge __Hotfix_remove_EventOnGetStarReward;
    private static DelegateBridge __Hotfix_add_EventOnSelectDungeonLevel;
    private static DelegateBridge __Hotfix_remove_EventOnSelectDungeonLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Vector2> GetLineOnEllipseIntersection(
      double x1,
      double x2,
      double y1,
      double y2,
      double midX,
      double midY,
      double h,
      double v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool isPointInLine(double x1, double x2, double y1, double y2, double px, double py)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GetLineLineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCircleRotation(float deltaRotation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroDungeonLevelListItem(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearHeroDungeonLevelListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateDungeonLevelListItem(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroChar(int heroID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDungeonStarsProgress(ConfigDataHeroInformationInfo heroDungeonInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddStarRewardPreviewGoods(ConfigDataHeroInformationInfo heroDungeonInfo, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengeCountValueText(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroDungeonLevelButtonClick(HeroDungeonLevelListItemUIController b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStarRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGetStarReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ConfigDataHeroDungeonLevelInfo> EventOnSelectDungeonLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
