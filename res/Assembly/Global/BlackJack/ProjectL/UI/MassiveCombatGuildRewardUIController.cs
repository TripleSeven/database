﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.MassiveCombatGuildRewardUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class MassiveCombatGuildRewardUIController : UIControllerBase
  {
    [AutoBind("./GuildProgressInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_total;
    [AutoBind("./RewardInfoListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rewardContent;
    [AutoBind("./Prefab/GuildRewardItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPrefab;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<MassiveCombatGuildRewardItemUIController> m_rewardItemUIControllerList;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Refresh;

    [MethodImpl((MethodImplOptions) 32768)]
    public MassiveCombatGuildRewardUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
