﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FriendUIController : UIControllerBase
  {
    [AutoBind("./Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./Panel/ShieldButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shieldButton;
    [AutoBind("./Panel/AddFriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendButton;
    [AutoBind("./Panel/AddFriendButton/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendRedMark;
    [AutoBind("./Panel/CreateNewGroupButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createNewGroupButton;
    [AutoBind("./FriendPanelTypeCount", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendCountUIStateController;
    [AutoBind("./FriendPanelTypeCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendTypeCount;
    [AutoBind("./Panel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_panelUIStateController;
    [AutoBind("./FriendShipPointTip", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendShipPointTipStateCtrl;
    [AutoBind("./FriendShipPointTip/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendShipPointTipText;
    [AutoBind("./Panel/GetAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getAllFriendshipPointButton;
    [AutoBind("./Panel/GetAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_getAllFriendshipPointButtonUIStateCtrl;
    [AutoBind("./Panel/SendAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sendAllFriendshipPointButton;
    [AutoBind("./Panel/SendAllFriendshipPointButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_sendAllFriendshipPointButtonUIStateCtrl;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_friendToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Server", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_acrossServerToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Recent", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_recentToggle;
    [AutoBind("./Margin/ToggleListGroup/FilterButtons/Group", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_groupToggle;
    [AutoBind("./AddFriendPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_addFriendPanelUIStateController;
    [AutoBind("./AddFriendPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelCloseButton;
    [AutoBind("./AddFriendPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelBackgroundButton;
    [AutoBind("./AddFriendPanel/Detail/ServerSelectButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelSelectServerButton;
    [AutoBind("./AddFriendPanel/Detail/ServerSelectButton/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addFriendPanelServerNameText;
    [AutoBind("./AddFriendPanel/Detail/SearchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelSearchButton;
    [AutoBind("./AddFriendPanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_addFriendPanelInputText;
    [AutoBind("./AddFriendPanel/Detail/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelChangeButton;
    [AutoBind("./AddFriendPanel/Detail/AllRefuseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addFriendPanelRefuseAllButton;
    [AutoBind("./AddFriendPanel/Detail/RecommendListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelRecommendScrollView;
    [AutoBind("./AddFriendPanel/Detail/RecommendListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelRecommendScrollViewContent;
    [AutoBind("./AddFriendPanel/Detail/ApplyListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelApplyScrollView;
    [AutoBind("./AddFriendPanel/Detail/ApplyListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelApplyScrollViewContent;
    [AutoBind("/AddFriendPanel/Detail/RecommendText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelRecommendTextGameObject;
    [AutoBind("/AddFriendPanel/Detail/FindFriendText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelFindFriendTextGameObject;
    [AutoBind("./AddFriendPanel/Detail/NotFound", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_addFriendPanelNotFindFriendGameObject;
    [AutoBind("./AddFriendPanel/ServerListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_serverListPanelUIStateController;
    [AutoBind("./AddFriendPanel/ServerListPanel/Prefab/ServerItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_serverItemGameObject;
    [AutoBind("./AddFriendPanel/ServerListPanel/Prefab/ServerItem/ServerNameGroup/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverItemName;
    [AutoBind("./AddFriendPanel/ServerListPanel/Detail/ServerScrollView/Viewport/Content/ServerListGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_serverListGroup;
    [AutoBind("./AddFriendPanel/ServerListPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_serverListPanelBGButton;
    [AutoBind("./WatchGroupStaffPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_watchGroupStaffPanelUIStateController;
    [AutoBind("./WatchGroupStaffPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelBackgroundButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelCloseButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/ChangeNameButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelChangeGroupNameButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/QuitGroupButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelQuitGroupButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/InviteFriendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_watchGroupStaffPanelInviteFriendButton;
    [AutoBind("./WatchGroupStaffPanel/Detail/PlayerListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_watchGroupStaffPanelScrollView;
    [AutoBind("./WatchGroupStaffPanel/Detail/PlayerListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_watchGroupStaffPanelScrollViewContent;
    [AutoBind("./WatchGroupStaffPanel/Detail/GroupStaffCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_watchGroupStaffPanelCountText;
    [AutoBind("./WatchGroupStaffPanel/Detail/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_groupTitleName;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_changeGroupNameUIStateController;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeGroupNameBackgroundButton;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_changeGroupNameConfirmButton;
    [AutoBind("./WatchGroupStaffPanel/ChangeNamePanel/Detail/InputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_changeNamePanelInputField;
    [AutoBind("./InviteFriendPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_inviteFriendToGroupPanelUIStateController;
    [AutoBind("./InviteFriendPanel/Detail/PlayerListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inviteFriendToGroupPanelScrollView;
    [AutoBind("./InviteFriendPanel/Detail/PlayerListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_inviteFriendToGroupPanelScrollViewContent;
    [AutoBind("./InviteFriendPanel/Detail/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteFriendToGroupPanelConfirmButton;
    [AutoBind("./InviteFriendPanel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteFriendToGroupPanelCancelButton;
    [AutoBind("./InviteFriendPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inviteFriendToGroupPanelBackgroundButton;
    [AutoBind("./InviteFriendPanel/Detail/InvitedCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_inviteFriendToGroupPanelCountText;
    [AutoBind("./InviteFriendPanel/Detail/ToggleGroup/ServerFriend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_inviteFriendToGroupPanelServerFriendToggle;
    [AutoBind("./InviteFriendPanel/Detail/ToggleGroup/Friend", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_inviteFriendToGroupPanelFriendToggle;
    [AutoBind("./ShieldPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_shieldPanelUIStateController;
    [AutoBind("./ShieldPanel/Detail/PlayListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shieldPanelScrollView;
    [AutoBind("./ShieldPanel/Detail/PlayListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shieldPanelScrollViewContent;
    [AutoBind("./ShieldPanel/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shieldPanelBackgroundButton;
    [AutoBind("./ShieldPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_shieldPanelCloseButton;
    [AutoBind("./ShieldPanel/Detail/ShieldCount/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_shieldPanelShieldCount;
    [AutoBind("./Panel/FriendListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendScrollView;
    [AutoBind("./Panel/FriendListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendScrollViewContent;
    [AutoBind("./Panel/GroupListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_groupScrollView;
    [AutoBind("./Panel/GroupListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_groupScrollViewContent;
    [AutoBind("./Prefab/PlayerBigListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendBigItemPrefab;
    [AutoBind("./Prefab/PlayerSmallListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendSmallItemPrefab;
    [AutoBind("./Prefab/GroupListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_groupItemPrefab;
    private int m_currentServerID;
    private ProChatGroupInfo m_currentChatGroupInfo;
    private bool m_isCreateNewGroup;
    private const int m_maxInviteToGroupCount = 10;
    public bool m_isFindFriend;
    public bool m_isRecommendFriend;
    private List<UserSummary> m_recommendFriendList;
    private List<UserSummary> m_applyFriendList;
    private List<string> m_inviteToGroupList;
    private List<UserSummary> m_groupStaffList;
    private List<string> m_friendApplyAcceptList;
    private List<string> m_friendApplyDeclineList;
    private List<string> m_friendAddList;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetFriendPanel;
    private static DelegateBridge __Hotfix_SetAcrossServerFriendPanel;
    private static DelegateBridge __Hotfix_SetRecentPanel;
    private static DelegateBridge __Hotfix_SetGroupPanel;
    private static DelegateBridge __Hotfix_SetAddFriendPanel;
    private static DelegateBridge __Hotfix_SetFirstToggleOn;
    private static DelegateBridge __Hotfix_SetPanelScrollViewReset;
    private static DelegateBridge __Hotfix_IsAddFriendRedMarkShow;
    private static DelegateBridge __Hotfix_ShowFriendShipPointTip;
    private static DelegateBridge __Hotfix_SetShieldPanel;
    private static DelegateBridge __Hotfix_SetFindFriendList;
    private static DelegateBridge __Hotfix_SetRecommendFriendsList;
    private static DelegateBridge __Hotfix_SetApplyFriendsList;
    private static DelegateBridge __Hotfix_SetInviteFriendToGroupPanel;
    private static DelegateBridge __Hotfix_SetInviteFriendToGroupList;
    private static DelegateBridge __Hotfix_SetInviteAcrossServerFriendToGroupList;
    private static DelegateBridge __Hotfix_SetWatchGroupStaffPanel;
    private static DelegateBridge __Hotfix_SetBasicBigFriendInfo;
    private static DelegateBridge __Hotfix_SetBasicSmallFriendInfo;
    private static DelegateBridge __Hotfix_SetServerListInfo;
    private static DelegateBridge __Hotfix_OnFriendToggle;
    private static DelegateBridge __Hotfix_OnAcrossServerToggle;
    private static DelegateBridge __Hotfix_OnRecentToggle;
    private static DelegateBridge __Hotfix_OnGroupToggle;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnShieldButtonClick;
    private static DelegateBridge __Hotfix_OnShieldPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_OnSendAllFriendshipPointButtonClick;
    private static DelegateBridge __Hotfix_OnGetAllFriendshipPointButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_OnSelectServerButtonClick;
    private static DelegateBridge __Hotfix_OnServerListPanelBGButtonClick;
    private static DelegateBridge __Hotfix_SetSendAllFriendshipPointButtonState;
    private static DelegateBridge __Hotfix_SetGetAllFriendshipPointButtonState;
    private static DelegateBridge __Hotfix_OnAddFriendPanelSearchButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendPanelChangeRecommendFriendButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendPanelAddAllButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendPanelAcceptAllButtonClick;
    private static DelegateBridge __Hotfix_OnAddFriendPanelRefuseAllButtonClick;
    private static DelegateBridge __Hotfix_OnWatchGroupStaffPanelCloseButtonClick;
    private static DelegateBridge __Hotfix_WatchGroupStaffPanelClose;
    private static DelegateBridge __Hotfix_GroupDissolved;
    private static DelegateBridge __Hotfix_OnWatchGroupStaffPanelChangeNameButtonClick;
    private static DelegateBridge __Hotfix_OnWatchGroupStaffPanelChangeNamePanelBGButtonClick;
    private static DelegateBridge __Hotfix_OnWatchGroupStaffPanelChangeNameConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnQuitGroupButtonClick;
    private static DelegateBridge __Hotfix_OnCreateNewGroupButtonClick;
    private static DelegateBridge __Hotfix_OnInviteFriendToGroupButtonClick;
    private static DelegateBridge __Hotfix_InviteFriendToGoupPanelClose;
    private static DelegateBridge __Hotfix_OnInviteFriendToGoupPanelConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnInviteFriendToGroupPanelServerFriendToggle;
    private static DelegateBridge __Hotfix_OnInviteFriendToGroupPanelFriendToggle;
    private static DelegateBridge __Hotfix_OnPlayerItemButtonClick_0;
    private static DelegateBridge __Hotfix_OnPlayerItemButtonClick_1;
    private static DelegateBridge __Hotfix_OnItemAddFriendButtonClick_0;
    private static DelegateBridge __Hotfix_OnItemKickFromGroupButtonClick;
    private static DelegateBridge __Hotfix_OnUnblockPlayerButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick_0;
    private static DelegateBridge __Hotfix_OnSendFriendshipPointButtonClick;
    private static DelegateBridge __Hotfix_OnGetFriendshipPointButtonClick;
    private static DelegateBridge __Hotfix_OnFriendshipPointDoneButtonClick;
    private static DelegateBridge __Hotfix_OnItemAddFriendButtonClick_1;
    private static DelegateBridge __Hotfix_OnChatButtonClick_1;
    private static DelegateBridge __Hotfix_OnFriendApplyAceeptButtonClick;
    private static DelegateBridge __Hotfix_OnFriendApplyDeclineButtonClick;
    private static DelegateBridge __Hotfix_OnInviteFriendToGroupToggleValueChanged;
    private static DelegateBridge __Hotfix_OnGroupChatButtonClick;
    private static DelegateBridge __Hotfix_OnWatchGroupStaffButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnShowPanel;
    private static DelegateBridge __Hotfix_remove_EventOnShowPanel;
    private static DelegateBridge __Hotfix_add_EventOnAddFriend;
    private static DelegateBridge __Hotfix_remove_EventOnAddFriend;
    private static DelegateBridge __Hotfix_add_EventOnKickFromGroup;
    private static DelegateBridge __Hotfix_remove_EventOnKickFromGroup;
    private static DelegateBridge __Hotfix_add_EventOnUnblockPlayer;
    private static DelegateBridge __Hotfix_remove_EventOnUnblockPlayer;
    private static DelegateBridge __Hotfix_add_EventOnChat;
    private static DelegateBridge __Hotfix_remove_EventOnChat;
    private static DelegateBridge __Hotfix_add_EventOnSendFriendshipPoint;
    private static DelegateBridge __Hotfix_remove_EventOnSendFriendshipPoint;
    private static DelegateBridge __Hotfix_add_EventOnGetFriendshipPoint;
    private static DelegateBridge __Hotfix_remove_EventOnGetFriendshipPoint;
    private static DelegateBridge __Hotfix_add_EventOnFindFriend;
    private static DelegateBridge __Hotfix_remove_EventOnFindFriend;
    private static DelegateBridge __Hotfix_add_EventOnSendAllFriendshipPoint;
    private static DelegateBridge __Hotfix_remove_EventOnSendAllFriendshipPoint;
    private static DelegateBridge __Hotfix_add_EventOnGetAllFriendshipPoint;
    private static DelegateBridge __Hotfix_remove_EventOnGetAllFriendshipPoint;
    private static DelegateBridge __Hotfix_add_EventOnGetRecommendFriendsList;
    private static DelegateBridge __Hotfix_remove_EventOnGetRecommendFriendsList;
    private static DelegateBridge __Hotfix_add_EventOnFriendApplyAceept;
    private static DelegateBridge __Hotfix_remove_EventOnFriendApplyAceept;
    private static DelegateBridge __Hotfix_add_EventOnFriendApplyDecline;
    private static DelegateBridge __Hotfix_remove_EventOnFriendApplyDecline;
    private static DelegateBridge __Hotfix_add_EventOnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_add_EventOnGroupChat;
    private static DelegateBridge __Hotfix_remove_EventOnGroupChat;
    private static DelegateBridge __Hotfix_add_EventOnWatchGroupStaff;
    private static DelegateBridge __Hotfix_remove_EventOnWatchGroupStaff;
    private static DelegateBridge __Hotfix_add_EventOnCreateNewGroup;
    private static DelegateBridge __Hotfix_remove_EventOnCreateNewGroup;
    private static DelegateBridge __Hotfix_add_EventOnInviteFriendToGroup;
    private static DelegateBridge __Hotfix_remove_EventOnInviteFriendToGroup;
    private static DelegateBridge __Hotfix_add_EventOnChangeGroupName;
    private static DelegateBridge __Hotfix_remove_EventOnChangeGroupName;
    private static DelegateBridge __Hotfix_add_EventOnLeaveGroup;
    private static DelegateBridge __Hotfix_remove_EventOnLeaveGroup;

    [MethodImpl((MethodImplOptions) 32768)]
    private FriendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendPanel(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAcrossServerFriendPanel(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecentPanel(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupPanel(List<ProChatGroupCompactInfo> chatGroupList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetAddFriendPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFirstToggleOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPanelScrollViewReset()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IsAddFriendRedMarkShow(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFriendShipPointTip(string s)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetShieldPanel(List<UserSummary> userSummy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFindFriendList(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecommendFriendsList(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetApplyFriendsList(List<UserSummary> userList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetInviteFriendToGroupPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteFriendToGroupList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInviteAcrossServerFriendToGroupList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWatchGroupStaffPanel(ProChatGroupInfo chatGroupInfo, bool needOpenTween)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBasicBigFriendInfo(
      GameObject parentScrollViewContent,
      List<UserSummary> userList,
      FriendInfoType friendInfoType,
      bool isChatGroupOwner = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBasicSmallFriendInfo(
      GameObject parentScrollViewContent,
      List<UserSummary> userList,
      FriendInfoType friendInfoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetServerListInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAcrossServerToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRecentToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShieldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShieldPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendAllFriendshipPointButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAllFriendshipPointButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAddFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectServerButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnServerListPanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSendAllFriendshipPointButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGetAllFriendshipPointButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelSearchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelChangeRecommendFriendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelAddAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelAcceptAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddFriendPanelRefuseAllButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffPanelCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WatchGroupStaffPanelClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GroupDissolved(ProChatGroupInfo chatGroupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffPanelChangeNameButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWatchGroupStaffPanelChangeNamePanelBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffPanelChangeNameConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnQuitGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCreateNewGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InviteFriendToGoupPanelClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGoupPanelConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupPanelServerFriendToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupPanelFriendToggle(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerItemButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPlayerItemButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddFriendButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemKickFromGroupButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnblockPlayerButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendFriendshipPointButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetFriendshipPointButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendshipPointDoneButtonClick(FriendBigItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemAddFriendButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendApplyAceeptButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFriendApplyDeclineButtonClick(FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendToGroupToggleValueChanged(
      bool isOn,
      FriendSmallItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupChatButtonClick(FriendGroupUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWatchGroupStaffButtonClick(FriendGroupUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendPanelType> EventOnShowPanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnAddFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProChatGroupInfo, UserSummary> EventOnKickFromGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnUnblockPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary> EventOnChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSendFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnGetFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, string> EventOnFindFriend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSendAllFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetAllFriendshipPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnGetRecommendFriendsList
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnFriendApplyAceept
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnFriendApplyDecline
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<UserSummary, Vector3, PlayerSimpleInfoUITask.PostionType> EventOnShowPlayerSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnGroupChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, bool> EventOnWatchGroupStaff
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<List<string>> EventOnCreateNewGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, List<string>> EventOnInviteFriendToGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string, string> EventOnChangeGroupName
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ProChatGroupInfo> EventOnLeaveGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
