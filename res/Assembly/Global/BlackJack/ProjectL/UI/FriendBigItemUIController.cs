﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendBigItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FriendBigItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupUIStateController;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendOnlineUIStateController;
    [AutoBind("./PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconImage;
    [AutoBind("./PlayerIconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconGreyImage;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_friendHeadFrameTransform;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendNameText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendLevelText;
    [AutoBind("./ServerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendServerText;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendOnlineLongText;
    [AutoBind("./ButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./ButtonGroup/UnblockButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_unblockButton;
    [AutoBind("./ButtonGroup/DeleteButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_kickButton;
    [AutoBind("./ButtonGroup/BigChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bigChatButton;
    [AutoBind("./ButtonGroup/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_sendButton;
    [AutoBind("./ButtonGroup/GetButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getButton;
    [AutoBind("./ButtonGroup/DoneButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_doneButton;
    private string m_userID;
    private UserSummary m_userSummy;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetFriendInfo;
    private static DelegateBridge __Hotfix_GetServerName;
    private static DelegateBridge __Hotfix_GetUserID;
    private static DelegateBridge __Hotfix_GetUserSummy;
    private static DelegateBridge __Hotfix_GetSimpleInfoPostionType;
    private static DelegateBridge __Hotfix_GetPlayerSimpleInfoPos;
    private static DelegateBridge __Hotfix_OnItemButtonClick;
    private static DelegateBridge __Hotfix_OnAddButtonClick;
    private static DelegateBridge __Hotfix_OnKickButtonClick;
    private static DelegateBridge __Hotfix_OnUnblockButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnSendButtonClick;
    private static DelegateBridge __Hotfix_OnGetButtonClick;
    private static DelegateBridge __Hotfix_OnDoneButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_add_EventOnAdd;
    private static DelegateBridge __Hotfix_remove_EventOnAdd;
    private static DelegateBridge __Hotfix_add_EventOnKickFromGroup;
    private static DelegateBridge __Hotfix_remove_EventOnKickFromGroup;
    private static DelegateBridge __Hotfix_add_EventOnUnblock;
    private static DelegateBridge __Hotfix_remove_EventOnUnblock;
    private static DelegateBridge __Hotfix_add_EventOnChat;
    private static DelegateBridge __Hotfix_remove_EventOnChat;
    private static DelegateBridge __Hotfix_add_EventOnSendFriendShipPoints;
    private static DelegateBridge __Hotfix_remove_EventOnSendFriendShipPoints;
    private static DelegateBridge __Hotfix_add_EventOnGetFriendShipPoints;
    private static DelegateBridge __Hotfix_remove_EventOnGetFriendShipPoints;
    private static DelegateBridge __Hotfix_add_EventOnFriendShipPointsDone;
    private static DelegateBridge __Hotfix_remove_EventOnFriendShipPointsDone;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendBigItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendInfo(
      UserSummary userSummy,
      FriendInfoType friendInfoType,
      bool isChatGroupOwner = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetServerName(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary GetUserSummy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask.PostionType GetSimpleInfoPostionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetPlayerSimpleInfoPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnKickButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUnblockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDoneButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FriendBigItemUIController> EventOnShowPlayerSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnKickFromGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnUnblock
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnSendFriendShipPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnGetFriendShipPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendBigItemUIController> EventOnFriendShipPointsDone
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
