﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RewardGoodsUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RewardGoodsUIController : UIControllerBase
  {
    private static bool m_isNeedAutoClose = true;
    private GoodsType m_goodsType;
    private int m_goodsId;
    private int m_goodsCount;
    private int m_alignType;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./FrameImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_frameImage;
    private GameObject m_haveGetGroupRoot;
    private Text m_nameText;
    private Text m_countText;
    private GameObject m_crystalEffectGameObject;
    private GameObject m_ssrEffectGameObject;
    private GameObject m_ssrPieceEffectGameObject;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetReward;
    private static DelegateBridge __Hotfix_ShowCount;
    private static DelegateBridge __Hotfix_ShowTag;
    private static DelegateBridge __Hotfix_ShowHaveGot;
    private static DelegateBridge __Hotfix_CreateRewardGoods;
    private static DelegateBridge __Hotfix_CreateRewardGoodsList;
    private static DelegateBridge __Hotfix_OnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReward(Goods r, int alignType = 0, bool isNeedAutoClose = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCount(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTag(string tagName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowHaveGot(bool isGot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RewardGoodsUIController CreateRewardGoods(
      Goods g,
      Transform parent,
      GameObject prefab,
      bool showCount = true,
      int alignType = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CreateRewardGoodsList(
      List<Goods> goods,
      Transform parent,
      GameObject prefab,
      List<RewardGoodsUIController> ctrlList = null,
      bool showCount = true,
      int alignType = 0,
      bool isNeedAutoClose = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
