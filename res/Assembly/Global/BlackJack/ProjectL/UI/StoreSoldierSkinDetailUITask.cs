﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.StoreSoldierSkinDetailUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class StoreSoldierSkinDetailUITask : UITask
  {
    public StoreId m_storeId;
    public int m_soldierSkinConfigId;
    public StoreSoldierSkinDetailUITask.StartTaskFromType m_startFormType;
    public StoreSoldierSkinDetailUIController m_soldierSkinDetailPanelCtrl;
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    public const string ParamKey_StoreID = "FixedStoreID";
    public const string ParamKey_SoldierSkinConfigID = "SoldierSkinFixedStoreID";
    public const string ParamKey_StartTaskFromType = "StartTaskFromType";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartSoldierSkinDetailPanel;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_IsNeedUpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateDataCache;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_OnBgButtonClick;
    private static DelegateBridge __Hotfix_OnBuyButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnBuySuccessEnd;
    private static DelegateBridge __Hotfix_remove_EventOnBuySuccessEnd;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public StoreSoldierSkinDetailUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static StoreSoldierSkinDetailUITask StartSoldierSkinDetailPanel(
      StoreId storeId,
      int soldierSkinId,
      StoreSoldierSkinDetailUITask.StartTaskFromType startType = StoreSoldierSkinDetailUITask.StartTaskFromType.StartTaskFromStore,
      UIIntent preUIIntent = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedUpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateDataCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBuySuccessEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum StartTaskFromType
    {
      StartTaskFromGift,
      StartTaskFromHeroDetail,
      StartTaskFromStore,
    }
  }
}
