﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildJournalItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildJournalItemUIController : UIControllerBase
  {
    [AutoBind("./DateText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_dateText;
    [AutoBind("./InfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitItemInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitItemInfo(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
