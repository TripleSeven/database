﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.EventGiftBoxToggleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class EventGiftBoxToggleUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_giftBoxToggle;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./New", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMark;
    [AutoBind("./BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buyTimes;
    [AutoBind("./BuyTimes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buyTimesAnimation;
    [AutoBind("./BuyTimes/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyTimesText;
    [AutoBind("./RemainingDays", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_remainingDaysGameObject;
    [AutoBind("./RemainingDays/TimeValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_remainingDaysText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    public GiftStoreItem m_giftStoreItem;
    private DateTime m_endTime;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetToggleOn;
    private static DelegateBridge __Hotfix_UpdateData;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewRedMark;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_UpdateRemoveTime;
    private static DelegateBridge __Hotfix_OnGiftBoxClick;
    private static DelegateBridge __Hotfix_IsBoughtLimit;
    private static DelegateBridge __Hotfix_IsEventGiftValid;
    private static DelegateBridge __Hotfix_add_EventOnGiftBoxClick;
    private static DelegateBridge __Hotfix_remove_EventOnGiftBoxClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleOn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(GiftStoreItem giftStoreItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemoveTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGiftBoxClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBoughtLimit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventGiftValid()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<EventGiftBoxToggleUIController> EventOnGiftBoxClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
