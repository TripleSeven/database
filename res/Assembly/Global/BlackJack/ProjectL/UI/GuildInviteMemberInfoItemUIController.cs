﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GuildInviteMemberInfoItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GuildInviteMemberInfoItemUIController : UIControllerBase
  {
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_headIconStateCtrl;
    [AutoBind("./Char/HeadIcon/HeadIconGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconGrey;
    [AutoBind("./Char/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIcon;
    [AutoBind("./Char/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_HeadFrameDummy;
    [AutoBind("./Char/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./Char/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PeopleValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_powerText;
    [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_addButtonStateCtrl;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineStateCtrl;
    [AutoBind("./StateGroup/OffLine/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_OffLineText;
    private bool m_isAdd;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitPlayerInfo;
    private static DelegateBridge __Hotfix_OnAddClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;
    private static DelegateBridge __Hotfix_get_InvitePlayer;
    private static DelegateBridge __Hotfix_set_InvitePlayer;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayerInfo(UserSummary player, bool isInvited)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<GuildInviteMemberInfoItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public UserSummary InvitePlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
