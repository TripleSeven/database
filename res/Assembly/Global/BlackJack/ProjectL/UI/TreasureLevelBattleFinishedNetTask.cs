﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TreasureLevelBattleFinishedNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class TreasureLevelBattleFinishedNetTask : UINetTask
  {
    private int m_treasureLevelID;
    private ProBattleReport m_battleReport;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnTreasureLevelBattleFinishedAck;
    private static DelegateBridge __Hotfix_set_IsWin;
    private static DelegateBridge __Hotfix_get_IsWin;
    private static DelegateBridge __Hotfix_set_Reward;
    private static DelegateBridge __Hotfix_get_Reward;

    [MethodImpl((MethodImplOptions) 32768)]
    public TreasureLevelBattleFinishedNetTask(int treasureLevelID, ProBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTreasureLevelBattleFinishedAck(int result, bool isWin, BattleReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsWin
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleReward Reward
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
