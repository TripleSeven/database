﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BusinessCardHeroListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BusinessCardHeroListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_heroItemButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroItemUIStateController;
    [AutoBind("./HeroStars", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroStar;
    [AutoBind("./HeroTypeImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroTypeImg;
    [AutoBind("./HeroLvText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLvText;
    [AutoBind("./SSRFrameEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSSRFrameEffect;
    [AutoBind("./FrameImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroFrameImg;
    [AutoBind("./HeroIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIconImg;
    [AutoBind("./SelectFrameImg/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroIndexText;
    private int m_heroPositionIndex;
    private Hero m_hero;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroListItemInfo;
    private static DelegateBridge __Hotfix_GetHeroID;
    private static DelegateBridge __Hotfix_GetPositionIndex;
    private static DelegateBridge __Hotfix_IsSelect;
    private static DelegateBridge __Hotfix_SetNormalState;
    private static DelegateBridge __Hotfix_SetSelectState;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_add_EventOnSelectHeroItem;
    private static DelegateBridge __Hotfix_remove_EventOnSelectHeroItem;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardHeroListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListItemInfo(Hero hero, BusinessCard businessCard)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPositionIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSelect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNormalState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectState(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BusinessCardHeroListItemUIController> EventOnSelectHeroItem
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
