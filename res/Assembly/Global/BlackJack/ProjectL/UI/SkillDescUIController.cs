﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.SkillDescUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class SkillDescUIController : UIControllerBase
  {
    [AutoBind("./Lay/FrameImage/TOP/TitleNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Lay/FrameImage/TOP/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_costObj;
    [AutoBind("./Lay/FrameImage/TOP/CostBgs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_costBgsObj;
    [AutoBind("./Lay/FrameImage/TOP/CostImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_costImageObj;
    [AutoBind("./Lay/FrameImage/TOP/Type/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_typeText;
    [AutoBind("./Lay/FrameImage/TOP/CD/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_cdText;
    [AutoBind("./Lay/FrameImage/TOP/Distance/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_distanceText;
    [AutoBind("./Lay/FrameImage/TOP/Range/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rangeText;
    [AutoBind("./Lay/FrameImage/SkillType/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./Lay", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_boundaryGo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitSkillDesc;
    private static DelegateBridge __Hotfix_ShowPanel;
    private static DelegateBridge __Hotfix_ClosePanel;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_set_SkillInfo;
    private static DelegateBridge __Hotfix_get_SkillInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSkillDesc(ConfigDataSkillInfo skillInfo, bool isShowCost = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSkillInfo SkillInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
