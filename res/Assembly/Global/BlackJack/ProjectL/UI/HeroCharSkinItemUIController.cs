﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroCharSkinItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroCharSkinItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./SelectImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectImage;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./SkinNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./LimitTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGo;
    [AutoBind("./LimitTime/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeValueText;
    [AutoBind("./Champion", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_championStateCtrl;
    [AutoBind("./Champion/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_championTitleText;
    [AutoBind("./Champion/TimeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_championTimeValueText;
    private int m_index;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetHeroCharSkinItem;
    private static DelegateBridge __Hotfix_CreateDefaultItem;
    private static DelegateBridge __Hotfix_ShowSelectImage;
    private static DelegateBridge __Hotfix_OnHeroJobCardItemClick;
    private static DelegateBridge __Hotfix_GetIndex;
    private static DelegateBridge __Hotfix_SetLimitTime;
    private static DelegateBridge __Hotfix_SetChampionTitle;
    private static DelegateBridge __Hotfix_add_EventOnItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnItemClick;
    private static DelegateBridge __Hotfix_get_IsDefaultSkin;
    private static DelegateBridge __Hotfix_set_IsDefaultSkin;
    private static DelegateBridge __Hotfix_get_HeroInfo;
    private static DelegateBridge __Hotfix_set_HeroInfo;
    private static DelegateBridge __Hotfix_get_HeroSkinInfo;
    private static DelegateBridge __Hotfix_set_HeroSkinInfo;
    private static DelegateBridge __Hotfix_get_CharSkinInfo;
    private static DelegateBridge __Hotfix_set_CharSkinInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCharSkinItem(
      int heroSkinId,
      int index,
      ConfigDataHeroInfo heroInfo,
      int heroCurSkinId,
      string mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateDefaultItem(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelectImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobCardItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLimitTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChampionTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroCharSkinItemUIController> EventOnItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsDefaultSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroSkinInfo HeroSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCharImageSkinResourceInfo CharSkinInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
