﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.FriendSmallItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class FriendSmallItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemButton;
    [AutoBind("./ChatButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_chatButton;
    [AutoBind("./ButtonGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buttonGroupUIStateController;
    [AutoBind("./StateGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_friendOnlineUIStateController;
    [AutoBind("./StateGroup/OnLineText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendOnlineLongText;
    [AutoBind("./PlayerIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconImage;
    [AutoBind("./PlayerIconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_friendIconGreyImage;
    [AutoBind("./HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_friendHeadFrameTransform;
    [AutoBind("./ButtonGroup/AgreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_agreeButton;
    [AutoBind("./ButtonGroup/DisagreeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_disagreeButton;
    [AutoBind("./ButtonGroup/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addButton;
    [AutoBind("./ButtonGroup/SelectToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_selectToggle;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendLevelText;
    [AutoBind("./PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_friendNameText;
    [AutoBind("./ServerText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverText;
    [AutoBind("./InGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_inGroupButton;
    private string m_userID;
    private UserSummary m_userSummy;
    private IConfigDataLoader m_configDataLoader;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetFriendInfo;
    private static DelegateBridge __Hotfix_GetUserID;
    private static DelegateBridge __Hotfix_GetUserSummy;
    private static DelegateBridge __Hotfix_SetUserSelect;
    private static DelegateBridge __Hotfix_SetUserInGroup;
    private static DelegateBridge __Hotfix_GetSimpleInfoPostionType;
    private static DelegateBridge __Hotfix_GetPlayerSimpleInfoPos;
    private static DelegateBridge __Hotfix_OnItemButtonClick;
    private static DelegateBridge __Hotfix_OnChatButtonClick;
    private static DelegateBridge __Hotfix_OnAgreeButtonClick;
    private static DelegateBridge __Hotfix_OnDisagreeButtonClick;
    private static DelegateBridge __Hotfix_OnInGroupButtonClick;
    private static DelegateBridge __Hotfix_OnAddButtonClick;
    private static DelegateBridge __Hotfix_OnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_add_EventOnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_remove_EventOnShowPlayerSimpleInfo;
    private static DelegateBridge __Hotfix_add_EventOnAgree;
    private static DelegateBridge __Hotfix_remove_EventOnAgree;
    private static DelegateBridge __Hotfix_add_EventOnDisagree;
    private static DelegateBridge __Hotfix_remove_EventOnDisagree;
    private static DelegateBridge __Hotfix_add_EventOnAdd;
    private static DelegateBridge __Hotfix_remove_EventOnAdd;
    private static DelegateBridge __Hotfix_add_EventOnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_remove_EventOnSelectToggleValueChanged;
    private static DelegateBridge __Hotfix_add_EventOnChat;
    private static DelegateBridge __Hotfix_remove_EventOnChat;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendSmallItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendInfo(UserSummary userSummy, FriendInfoType friendInfoType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary GetUserSummy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUserSelect(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUserInGroup(bool isIn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerSimpleInfoUITask.PostionType GetSimpleInfoPostionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetPlayerSimpleInfoPos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAgreeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisagreeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInGroupButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<FriendSmallItemUIController> EventOnShowPlayerSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnAgree
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnDisagree
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool, FriendSmallItemUIController> EventOnSelectToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<FriendSmallItemUIController> EventOnChat
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
