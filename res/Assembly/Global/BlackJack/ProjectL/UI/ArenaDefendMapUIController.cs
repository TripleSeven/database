﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendMapUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class ArenaDefendMapUIController : UIControllerBase
  {
    private GameObjectPool m_stagePosition0Pool;
    private GameObjectPool m_stagePosition1Pool;
    [AutoBind("./Grid", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gridGameObject;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    private ArenaDefendBattle m_arenaDefendBattle;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_AddChildPrefab;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_ShowStagePosition;
    private static DelegateBridge __Hotfix_ShowStageAttackerPosition;
    private static DelegateBridge __Hotfix_HideStagePositions;
    private static DelegateBridge __Hotfix_PlayOnStageFx;
    private static DelegateBridge __Hotfix_StagePositionToWorldPosition;
    private static DelegateBridge __Hotfix_GridPositionToWorldPosition;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendMapUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AddChildPrefab(GameObject go, string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ArenaDefendBattle battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStagePosition(GridPosition pos, bool isEmpty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowStageAttackerPosition(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayOnStageFx(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 StagePositionToWorldPosition(GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GridPositionToWorldPosition(GridPosition p, float z)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
