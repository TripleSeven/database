﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LongPressComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  public class LongPressComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IEventSystemHandler
  {
    private bool hasInvokeLongPressEvent;
    private bool IsNeedEventOnLongPressLoop;
    private bool m_isPointDown;
    private float m_lastInvokeTime;
    private float m_delay;
    private static DelegateBridge __Hotfix_SetBasicParam;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnPointerDown;
    private static DelegateBridge __Hotfix_OnPointerUp;
    private static DelegateBridge __Hotfix_OnPointerExit;
    private static DelegateBridge __Hotfix_get_Interval;
    private static DelegateBridge __Hotfix_set_Interval;
    private static DelegateBridge __Hotfix_get_Delay;
    private static DelegateBridge __Hotfix_set_Delay;
    private static DelegateBridge __Hotfix_add_EventOnLongPress;
    private static DelegateBridge __Hotfix_remove_EventOnLongPress;
    private static DelegateBridge __Hotfix_add_EventOnPointDown;
    private static DelegateBridge __Hotfix_remove_EventOnPointDown;
    private static DelegateBridge __Hotfix_add_EventOnPointUp;
    private static DelegateBridge __Hotfix_remove_EventOnPointUp;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBasicParam(
      float delay = 1f,
      float interval = 0.3f,
      Action eventOnLongPress = null,
      Action eventOnPointDown = null,
      Action eventOnPointUp = null,
      bool isNeedEventOnLongPressLoop = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerExit(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public float Interval
    {
      [MethodImpl((MethodImplOptions) 32768)] private get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public float Delay
    {
      [MethodImpl((MethodImplOptions) 32768)] private get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action EventOnLongPress
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action EventOnPointDown
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private event Action EventOnPointUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
