﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerLevelUpUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerLevelUpUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./LevelUpInfo/LevelGroup/FromValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_oldLevelText;
    [AutoBind("./LevelUpInfo/LevelGroup/AfterValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_newLevelText;
    [AutoBind("./LevelUpInfo/HeroLevelText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addHeroLevelText;
    [AutoBind("./LevelUpInfo/APLevelText/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_addEnergyText;
    private bool m_isClick;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowPlayerLevelUp;
    private static DelegateBridge __Hotfix_Co_Open;
    private static DelegateBridge __Hotfix_Co_SetAndWaitUIState;
    private static DelegateBridge __Hotfix_Co_WaitClick;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;

    private PlayerLevelUpUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerLevelUp(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_Open(int oldLevel, int newLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetAndWaitUIState(CommonUIStateController ctrl, string state)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_WaitClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
