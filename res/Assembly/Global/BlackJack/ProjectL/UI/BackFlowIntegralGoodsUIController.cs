﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BackFlowIntegralGoodsUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BackFlowIntegralGoodsUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImage;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./IconImage/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_crystalEffect;
    [AutoBind("./SSREffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ssrEffect;
    [AutoBind("./CountValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_countValueText;
    [AutoBind("./IntegralGroup/IntegralText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_integralText;
    private BackFlowIntegralGoodsUIController.IntegralGoodsState m_integralGoodsState;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitIntegralGoodsInfo;
    private static DelegateBridge __Hotfix_SetIntegralGoodsState;
    private static DelegateBridge __Hotfix_SetIntegralGoodsInfo;
    private static DelegateBridge __Hotfix_OnItemClick;
    private static DelegateBridge __Hotfix_PlayGotChangeEffect;
    private static DelegateBridge __Hotfix_add_EventOnIntegralGoodsClick;
    private static DelegateBridge __Hotfix_remove_EventOnIntegralGoodsClick;
    private static DelegateBridge __Hotfix_set_Slot;
    private static DelegateBridge __Hotfix_get_Slot;
    private static DelegateBridge __Hotfix_set_NoviceReward;
    private static DelegateBridge __Hotfix_get_NoviceReward;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitIntegralGoodsInfo(ConfigDataRefluxRewardInfo reward, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetIntegralGoodsState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetIntegralGoodsInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayGotChangeEffect(Action OnStateFinish)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BackFlowIntegralGoodsUIController> EventOnIntegralGoodsClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Slot
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRefluxRewardInfo NoviceReward
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public enum IntegralGoodsState
    {
      Normal,
      CanGet,
      Got,
    }
  }
}
