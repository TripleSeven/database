﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TrainingSkillItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TrainingSkillItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./IconImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImageGrey;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvValueText;
    [AutoBind("./MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lvMaxText;
    [AutoBind("./SoldierNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_graphic;
    [AutoBind("./RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMark;
    private UISpineGraphic m_soldierInfoGraphic;
    public TrainingTech TrainingTech;
    public int TechMaxLv;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitTrainingSkillItemInfo;
    private static DelegateBridge __Hotfix_OnTrainingSkillItemClick;
    private static DelegateBridge __Hotfix_add_EventOnTrainingSkillItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnTrainingSkillItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTrainingSkillItemInfo(TrainingTech tech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingSkillItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<TrainingSkillItemUIController> EventOnTrainingSkillItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
