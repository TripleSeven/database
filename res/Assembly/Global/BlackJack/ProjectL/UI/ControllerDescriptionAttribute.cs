﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ControllerDescriptionAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  [AttributeUsage(AttributeTargets.All)]
  public class ControllerDescriptionAttribute : Attribute
  {
    public string prefabPath;
    public string controllerName;

    [MethodImpl((MethodImplOptions) 32768)]
    public ControllerDescriptionAttribute(string prefabPath, string controllerName)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
