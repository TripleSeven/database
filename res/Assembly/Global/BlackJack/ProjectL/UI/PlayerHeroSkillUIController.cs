﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerHeroSkillUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  public class PlayerHeroSkillUIController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
  {
    private GameObject m_popupGameObject;
    private ConfigDataSkillInfo m_skillInfo;
    private static DelegateBridge __Hotfix_SetSkillInfoAndDescObj;
    private static DelegateBridge __Hotfix_OnPointerDown;
    private static DelegateBridge __Hotfix_OnPointerUp;
    private static DelegateBridge __Hotfix_add_EventOnSkillClick;
    private static DelegateBridge __Hotfix_remove_EventOnSkillClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkillInfoAndDescObj(ConfigDataSkillInfo skillInfo, GameObject descObj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ConfigDataSkillInfo> EventOnSkillClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
