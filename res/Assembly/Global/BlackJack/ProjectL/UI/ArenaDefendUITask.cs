﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaDefendUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.UI
{
  public class ArenaDefendUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ArenaDefendUIController m_arenaDefendUIController;
    private ArenaDefendMapUIController m_arenaDefendMapUIController;
    private ArenaDefendSceneUIController m_arenaDefendSceneUIController;
    private BattlePrepareActorInfoUIController m_battlePrepareActorInfoUIController;
    private ArenaDefendBattle m_arenaDefendBattle;
    private List<GridPosition> m_defendPositions;
    private List<GridPosition> m_attackPositions;
    private List<ConfigDataArenaBattleInfo> m_arenaBattleInfos;
    private List<ConfigDataArenaDefendRuleInfo> m_defendRuleInfos;
    private List<BattleHero> m_playerBattleHeros;
    private List<BattleHero> m_defendStageHeros;
    private List<TrainingTech> m_trainingTechs;
    private int m_curBattleIndex;
    private int m_curDefendRuleIndex;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectBattlefieldAssets;
    private static DelegateBridge __Hotfix_CollectHeroAndSoldierModelAssets;
    private static DelegateBridge __Hotfix_CollectHeadImageAssets;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_InitArenaDefendUIController;
    private static DelegateBridge __Hotfix_UninitArenaDefendUIController;
    private static DelegateBridge __Hotfix_OnMemoryWarning;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_SetupStageActors;
    private static DelegateBridge __Hotfix_ShowStagePositions;
    private static DelegateBridge __Hotfix_ClearMapAndActors;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnReturn;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnSave;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnShowActionOrderPanel;
    private static DelegateBridge __Hotfix_UpdateStageHeroActionValues;
    private static DelegateBridge __Hotfix_CompareHeroActionValue;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnShowMapPanel;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnShowDefendRulePanel;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnConfirmActionOrder;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnConfirmMap;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnConfirmDefendRule;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnShowMyActorInfo;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnHideActorInfo;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnStageActorChange;
    private static DelegateBridge __Hotfix_UpdateBattlePower;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnHeroOnStage;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnActorOffStage;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnStageActorMove;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnStageActorSwap;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnPointerDown;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnPointerUp;
    private static DelegateBridge __Hotfix_ArenaDefendUIController_OnPointerClick;
    private static DelegateBridge __Hotfix_GetHeroFromBattleHero;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnShowSelectSkillPanel;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnChangeSkill;
    private static DelegateBridge __Hotfix_BattlePrepareActorInfoUIController_OnChangeSoldier;
    private static DelegateBridge __Hotfix_ArenaDefendSceneUIController_OnPointerDown;
    private static DelegateBridge __Hotfix_ArenaDefendSceneUIController_OnPointerUp;
    private static DelegateBridge __Hotfix_ArenaDefendSceneUIController_OnPointerClick;
    private static DelegateBridge __Hotfix_ArenaDefendSceneUIController_OnBeginDrag;
    private static DelegateBridge __Hotfix_ArenaDefendSceneUIController_OnEndDrag;
    private static DelegateBridge __Hotfix_ArenaDefendSceneUIController_OnDrag;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaDefendUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBattlefieldAssets(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeroAndSoldierModelAssets(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectHeadImageAssets(ConfigDataCharImageInfo charImageInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UninitArenaDefendUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupStageActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowStagePositions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapAndActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnSave()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowActionOrderPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateStageHeroActionValues()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHeroActionValue(BattleHero hero0, BattleHero hero1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowMapPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowDefendRulePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmActionOrder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmMap(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnConfirmDefendRule(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnShowMyActorInfo(BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnHideActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnHeroOnStage(BattleHero hero, GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnActorOffStage(ArenaDefendActor sa)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorMove(ArenaDefendActor sa, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnStageActorSwap(
      ArenaDefendActor sa1,
      ArenaDefendActor sa2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerDown(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerUp(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendUIController_OnPointerClick(
      PointerEventData.InputButton button,
      Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Hero GetHeroFromBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSkillPanel(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnShowSelectSoldierPanel(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSkill(
      BattleHero battleHero,
      List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BattlePrepareActorInfoUIController_OnChangeSoldier(
      BattleHero battleHero,
      int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnPointerDown(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnPointerUp(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnPointerClick(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnBeginDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnEndDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ArenaDefendSceneUIController_OnDrag(PointerEventData eventData)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
