﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallSelfRankAllUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallSelfRankAllUIController : UIControllerBase
  {
    [AutoBind("./PlayerInfo/PlayerIcon/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerInfo/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./PlayerInfo/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./PlayerInfo/PlayerIcon/RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText;
    [AutoBind("./PlayerInfo/PlayerIcon/RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankImage;
    [AutoBind("./PlayerInfo/PlayerIcon/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./PlayerInfo/PlayerIcon/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private RankingListInfo m_rankingListInfo;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetRankPlayerInfo;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_GetRankLevelSprite;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRankPlayerInfo(RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
