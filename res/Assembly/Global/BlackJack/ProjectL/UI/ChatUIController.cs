﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ChatUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using MarchingBytes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ChatUIController : UIControllerBase
  {
    private bool m_InputFocused;
    private Action<ChatComponent.ChatMessageClient> m_itemVoiceButtonClickEvent;
    private Action<ChatMessage, GameObject> m_itemPlayerIconClickEvent;
    private Action<ulong, ChatBagItemMessage> m_heroEquipmentLinkClickAction;
    private Action<ChatPeakArenaBattleReportMessage> m_battleReportClickAction;
    private ChatChannel m_currentTableType;
    private List<ChatComponent.ChatMessageClient> m_currentChatList;
    private List<GameObject> m_redMarkObjs;
    private ProjectLPlayerContext m_playerContext;
    private FieldInfo m_inputPanelFieldInfo_AllowInput;
    private Coroutine m_coroutineTalkButtonTestHolding;
    private IConfigDataLoader m_configDataLoader;
    private int m_lastUpdateChatInfoListMaxIndex;
    private List<string> m_idList;
    private List<string> m_nameList;
    private ChatComponent.ChatMessageClient m_currPlayVoiceMsg;
    private ChatChannel m_currentChannel;
    private Button m_lastSelectButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/InputField", AutoBindAttribute.InitState.NotInit, false)]
    public InputField RoomInput;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/InputField/Placeholder", AutoBindAttribute.InitState.NotInit, false)]
    public Text RoomIDShowText;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text RoomRequireIDText;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject RoomIdShowRoot;
    [AutoBind("./ChatPanel/ChatDetail/RoomIDInput/ChangeRoomButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ChangeRoomButton;
    [AutoBind("./ChatPanel/SoundMessage/Send/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text voiceTimeText;
    [AutoBind("./ChatPanel/SoundMessage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController voiceRecordStateUICtrl;
    [AutoBind("./ChatPanel/SoundMessage/Send/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image recordTimeProgressBar;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool m_objPool;
    [AutoBind("./ChatPanel/ChatDetail/UnusedRoot", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject UnusedRoot;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ListScrollViewContent;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView/", AutoBindAttribute.InitState.NotInit, false)]
    public ChatLoopVerticalScrollRect ScrollView;
    [AutoBind("./ChatPanel/ChatDetail/ListScrollView/Viewport", AutoBindAttribute.InitState.NotInit, false)]
    public RectTransform ViewPort;
    [AutoBind("./ChatPanel/ChannelPanel/WorldToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button WorldButton;
    [AutoBind("./ChatPanel/ChannelPanel/SystemToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button SystemButton;
    [AutoBind("./ChatPanel/ChannelPanel/TeamToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button TeamButton;
    [AutoBind("./ChatPanel/ChannelPanel/GroupToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button GroupButton;
    [AutoBind("./ChatPanel/ChannelPanel/PrivateToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button PrivateButton;
    [AutoBind("./ChatPanel/ChannelPanel/SociatyToggle", AutoBindAttribute.InitState.NotInit, false)]
    public Button GuildButton;
    [AutoBind("./ChatPanel/ChannelPanel/WorldToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject WorldToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/SystemToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject SystemToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/TeamToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject TeamToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/GroupToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject GroupToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/PrivateToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject PrivateToggleRedMark;
    [AutoBind("./ChatPanel/ChannelPanel/SociatyToggle/RedMark", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject GuildToggleRedMark;
    [AutoBind("./ChatPanel/CloseBtn", AutoBindAttribute.InitState.NotInit, false)]
    public Button CloseButton;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button BGButton;
    [AutoBind("./EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject EmptyPanel;
    [AutoBind("./ChatPanel/NewChatTip", AutoBindAttribute.InitState.NotInit, false)]
    public Button NewChatTip;
    [AutoBind("./ChatPanel/InputPanel/TalkButtonImage/TalkButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button TalkButton;
    [AutoBind("./ChatPanel/InputPanel/EmoticonButton", AutoBindAttribute.InitState.NotInit, false)]
    public Button ExpressionFunctionButton;
    [AutoBind("./ChatPanel/InputPanel/InputField", AutoBindAttribute.InitState.NotInit, false)]
    public InputField InputPanel;
    [AutoBind("./ChatPanel/InputPanel/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text Placeholder;
    [AutoBind("./ChatPanel/InputPanel/InputField/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text ContentText;
    [AutoBind("./ChatPanel/InputPanel/SendButton", AutoBindAttribute.InitState.NotInit, false)]
    public ButtonEx SendButton;
    [AutoBind("./ChatPanel/InputPanel/CantInput", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject CantInputObj;
    [AutoBind("./ChatPanel/ChatDetail/PrivatePlayerChoose", AutoBindAttribute.InitState.NotInit, false)]
    public GameObject ChooseListRoot;
    [AutoBind("./ChatPanel/ChatDetail/PrivatePlayerChoose", AutoBindAttribute.InitState.NotInit, false)]
    public Dropdown ChatTargetChooseDropdown;
    [AutoBind("./ChatPanel/ChatDetail/PrivatePlayerChoose/Label", AutoBindAttribute.InitState.NotInit, false)]
    public Text PlayerNameLabel;
    private const string emojiInfoTxtName = "EmojiUnicodeInfo";
    private const string emojiShowAssetName = "EmojiShowImage";
    private const string ChatManagerItem = "ChatManagerItem";
    private const string LeftWorldChatItem = "WorldChatItemLeft";
    private const string RightWorldChatItem = "WorldChatItemRight";
    private const string LeftSystemChatItem = "SystemChatItem";
    private const string ChatManagerItemPoolName = "ChatManagerPool";
    private const string LeftWorldChatItemPoolName = "WorldLeftPool";
    private const string RightWorldChatItemPoolName = "WorldRightPool";
    private const string LeftSystemChatItemPoolName = "SystemPool";
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnPoolObjectCreated;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnRoomIdInputEnd;
    private static DelegateBridge __Hotfix_OnExpressionFunctionButtonClick;
    private static DelegateBridge __Hotfix_OnTalkButtonClickDown;
    private static DelegateBridge __Hotfix_OnTalkButtonTestHoding;
    private static DelegateBridge __Hotfix_OnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_OnExitTalkButton;
    private static DelegateBridge __Hotfix_OnEnterTalkButton;
    private static DelegateBridge __Hotfix_OnSendButtonClick;
    private static DelegateBridge __Hotfix_OnCloseButtonClick;
    private static DelegateBridge __Hotfix_OnTableSelected;
    private static DelegateBridge __Hotfix_OnChatTargetChanged;
    private static DelegateBridge __Hotfix_OnChooseTargetRootClick;
    private static DelegateBridge __Hotfix_OnChangeRoomButtonClick;
    private static DelegateBridge __Hotfix_RefreshDropDownList;
    private static DelegateBridge __Hotfix_InputPanel_OnEndEdit;
    private static DelegateBridge __Hotfix_ShowOrHidePanel;
    private static DelegateBridge __Hotfix_ShowNewChatTip;
    private static DelegateBridge __Hotfix_InputFieldTextAppend;
    private static DelegateBridge __Hotfix_UpdateChatList;
    private static DelegateBridge __Hotfix_UpdateChatCacheInfo;
    private static DelegateBridge __Hotfix_GetChannelToggleSelectState;
    private static DelegateBridge __Hotfix_SetChannelToggleSelected;
    private static DelegateBridge __Hotfix_IsScrollViewInEnd;
    private static DelegateBridge __Hotfix_UpdateChooseChatPlayerOrGroupListInfo;
    private static DelegateBridge __Hotfix_ShowOrHideChooseChatTargetPannel;
    private static DelegateBridge __Hotfix_SetTalkButtonSize;
    private static DelegateBridge __Hotfix_ShowVoiceRecordTip;
    private static DelegateBridge __Hotfix_HideVoiceRecordTip;
    private static DelegateBridge __Hotfix_ShowVoiceCancelTip;
    private static DelegateBridge __Hotfix_ShowVoiceShortTip;
    private static DelegateBridge __Hotfix_UpdateVoiceRecordTime;
    private static DelegateBridge __Hotfix_RegItemButtonClickEvent;
    private static DelegateBridge __Hotfix_SetRoomIdTip;
    private static DelegateBridge __Hotfix_EnableRoomIdShow;
    private static DelegateBridge __Hotfix_CloseAllTableRedMark;
    private static DelegateBridge __Hotfix_SetTableRedMark;
    private static DelegateBridge __Hotfix_ClearChatContent;
    private static DelegateBridge __Hotfix_OnChatItemFill;
    private static DelegateBridge __Hotfix_GetShowChatItemPoolName4Info;
    private static DelegateBridge __Hotfix_SetScrollViewToBottom;
    private static DelegateBridge __Hotfix_FindChannelButton;
    private static DelegateBridge __Hotfix_add_EventOnTableChange;
    private static DelegateBridge __Hotfix_remove_EventOnTableChange;
    private static DelegateBridge __Hotfix_add_EventOnSendButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSendButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCloseButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnCloseButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnExpressionFunctionButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnExpressionFunctionButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnRoomIdInputEnd;
    private static DelegateBridge __Hotfix_remove_EventOnRoomIdInputEnd;
    private static DelegateBridge __Hotfix_add_EventOnTalkButtonHold;
    private static DelegateBridge __Hotfix_remove_EventOnTalkButtonHold;
    private static DelegateBridge __Hotfix_add_EventOnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_remove_EventOnTalkButtonClickUp;
    private static DelegateBridge __Hotfix_add_EventOnExitTalkButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnExitTalkButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnEnterTalkButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnEnterTalkButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnChatTargetChanged;
    private static DelegateBridge __Hotfix_remove_EventOnChatTargetChanged;
    private static DelegateBridge __Hotfix_get_CurrPlayVoiceMsg;
    private static DelegateBridge __Hotfix_set_CurrPlayVoiceMsg;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRoomIdInputEnd(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExpressionFunctionButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickDown(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator OnTalkButtonTestHoding()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalkButtonClickUp(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExitTalkButton(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnterTalkButton(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTableSelected(Button currentButton)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatTargetChanged(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChooseTargetRootClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeRoomButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshDropDownList(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InputPanel_OnEndEdit(string str)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHidePanel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNewChatTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InputFieldTextAppend(string appendStr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatList(
      List<ChatComponent.ChatMessageClient> currentChatList,
      ChatChannel channel,
      bool isRefill,
      bool isScroll = false,
      bool hasNew = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChatCacheInfo(List<ChatComponent.ChatMessageClient> chatInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetChannelToggleSelectState(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChannelToggleSelected(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScrollViewInEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateChooseChatPlayerOrGroupListInfo(
      ChatChannel channel,
      List<string> idList,
      List<string> nameList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOrHideChooseChatTargetPannel(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTalkButtonSize(Vector2 size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceCancelTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceShortTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateVoiceRecordTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegItemButtonClickEvent(
      Action<ChatComponent.ChatMessageClient> voiceClickAction,
      Action<ChatMessage, GameObject> playerIconClickAction,
      Action<ulong, ChatBagItemMessage> heroEquipmentLinkClickAction,
      Action<ChatPeakArenaBattleReportMessage> battleReportClickAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRoomIdTip(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableRoomIdShow(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CloseAllTableRedMark()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTableRedMark(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearChatContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChatItemFill(UIControllerBase uCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetShowChatItemPoolName4Info(ChatMessage chatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetScrollViewToBottom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Button FindChannelButton(ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<ChatChannel> EventOnTableChange
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnSendButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCloseButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExpressionFunctionButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnRoomIdInputEnd
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTalkButtonHold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnTalkButtonClickUp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnExitTalkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnEnterTalkButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ChatChannel, string, string> EventOnChatTargetChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ChatComponent.ChatMessageClient CurrPlayVoiceMsg
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
