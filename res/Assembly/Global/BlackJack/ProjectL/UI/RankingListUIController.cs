﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RankingListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using MarchingBytes;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RankingListUIController : UIControllerBase
  {
    protected GameObject m_itemTemplete;
    protected RankingListInfo m_cachedRankList;
    private int m_curSelectSeasonId;
    protected const string RankListItemPrefabName = "RankingListItemUIPrefab";
    [AutoBind("./ItemRoot", AutoBindAttribute.InitState.NotInit, false)]
    public Transform ItemRoot;
    [AutoBind("./RankingListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public EasyObjectPool RankListItemPool;
    [AutoBind("./PlayerInfoGroup", AutoBindAttribute.InitState.NotInit, false)]
    public SelfRankingListItemUIController SelfRankItemUICtrl;
    [AutoBind("./RankingListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public LoopVerticalScrollRect RankListScrollRect;
    [AutoBind("./RankingListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController RankListStateCtrl;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_CreateItemListPool;
    private static DelegateBridge __Hotfix_OnPoolObjectCreated;
    private static DelegateBridge __Hotfix_UpdateRankingListInfo;
    private static DelegateBridge __Hotfix_UpdateSelfRankInfo;
    private static DelegateBridge __Hotfix_OnRankItemFill;
    private static DelegateBridge __Hotfix_OnRankItemClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateItemListPool(int poolSize)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPoolObjectCreated(string poolName, GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRankingListInfo(RankingListInfo rankList, int curSelectSeasonId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateSelfRankInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankItemFill(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnRankItemClick(UIControllerBase itemCtrl)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
