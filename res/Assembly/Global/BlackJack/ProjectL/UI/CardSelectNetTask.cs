﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CardSelectNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class CardSelectNetTask : UINetTask
  {
    private int m_cardPoolId;
    private bool m_isSingleSlect;
    private bool m_isUsingTicket;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_OnCardSelectAck;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_set_Rewards;
    private static DelegateBridge __Hotfix_get_Rewards;
    private static DelegateBridge __Hotfix_set_ExtraRewards;
    private static DelegateBridge __Hotfix_get_ExtraRewards;

    [MethodImpl((MethodImplOptions) 32768)]
    public CardSelectNetTask(int cardPoolId, bool isSingleSlect, bool isUsingTicket = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCardSelectAck(int result, List<Goods> rewards, List<Goods> extraRewards)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Goods> Rewards
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<Goods> ExtraRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
