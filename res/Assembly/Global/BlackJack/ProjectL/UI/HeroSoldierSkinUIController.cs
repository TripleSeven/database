﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSoldierSkinUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroSoldierSkinUIController : UIControllerBase
  {
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rootGo;
    [AutoBind("./LayoutRoot/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_scrollContent;
    [AutoBind("./LayoutRoot/ShowTogglePanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_showToggleStateCtrl;
    [AutoBind("./LayoutRoot/ShowTogglePanel/HeroCurrentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_showHeroToggle;
    [AutoBind("./LayoutRoot/ShowTogglePanel/SoldierCurrentButton", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_showSoliderToggle;
    [AutoBind("./ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_infoPanelStateCtrl;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoTitleText;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoDescText;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/HeroInfoPanel/JobScrollView/Viewport/JobGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroJobContent;
    [AutoBind("./ResonanceInfoPanel/InfoPanel/HeroInfoPanel/JobScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_infoHeroJobScrollRect;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/GetOn", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoSwitchButton;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/Unavailable", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoUnavailableButton;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/Buy", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoBuyButton;
    [AutoBind("./ResonanceInfoPanel/ButtonGroup/FromText/Title/DetailText ", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoFromDetailText;
    [AutoBind("./Prefab/Hero&SoldierSkinItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroSoldierSkinItemPrefab;
    private Hero m_hero;
    private bool m_isFromHeroTab;
    private ConfigDataSoldierInfo m_soldierInfo;
    private HeroOrSoliderSkinUIController m_curSelectHeroSkinItemCtrl;
    private List<HeroOrSoliderSkinUIController> m_itemCtrlList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInHeroSoldierSkin;
    private static DelegateBridge __Hotfix_LateUpdate;
    private static DelegateBridge __Hotfix_ShowHeroSkin;
    private static DelegateBridge __Hotfix_CreatDefaultHeroSkin;
    private static DelegateBridge __Hotfix_SetHeroInfoPanel;
    private static DelegateBridge __Hotfix_SetHeroButtonState;
    private static DelegateBridge __Hotfix_ShowSoldierSkin;
    private static DelegateBridge __Hotfix_CreateDefaultSoldier;
    private static DelegateBridge __Hotfix_SetSoldierInfoPanel;
    private static DelegateBridge __Hotfix_OnSkinItemClick;
    private static DelegateBridge __Hotfix_OnShowHeroToggleValueChanged;
    private static DelegateBridge __Hotfix_OnShowSoldierToggleValueChanged;
    private static DelegateBridge __Hotfix_OnInfoUnavailableButtonClick;
    private static DelegateBridge __Hotfix_OnInfoBuyButtonClick;
    private static DelegateBridge __Hotfix_OnInfoSwitchButtonClick;
    private static DelegateBridge __Hotfix_ShowSwitchHeroCharSkinPanel;
    private static DelegateBridge __Hotfix_ShowSwitchDefaultHeroCharSkinPanel;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnBuyHeroSkin;
    private static DelegateBridge __Hotfix_remove_EventOnBuyHeroSkin;
    private static DelegateBridge __Hotfix_add_EventOnBuySoldierSkin;
    private static DelegateBridge __Hotfix_remove_EventOnBuySoldierSkin;
    private static DelegateBridge __Hotfix_add_EventOnTakeOffModelSkin;
    private static DelegateBridge __Hotfix_remove_EventOnTakeOffModelSkin;
    private static DelegateBridge __Hotfix_add_EventOnTakeOffSoldierSkin;
    private static DelegateBridge __Hotfix_remove_EventOnTakeOffSoldierSkin;
    private static DelegateBridge __Hotfix_add_EventOnWearModelSkin;
    private static DelegateBridge __Hotfix_remove_EventOnWearModelSkin;
    private static DelegateBridge __Hotfix_add_EventOnWearSoldierSkin;
    private static DelegateBridge __Hotfix_remove_EventOnWearSoldierSkin;
    private static DelegateBridge __Hotfix_add_EventOnWearCharSkin;
    private static DelegateBridge __Hotfix_remove_EventOnWearCharSkin;
    private static DelegateBridge __Hotfix_add_EventOnTakeOffCharSkin;
    private static DelegateBridge __Hotfix_remove_EventOnTakeOffCharSkin;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSoldierSkinUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInHeroSoldierSkin(
      Hero hero,
      bool isFromHeroTab,
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeroSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreatDefaultHeroSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroInfoPanel(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroButtonState(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSoldierSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateDefaultSoldier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierInfoPanel(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinItemClick(HeroOrSoliderSkinUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowHeroToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowSoldierToggleValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoUnavailableButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInfoSwitchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchHeroCharSkinPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowSwitchDefaultHeroCharSkinPanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuyHeroSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnBuySoldierSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, Action> EventOnTakeOffModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnTakeOffSoldierSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int, Action> EventOnWearModelSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int, int> EventOnWearSoldierSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> EventOnWearCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnTakeOffCharSkin
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
