﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleTeamPlayerUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectLBasic;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleTeamPlayerUIController : UIControllerBase
  {
    [AutoBind("./PlayerIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_headIconImage;
    [AutoBind("./PlayerIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./PlayerTag", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerTagImage;
    [AutoBind("./Chat", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_chatUIStateController;
    [AutoBind("./Chat/BGImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private EmojiText m_dialogText;
    [AutoBind("./Chat/BGImage/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_expressionImage;
    [AutoBind("./Chat/BGImage/Voice", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_voiceButton;
    [AutoBind("./Chat/BGImage/Voice/Voice/SpeakImage", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_voiceSpeakImageStateCtrl;
    [AutoBind("./Chat/BGImage/Voice/Voice/TimeButton/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceTimeButtonText;
    [AutoBind("./Chat/BGImage/Voice/ContentText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_voiceContentText;
    [AutoBind("./StateGroup/ActionImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusActionGameObject;
    [AutoBind("./StateGroup/ReadyImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusReadyGameObject;
    [AutoBind("./StateGroup/AutoImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusAutoGameObject;
    [AutoBind("./StateGroup/OfflineImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_statusOfflineGameObject;
    private float m_hideChatTime;
    private const int HeroPointCountMax = 5;
    private ChatVoiceMessage m_voiceMessage;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitEmojiText;
    private static DelegateBridge __Hotfix_SetStatus;
    private static DelegateBridge __Hotfix_SetAction;
    private static DelegateBridge __Hotfix_SetHeadIcon;
    private static DelegateBridge __Hotfix_SetName;
    private static DelegateBridge __Hotfix_SetLevel;
    private static DelegateBridge __Hotfix_SetPlayerIndex;
    private static DelegateBridge __Hotfix_ShowPlayerIndex;
    private static DelegateBridge __Hotfix_SetHeroCount;
    private static DelegateBridge __Hotfix_SetHeroAlive;
    private static DelegateBridge __Hotfix_ShowChat;
    private static DelegateBridge __Hotfix_ShowBigExpression;
    private static DelegateBridge __Hotfix_ShowVoice;
    private static DelegateBridge __Hotfix_OnVoiceTimeButtonClick;
    private static DelegateBridge __Hotfix_HideChat;
    private static DelegateBridge __Hotfix_HideBigExpression;
    private static DelegateBridge __Hotfix_GetHeroPointBGGameObject;
    private static DelegateBridge __Hotfix_GetHeroPointGameObject;
    private static DelegateBridge __Hotfix_GetHeroRedPointGameObject;
    private static DelegateBridge __Hotfix_Update;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEmojiText(SmallExpressionParseDesc expressionDesc, Image image)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(PlayerBattleStatus status, bool isOffline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAction(bool isAction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeadIcon(int headIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndex(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroAlive(int heroIdx, bool isAlive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowChat(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBigExpression(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoice(ChatVoiceMessage voiceMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnVoiceTimeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideBigExpression()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroPointBGGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroPointGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetHeroRedPointGameObject(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
