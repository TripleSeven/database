﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AnikiGymListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AnikiGymListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./NameTextGray", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextGray;
    [AutoBind("./BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./BGImageGray", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImageGray;
    [AutoBind("./OpenTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_openTimeText;
    [AutoBind("./Time", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_timeGameObject;
    [AutoBind("./Time/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeText;
    [AutoBind("./NotOpenButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_notOpenButton;
    private ConfigDataAnikiGymInfo m_anikiGymInfo;
    private bool m_isLocked;
    private DateTime m_expireTime;
    private float m_updateCountdown;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetAnikiGymInfo;
    private static DelegateBridge __Hotfix_GetAnikiGymInfo;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_SetSelected;
    private static DelegateBridge __Hotfix_IsSelected;
    private static DelegateBridge __Hotfix_SetExpireTime;
    private static DelegateBridge __Hotfix_UpdateRemainTime;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_OnNotOpenButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnToggleValueChanged;
    private static DelegateBridge __Hotfix_remove_EventOnToggleValueChanged;

    [MethodImpl((MethodImplOptions) 32768)]
    public AnikiGymListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnikiGymInfo(ConfigDataAnikiGymInfo anikiGymInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataAnikiGymInfo GetAnikiGymInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool locked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelected(bool selected)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSelected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetExpireTime(DateTime expireTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRemainTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNotOpenButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, AnikiGymListItemUIController> EventOnToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
