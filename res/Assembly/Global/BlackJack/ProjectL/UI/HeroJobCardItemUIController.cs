﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobCardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroJobCardItemUIController : UIControllerBase
  {
    [AutoBind("./Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_icon;
    [AutoBind("./CurJobImg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curJobImg;
    [AutoBind("./MagicStone", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_magicStone;
    [AutoBind("./LockImg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_lockImg;
    [AutoBind("./Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Frame", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_frameImg;
    [AutoBind("./RedIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redIcon;
    [AutoBind("./Master", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_masterIcon;
    private Hero m_hero;
    private int m_rank;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitHeroJobCardItem;
    private static DelegateBridge __Hotfix_SetInfoByState;
    private static DelegateBridge __Hotfix_SetFrameImage;
    private static DelegateBridge __Hotfix_IsJobMaster;
    private static DelegateBridge __Hotfix_IsLight;
    private static DelegateBridge __Hotfix_OnHeroJobCardItemClick;
    private static DelegateBridge __Hotfix_add_EventOnJobCardItemClick;
    private static DelegateBridge __Hotfix_remove_EventOnJobCardItemClick;
    private static DelegateBridge __Hotfix_set_JobConnectionInfo;
    private static DelegateBridge __Hotfix_get_JobConnectionInfo;
    private static DelegateBridge __Hotfix_set_IsNeedMagicStone;
    private static DelegateBridge __Hotfix_get_IsNeedMagicStone;
    private static DelegateBridge __Hotfix_set_IsJobLocked;
    private static DelegateBridge __Hotfix_get_IsJobLocked;
    private static DelegateBridge __Hotfix_set_HaveRedIcon;
    private static DelegateBridge __Hotfix_get_HaveRedIcon;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroJobCardItem(
      Hero hero,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetInfoByState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFrameImage(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobMaster()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLight()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroJobCardItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroJobCardItemUIController> EventOnJobCardItemClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsNeedMagicStone
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsJobLocked
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HaveRedIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
