﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDungeonLevelListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDungeonLevelListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_button;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Detail/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Detail/StarImage1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star1GameObject;
    [AutoBind("./Detail/StarImage2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star2GameObject;
    [AutoBind("./Detail/StarImage3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_star3GameObject;
    [AutoBind("./Detail/Trophy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_trophyGameObject;
    [AutoBind("./Detail/Trophy/TrophyValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_trophyValueText;
    [AutoBind("./Lock/FavorLevelNotEnoughText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_favorLevelNotEnoughText;
    [AutoBind("./Lock/PreLevelNotCompleteText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_preLevelNotCompleteText;
    [AutoBind("./Detail/PieceImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_pieceImage;
    [AutoBind("./Detail/CountDetail/CountValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lastCountValueText;
    private ConfigDataHeroDungeonLevelInfo m_levelInfo;
    private int m_index;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetDungeonInfo;
    private static DelegateBridge __Hotfix_GetDungeonInfo;
    private static DelegateBridge __Hotfix_SetIndex;
    private static DelegateBridge __Hotfix_GetIndex;
    private static DelegateBridge __Hotfix_IsFavorLevelLock;
    private static DelegateBridge __Hotfix_IsPreLevelLock;
    private static DelegateBridge __Hotfix_OnClick;
    private static DelegateBridge __Hotfix_add_EventOnClick;
    private static DelegateBridge __Hotfix_remove_EventOnClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevelListItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDungeonInfo(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataHeroDungeonLevelInfo GetDungeonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFavorLevelLock()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPreLevelLock()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<HeroDungeonLevelListItemUIController> EventOnClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
