﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.JobMaterialEquipedUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class JobMaterialEquipedUIController : UIControllerBase
  {
    [AutoBind("./ImageButton", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImg;
    [AutoBind("./BgImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_bgImg;
    [AutoBind("./ValueGroup/NeedCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_needCountText;
    [AutoBind("./ValueGroup/HaveCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_haveCountText;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitJobMaterial;
    private static DelegateBridge __Hotfix_OnJobMaterialClick;
    private static DelegateBridge __Hotfix_add_EventOnJobMaterialClick;
    private static DelegateBridge __Hotfix_remove_EventOnJobMaterialClick;
    private static DelegateBridge __Hotfix_set_NeedCount;
    private static DelegateBridge __Hotfix_get_NeedCount;
    private static DelegateBridge __Hotfix_set_HaveCount;
    private static DelegateBridge __Hotfix_get_HaveCount;
    private static DelegateBridge __Hotfix_set_JobMaterialInfo;
    private static DelegateBridge __Hotfix_get_JobMaterialInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitJobMaterial(Goods jobMaterialGood)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobMaterialClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<JobMaterialEquipedUIController> EventOnJobMaterialClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int NeedCount
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HaveCount
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobMaterialInfo JobMaterialInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
