﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleActorInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.Scene;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class BattleActorInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Hero/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroNameText;
    [AutoBind("./Hero/Army/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroArmyText;
    [AutoBind("./HeroPanelGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_heroPanelGroupCtrl;
    [AutoBind("./HeroPanelGroup/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroIcon;
    [AutoBind("./Hero/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroArmyIcon;
    [AutoBind("./Hero/Job/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroJobText;
    [AutoBind("./Hero/Level/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroLevelText;
    [AutoBind("./Hero/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroHPText;
    [AutoBind("./Hero/DEX/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDEXText;
    [AutoBind("./Hero/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroHPImage;
    [AutoBind("./Hero/Attack/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroAttackText;
    [AutoBind("./Hero/Defense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroDefenseText;
    [AutoBind("./Hero/Magic/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicText;
    [AutoBind("./Hero/MagicDefense/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMagicDefenseText;
    [AutoBind("./Hero/Range/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroRangeText;
    [AutoBind("./Hero/Move/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroMoveText;
    [AutoBind("./Soldier", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGameObject;
    [AutoBind("./Soldier/Name/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierNameText;
    [AutoBind("./Soldier/Army/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierArmyIcon;
    [AutoBind("./Soldier/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicGameObject;
    [AutoBind("./Soldier/HP/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierHPImage;
    [AutoBind("./Soldier/HP/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierHPText;
    [AutoBind("./Soldier/Prop/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAttackText;
    [AutoBind("./Soldier/Prop/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDefenseText;
    [AutoBind("./Soldier/Prop/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFText;
    [AutoBind("./Soldier/Prop/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierAttackAddText;
    [AutoBind("./Soldier/Prop/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDefenseAddText;
    [AutoBind("./Soldier/Prop/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierMagicDFAddText;
    [AutoBind("./Hero/SkillInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillInfoButtonGo;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/TalentSkill", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillInfoGroupTalentSkillImage;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo2;
    [AutoBind("./Hero/SkillInfoButton/SkillGroup/SkillIcon3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillInfoGroupSkillGo3;
    [AutoBind("./SkillDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailPanel;
    [AutoBind("./SkillDetailPanel/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDetailListGameObject;
    [AutoBind("./SkillDetailPanel/List/Content/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillTalentGameObject;
    [AutoBind("./SkillDetailPanel/List/Content/Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillTalentIcon;
    [AutoBind("./SkillDetailPanel/List/Content/Talent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillTalentNameText;
    [AutoBind("./SkillDetailPanel/List/Content/Talent/DescTextScrollRect/Mask/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillTalentDescText;
    [AutoBind("./SkillDetailPanel/List/Content/LineImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_skillLineImage;
    [AutoBind("./SkillDetailPanel/List/Content/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListGameObject;
    [AutoBind("./SkillDetailPanel/List/NoSkills", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noSkillsAndTalentGameObject;
    [AutoBind("./SkillDetailPanel/List/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillListContentGameObject;
    [AutoBind("./DebugBuffToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_debugBuffToggle;
    [AutoBind("./BuffList", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buffsListGameObject;
    [AutoBind("./BuffsDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffDescDetailGameObject;
    [AutoBind("./BuffsDetailPanel/List", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffsDescDetailListGameObject;
    [AutoBind("./BuffsDetailPanel/List/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffsDescContentGameObject;
    [AutoBind("./BuffsDetailPanel/List/NoBuff", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noBuffsGameObject;
    [AutoBind("./Soldier/Info", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_soldierDetailButton;
    [AutoBind("./SoldierDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_soldierDetailPanelUIStateController;
    [AutoBind("./SoldierDetailPanel/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierGraphicObj;
    [AutoBind("./SoldierDetailPanel/SoldierIconImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_soldierIconImg;
    [AutoBind("./SoldierDetailPanel/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierTitleText;
    [AutoBind("./SoldierDetailPanel/Desc/DescTextScroll/Mask/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescText;
    [AutoBind("./SoldierDetailPanel/Faction/Melee", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDescMeleeGo;
    [AutoBind("./SoldierDetailPanel/Faction/NotMelee", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_soldierDescNotMeleeGo;
    [AutoBind("./SoldierDetailPanel/Faction/RangeValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescRangeValueText;
    [AutoBind("./SoldierDetailPanel/Faction/MoveValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescMoveValueText;
    [AutoBind("./SoldierDetailPanel/Desc/Restrain/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescRestrainValueText;
    [AutoBind("./SoldierDetailPanel/Desc/Weak/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierDescWeakValueText;
    [AutoBind("./SoldierDetailPanel/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPValueText;
    [AutoBind("./SoldierDetailPanel/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFValueText;
    [AutoBind("./SoldierDetailPanel/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATValueText;
    [AutoBind("./SoldierDetailPanel/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFValueText;
    [AutoBind("./SoldierDetailPanel/HP/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropHPAddText;
    [AutoBind("./SoldierDetailPanel/DF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropDFAddText;
    [AutoBind("./SoldierDetailPanel/AT/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropATAddText;
    [AutoBind("./SoldierDetailPanel/MagicDF/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_soldierPropMagicDFAddText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/BuffDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffDescPrefab;
    [AutoBind("./Prefabs/SkillDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillDescPrefab;
    [AutoBind("./Prefabs/BuffIcon", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_buffIconPrefab;
    private GameObjectPool<SkillDesc> m_skillDescPool;
    private GameObjectPool<BuffDesc> m_buffDescPool;
    private UISpineGraphic m_soldierGraphic;
    private UISpineGraphic m_soldierDetailGraphic;
    private bool m_isOpened;
    private BattleActor m_actor;
    private List<TrainingTech> m_trainTechs;
    private int m_myPlayerTeam;
    private bool m_isPvpBattle;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_IsOpened;
    private static DelegateBridge __Hotfix_SetActorInfo;
    private static DelegateBridge __Hotfix_SetSoldierDetailInfo;
    private static DelegateBridge __Hotfix_CalcPropValue;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_DebugBuffToggle_OnValueChanged;

    private BattleActorInfoUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActorInfo(
      BattleActor a,
      int myPlayerTeam,
      List<TrainingTech> techs,
      bool isPvp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierDetailInfo(
      BattleActor a,
      ConfigDataSoldierInfo soldierInfo,
      int myPlayerTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CalcPropValue(int v0, int v1, bool isAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      string assetName,
      float scale,
      Vector2 offset,
      GameObject parent,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DestroySpineGraphic(UISpineGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DebugBuffToggle_OnValueChanged(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
