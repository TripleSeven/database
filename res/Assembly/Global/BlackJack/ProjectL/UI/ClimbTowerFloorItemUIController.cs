﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ClimbTowerFloorItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class ClimbTowerFloorItemUIController : UIControllerBase
  {
    public Action<ClimbTowerFloorItemUIController> EventOnButtonClick;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_graphicGameObject;
    [AutoBind("./RewardGroup", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_rewardGroupTransform;
    private UISpineGraphic m_spineGraphic;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetTowerFloor;
    private static DelegateBridge __Hotfix_SetState;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_DestroySpineGraphic;
    private static DelegateBridge __Hotfix_OnButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTowerFloor(
      ConfigDataTowerFloorInfo floorInfo,
      ConfigDataTowerLevelInfo levelInfo,
      GameObject rewardGoodsPrefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetState(bool isCleared, bool isLocked, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateSpineGraphic(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroySpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
