﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ActivityUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using Assets.Script.ProjectL.Runtime.UI.Activity;
using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ActivityUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activityStateCtrl;
    [AutoBind("./Prefab/ToggleItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleItemPrefab;
    [AutoBind("./Prefab/ToggleItem/ItemContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameText;
    [AutoBind("./Prefab/ToggleItem/ItemContent/NameTextDark", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityNameDarkText;
    [AutoBind("./Prefab/ToggleItem/ItemContent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tabIcon;
    [AutoBind("./Prefab/ToggleItem/ItemContent/NewLogo", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tabNewLogo;
    [AutoBind("./Prefab/ToggleItem/ItemContent/RedPoint", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tabRedPoint;
    [AutoBind("./LeftSidebar/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftSidebarScrollView;
    [AutoBind("./LeftSidebar/ScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_leftSidebarScrollViewContent;
    [AutoBind("./Right/AnnouncementPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announcementPanelObj;
    [AutoBind("./Right/AnnouncementPanel/ScrollView/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_announcementContentText;
    [AutoBind("./Right/AnnouncementPanel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_announcementScrollView;
    [AutoBind("./Right/ActivityPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityPanelObj;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityScrollView;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/Time/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityTimeText;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/Time/ValueText2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityTimeText2;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/Detail/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_activityContentText;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_activityPanelStateCtrl;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/BillboardImage/BGImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_activityBillboardImage;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/ActivityRewardGroup/Title", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRewardTitleObj;
    [AutoBind("./Right/ActivityPanel/DetaileScrollView/Viewport/Content/ActivityRewardGroup/ActivityRewardList", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRewardList;
    [AutoBind("./Prefab/ActivityReward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityRewardPrefab;
    [AutoBind("./Right/ActivityPanel/GoToButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_goButton;
    [AutoBind("./Right/ActivityPanel/GetRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getRewardButton;
    [AutoBind("./Right/ActivityPanel/ReceivedReward", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ReceivedRewardObj;
    [AutoBind("./Right/ActivityPanel/BigImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_BigImage;
    [AutoBind("./Right/ActivityPanel/BigImage/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ActivityGiftItemIcon;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ActivityGiftItemPriceObj;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_PriceBefore/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ActivityGiftItemPriceBeforeObj;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_Discount/", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ActivityGiftItemPriceDiscountObj;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_PriceBefore/Text_Currency", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ActivityGiftItemPriceCurrency_Old;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_PriceBefore/Text_Price", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ActivityGiftItemPrice_Old;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_PriceNow/Text_Currency", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_ActivityGiftItemPriceCurrency_New;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_PriceNow/Text_Price", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ActivityGiftItemPrice_New;
    [AutoBind("./Right/ActivityPanel/BigImage/LayoutGroup/Item_Discount/Text_Discount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ActivityGiftItemDiscount;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ActivityGM", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_activityGMObj;
    [AutoBind("./ActivityGM/ActivityInputField", AutoBindAttribute.InitState.NotInit, false)]
    private InputField m_activityInputField;
    [AutoBind("./ActivityGM/AddActivityButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addActivityButton;
    public bool m_activityIsOn;
    private int announceCount;
    private OperationalActivityBase m_currentActivity;
    private List<GameObject> m_tabList;
    private List<OperationalActivityBase> m_activityBaseList;
    private List<ulong> m_instanceIDList;
    private List<ulong> m_readAnnounceActivityList;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_UpdateViewInActivity;
    private static DelegateBridge __Hotfix_OpenSpecificActivity;
    private static DelegateBridge __Hotfix_SetTab;
    private static DelegateBridge __Hotfix_IsActivityInOperationPeriod;
    private static DelegateBridge __Hotfix_SetCurrentAnnouncement;
    private static DelegateBridge __Hotfix_SetCurrentActivity;
    private static DelegateBridge __Hotfix_SetTabRedPoint;
    private static DelegateBridge __Hotfix_SetWebActivityTabRedPoint;
    private static DelegateBridge __Hotfix_SetActivityRewardPanel;
    private static DelegateBridge __Hotfix_ShowGiftPrice;
    private static DelegateBridge __Hotfix_GetGiftOldPriceString;
    private static DelegateBridge __Hotfix_SetRewardListPanel;
    private static DelegateBridge __Hotfix_SetRaffleRewardPanel;
    private static DelegateBridge __Hotfix_GainRaffleRewardLevelInfo;
    private static DelegateBridge __Hotfix_GetRedeemContent;
    private static DelegateBridge __Hotfix_AnnouncementComparer;
    private static DelegateBridge __Hotfix_ActivityComparer;
    private static DelegateBridge __Hotfix_OnGainRewardButtonClick;
    private static DelegateBridge __Hotfix_OnExchangeItemGroupButtonClick;
    private static DelegateBridge __Hotfix_OnExchangeItemGroupCrystalNotEnough;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnAddActivityButtonClick;
    private static DelegateBridge __Hotfix_OnGotoButtonClick;
    private static DelegateBridge __Hotfix_OnGetRewardButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnGainReward;
    private static DelegateBridge __Hotfix_remove_EventOnGainReward;
    private static DelegateBridge __Hotfix_add_EventOnExchangeItemGroup;
    private static DelegateBridge __Hotfix_remove_EventOnExchangeItemGroup;
    private static DelegateBridge __Hotfix_add_EventOnAddActivity;
    private static DelegateBridge __Hotfix_remove_EventOnAddActivity;
    private static DelegateBridge __Hotfix_add_EventOnGoToButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGoToButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGetRewardButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGetRewardButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnCrystalNotEnough;
    private static DelegateBridge __Hotfix_remove_EventOnCrystalNotEnough;

    [MethodImpl((MethodImplOptions) 32768)]
    public ActivityUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenSpecificActivity(int activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsActivityInOperationPeriod(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurrentAnnouncement(Announcement currAnnouncement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentActivity(bool needRefreshToTop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTabRedPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWebActivityTabRedPoint(int tagId, bool isShow = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetActivityRewardPanel(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowGiftPrice(ConfigDataOperationalActivityInfo cfg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetGiftOldPriceString(double price, string discountText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRewardListPanel(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaffleRewardPanel(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Dictionary<int, List<RaffleItem>> GainRaffleRewardLevelInfo(
      RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string GetRedeemContent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AnnouncementComparer(Announcement announcementA, Announcement announcementB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ActivityComparer(
      OperationalActivityBase activityA,
      OperationalActivityBase activityB)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGainRewardButtonClick(
      ulong activityInstanceID,
      int rewardIndex,
      ActivityRewardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeItemGroupButtonClick(
      ulong activityInstanceID,
      int itemGroupIndex,
      ActivityRewardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExchangeItemGroupCrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddActivityButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnGotoButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnGetRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, List<Goods>> EventOnGainReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ulong, int, List<Goods>> EventOnExchangeItemGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<string> EventOnAddActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OperationalActivityBase> EventOnGoToButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<OperationalActivityBase> EventOnGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCrystalNotEnough
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
