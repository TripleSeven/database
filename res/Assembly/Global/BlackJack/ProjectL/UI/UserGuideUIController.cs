﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UserGuideUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class UserGuideUIController : UIControllerBase
  {
    [AutoBind("./BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_backgroundButton;
    [AutoBind("./DisableInputButton", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_disableInputObject;
    [AutoBind("./Mask", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_maskTransform;
    [AutoBind("./Mask/Left", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_maskLeftTransform;
    [AutoBind("./Mask/Right", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_maskRightTransform;
    [AutoBind("./Mask/Top", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_maskTopTransform;
    [AutoBind("./Mask/Bottom", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_maskBottomTransform;
    [AutoBind("./IndicatorToLeftDown", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_indicatorToLeftDown;
    [AutoBind("./IndicatorToRightDown", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_indicatorToRightDown;
    [AutoBind("./IndicatorToLeftUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_indicatorToLeftUp;
    [AutoBind("./IndicatorToRightUp", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_indicatorToRightUp;
    [AutoBind("./Pages", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_pagesUIStateController;
    [AutoBind("./Pages", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pagesGameObject;
    [AutoBind("./Pages/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_pageBackgroundButton;
    [AutoBind("./Pages/UserGuidePanel/NextButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_nextPageButton;
    [AutoBind("./Pages/UserGuidePanel/PrevButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_prevPageButton;
    [AutoBind("./Pages/UserGuidePanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closePageButton;
    [AutoBind("./Pages/UserGuidePanel/PageDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_pageDummyObject;
    [AutoBind("./FunctionOpen", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_functionOpenGameObject;
    [AutoBind("./Tips", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_userGuideTipsObject;
    [AutoBind("./Tips/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_userGuideTipsText;
    [AutoBind("./BtnSkipGuide", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_BtnSkipGuide;
    private CommonUIStateController m_functionOpenUIStateController;
    private GameObject m_pagePanelObject;
    private Camera m_camera;
    private static DelegateBridge __Hotfix_get_EnableBackgroundButton;
    private static DelegateBridge __Hotfix_set_EnableBackgroundButton;
    private static DelegateBridge __Hotfix_EnableInput;
    private static DelegateBridge __Hotfix_IsDisableInputObject;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowFunctionOpen;
    private static DelegateBridge __Hotfix_HideFunctionOpen;
    private static DelegateBridge __Hotfix_ShowUserGuideTips;
    private static DelegateBridge __Hotfix_HideUserGuideTips;
    private static DelegateBridge __Hotfix__ShowMask;
    private static DelegateBridge __Hotfix_ShowClickMask;
    private static DelegateBridge __Hotfix_HideClickMask;
    private static DelegateBridge __Hotfix_IsBgButtonObj;
    private static DelegateBridge __Hotfix_ShowIndicator;
    private static DelegateBridge __Hotfix_HideIndicator;
    private static DelegateBridge __Hotfix_GetIndicatorGameObject;
    private static DelegateBridge __Hotfix_PlayPageOpenTween;
    private static DelegateBridge __Hotfix_CreatePages;
    private static DelegateBridge __Hotfix_GetPagesCount;
    private static DelegateBridge __Hotfix_ShowPage;
    private static DelegateBridge __Hotfix_HidePage;
    private static DelegateBridge __Hotfix_EnableSkipButton;
    private static DelegateBridge __Hotfix_SetRectTransformWidth;
    private static DelegateBridge __Hotfix_OnBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnNextPageButtonClick;
    private static DelegateBridge __Hotfix_OnPrevPageButtonClick;
    private static DelegateBridge __Hotfix_OnClosePageButtonClick;
    private static DelegateBridge __Hotfix_OnBtnSkipGuideClick;
    private static DelegateBridge __Hotfix_add_EventOnNext;
    private static DelegateBridge __Hotfix_remove_EventOnNext;
    private static DelegateBridge __Hotfix_add_EventOnNextPage;
    private static DelegateBridge __Hotfix_remove_EventOnNextPage;
    private static DelegateBridge __Hotfix_add_EventOnPrevPage;
    private static DelegateBridge __Hotfix_remove_EventOnPrevPage;
    private static DelegateBridge __Hotfix_add_EventOnSkip;
    private static DelegateBridge __Hotfix_remove_EventOnSkip;

    private UserGuideUIController()
    {
    }

    public bool EnableBackgroundButton
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDisableInputObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowFunctionOpen(
      string functionName,
      string animPrefabName,
      Action onAnimEnd,
      Action<GameObject> postProcess = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideFunctionOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUserGuideTips(string tipsText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideUserGuideTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _ShowMask(Vector2 pos, Vector2 size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowClickMask(Vector2 screenPos, Vector2 screenSize)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideClickMask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBgButtonObj(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowIndicator(Vector2 screenPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideIndicator()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GameObject GetIndicatorGameObject(Vector2 indicateScreenPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayPageOpenTween()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreatePages(string pagesPrefab)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPagesCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPage(int index, bool hasNext, bool hasPrev)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HidePage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableSkipButton(bool bEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void SetRectTransformWidth(RectTransform rt, float w)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNextPageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPrevPageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClosePageButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBtnSkipGuideClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnNext
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnNextPage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnPrevPage
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkip
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
