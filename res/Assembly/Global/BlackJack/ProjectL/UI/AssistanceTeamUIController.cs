﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AssistanceTeamUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AssistanceTeamUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Char", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_charGameObject;
    [AutoBind("./TrainingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_trainingButton;
    [AutoBind("./StopButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_stopButton;
    [AutoBind("./GetRewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_getRewardButton;
    [AutoBind("./DifficultyText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_difficultyText;
    [AutoBind("./TimeStateText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_timeStateText;
    private UISpineGraphic m_playerHeroGraphic;
    public int m_slot;
    public HeroAssistantsTaskAssignment m_heroAssistantsTask;
    private ProjectLPlayerContext m_playerContext;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitAssistanceTeamInfo;
    private static DelegateBridge __Hotfix_SetTimeText;
    private static DelegateBridge __Hotfix_SetSoldierSpine;
    private static DelegateBridge __Hotfix_OnTrainingButtonClick;
    private static DelegateBridge __Hotfix_OnStopButtonClick;
    private static DelegateBridge __Hotfix_OnGetRewardButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnTrainingButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnTrainingButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnStopButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnStopButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnGetRewardButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGetRewardButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAssistanceTeamInfo(HeroAssistantsTaskAssignment task, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTimeText(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetSoldierSpine()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStopButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> EventOnTrainingButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<AssistanceTeamUIController> EventOnStopButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<AssistanceTeamUIController> EventOnGetRewardButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
