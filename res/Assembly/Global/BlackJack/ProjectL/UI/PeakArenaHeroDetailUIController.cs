﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaHeroDetailUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaHeroDetailUIController : UIControllerBase
  {
    [AutoBind("./HeroDetailSelectSkillUIPrefab", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_selectSkillPanel;
    [AutoBind("./EquipmentDescDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipmentDescDummy;
    [AutoBind("./HeroDetailPanel/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeButton;
    [AutoBind("./HeroDetailPanel/Detail/RightPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_rightPanel;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_heroInfoArmyImage;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoLevelText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/HeroNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoNameText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/JobText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoJobText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/PowerValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_heroInfoPowerText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_playerNameText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/ServerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_serverNameText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/HeroInfo/InfoGroup/Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroInfoGraphicDummy;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropHPImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDFImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropATImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicDFImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDEXImg;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXValueText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXAddText;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/Talent", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_talentSkillButton;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/Talent/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroTalentIcon;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/CostGroup/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillTotalCost;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/SkillGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_curSkillContent;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skillChangeButton;
    [AutoBind("./Prefab/SkillItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillItemPrefab;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Skill/SkillItemDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_SkillItemDetailDummy;
    [AutoBind("./Prefab/EquipItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItemPrefab;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem1;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem2;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem3;
    [AutoBind("./HeroDetailPanel/Detail/LeftPanel/Equip/EquipGroup/EquipItem4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipItem4;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaHeroDetailUITask m_peakArenaHeroDetailUITask;
    private PeakArenaManagementTeamUIController.HeroWrap m_heroWrap;
    private UISpineGraphic m_playerHeroGraphic;
    private HeroDetailSelectSkillUIController m_heroDetailSelectSkillUIController;
    private SkillDescUIController m_skillDescUIController;
    private PeakArenaSoldierUIController m_heroDetailSoldierUIController;
    private PeakArenaEquipmentDetailUIController m_equipmentDetailUIController;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetData;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewBaseInfo;
    private static DelegateBridge __Hotfix_UpdateHeroProperty;
    private static DelegateBridge __Hotfix_UpdateHeroSkill;
    private static DelegateBridge __Hotfix_UpdateHeroEquipment;
    private static DelegateBridge __Hotfix_UpdateHeroSingleEquipment;
    private static DelegateBridge __Hotfix_UpdateSoldier;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_OnCloseClick;
    private static DelegateBridge __Hotfix_OnSkillChangeClick;
    private static DelegateBridge __Hotfix_OnHeroSkillsSelect;
    private static DelegateBridge __Hotfix_OnSkillItemClick;
    private static DelegateBridge __Hotfix_OnTalentSkillItemClick;
    private static DelegateBridge __Hotfix_OnItemDetailClick;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnHeroSoldierSelect;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnGotoDrill;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnGotoJobTransfer;
    private static DelegateBridge __Hotfix_HeroDetailSoldierUIController_OnSkinInfoButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetData(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateViewBaseInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroProperty(
      PeakArenaManagementTeamUIController.HeroWrap heroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroSkill(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroSingleEquipment(ulong id, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldier(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillChangeClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroSkillsSelect(int heroId, List<int> skillIds, bool isSkillChanged)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(
      PeakArenaSkillItemUIController peakArenaSkillItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTalentSkillItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnItemDetailClick(
      PeakArenaEquipItemUIController equipItemUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnHeroSoldierSelect(
      int heroId,
      int soldierId,
      Action OnSucceed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoDrill(int techId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnGotoJobTransfer(
      ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HeroDetailSoldierUIController_OnSkinInfoButtonClick(
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
