﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PlayerResourceUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PlayerResourceUIController : UIControllerBase
  {
    private Text m_goldText;
    private Button m_addGoldButton;
    private Text m_crystalText;
    private Button m_addCrystalButton;
    private Text m_energyText;
    private Button m_addEnergyButton;
    private Button m_energyStatusButton;
    private BuyEnergyUITask m_buyEnergyUITask;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnEnable;
    private static DelegateBridge __Hotfix_OnDisable;
    private static DelegateBridge __Hotfix_OnDestroy;
    private static DelegateBridge __Hotfix_UpdatePlayerResource;
    private static DelegateBridge __Hotfix_SetGoldCount;
    private static DelegateBridge __Hotfix_SetCrystalCount;
    private static DelegateBridge __Hotfix_SetEnergy;
    private static DelegateBridge __Hotfix_OnAddGoldButtonClick;
    private static DelegateBridge __Hotfix_OnAddCrystalButtonClick;
    private static DelegateBridge __Hotfix_OnAddEnergyButtonClick;
    private static DelegateBridge __Hotfix_OnEnergyStatusButtonClick;
    private static DelegateBridge __Hotfix_PlayerContext_OnPlayerInfoChangeNtf;
    private static DelegateBridge __Hotfix_add_EventOnAddGold;
    private static DelegateBridge __Hotfix_remove_EventOnAddGold;
    private static DelegateBridge __Hotfix_add_EventOnAddCrystal;
    private static DelegateBridge __Hotfix_remove_EventOnAddCrystal;

    private PlayerResourceUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDisable()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGoldCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCrystalCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEnergy(int count, int max)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddGoldButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddCrystalButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddEnergyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnergyStatusButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnPlayerInfoChangeNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnAddGold
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAddCrystal
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
