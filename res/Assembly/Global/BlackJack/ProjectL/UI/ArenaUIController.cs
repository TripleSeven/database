﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArenaUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Protocol;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArenaUIController : UIControllerBase
  {
    private static bool s_isShowAutoBattleTips = true;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./PlayerResource/ArenaCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaCoinText;
    [AutoBind("./PlayerResource/ArenaOnlineCoin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaCoinText;
    [AutoBind("./PlayerResource/Ticket", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaTicketButton;
    [AutoBind("./PlayerResource/Ticket/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_arenaTicketText;
    [AutoBind("./PlayerResource/Ticket/AddButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_addArenaTicketButton;
    [AutoBind("./Switch", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_switchUIStateController;
    [AutoBind("./Switch/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_switchButton;
    [AutoBind("./Switch/SwitchButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_switchButtonUIStateController;
    [AutoBind("./OfflineAndOnlingChange", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_offlineOnlineChangeUIStateController;
    [AutoBind("./AutoBattleTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_autoBattleTipsUIStateController;
    [AutoBind("./AutoBattleTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoBattleTipsBackgroundButton;
    [AutoBind("./AutoBattleTips/Panel/ShowToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_autoBattleTipsShowToggle;
    [AutoBind("./AutoBattleTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoBattleTipsConfirmButton;
    [AutoBind("./AutoBattleTips/Panel/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoBattleTipsCancelButton;
    [AutoBind("./RewardPreview", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_rewardPreviewUIStateController;
    [AutoBind("./RewardPreview/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_rewardPreviewBackgroundButton;
    [AutoBind("./RewardPreview/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_rewardPreviewScrollRect;
    [AutoBind("./ArenaTicketDesc", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaTicketDescGameObject;
    [AutoBind("./ArenaTicketDesc", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_arenaTicketDescUIStateCtrl;
    [AutoBind("./ArenaTicketDesc/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_arenaTicketDescBackgroundButton;
    [AutoBind("./BuyArenaTicket", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_buyArenaTicketUIStateController;
    [AutoBind("./BuyArenaTicket/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyArenaTicketBackgroundButton;
    [AutoBind("./BuyArenaTicket/Panel/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyArenaTicketCountText;
    [AutoBind("./BuyArenaTicket/Panel/BuyButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_buyArenaTicketBuyButton;
    [AutoBind("./BuyArenaTicket/Panel/BuyButton/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_buyArenaTicketPriceText;
    [AutoBind("./ArenaPointReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_offineArenaPointRewardUIStateCtrl;
    [AutoBind("./ArenaPointReward/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineArenaPointRewardBGButton;
    [AutoBind("./ArenaPointReward/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineArenaPointRewardCloseButton;
    [AutoBind("./ArenaPointReward/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_offlineArenaPointRewardScrollRect;
    [AutoBind("./ArenaRankingReward", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineArenaRankingRewardUIStateCtrl;
    [AutoBind("./ArenaRankingReward/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineArenaRankingRewardBGButton;
    [AutoBind("./ArenaRankingReward/Detail/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineArenaRankingRewardCloseButton;
    [AutoBind("./ArenaRankingReward/Detail/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineArenaRankingRewardscrollRect;
    [AutoBind("./Attack", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_attackUIStateController;
    [AutoBind("./Attack/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackBackgroundButton;
    [AutoBind("./Attack/Panel/Detail/Title/PowerValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackBattlePowerText;
    [AutoBind("./Attack/Panel/Detail/AttackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackConfirmButton;
    [AutoBind("./Attack/Panel/Detail/AutoBattleButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_attackAutoConfirmButton;
    [AutoBind("./Attack/Panel/Detail/Graphics", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_attackGraphicsGameObject;
    [AutoBind("./Attack/Panel/Detail/Title/PlayerImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_attackPlayerIconImage;
    [AutoBind("./Attack/Panel/Detail/Title/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackPlayerNameText;
    [AutoBind("./Attack/Panel/Detail/Title/LvValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackPlayerLVText;
    [AutoBind("./Attack/Panel/Detail/Title/IntegralValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_attackPlayerArenaPointText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/ArenaLevelListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaLevelListItemPrefab;
    [AutoBind("./Prefabs/ArenaRankingListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaRankingListItemPrefab;
    [AutoBind("./Prefabs/ArenaBattleReportListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineArenaBattleReportListItemPrefab;
    [AutoBind("./Prefabs/ArenaOpponentListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaOpponentListItemPrefab;
    [AutoBind("./Prefabs/ArenaPointRewardListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_arenaPointRewardListItemPrefab;
    [AutoBind("./Prefabs/ArenaOnlineBattleReportListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineArenaBattleReportListItemPrefab;
    private bool m_isIgnoreToggleEvent;
    private bool m_isRevengeOpponent;
    private GainRewardButton[] m_offlineVictoryPointsRewardButtons;
    private GainRewardButton[] m_onlineWeekWinRewardButtons;
    private GameObjectPool<ArenaLevelListItemUIController> m_onlineArenaLevelPool;
    private List<UISpineGraphic> m_opponentGraphics;
    [AutoBind("./Offline", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineGameObject;
    [AutoBind("./Offline/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_offlineMarginTransform;
    [AutoBind("./Offline/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_offlineClashToggle;
    [AutoBind("./Offline/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_offlineBattleReportToggle;
    [AutoBind("./Offline/Margin/Tabs/RankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_offlineRankingToggle;
    [AutoBind("./Offline/Panels/Clash", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineClashPanelGameObject;
    [AutoBind("./Offline/Panels/Clash/Opponents", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineArenaOpponentsGameObject;
    [AutoBind("./Offline/Panels/Clash/InSettle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineInSettleGameObject;
    [AutoBind("./Offline/Panels/Clash/RefreshTime/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaOpponentsRefreshTimeText;
    [AutoBind("./Offline/Panels/BattleReport", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineBattleReportPanelGameObject;
    [AutoBind("./Offline/Panels/BattleReport/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_offlineBattleReportScrollRect;
    [AutoBind("./Offline/Panels/BattleReport/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineNoBattleReportGameObject;
    [AutoBind("./Offline/Panels/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineRankingPanelGameObject;
    [AutoBind("./Offline/Panels/Ranking/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_offlineRankingScrollRect;
    [AutoBind("./Offline/Panels/Ranking/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingPlayerNameText;
    [AutoBind("./Offline/Panels/Ranking/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_offlineRankingPlayerRankingUIStateController;
    [AutoBind("./Offline/Panels/Ranking/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingPlayerRankingText;
    [AutoBind("./Offline/Panels/Ranking/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineRankingPlayerRankingImage;
    [AutoBind("./Offline/Panels/Ranking/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingArenaPointsText;
    [AutoBind("./Offline/Panels/Ranking/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineRankingPlayerIconImage;
    [AutoBind("./Offline/Panels/Ranking/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_offlineRankingPlayerHeadFrameTransform;
    [AutoBind("./Offline/Panels/Ranking/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineRankingPlayerLevelText;
    [AutoBind("./Offline/Margin/Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlinePlayerIconImage;
    [AutoBind("./Offline/Margin/Player/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlinePlayerNameText;
    [AutoBind("./Offline/Margin/Player/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlinePlayerLevelText;
    [AutoBind("./Offline/Margin/Player/BattlePower/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlinePlayerBattlePowerText;
    [AutoBind("./Offline/Margin/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaPointsText;
    [AutoBind("./Offline/Margin/Player/ArenaPointsUp/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaPointsUpText1;
    [AutoBind("./Offline/Margin/Player/ArenaPointsUp/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineArenaPointsUpText2;
    [AutoBind("./Offline/Margin/Player/ArenaPointsUp/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineArenaPointsUpBarImage;
    [AutoBind("./Offline/Margin/Player/DefendButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineDefendButton;
    [AutoBind("./Offline/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_offlineArenaPointRewardButton;
    [AutoBind("./Offline/DownGroup/VictoryPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineVictoryPointsText;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineVictoryPointsRewardButton1GameObject;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineVictoryPointsRewardButton2GameObject;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_offlineVictoryPointsRewardButton3GameObject;
    [AutoBind("./Offline/DownGroup/VictoryPointsReward/BarFrame/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_offlineVictoryPointsRewardImage;
    [AutoBind("./Offline/DownGroup/WeekWin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_offlineWeekWinText;
    private GameObjectPool<ArenaOpponentListItemUIController> m_arenaOpponentPool;
    private GameObjectPool<ArenaRankingListItemUIController> m_arenaRankingPool;
    private GameObjectPool<OfflineArenaBattleReportListItemUIController> m_offlineArenaBattleReportPool;
    private GameObjectPool<ArenaPointRewardListItemUIController> m_offlineArenaPointRewardPool;
    [AutoBind("./Online", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineGameObject;
    [AutoBind("./Online/Margin", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_onlineMarginTransform;
    [AutoBind("./Online/Margin/Tabs/ClashToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineClashToggle;
    [AutoBind("./Online/Margin/Tabs/BattleReportToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineBattleReportToggle;
    [AutoBind("./Online/Margin/Tabs/ArenaLevelToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineDanToggle;
    [AutoBind("./Online/Margin/Tabs/LocalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineLocalRankingToggle;
    [AutoBind("./Online/Margin/Tabs/GlobalRankingToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_onlineGlobalRankingToggle;
    [AutoBind("./Online/Panels/Clash", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineClashPanelGameObject;
    [AutoBind("./Online/Panels/Clash/LadderMode", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineClashLadderModeUIStateController;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/WinRateValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashWinRateText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/WinTimes/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashWinCountText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/FailedTimes/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashLoseCountText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/Times/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashTotalCountText;
    [AutoBind("./Online/Panels/Clash/LadderMode/WinRate/ArenaLevelIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashDanIconImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/NowArenaLevelIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashPromoteDanIconImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/AfterArenaLevelIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashPromoteNextDanIconImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineClashPromoteCountImage;
    [AutoBind("./Online/Panels/Clash/LadderMode/PromoteCompetitionInfo/CompetitionRoundGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineClashPromoteRoundGroupGameObject;
    [AutoBind("./Online/Panels/Clash/LadderMode/OpenTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineClashLadderOpenTimeGameObject;
    [AutoBind("./Online/Panels/Clash/LadderMode/OpenTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineClashLadderOpenTimeText;
    [AutoBind("./Online/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineClashLadderChallengeButton;
    [AutoBind("./Online/Panels/Clash/LadderMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineClashLadderChallengeButtonUIStateController;
    [AutoBind("./Online/Panels/Clash/CasualMode/ChallengeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineClashCasualChallengeButton;
    [AutoBind("./Online/Panels/BattleReport", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineBattleReportPanelGameObject;
    [AutoBind("./Online/Panels/BattleReport/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineBattleReportScrollRect;
    [AutoBind("./Online/Panels/BattleReport/EmptyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineNoBattleReportGameObject;
    [AutoBind("./Online/Panels/ArenaLevel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineDanPanelGameObject;
    [AutoBind("./Online/Panels/ArenaLevel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineDanScrollRect;
    [AutoBind("./Online/Panels/GetLevelRewardNotice", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineDanGetRewardNoticeGameObject;
    [AutoBind("./Online/Panels/GetLevelRewardNotice/FrameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineDanGetRewardNoticeText;
    [AutoBind("./Online/Panels/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineRankingPanelGameObject;
    [AutoBind("./Online/Panels/Ranking/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_onlineRankingScrollRect;
    [AutoBind("./Online/Panels/Ranking/Player/PlayerNameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingPlayerNameText;
    [AutoBind("./Online/Panels/Ranking/Player/Ranking", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_onlineRankingPlayerRankingUIStateController;
    [AutoBind("./Online/Panels/Ranking/Player/Ranking/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingPlayerRankingText;
    [AutoBind("./Online/Panels/Ranking/Player/Ranking/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineRankingPlayerRankingImage;
    [AutoBind("./Online/Panels/Ranking/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingArenaPointsText;
    [AutoBind("./Online/Panels/Ranking/Player/HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineRankingPlayerIconImage;
    [AutoBind("./Online/Panels/Ranking/Player/HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_onlineRankingPlayerHeadFrameTransform;
    [AutoBind("./Online/Panels/Ranking/Player/HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineRankingPlayerLevelText;
    [AutoBind("./Online/Margin/Player/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlinePlayerIconImage;
    [AutoBind("./Online/Margin/Player/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlinePlayerNameText;
    [AutoBind("./Online/Margin/Player/ArenaLevel/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineDanText;
    [AutoBind("./Online/Margin/Player/ArenaLevel/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineDanIconImage;
    [AutoBind("./Online/Margin/Player/LvGroup/LvNumberText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlinePlayerLevelText;
    [AutoBind("./Online/Margin/Player/ArenaPoints/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaPointsText;
    [AutoBind("./Online/Margin/Player/ArenaPointsUp/Text1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaPointsUpText1;
    [AutoBind("./Online/Margin/Player/ArenaPointsUp/Text2", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineArenaPointsUpText2;
    [AutoBind("./Online/Margin/Player/ArenaPointsUp/ProgressBarImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineArenaPointsUpBarImage;
    [AutoBind("./Online/Margin/Player/RewardButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_onlineArenaRankingRewardButton;
    [AutoBind("./Online/DownGroup/WeekWin/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_onlineWeekWinText;
    [AutoBind("./Online/DownGroup/WeekWinReward/Button1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineWeekWinRewardButton1GameObject;
    [AutoBind("./Online/DownGroup/WeekWinReward/Button2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineWeekWinRewardButton2GameObject;
    [AutoBind("./Online/DownGroup/WeekWinReward/Button3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_onlineWeekWinRewardButton3GameObject;
    [AutoBind("./Online/DownGroup/WeekWinReward/BarFrame/Bar", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_onlineWeekWinRewardImage;
    [AutoBind("./PromoteBattleTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteBattleUIStateController;
    [AutoBind("./PromoteBattleTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleBGButton;
    [AutoBind("./PromoteBattleTips/Panel/TipsText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteBattleText;
    [AutoBind("./PromoteBattleTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteBattleConfirmButton;
    [AutoBind("./PromoteSucceedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_promoteSucceedUIStateController;
    [AutoBind("./PromoteSucceedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedBGButton;
    [AutoBind("./PromoteSucceedTips/Panel/ArenaLevelImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_promoteSucceedDanImage;
    [AutoBind("./PromoteSucceedTips/Panel/Info/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_promoteSucceedDanText;
    [AutoBind("./PromoteSucceedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_promoteSucceedConfirmButton;
    [AutoBind("./MatchingFailedTips", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingFailedUIStateController;
    [AutoBind("./MatchingFailedTips/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedBGButton;
    [AutoBind("./MatchingFailedTips/Panel/ConfirmButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingFailedConfirmButton;
    [AutoBind("./MatchingNow", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_matchingNowUIStateController;
    [AutoBind("./MatchingNow/Panel/Detail/TimeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_matchingNowPredictTimeGameObject;
    [AutoBind("./MatchingNow/Panel/Detail/PredictTime/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_matchingNowPredictTimeText;
    [AutoBind("./MatchingNow/Panel/Detail/CancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_matchingNowCancelButton;
    private GameObjectPool<ArenaRankingListItemUIController> m_onlineArenaRankingPool;
    private GameObjectPool<OnlineArenaBattleReportListItemUIController> m_onlineArenaBattleReportPool;
    private GameObjectPool<ArenaRankingRewardListItemUIController> m_onlineArenaRankingRewardPool;
    private bool m_isMatchingNow;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_SwitchOfflineOnline;
    private static DelegateBridge __Hotfix_SetPlayerHeadIcon;
    private static DelegateBridge __Hotfix_SetPlayerName;
    private static DelegateBridge __Hotfix_SetPlayerLevel;
    private static DelegateBridge __Hotfix_SetArenaHonor;
    private static DelegateBridge __Hotfix_SetArenaTicket;
    private static DelegateBridge __Hotfix_SetBattlePower;
    private static DelegateBridge __Hotfix_ShowArenaTicketDesc;
    private static DelegateBridge __Hotfix_CloseArenaTicketDesc;
    private static DelegateBridge __Hotfix_CreateSpineGraphic;
    private static DelegateBridge __Hotfix_AddRewardPreviewGoods;
    private static DelegateBridge __Hotfix_ShowBuyArenaTicket;
    private static DelegateBridge __Hotfix_HideBuyArenaTicket;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnRewardPreviewBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnAddArenaTicketButtonClick;
    private static DelegateBridge __Hotfix_OnBuyArenaTicketBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnBuyArenaTicketBuyButtonClick;
    private static DelegateBridge __Hotfix_OnSwitchButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnDefend;
    private static DelegateBridge __Hotfix_remove_EventOnDefend;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnShowBuyArenaTicket;
    private static DelegateBridge __Hotfix_remove_EventOnShowBuyArenaTicket;
    private static DelegateBridge __Hotfix_add_EventOnBuyArenaTicket;
    private static DelegateBridge __Hotfix_remove_EventOnBuyArenaTicket;
    private static DelegateBridge __Hotfix_add_EventOnSwitchOnlineOffline;
    private static DelegateBridge __Hotfix_remove_EventOnSwitchOnlineOffline;
    private static DelegateBridge __Hotfix_OfflineOnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetOfflineArenaPointsAndLevel;
    private static DelegateBridge __Hotfix_SetOfflineWeekWin;
    private static DelegateBridge __Hotfix_SetOfflineInSettleTime;
    private static DelegateBridge __Hotfix_SetOfflineVictoryPointsRewardStatus;
    private static DelegateBridge __Hotfix_SetOfflineVictoryPointsRewardProgress;
    private static DelegateBridge __Hotfix_SetOfflineArenaOpponents;
    private static DelegateBridge __Hotfix_SetOfflineArenaOpponentsRefreshTime;
    private static DelegateBridge __Hotfix_SetOfflineArenaBattleReports;
    private static DelegateBridge __Hotfix_SetOfflineArenaRankings;
    private static DelegateBridge __Hotfix_ShowOfflinePanel;
    private static DelegateBridge __Hotfix_ShowAutoBattleTips;
    private static DelegateBridge __Hotfix_HideAutoBattleTips;
    private static DelegateBridge __Hotfix_ShowArenaOpponent;
    private static DelegateBridge __Hotfix_HideArenaOpponent;
    private static DelegateBridge __Hotfix_ClearOpponentGraphcs;
    private static DelegateBridge __Hotfix_OnOfflineClashToggle;
    private static DelegateBridge __Hotfix_OnOfflineBattleReportToggle;
    private static DelegateBridge __Hotfix_OnOfflineRankingToggle;
    private static DelegateBridge __Hotfix_OnOfflineArenaPointRewardButtonClick;
    private static DelegateBridge __Hotfix_OnOfflineArenaPointRewardCloseButtonClick;
    private static DelegateBridge __Hotfix_OnOfflineVictoryPointsRewardButtonClick;
    private static DelegateBridge __Hotfix_OnOfflineArenaOpponentAttackButtonClick;
    private static DelegateBridge __Hotfix_OnOfflineBattleReportRevengeButtonClick;
    private static DelegateBridge __Hotfix_OnOfflineBattleReportReplayButtonClick;
    private static DelegateBridge __Hotfix_OnDefendButtonClick;
    private static DelegateBridge __Hotfix_OnAttackConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnAutoBattleButtonClick;
    private static DelegateBridge __Hotfix_OnAttackBackgroundButtonClick;
    private static DelegateBridge __Hotfix_OnAutoBattleTipsConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnAutoBattleTipsCancelButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowOfflinePanel;
    private static DelegateBridge __Hotfix_remove_EventOnShowOfflinePanel;
    private static DelegateBridge __Hotfix_add_EventOnGainOfflineVictoryPointsReward;
    private static DelegateBridge __Hotfix_remove_EventOnGainOfflineVictoryPointsReward;
    private static DelegateBridge __Hotfix_add_EventOnShowOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_remove_EventOnShowOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_add_EventOnAttackOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_remove_EventOnAttackOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_add_EventOnShowRevengeOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_remove_EventOnShowRevengeOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_add_EventOnRevengeOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_remove_EventOnRevengeOfflineArenaOpponent;
    private static DelegateBridge __Hotfix_add_EventOnOfflineBattleReportReplay;
    private static DelegateBridge __Hotfix_remove_EventOnOfflineBattleReportReplay;
    private static DelegateBridge __Hotfix_OnlineOnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetOnlineScoreAndDan;
    private static DelegateBridge __Hotfix_SetLadderMode;
    private static DelegateBridge __Hotfix_SetLadderOpenTime;
    private static DelegateBridge __Hotfix_SetLadderWeekWin;
    private static DelegateBridge __Hotfix_SetOnlineWeekWinRewardStatus;
    private static DelegateBridge __Hotfix_SetOnlineWeekWinRewardProgress;
    private static DelegateBridge __Hotfix_ShowPromoteBattle;
    private static DelegateBridge __Hotfix_ShowPromoteSucceed;
    private static DelegateBridge __Hotfix_SetPromoteBattleStatus;
    private static DelegateBridge __Hotfix_SetOnlineDans;
    private static DelegateBridge __Hotfix_SetOnlineArenaBattleReports;
    private static DelegateBridge __Hotfix_SetOnlineArenaRankings;
    private static DelegateBridge __Hotfix_ShowGlobalRankingToggle;
    private static DelegateBridge __Hotfix_ShowOnlinePanel;
    private static DelegateBridge __Hotfix_ShowMatchingNow;
    private static DelegateBridge __Hotfix_HideMatchingNow;
    private static DelegateBridge __Hotfix_SetMatchingNowTime;
    private static DelegateBridge __Hotfix_SetMatchingPredictTime;
    private static DelegateBridge __Hotfix_IsMatchingNowPredictTimeActive;
    private static DelegateBridge __Hotfix_ShowMatchingFailed;
    private static DelegateBridge __Hotfix_OnOnlineClashToggle;
    private static DelegateBridge __Hotfix_OnOnlineBattleReportToggle;
    private static DelegateBridge __Hotfix_OnOnlineDanToggle;
    private static DelegateBridge __Hotfix_OnOnlineLocalRankingToggle;
    private static DelegateBridge __Hotfix_OnOnlineGlobalRankingToggle;
    private static DelegateBridge __Hotfix_OnOnlineWeekWinRewardButtonClick;
    private static DelegateBridge __Hotfix_OnOnlineBattleReportReplayButtonClick;
    private static DelegateBridge __Hotfix_OnOnlineClashLadderChallengeButtonClick;
    private static DelegateBridge __Hotfix_OnOnlineClashCasualChallengeButtonClick;
    private static DelegateBridge __Hotfix_OnMatchingFailedConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnMatchingNowCancelButtonClick;
    private static DelegateBridge __Hotfix_OnOnlineArenaRankingRewardButtonClick;
    private static DelegateBridge __Hotfix_OnOnlineArenaRankingRewardCloseButtonClick;
    private static DelegateBridge __Hotfix_OnPromoteBattleConfirmButtonClick;
    private static DelegateBridge __Hotfix_OnPromoteSucceedConfirmButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnShowOnlinePanel;
    private static DelegateBridge __Hotfix_remove_EventOnShowOnlinePanel;
    private static DelegateBridge __Hotfix_add_EventOnGainOnlineWeekWinReward;
    private static DelegateBridge __Hotfix_remove_EventOnGainOnlineWeekWinReward;
    private static DelegateBridge __Hotfix_add_EventOnOnlineBattleReportReplay;
    private static DelegateBridge __Hotfix_remove_EventOnOnlineBattleReportReplay;
    private static DelegateBridge __Hotfix_add_EventOnLadderChallenge;
    private static DelegateBridge __Hotfix_remove_EventOnLadderChallenge;
    private static DelegateBridge __Hotfix_add_EventOnCasualChallenge;
    private static DelegateBridge __Hotfix_remove_EventOnCasualChallenge;
    private static DelegateBridge __Hotfix_add_EventOnMatchingCancel;
    private static DelegateBridge __Hotfix_remove_EventOnMatchingCancel;

    [MethodImpl((MethodImplOptions) 32768)]
    private ArenaUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(ArenaUIType uiType, bool canSwitchOnline)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchOfflineOnline(ArenaUIType uiType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerHeadIcon(int playerHeadIconId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaHonor(int areanaCoin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaTicket(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattlePower(int battlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowArenaTicketDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CloseArenaTicketDesc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UISpineGraphic CreateSpineGraphic(
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataModelSkinResourceInfo skinResInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddRewardPreviewGoods(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBuyArenaTicket(int ticketCount, int price)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideBuyArenaTicket()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRewardPreviewBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddArenaTicketButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyArenaTicketBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyArenaTicketBuyButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSwitchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnDefend
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowBuyArenaTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnBuyArenaTicket
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSwitchOnlineOffline
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OfflineOnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaPointsAndLevel(int arenaPoints, int arenaLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineWeekWin(int count, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineInSettleTime(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineVictoryPointsRewardStatus(int idx, int num, GainRewardStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineVictoryPointsRewardProgress(int victoryPoints, int victoryPointsMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaOpponents(List<ArenaOpponent> opponents, bool isGuardBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaOpponentsRefreshTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaBattleReports(List<ArenaBattleReport> battleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOfflineArenaRankings(int mineRank, List<ProArenaTopRankPlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOfflinePanel(OfflineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowAutoBattleTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideAutoBattleTips()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowArenaOpponent(
      ArenaOpponent opponent,
      List<BattleHero> heros,
      int battlePower,
      bool isRevenge)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HideArenaOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearOpponentGraphcs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineClashToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineBattleReportToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineArenaPointRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineArenaPointRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineVictoryPointsRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineArenaOpponentAttackButtonClick(ArenaOpponentListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineBattleReportRevengeButtonClick(
      OfflineArenaBattleReportListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOfflineBattleReportReplayButtonClick(
      OfflineArenaBattleReportListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDefendButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoBattleButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAttackBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoBattleTipsConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoBattleTipsCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<OfflineArenaPanelType> EventOnShowOfflinePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGainOfflineVictoryPointsReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnShowOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnAttackOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaBattleReport> EventOnShowRevengeOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<bool> EventOnRevengeOfflineArenaOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<ArenaBattleReport> EventOnOfflineBattleReportReplay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnlineOnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineScoreAndDan(int score, int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderMode(bool isOpened, bool isPromote)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderOpenTime(string openTimeTxt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLadderWeekWin(int wins, int matches)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineWeekWinRewardStatus(int idx, int num, GainRewardStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineWeekWinRewardProgress(int wins, int countMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteBattle(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPromoteSucceed(int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPromoteBattleStatus(
      int danId,
      List<RealTimePVPBattleReport> promoteBattleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineDans(List<ConfigDataRealTimePVPDanInfo> danInfos, int danId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineArenaBattleReports(List<RealTimePVPBattleReport> battleReports)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOnlineArenaRankings(
      int mineRank,
      List<ProRealTimePVPLeaderboardPlayerInfo> playerInfos,
      List<ProUserSummary> userSummarys,
      bool isGlobal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowGlobalRankingToggle(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowOnlinePanel(OnlineArenaPanelType panelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideMatchingNow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingNowTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMatchingPredictTime(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMatchingNowPredictTimeActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMatchingFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineClashToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineBattleReportToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineDanToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineLocalRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineGlobalRankingToggle(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineWeekWinRewardButtonClick(GainRewardButton b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineBattleReportReplayButtonClick(
      OnlineArenaBattleReportListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineClashLadderChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineClashCasualChallengeButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingFailedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMatchingNowCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineArenaRankingRewardButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnOnlineArenaRankingRewardCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteBattleConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPromoteSucceedConfirmButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<OnlineArenaPanelType> EventOnShowOnlinePanel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> EventOnGainOnlineWeekWinReward
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPBattleReport> EventOnOnlineBattleReportReplay
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnLadderChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnCasualChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMatchingCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
