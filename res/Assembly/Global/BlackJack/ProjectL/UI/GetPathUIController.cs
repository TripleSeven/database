﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.GetPathUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class GetPathUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_commonUIStateController;
    [AutoBind("./Panel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./Panel/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_iconImage;
    [AutoBind("./Panel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descText;
    [AutoBind("./ReturnImage", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnImgButton;
    [AutoBind("./Panel/PathItemScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_contentObj;
    [AutoBind("./Panel/Tips", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tips;
    [AutoBind("./Panel/Tips/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tipsText;
    [AutoBind("./Prefab/GetPathItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPrefab;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_UpdateViewInGetPathPanel;
    private static DelegateBridge __Hotfix_CreatePathItemList;
    private static DelegateBridge __Hotfix_OnReturnImgButtonClick;
    private static DelegateBridge __Hotfix_OnGetPathItemGotoButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnClose;
    private static DelegateBridge __Hotfix_remove_EventOnClose;
    private static DelegateBridge __Hotfix_add_EventOnGotoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnGotoButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInGetPathPanel(GoodsType goodsType, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreatePathItemList(List<GetPathData> getPathList, string getPathDesc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnImgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetPathItemGotoButtonClick(GetPathData getPathInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnClose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GetPathData> EventOnGotoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
