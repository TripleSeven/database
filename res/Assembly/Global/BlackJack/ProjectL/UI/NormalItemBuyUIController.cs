﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NormalItemBuyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class NormalItemBuyUIController : UIControllerBase
  {
    private int m_selfChooseIndex = -1;
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_itemBuyPanelUIStateController;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIconImage;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/CountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemCountText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/Item/Image", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemCountBgGo;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemNameText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/GoodCount", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemGoodCountObj;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/GoodCount/HaveCountText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemHaveCountText;
    [AutoBind("./LayoutRoot/ItemGoodsDesc/DescPanel/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemDescText;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemPriceIcon;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceIcon/PriceIcon/U_crystal", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemPriceIconEffectGameObject;
    [AutoBind("./LayoutRoot/BuyPanel/Group/PriceText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_itemPriceText;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyPanelCloseButton;
    [AutoBind("./LayoutRoot/BuyPanel", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_itemBuyButton;
    [AutoBind("./ResonanceInfoPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_resonanceInfoPanel;
    [AutoBind("./ResonanceInfoPanel/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanelNameText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/2SuitInfo/2SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel2SuitInfoText;
    [AutoBind("./ResonanceInfoPanel/SuitInfo/4SuitInfo/4SuitInfoText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_resonanceInfoPanel4SuitInfoText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private NormalItemBuyUITask m_normalItemBuyUITask;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_ClosePanel;
    private static DelegateBridge __Hotfix_ShowItemBuyPanel;
    private static DelegateBridge __Hotfix_ShowSelfChooseITemBuyPlane;
    private static DelegateBridge __Hotfix_SetFiexdStoreItemBuyPanel;
    private static DelegateBridge __Hotfix_SetFiexdStoreSelfChooseItemBuyPanel;
    private static DelegateBridge __Hotfix_SetFiexdStoreBuyPanel;
    private static DelegateBridge __Hotfix_SetEnchantStoneResonanceInfoPanel;
    private static DelegateBridge __Hotfix_HandleBoxOpenNetTask;
    private static DelegateBridge __Hotfix_GetStoreGoods;
    private static DelegateBridge __Hotfix_GetSelfChooseGoods;
    private static DelegateBridge __Hotfix_StoreUIController_CrystalNotEnough;
    private static DelegateBridge __Hotfix_OnBGButtonClick;
    private static DelegateBridge __Hotfix_OnBuyItemClick;
    private static DelegateBridge __Hotfix_add_EventOnBuySuccess;
    private static DelegateBridge __Hotfix_remove_EventOnBuySuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(NormalItemBuyUITask normalItemBuyUITask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClosePanel(Action OnCompleteClose)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowItemBuyPanel(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSelfChooseITemBuyPlane(
      StoreId storeId,
      int fixedStoreItemId,
      int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFiexdStoreItemBuyPanel(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFiexdStoreSelfChooseItemBuyPanel(
      StoreId storeId,
      int fixedStoreItemId,
      int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetFiexdStoreBuyPanel(Goods goods, GoodsType currencyType, int price)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetEnchantStoneResonanceInfoPanel(GoodsType goodsType, int goodsID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleBoxOpenNetTask(
      GoodsType type,
      int id,
      int count,
      Action<List<Goods>> successedCallback,
      Action failedCallback)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetStoreGoods(int fixedStoreItemId, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Goods GetSelfChooseGoods(int fixedStoreItemId, int selfChooseIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StoreUIController_CrystalNotEnough()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBuyItemClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnBuySuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
