﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.DrillRoomToggleUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class DrillRoomToggleUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_stateCtrl;
    [AutoBind("./Value", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_lvValueText;
    [AutoBind("./ImageGrey", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockGreyButton;
    [AutoBind("./RedMark", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_redMark;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_InitRoomToggleInfo;
    private static DelegateBridge __Hotfix_OnToggleValueChange;
    private static DelegateBridge __Hotfix_OnLockButtonClick;
    private static DelegateBridge __Hotfix_ClickToggle;
    private static DelegateBridge __Hotfix_add_EventOnToggleClick;
    private static DelegateBridge __Hotfix_remove_EventOnToggleClick;
    private static DelegateBridge __Hotfix_set_Room;
    private static DelegateBridge __Hotfix_get_Room;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRoomToggleInfo(int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChange(bool isOn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClickToggle()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<DrillRoomToggleUIController> EventOnToggleClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TrainingRoom Room
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
