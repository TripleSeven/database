﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.NormalItemSelfChooseBuyUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class NormalItemSelfChooseBuyUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./LayoutRoot/Prefab/Item", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_itemTemplate;
    [AutoBind("./LayoutRoot/ListScrollView /Mask/Content", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_content;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private List<GoodsUIController> m_goodUIControllerList;
    private NormalItemBuyUITask m_normalItemBuyUITask;
    private StoreId m_storeId;
    private int m_fixedStoreItemId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_SetSelfChooseItemBuyPlane;
    private static DelegateBridge __Hotfix_OnGoodsClick;
    private static DelegateBridge __Hotfix_OnBGButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnSelfBoxChoose;
    private static DelegateBridge __Hotfix_remove_EventOnSelfBoxChoose;

    [MethodImpl((MethodImplOptions) 32768)]
    public NormalItemSelfChooseBuyUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(NormalItemBuyUITask normalItemBuyUITask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelfChooseItemBuyPlane(StoreId storeId, int fixedStoreItemId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGoodsClick(GoodsUIController goodsUIController)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<StoreId, int, int> EventOnSelfBoxChoose
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
