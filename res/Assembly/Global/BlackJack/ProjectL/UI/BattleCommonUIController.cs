﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.BattleCommonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class BattleCommonUIController : UIControllerBase
  {
    [AutoBind("./ScreenEffect", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_screenEffectGameObject;
    [AutoBind("./ScreenEffect", AutoBindAttribute.InitState.NotInit, false)]
    private Animator m_screenEffectAnimator;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_PlayScreenEffect;

    private BattleCommonUIController()
    {
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
