﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamGameFunctionTypeListItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamGameFunctionTypeListItemUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_toggle;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./NameTextChosen", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextChosen;
    [AutoBind("./NameTextUnchosen", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextUnchosen;
    [AutoBind("./NameTextLock", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameTextLock;
    [AutoBind("./LockState", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_lockButton;
    private bool m_isLocked;
    private GameFunctionType m_gameFunctionType;
    private int m_chapterId;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetName;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_SetGameFunctionType;
    private static DelegateBridge __Hotfix_GetGameFunctionType;
    private static DelegateBridge __Hotfix_SetChapterId;
    private static DelegateBridge __Hotfix_GetChapterId;
    private static DelegateBridge __Hotfix_SetLocked;
    private static DelegateBridge __Hotfix_IsLocked;
    private static DelegateBridge __Hotfix_SetToggleValue;
    private static DelegateBridge __Hotfix_GetToggleValue;
    private static DelegateBridge __Hotfix_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_OnLockButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnToggleValueChanged;
    private static DelegateBridge __Hotfix_remove_EventOnToggleValueChanged;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameFunctionType(GameFunctionType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameFunctionType GetGameFunctionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapterId(int chatperId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLocked(bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetToggleValue(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetToggleValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnToggleValueChanged(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLockButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool, TeamGameFunctionTypeListItemUIController> EventOnToggleValueChanged
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
