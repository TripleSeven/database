﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamRoomPlayerPositionChangeNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class TeamRoomPlayerPositionChangeNetTask : UINetTask
  {
    private List<ProTeamRoomPlayerPositionInfo> m_positionInfos;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_StartNetWorking;
    private static DelegateBridge __Hotfix_OnTeamRoomPlayerPositionChangeAck;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayerPositionChangeNetTask(List<ProTeamRoomPlayerPositionInfo> positionInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTeamRoomPlayerPositionChangeAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
