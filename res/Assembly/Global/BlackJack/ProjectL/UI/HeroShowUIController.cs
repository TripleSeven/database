﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroShowUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroShowUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private PrefabResourceContainer m_resourceContainer;
    [AutoBind("./HeroShow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroShowRoot;
    [AutoBind("./HeroShow/Heros/LM", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosLMGameObject;
    [AutoBind("./HeroShow/Heros/L1", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosL1GameObject;
    [AutoBind("./HeroShow/Heros/L2", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosL2GameObject;
    [AutoBind("./HeroShow/Heros/L3", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosL3GameObject;
    [AutoBind("./HeroShow/Heros/L4", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosL4GameObject;
    [AutoBind("./HeroShow/Heros/L5", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_herosL5GameObject;
    [AutoBind("./ShareButtonDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_shareButtonDummy;
    [AutoBind("./SharePhotpDummy", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_sharePhotoDummy;
    [AutoBind("./BackButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./ToggleGroup", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_toggleGroup;
    [AutoBind("./ToggleGroup/LMToggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_lmToggle;
    [AutoBind("./ToggleGroup/L1Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_l1Toggle;
    [AutoBind("./ToggleGroup/L2Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_l2Toggle;
    [AutoBind("./ToggleGroup/L3Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_l3Toggle;
    [AutoBind("./ToggleGroup/L4Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_l4Toggle;
    [AutoBind("./ToggleGroup/L5Toggle", AutoBindAttribute.InitState.NotInit, false)]
    private Toggle m_l5Toggle;
    private Button m_weiBoButton;
    private Button m_weChatButton;
    private Button m_fbButton;
    private Button m_twitterButton;
    private Button m_instagramButton;
    private Text m_nameText;
    private Text m_lvText;
    private Text m_serverNameText;
    private HeroShowComponent m_heroShowComponent;
    private ArchiveUITask m_task;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SwitchHeroShow;
    private static DelegateBridge __Hotfix_LoadHeroTeam;
    private static DelegateBridge __Hotfix_DestroyHeroShowComponent;
    private static DelegateBridge __Hotfix_OnInstagramShare;
    private static DelegateBridge __Hotfix_OnTwitterShare;
    private static DelegateBridge __Hotfix_OnFacebookShare;
    private static DelegateBridge __Hotfix_OnWeiBoClick;
    private static DelegateBridge __Hotfix_OnWeChatClick;
    private static DelegateBridge __Hotfix_OnReturnClick;
    private static DelegateBridge __Hotfix_SwitchRankShowClick;
    private static DelegateBridge __Hotfix_OnScaleHeroShow;
    private static DelegateBridge __Hotfix_OnMoveHeroShow;
    private static DelegateBridge __Hotfix_InstagramShare;
    private static DelegateBridge __Hotfix_TwitterShare;
    private static DelegateBridge __Hotfix_FacebookShare;
    private static DelegateBridge __Hotfix_WeiBoShare;
    private static DelegateBridge __Hotfix_WeChatShare;
    private static DelegateBridge __Hotfix_CaptureFrame;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SwitchHeroShow(HeroBelongProduction heroBelongProduction = HeroBelongProduction.HeroBelongProduction_LM)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadHeroTeam(GameObject teamGameObject, HeroBelongProduction heroBelongProduction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyHeroShowComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInstagramShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTwitterShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFacebookShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeiBoClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnWeChatClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SwitchRankShowClick(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScaleHeroShow(float scaleDelta)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMoveHeroShow(Vector2 offset)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator InstagramShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator TwitterShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator FacebookShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WeiBoShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator WeChatShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CaptureFrame()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
