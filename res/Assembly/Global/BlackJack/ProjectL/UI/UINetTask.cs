﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UINetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class UINetTask : NetWorkTransactionTask
  {
    protected bool m_isDisableInput;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnTimeOut;
    private static DelegateBridge __Hotfix_OnReLoginSuccess;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_IsReloginSuccess;
    private static DelegateBridge __Hotfix_get_IsReloginSuccess;

    [MethodImpl((MethodImplOptions) 32768)]
    public UINetTask(float timeout = 10f, UITaskBase blockedUITask = null, bool autoRetry = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(object param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnReLoginSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] protected set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsReloginSuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] protected set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
