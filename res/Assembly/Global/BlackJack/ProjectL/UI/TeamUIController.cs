﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.TeamUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class TeamUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiStateController;
    [AutoBind("./ReturnButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_returnButton;
    [AutoBind("./HelpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_helpButton;
    [AutoBind("./ToggleList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_gameFunctionTypeListScrollRect;
    [AutoBind("./ToggleList/DisableButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_gameFunctionTypeListDisableButton;
    [AutoBind("./LevelListPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_locationListUiStateController;
    [AutoBind("./LevelListPanel/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_locationListScrollRect;
    [AutoBind("./LevelListPanel/CloseButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_locationListCloseButton;
    [AutoBind("./LevelListPanel/DisableButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_locationListDisableButton;
    [AutoBind("./TeamRoomList/TitleText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_titleText;
    [AutoBind("./TeamRoomList/ScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_roomListScrollRect;
    [AutoBind("./TeamRoomList/CreateTeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_createTeamRoomButton;
    [AutoBind("./TeamRoomList/RefreshButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_refreshTeamRoomButton;
    [AutoBind("./TeamRoomList/AutoMatchingButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoMatchButton;
    [AutoBind("./TeamRoomList/NoTeamPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_noTeamRoomUIStateController;
    [AutoBind("./TeamRoomList/NoTeamPanel/MatchingCancelButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_autoMatchCancelButton;
    [AutoBind("./TeamRoomList/NoTeamPanel/MatchingCount", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_autoMatchWaitCountText;
    [AutoBind("./Prefabs", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_prefabsGameObject;
    [AutoBind("./Prefabs/GameFunctionTypeListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_gameFunctionTypeListItemPrefab;
    [AutoBind("./Prefabs/LocationListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_locationListItemPrefab;
    [AutoBind("./Prefabs/RoomListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_roomListItemPrefab;
    private List<TeamGameFunctionTypeListItemUIController> m_gameFunctionTypeListItems;
    private List<TeamLocationListItemUIController> m_locationListItems;
    private List<TeamRoomListItemUIController> m_roomListItems;
    private bool m_isIgnoreFireSelectEvent;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_OpenCollectionActivity;
    private static DelegateBridge __Hotfix_SelectChapterAndLocation;
    private static DelegateBridge __Hotfix_AddGameFunctionTypeListItems;
    private static DelegateBridge __Hotfix_AddCollectionActivityWaypointListItems;
    private static DelegateBridge __Hotfix_ClearGameFunctionTypeListItems;
    private static DelegateBridge __Hotfix_AddGameFunctionTypeListItem;
    private static DelegateBridge __Hotfix_AddLocationListItems;
    private static DelegateBridge __Hotfix_ClearLocationListItems;
    private static DelegateBridge __Hotfix_AddLocationListItem;
    private static DelegateBridge __Hotfix_AddTeamRoomListItem;
    private static DelegateBridge __Hotfix_ClearTeamRoomListItems;
    private static DelegateBridge __Hotfix_SetTeamRooms;
    private static DelegateBridge __Hotfix_SetAutoMatchMode;
    private static DelegateBridge __Hotfix_SetAutoMatchWaitCount;
    private static DelegateBridge __Hotfix_IsAutoMatching;
    private static DelegateBridge __Hotfix_UpdateTitle;
    private static DelegateBridge __Hotfix_OnReturnButtonClick;
    private static DelegateBridge __Hotfix_OnHelpButtonClick;
    private static DelegateBridge __Hotfix_OnRefreshTeamRoomButtonClick;
    private static DelegateBridge __Hotfix_OnShowCreateTeamRoomButtonClick;
    private static DelegateBridge __Hotfix_OnAutoMatchButtonClick;
    private static DelegateBridge __Hotfix_OnAutoMatchCancelButtonClick;
    private static DelegateBridge __Hotfix_OnLocationListCloseButtonClick;
    private static DelegateBridge __Hotfix_TeamGameFunctionTypeListItem_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_GetSelectedGameFunctionTypeListItem;
    private static DelegateBridge __Hotfix_GetSelectedLocationListItem;
    private static DelegateBridge __Hotfix_TeamLocationListItem_OnToggleValueChanged;
    private static DelegateBridge __Hotfix_TeamRoomListItem_OnJoinButtonClick;
    private static DelegateBridge __Hotfix_GetGameFunctionType;
    private static DelegateBridge __Hotfix_GetChapterId;
    private static DelegateBridge __Hotfix_GetLocationId;
    private static DelegateBridge __Hotfix_GetGameFunctionName;
    private static DelegateBridge __Hotfix_GetLocationName;
    private static DelegateBridge __Hotfix_GetEnergy;
    private static DelegateBridge __Hotfix_add_EventOnReturn;
    private static DelegateBridge __Hotfix_remove_EventOnReturn;
    private static DelegateBridge __Hotfix_add_EventOnShowHelp;
    private static DelegateBridge __Hotfix_remove_EventOnShowHelp;
    private static DelegateBridge __Hotfix_add_EventOnRefreshTeamRoom;
    private static DelegateBridge __Hotfix_remove_EventOnRefreshTeamRoom;
    private static DelegateBridge __Hotfix_add_EventOnShowCreateTeamRoom;
    private static DelegateBridge __Hotfix_remove_EventOnShowCreateTeamRoom;
    private static DelegateBridge __Hotfix_add_EventOnAutoMatch;
    private static DelegateBridge __Hotfix_remove_EventOnAutoMatch;
    private static DelegateBridge __Hotfix_add_EventOnAutoMatchCancel;
    private static DelegateBridge __Hotfix_remove_EventOnAutoMatchCancel;
    private static DelegateBridge __Hotfix_add_EventOnSelectGameFunctionTypeAndLocation;
    private static DelegateBridge __Hotfix_remove_EventOnSelectGameFunctionTypeAndLocation;
    private static DelegateBridge __Hotfix_add_EventOnJoinTeamRoom;
    private static DelegateBridge __Hotfix_remove_EventOnJoinTeamRoom;

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(GameFunctionType gameFunctionType, int chapterId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenCollectionActivity(int collectionActivityId, int waypointId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SelectChapterAndLocation(
      GameFunctionType gameFunctionType,
      int chapterId,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCollectionActivityWaypointListItems(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGameFunctionTypeListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGameFunctionTypeListItem(
      string name,
      GameFunctionType gameFunctionType,
      int chapterId,
      bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItems(GameFunctionType gameFunctionType, int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearLocationListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLocationListItem(string name, int locationId, int energy, bool isLocked)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddTeamRoomListItem(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearTeamRoomListItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamRooms(List<TeamRoom> rooms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoMatchMode(bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoMatchWaitCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAutoMatching()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnReturnButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHelpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRefreshTeamRoomButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnShowCreateTeamRoomButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoMatchButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAutoMatchCancelButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLocationListCloseButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamGameFunctionTypeListItem_OnToggleValueChanged(
      bool isOn,
      TeamGameFunctionTypeListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamGameFunctionTypeListItemUIController GetSelectedGameFunctionTypeListItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private TeamLocationListItemUIController GetSelectedLocationListItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamLocationListItem_OnToggleValueChanged(
      bool isOn,
      TeamLocationListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TeamRoomListItem_OnJoinButtonClick(TeamRoomListItemUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameFunctionType GetGameFunctionType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLocationId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetGameFunctionName(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLocationName(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetEnergy(GameFunctionType gameFunctionType, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action EventOnReturn
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowHelp
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnRefreshTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnShowCreateTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAutoMatch
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnAutoMatchCancel
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GameFunctionType, int> EventOnSelectGameFunctionTypeAndLocation
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, GameFunctionType, int> EventOnJoinTeamRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
