﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ArchiveItemDetailInfoController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ArchiveItemDetailInfoController : UIControllerBase
  {
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./IconImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_itemIamge;
    [AutoBind("./EquipInfo/EquipGroup", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_equipLimitGroupAnimation;
    [AutoBind("./EquipInfo/EquipGroup/EquipLimitContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_equipLimitContent;
    [AutoBind("./EquipInfo/EquipGroup/EquipUnlimitText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_descEquipUnlimitText;
    [AutoBind("./EquipInfo/ExplainFrontToggle/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_equipmentExplain;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_HPGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/HP/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_HPText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_ATGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/AT/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_ATText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DFGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/DF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DFText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicDFGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/MagicDF/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicDFText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_MagicGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Magic/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_MagicText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_DexGameObject;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/PropGroup/PropContent/Dex/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_DexText;
    [AutoBind("./EquipInfo/PropertyFrontToggle", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillAnimation;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_skillContentAnimation;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/Owner", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillBelongText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/BelongBGImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_skillBelongBGText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/UnlockCoditionText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillUnlockConditionText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/DescScrollView/Mask/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillDescText;
    [AutoBind("./EquipInfo/PropertyFrontToggle/Detail/SkillContent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_skillNameText;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private EquipmentArchiveUIController.EquipmentInfoWrap m_equipmentInfoWrap;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetData;
    private static DelegateBridge __Hotfix_Refresh;
    private static DelegateBridge __Hotfix_ClosePropDisplay;
    private static DelegateBridge __Hotfix_SetEquipmentPropItem;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetData(
      EquipmentArchiveUIController.EquipmentInfoWrap equipmentInfoWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Refresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClosePropDisplay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEquipmentPropItem(PropertyModifyType type, int value)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
