﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.PeakArenaKnockoutUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class PeakArenaKnockoutUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_scheduleButton;
    [AutoBind("./SortButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_selectGroupButton;
    [AutoBind("./SortTypes", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_selectPanelState;
    [AutoBind("./SortButton/SortTypeText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_selectGroupText;
    [AutoBind("./DescButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_descButton;
    [AutoBind("./NextMatchTime", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nextMatchTimeText;
    [AutoBind("./MatchScrollView/Viewport/Content/MatchFlow", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_knockoutPlanPanel;
    [AutoBind("./SortTypes/ButtonGroup/FinalButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_finalButton;
    [AutoBind("./SortTypes/ButtonGroup/FinalButton", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_finalButtonState;
    [AutoBind("./SortTypes/ButtonGroup/GridLayout", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_buttonGroup;
    [AutoBind("./SortTypes/BgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_groupSelectBgButton;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private PeakArenaUITask m_peakArenaUITask;
    private PeakArenaKnockoutPlanUIController m_peakArenaKnockoutPlanUIController;
    private List<Button> m_selectGroupButtonList;
    private int m_lastSelectGroupId;
    private int FinalGroupID;
    private int GroupStateFinalRound;
    private float m_lastUpdateTime;
    private DateTime m_lastUpdateServerTime;
    private long m_peakArenaSeasonInfoVersion;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_Open;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_HasSelectedGroup;
    private static DelegateBridge __Hotfix_SetGroup;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UpdateViewNextMatchTime;
    private static DelegateBridge __Hotfix_UpdateViewFinalButtonState;
    private static DelegateBridge __Hotfix_UpdateViewSelfGroup;
    private static DelegateBridge __Hotfix_IsFinalMatchActive;
    private static DelegateBridge __Hotfix_OnFlush;
    private static DelegateBridge __Hotfix_OnScheduleClick;
    private static DelegateBridge __Hotfix_OnDescClick;
    private static DelegateBridge __Hotfix_OnSelectGroupClick;
    private static DelegateBridge __Hotfix_OnGroupClick;
    private static DelegateBridge __Hotfix_OnGroupSelectBGClick;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaKnockoutUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Open(int selectGroupId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSelectedGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroup(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewNextMatchTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewFinalButtonState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewSelfGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFinalMatchActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnScheduleClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDescClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectGroupClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupClick(int selectGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGroupSelectBGClick()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
