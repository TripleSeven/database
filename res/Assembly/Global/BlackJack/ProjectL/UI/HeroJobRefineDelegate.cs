﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroJobRefineDelegate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using System;

namespace BlackJack.ProjectL.UI
{
  public delegate void HeroJobRefineDelegate(
    int heroId,
    int jobConnectionId,
    int slotId,
    int index,
    int stoneId,
    Action<HeroJobSlotRefineryProperty> onComplete);
}
