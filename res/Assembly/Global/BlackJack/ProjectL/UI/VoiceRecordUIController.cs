﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.VoiceRecordUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class VoiceRecordUIController : UIControllerBase
  {
    [AutoBind("./SoundMessage/Send/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text voiceTimeText;
    [AutoBind("./SoundMessage", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController voiceRecordStateUICtrl;
    [AutoBind("./SoundMessage/Send/ProgressBar", AutoBindAttribute.InitState.NotInit, false)]
    public Image recordTimeProgressBar;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_ShowVoiceRecordTip;
    private static DelegateBridge __Hotfix_HideVoiceRecordTip;
    private static DelegateBridge __Hotfix_ShowVoiceCancelTip;
    private static DelegateBridge __Hotfix_ShowVoiceShortTip;
    private static DelegateBridge __Hotfix_UpdateVoiceRecordTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public VoiceRecordUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideVoiceRecordTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceCancelTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowVoiceShortTip()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateVoiceRecordTime(float time)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
