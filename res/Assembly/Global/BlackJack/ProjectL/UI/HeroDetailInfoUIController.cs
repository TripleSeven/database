﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroDetailInfoUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.Scene;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class HeroDetailInfoUIController : UIControllerBase
  {
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    private RectTransform m_marginTransform;
    [AutoBind("./ArmyImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroArmyImg;
    [AutoBind("./Lv/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroLvText;
    [AutoBind("./Lv/ValueText/MaxText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroLvMaxText;
    [AutoBind("./Exp/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroExpText;
    [AutoBind("./Exp/ProgressImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroExpImg;
    [AutoBind("./Graphic", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroGraphic;
    [AutoBind("./PropertyMsgButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_propertyMsgButton;
    [AutoBind("./Explanation", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_explanationStateCtrl;
    [AutoBind("./Explanation/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_explanationBackgroundButton;
    [AutoBind("./Explanation/Panel/BGImage/FrameImage/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_explanationText;
    [AutoBind("./Keyword", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagButtonsContent;
    [AutoBind("./KeywordPanel", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_tagPanelStateCtrl;
    [AutoBind("./KeywordPanel/BGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelBGButton;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/TitleText1", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelTitleText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/Scrollrect/Viewport/Content/DescText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tagPanelDescText;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/InfoPanel/KeywordIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_tagPanelIconImage;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/ListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelListScrollViewContent;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/ListScrollView", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_tagPanelListScrollRect;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/Prefab/HeroIconImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_tagPanelHeroIconImagePrefab;
    [AutoBind("./KeywordPanel/KeywordPanelDetil/CloseBth", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_tagPanelCloseBtn;
    [AutoBind("./Property/HP/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropHPImg;
    [AutoBind("./Property/DF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDFImg;
    [AutoBind("./Property/AT/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropATImg;
    [AutoBind("./Property/MagicDF/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicDFImg;
    [AutoBind("./Property/Magic/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropMagicImg;
    [AutoBind("./Property/DEX/EvaluateImg", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroPropDEXImg;
    [AutoBind("./Property/HP/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPValueText;
    [AutoBind("./Property/DF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFValueText;
    [AutoBind("./Property/AT/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATValueText;
    [AutoBind("./Property/MagicDF/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFValueText;
    [AutoBind("./Property/Magic/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicValueText;
    [AutoBind("./Property/DEX/Text/ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXValueText;
    [AutoBind("./Property/HP/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropHPAddText;
    [AutoBind("./Property/DF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDFAddText;
    [AutoBind("./Property/AT/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropATAddText;
    [AutoBind("./Property/MagicDF/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicDFAddText;
    [AutoBind("./Property/Magic/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropMagicAddText;
    [AutoBind("./Property/DEX/Text/AddText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroPropDEXAddText;
    [AutoBind("./Talent/Icon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_infoHeroTalentIcon;
    [AutoBind("./Talent/NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroTalentNameText;
    [AutoBind("./Talent/DescScrollView/Viewport/Content/Desc", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_infoHeroTalentDescText;
    [AutoBind("./Skill/Costs", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroCurSkillCostsObj;
    [AutoBind("./Skill/SkillContent", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroCurSkillContent;
    [AutoBind("./Skill/SkillBg", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoHeroCurSkillContentBg;
    [AutoBind("./Skill/ChangeButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoHeroCurSkillChangeButton;
    [AutoBind("./Skill/SkillItemDetailPanel", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_infoCurSkillDescPanel;
    [AutoBind("./AddExpButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_infoAddExpButton;
    [AutoBind("./SkinInfoButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_skinInfoButton;
    private Hero m_hero;
    private UISpineGraphic m_playerHeroGraphic;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_UpdateViewInInfoState;
    private static DelegateBridge __Hotfix_ShowHeroInfo;
    private static DelegateBridge __Hotfix_SetHeroTagInfo;
    private static DelegateBridge __Hotfix_OpenTagPanel;
    private static DelegateBridge __Hotfix_SetHeroProperty;
    private static DelegateBridge __Hotfix_SetHeroTalent;
    private static DelegateBridge __Hotfix_SetCurSelectSkills;
    private static DelegateBridge __Hotfix_OnSkillItemClick;
    private static DelegateBridge __Hotfix_OnAddExpButtonClick;
    private static DelegateBridge __Hotfix_OnSkinInfoButtonClcik;
    private static DelegateBridge __Hotfix_OnJobUpButtonClick;
    private static DelegateBridge __Hotfix_OnChangeSkillButtonClick;
    private static DelegateBridge __Hotfix_SetCommonUIState;
    private static DelegateBridge __Hotfix_OnPropertyMsgButtonClick;
    private static DelegateBridge __Hotfix_OnExplanationBackgroundButtonClick;
    private static DelegateBridge __Hotfix_IsShowHeroLittleSpine;
    private static DelegateBridge __Hotfix_add_EventOnSetDetailState;
    private static DelegateBridge __Hotfix_remove_EventOnSetDetailState;
    private static DelegateBridge __Hotfix_add_EventOnSkinInfoButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnSkinInfoButtonClick;
    private static DelegateBridge __Hotfix_add_EventOnJobUpButtonClick;
    private static DelegateBridge __Hotfix_remove_EventOnJobUpButtonClick;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateViewInInfoState(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroTagInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OpenTagPanel(ConfigDataHeroTagInfo tagInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroProperty(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHeroTalent(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCurSelectSkills()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillItemClick(HeroSkillItemUIController skillCtrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddExpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkinInfoButtonClcik()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJobUpButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnChangeSkillButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonUIState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnPropertyMsgButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExplanationBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IsShowHeroLittleSpine(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<string> EventOnSetDetailState
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnSkinInfoButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnJobUpButtonClick
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
