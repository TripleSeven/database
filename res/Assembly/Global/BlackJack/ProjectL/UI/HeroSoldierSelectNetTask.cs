﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.HeroSoldierSelectNetTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.UI
{
  public class HeroSoldierSelectNetTask : UINetTask
  {
    private int m_heroId;
    private int m_soldierId;
    private SkillAndSoldierSelectMode m_mode;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RegisterNetworkEvent;
    private static DelegateBridge __Hotfix_UnregisterNetworkEvent;
    private static DelegateBridge __Hotfix_OnHeroSoldierSelectAck;
    private static DelegateBridge __Hotfix_StartNetWorking;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroSoldierSelectNetTask(int heroId, int soldierId, SkillAndSoldierSelectMode mode = SkillAndSoldierSelectMode.NoneOfAbove)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void RegisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UnregisterNetworkEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroSoldierSelectAck(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool StartNetWorking()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
