﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.RaffleRewardItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class RaffleRewardItemUIController : UIControllerBase
  {
    protected List<RewardGoodsUIController> m_goodsCtrlList;
    [AutoBind("./", AutoBindAttribute.InitState.NotInit, false)]
    public CommonUIStateController UIStateCtrl;
    [AutoBind("./Detail/TitleGroup/Text", AutoBindAttribute.InitState.NotInit, false)]
    public Text LevelText;
    [AutoBind("./Detail/ItemContent", AutoBindAttribute.InitState.NotInit, false)]
    public Transform GoodsItemRoot;
    private static DelegateBridge __Hotfix_UpdateRaffleRewardItemInfo;
    private static DelegateBridge __Hotfix_UpdateRewardItemGotState;
    private static DelegateBridge __Hotfix_GetLvNameByLevel;
    private static DelegateBridge __Hotfix_GetLevelStrByLevel;
    private static DelegateBridge __Hotfix_GetLvStateNameByLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public RaffleRewardItemUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRaffleRewardItemInfo(
      int level,
      List<RaffleItem> itemList,
      HashSet<int> drawedRaffleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateRewardItemGotState(List<RaffleItem> itemList, HashSet<int> drawedRaffleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLvNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected StringTableId GetLevelStrByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected string GetLvStateNameByLevel(int level)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
