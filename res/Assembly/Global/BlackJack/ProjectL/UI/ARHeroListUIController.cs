﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.ARHeroListUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class ARHeroListUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_arHeroListAnimation;
    [AutoBind("./ARHeroSelectPanel/HeroListScrollView/Viewport/Content", AutoBindAttribute.InitState.NotInit, false)]
    private InfinityGridLayoutGroup m_heroListInfinityGrid;
    [AutoBind("./ARHeroSelectPanel/SingleAndTeamPanel/BattleTypeButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_battleTypeButton;
    [AutoBind("./ARHeroSelectPanel/SingleAndTeamPanel/HeroDrawButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_heroDrawButton;
    [AutoBind("./ARHeroSelectPanel/SingleAndTeamPanel/TeamButton", AutoBindAttribute.InitState.NotInit, false)]
    private ButtonEx m_teamShowButton;
    [AutoBind("./BlackBGButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_bgButton;
    [AutoBind("./Prefab/HeroListItem", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_heroListItemPrefab;
    private List<ARHeroListUIController.HeroWrap> m_heroWrapList;
    private List<ARHeroCardUIController> m_arHeroCardUIControllerList;
    private ARHeroListUIController.HeroWrap m_selectHeroWrap;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private UITask m_task;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetUITask;
    private static DelegateBridge __Hotfix_SelectHero;
    private static DelegateBridge __Hotfix_Show;
    private static DelegateBridge __Hotfix_RefreshSelectHeroShow;
    private static DelegateBridge __Hotfix_OnBattleTypeShowClick;
    private static DelegateBridge __Hotfix_OnHeroDrawShowClick;
    private static DelegateBridge __Hotfix_OnTeamShowClick;
    private static DelegateBridge __Hotfix_OnBGButtonClick;
    private static DelegateBridge __Hotfix_OnHeroItemClick;
    private static DelegateBridge __Hotfix_UpdateInfinityHeroItemCallback;

    [MethodImpl((MethodImplOptions) 32768)]
    private ARHeroListUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUITask(UITask task)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectHero(ARHeroListUIController.HeroWrap selectHeroWrap)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Show()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshSelectHeroShow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBattleTypeShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroDrawShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnTeamShowClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBGButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnHeroItemClick(ARHeroCardUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateInfinityHeroItemCallback(int index, Transform trans)
    {
      // ISSUE: unable to decompile the method.
    }

    public class HeroWrap
    {
      public Hero hero;
      public bool isSelect;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public HeroWrap()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
