﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.LoginUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using IL;
using PD.SDK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class LoginUITask : LoginUITaskBase
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private Coroutine m_loginAgentCoroutine;
    private LoginUIController m_loginUIController;
    private ServerListUIController m_serverListUIController;
    private DataNoticeUIController m_dataNoticeUIController;
    private DataNoticeDisagreeUIController m_dataNoticeDisagreeUIController;
    private CreateCharacterUIController m_createCharaterUIController;
    private LoginCommonUIController m_loginCommonUIController;
    private int m_retryLoginByAuthTokenCount;
    private int m_retryLoginBySessionTokenCount;
    private List<string> m_assets;
    private List<string> m_randomNameHead;
    private List<string> m_randomNameMiddle;
    private List<string> m_randomNameTail;
    private System.Random m_randomNameRandom;
    private ClientConfigDataLoader m_configDataLoader;
    private static bool s_isGlobalInitialized;
    private static string s_localConfigPath;
    private static string m_loadConfigFailedMessage;
    private List<UITaskBase.LayerDesc> m_curLoadingLayers;
    private static string m_serverListText;
    private static List<LoginUITask.ServerInfo> m_serverlist;
    private static bool m_isRecentLoginServerIDInServerlist;
    private static int m_curSelectServerID;
    private static int m_recommendServerIndex;
    private static float m_totalRecommandWeight;
    private List<LoginUITask.ExistCharInfo> m_exsitCharsInfo;
    private bool m_isOpeningUI;
    private string m_curLoginAgnetUrl;
    public const string CustomParams = "menghuanmonizhan";
    public const int m_messageDuration = 3;
    private bool m_ignoreNetworkErrorOnce;
    private Dictionary<string, HashSet<string>> m_gmUserIDs;
    private static bool m_isGMUser;
    private bool m_isGettingSDKPlatformUserID;
    private string m_pdsdkLoginReturnData;
    private string m_pdsdkLoginReturnOpcode;
    private string m_pdsdkLoginReturnChannelID;
    private string m_pdsdkLoginReturnCustomParams;
    private static bool m_isAutoRelogin;
    private static bool m_isReturnToLoginAndSwitchUser;
    private static bool m_isReturnToLoginAndOnLoginSuccess;
    private static LoginSuccessMsg m_onSwitchUserSuccessMsg;
    private bool m_isNewRole;
    private Coroutine m_downloadServerListCoroutine;
    private bool m_isClearSessionFailed;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Finalize;
    private static DelegateBridge __Hotfix_ShowDialogBox;
    private static DelegateBridge __Hotfix_Relogin;
    private static DelegateBridge __Hotfix_ReturnToLoginAndSwitchUser;
    private static DelegateBridge __Hotfix_InitializeGlobals;
    private static DelegateBridge __Hotfix_InitLocalConfig;
    private static DelegateBridge __Hotfix_get_ServerList;
    private static DelegateBridge __Hotfix_GetServerInfoByBornChannelID;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_CollectAllDynamicResForLoad;
    private static DelegateBridge __Hotfix_PostOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_InitLayerStateOnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_get_ShouldUsePDSDK;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_StartCoroutineKeepUpdatingServerList;
    private static DelegateBridge __Hotfix_ClearLocalPushNotifications;
    private static DelegateBridge __Hotfix_SetLocalPushNotifications;
    private static DelegateBridge __Hotfix_GetLocalPushNotificationTime;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_GetRecentLoginServerList;
    private static DelegateBridge __Hotfix_DataNoticeUIController_OnClosed;
    private static DelegateBridge __Hotfix_DataNoticeUIController_OnAgreeButtonClicked;
    private static DelegateBridge __Hotfix_DataNoticeUIController_OnPrivacyWebLinkButtonClicked;
    private static DelegateBridge __Hotfix_DataNoticeUIController_OnDisagreeButtonClicked;
    private static DelegateBridge __Hotfix_DataNoticeDisagreeUIController_OnExitButtonClicked;
    private static DelegateBridge __Hotfix_DataNoticeDisagreeUIController_OnBackButtonClicked;
    private static DelegateBridge __Hotfix_DataNoticeDisagreeUIController_OnClosed;
    private static DelegateBridge __Hotfix_ServerListUIController_OnServerListClosed;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_DeleteExpiredLogFiles;
    private static DelegateBridge __Hotfix_CheckClientVersion;
    private static DelegateBridge __Hotfix_PostUpdateView;
    private static DelegateBridge __Hotfix_StopEntryUITask;
    private static DelegateBridge __Hotfix_get_RecommendServerIndex;
    private static DelegateBridge __Hotfix_GetCurrentSelectServerInfo;
    private static DelegateBridge __Hotfix_DownloadServerListFile;
    private static DelegateBridge __Hotfix_DownloadAnnounceFile;
    private static DelegateBridge __Hotfix_DownloadGMUserIDs;
    private static DelegateBridge __Hotfix_ParseGMUserIDsText;
    private static DelegateBridge __Hotfix_get_IsGMUser;
    private static DelegateBridge __Hotfix_UpdateLoginPCClientButton;
    private static DelegateBridge __Hotfix_IsSdkIDCanLoginPCClient;
    private static DelegateBridge __Hotfix_UpdateGMUserFlag;
    private static DelegateBridge __Hotfix_UpdateLocalConfigGMUser;
    private static DelegateBridge __Hotfix_GetSDKPlatformUserID;
    private static DelegateBridge __Hotfix_ReqExistChars;
    private static DelegateBridge __Hotfix_ShowWaitingNet;
    private static DelegateBridge __Hotfix_LaunchEnterGameUIWithGameSettingTokenAndServer;
    private static DelegateBridge __Hotfix_LaunchEnterGameUIWithUIInputAccountAndServer;
    private static DelegateBridge __Hotfix_DownloadGameServerLoginAnnouncement;
    private static DelegateBridge __Hotfix_ShowGameServerLoginAnnouncementUI;
    private static DelegateBridge __Hotfix_ShowEnterGameUI;
    private static DelegateBridge __Hotfix_StartLoginAgentLogin;
    private static DelegateBridge __Hotfix_OnLoginByAuthTokenAck;
    private static DelegateBridge __Hotfix_DelayEnableInput;
    private static DelegateBridge __Hotfix_ShowMessage;
    private static DelegateBridge __Hotfix_OnLoginBySessionTokenAck;
    private static DelegateBridge __Hotfix_LoginBySessionTokenAgain;
    private static DelegateBridge __Hotfix_StartPlayerInfoInitReq;
    private static DelegateBridge __Hotfix_OnPlayerInfoInitEnd;
    private static DelegateBridge __Hotfix_GetStartGameOrCreateRoleJsonString;
    private static DelegateBridge __Hotfix_get_PDSDKStartGameJsonString;
    private static DelegateBridge __Hotfix_get_PDSDKGameRoleJsonString;
    private static DelegateBridge __Hotfix_PlayerContext_OnGameServerNetworkError;
    private static DelegateBridge __Hotfix_LauncheMainUI;
    private static DelegateBridge __Hotfix_Co_UnloadAssetsAndStartPreloadUITask;
    private static DelegateBridge __Hotfix_OnWaitingMsgAckOutTime;
    private static DelegateBridge __Hotfix_OnGameServerConnected;
    private static DelegateBridge __Hotfix_OnGameServerNetworkError;
    private static DelegateBridge __Hotfix_SendLanguageToServer;
    private static DelegateBridge __Hotfix_ShowCreateCharacterUI;
    private static DelegateBridge __Hotfix_get_IsEuropeanUnion;
    private static DelegateBridge __Hotfix_StartCreateCharacterReq;
    private static DelegateBridge __Hotfix_SaveSessionTokenCache;
    private static DelegateBridge __Hotfix_GetSessionTokenCache;
    private static DelegateBridge __Hotfix_get_SessionTokenCacheFileName;
    private static DelegateBridge __Hotfix_ClearSessionTokenCache;
    private static DelegateBridge __Hotfix_IsSessionTokenValid;
    private static DelegateBridge __Hotfix_SaveLoginInfo;
    private static DelegateBridge __Hotfix_CreateRandomName;
    private static DelegateBridge __Hotfix_EnableInput;
    private static DelegateBridge __Hotfix_PDLogin;
    private static DelegateBridge __Hotfix_LoginUIController_OnLoginPCClient;
    private static DelegateBridge __Hotfix_PDSDK_OnQRLoginSuccess;
    private static DelegateBridge __Hotfix_PDSDK_OnQRLoginFailed;
    private static DelegateBridge __Hotfix_PDSDK_OnQRLoginCancel;
    private static DelegateBridge __Hotfix_PDSK_OnLoginFailed;
    private static DelegateBridge __Hotfix_PDSDK_OnInitFailed;
    private static DelegateBridge __Hotfix_PDSDK_OnInitSucess;
    private static DelegateBridge __Hotfix_ClearSDKUserIDOfAllServer;
    private static DelegateBridge __Hotfix_PDSDK_OnLogoutSuccess;
    private static DelegateBridge __Hotfix_PDSDK_OnSwitchUserSuccess;
    private static DelegateBridge __Hotfix_PDSDK_OnLoginSuccess;
    private static DelegateBridge __Hotfix_ParseLoginAgentAck;
    private static DelegateBridge __Hotfix_LoginAgent;
    private static DelegateBridge __Hotfix_SetPlatformInfoToServerList;
    private static DelegateBridge __Hotfix_LoginUIController_OnOpenUserCenter;
    private static DelegateBridge __Hotfix_SwitchUserAccount;
    private static DelegateBridge __Hotfix_LoginUIController_OnAccountTextChanged;
    private static DelegateBridge __Hotfix_LoginUIController_OnSaveServerConfig;
    private static DelegateBridge __Hotfix_LoginUIController_OnCloseAnnouncePanel;
    private static DelegateBridge __Hotfix_LoginUIController_OnOpenAnnouncePanel;
    private static DelegateBridge __Hotfix_LoginUIController_OnSelectServerClick;
    private static DelegateBridge __Hotfix_IsNeedLoadStaticRes;
    private static DelegateBridge __Hotfix_CollectAllStaticResDescForLoad;
    private static DelegateBridge __Hotfix_ShowServerListUI;
    private static DelegateBridge __Hotfix_ShowDataNoticeUI;
    private static DelegateBridge __Hotfix_ShowDataNoticeDisagreeUI;
    private static DelegateBridge __Hotfix_get_SessionAccountInfo;
    private static DelegateBridge __Hotfix_KeepUpdatingServerList;
    private static DelegateBridge __Hotfix_LoginUIController_OnLogin;
    private static DelegateBridge __Hotfix_CreateCharacterUIController_OnCreate;
    private static DelegateBridge __Hotfix_CreateCharacterUIController_OnAutoName;
    private static DelegateBridge __Hotfix_CheckServerMaitainState;
    private static DelegateBridge __Hotfix_NetWorkTransactionTask_OnReLoginBySession;
    private static DelegateBridge __Hotfix_get_FindAInstance;
    private static DelegateBridge __Hotfix_NetWorkTransactionTask_ReturnToLoginUI;
    private static DelegateBridge __Hotfix_Co_StartLoginUITask;
    private static DelegateBridge __Hotfix_ParseServerListText;
    private static DelegateBridge __Hotfix_UpdateServerList;
    private static DelegateBridge __Hotfix_ParseServerOpenDateTime;
    private static DelegateBridge __Hotfix_NetWorkTransactionTask_OnEventShowUIWaiting;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public LoginUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    ~LoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(StringTableId strID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Relogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ReturnToLoginAndSwitchUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitializeGlobals()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void InitLocalConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    public static List<LoginUITask.ServerInfo> ServerList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LoginUITask.ServerInfo GetServerInfoByBornChannelID(int bornChannelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<string> CollectAllDynamicResForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitLayerStateOnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    private static bool ShouldUsePDSDK
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCoroutineKeepUpdatingServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ClearLocalPushNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLocalPushNotifications()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static DateTime GetLocalPushNotificationTime(ConfigDataDailyPushNotification cfg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static List<LoginUITask.ServerInfo> GetRecentLoginServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeUIController_OnClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeUIController_OnAgreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeUIController_OnPrivacyWebLinkButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeUIController_OnDisagreeButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeDisagreeUIController_OnExitButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeDisagreeUIController_OnBackButtonClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DataNoticeDisagreeUIController_OnClosed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ServerListUIController_OnServerListClosed(int selectedServerID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DeleteExpiredLogFiles(string logFolder, int days)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public static IEnumerator CheckClientVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void PostUpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopEntryUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    public static int RecommendServerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static LoginUITask.ServerInfo GetCurrentSelectServerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadServerListFile()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadAnnounceFile(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DownloadGMUserIDs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ParseGMUserIDsText(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsGMUser
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateLoginPCClientButton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSdkIDCanLoginPCClient(string sdkChannelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGMUserFlag(LoginUITask.ServerInfo si = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLocalConfigGMUser()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerator GetSDKPlatformUserID(LoginUITask.ServerInfo si)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ReqExistChars()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitingNet(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LaunchEnterGameUIWithGameSettingTokenAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LaunchEnterGameUIWithUIInputAccountAndServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator DownloadGameServerLoginAnnouncement(Action<bool> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ShowGameServerLoginAnnouncementUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ShowEnterGameUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator StartLoginAgentLogin(Action<int, string, string> onEnd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnLoginByAuthTokenAck(int ret, string sessionToken, bool needRedirect)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayEnableInput(bool isEnable, float delaySeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, int time = 0, bool isOverride = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnLoginBySessionTokenAck(int ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoginBySessionTokenAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void StartPlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnPlayerInfoInitEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string GetStartGameOrCreateRoleJsonString(string rolenameProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string PDSDKStartGameJsonString
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static string PDSDKGameRoleJsonString
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayerContext_OnGameServerNetworkError()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LauncheMainUI()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_UnloadAssetsAndStartPreloadUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnWaitingMsgAckOutTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnGameServerConnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnGameServerNetworkError(int err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SendLanguageToServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowCreateCharacterUI()
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsEuropeanUnion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCreateCharacterReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveSessionTokenCache(string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSessionTokenCache()
    {
      // ISSUE: unable to decompile the method.
    }

    private string SessionTokenCacheFileName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearSessionTokenCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsSessionTokenValid(string sessionToken)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveLoginInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CreateRandomName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void EnableInput(bool isEnable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnLoginPCClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginSuccess(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnQRLoginCancel(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSK_OnLoginFailed(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnInitFailed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PDSDK_OnInitSucess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearSDKUserIDOfAllServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDK_OnLogoutSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PDSDK_OnSwitchUserSuccess(LoginSuccessMsg msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PDSDK_OnLoginSuccess(LoginSuccessMsg msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ParseLoginAgentAck(
      string ackText,
      ref string status,
      ref string platformName,
      ref string userId,
      ref string token,
      ref string error)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator LoginAgent(
      string svrUrl,
      string data,
      string opcode,
      string channel_id,
      string customparams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPlatformInfoToServerList(
      string loginAgentUrl,
      string platformName,
      string platformUserID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnOpenUserCenter()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator SwitchUserAccount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnAccountTextChanged(string text)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnSaveServerConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnCloseAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnOpenAnnouncePanel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnSelectServerClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadStaticRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override List<UITaskBase.LayerDesc> CollectAllStaticResDescForLoad()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowServerListUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDataNoticeUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowDataNoticeDisagreeUI(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    public static string SessionAccountInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator KeepUpdatingServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LoginUIController_OnLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharacterUIController_OnCreate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateCharacterUIController_OnAutoName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void CheckServerMaitainState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_OnReLoginBySession(Action action)
    {
      // ISSUE: unable to decompile the method.
    }

    public static LoginUITask FindAInstance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_ReturnToLoginUI(bool isDataDirty)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator Co_StartLoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void ParseServerListText(string serverListText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateServerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string ParseServerOpenDateTime(string dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void NetWorkTransactionTask_OnEventShowUIWaiting(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static LoginUITask()
    {
      // ISSUE: unable to decompile the method.
    }

    public class ExistCharInfo
    {
      public string m_roleListURL;
      public int m_channelId;
      public int m_playerLevel;
      public int m_headIcon;
      public int m_lastLoginHours;
      public string m_charName;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public ExistCharInfo(
        string roleListURL,
        int channelId,
        int playerId,
        int headIcon,
        int lastLoginHours,
        string charName)
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class ServerInfo
    {
      public LoginUITask.ServerInfo.State m_state = LoginUITask.ServerInfo.State.Normal;
      public int m_id;
      public string m_name;
      public bool m_isNew;
      public string m_ip;
      public string m_domain;
      public int m_port;
      public string m_loginAgentUrl;
      public int m_channelId;
      public int m_bornChannelId;
      public float m_recommendWeight;
      public int m_initialIndex;
      public string m_sdkPlatformName;
      public string m_sdkPlatformUserID;
      public string m_roleListURL;
      public int m_realmID;
      public bool m_isAppleReview;
      public string m_serverOpenDateTime;
      public bool m_isRefuseNewPlayer;
      public string m_areaName;
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_ToString;

      public ServerInfo(
        int initialIndex,
        int id,
        string name,
        LoginUITask.ServerInfo.State state,
        bool isNew,
        string ip,
        string domain,
        int port,
        string loginAgentUrl,
        int channelId,
        int bornChannelId,
        float recommendWeight,
        string roleListURL,
        int realmID,
        bool isAppleReview,
        string serverOpenDateTime,
        bool isRefuseNewPlayer,
        string areaName)
      {
        this.m_initialIndex = initialIndex;
        this.m_id = id;
        this.m_name = name;
        this.m_state = state;
        this.m_isNew = isNew;
        this.m_ip = ip;
        this.m_domain = domain;
        this.m_port = port;
        this.m_loginAgentUrl = loginAgentUrl;
        this.m_channelId = channelId;
        this.m_bornChannelId = bornChannelId;
        this.m_recommendWeight = recommendWeight;
        this.m_roleListURL = roleListURL.ToLower();
        this.m_realmID = realmID;
        this.m_isAppleReview = isAppleReview;
        this.m_serverOpenDateTime = serverOpenDateTime;
        this.m_isRefuseNewPlayer = isRefuseNewPlayer;
        this.m_areaName = areaName;
        LoginUITask.ServerInfo._c__Hotfix_ctor?.__Gen_Delegate_Imp2074((object) this, initialIndex, id, (object) name, state, isNew, (object) ip, (object) domain, port, (object) loginAgentUrl, channelId, bornChannelId, recommendWeight, (object) roleListURL, realmID, isAppleReview, (object) serverOpenDateTime, isRefuseNewPlayer, (object) areaName);
      }

      public override string ToString()
      {
        DelegateBridge hotfixToString = LoginUITask.ServerInfo.__Hotfix_ToString;
        if (hotfixToString != null)
          return hotfixToString.__Gen_Delegate_Imp7((object) this);
        return this.m_ip + (object) this.m_port + this.m_loginAgentUrl + (object) this.m_channelId + (object) this.m_bornChannelId;
      }

      public enum State
      {
        Hot = 1,
        Crowded = 2,
        Maintain = 3,
        Normal = 4,
      }
    }
  }
}
