﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.UnchartedBlessingDescUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.UI
{
  public class UnchartedBlessingDescUITask : UITask
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ProjectLPlayerContext m_playerContext;
    private const string ParamKey_UnchartedBlessingDescPostion = "UnchartedBlessingDescPostion";
    private const string ParamKey_UnchartedBlessingDescOffset = "UnchartedBlessingDescOffset";
    private const string ParamKey_UnchartedBlessingSkillId = "UnchartedBlessingSkillId";
    private const string ParamKey_UnchartedBlessingBattleType = "UnchartedBlessingBattleType";
    private const string ParamKey_UnchartedBlessingCharapterId = "ParamKey_UnchartedBlessingCharapterId";
    private UnchartedBlessingDescUIController m_unchartedBlessingDescUIController;
    private Vector3 m_panelPosition;
    private Vector2 m_panelOffset;
    private int m_blessingSkillId;
    private BattleType m_battleType;
    private int m_levelId;
    private UnchartedLevelRaidResultUITask m_unchartedLevelRaidResultUITask;
    private Action<int> m_eventOnLevelRaidComplete;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_StartUITask;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_InitDataFromUIIntent;
    private static DelegateBridge __Hotfix_IsNeedLoadDynamicRes;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_ClearAllContextAndRes;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_UnchartedBlessingDescUIController_OnClose;
    private static DelegateBridge __Hotfix_UnchartedBlessingDescUIController_OnSweepButtonClick;
    private static DelegateBridge __Hotfix_StartUnchartedLevelRaidNetTask;
    private static DelegateBridge __Hotfix_StartUnchartedLevelRaidResultUITask;
    private static DelegateBridge __Hotfix_UnchartedLevelRaidResultUITask_OnLoadAllResCompleted;
    private static DelegateBridge __Hotfix_UnchartedLevelRaidResultUITaskOnClose;
    private static DelegateBridge __Hotfix_UnchartedLevelRaidLevelUiTaskOnUnchartedLevelUnchartedLevelRaidComplete;
    private static DelegateBridge __Hotfix_SetRecallOnLevelRaidComplete;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedBlessingDescUITask(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UnchartedBlessingDescUITask StartUITask(
      int skillId,
      Vector3 pos,
      Vector3 offset,
      BattleType battleType = BattleType.None,
      int characterId = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnStart(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool OnResume(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitDataFromUIIntent(UIIntent intent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool IsNeedLoadDynamicRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void ClearAllContextAndRes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedBlessingDescUIController_OnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedBlessingDescUIController_OnSweepButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedLevelRaidNetTask()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartUnchartedLevelRaidResultUITask(
      BattleType battleType,
      int levelID,
      BattleReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedLevelRaidResultUITask_OnLoadAllResCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedLevelRaidResultUITaskOnClose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UnchartedLevelRaidLevelUiTaskOnUnchartedLevelUnchartedLevelRaidComplete(int levelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecallOnLevelRaidComplete(Action<int> onLevelRaidComplete)
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
