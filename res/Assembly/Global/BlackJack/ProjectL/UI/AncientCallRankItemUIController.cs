﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.AncientCallRankItemUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class AncientCallRankItemUIController : UIControllerBase
  {
    [AutoBind(".", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_uiState;
    [AutoBind("./HeadIcon/LevelText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_levelText;
    [AutoBind("./ValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_scoreText;
    [AutoBind("./DamageValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_damageText;
    [AutoBind("./NameText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_nameText;
    [AutoBind("./RankValueText", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_rankText;
    [AutoBind("./RankValueImage", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_rankImage;
    [AutoBind("./HeadIcon", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_playerIconImage;
    [AutoBind("./HeadIcon/HeadFrameDummy", AutoBindAttribute.InitState.NotInit, false)]
    private Transform m_headFrameTransform;
    [AutoBind("./HeadIcon/FriendImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_friendImageGameObject;
    [AutoBind("./HeadIcon/GuildImage", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_guildImageGameObject;
    private ProjectLPlayerContext m_playerContext;
    private IConfigDataLoader m_configDataLoader;
    private RankingTargetPlayerInfo m_rankingTargetPlayerInfo;
    private int m_rank;
    private bool m_isSocialRanking;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_SetRankPlayerInfo;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_GetRankLevelSprite;

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRankPlayerInfo(
      RankingTargetPlayerInfo rankingTargetPlayerInfo,
      int rank,
      bool isSocialRanking = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Sprite GetRankLevelSprite(int rankLevel)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
