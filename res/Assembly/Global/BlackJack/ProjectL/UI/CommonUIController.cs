﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.UI.CommonUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectLBasic;
using IL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.UI
{
  public class CommonUIController : UIControllerBase
  {
    public static bool m_hasFinishAndroidBackEvent;
    private static bool m_isRunningMessageBoxAndQuitApp;
    private static CommonUIController s_instance;
    public TestUI TestUI;
    [AutoBind("./ASRRoot", AutoBindAttribute.InitState.NotInit, false)]
    public XunfeiSDKWrapper m_xfWrapper;
    [AutoBind("./Loading", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_loadingGameObject;
    [AutoBind("./DisableInput", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_disableInputGameObject;
    [AutoBind("./DisableInput/Image", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_disableInputHintImage;
    [AutoBind("./TransparentMask", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_transparentMaskGameObject;
    [AutoBind("./BlackFrame", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_blackFrameGameObject;
    [AutoBind("./Fade", AutoBindAttribute.InitState.NotInit, false)]
    private Image m_fadeImage;
    [AutoBind("./TestUIBackground", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testUIBackgroundGameObject;
    [AutoBind("./TestUIBackground/ToolBar", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testUIBackgroundToolBarGameObject;
    [AutoBind("./TestUIBackground/ToolToggle", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_testUIBackgroundToolToggleGameObject;
    [AutoBind("./iPhoneXTest", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_iPhoneXTestGameObject;
    [AutoBind("./Message", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_messageUIStateController;
    [AutoBind("./Message/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    [AutoBind("./Explanation", AutoBindAttribute.InitState.NotInit, false)]
    private CommonUIStateController m_explanationUIStateController;
    [AutoBind("./Explanation/BackgroundButton", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_explanationBackgroundButton;
    [AutoBind("./Explanation/Panel/BGImage/FrameImage/Scrollrect", AutoBindAttribute.InitState.NotInit, false)]
    private ScrollRect m_explanationScrollRect;
    [AutoBind("./Explanation/Panel/BGImage/FrameImage/Scrollrect/Viewport/Content/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_explanationText;
    [AutoBind("./Notice", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_noticeGameObject;
    [AutoBind("./Notice/Mask/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_noticeText;
    [AutoBind("./Recorder", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_screenRecorderGameObject;
    [AutoBind("./Tip", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_tipUIStateController;
    [AutoBind("./Tip/Text", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_tipText;
    private DialogBox m_dialogBox;
    private CommonUIStateController m_networkWatingStateController;
    private ScreenFade m_screenFade;
    private bool m_hideFadeOutLoadingFadeIn;
    private float m_defaultFadeOutTime;
    private float m_defaultFadeInTime;
    private const float NoticeTweenSpeed = 100f;
    private const float ShowNoticeInterval = 2f;
    private const float ShowNoticeTime = 10f;
    private bool m_isShowingNotice;
    private List<string> m_waitingNotices;
    private Vector3 m_noticeTextInitPos;
    private float m_noticeTextInitWidth;
    private Coroutine m_delayHideNoticeCoroutine;
    private TweenPos m_noticeShowTween;
    private TweenPos m_noticeMoveTween;
    private bool m_isGameDisableInput;
    private bool m_isFrameworkUITaskDisableInput;
    private bool m_isFrameworkNetTaskDisableInput;
    private Coroutine m_showMessageCoroutine;
    private Action m_showMessageEndAction;
    private DateTime m_pauseTime;
    private DateTime m_unfocusDateTime;
    private double m_unfocusTimer;
    private TouchFx m_touchFx;
    private Vector2 m_mysteriesScrollRectValue;
    private static DelegateBridge __Hotfix_StaticLateUpdate;
    private static DelegateBridge __Hotfix_OnAndroidBackKeyUp;
    private static DelegateBridge __Hotfix_MessageBoxAndQuitApp;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnDestroy;
    private static DelegateBridge __Hotfix_InitTouchFx;
    private static DelegateBridge __Hotfix_DisposeTouchFx;
    private static DelegateBridge __Hotfix_SetTouchFxStyle;
    private static DelegateBridge __Hotfix_SetTouchFXParentActive;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_ShowMessage_1;
    private static DelegateBridge __Hotfix_Co_ShowMessage;
    private static DelegateBridge __Hotfix_ShowMessage_0;
    private static DelegateBridge __Hotfix_ShowErrorMessage;
    private static DelegateBridge __Hotfix_ShowDialogBox_1;
    private static DelegateBridge __Hotfix_CancelDialogBox;
    private static DelegateBridge __Hotfix_ShowDialogBox_0;
    private static DelegateBridge __Hotfix_ShowExplanation_1;
    private static DelegateBridge __Hotfix_ShowExplanation_0;
    private static DelegateBridge __Hotfix_HideExplanation;
    private static DelegateBridge __Hotfix_ShowTip;
    private static DelegateBridge __Hotfix_ShowNotice;
    private static DelegateBridge __Hotfix_ShowNextNotice;
    private static DelegateBridge __Hotfix_OnNoticeShowTweenFinished;
    private static DelegateBridge __Hotfix_OnNoticeMoveTweenFinished;
    private static DelegateBridge __Hotfix_DelayHideNotice;
    private static DelegateBridge __Hotfix_ShowLoading;
    private static DelegateBridge __Hotfix_HideLoading;
    private static DelegateBridge __Hotfix_ShowWaitingNet;
    private static DelegateBridge __Hotfix_EnableInput;
    private static DelegateBridge __Hotfix_IsEnableInput;
    private static DelegateBridge __Hotfix_FrameworkUITaskEnableInput;
    private static DelegateBridge __Hotfix_IsFrameworkUITaskDisableInput;
    private static DelegateBridge __Hotfix_FrameworkNetTaskEnableInput;
    private static DelegateBridge __Hotfix_IsFrameworkNetTaskDisableInput;
    private static DelegateBridge __Hotfix_UpdateDisableInput;
    private static DelegateBridge __Hotfix_IsAnyDisableInput;
    private static DelegateBridge __Hotfix_IsDisableInputObject;
    private static DelegateBridge __Hotfix_IsTestToolbarObject;
    private static DelegateBridge __Hotfix_SetDisableInputHintColor;
    private static DelegateBridge __Hotfix_ShowBlackFrame;
    private static DelegateBridge __Hotfix_ShowTestUIBackground;
    private static DelegateBridge __Hotfix_ShowiPhoneXTest;
    private static DelegateBridge __Hotfix_StartFadeOut;
    private static DelegateBridge __Hotfix_StartShowFadeOutLoadingFadeIn;
    private static DelegateBridge __Hotfix_ShowFadeOutLoadingFadeIn;
    private static DelegateBridge __Hotfix_HideFadeOutLoadingFadeIn;
    private static DelegateBridge __Hotfix_FadeIn;
    private static DelegateBridge __Hotfix_FadeOut;
    private static DelegateBridge __Hotfix_IsFading;
    private static DelegateBridge __Hotfix_set_EnableScreenRecordFunction;
    private static DelegateBridge __Hotfix_get_EnableScreenRecordFunction;
    private static DelegateBridge __Hotfix_OnApplicationFocus;
    private static DelegateBridge __Hotfix_OnApplicationPause;
    private static DelegateBridge __Hotfix_SetMysteriesScrollRectByValue;
    private static DelegateBridge __Hotfix_Co_SetMysteriesScrollRect_1;
    private static DelegateBridge __Hotfix_SaveMysteriesScrollRectValue;
    private static DelegateBridge __Hotfix_SetMysteriesScrollRect;
    private static DelegateBridge __Hotfix_Co_SetMysteriesScrollRect_0;
    private static DelegateBridge __Hotfix_GetMysteriesScrollRectVerticalValue;
    private static DelegateBridge __Hotfix_OnExplanationBackgroundButtonClick;
    private static DelegateBridge __Hotfix_get_Instance;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StaticLateUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnAndroidBackKeyUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private static IEnumerator MessageBoxAndQuitApp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnDestroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DisposeTouchFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchFxStyle(int style)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTouchFXParentActive(bool isActive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(string txt, float time = 2f, Action onEnd = null, bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_ShowMessage(
      string txt,
      float time,
      Action onEnd,
      bool disableInput)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessage(StringTableId id, float time = 2f, Action onEnd = null, bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowErrorMessage(int errorCode, float time = 2f, Action onEnd = null, bool disableInput = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(
      string msgText,
      Action<DialogBoxResult> callback,
      string okText = "",
      string cancelText = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CancelDialogBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowDialogBox(
      StringTableId msgId,
      Action<DialogBoxResult> callback,
      StringTableId okId = (StringTableId) 0,
      StringTableId cancelId = (StringTableId) 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExplanation(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowExplanation(ExplanationId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideExplanation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTip(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowNotice(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ShowNextNotice()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeShowTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNoticeMoveTweenFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator DelayHideNotice(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowLoading(FadeStyle style)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideLoading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWaitingNet(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FrameworkUITaskEnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFrameworkUITaskDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FrameworkNetTaskEnableInput(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFrameworkNetTaskDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyDisableInput()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDisableInputObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTestToolbarObject(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetDisableInputHintColor(Color c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBlackFrame(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowTestUIBackground(bool showToggle, bool showBar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowiPhoneXTest(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFadeOut(Action fadeoutEnd, FadeStyle style = FadeStyle.Black, float fadeOutTime = -1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartShowFadeOutLoadingFadeIn(
      Action fadeOutEnd,
      FadeStyle style = FadeStyle.Black,
      float fadeOutTime = -1f,
      float fadeInTime = -1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator ShowFadeOutLoadingFadeIn(
      Action fadeOutEnd,
      FadeStyle style,
      float fadeOutTime,
      float fadeInTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void HideFadeOutLoadingFadeIn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeIn(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FadeOut(float time, Color color, Action onEnd = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFading()
    {
      // ISSUE: unable to decompile the method.
    }

    public bool EnableScreenRecordFunction
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationFocus(bool focus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnApplicationPause(bool isPause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMysteriesScrollRectByValue(ScrollRect sr, float v)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetMysteriesScrollRect(ScrollRect sr, float v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveMysteriesScrollRectValue(Vector2 v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMysteriesScrollRect(ScrollRect sr)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator Co_SetMysteriesScrollRect(ScrollRect sr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetMysteriesScrollRectVerticalValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnExplanationBackgroundButtonClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public static CommonUIController Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
