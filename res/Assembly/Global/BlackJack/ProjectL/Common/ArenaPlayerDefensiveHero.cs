﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerDefensiveHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaPlayerDefensiveHero
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_set_HeroId;
    private static DelegateBridge __Hotfix_get_ActionPositionIndex;
    private static DelegateBridge __Hotfix_set_ActionPositionIndex;
    private static DelegateBridge __Hotfix_get_ActionValue;
    private static DelegateBridge __Hotfix_set_ActionValue;
    private static DelegateBridge __Hotfix_PBArenaDefensiveHeroToArenaDefensiveHero;
    private static DelegateBridge __Hotfix_ArenaDefensiveHeroToPBDfensiveHero;
    private static DelegateBridge __Hotfix_HeroToArenaDefensiveHero;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveHero()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionPositionIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerDefensiveHero PBArenaDefensiveHeroToArenaDefensiveHero(
      ProArenaPlayerDefensiveHero pbDefensiveHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerDefensiveHero ArenaDefensiveHeroToPBDfensiveHero(
      ArenaPlayerDefensiveHero defensiveHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerDefensiveHero HeroToArenaDefensiveHero(
      Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
