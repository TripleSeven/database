﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatPeakArenaBattleReportMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [Serializable]
  public class ChatPeakArenaBattleReportMessage : ChatMessage
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_BattleReportInstanceId;
    private static DelegateBridge __Hotfix_set_BattleReportInstanceId;
    private static DelegateBridge __Hotfix_get_BattleReportSummary;
    private static DelegateBridge __Hotfix_set_BattleReportSummary;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatPeakArenaBattleReportMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong BattleReportInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleReportHead BattleReportSummary
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
