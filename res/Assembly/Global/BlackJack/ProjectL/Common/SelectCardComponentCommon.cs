﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.SelectCardComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class SelectCardComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    protected DataSectionSelectCard m_selectCardDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_RemoveCardPool;
    private static DelegateBridge __Hotfix_InitCardPoolConfigs;
    private static DelegateBridge __Hotfix_IsActivityCardPoolOnActivityTime;
    private static DelegateBridge __Hotfix_FindOperationalActivityByActivityCardPoolId;
    private static DelegateBridge __Hotfix_GetAllOpenActivityCardPool;
    private static DelegateBridge __Hotfix_IsUsedOutActivityCardPoolSelectOpportunity;
    private static DelegateBridge __Hotfix_GetCardPoolById;
    private static DelegateBridge __Hotfix_IsTicketsEnough;
    private static DelegateBridge __Hotfix_GetCrystalCount;
    private static DelegateBridge __Hotfix_SelectCardUseTickets;
    private static DelegateBridge __Hotfix_SelectCardUseCrystal;
    private static DelegateBridge __Hotfix_CanSelectCard;
    private static DelegateBridge __Hotfix_IsCrystalOrActivityCardPool;
    private static DelegateBridge __Hotfix_CanUseTicketSelectCard;
    private static DelegateBridge __Hotfix_OnSelectCardFinished;
    private static DelegateBridge __Hotfix_SelectCardSpendTicketsOrCrystal;
    private static DelegateBridge __Hotfix_SelectHeroCard;
    private static DelegateBridge __Hotfix_add_SelectCardMissionEvent;
    private static DelegateBridge __Hotfix_remove_SelectCardMissionEvent;
    private static DelegateBridge __Hotfix_add_SummonHeroMissionEvent;
    private static DelegateBridge __Hotfix_remove_SummonHeroMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public SelectCardComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCardPool(int cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitCardPoolConfigs(List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsActivityCardPoolOnActivityTime(int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllOpenActivityCardPool()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsUsedOutActivityCardPoolSelectOpportunity(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool GetCardPoolById(int cardPoolId, bool ignoreActivityCheck = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsTicketsEnough(int ticketId, int ticketCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCrystalCount(
      CardPool cardPool,
      bool isSingleSelect,
      out bool isTenSelectDiscount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectCardUseTickets(CardPool cardPool, bool isSingleSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectCardUseCrystal(CardPool cardPool, bool isSingleSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanSelectCard(CardPool cardPool, bool isSingleSelect, bool isUsingTickets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsCrystalOrActivityCardPool(CardPoolType cardPoolType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int CanUseTicketSelectCard(CardPool cardPool, int ticketNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectCardFinished(int cardPoolId, bool isSingleSelect)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SelectCardSpendTicketsOrCrystal(
      CardPool cardPool,
      bool isSingleSelect,
      bool isUsingTickets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SelectHeroCard(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> SelectCardMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> SummonHeroMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
