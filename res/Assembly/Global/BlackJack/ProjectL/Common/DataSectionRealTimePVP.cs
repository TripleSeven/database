﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRealTimePVP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionRealTimePVP : DataSection
  {
    public LinkedList<RealTimePVPBattleReport> PromotionReports;
    public LinkedList<RealTimePVPBattleReport> Reports;
    public RealTimePVPMatchStats FriendlyMatchStats;
    public RealTimePVPMatchStats LadderMatchStats;
    public RealTimePVPMatchStats FriendlyCareerMatchStats;
    public RealTimePVPMatchStats LadderCareerMatchStats;
    public List<int> WinsBonusIdAcquired;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_StartPromotion;
    private static DelegateBridge __Hotfix_EndPromotion;
    private static DelegateBridge __Hotfix_SavePromotionReport;
    private static DelegateBridge __Hotfix_SaveReport;
    private static DelegateBridge __Hotfix_StartNewSeason;
    private static DelegateBridge __Hotfix_AcquireWinsBonus;
    private static DelegateBridge __Hotfix_get_IsPromotion;
    private static DelegateBridge __Hotfix_set_IsPromotion;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRealTimePVP()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartPromotion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EndPromotion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SavePromotionReport(RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveReport(RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartNewSeason()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsPromotion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
