﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHeroDungeon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionHeroDungeon : DataSection
  {
    private int m_dailyChallengeNums;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_FindChapter;
    private static DelegateBridge __Hotfix_FindLevel;
    private static DelegateBridge __Hotfix_ResetLevelChallengeNums;
    private static DelegateBridge __Hotfix_InitLevel;
    private static DelegateBridge __Hotfix_FinishedLevel;
    private static DelegateBridge __Hotfix_AddLevel;
    private static DelegateBridge __Hotfix_AddChapter;
    private static DelegateBridge __Hotfix_InitChapteStarRewardIndexes;
    private static DelegateBridge __Hotfix_AddChapterStarRewardIndex;
    private static DelegateBridge __Hotfix_IsFinishedLevel;
    private static DelegateBridge __Hotfix_HasGotAchievementRelationId;
    private static DelegateBridge __Hotfix_AddAchievementRelationId;
    private static DelegateBridge __Hotfix_InitAchievementRelationIds;
    private static DelegateBridge __Hotfix_AddHeroDungeonLevelChallengeNums;
    private static DelegateBridge __Hotfix_InitDailyChallengeNums;
    private static DelegateBridge __Hotfix_SetDailyChallengeNums;
    private static DelegateBridge __Hotfix_AddDailyChallengeNums;
    private static DelegateBridge __Hotfix_get_DailyChallengeNums;
    private static DelegateBridge __Hotfix_get_FinishedLevels;
    private static DelegateBridge __Hotfix_set_FinishedLevels;
    private static DelegateBridge __Hotfix_get_AchievementRelationIds;
    private static DelegateBridge __Hotfix_set_AchievementRelationIds;
    private static DelegateBridge __Hotfix_get_Chapters;
    private static DelegateBridge __Hotfix_set_Chapters;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHeroDungeon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonChapter FindChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevel FindLevel(int chapterId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetLevelChallengeNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLevel(HeroDungeonChapter chapter, int levelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishedLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevel(HeroDungeonChapter chapter, int levelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonChapter AddChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitChapteStarRewardIndexes(int chapterId, List<int> starRewardIndexes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChapterStarRewardIndex(HeroDungeonChapter chapter, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinishedLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotAchievementRelationId(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAchievementRelationId(int achievementRelationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievementRelationIds(List<int> achievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroDungeonLevelChallengeNums(HeroDungeonLevel level, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDailyChallengeNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDailyChallengeNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDailyChallengeNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int DailyChallengeNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> FinishedLevels
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> AchievementRelationIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, HeroDungeonChapter> Chapters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
