﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CommentComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CommentComponentCommon : IComponentBase
  {
    protected DataSectionComment m_commentDS;
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_CanCommentHero;
    private static DelegateBridge __Hotfix_CanPraiseHeroCommentEntry;
    private static DelegateBridge __Hotfix_CanGetHeroComment;
    private static DelegateBridge __Hotfix_CanViewPlayerHero;
    private static DelegateBridge __Hotfix_IsNeedTruncateHeroComment;
    private static DelegateBridge __Hotfix_TruncateHeroComment;
    private static DelegateBridge __Hotfix_FindPlayerHeroCommentEntry;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommentComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCommentHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanPraiseHeroCommentEntry(int heroId, ulong entryInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetHeroComment(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanViewPlayerHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedTruncateHeroComment(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string TruncateHeroComment(string content)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry FindPlayerHeroCommentEntry(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
