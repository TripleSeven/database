﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionMail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionMail : DataSection
  {
    private Dictionary<ulong, Mail> m_mails;
    private int m_globalMailId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_IsMailReaded;
    private static DelegateBridge __Hotfix_AddMail;
    private static DelegateBridge __Hotfix_InitMail;
    private static DelegateBridge __Hotfix_RemoveMail;
    private static DelegateBridge __Hotfix_ClearMailBox;
    private static DelegateBridge __Hotfix_FindMailByTemplateId;
    private static DelegateBridge __Hotfix_FindMail;
    private static DelegateBridge __Hotfix_ReadMail;
    private static DelegateBridge __Hotfix_SetGotAttachments;
    private static DelegateBridge __Hotfix_InitGlobalMailId;
    private static DelegateBridge __Hotfix_SetGlobalMailId;
    private static DelegateBridge __Hotfix_get_GlobalMailId;
    private static DelegateBridge __Hotfix_GetFirstMail;
    private static DelegateBridge __Hotfix_GetMailBoxSize;
    private static DelegateBridge __Hotfix_GetMails;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMailReaded(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail AddMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveMail(Mail mail)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearMailBox()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail FindMailByTemplateId(int templateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail FindMail(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadMail(Mail mail, DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGotAttachments(Mail mail, DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGlobalMailId(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGlobalMailId(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GlobalMailId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Mail GetFirstMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMailBoxSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mail> GetMails()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
