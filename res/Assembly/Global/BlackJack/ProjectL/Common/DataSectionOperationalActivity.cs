﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionOperationalActivity : DataSection
  {
    private List<OperationalActivityBase> m_operationalActivities;
    private List<AdvertisementFlowLayout> m_layouts;
    private WebInfoControl m_webInfo;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_FindOperationalActivityById;
    private static DelegateBridge __Hotfix_FindOperationalActivitiesByType;
    private static DelegateBridge __Hotfix_InitOperationalActivities;
    private static DelegateBridge __Hotfix_SetOperationalActivities;
    private static DelegateBridge __Hotfix_InitWebInfo;
    private static DelegateBridge __Hotfix_GetWebInfo;
    private static DelegateBridge __Hotfix_AddAccumulateLoginTime;
    private static DelegateBridge __Hotfix_AddRechargeRMB;
    private static DelegateBridge __Hotfix_AddConsumeCrystal;
    private static DelegateBridge __Hotfix_SetPlayerLevel;
    private static DelegateBridge __Hotfix_AddSpecificLoginTimes;
    private static DelegateBridge __Hotfix_ExchangeItemGroup;
    private static DelegateBridge __Hotfix_GainReward;
    private static DelegateBridge __Hotfix_ClearOperationalActivities;
    private static DelegateBridge __Hotfix_AddNewOperationalActivity;
    private static DelegateBridge __Hotfix_UpdateOperationalActivity;
    private static DelegateBridge __Hotfix_RemoveExpiredOperationalActivity;
    private static DelegateBridge __Hotfix_AddActivityEventId;
    private static DelegateBridge __Hotfix_ClearAdvertisementLayout;
    private static DelegateBridge __Hotfix_InitAdvertisementLayout;
    private static DelegateBridge __Hotfix_GetAllAdvertisementFlowLayouts;
    private static DelegateBridge __Hotfix_get_AllOperationalActivities;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionOperationalActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> FindOperationalActivitiesByType(
      OperationalActivityType activityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOperationalActivities(
      List<OperationalActivityBase> operationalActivities)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOperationalActivities(
      List<OperationalActivityBase> operationalActivities)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitWebInfo(WebInfoControl info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WebInfoControl GetWebInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddAccumulateLoginTime(
      AccumulateLoginOperationalActivity accumulateLoginOperationalActivity,
      DateTime loginTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRechargeRMB(AccumulateRechargeOperationalActivity activity, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddConsumeCrystal(
      AccumulateConsumeCrystalOperationalActivity activity,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerLevel(
      PlayerLevelUpOperationalActivity playerLevelUpOperationalActivity,
      int playerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSpecificLoginTimes(
      SpecificDaysLoginOperationalActivity specificDaysLoginOperationalActivity,
      long time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ExchangeItemGroup(
      LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationActivity,
      int itemGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GainReward(AwardOperationalActivityBase operationalActivity, int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpiredOperationalActivity(OperationalActivityBase operatinoalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddActivityEventId(EventOperationalActivity activity, int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAdvertisementLayout()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAdvertisementLayout(AdvertisementFlowLayout layout)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<OperationalActivityBase> AllOperationalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
