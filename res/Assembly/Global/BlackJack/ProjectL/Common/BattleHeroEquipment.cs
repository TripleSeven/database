﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroEquipment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleHeroEquipment
  {
    private int m_id;
    public List<CommonBattleProperty> EnchantProperties;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_get_Exp;
    private static DelegateBridge __Hotfix_set_Exp;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_StarLevel;
    private static DelegateBridge __Hotfix_set_StarLevel;
    private static DelegateBridge __Hotfix_get_ResonanceId;
    private static DelegateBridge __Hotfix_set_ResonanceId;
    private static DelegateBridge __Hotfix_BattleHeroEquipmentToPBBattleHeroEquipment;
    private static DelegateBridge __Hotfix_PBBattleHeroEquipmentToBattleHeroEquipment;
    private static DelegateBridge __Hotfix_UpdateEquipmentInfo;
    private static DelegateBridge __Hotfix_set_EquipmentInfo;
    private static DelegateBridge __Hotfix_get_EquipmentInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroEquipment()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Id
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Exp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int StarLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ResonanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroEquipment BattleHeroEquipmentToPBBattleHeroEquipment(
      BattleHeroEquipment equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroEquipment PBBattleHeroEquipmentToBattleHeroEquipment(
      ProBattleHeroEquipment pbEquipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEquipmentInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataEquipmentInfo EquipmentInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
