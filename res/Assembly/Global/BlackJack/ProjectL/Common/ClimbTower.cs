﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ClimbTower
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ClimbTower
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Floor;
    private static DelegateBridge __Hotfix_set_Floor;
    private static DelegateBridge __Hotfix_get_HistoryFloorMax;
    private static DelegateBridge __Hotfix_set_HistoryFloorMax;
    private static DelegateBridge __Hotfix_get_NextFlushTime;
    private static DelegateBridge __Hotfix_set_NextFlushTime;
    private static DelegateBridge __Hotfix_get_TowerFloorUpdateTime;
    private static DelegateBridge __Hotfix_set_TowerFloorUpdateTime;
    private static DelegateBridge __Hotfix_get_LastTowerFloorUpdateTime;
    private static DelegateBridge __Hotfix_set_LastTowerFloorUpdateTime;
    private static DelegateBridge __Hotfix_get_LastTowerFloorRank;
    private static DelegateBridge __Hotfix_set_LastTowerFloorRank;
    private static DelegateBridge __Hotfix_ToPb_0;
    private static DelegateBridge __Hotfix_ToPb_1;
    private static DelegateBridge __Hotfix_FromPb_1;
    private static DelegateBridge __Hotfix_FromPb_0;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClimbTower()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Floor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HistoryFloorMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime TowerFloorUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastTowerFloorUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastTowerFloorRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProClimbTower ToPb(GlobalClimbTowerInfo globalClimbTowerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProClimbTowerFloor ToPb(
      GlobalClimbTowerFloor globalClimbTowerFloor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GlobalClimbTowerInfo FromPb(ProClimbTower pbClimbTower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GlobalClimbTowerFloor FromPb(
      ProClimbTowerFloor pbClimbTowerFloor)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
