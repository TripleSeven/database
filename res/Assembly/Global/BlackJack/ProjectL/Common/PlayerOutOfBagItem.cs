﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerOutOfBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PlayerOutOfBagItem
  {
    public List<int> HeadFrames;
    public List<int> HeroSkinIds;
    public List<int> SoldierSkinIds;
    public List<int> EquipmentIds;
    public List<MonthCard> MonthCards;
    public List<int> TitleIds;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_FromPB;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerOutOfBagItem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProResource ToPB(PlayerOutOfBagItem resource)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerOutOfBagItem FromPB(ProResource pbResource)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
