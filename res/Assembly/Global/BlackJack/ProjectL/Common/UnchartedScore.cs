﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnchartedScore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UnchartedScore
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ProUnchartedScores2PbProUnchartedScores;
    private static DelegateBridge __Hotfix_UnchartedScore2PbUnchartedScore;
    private static DelegateBridge __Hotfix_PbUnchartedScores;
    private static DelegateBridge __Hotfix_PbUnchartedScoreToUnchartedScore;
    private static DelegateBridge __Hotfix_get_ActivityInstanceId;
    private static DelegateBridge __Hotfix_set_ActivityInstanceId;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_BonusCount;
    private static DelegateBridge __Hotfix_set_BonusCount;
    private static DelegateBridge __Hotfix_get_Score;
    private static DelegateBridge __Hotfix_set_Score;
    private static DelegateBridge __Hotfix_get_RewardGains;
    private static DelegateBridge __Hotfix_set_RewardGains;
    private static DelegateBridge __Hotfix_get_SocreLevelCompleteIds;
    private static DelegateBridge __Hotfix_set_SocreLevelCompleteIds;
    private static DelegateBridge __Hotfix_get_ChallengeLevelCompleteIds;
    private static DelegateBridge __Hotfix_set_ChallengeLevelCompleteIds;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_set_Config;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScore(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProUnchartedScore> ProUnchartedScores2PbProUnchartedScores(
      List<UnchartedScore> unchartedScores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ProUnchartedScore UnchartedScore2PbUnchartedScore(
      UnchartedScore unchartedScore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<UnchartedScore> PbUnchartedScores(
      List<ProUnchartedScore> pbUnchartedScores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static UnchartedScore PbUnchartedScoreToUnchartedScore(
      ProUnchartedScore pbUnchartedScore)
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong ActivityInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BonusCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Score
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> RewardGains
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> SocreLevelCompleteIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> ChallengeLevelCompleteIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataUnchartedScoreInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
