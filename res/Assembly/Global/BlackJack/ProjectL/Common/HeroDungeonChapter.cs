﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroDungeonChapter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroDungeonChapter
  {
    public List<HeroDungeonLevel> ChapterLevels;
    public List<int> StarRewardIndexes;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ChapterId;
    private static DelegateBridge __Hotfix_set_ChapterId;
    private static DelegateBridge __Hotfix_get_TotalStars;
    private static DelegateBridge __Hotfix_set_TotalStars;
    private static DelegateBridge __Hotfix_ToPBChapter;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonChapter()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ChapterId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalStars
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroDungeonChapter ToPBChapter(HeroDungeonChapter chapter)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
