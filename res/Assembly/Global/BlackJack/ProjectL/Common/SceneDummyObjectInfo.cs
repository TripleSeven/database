﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.SceneDummyObjectInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "SceneDummyObjectInfo")]
  [Serializable]
  public class SceneDummyObjectInfo : IExtensible
  {
    private uint _objType;
    private int _objConfId;
    private PVector3D _location;
    private PVector3D _rotation;
    private float _scale;
    private bool _createOnSceneCreate;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ObjType;
    private static DelegateBridge __Hotfix_set_ObjType;
    private static DelegateBridge __Hotfix_get_ObjConfId;
    private static DelegateBridge __Hotfix_set_ObjConfId;
    private static DelegateBridge __Hotfix_get_Location;
    private static DelegateBridge __Hotfix_set_Location;
    private static DelegateBridge __Hotfix_get_Rotation;
    private static DelegateBridge __Hotfix_set_Rotation;
    private static DelegateBridge __Hotfix_get_Scale;
    private static DelegateBridge __Hotfix_set_Scale;
    private static DelegateBridge __Hotfix_get_CreateOnSceneCreate;
    private static DelegateBridge __Hotfix_set_CreateOnSceneCreate;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public SceneDummyObjectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "objType")]
    public uint ObjType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "objConfId")]
    public int ObjConfId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "location")]
    public PVector3D Location
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "rotation")]
    public PVector3D Rotation
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "scale")]
    public float Scale
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "createOnSceneCreate")]
    public bool CreateOnSceneCreate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
