﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GameFunctionStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum GameFunctionStatus
  {
    Start = 1,
    End = 2,
    Cancel = 3,
    FightOutTime = 4,
    Error = 5,
    Inteam = 6,
    OutTeam = 7,
    MatchmakingStart = 8,
    MatchmakingCancel = 9,
    ProtectHero = 10, // 0x0000000A
    BanHero = 11, // 0x0000000B
    RealTimePVPStart = 12, // 0x0000000C
  }
}
