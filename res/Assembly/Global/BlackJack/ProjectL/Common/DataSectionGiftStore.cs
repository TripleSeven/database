﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionGiftStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionGiftStore : DataSection
  {
    private List<GiftStoreItem> m_localBoughtItems;
    private Dictionary<int, string> m_firstboughtItemsRecords;
    protected List<GiftStoreOperationalGoods> m_operationalGoodsList;
    private Dictionary<int, DateTime> m_banBuyingGoodsList;
    private Dictionary<string, OrderReward> m_orderRerads;
    private Dictionary<int, EventGift> m_eventGifts;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_FindLocalBoughtItemById;
    private static DelegateBridge __Hotfix_AddBoughtItem;
    private static DelegateBridge __Hotfix_AddFirstBuyGoodsRecord;
    private static DelegateBridge __Hotfix_HasBought;
    private static DelegateBridge __Hotfix_GetFirstBoughtItemRegisterId;
    private static DelegateBridge __Hotfix_InitBoughtItem;
    private static DelegateBridge __Hotfix_SetBoughtItems;
    private static DelegateBridge __Hotfix_InitFirstBuyGoodsRecord;
    private static DelegateBridge __Hotfix_GetLocalBoughtItems;
    private static DelegateBridge __Hotfix_GetFirstBoughtRecords;
    private static DelegateBridge __Hotfix_IsGoodsOnBanBuyingPeriod;
    private static DelegateBridge __Hotfix_SetBanBuyingGoodsTime;
    private static DelegateBridge __Hotfix_InitmBanBuyingGoodsList;
    private static DelegateBridge __Hotfix_get_BanBuyingGoodsList;
    private static DelegateBridge __Hotfix_FindGiftStoreOperationalGoods;
    private static DelegateBridge __Hotfix_ClearGiftStoreOperationalGoods;
    private static DelegateBridge __Hotfix_InitOperationalGoodsList;
    private static DelegateBridge __Hotfix_GetOperationalGoodsList;
    private static DelegateBridge __Hotfix_InitOrderReward;
    private static DelegateBridge __Hotfix_RemoveOrderReward;
    private static DelegateBridge __Hotfix_AddOrderReward;
    private static DelegateBridge __Hotfix_FindOrderReward;
    private static DelegateBridge __Hotfix_GetAllOrderRewards;
    private static DelegateBridge __Hotfix_IsEventGiftValid;
    private static DelegateBridge __Hotfix_GetAllNotNoticedEventGifts;
    private static DelegateBridge __Hotfix_GetEventGift;
    private static DelegateBridge __Hotfix_GetEventGiftExpiredTime;
    private static DelegateBridge __Hotfix_AddEventGift;
    private static DelegateBridge __Hotfix_RemoveEventGift;
    private static DelegateBridge __Hotfix_NoticeEventGift;
    private static DelegateBridge __Hotfix_InitEventGift;
    private static DelegateBridge __Hotfix_GetAllEventGifts;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionGiftStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem FindLocalBoughtItemById(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBoughtItem(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFirstBuyGoodsRecord(int goodsId, string goodsRegisterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBought(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetFirstBoughtItemRegisterId(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBoughtItem(GiftStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBoughtItems(List<GiftStoreItem> storeItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFirstBuyGoodsRecord(int goodsId, string registerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> GetLocalBoughtItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, string> GetFirstBoughtRecords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsOnBanBuyingPeriod(int goodsId, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanBuyingGoodsTime(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitmBanBuyingGoodsList(Dictionary<int, DateTime> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, DateTime> BanBuyingGoodsList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreOperationalGoods FindGiftStoreOperationalGoods(
      int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGiftStoreOperationalGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOperationalGoodsList(GiftStoreOperationalGoods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreOperationalGoods> GetOperationalGoodsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOrderReward(OrderReward orderReward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOrderReward(string orderId, OrderReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OrderReward FindOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<string, OrderReward> GetAllOrderRewards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventGiftValid(int goodsId, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllNotNoticedEventGifts(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public EventGift GetEventGift(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEventGiftExpiredTime(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEventGift(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveEventGift(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NoticeEventGift(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEventGift(EventGift gift)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, EventGift> GetAllEventGifts()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
