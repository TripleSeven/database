﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionArenaBattleReport : DataSection
  {
    private Dictionary<ulong, int> m_instanceId2CacheIndex;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_InitArenaBattleReport;
    private static DelegateBridge __Hotfix_SetArenaBattleReport;
    private static DelegateBridge __Hotfix_AddArenaBattleReport;
    private static DelegateBridge __Hotfix_DirtyArenaBattleReport;
    private static DelegateBridge __Hotfix_GetAllArenaBattleReports;
    private static DelegateBridge __Hotfix_FindArenaBattleReportByInstanceId;
    private static DelegateBridge __Hotfix_SetNextBattleReportIndex;
    private static DelegateBridge __Hotfix_get_NextBattleReportIndex;
    private static DelegateBridge __Hotfix_set_NextBattleReportIndex;
    private static DelegateBridge __Hotfix_get_BattleReportNums;
    private static DelegateBridge __Hotfix_get_ArenaBattleReportInfo;
    private static DelegateBridge __Hotfix_set_ArenaBattleReportInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaBattleReport(ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleReport(int index, ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaBattleReport(ArenaBattleReport report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DirtyArenaBattleReport(ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport FindArenaBattleReportByInstanceId(
      ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNextBattleReportIndex(byte index)
    {
      // ISSUE: unable to decompile the method.
    }

    public byte NextBattleReportIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleReportNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ArenaBattleReportUpdateCache ArenaBattleReportInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
