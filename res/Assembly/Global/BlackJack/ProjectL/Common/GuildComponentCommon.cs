﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GuildComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected DataSectionGuild m_guildDS;
    private DateTime m_latGuildSearchTime;
    private DateTime m_latGuildRandomListTime;
    private DateTime m_latGuildInvitePlayerListTime;
    protected MissionComponentCommon m_mission;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFlushPlayerGuild;
    private static DelegateBridge __Hotfix_HasOwnGuild;
    private static DelegateBridge __Hotfix_QuitGuild;
    private static DelegateBridge __Hotfix_GetGuildId;
    private static DelegateBridge __Hotfix_SetGuildId;
    private static DelegateBridge __Hotfix_CanCreateGuild;
    private static DelegateBridge __Hotfix_CanJoinGuild;
    private static DelegateBridge __Hotfix_CanQuitGuild;
    private static DelegateBridge __Hotfix_CanKickOutGuild;
    private static DelegateBridge __Hotfix_CanApplyToJoinGuild;
    private static DelegateBridge __Hotfix_CanConfirmJoinGuildInvitation;
    private static DelegateBridge __Hotfix_RefuseJoinGuildInvitation;
    private static DelegateBridge __Hotfix_RefuseAllJoinGuildInvitation;
    private static DelegateBridge __Hotfix_CheckGuildName;
    private static DelegateBridge __Hotfix_CheckGuildSearch;
    private static DelegateBridge __Hotfix_CheckGuildRandomList;
    private static DelegateBridge __Hotfix_CheckGuildInvitePlayerList;
    private static DelegateBridge __Hotfix_CanSetGuildHiringDeclaration;
    private static DelegateBridge __Hotfix_CheckGuildHiringDeclaration;
    private static DelegateBridge __Hotfix_CanSetGuildAnnouncement;
    private static DelegateBridge __Hotfix_CheckGuildAnnouncement;
    private static DelegateBridge __Hotfix_CanStartMassiveCombat;
    private static DelegateBridge __Hotfix_CanTheseHeroesAttackStronghold;
    private static DelegateBridge __Hotfix_CanAttackStronghold;
    private static DelegateBridge __Hotfix_GetMassiveCombatUnusedHeroes;
    private static DelegateBridge __Hotfix_GetEliminateRate;
    private static DelegateBridge __Hotfix_GetStrongholdEliminateRate;
    private static DelegateBridge __Hotfix_GetStartedCombatThisWeek;
    private static DelegateBridge __Hotfix_get_GuildId;
    private static DelegateBridge __Hotfix_get_NextJoinTime;
    private static DelegateBridge __Hotfix_get_GuildDS;
    private static DelegateBridge __Hotfix_get_GetGuildLastUpdateTime;
    private static DelegateBridge __Hotfix_set_GetGuildLastUpdateTime;
    private static DelegateBridge __Hotfix_add_JoinGuildEvent;
    private static DelegateBridge __Hotfix_remove_JoinGuildEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushPlayerGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwnGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void QuitGuild(DateTime nextJoinTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGuildId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuildId(string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateGuild(string guildName, string hiringDeclaration, int joinLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanJoinGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanQuitGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanKickOutGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanApplyToJoinGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanConfirmJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefuseJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefuseAllJoinGuildInvitation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckGuildName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildSearch(string searchText)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildRandomList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildInvitePlayerList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildHiringDeclaration(string hiringDeclaration)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckGuildAnnouncement(string announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanStartMassiveCombat(int difficulty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTheseHeroesAttackStronghold(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackStronghold(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetMassiveCombatUnusedHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEliminateRate(GuildMassiveCombatInfo combat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStrongholdEliminateRate(GuildMassiveCombatStronghold stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStartedCombatThisWeek(GuildMassiveCombatGeneral generalInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public string GuildId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextJoinTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DataSectionGuild GuildDS
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long GetGuildLastUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action JoinGuildEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
