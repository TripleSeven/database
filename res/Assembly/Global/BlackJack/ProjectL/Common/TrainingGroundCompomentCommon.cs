﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingGroundCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class TrainingGroundCompomentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    public DataSectionTrainingGround m_trainingGroundDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected AnikiGymComponentCommon m_anikiGym;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_GetTechMaxLevel;
    private static DelegateBridge __Hotfix_IsTechLocked;
    private static DelegateBridge __Hotfix_CanLevelup;
    private static DelegateBridge __Hotfix_GetResourceRequirementsByLevel;
    private static DelegateBridge __Hotfix_GetTechLevelupResourceRequirements;
    private static DelegateBridge __Hotfix_CheckTechLevelup;
    private static DelegateBridge __Hotfix_TechLevelup;
    private static DelegateBridge __Hotfix_AddTechLevel;
    private static DelegateBridge __Hotfix_ApplyTrainingTechToHero_1;
    private static DelegateBridge __Hotfix_ApplyTrainingTechToHero_3;
    private static DelegateBridge __Hotfix_ApplyTrainingTechToHero_0;
    private static DelegateBridge __Hotfix_ApplyTrainingTechToHero_2;
    private static DelegateBridge __Hotfix_OutPutTeachnologyTreeOperateLog;
    private static DelegateBridge __Hotfix_GetAvailableTechs;
    private static DelegateBridge __Hotfix_IterateAvailableTechInfos;
    private static DelegateBridge __Hotfix_IterateAvailableTechs;
    private static DelegateBridge __Hotfix_GetTechLevel;
    private static DelegateBridge __Hotfix_GetTech;
    private static DelegateBridge __Hotfix_GetRoom;
    private static DelegateBridge __Hotfix_GetSoldierSkillLevelBySoldierId;
    private static DelegateBridge __Hotfix_get_m_configDataLoader;
    private static DelegateBridge __Hotfix_set_m_configDataLoader;
    private static DelegateBridge __Hotfix_add_TrainingTechLevelupMissionEvent;
    private static DelegateBridge __Hotfix_remove_TrainingTechLevelupMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingGroundCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTechMaxLevel(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTechLocked(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTechResourceRequirements GetResourceRequirementsByLevel(
      int TechId,
      int Level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTechResourceRequirements GetTechLevelupResourceRequirements(
      int TechId,
      int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckTechLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int TechLevelup(int TechId, int DeltaLevel = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTechLevel(int TechId, int DeltaLevel, bool NoCheckAndCost = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyTrainingTechToHero(Hero hero, TrainingTech tech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyTrainingTechToHero(
      Hero hero,
      TrainingTech tech,
      IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyTrainingTechToHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ApplyTrainingTechToHero(
      Hero hero,
      TrainingGroundCompomentCommon trainingGround,
      IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutTeachnologyTreeOperateLog(
      int courseId,
      int techId,
      int currentLvl,
      List<Goods> itemGot,
      List<Goods> itemCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> GetAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTechInfo> IterateAvailableTechInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTech> IterateAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTechLevel(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetTech(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom GetRoom(int RoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierSkillLevelBySoldierId(int SoldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<TrainingTech> TrainingTechLevelupMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
