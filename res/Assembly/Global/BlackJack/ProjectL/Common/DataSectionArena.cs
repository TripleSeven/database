﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionArena : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitArenaPlayerInfo;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_IsEmptyArenaPlayerInfo;
    private static DelegateBridge __Hotfix_SetDefensiveTeam;
    private static DelegateBridge __Hotfix_SetWeekLastFlushTime;
    private static DelegateBridge __Hotfix_SetThisWeekBattleIds;
    private static DelegateBridge __Hotfix_SetDefensiveBattleId;
    private static DelegateBridge __Hotfix_SetDefensiveRuleId;
    private static DelegateBridge __Hotfix_AddVictoryPoints;
    private static DelegateBridge __Hotfix_GetCurrentVictoryPoints;
    private static DelegateBridge __Hotfix_ResetVictoryPoints;
    private static DelegateBridge __Hotfix_ResetReceivedVictoryPointsRewardedIndexs;
    private static DelegateBridge __Hotfix_HasReceivedVictoryPointsRewardedIndex;
    private static DelegateBridge __Hotfix_AddReceivedVictoryPointsRewardIndex;
    private static DelegateBridge __Hotfix_FindOpponent;
    private static DelegateBridge __Hotfix_SetAttackedOpponent;
    private static DelegateBridge __Hotfix_SetOpponents;
    private static DelegateBridge __Hotfix_SetArenaDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_GetArenaDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_GetOpponentUserId;
    private static DelegateBridge __Hotfix_SetArenaLevelId;
    private static DelegateBridge __Hotfix_SetArenaPoints;
    private static DelegateBridge __Hotfix_SetArenaPlayerInfo;
    private static DelegateBridge __Hotfix_IsFirstFindOpponents;
    private static DelegateBridge __Hotfix_ResetConsecutiveVictoryNums;
    private static DelegateBridge __Hotfix_AddConsecutiveVictoryNums;
    private static DelegateBridge __Hotfix_get_ArenaPlayerInfo;
    private static DelegateBridge __Hotfix_set_ArenaPlayerInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaPlayerInfo(ArenaPlayerInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmptyArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveTeam(ArenaPlayerDefensiveTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWeekLastFlushTime(DateTime weekLastFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetThisWeekBattleIds(List<int> battleIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveBattleId(byte battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveRuleId(byte ruleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddVictoryPoints(int points)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentVictoryPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetVictoryPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetReceivedVictoryPointsRewardedIndexs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasReceivedVictoryPointsRewardedIndex(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddReceivedVictoryPointsRewardIndex(int victoryPointsIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponent FindOpponent(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAttackedOpponent(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpponents(List<ArenaOpponent> opponents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaDefensiveBattleInfo(ArenaOpponentDefensiveBattleInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo GetArenaDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetOpponentUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaLevelId(int arenaLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaPoints(ushort ArenaPoints)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaPlayerInfo(ArenaPlayerInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstFindOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetConsecutiveVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddConsecutiveVictoryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerInfo ArenaPlayerInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
