﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionCollection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionCollection : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_FindCollectionActivity;
    private static DelegateBridge __Hotfix_RemoveCollectionActivity;
    private static DelegateBridge __Hotfix_AddCollectionActivity;
    private static DelegateBridge __Hotfix_AddChallengeLevelId;
    private static DelegateBridge __Hotfix_AddLootLevelId;
    private static DelegateBridge __Hotfix_IsChallengeLevelFinished;
    private static DelegateBridge __Hotfix_IsLootLevelFinished;
    private static DelegateBridge __Hotfix_SetCurrentWayPointId;
    private static DelegateBridge __Hotfix_SetScenarioId;
    private static DelegateBridge __Hotfix_AddScore;
    private static DelegateBridge __Hotfix_RemoveCollectionEvents;
    private static DelegateBridge __Hotfix_AddCollectionEvents;
    private static DelegateBridge __Hotfix_AddExchangeCount;
    private static DelegateBridge __Hotfix_GetExchangeCount;
    private static DelegateBridge __Hotfix_GetExchangeInfoList;
    private static DelegateBridge __Hotfix_get_CollectionActivities;
    private static DelegateBridge __Hotfix_set_CollectionActivities;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionCollection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity FindCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionActivity(CollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChallengeLevelId(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLootLevelId(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelFinished(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelFinished(CollectionActivity acitivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentWayPointId(CollectionActivity acitivity, int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScenarioId(CollectionActivity acitivity, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddScore(CollectionActivity acitivity, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionEvents(List<CollectionEvent> Events, ulong activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionEvents(List<CollectionEvent> Events, ulong activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddExchangeCount(CollectionActivity activity, int exchangeID, int delta)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetExchangeCount(CollectionActivity activity, int exchangeID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivityPlayerExchangeInfo> GetExchangeInfoList(
      CollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<CollectionActivity> CollectionActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
