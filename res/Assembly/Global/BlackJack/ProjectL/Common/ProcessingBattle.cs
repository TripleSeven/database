﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ProcessingBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ProcessingBattle
  {
    public List<int> Params;
    public List<ulong> ULongParams;
    public List<string> StrParams;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Type;
    private static DelegateBridge __Hotfix_set_Type;
    private static DelegateBridge __Hotfix_get_TypeId;
    private static DelegateBridge __Hotfix_set_TypeId;
    private static DelegateBridge __Hotfix_get_RandomSeed;
    private static DelegateBridge __Hotfix_set_RandomSeed;
    private static DelegateBridge __Hotfix_get_ArmyRandomSeed;
    private static DelegateBridge __Hotfix_set_ArmyRandomSeed;
    private static DelegateBridge __Hotfix_BattleProcessingToPbBattleProcessing;
    private static DelegateBridge __Hotfix_PbBattleProcessingToBattleProcessing;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleType Type
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArmyRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleProcessing BattleProcessingToPbBattleProcessing(
      ProcessingBattle battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProcessingBattle PbBattleProcessingToBattleProcessing(
      ProBattleProcessing pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
