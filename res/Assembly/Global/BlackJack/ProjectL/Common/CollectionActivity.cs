﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CollectionActivity
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_CollectionEvents;
    private static DelegateBridge __Hotfix_set_CollectionEvents;
    private static DelegateBridge __Hotfix_get_ActivityInstanceId;
    private static DelegateBridge __Hotfix_set_ActivityInstanceId;
    private static DelegateBridge __Hotfix_get_FinishedChallengeLevelIds;
    private static DelegateBridge __Hotfix_set_FinishedChallengeLevelIds;
    private static DelegateBridge __Hotfix_get_LastFinishedScenarioId;
    private static DelegateBridge __Hotfix_set_LastFinishedScenarioId;
    private static DelegateBridge __Hotfix_get_FinishedLootLevelIds;
    private static DelegateBridge __Hotfix_set_FinishedLootLevelIds;
    private static DelegateBridge __Hotfix_get_CurrentWayPointId;
    private static DelegateBridge __Hotfix_set_CurrentWayPointId;
    private static DelegateBridge __Hotfix_get_Score;
    private static DelegateBridge __Hotfix_set_Score;
    private static DelegateBridge __Hotfix_get_ExchangeInfoList;
    private static DelegateBridge __Hotfix_set_ExchangeInfoList;
    private static DelegateBridge __Hotfix_ToPb;
    private static DelegateBridge __Hotfix_FromPb;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<CollectionEvent> CollectionEvents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong ActivityInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> FinishedChallengeLevelIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastFinishedScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> FinishedLootLevelIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentWayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Score
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<CollectionActivityPlayerExchangeInfo> ExchangeInfoList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProCollectionActivity ToPb(CollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CollectionActivity FromPb(ProCollectionActivity activity)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
