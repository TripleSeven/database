﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RandomStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected DataSectionRandomStore m_randomStoreDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFlushManualFlushNums;
    private static DelegateBridge __Hotfix_CanAutoFlushStore;
    private static DelegateBridge __Hotfix_IsBoughtStoreItem;
    private static DelegateBridge __Hotfix_CanManualFlushStore;
    private static DelegateBridge __Hotfix_CanBuyRandomStoreItem;
    private static DelegateBridge __Hotfix_OnBuyStoreItem;
    private static DelegateBridge __Hotfix_add_BuyStoreItemEvent;
    private static DelegateBridge __Hotfix_remove_BuyStoreItemEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushManualFlushNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAutoFlushStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBoughtStoreItem(RandomStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanManualFlushStore(RandomStore store, ConfigDataRandomStoreInfo storeInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyRandomStoreItem(int storeId, int index, int selectedIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBuyStoreItem(int storeId, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
