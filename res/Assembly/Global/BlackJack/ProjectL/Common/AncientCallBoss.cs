﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCallBoss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class AncientCallBoss
  {
    public List<int> TeamList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_BossId;
    private static DelegateBridge __Hotfix_set_BossId;
    private static DelegateBridge __Hotfix_get_CurrentPeriodMaxDamage;
    private static DelegateBridge __Hotfix_set_CurrentPeriodMaxDamage;
    private static DelegateBridge __Hotfix_get_CurrentMaxDamage;
    private static DelegateBridge __Hotfix_set_CurrentMaxDamage;
    private static DelegateBridge __Hotfix_get_TeamListBattlePower;
    private static DelegateBridge __Hotfix_set_TeamListBattlePower;
    private static DelegateBridge __Hotfix_get_UpdateTime;
    private static DelegateBridge __Hotfix_set_UpdateTime;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_FromPB;
    private static DelegateBridge __Hotfix_UpdateDamage;
    private static DelegateBridge __Hotfix_Reset;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int BossId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentPeriodMaxDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentMaxDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamListBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime UpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCallBoss ToPB(AncientCallBoss boss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCallBoss FromPB(ProAncientCallBoss pbBoss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateDamage(
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Reset()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
