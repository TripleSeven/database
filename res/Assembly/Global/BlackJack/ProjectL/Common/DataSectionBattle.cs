﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionBattle : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitProcessingBattle;
    private static DelegateBridge __Hotfix_InitTeams;
    private static DelegateBridge __Hotfix_SetTeam;
    private static DelegateBridge __Hotfix_GetTeam;
    private static DelegateBridge __Hotfix_SetProcessingBattleInfo;
    private static DelegateBridge __Hotfix_SetRandomSeed;
    private static DelegateBridge __Hotfix_SetArmyRandomSeed;
    private static DelegateBridge __Hotfix_GetArmyRandomSeed;
    private static DelegateBridge __Hotfix_IsGotBattleTreasureId;
    private static DelegateBridge __Hotfix_AddBattleTreasureId;
    private static DelegateBridge __Hotfix_ClearBattleTreasure;
    private static DelegateBridge __Hotfix_GetArmyRandomSeedByBattleId;
    private static DelegateBridge __Hotfix_RemoveEveryTimeArmyRandomSeed;
    private static DelegateBridge __Hotfix_AddEveryTimeArmyRandomSeed;
    private static DelegateBridge __Hotfix_AddDailyTimeArmyRandomSeed;
    private static DelegateBridge __Hotfix_InitDailyTimeArmyRandomSeed;
    private static DelegateBridge __Hotfix_InitEveryTimeArmyRandomSeed;
    private static DelegateBridge __Hotfix_ClearDailyArmyRandomSeeds;
    private static DelegateBridge __Hotfix_ClearProcesingBattleInfo;
    private static DelegateBridge __Hotfix_get_ProcessingBattleInfo;
    private static DelegateBridge __Hotfix_set_ProcessingBattleInfo;
    private static DelegateBridge __Hotfix_get_Teams;
    private static DelegateBridge __Hotfix_set_Teams;
    private static DelegateBridge __Hotfix_get_GotBattleTreasureIds;
    private static DelegateBridge __Hotfix_set_GotBattleTreasureIds;
    private static DelegateBridge __Hotfix_get_DailyArmyRandomSeedDict;
    private static DelegateBridge __Hotfix_set_DailyArmyRandomSeedDict;
    private static DelegateBridge __Hotfix_get_EveryTimeArmyRandomSeedDict;
    private static DelegateBridge __Hotfix_set_EveryTimeArmyRandomSeedDict;
    private static DelegateBridge __Hotfix_get_ArenaBattleStatus;
    private static DelegateBridge __Hotfix_set_ArenaBattleStatus;
    private static DelegateBridge __Hotfix_get_ArenaBattleId;
    private static DelegateBridge __Hotfix_set_ArenaBattleId;
    private static DelegateBridge __Hotfix_get_ArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_set_ArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_get_BattleRoomId;
    private static DelegateBridge __Hotfix_set_BattleRoomId;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitProcessingBattle(ProcessingBattle info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTeams(Dictionary<int, List<int>> teams)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeam(int teamTypeId, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetTeam(int teamTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProcessingBattleInfo(BattleType type, int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomSeed(int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArmyRandomSeed(int ArmyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArmyRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGotBattleTreasureId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleTreasureId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleTreasure()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArmyRandomSeedByBattleId(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveEveryTimeArmyRandomSeed(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEveryTimeArmyRandomSeed(int battleId, int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDailyTimeArmyRandomSeed(int battleId, int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDailyTimeArmyRandomSeed(int bettleId, int armyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEveryTimeArmyRandomSeed(int bettleId, int armyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearDailyArmyRandomSeeds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearProcesingBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ProcessingBattle ProcessingBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, List<int>> Teams
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> GotBattleTreasureIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, int> DailyArmyRandomSeedDict
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, int> EveryTimeArmyRandomSeedDict
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ArenaBattleStatus ArenaBattleStatus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaBattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaBattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong BattleRoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
