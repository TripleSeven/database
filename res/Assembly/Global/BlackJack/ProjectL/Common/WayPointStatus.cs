﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.WayPointStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum WayPointStatus
  {
    None = 0,
    Close = 1,
    Open = 2,
    Public = 4,
    Arrived = 8,
    NormalEvent = 16, // 0x00000010
    RandomEvent = 32, // 0x00000020
  }
}
