﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [Serializable]
  public class ChatMessage
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ChannelId;
    private static DelegateBridge __Hotfix_set_ChannelId;
    private static DelegateBridge __Hotfix_get_ChatSrcType;
    private static DelegateBridge __Hotfix_set_ChatSrcType;
    private static DelegateBridge __Hotfix_get_SrcName;
    private static DelegateBridge __Hotfix_set_SrcName;
    private static DelegateBridge __Hotfix_get_AvatarId;
    private static DelegateBridge __Hotfix_set_AvatarId;
    private static DelegateBridge __Hotfix_get_ChatContentType;
    private static DelegateBridge __Hotfix_set_ChatContentType;
    private static DelegateBridge __Hotfix_get_SrcPlayerLevel;
    private static DelegateBridge __Hotfix_set_SrcPlayerLevel;
    private static DelegateBridge __Hotfix_get_SrcGameUserID;
    private static DelegateBridge __Hotfix_set_SrcGameUserID;
    private static DelegateBridge __Hotfix_get_SendTime;
    private static DelegateBridge __Hotfix_set_SendTime;
    private static DelegateBridge __Hotfix_get_DestGameUserId;
    private static DelegateBridge __Hotfix_set_DestGameUserId;
    private static DelegateBridge __Hotfix_get_DestChatGroupId;
    private static DelegateBridge __Hotfix_set_DestChatGroupId;
    private static DelegateBridge __Hotfix_get_Title;
    private static DelegateBridge __Hotfix_set_Title;
    private static DelegateBridge __Hotfix_ToPbChatMessage;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    public ChatChannel ChannelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ChatSrcType ChatSrcType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string SrcName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AvatarId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ChatContentType ChatContentType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SrcPlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string SrcGameUserID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime SendTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string DestGameUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string DestChatGroupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf ToPbChatMessage()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
