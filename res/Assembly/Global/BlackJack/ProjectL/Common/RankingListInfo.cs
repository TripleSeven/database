﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RankingListInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RankingListInfo
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Type;
    private static DelegateBridge __Hotfix_set_Type;
    private static DelegateBridge __Hotfix_get_Score;
    private static DelegateBridge __Hotfix_set_Score;
    private static DelegateBridge __Hotfix_get_CurrRank;
    private static DelegateBridge __Hotfix_set_CurrRank;
    private static DelegateBridge __Hotfix_get_LastRank;
    private static DelegateBridge __Hotfix_set_LastRank;
    private static DelegateBridge __Hotfix_get_ChampionHeroId;
    private static DelegateBridge __Hotfix_set_ChampionHeroId;
    private static DelegateBridge __Hotfix_get_PlayerList;
    private static DelegateBridge __Hotfix_set_PlayerList;
    private static DelegateBridge __Hotfix_get_TotalTargetCount;
    private static DelegateBridge __Hotfix_set_TotalTargetCount;
    private static DelegateBridge __Hotfix_get_BossId;
    private static DelegateBridge __Hotfix_set_BossId;
    private static DelegateBridge __Hotfix_RankingListToPBRankingList;
    private static DelegateBridge __Hotfix_PBRankingListToRankingList;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public RankingListType Type
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Score
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChampionHeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<RankingTargetPlayerInfo> PlayerList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalTargetCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BossId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRankingListInfo RankingListToPBRankingList(
      RankingListInfo rankingList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RankingListInfo PBRankingListToRankingList(
      ProRankingListInfo proRankingList)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
