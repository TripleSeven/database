﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class AncientCall
  {
    public List<int> HasGainAchievementRelationIds;
    public AncientCallPeriod PeriodInfo;
    public List<AncientCallBossHistory> BossHistories;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetSpecificBossHistoryMaxDamage;
    private static DelegateBridge __Hotfix_ClearPeriodInfo;
    private static DelegateBridge __Hotfix_FromPB;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_UpdateBossDamage;
    private static DelegateBridge __Hotfix_IsCurrentPeriodExist;
    private static DelegateBridge __Hotfix_SetCurrentBoss;
    private static DelegateBridge __Hotfix_SetCurrentPeriodInfo;
    private static DelegateBridge __Hotfix_UpdateBossHistoryPeriodMaxRank;
    private static DelegateBridge __Hotfix_IsPeriodChanged;
    private static DelegateBridge __Hotfix_IsCurrentBoosChanged;
    private static DelegateBridge __Hotfix_FindBossById;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCall()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSpecificBossHistoryMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCall FromPB(ProAncientCall pbCall)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCall ToPB(AncientCall call)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateBossDamage(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentPeriodExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentBoss(int currentBossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentPeriodInfo(int periodId, int bossIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateBossHistoryPeriodMaxRank(int bossId, int finalRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeriodChanged(int periodId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentBoosChanged(int bossIdIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss FindBossById(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
