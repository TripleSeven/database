﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CardPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CardPool
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_PoolId;
    private static DelegateBridge __Hotfix_set_PoolId;
    private static DelegateBridge __Hotfix_get_SummonedRareHero;
    private static DelegateBridge __Hotfix_set_SummonedRareHero;
    private static DelegateBridge __Hotfix_get_SelectCardCount;
    private static DelegateBridge __Hotfix_set_SelectCardCount;
    private static DelegateBridge __Hotfix_get_DisconnectCount;
    private static DelegateBridge __Hotfix_set_DisconnectCount;
    private static DelegateBridge __Hotfix_get_IsFirstSignleSelect;
    private static DelegateBridge __Hotfix_set_IsFirstSignleSelect;
    private static DelegateBridge __Hotfix_get_IsFirstTenSelect;
    private static DelegateBridge __Hotfix_set_IsFirstTenSelect;
    private static DelegateBridge __Hotfix_get_ActivityInstanceId;
    private static DelegateBridge __Hotfix_set_ActivityInstanceId;
    private static DelegateBridge __Hotfix_get_GuaranteedEquipmentSuitSelectCardCount;
    private static DelegateBridge __Hotfix_set_GuaranteedEquipmentSuitSelectCardCount;
    private static DelegateBridge __Hotfix_get_GuaranteedEquipmentSuitSelectCardMaxCount;
    private static DelegateBridge __Hotfix_set_GuaranteedEquipmentSuitSelectCardMaxCount;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_set_Config;
    private static DelegateBridge __Hotfix_IsReachEquipmentSuitGuaranteedValue;
    private static DelegateBridge __Hotfix_PBCardPoolToCardPool;
    private static DelegateBridge __Hotfix_PBActivityCardPoolsToCardPools;
    private static DelegateBridge __Hotfix_CardPoolToPBCardPool;
    private static DelegateBridge __Hotfix_CardPoolsToPBActivityCardPools;

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int PoolId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool SummonedRareHero
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SelectCardCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DisconnectCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFirstSignleSelect
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFirstTenSelect
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong ActivityInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GuaranteedEquipmentSuitSelectCardCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GuaranteedEquipmentSuitSelectCardMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCardPoolInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachEquipmentSuitGuaranteedValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CardPool PBCardPoolToCardPool(ProCardPool pbCardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<CardPool> PBActivityCardPoolsToCardPools(
      List<ProCardPool> pbCardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProCardPool CardPoolToPBCardPool(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProCardPool> CardPoolsToPBActivityCardPools(
      List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
