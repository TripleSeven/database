﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AccumulateRechargeOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class AccumulateRechargeOperationalActivity : AwardOperationalActivityBase
  {
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_DeserializeFromPB;
    private static DelegateBridge __Hotfix_ToPBNtf;
    private static DelegateBridge __Hotfix_SerializeToPB;
    private static DelegateBridge __Hotfix_CanGainRewardByIndex;
    private static DelegateBridge __Hotfix_AddRechargeRMB;
    private static DelegateBridge __Hotfix_get_AccumulateRechargeRMB;
    private static DelegateBridge __Hotfix_set_AccumulateRechargeRMB;

    [MethodImpl((MethodImplOptions) 32768)]
    public AccumulateRechargeOperationalActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AccumulateRechargeOperationalActivity(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeserializeFromPB(
      ProAccumulateRechargeOperationalActivity pbOperationalActivity,
      ConfigDataOperationalActivityInfo config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateRechargeOperationalActivity SerializeToPB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanGainRewardByIndex(int rewardIndex, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRechargeRMB(int addNums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int AccumulateRechargeRMB
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
