﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnThreadSafeLRUCacheObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UnThreadSafeLRUCacheObject : CacheObject, ILRUCacheObject
  {
    private long m_lastReadTime;
    private long m_lastUpdateTime;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SetLastUpdateTime;
    private static DelegateBridge __Hotfix_GetLastUpdateTime;
    private static DelegateBridge __Hotfix_SetLastReadTime;
    private static DelegateBridge __Hotfix_GetLastReadTime;
    private static DelegateBridge __Hotfix_GetNewestTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnThreadSafeLRUCacheObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastUpdateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetLastUpdateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastReadTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetLastReadTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetNewestTime()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
