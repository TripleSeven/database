﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomBattleLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Battle;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleRoomBattleLog
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_set_RoomId;
    private static DelegateBridge __Hotfix_get_BattleRoomType;
    private static DelegateBridge __Hotfix_set_BattleRoomType;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_RandomNumberSeed;
    private static DelegateBridge __Hotfix_set_RandomNumberSeed;
    private static DelegateBridge __Hotfix_get_ArmyRandomNumberSeed;
    private static DelegateBridge __Hotfix_set_ArmyRandomNumberSeed;
    private static DelegateBridge __Hotfix_get_Team0;
    private static DelegateBridge __Hotfix_set_Team0;
    private static DelegateBridge __Hotfix_get_Team1;
    private static DelegateBridge __Hotfix_set_Team1;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_set_Players;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomBattleLog()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleRoomType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RandomNumberSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArmyRandomNumberSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleActorSetup> Team0
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleActorSetup> Team1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattlePlayer> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
