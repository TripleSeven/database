﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FixedStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class FixedStore
  {
    public List<FixedStoreItem> Items;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_PBFixedStoreToFixedStore;
    private static DelegateBridge __Hotfix_PBFixedStoresToFixedStores;
    private static DelegateBridge __Hotfix_FixedStoreToPBFixedStore;
    private static DelegateBridge __Hotfix_FixedStoresToPBFixedStores;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static FixedStore PBFixedStoreToFixedStore(ProFixedStore pbStore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<FixedStore> PBFixedStoresToFixedStores(
      List<ProFixedStore> pbStores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProFixedStore FixedStoreToPBFixedStore(FixedStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProFixedStore> FixedStoresToPBFixedStores(
      List<FixedStore> stores)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
