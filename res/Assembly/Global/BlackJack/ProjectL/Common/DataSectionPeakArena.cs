﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionPeakArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionPeakArena : DataSection
  {
    public PeakArenaMatchStats FriendlyMatchStats;
    public PeakArenaMatchStats LadderMatchStats;
    public PeakArenaMatchStats FriendlyCareerMatchStats;
    public PeakArenaMatchStats LadderCareerMatchStats;
    public List<int> WinsBonusIdAcquired;
    public List<int> RegularRewardIndexAcquired;
    public int CurrentSeasonRealTimeRank;
    public bool NoticeEnterPlayOffRace;
    public PeakArenaBattleTeam BattleTeam;
    public List<PeakArenaBattleReportNameRecord> BattleReportNameRecords;
    public List<BattleReportHead> BattleReports;
    public bool BattleReportInited;
    public PeakArenaBetInfo BetInfo;
    public ulong LiveRoomId;
    public DateTime BannedTime;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_SetNoticeEnterPlayOffRace;
    private static DelegateBridge __Hotfix_WeeklyRefresh;
    private static DelegateBridge __Hotfix_SaveStatistics;
    private static DelegateBridge __Hotfix_StartNewSeason;
    private static DelegateBridge __Hotfix_AcquireWinsBonus;
    private static DelegateBridge __Hotfix_AcquireRegularReward;
    private static DelegateBridge __Hotfix_AddBattleReportNameRecord;
    private static DelegateBridge __Hotfix_GetBattleReportName;
    private static DelegateBridge __Hotfix_ExistBattleReportNameRecord;
    private static DelegateBridge __Hotfix_RemoveFirstBattleReportRecord;
    private static DelegateBridge __Hotfix_AddBattleReport;
    private static DelegateBridge __Hotfix_RemoveFirstBattleReport;
    private static DelegateBridge __Hotfix_BetPeakArena;
    private static DelegateBridge __Hotfix_SettlePeakArenaMatchupBetReward;
    private static DelegateBridge __Hotfix_BetDailyFlush;
    private static DelegateBridge __Hotfix_SetLiveRoomId;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_ResetBattleReportNameRecords;
    private static DelegateBridge __Hotfix_get_PreRankingScore;
    private static DelegateBridge __Hotfix_set_PreRankingScore;
    private static DelegateBridge __Hotfix_get_CurrentSeason;
    private static DelegateBridge __Hotfix_set_CurrentSeason;
    private static DelegateBridge __Hotfix_get_CurrentSeasonFinalRank;
    private static DelegateBridge __Hotfix_set_CurrentSeasonFinalRank;
    private static DelegateBridge __Hotfix_get_TodaysQuickLeavingCount;
    private static DelegateBridge __Hotfix_set_TodaysQuickLeavingCount;
    private static DelegateBridge __Hotfix_get_MatchmakingBannedTime;
    private static DelegateBridge __Hotfix_set_MatchmakingBannedTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPeakArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNoticeEnterPlayOffRace(bool notice)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WeeklyRefresh()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveStatistics(int Type, bool Win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartNewSeason(int initScore, int season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AcquireRegularReward(int RewardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleReportNameRecord(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportName(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistBattleReportNameRecord(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveFirstBattleReportRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleReport(BattleReportHead report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveFirstBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetPeakArena(int seasonId, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SettlePeakArenaMatchupBetReward(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetDailyFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLiveRoomId(ulong liveRoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBattleReportNameRecords()
    {
      // ISSUE: unable to decompile the method.
    }

    public int PreRankingScore
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentSeason
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentSeasonFinalRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TodaysQuickLeavingCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime MatchmakingBannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
