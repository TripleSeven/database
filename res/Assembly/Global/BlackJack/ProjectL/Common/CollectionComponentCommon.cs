﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CollectionComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionCollection m_collectionDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_HandleTresureEvent;
    private static DelegateBridge __Hotfix_HandleDialogEvent;
    private static DelegateBridge __Hotfix_GetEventRewards;
    private static DelegateBridge __Hotfix_RemoveEvent;
    private static DelegateBridge __Hotfix_IsEventTimeOut;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_IterateAvailableExchangeItems;
    private static DelegateBridge __Hotfix_CanExchangeItem;
    private static DelegateBridge __Hotfix_ExchangeItem;
    private static DelegateBridge __Hotfix_GetCollectionActivityInfo_0;
    private static DelegateBridge __Hotfix_GetActivityInstanceId;
    private static DelegateBridge __Hotfix_GetCollectionActivityInfo_1;
    private static DelegateBridge __Hotfix_GetEventExpiredTime;
    private static DelegateBridge __Hotfix_GetCollectionActivity;
    private static DelegateBridge __Hotfix_CanMoveToWayPoint;
    private static DelegateBridge __Hotfix_MoveToWayPoint;
    private static DelegateBridge __Hotfix_CanHandleScenario;
    private static DelegateBridge __Hotfix_IsNextScenarioId;
    private static DelegateBridge __Hotfix_GetNextScenarioId;
    private static DelegateBridge __Hotfix_CanAttackLevel;
    private static DelegateBridge __Hotfix_CanAttackScenarioLevel;
    private static DelegateBridge __Hotfix_CanAttackChallengeLevel;
    private static DelegateBridge __Hotfix_CanAttackLootLevel;
    private static DelegateBridge __Hotfix__IsLootLevelUnlock;
    private static DelegateBridge __Hotfix_IsPrevLevelUnlock;
    private static DelegateBridge __Hotfix_IsScenarioFinished;
    private static DelegateBridge __Hotfix_IsChallengeLevelFinished;
    private static DelegateBridge __Hotfix_AttackLevel;
    private static DelegateBridge __Hotfix_GetAllUnlockedLootLevels;
    private static DelegateBridge __Hotfix_IsLootLevelUnlock;
    private static DelegateBridge __Hotfix_AddFinishedLootLevel;
    private static DelegateBridge __Hotfix_GetAllEventsByInstanceId;
    private static DelegateBridge __Hotfix_SetCommonSuccessLevel;
    private static DelegateBridge __Hotfix_SendAddEvent;
    private static DelegateBridge __Hotfix_SendDeleteEvent;
    private static DelegateBridge __Hotfix_CollectionEventHandle;
    private static DelegateBridge __Hotfix_CanHandleCollectionEvent;
    private static DelegateBridge __Hotfix_HandleCollectionEvent;
    private static DelegateBridge __Hotfix_AttackEvent;
    private static DelegateBridge __Hotfix_CanAttackEvent;
    private static DelegateBridge __Hotfix_CanAttackCollectionEvent;
    private static DelegateBridge __Hotfix_AddCollectionEvent;
    private static DelegateBridge __Hotfix_DeleteCollectionEvent;
    private static DelegateBridge __Hotfix_OnEventComplete;
    private static DelegateBridge __Hotfix_SetBattleEventSuccessful;
    private static DelegateBridge __Hotfix_SortByExpiredTimeAscend;
    private static DelegateBridge __Hotfix_TryDeleteCompleteTime;
    private static DelegateBridge __Hotfix_TryHandleCollectionEvent;
    private static DelegateBridge __Hotfix_TryDeleteCollectionEventChallengeLevel;
    private static DelegateBridge __Hotfix_TryProduceCollectionEvent;
    private static DelegateBridge __Hotfix_GetCollectionActivityWayPointState;
    private static DelegateBridge __Hotfix_IsNewCollectionActivityOpened;
    private static DelegateBridge __Hotfix_GetNewFinishedScenarioLevelInfos;
    private static DelegateBridge __Hotfix_TryDeleteCollectionEventScenario;
    private static DelegateBridge __Hotfix_AddScore;
    private static DelegateBridge __Hotfix_CalculateScore;
    private static DelegateBridge __Hotfix_CalculateAllHeroBonus;
    private static DelegateBridge __Hotfix_FindCollectionActivity;
    private static DelegateBridge __Hotfix_RemoveCollectionActivity;
    private static DelegateBridge __Hotfix_AddCollectionActivity;
    private static DelegateBridge __Hotfix_add_CompleteCollectionEventMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteCollectionEventMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleDialogEvent(
      ConfigDataCollectionActivityWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int expReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void RemoveEvent(List<CollectionEvent> Events, ulong activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventTimeOut(CollectionEvent Event)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<ConfigDataCollectionActivityExchangeTableInfo> IterateAvailableExchangeItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ExchangeItem(ulong activityInstanceId, int exchangeItemID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo(
      ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetActivityInstanceId(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCollectionActivityInfo GetCollectionActivityInfo(
      GameFunctionType typeId,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected long GetEventExpiredTime(int EventinfoId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity GetCollectionActivity(
      GameFunctionType typeId,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanMoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanHandleScenario(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsNextScenarioId(ulong activityInstanceId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextScenarioId(ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackLevel(GameFunctionType typeId, int levelId, bool team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackScenarioLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackChallengeLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLootLevel(
      CollectionActivity collectionActivity,
      int levelId,
      int deltaDays,
      bool team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int _IsLootLevelUnlock(
      CollectionActivity collectionActivity,
      ConfigDataCollectionActivityLootLevelInfo levelInfo,
      int deltaDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsPrevLevelUnlock(CollectionActivity activity, List<LevelInfo> PrevLevels)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(CollectionActivity collectionActivity, int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelFinished(CollectionActivity collectionActivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackLevel(GameFunctionType typeId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLootLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLootLevelUnlock(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFinishedLootLevel(CollectionActivity collectionActivity, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> GetAllEventsByInstanceId(
      ulong ActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessLevel(
      CollectionActivity collectionActivity,
      GameFunctionType typeId,
      int levelId,
      List<int> heroes,
      List<int> allHeroes,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SendAddEvent(List<CollectionEvent> events, ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SendDeleteEvent(List<CollectionEvent> events, ulong activityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CollectionEventHandle(ulong activityId, int EventId, int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHandleCollectionEvent(ulong activityId, int wayPointId, int EventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleCollectionEvent(
      ulong activityId,
      int EventId,
      ConfigDataEventInfo eventInfo,
      ConfigDataCollectionActivityWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackEvent(ulong activityId, int wayPointId, int EventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEvent(ulong activityId, int wayPointId, int EventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackCollectionEvent(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeleteCollectionEvent(List<CollectionEvent> events, ulong ActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnEventComplete(
      ulong activityId,
      int EventId,
      int wayPointId,
      ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetBattleEventSuccessful(
      ConfigDataCollectionActivityWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SortByExpiredTimeAscend(List<CollectionEvent> events)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryDeleteCompleteTime(
      CollectionActivity collectionActivity,
      int EventId,
      int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryHandleCollectionEvent(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryDeleteCollectionEventChallengeLevel(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryProduceCollectionEvent(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypointStateType GetCollectionActivityWayPointState(
      CollectionActivity collectionActivity,
      int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNewCollectionActivityOpened(int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataCollectionActivityScenarioLevelInfo> GetNewFinishedScenarioLevelInfos(
      CollectionActivity collectionActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionEvent> TryDeleteCollectionEventScenario(
      CollectionActivity collectionActivity,
      int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddScore(
      CollectionActivity collectionActivity,
      int scoreBase,
      List<int> allHeroes,
      GameFunctionType causeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateScore(
      ConfigDataCollectionActivityInfo info,
      int scoreBase,
      List<int> allHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateAllHeroBonus(ConfigDataCollectionActivityInfo info, List<int> allHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivity FindCollectionActivity(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCollectionActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCollectionActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> CompleteCollectionEventMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
