﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatVoiceMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [Serializable]
  public class ChatVoiceMessage : ChatMessage
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_VoiceData;
    private static DelegateBridge __Hotfix_set_VoiceData;
    private static DelegateBridge __Hotfix_get_VoiceLength;
    private static DelegateBridge __Hotfix_set_VoiceLength;
    private static DelegateBridge __Hotfix_get_AudioFrequency;
    private static DelegateBridge __Hotfix_set_AudioFrequency;
    private static DelegateBridge __Hotfix_get_SampleLength;
    private static DelegateBridge __Hotfix_set_SampleLength;
    private static DelegateBridge __Hotfix_get_IsOverdued;
    private static DelegateBridge __Hotfix_set_IsOverdued;
    private static DelegateBridge __Hotfix_get_TranslateText;
    private static DelegateBridge __Hotfix_set_TranslateText;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatVoiceMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte[] VoiceData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int VoiceLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AudioFrequency
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SampleLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsOverdued
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string TranslateText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
