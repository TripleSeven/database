﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleCommand
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleCommand
  {
    public BattleCommandType Type;
    public int Step;
    public int ActorId;
    public int SkillIndex;
    public GridPosition TargetPosition;
    public GridPosition TargetPosition2;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_PBBattleCommandToBattleCommand;
    private static DelegateBridge __Hotfix_BattleCommandToPBBattleCommand;
    private static DelegateBridge __Hotfix_GridPositionToPBBattlePosition;
    private static DelegateBridge __Hotfix_PBBattlePositionToGridPosition;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleCommand PBBattleCommandToBattleCommand(
      ProBattleCommand pbBattleCommand)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleCommand BattleCommandToPBBattleCommand(
      BattleCommand battleCommand)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattlePosition GridPositionToPBBattlePosition(
      GridPosition gridPosition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GridPosition PBBattlePositionToGridPosition(
      ProBattlePosition pbBattlePosition)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
