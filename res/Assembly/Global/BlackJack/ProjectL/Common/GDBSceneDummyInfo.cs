﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBSceneDummyInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "GDBSceneDummyInfo")]
  [Serializable]
  public class GDBSceneDummyInfo : IExtensible
  {
    private int _Id;
    private int _confId;
    private string _name;
    private PVector3D _location;
    private uint _rotationY;
    private bool _isNeedLocalization;
    private string _localizationKey;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_ConfId;
    private static DelegateBridge __Hotfix_set_ConfId;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_Location;
    private static DelegateBridge __Hotfix_set_Location;
    private static DelegateBridge __Hotfix_get_RotationY;
    private static DelegateBridge __Hotfix_set_RotationY;
    private static DelegateBridge __Hotfix_get_IsNeedLocalization;
    private static DelegateBridge __Hotfix_set_IsNeedLocalization;
    private static DelegateBridge __Hotfix_get_LocalizationKey;
    private static DelegateBridge __Hotfix_set_LocalizationKey;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBSceneDummyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "confId")]
    public int ConfId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "name")]
    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "location")]
    public PVector3D Location
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "rotationY")]
    public uint RotationY
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "isNeedLocalization")]
    public bool IsNeedLocalization
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "localizationKey")]
    public string LocalizationKey
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
