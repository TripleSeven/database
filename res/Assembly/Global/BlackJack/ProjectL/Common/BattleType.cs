﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum BattleType
  {
    None = 0,
    WayPoint = 1,
    Rift = 2,
    ThearchyTrial = 3,
    AnikiGym = 4,
    ArenaAttack = 5,
    HeroDungeon = 6,
    Scenario = 7,
    TreasureMap = 8,
    MemoryCorridor = 9,
    HeroTrainning = 10, // 0x0000000A
    PVP = 11, // 0x0000000B
    HeroPhantom = 12, // 0x0000000C
    CooperateBattle = 13, // 0x0000000D
    RealTimePVP = 14, // 0x0000000E
    UnchartedScore_ChallengeLevel = 15, // 0x0000000F
    UnchartedScore_ScoreLevel = 16, // 0x00000010
    ClimbTower = 17, // 0x00000011
    GuildMassiveCombat = 18, // 0x00000012
    EternalShrine = 22, // 0x00000016
    CollectionActivityScenario = 25, // 0x00000019
    CollectionActivityChallenge = 26, // 0x0000001A
    CollectionActivityLoot = 27, // 0x0000001B
    HeroAnthem = 28, // 0x0000001C
    PeakArena = 29, // 0x0000001D
    CollectionEvent = 30, // 0x0000001E
    AncientCall = 31, // 0x0000001F
  }
}
