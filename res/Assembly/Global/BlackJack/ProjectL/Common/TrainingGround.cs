﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingGround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class TrainingGround
  {
    public AnikiGymLevelClearCheck IsAnikiGymLevelCleared;
    public List<TrainingRoom> Rooms;
    private IConfigDataLoader _ConfigDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_ReloadConfigData;
    private static DelegateBridge __Hotfix_UpdateInstantiatedData;
    private static DelegateBridge __Hotfix_GetRoom;
    private static DelegateBridge __Hotfix_GetLearntTech;
    private static DelegateBridge __Hotfix_GetUnlearntTech;
    private static DelegateBridge __Hotfix_GetAvailableTechs;
    private static DelegateBridge __Hotfix_IterateAvailableTechs;
    private static DelegateBridge __Hotfix_AddNewLearntTech;

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingGround()
    {
      // ISSUE: unable to decompile the method.
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(List<TrainingTech> AvailableTechs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingRoom GetRoom(int RoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetLearntTech(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingTech GetUnlearntTech(int TechId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TrainingTech> GetAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<TrainingTech> IterateAvailableTechs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewLearntTech(TrainingTech Tech)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
