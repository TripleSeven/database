﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GiftStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GiftStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionGiftStore m_giftStoreDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected OperationalActivityCompomentCommon m_operationActivity;
    protected BagComponentCommon m_bag;
    protected ResourceComponentCommon m_resource;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_TryGenerateEventGift;
    private static DelegateBridge __Hotfix_RemoveExpiredEventGifts;
    private static DelegateBridge __Hotfix_GetAllNotNoticedEventGifts;
    private static DelegateBridge __Hotfix_GetEventGiftExpiredTime;
    private static DelegateBridge __Hotfix_NoticeEventGift;
    private static DelegateBridge __Hotfix_AddEventGift;
    private static DelegateBridge __Hotfix_OnFlushBoughtNums;
    private static DelegateBridge __Hotfix_GetOfferedStoreItemsByConfig;
    private static DelegateBridge __Hotfix_GetOfferedStoreItems;
    private static DelegateBridge __Hotfix_IsOnSaleTime;
    private static DelegateBridge __Hotfix_HasBought;
    private static DelegateBridge __Hotfix_CanBuyGoods;
    private static DelegateBridge __Hotfix_CanAppleSubscribeGoods;
    private static DelegateBridge __Hotfix_AddFirstBuyGoodsRecord;
    private static DelegateBridge __Hotfix_GetAllOrderRewards;
    private static DelegateBridge __Hotfix_RemoveOrderReward;
    private static DelegateBridge __Hotfix_AddOrderReward;
    private static DelegateBridge __Hotfix_FindOrderReward;
    private static DelegateBridge __Hotfix_OnBuyGiftStoreGoods;
    private static DelegateBridge __Hotfix_add_BuyGiftStoreGoodsEvent;
    private static DelegateBridge __Hotfix_remove_BuyGiftStoreGoodsEvent;
    private static DelegateBridge __Hotfix_add_BuyStoreItemEvent;
    private static DelegateBridge __Hotfix_remove_BuyStoreItemEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void TryGenerateEventGift(
      EventGiftUnlockConditionType unlockType,
      int param1,
      int param2 = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpiredEventGifts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllNotNoticedEventGifts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetEventGiftExpiredTime(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NoticeEventGift(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddEventGift(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBoughtNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GiftStoreItem> GetOfferedStoreItemsByConfig(
      bool careShowInStore,
      GiftType giftType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GiftStoreItem> GetOfferedStoreItems(
      bool careShowInStore = true,
      GiftType giftType = GiftType.GiftType_Normal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsOnSaleTime(DateTime saleStartTime, DateTime saleEndTime, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBought(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAppleSubscribeGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddFirstBuyGoodsRecord(int goodsId, string goodsRegisterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OrderReward> GetAllOrderRewards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOrderReward(string orderId, OrderReward reward)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OrderReward FindOrderReward(string orderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnBuyGiftStoreGoods(ConfigDataGiftStoreItemInfo goodsInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> BuyGiftStoreGoodsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
