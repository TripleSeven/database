﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionAnnouncement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionAnnouncement : DataSection
  {
    private List<Announcement> m_announcements;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitAnnouncements;
    private static DelegateBridge __Hotfix_SetAnnouncements;
    private static DelegateBridge __Hotfix_SetClientAnnouncementCurrentVersion;
    private static DelegateBridge __Hotfix_SetServerAnnouncementCurrentVersion;
    private static DelegateBridge __Hotfix_RemoveAnnouncement;
    private static DelegateBridge __Hotfix_FindAnnouncementsByCondition;
    private static DelegateBridge __Hotfix_FindAnnouncementsByInstanceId;
    private static DelegateBridge __Hotfix_AddAnnouncement;
    private static DelegateBridge __Hotfix_get_ClientCurrentVersion;
    private static DelegateBridge __Hotfix_set_ClientCurrentVersion;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionAnnouncement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAnnouncements(List<Announcement> announcements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnnouncements(List<Announcement> announcements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClientAnnouncementCurrentVersion(uint currentVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetServerAnnouncementCurrentVersion(int serverCurrentVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAnnouncement(Announcement announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> FindAnnouncementsByCondition(
      Predicate<Announcement> isConditionMatched)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Announcement FindAnnouncementsByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnnouncement(Announcement announcement)
    {
      // ISSUE: unable to decompile the method.
    }

    public uint ClientCurrentVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
