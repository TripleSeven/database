﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.OperationalActivityCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class OperationalActivityCompomentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected AnikiGymComponentCommon m_anikiGym;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected MemoryCorridorCompomentCommon m_memoryCorridor;
    protected HeroTrainningComponentCommon m_heroTrainning;
    protected CooperateBattleCompomentCommon m_cooperateBattle;
    protected RiftComponentCommon m_rift;
    protected SelectCardComponentCommon m_selectCard;
    protected HeroDungeonComponentCommon m_heroDungeon;
    protected EternalShrineCompomentCommon m_eternalShrine;
    protected RaffleComponentCommon m_raffle;
    protected UnchartedScoreComponentCommon m_unchartedScore;
    protected FixedStoreComponentCommon m_fixedStore;
    protected RandomStoreComponentCommon m_randomStore;
    protected GiftStoreComponentCommon m_giftStore;
    protected RechargeStoreComponentCommon m_rechargeStore;
    protected CollectionComponentCommon m_collectionActivity;
    protected LevelComponentCommon m_level;
    protected DataSectionOperationalActivity m_operationalActivityDS;
    protected DataSectionAnnouncement m_announcementDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFlushLoginDaysEvent;
    private static DelegateBridge __Hotfix_OnFlushSuccessCallBack;
    private static DelegateBridge __Hotfix_OnPlayerLevelUpEvent;
    private static DelegateBridge __Hotfix_AddNewOperationalActivity;
    private static DelegateBridge __Hotfix_PreInitNewNewOperationalActivityInfo;
    private static DelegateBridge __Hotfix_InitNewNewOperationalActivityInfo;
    private static DelegateBridge __Hotfix_OnAddNewActivityCallBack;
    private static DelegateBridge __Hotfix_OnActivityInitCallBack;
    private static DelegateBridge __Hotfix_IsEffectOperationalActivity;
    private static DelegateBridge __Hotfix_IsAwardOperationActivity;
    private static DelegateBridge __Hotfix_AddActivityEventId;
    private static DelegateBridge __Hotfix_RemoveAllExpiredOperationalActivities;
    private static DelegateBridge __Hotfix_CanExchangeItemGroup;
    private static DelegateBridge __Hotfix_CanGainOperactionalActivityReward;
    private static DelegateBridge __Hotfix_GetAllOperationalActivities;
    private static DelegateBridge __Hotfix_GetAllValidOperationalActivities;
    private static DelegateBridge __Hotfix_GetAwardOperationActivityRewardItemGroupIdByIndex;
    private static DelegateBridge __Hotfix_EffectOperationActivityGenerateEffect;
    private static DelegateBridge __Hotfix_FindOperationalActivityById;
    private static DelegateBridge __Hotfix_FindOperationalActivitiesByType;
    private static DelegateBridge __Hotfix_FindOperationalActivityByActivityCardPoolId;
    private static DelegateBridge __Hotfix_FindOperationalActivityByRafflePoolId;
    private static DelegateBridge __Hotfix_FindOperationalActivityByUnchartedScoreId;
    private static DelegateBridge __Hotfix_FindShowActivityByUnchartedScoreId;
    private static DelegateBridge __Hotfix_FindOperationalActivityByCollectionActivityId;
    private static DelegateBridge __Hotfix_FindExsitOperationalActivityByCollectionActivityId;
    private static DelegateBridge __Hotfix_FindShowActivityByCollectionActivityId;
    private static DelegateBridge __Hotfix_FindGainRewardActivityByCollectionActivityId;
    private static DelegateBridge __Hotfix_ExistSpecificIdValidActivity;
    private static DelegateBridge __Hotfix_ExistSpecificTypeValidActivity;
    private static DelegateBridge __Hotfix_OnAddRechargeRMBEvent;
    private static DelegateBridge __Hotfix_OnConsumeCrystalEvent;
    private static DelegateBridge __Hotfix_GetAllAdvertisementFlowLayouts;
    private static DelegateBridge __Hotfix_OnBuyStoreItemCallBack;

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushLoginDaysEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnFlushSuccessCallBack(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayerLevelUpEvent(int palyerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreInitNewNewOperationalActivityInfo(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitNewNewOperationalActivityInfo(OperationalActivityBase operationalActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnAddNewActivityCallBack(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnActivityInitCallBack(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsEffectOperationalActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsAwardOperationActivity(OperationalActivityBase operationalActivity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddActivityEventId(EventOperationalActivity activity, int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllExpiredOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeItemGroup(
      LimitedTimeExchangeOperationActivity limitedTimeExchangeOperationalActivity,
      int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainOperactionalActivityReward(
      OperationalActivityBase operationalActivityBase,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> GetAllOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> GetAllValidOperationalActivities()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetAwardOperationActivityRewardItemGroupIdByIndex(
      OperationalActivityBase operationalActivity,
      int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EffectOperationActivityGenerateEffect(
      OperationalActivityBase operationalActivity,
      bool isPositive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityById(
      ulong operationalActivityInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<OperationalActivityBase> FindOperationalActivitiesByType(
      OperationalActivityType activityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByActivityCardPoolId(
      int activityCardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByRafflePoolId(
      int rafflePoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindShowActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindExsitOperationalActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindShowActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindGainRewardActivityByCollectionActivityId(
      int collectionActivityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistSpecificIdValidActivity(int activityId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistSpecificTypeValidActivity(OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddRechargeRMBEvent(int nums, DateTime rechargeTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConsumeCrystalEvent(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<AdvertisementFlowLayout> GetAllAdvertisementFlowLayouts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBuyStoreItemCallBack(int storeId, int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
