﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroCommentEntry
  {
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_Content;
    private static DelegateBridge __Hotfix_set_Content;
    private static DelegateBridge __Hotfix_get_CommenterUserId;
    private static DelegateBridge __Hotfix_set_CommenterUserId;
    private static DelegateBridge __Hotfix_get_CommenterName;
    private static DelegateBridge __Hotfix_set_CommenterName;
    private static DelegateBridge __Hotfix_get_CommenterLevel;
    private static DelegateBridge __Hotfix_set_CommenterLevel;
    private static DelegateBridge __Hotfix_get_PraiseNums;
    private static DelegateBridge __Hotfix_set_PraiseNums;
    private static DelegateBridge __Hotfix_get_CommentTime;
    private static DelegateBridge __Hotfix_set_CommentTime;
    private static DelegateBridge __Hotfix_HeroCommentEntryToPBHeroCommentEntry;
    private static DelegateBridge __Hotfix_PBHeroCommentEntryToHeroCommentEntry;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentEntry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroCommentEntry(HeroCommentEntry other)
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Content
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string CommenterUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string CommenterName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CommenterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PraiseNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long CommentTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroCommentEntry HeroCommentEntryToPBHeroCommentEntry(
      HeroCommentEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroCommentEntry PBHeroCommentEntryToHeroCommentEntry(
      ProHeroCommentEntry pbEntry)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
