﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CooperateBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CooperateBattle
  {
    public CooperateBattleCollections WhichBattleCollections;
    public List<CooperateBattleLevel> Levels;
    private IConfigDataLoader _ConfigDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ID;
    private static DelegateBridge __Hotfix_set_ID;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_get_Description;
    private static DelegateBridge __Hotfix_get_OpenDateTime;
    private static DelegateBridge __Hotfix_get_CloseDateTime;
    private static DelegateBridge __Hotfix_get_OpenDateTime2;
    private static DelegateBridge __Hotfix_get_CloseDateTime2;
    private static DelegateBridge __Hotfix_get_ShowWeekDays;
    private static DelegateBridge __Hotfix_get_ChallengedNums;
    private static DelegateBridge __Hotfix_set_ChallengedNums;
    private static DelegateBridge __Hotfix_get_IsAvailableForChallenge;
    private static DelegateBridge __Hotfix_get_IsAvailableForDisplay;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_ReloadConfigData;

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Description
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime OpenDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime CloseDateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime OpenDateTime2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime CloseDateTime2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<DayOfWeek> ShowWeekDays
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChallengedNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForDisplay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCooperateBattleInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
