﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class TeamRoom
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_set_RoomId;
    private static DelegateBridge __Hotfix_get_Leader;
    private static DelegateBridge __Hotfix_set_Leader;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_set_Players;
    private static DelegateBridge __Hotfix_get_LeaderKickOutTime;
    private static DelegateBridge __Hotfix_set_LeaderKickOutTime;
    private static DelegateBridge __Hotfix_get_Setting;
    private static DelegateBridge __Hotfix_set_Setting;
    private static DelegateBridge __Hotfix_IsLeader;
    private static DelegateBridge __Hotfix_IsLeaderBySessionId;
    private static DelegateBridge __Hotfix_JoinTeamRoom;
    private static DelegateBridge __Hotfix_QuitTeamRoom;
    private static DelegateBridge __Hotfix_FindTeamRoomPlayer;
    private static DelegateBridge __Hotfix_FindTeamRoomPlayerAtPosition;
    private static DelegateBridge __Hotfix_TeamRoomToPbTeamRoom;
    private static DelegateBridge __Hotfix_PbTeamRoomToTeamRoom;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TeamRoomPlayer Leader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<TeamRoomPlayer> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LeaderKickOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TeamRoomSetting Setting
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLeader(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLeaderBySessionId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamRoom(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void QuitTeamRoom(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer FindTeamRoomPlayer(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomPlayer FindTeamRoomPlayerAtPosition(int position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoom TeamRoomToPbTeamRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoom PbTeamRoomToTeamRoom(ProTeamRoom pbRoom)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
