﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleReport
  {
    public List<BattleCommand> Commands;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Head;
    private static DelegateBridge __Hotfix_set_Head;
    private static DelegateBridge __Hotfix_get_BattleType;
    private static DelegateBridge __Hotfix_set_BattleType;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_RandomSeed;
    private static DelegateBridge __Hotfix_set_RandomSeed;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleReportHead Head
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleType BattleType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
