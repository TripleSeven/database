﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaTopRankPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaTopRankPlayer
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_ArenaPoints;
    private static DelegateBridge __Hotfix_set_ArenaPoints;
    private static DelegateBridge __Hotfix_get_LevelId;
    private static DelegateBridge __Hotfix_set_LevelId;
    private static DelegateBridge __Hotfix_get_HeadIcon;
    private static DelegateBridge __Hotfix_set_HeadIcon;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_ArenaTopRankPlayerToPBArenaTopRankPlayer;
    private static DelegateBridge __Hotfix_PBArenaTopRankPlayerToArenaTopRankPlayer;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaTopRankPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ushort ArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte LevelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaTopRankPlayer ArenaTopRankPlayerToPBArenaTopRankPlayer(
      ArenaTopRankPlayer topRankPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaTopRankPlayer PBArenaTopRankPlayerToArenaTopRankPlayer(
      ProArenaTopRankPlayer pbTopRankPlayer)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
