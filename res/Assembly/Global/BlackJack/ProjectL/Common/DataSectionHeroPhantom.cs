﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHeroPhantom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionHeroPhantom : DataSection
  {
    public HeroPhantomCollections PhantomCollections;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_UpdateInstantiatedData;
    private static DelegateBridge __Hotfix_GetHeroPhantom;
    private static DelegateBridge __Hotfix_GetHeroPhantomLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHeroPhantom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(DSHeroPhantomNtf DS)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantom GetHeroPhantom(int HeroPhantomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel GetHeroPhantomLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
