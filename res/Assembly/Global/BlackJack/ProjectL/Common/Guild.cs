﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Guild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class Guild
  {
    public List<GuildLog> Logs;
    public GuildMassiveCombatGeneral MassiveCombat;
    public static int GuildMemberCountMax;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_Bulletin;
    private static DelegateBridge __Hotfix_set_Bulletin;
    private static DelegateBridge __Hotfix_get_HiringDeclaration;
    private static DelegateBridge __Hotfix_set_HiringDeclaration;
    private static DelegateBridge __Hotfix_get_AutoJoin;
    private static DelegateBridge __Hotfix_set_AutoJoin;
    private static DelegateBridge __Hotfix_get_JoinLevel;
    private static DelegateBridge __Hotfix_set_JoinLevel;
    private static DelegateBridge __Hotfix_get_TotalBattlePower;
    private static DelegateBridge __Hotfix_set_TotalBattlePower;
    private static DelegateBridge __Hotfix_get_TotalActivities;
    private static DelegateBridge __Hotfix_set_TotalActivities;
    private static DelegateBridge __Hotfix_get_LastWeekActivities;
    private static DelegateBridge __Hotfix_set_LastWeekActivities;
    private static DelegateBridge __Hotfix_get_CurrentWeekActivities;
    private static DelegateBridge __Hotfix_set_CurrentWeekActivities;
    private static DelegateBridge __Hotfix_get_Activities;
    private static DelegateBridge __Hotfix_set_Activities;
    private static DelegateBridge __Hotfix_get_GuildNextWeekFlushTime;
    private static DelegateBridge __Hotfix_set_GuildNextWeekFlushTime;
    private static DelegateBridge __Hotfix_get_Members;
    private static DelegateBridge __Hotfix_set_Members;
    private static DelegateBridge __Hotfix_get_IsMemberFull;
    private static DelegateBridge __Hotfix_WeeklyFlush;
    private static DelegateBridge __Hotfix_AddMember;
    private static DelegateBridge __Hotfix_FindMember;
    private static DelegateBridge __Hotfix_RemoveMember;
    private static DelegateBridge __Hotfix_IsEmpty;
    private static DelegateBridge __Hotfix_GetPresident;
    private static DelegateBridge __Hotfix_GetVicePresidentNums;
    private static DelegateBridge __Hotfix_GetMemberUserIds;
    private static DelegateBridge __Hotfix_GetAdminUserIds;
    private static DelegateBridge __Hotfix_ToPb;
    private static DelegateBridge __Hotfix_FromPb;

    [MethodImpl((MethodImplOptions) 32768)]
    public Guild()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Bulletin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string HiringDeclaration
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool AutoJoin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JoinLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TotalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastWeekActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentWeekActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Activities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime GuildNextWeekFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<GuildMemberCacheObject> Members
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsMemberFull
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool WeeklyFlush(DateTime nextWeekFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject AddMember(GuildMember member)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject FindMember(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveMember(GuildMemberCacheObject cache)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmpty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMemberCacheObject GetPresident()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetVicePresidentNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> GetMemberUserIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> GetAdminUserIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuild ToPb(Guild g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Guild FromPb(ProGuild pb)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
