﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PeakArenaComponentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    public DataSectionPeakArena m_peakArenaDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_CanBetPeakArena;
    private static DelegateBridge __Hotfix_BetPeakArena;
    private static DelegateBridge __Hotfix_SettlePeakArenaMatchupBetReward;
    private static DelegateBridge __Hotfix_IsTimeForBetted;
    private static DelegateBridge __Hotfix_IsTimeForBuyingJetton;
    private static DelegateBridge __Hotfix_IsRoundTimeForBet;
    private static DelegateBridge __Hotfix_GetCurrentSeasonBetInfo;
    private static DelegateBridge __Hotfix_GetCurrentSeasonConfigInfo;
    private static DelegateBridge __Hotfix_PeakArenaDanUpdate;
    private static DelegateBridge __Hotfix_PeakArenaBattleFinish;
    private static DelegateBridge __Hotfix_PeakArenaBattleStart;
    private static DelegateBridge __Hotfix_IsPeakArenaUnlocked;
    private static DelegateBridge __Hotfix_IsPeakArenaMatchmakingAvailable;
    private static DelegateBridge __Hotfix_OnFlushPeakArena;
    private static DelegateBridge __Hotfix_OnFlushPeakArenaBet;
    private static DelegateBridge __Hotfix_SaveReport;
    private static DelegateBridge __Hotfix_IsBattleReportFull;
    private static DelegateBridge __Hotfix_SaveStatistics;
    private static DelegateBridge __Hotfix_GetBattleReport;
    private static DelegateBridge __Hotfix_CheckAcquireWinsBonus;
    private static DelegateBridge __Hotfix_AcquireWinsBonus;
    private static DelegateBridge __Hotfix_GetConsecutiveWins;
    private static DelegateBridge __Hotfix_GetConsecutiveLosses;
    private static DelegateBridge __Hotfix_GetLadderCareerMatchStats;
    private static DelegateBridge __Hotfix_SetupBattleTeam;
    private static DelegateBridge __Hotfix_GetBattleTeam;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_GetBattleReportNameRecords;
    private static DelegateBridge __Hotfix_AddBattleReportNameRecord;
    private static DelegateBridge __Hotfix_ExistBattleReportNameRecord;
    private static DelegateBridge __Hotfix_GetBattleReportName;
    private static DelegateBridge __Hotfix_CanChangeBattleReportName;
    private static DelegateBridge __Hotfix_ChangeBattleReportName;
    private static DelegateBridge __Hotfix_SelectHeroSkillInPublicPools;
    private static DelegateBridge __Hotfix_SelectHiredHeroSoldier;
    private static DelegateBridge __Hotfix_IsBattleTeamSetupDone;
    private static DelegateBridge __Hotfix_IsInPreRankingState;
    private static DelegateBridge __Hotfix_IsLastPreRankingGameJustFinished;
    private static DelegateBridge __Hotfix_GetSeasonEndTime;
    private static DelegateBridge __Hotfix_GetRegularSeasonEndTime;
    private static DelegateBridge __Hotfix_GetDanConfigByScore_1;
    private static DelegateBridge __Hotfix_GetDanConfigByScore_0;
    private static DelegateBridge __Hotfix_GetPrevDanConfigByScore_1;
    private static DelegateBridge __Hotfix_GetPrevDanConfigByScore_0;
    private static DelegateBridge __Hotfix_GetDisplayDan_1;
    private static DelegateBridge __Hotfix_GetDisplayDan_0;
    private static DelegateBridge __Hotfix_GetNextDisplayDanConfig_1;
    private static DelegateBridge __Hotfix_GetNextDisplayDanConfig_0;
    private static DelegateBridge __Hotfix_GetMatchmakingDan;
    private static DelegateBridge __Hotfix_GetScoreThresholdAfterSingleMatch_1;
    private static DelegateBridge __Hotfix_GetScoreThresholdAfterSingleMatch_0;
    private static DelegateBridge __Hotfix_GetAvailableRegularSeasonRewards;
    private static DelegateBridge __Hotfix_CanGetRegularSeasonRewards;
    private static DelegateBridge __Hotfix_GetCurrentPlayOffRound;
    private static DelegateBridge __Hotfix_GetCurrentPlayOffMatchStartTime;
    private static DelegateBridge __Hotfix_GetLastSeason;
    private static DelegateBridge __Hotfix_GetNextSeason;
    private static DelegateBridge __Hotfix_GetSeason;
    private static DelegateBridge __Hotfix_IsRegularSeasonEnd;
    private static DelegateBridge __Hotfix_IsInPlayOffMatchPeriod;
    private static DelegateBridge __Hotfix_IsInPlayOffMatchDay;
    private static DelegateBridge __Hotfix_GetPlayOffRoundByStage;
    private static DelegateBridge __Hotfix_SetLiveRoomId;
    private static DelegateBridge __Hotfix_SetNoticeEnterPlayOffRace;
    private static DelegateBridge __Hotfix_IsOnLiveRoom;
    private static DelegateBridge __Hotfix_GetLiveRoomId;
    private static DelegateBridge __Hotfix_get_m_configDataLoader;
    private static DelegateBridge __Hotfix_set_m_configDataLoader;
    private static DelegateBridge __Hotfix_add_PeakArenaBattleStartMissionEvent;
    private static DelegateBridge __Hotfix_remove_PeakArenaBattleStartMissionEvent;
    private static DelegateBridge __Hotfix_add_PeakArenaBattleFinishMissionEvent;
    private static DelegateBridge __Hotfix_remove_PeakArenaBattleFinishMissionEvent;
    private static DelegateBridge __Hotfix_add_PeakArenaDanUpdateMissionEvent;
    private static DelegateBridge __Hotfix_remove_PeakArenaDanUpdateMissionEvent;
    private static DelegateBridge __Hotfix_get_BattleReportInited;
    private static DelegateBridge __Hotfix_set_BattleReportInited;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBetPeakArena(int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetPeakArena(int seasonId, int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SettlePeakArenaMatchupBetReward(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTimeForBetted(ConfigDataPeakArenaSeasonInfo seaonInfo, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTimeForBuyingJetton(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRoundTimeForBet(
      DateTime currentTime,
      ConfigDataPeakArenaSeasonInfo seaonInfo,
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBetInfo GetCurrentSeasonBetInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaSeasonInfo GetCurrentSeasonConfigInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaDanUpdate(int dan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaBattleFinish(RealTimePVPMode mode, bool win, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PeakArenaBattleStart(RealTimePVPMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsPeakArenaUnlocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int IsPeakArenaMatchmakingAvailable(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnFlushPeakArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFlushPeakArenaBet()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveReport(BattleReportHead report, string reportName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBattleReportFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveStatistics(int Type, bool Win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleReportHead> GetBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckAcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AcquireWinsBonus(int BonusId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetConsecutiveWins(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetConsecutiveLosses(RealTimePVPMode Mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaMatchStats GetLadderCareerMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SetupBattleTeam(PeakArenaBattleTeam BattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleTeam GetBattleTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PeakArenaBattleReportNameRecord> GetBattleReportNameRecords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleReportNameRecord(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistBattleReportNameRecord(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetBattleReportName(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangeBattleReportName(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ChangeBattleReportName(ulong battleReportId, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SelectHeroSkillInPublicPools(int heroId, List<int> skillIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SelectHiredHeroSoldier(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsBattleTeamSetupDone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInPreRankingState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLastPreRankingGameJustFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetRegularSeasonEndTime(int Season)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaDanInfo GetDanConfigByScore(
      int Score,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetDanConfigByScore(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaDanInfo GetPrevDanConfigByScore(
      int Score,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetPrevDanConfigByScore(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetDisplayDan(int Score, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDisplayDan(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaDanInfo GetNextDisplayDanConfig(
      int Score,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataPeakArenaDanInfo GetNextDisplayDanConfig(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMatchmakingDan(int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetScoreThresholdAfterSingleMatch(
      int CurrentScore,
      bool IsPreRankingState,
      IConfigDataLoader configDataLoader,
      out int InclusiveMinScore,
      out int InclusiveMaxScore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetScoreThresholdAfterSingleMatch(
      int CurrentScore,
      out int InclusiveMinScore,
      out int InclusiveMaxScore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAvailableRegularSeasonRewards(int CurrentSeason, int Score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetRegularSeasonRewards(int CurrentSeason, int Score, int RewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetCurrentPlayOffRound(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DateTime GetCurrentPlayOffMatchStartTime(
      DateTime dt,
      IConfigDataLoader configDataLoader,
      int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaSeasonInfo GetLastSeason(
      DateTime dt,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaSeasonInfo GetNextSeason(
      DateTime dt,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataPeakArenaSeasonInfo GetSeason(
      DateTime dt,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsRegularSeasonEnd(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInPlayOffMatchPeriod(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInPlayOffMatchDay(DateTime dt, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetPlayOffRoundByStage(string stage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLiveRoomId(ulong liveRoomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNoticeEnterPlayOffRace(bool notice)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOnLiveRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong GetLiveRoomId()
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPMode> PeakArenaBattleStartMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<RealTimePVPMode, bool> PeakArenaBattleFinishMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> PeakArenaDanUpdateMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool BattleReportInited
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
