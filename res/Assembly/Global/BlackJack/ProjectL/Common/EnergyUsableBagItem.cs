﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.EnergyUsableBagItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class EnergyUsableBagItem : UseableBagItem
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_HaveEffect;
    private static DelegateBridge __Hotfix_get_EnergyCount;
    private static DelegateBridge __Hotfix_set_EnergyCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public EnergyUsableBagItem(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int HaveEffect(IComponentOwner owner, params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    public int EnergyCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
