﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomEventStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum RandomEventStatus
  {
    Generate = 1,
    Completed = 2,
    Fail = 3,
    OutTime = 4,
  }
}
