﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionAncientCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionAncientCall : DataSection
  {
    public AncientCall AncientCallInfo;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_AddAncientCallAchievement;
    private static DelegateBridge __Hotfix_UpdateBossDamage;
    private static DelegateBridge __Hotfix_UpdateBossHistoryPeriodMaxRank;
    private static DelegateBridge __Hotfix_FindBossById;
    private static DelegateBridge __Hotfix_IsCurrentPeriodExist;
    private static DelegateBridge __Hotfix_GetBossCurrentPeriodMaxDamage;
    private static DelegateBridge __Hotfix_GetBossCurrentMaxDamage;
    private static DelegateBridge __Hotfix_GetSpecificBossHistoryMaxDamage;
    private static DelegateBridge __Hotfix_ClearPeriodInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionAncientCall()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAncientCallAchievement(List<int> newAchievementRelationIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBossDamage(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateBossHistoryPeriodMaxRank(int bossId, int finalRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss FindBossById(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentPeriodExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossCurrentPeriodMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossCurrentMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSpecificBossHistoryMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearPeriodInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
