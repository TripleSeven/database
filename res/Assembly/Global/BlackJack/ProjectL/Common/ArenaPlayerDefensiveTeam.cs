﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerDefensiveTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaPlayerDefensiveTeam
  {
    public List<ArenaPlayerDefensiveHero> Heroes;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_ArenaDefenderRuleId;
    private static DelegateBridge __Hotfix_set_ArenaDefenderRuleId;
    private static DelegateBridge __Hotfix_PBArenaDefensiveTeamToArenaDefensiveTeam;
    private static DelegateBridge __Hotfix_ArenaDefensiveTeamToPBArenaDefensiveTeam;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    public byte BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public byte ArenaDefenderRuleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerDefensiveTeam PBArenaDefensiveTeamToArenaDefensiveTeam(
      ProArenaPlayerDefensiveTeam pbDefensiveTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerDefensiveTeam ArenaDefensiveTeamToPBArenaDefensiveTeam(
      ArenaPlayerDefensiveTeam defensiveTeam)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
