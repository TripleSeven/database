﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaConstDefine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PeakArenaConstDefine
  {
    public const int NoRank = -2147483648;
    public const DayOfWeek StartDayOfWeek = DayOfWeek.Monday;
    public const int RankingRewardSingleOperationTimeThreshold = 2000;
    public const string PlayOffWinnerIdForfeited = "0";
    public const int TotalPlayOffRound = 8;
    public const int TotalPlayOffAttenders = 256;
    public const int MatchupInfoDataSendMaxCount = 20;
    public const int MatchupIndexEnd = -1;
    public const int MatchRoomIdUndefined = 0;
    public const string GMClearWinnerId = "";
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaConstDefine()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
