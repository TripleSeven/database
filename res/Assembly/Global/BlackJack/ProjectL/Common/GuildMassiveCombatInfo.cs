﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildMassiveCombatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GuildMassiveCombatInfo
  {
    public List<GuildMassiveCombatStronghold> Strongholds;
    public List<GuildMassiveCombatMemberInfo> Members;
    public DateTime CreateTime;
    public DateTime FinishTime;
    public DateTime RewardSendTime;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_Difficulty;
    private static DelegateBridge __Hotfix_set_Difficulty;
    private static DelegateBridge __Hotfix_IsRewardSent;
    private static DelegateBridge __Hotfix_GetStronghold;
    private static DelegateBridge __Hotfix_IsConquered;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Difficulty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRewardSent(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildMassiveCombatStronghold GetStronghold(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsConquered(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
