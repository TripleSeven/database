﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleUtility
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_CreateDefaultConfigBattleHero;
    private static DelegateBridge __Hotfix_CreateBattleActorSetupFromBattleHero;
    private static DelegateBridge __Hotfix_GetHeroFetterSkills;
    private static DelegateBridge __Hotfix_GetHeroHeartFetterSkills;
    private static DelegateBridge __Hotfix_CreatePveMyBattleActorSetup;
    private static DelegateBridge __Hotfix_CreateArenaBattleActorSetup;
    private static DelegateBridge __Hotfix_CreatePvpBattleActorSetup;
    private static DelegateBridge __Hotfix_CreateRealtimePvpBattleActorSetup;
    private static DelegateBridge __Hotfix_CreatePeakArenaBattleActorSetup;
    private static DelegateBridge __Hotfix_AppendPveMyNpcBattleActorSetups;
    private static DelegateBridge __Hotfix_AppendPveOpponentBattleActorSetups;
    private static DelegateBridge __Hotfix_GetRandomBattleArmyActors;
    private static DelegateBridge __Hotfix_GetRandomTalentSkillId;
    private static DelegateBridge __Hotfix_GetRandomTalentSkillInfos;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceSkillInfos;
    private static DelegateBridge __Hotfix_GetTrainingTechSoldierSkillInfos;
    private static DelegateBridge __Hotfix_GetTrainingTechSummonSkills;
    private static DelegateBridge __Hotfix_CreateBattlePlayer;
    private static DelegateBridge __Hotfix_CompareTrainingTechById;
    private static DelegateBridge __Hotfix_GetHeroCharImageSkinResourceInfo;
    private static DelegateBridge __Hotfix_GetHeroModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_GetSoldierModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_ModifyClimbTowerBattleTeamSetups;
    private static DelegateBridge __Hotfix_ModifyGuildMassiveCombatBattleTeamSetups;
    private static DelegateBridge __Hotfix_ModifyEternalShrineBattleTeamSetups;
    private static DelegateBridge __Hotfix_ModifyAncientCallBattleTeamsSetups;
    private static DelegateBridge __Hotfix_ModifyHeroAnthemBattleTeamSetups;
    private static DelegateBridge __Hotfix_GetUnchartedBlessingSkillId;
    private static DelegateBridge __Hotfix_ModifyUnchartedBlessingBattleTeamSetups;
    private static DelegateBridge __Hotfix_AppendExtraSkillToBattleActorSetup;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero CreateDefaultConfigBattleHero(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActorSetup CreateBattleActorSetupFromBattleHero(
      IConfigDataLoader configDataLoader,
      BattleHero hero,
      int level = 0,
      int behaviorId = -1,
      int groupId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> GetHeroFetterSkills(
      IConfigDataLoader configDataLoader,
      Dictionary<int, int> heroFetters)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> GetHeroHeartFetterSkills(
      IConfigDataLoader configDataLoader,
      int heroId,
      int heartFetterLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActorSetup CreatePveMyBattleActorSetup(
      IConfigDataLoader configDataLoader,
      ConfigDataBattleInfo battleInfo,
      int position,
      BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActorSetup CreateArenaBattleActorSetup(
      IConfigDataLoader configDataLoader,
      int team,
      ConfigDataArenaBattleInfo battleInfo,
      int position,
      BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActorSetup CreatePvpBattleActorSetup(
      IConfigDataLoader configDataLoader,
      int team,
      ConfigDataPVPBattleInfo battleInfo,
      int position,
      BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActorSetup CreateRealtimePvpBattleActorSetup(
      IConfigDataLoader configDataLoader,
      int team,
      ConfigDataRealTimePVPBattleInfo battleInfo,
      int position,
      BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActorSetup CreatePeakArenaBattleActorSetup(
      IConfigDataLoader configDataLoader,
      int team,
      ConfigDataPeakArenaBattleInfo battleInfo,
      int position,
      BattleHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int AppendPveMyNpcBattleActorSetups(
      IConfigDataLoader configDataLoader,
      ConfigDataBattleInfo battleInfo,
      int monsterLevel,
      List<BattleActorSetup> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int AppendPveOpponentBattleActorSetups(
      IConfigDataLoader configDataLoader,
      ConfigDataBattleInfo battleInfo,
      int monsterLevel,
      int randomSeed,
      List<BattleActorSetup> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRandomBattleArmyActors(
      IConfigDataLoader configDataLoader,
      List<int> randomArmies,
      RandomNumber randomNumber,
      out List<RandomArmyActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetRandomTalentSkillId(
      IConfigDataLoader configDataLoader,
      RandomNumber randomNumber,
      int randomTalentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> GetRandomTalentSkillInfos(
      IConfigDataLoader configDataLoader,
      int randomTalentId,
      List<int> randomTalentProbilities,
      RandomNumber randomNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> GetEquipmentResonanceSkillInfos(
      IConfigDataLoader configDataLoader,
      List<BattleHeroEquipment> equipments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> GetTrainingTechSoldierSkillInfos(
      IConfigDataLoader configDataLoader,
      List<TrainingTech> techs,
      ConfigDataSoldierInfo soldierInfo,
      out int soldierSkillLevelUp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ConfigDataSkillInfo> GetTrainingTechSummonSkills(
      IConfigDataLoader configDataLoader,
      List<TrainingTech> techs,
      ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattlePlayer CreateBattlePlayer(
      IConfigDataLoader configDataLoader,
      int playerLevel,
      List<TrainingTech> techs,
      ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareTrainingTechById(
      BattlePlayerTrainingTech t1,
      BattlePlayerTrainingTech t2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataCharImageSkinResourceInfo GetHeroCharImageSkinResourceInfo(
      IConfigDataLoader configDataLoader,
      int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataModelSkinResourceInfo GetHeroModelSkinResourceInfo(
      IConfigDataLoader configDataLoader,
      int heroSkinId,
      int activeJobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataModelSkinResourceInfo GetSoldierModelSkinResourceInfo(
      IConfigDataLoader configDataLoader,
      int soldierSkinId,
      int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ModifyClimbTowerBattleTeamSetups(
      IConfigDataLoader configDataLoader,
      int floorId,
      int bonusHeroGroupId,
      int battleRuleId,
      BattleTeamSetup mineTime,
      BattleTeamSetup opponentTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ModifyGuildMassiveCombatBattleTeamSetups(
      IConfigDataLoader configDataLoader,
      int strongholdId,
      List<int> preferredHeroTagIds,
      BattleTeamSetup mineTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ModifyEternalShrineBattleTeamSetups(
      IConfigDataLoader configDataLoader,
      int eternalShrineId,
      BattleTeamSetup mineTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ModifyAncientCallBattleTeamsSetups(
      IConfigDataLoader configDataLoader,
      int ancientCallBossId,
      BattleTeamSetup mineTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ModifyHeroAnthemBattleTeamSetups(
      IConfigDataLoader configDataLoader,
      int heroAnthemLevelId,
      BattleTeamSetup mineTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetUnchartedBlessingSkillId(
      IConfigDataLoader configDataLoader,
      BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void ModifyUnchartedBlessingBattleTeamSetups(
      IConfigDataLoader configDataLoader,
      BattleType battleType,
      int playerIdx,
      BattleTeamSetup mineTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void AppendExtraSkillToBattleActorSetup(
      ConfigDataSkillInfo skillInfo,
      BattleActorSetup a)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
