﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.EventGift
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class EventGift
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsEventGiftValid;
    private static DelegateBridge __Hotfix_get_GoodsId;
    private static DelegateBridge __Hotfix_set_GoodsId;
    private static DelegateBridge __Hotfix_get_ExpiredTime;
    private static DelegateBridge __Hotfix_set_ExpiredTime;
    private static DelegateBridge __Hotfix_get_Noticed;
    private static DelegateBridge __Hotfix_set_Noticed;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_FromPB;

    [MethodImpl((MethodImplOptions) 32768)]
    public EventGift()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventGiftValid(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public int GoodsId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime ExpiredTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Noticed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProEventGift ToPB(EventGift gift)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static EventGift FromPB(ProEventGift pbGift)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
