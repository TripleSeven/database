﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollisionInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "CollisionInfo")]
  [Serializable]
  public class CollisionInfo : IExtensible
  {
    private float _RadiusMax;
    private readonly List<SphereCollisionInfo> _sphereCollisions;
    private readonly List<CapsuleCollisionInfo> _capsuleCollisions;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_RadiusMax;
    private static DelegateBridge __Hotfix_set_RadiusMax;
    private static DelegateBridge __Hotfix_get_SphereCollisions;
    private static DelegateBridge __Hotfix_get_CapsuleCollisions;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollisionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "RadiusMax")]
    public float RadiusMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "sphereCollisions")]
    public List<SphereCollisionInfo> SphereCollisions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "capsuleCollisions")]
    public List<CapsuleCollisionInfo> CapsuleCollisions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
