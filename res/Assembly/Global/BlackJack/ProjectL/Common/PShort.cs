﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PShort
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct PShort
  {
    private short m_Value;
    private short m_Check;

    [MethodImpl((MethodImplOptions) 32768)]
    public PShort(short v)
    {
      // ISSUE: unable to decompile the method.
    }

    public static implicit operator PShort(short value)
    {
      return new PShort(value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static implicit operator short(PShort p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    private static short Encode(short v)
    {
      return (short) ((int) v ^ 21845);
    }

    private static short Decode(short v)
    {
      return (short) ((int) v ^ 21845);
    }

    private static short Check(short v)
    {
      return (short) ((int) v ^ 30583);
    }
  }
}
