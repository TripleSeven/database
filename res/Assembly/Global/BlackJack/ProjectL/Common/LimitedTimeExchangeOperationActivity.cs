﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.LimitedTimeExchangeOperationActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class LimitedTimeExchangeOperationActivity : OperationalActivityBase
  {
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_DeserializeFromPB;
    private static DelegateBridge __Hotfix_ToPBNtf;
    private static DelegateBridge __Hotfix_SerializeToPB;
    private static DelegateBridge __Hotfix_get_ExchangedItemGroups;
    private static DelegateBridge __Hotfix_set_ExchangedItemGroups;
    private static DelegateBridge __Hotfix_CanExchangeOperationalActivityItem;
    private static DelegateBridge __Hotfix_AddOperationalActivityItemExchangeTimes;

    [MethodImpl((MethodImplOptions) 32768)]
    public LimitedTimeExchangeOperationActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LimitedTimeExchangeOperationActivity(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeserializeFromPB(
      ProLimitedTimeExchangeOperationActivity pbOperationalActivity,
      ConfigDataOperationalActivityInfo config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLimitedTimeExchangeOperationActivity SerializeToPB()
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, int> ExchangedItemGroups
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExchangeOperationalActivityItem(int itemGroupIndex, int exchangeTimes = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddOperationalActivityItemExchangeTimes(int itemGroupIndex, int exchangeTimes = 1)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
