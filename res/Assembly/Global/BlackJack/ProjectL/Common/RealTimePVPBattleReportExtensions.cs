﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPBattleReportExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public static class RealTimePVPBattleReportExtensions
  {
    private static DelegateBridge __Hotfix_ToMemory_1;
    private static DelegateBridge __Hotfix_ToPro_1;
    private static DelegateBridge __Hotfix_GetHeroes;
    private static DelegateBridge __Hotfix_ToMemory_0;
    private static DelegateBridge __Hotfix_ToPro_0;
    private static DelegateBridge __Hotfix_ToMemory_2;
    private static DelegateBridge __Hotfix_ToPro_2;

    [MethodImpl((MethodImplOptions) 32768)]
    public static RealTimePVPBattleReportPlayerData ToMemory(
      this ProRealTimePVPBattleReportPlayerData Data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRealTimePVPBattleReportPlayerData ToPro(
      this RealTimePVPBattleReportPlayerData Data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<int> GetHeroes(this ProRealTimePVPBattleReportPlayerData Data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RealTimePVPBattleReport ToMemory(
      this ProRealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRealTimePVPBattleReport ToPro(
      this RealTimePVPBattleReport Report)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RealTimePVPMatchStats ToMemory(
      this ProRealTimePVPMatchStats ProStats)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRealTimePVPMatchStats ToPro(
      this RealTimePVPMatchStats Stats)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
