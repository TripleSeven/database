﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.MissionComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class MissionComponentCommon : IComponentBase
  {
    protected HashSet<ConfigDataMissionInfo> m_lockedMissionConfigsInLogic;
    protected HashSet<int> m_existMissions;
    public DataSectionMission m_missionDS;
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected SelectCardComponentCommon m_selectCard;
    protected BagComponentCommon m_bag;
    protected LevelComponentCommon m_level;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected AnikiGymComponentCommon m_anikiGym;
    protected BattleComponentCommon m_battle;
    protected ArenaComponentCommon m_arena;
    protected HeroDungeonComponentCommon m_heroDungeon;
    protected HeroTrainningComponentCommon m_heroTrainning;
    protected MemoryCorridorCompomentCommon m_memoryCorridor;
    protected TreasureMapComponentCommon m_treasureMap;
    protected HeroPhantomCompomentCommon m_heroPhantom;
    protected CooperateBattleCompomentCommon m_cooperateBattle;
    protected TrainingGroundCompomentCommon m_trainingGround;
    protected TeamComponentCommon m_team;
    protected NoviceComponentCommon m_novice;
    protected RefluxComponentCommon m_reflux;
    protected FriendComponentCommon m_friend;
    protected HeroAssistantsCompomentCommon m_heroAssistants;
    protected ResourceComponentCommon m_resource;
    protected RealTimePVPComponentCommon m_realTimePvp;
    protected GiftStoreComponentCommon m_giftStore;
    protected RechargeStoreComponentCommon m_rechargeStore;
    protected ClimbTowerComponentCommon m_climb;
    protected GuildComponentCommon m_guild;
    protected EternalShrineCompomentCommon m_eternalShrine;
    protected HeroAnthemComponentCommon m_heroAnthem;
    protected AncientCallComponentCommon m_ancientCall;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_RemoveProcessingMissionById;
    private static DelegateBridge __Hotfix_OnMonthCardVaildEveryDayFlush;
    private static DelegateBridge __Hotfix_OnMonthCardVaildCallBack;
    private static DelegateBridge __Hotfix_InitProcessingMission;
    private static DelegateBridge __Hotfix_GetProcessingMissionByMissionType;
    private static DelegateBridge __Hotfix_InitExistMissions;
    private static DelegateBridge __Hotfix_OnFlushEverydayMissionEvent;
    private static DelegateBridge __Hotfix_ResetEverydayMissions;
    private static DelegateBridge __Hotfix_InitMissionListByMissionPeriodType;
    private static DelegateBridge __Hotfix_IsRefluxActivityHasOpening;
    private static DelegateBridge __Hotfix_CanUnlockMissionInLogicFromInit;
    private static DelegateBridge __Hotfix_OnDirectActivatedMissionLocked;
    private static DelegateBridge __Hotfix_IsNoviceMission_1;
    private static DelegateBridge __Hotfix_IsRefluxMission_1;
    private static DelegateBridge __Hotfix_IsRefluxMission_0;
    private static DelegateBridge __Hotfix_IsNoviceMission_0;
    private static DelegateBridge __Hotfix_IsNoviceMissionActivated_0;
    private static DelegateBridge __Hotfix_IsNoviceMissionActivatedForRewarding;
    private static DelegateBridge __Hotfix_IsNoviceMissionActivated_1;
    private static DelegateBridge __Hotfix_IsRefluxMissionActivated_1;
    private static DelegateBridge __Hotfix_IsRefluxMissionActivated_0;
    private static DelegateBridge __Hotfix_IsRefluxMissionActivatedForRewarding;
    private static DelegateBridge __Hotfix_CanGetRewarding;
    private static DelegateBridge __Hotfix_CanUnlockMissionInLogic;
    private static DelegateBridge __Hotfix_CanUnlockMission;
    private static DelegateBridge __Hotfix_CaculateDirectlyActivationMissionStatus;
    private static DelegateBridge __Hotfix_AddMission;
    private static DelegateBridge __Hotfix_OnAddProcessingDirectelyActivitedMission;
    private static DelegateBridge __Hotfix_GetProcessingMissionByMissionPeriod;
    private static DelegateBridge __Hotfix_OnGetProcessingDirectActivationMissionFail;
    private static DelegateBridge __Hotfix_AddEverydayMission;
    private static DelegateBridge __Hotfix_AddOneOffMission;
    private static DelegateBridge __Hotfix_IsMissionExist;
    private static DelegateBridge __Hotfix_IsMissionFinished;
    private static DelegateBridge __Hotfix_InitMissionsFromConfig;
    private static DelegateBridge __Hotfix_FinishMission;
    private static DelegateBridge __Hotfix_GetAllEverydayMissionConfigByPreMissionId;
    private static DelegateBridge __Hotfix_GetAllEverydayMissionListByPlayerLevel;
    private static DelegateBridge __Hotfix_GetAllEverydayMissionListByScenario;
    private static DelegateBridge __Hotfix_GetAllProcessingRefluxMissionList;
    private static DelegateBridge __Hotfix_GetAllFinishedRefluxMissionList;
    private static DelegateBridge __Hotfix_GetAllProcessingNoviceMissionList;
    private static DelegateBridge __Hotfix_GetAllFinishedNoviceMissionList;
    private static DelegateBridge __Hotfix_UpdateMissionListByMissionComplete;
    private static DelegateBridge __Hotfix_UpdateMissionListByLevelUp;
    private static DelegateBridge __Hotfix_UpdateMissionListByScenario;
    private static DelegateBridge __Hotfix_UpdateMissionListByNewMissionConfigList;
    private static DelegateBridge __Hotfix_AddMissionProcess;
    private static DelegateBridge __Hotfix_GetMissionMaxProcess;
    private static DelegateBridge __Hotfix_IsMissionProcessFinished;
    private static DelegateBridge __Hotfix_CanGainMissionReward;
    private static DelegateBridge __Hotfix_RegisterMissionCallBack;
    private static DelegateBridge __Hotfix_OnGetAncientCallBossDamageEvaluate;
    private static DelegateBridge __Hotfix_IsCompleted;
    private static DelegateBridge __Hotfix_OnMissionFinishCallBack;
    private static DelegateBridge __Hotfix_OnKillGoblinCallBack;
    private static DelegateBridge __Hotfix_OnEquipmentStarLevelUp;
    private static DelegateBridge __Hotfix_OnJoinGuildCallBack;
    private static DelegateBridge __Hotfix_GuildMassiveCombatAttack;
    private static DelegateBridge __Hotfix_OnCompleteTowerFloorCallBack;
    private static DelegateBridge __Hotfix_OnSelectCardMissionCallBack;
    private static DelegateBridge __Hotfix_OnMissionSelectCardCallBack;
    private static DelegateBridge __Hotfix_SetStatisticalData;
    private static DelegateBridge __Hotfix_AddStatisticalData;
    private static DelegateBridge __Hotfix_GetMissionCompletedProcess;
    private static DelegateBridge __Hotfix_OnSummonHeroCallBack;
    private static DelegateBridge __Hotfix_OnConsumeEnergyCallBack;
    private static DelegateBridge __Hotfix_RollbackConsumeEnergy;
    private static DelegateBridge __Hotfix_IsFamGameFuncionType;
    private static DelegateBridge __Hotfix_OnCompleteEventCallBack;
    private static DelegateBridge __Hotfix_OnCompleteLevelCallBack;
    private static DelegateBridge __Hotfix_RiftLevelAttackDiffculityCallBack;
    private static DelegateBridge __Hotfix_AddBattleTypeLevelStatisticalData;
    private static DelegateBridge __Hotfix_OnArenaConsecutiveVictoryCallBack;
    private static DelegateBridge __Hotfix_OnArenaFightCallBack;
    private static DelegateBridge __Hotfix_OnCompleteScenaioCallBack;
    private static DelegateBridge __Hotfix_OnCompleteRiftLevelCallBack;
    private static DelegateBridge __Hotfix_OnGetRiftLevelFightAchievementCallBack;
    private static DelegateBridge __Hotfix_OnFinishFightAchievementCallBack;
    private static DelegateBridge __Hotfix_OnGetRiftLevelStarCallBack;
    private static DelegateBridge __Hotfix_OnConsumeCrystalCallBack;
    private static DelegateBridge __Hotfix_OnComsumeGoldCallBack;
    private static DelegateBridge __Hotfix_OnUseHeroExpItemCallBack;
    private static DelegateBridge __Hotfix_OnAllHeroAllJobLevelUpCallBack;
    private static DelegateBridge __Hotfix_OnAllHeroAddJobNums;
    private static DelegateBridge __Hotfix_OnRankJobHaveCallBack;
    private static DelegateBridge __Hotfix_OnAllHeroAddSkillNumsCallBack;
    private static DelegateBridge __Hotfix_OnAllHeroAddSoliderNumsCallBack;
    private static DelegateBridge __Hotfix_OnAllJobMasterHeroCallBack;
    private static DelegateBridge __Hotfix_OnHeroMasterJobCallBack;
    private static DelegateBridge __Hotfix_OnLevelUpHeroStarLevelCallBack;
    private static DelegateBridge __Hotfix_UpdateHeroRankLevelCallBack;
    private static DelegateBridge __Hotfix_OnAddHeroNumsCallBack;
    private static DelegateBridge __Hotfix_OnHasAboveLevelHeroNumsCallBack;
    private static DelegateBridge __Hotfix_OnLevelUpPlayerLevelCallBack;
    private static DelegateBridge __Hotfix_OnUseCrystalBuyEnergyCallBack;
    private static DelegateBridge __Hotfix_OnLoginGameCallBack;
    private static DelegateBridge __Hotfix_OnNewHeroJobTransferCallBack;
    private static DelegateBridge __Hotfix_OnSpecificHeroFight;
    private static DelegateBridge __Hotfix_OnSpecificHeroLevelUp;
    private static DelegateBridge __Hotfix_OnTrainingTechToLevelCallBack;
    private static DelegateBridge __Hotfix_OnEquipmentToLevelCallBack;
    private static DelegateBridge __Hotfix_OnFinishTeamBattleCallBack;
    private static DelegateBridge __Hotfix_OnFavorabilityToLevelCallBack;
    private static DelegateBridge __Hotfix_OnFetterToLevelCallBack;
    private static DelegateBridge __Hotfix_OnEnchantPropertiesSaveCallBack;
    private static DelegateBridge __Hotfix_OnEnchantEquipmentCallBack;
    private static DelegateBridge __Hotfix_OnTotalHeroJobLevelUpCallBack;
    private static DelegateBridge __Hotfix_OnInviteFriendCallBack;
    private static DelegateBridge __Hotfix_OnAssignHeroToTaskCallBack;
    private static DelegateBridge __Hotfix_OnBattlePracticeCallBack;
    private static DelegateBridge __Hotfix_OnRealTimeArenaBattleStartCallBack;
    private static DelegateBridge __Hotfix_OnRealTimeArenaBattleFinishCallBack;
    private static DelegateBridge __Hotfix_OnRealTimeArenaDanUpdateCallBack;
    private static DelegateBridge __Hotfix_OnBuyGiftStoreGoodsCallBack;
    private static DelegateBridge __Hotfix_OnBuyRechargeStoreGoodsCallBack;
    private static DelegateBridge __Hotfix_OnDoShareCallBack;
    private static DelegateBridge __Hotfix_OnClientDecidedAchievementCallBack;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveProcessingMissionById(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardVaildEveryDayFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMonthCardVaildCallBack(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitProcessingMission(List<Mission> processingMissionList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetProcessingMissionByMissionType(MissionType missionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitExistMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushEverydayMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void ResetEverydayMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitMissionListByMissionPeriodType(MissionPeriodType missionPeriod)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsRefluxActivityHasOpening()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual bool CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnDirectActivatedMissionLocked(ConfigDataMissionInfo missionConfig)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMission(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMission(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMissionActivated(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMissionActivatedForRewarding(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsNoviceMissionActivated(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMissionActivated(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMissionActivated(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsRefluxMissionActivatedForRewarding(ConfigDataMissionInfo mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanGetRewarding(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool CanUnlockMissionInLogic(ConfigDataMissionInfo missionConfigInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool CanUnlockMission(ConfigDataMissionInfo missionConfigInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected DirectlyActivatedMissionSatatus CaculateDirectlyActivationMissionStatus(
      ConfigDataMissionInfo missionConfigInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool AddMission(ConfigDataMissionInfo missionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnAddProcessingDirectelyActivitedMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Mission GetProcessingMissionByMissionPeriod(
      MissionPeriodType missionPeriodType,
      int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnGetProcessingDirectActivationMissionFail(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEverydayMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddOneOffMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsMissionExist(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMissionFinished(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitMissionsFromConfig(List<Mission> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void FinishMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataMissionInfo> GetAllEverydayMissionConfigByPreMissionId(
      int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataMissionInfo> GetAllEverydayMissionListByPlayerLevel(
      int playerLvl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<ConfigDataMissionInfo> GetAllEverydayMissionListByScenario(
      int scenario)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingRefluxMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedRefluxMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingNoviceMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedNoviceMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByMissionComplete(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByLevelUp(int playerLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByScenario(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMissionListByNewMissionConfigList(
      List<ConfigDataMissionInfo> newMissionConfigList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddMissionProcess(Mission mission, long process)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionMaxProcess(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMissionProcessFinished(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanGainMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RegisterMissionCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetAncientCallBossDamageEvaluate(int bossId, int evaluateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsCompleted(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionFinishCallBack(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnKillGoblinCallBack(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentStarLevelUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnJoinGuildCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GuildMassiveCombatAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteTowerFloorCallBack(int floor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSelectCardMissionCallBack(int cardPoolId, int selectCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnMissionSelectCardCallBack(int cardPoolId, int selectCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetStatisticalData(StatisticalDataType typeId, long nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void AddStatisticalData(StatisticalDataType typeId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetMissionCompletedProcess(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSummonHeroCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConsumeEnergyCallBack(GameFunctionType gameFuncTypeId, int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RollbackConsumeEnergy(GameFunctionType gameFuncTypeId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFamGameFuncionType(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteEventCallBack(bool isRandomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteLevelCallBack(BattleType levelType, int levelId, List<int> fightHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RiftLevelAttackDiffculityCallBack(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddBattleTypeLevelStatisticalData(BattleType levelType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaConsecutiveVictoryCallBack(int consecutiveVictoryNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnArenaFightCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteScenaioCallBack(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCompleteRiftLevelCallBack(int diffculty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRiftLevelFightAchievementCallBack(int achievementRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFinishFightAchievementCallBack(int achievementRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnGetRiftLevelStarCallBack(int starCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnConsumeCrystalCallBack(int crystalCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnComsumeGoldCallBack(int goldCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnUseHeroExpItemCallBack(int itemCount = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAllJobLevelUpCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAddJobNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRankJobHaveCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAddSkillNumsCallBack(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllHeroAddSoliderNumsCallBack(int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAllJobMasterHeroCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroMasterJobCallBack(int heroId, int jobRelateId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelUpHeroStarLevelCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroRankLevelCallBack(int heroRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddHeroNumsCallBack(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHasAboveLevelHeroNumsCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLevelUpPlayerLevelCallBack(int upLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUseCrystalBuyEnergyCallBack(int buyCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnLoginGameCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnNewHeroJobTransferCallBack(int heroId, int jobConnnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpecificHeroFight(int heroId, int fightNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSpecificHeroLevelUp(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTrainingTechToLevelCallBack(TrainingTech Tech)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEquipmentToLevelCallBack(EquipmentBagItem Equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFinishTeamBattleCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFavorabilityToLevelCallBack(Hero Hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFetterToLevelCallBack(Hero Hero, int FetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantPropertiesSaveCallBack(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnEnchantEquipmentCallBack(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTotalHeroJobLevelUpCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnInviteFriendCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAssignHeroToTaskCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBattlePracticeCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRealTimeArenaBattleStartCallBack(RealTimePVPMode battleMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRealTimeArenaBattleFinishCallBack(RealTimePVPMode battleMode, bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRealTimeArenaDanUpdateCallBack(int dan)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyGiftStoreGoodsCallBack(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyRechargeStoreGoodsCallBack(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnDoShareCallBack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnClientDecidedAchievementCallBack(int param1)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
