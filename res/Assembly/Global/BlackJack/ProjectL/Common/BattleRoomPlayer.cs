﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleRoomPlayer
  {
    public List<BattleHero> Heroes;
    public List<TrainingTech> Techs;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_UserId;
    private static DelegateBridge __Hotfix_set_UserId;
    private static DelegateBridge __Hotfix_get_SessionId;
    private static DelegateBridge __Hotfix_set_SessionId;
    private static DelegateBridge __Hotfix_get_ChannelId;
    private static DelegateBridge __Hotfix_set_ChannelId;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_HeadIcon;
    private static DelegateBridge __Hotfix_set_HeadIcon;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_RTPVPInfo;
    private static DelegateBridge __Hotfix_set_RTPVPInfo;
    private static DelegateBridge __Hotfix_get_IsOffline;
    private static DelegateBridge __Hotfix_set_IsOffline;
    private static DelegateBridge __Hotfix_get_PlayerBattleStatus;
    private static DelegateBridge __Hotfix_set_PlayerBattleStatus;
    private static DelegateBridge __Hotfix_get_Blessing;
    private static DelegateBridge __Hotfix_set_Blessing;
    private static DelegateBridge __Hotfix_get_PublicTimeSpan;
    private static DelegateBridge __Hotfix_set_PublicTimeSpan;
    private static DelegateBridge __Hotfix_FindBattleHero;
    private static DelegateBridge __Hotfix_BattleRoomPlayerToPbBattleRoomPlayer;
    private static DelegateBridge __Hotfix_PbBattleRoomPlayerToBattleRoomPlayer;
    private static DelegateBridge __Hotfix_ToPeakArenaBattleReportPlayerSummaryInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong SessionId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChannelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RealTimePVPInfo RTPVPInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsOffline
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PlayerBattleStatus PlayerBattleStatus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Blessing
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long PublicTimeSpan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero FindBattleHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleRoomPlayer BattleRoomPlayerToPbBattleRoomPlayer(
      BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleRoomPlayer PbBattleRoomPlayerToBattleRoomPlayer(
      ProBattleRoomPlayer pbPlayer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleReportPlayerSummaryInfo ToPeakArenaBattleReportPlayerSummaryInfo(
      BattleRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
