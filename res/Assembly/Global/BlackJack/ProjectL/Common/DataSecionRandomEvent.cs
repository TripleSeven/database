﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSecionRandomEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSecionRandomEvent : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_InitDefiniteLevelZone;
    private static DelegateBridge __Hotfix_InitRandomLevelZone;
    private static DelegateBridge __Hotfix_InitGenerateEventTime;
    private static DelegateBridge __Hotfix_InitGenerateRandomEventTotalCount;
    private static DelegateBridge __Hotfix_InitRandomEventTotalCount;
    private static DelegateBridge __Hotfix_SetDefiniteEventLevelZone;
    private static DelegateBridge __Hotfix_DefiniteGroupGenerateRandomEvent;
    private static DelegateBridge __Hotfix_RandomGroupGenerateRandomEvent;
    private static DelegateBridge __Hotfix_AddRandomEventLevelZone;
    private static DelegateBridge __Hotfix_SetRandomEventTotalCount;
    private static DelegateBridge __Hotfix_SetRandomEventTimes;
    private static DelegateBridge __Hotfix_SetNextRandomEventFlushTime;
    private static DelegateBridge __Hotfix_SetDefiniteEventMaxCount;
    private static DelegateBridge __Hotfix_AddRandomEventCount;
    private static DelegateBridge __Hotfix_GetDefiniteGroupRandomEventGroups;
    private static DelegateBridge __Hotfix_AddDefiniteGroupRandomEventGroup;
    private static DelegateBridge __Hotfix_GetRandomGroupLevelZoneEventGroups;
    private static DelegateBridge __Hotfix_ClearRandomInfo;
    private static DelegateBridge __Hotfix_RemoveRandomEventTime;
    private static DelegateBridge __Hotfix_IsCompletedRandomEventGenerate;
    private static DelegateBridge __Hotfix_get_RandomEventTotalCount;
    private static DelegateBridge __Hotfix_set_RandomEventTotalCount;
    private static DelegateBridge __Hotfix_get_GenerateRandomEventTotalCount;
    private static DelegateBridge __Hotfix_set_GenerateRandomEventTotalCount;
    private static DelegateBridge __Hotfix_get_DefiniteEventMaxCount;
    private static DelegateBridge __Hotfix_set_DefiniteEventMaxCount;
    private static DelegateBridge __Hotfix_get_NextRandomEventFlushTime;
    private static DelegateBridge __Hotfix_set_NextRandomEventFlushTime;
    private static DelegateBridge __Hotfix_get_DefiniteLevelZone;
    private static DelegateBridge __Hotfix_set_DefiniteLevelZone;
    private static DelegateBridge __Hotfix_get_RandomLevelZone;
    private static DelegateBridge __Hotfix_set_RandomLevelZone;
    private static DelegateBridge __Hotfix_get_RandomEventTimeList;
    private static DelegateBridge __Hotfix_set_RandomEventTimeList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSecionRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitDefiniteLevelZone(RandomEventLevelZone zone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRandomLevelZone(List<RandomEventLevelZone> zones)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGenerateEventTime(List<int> timeList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGenerateRandomEventTotalCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRandomEventTotalCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefiniteEventLevelZone(RandomEventLevelZone zone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DefiniteGroupGenerateRandomEvent(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RandomGroupGenerateRandomEvent(int levelZoneId, int groupId, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEventLevelZone(int index, RandomEventLevelZone zone)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEventTotalCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEventTimes(List<int> timeList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNextRandomEventFlushTime(DateTime setTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefiniteEventMaxCount(int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEventCount(int count = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> GetDefiniteGroupRandomEventGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDefiniteGroupRandomEventGroup(RandomEventGroup newGroup)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, RandomEventGroup> GetRandomGroupLevelZoneEventGroups(
      int levelZoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRandomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRandomEventTime(int time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCompletedRandomEventGenerate()
    {
      // ISSUE: unable to decompile the method.
    }

    public int RandomEventTotalCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GenerateRandomEventTotalCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DefiniteEventMaxCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextRandomEventFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomEventLevelZone DefiniteLevelZone
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, RandomEventLevelZone> RandomLevelZone
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RandomEventTimeList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
