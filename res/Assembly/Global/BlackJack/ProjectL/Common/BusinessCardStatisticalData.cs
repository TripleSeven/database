﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BusinessCardStatisticalData
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_MostSkilledHeroId;
    private static DelegateBridge __Hotfix_set_MostSkilledHeroId;
    private static DelegateBridge __Hotfix_get_HeroTotalPower;
    private static DelegateBridge __Hotfix_set_HeroTotalPower;
    private static DelegateBridge __Hotfix_get_AchievementMissionNums;
    private static DelegateBridge __Hotfix_set_AchievementMissionNums;
    private static DelegateBridge __Hotfix_get_MasterJobNums;
    private static DelegateBridge __Hotfix_set_MasterJobNums;
    private static DelegateBridge __Hotfix_get_RiftAchievementNums;
    private static DelegateBridge __Hotfix_set_RiftAchievementNums;
    private static DelegateBridge __Hotfix_get_ChooseLevelAchievementNums;
    private static DelegateBridge __Hotfix_set_ChooseLevelAchievementNums;
    private static DelegateBridge __Hotfix_ToProtocol;
    private static DelegateBridge __Hotfix_FromProtocol;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardStatisticalData()
    {
      // ISSUE: unable to decompile the method.
    }

    public int MostSkilledHeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroTotalPower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AchievementMissionNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MasterJobNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RiftAchievementNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChooseLevelAchievementNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardStatisticalData ToProtocol(
      BusinessCardStatisticalData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardStatisticalData FromProtocol(
      ProBusinessCardStatisticalData pbData)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
