﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardHeroSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BusinessCardHeroSet
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_set_HeroId;
    private static DelegateBridge __Hotfix_get_Direction;
    private static DelegateBridge __Hotfix_set_Direction;
    private static DelegateBridge __Hotfix_get_Action;
    private static DelegateBridge __Hotfix_set_Action;
    private static DelegateBridge __Hotfix_get_PositionIndex;
    private static DelegateBridge __Hotfix_set_PositionIndex;
    private static DelegateBridge __Hotfix_ToProtocol;
    private static DelegateBridge __Hotfix_ToProtocols;
    private static DelegateBridge __Hotfix_FromProtocol;
    private static DelegateBridge __Hotfix_FromProtocols;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardHeroSet()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroDirectionType Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroActionType Action
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PositionIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardHeroSet ToProtocol(
      BusinessCardHeroSet heroSet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProBusinessCardHeroSet> ToProtocols(
      List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardHeroSet FromProtocol(
      ProBusinessCardHeroSet pbHeroSet)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<BusinessCardHeroSet> FromProtocols(
      List<ProBusinessCardHeroSet> pbHeroSets)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
