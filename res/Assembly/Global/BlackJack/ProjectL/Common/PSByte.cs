﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PSByte
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct PSByte
  {
    private sbyte m_Value;
    private sbyte m_Check;

    [MethodImpl((MethodImplOptions) 32768)]
    public PSByte(sbyte v)
    {
      // ISSUE: unable to decompile the method.
    }

    public static implicit operator PSByte(sbyte value)
    {
      return new PSByte(value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static implicit operator sbyte(PSByte p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    private static sbyte Encode(sbyte v)
    {
      return (sbyte) ((int) v ^ 85);
    }

    private static sbyte Decode(sbyte v)
    {
      return (sbyte) ((int) v ^ 85);
    }

    private static sbyte Check(sbyte v)
    {
      return (sbyte) ((int) v ^ 119);
    }
  }
}
