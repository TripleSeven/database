﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BPStage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BPStage
  {
    public List<BPStageCommand> ExecutedCommands;
    public List<BPStageHeroSetupInfo> m_setupInfos;
    private List<int> m_bannedActorIds;
    private List<int> m_pickedActorIds;
    public IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_FromPB;
    private static DelegateBridge __Hotfix_SetupBPStageHeroes;
    private static DelegateBridge __Hotfix_GetPickedHero;
    private static DelegateBridge __Hotfix_GetBannedHero;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_ExecuteCommand;
    private static DelegateBridge __Hotfix_GenerateFinishedCommand;
    private static DelegateBridge __Hotfix_IsBannedOrPicked_0;
    private static DelegateBridge __Hotfix_IsBannedOrPicked_1;
    private static DelegateBridge __Hotfix_IsBanned_0;
    private static DelegateBridge __Hotfix_IsBanned_1;
    private static DelegateBridge __Hotfix_IsPicked_0;
    private static DelegateBridge __Hotfix_IsPicked_1;
    private static DelegateBridge __Hotfix_GetSetupInfos;
    private static DelegateBridge __Hotfix_GetSetupInfosByPred;
    private static DelegateBridge __Hotfix_GetSetupInfoByActorId;
    private static DelegateBridge __Hotfix_GetBPRule;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_Pick;
    private static DelegateBridge __Hotfix_GetPickedActorIds;
    private static DelegateBridge __Hotfix_GetBannedActorIds;
    private static DelegateBridge __Hotfix_get_m_randomSeed;
    private static DelegateBridge __Hotfix_set_m_randomSeed;
    private static DelegateBridge __Hotfix_get_Random;
    private static DelegateBridge __Hotfix_set_Random;
    private static DelegateBridge __Hotfix_get_m_bpRule;
    private static DelegateBridge __Hotfix_set_m_bpRule;

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(IBPStageRule rule, IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBPStage ToPB(BPStage stage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BPStage FromPB(ProBPStage stage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetupBPStageHeroes(int playerIndex, List<BPStageHero> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetPickedHero(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBannedHero(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(long deltaTicks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ExecuteCommand(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageCommand GenerateFinishedCommand(
      int playerIndex,
      BPStageCommandType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedOrPicked(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedOrPicked(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPicked(List<int> actorIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPicked(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BPStageHeroSetupInfo> GetSetupInfos(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BPStageHeroSetupInfo> GetSetupInfosByPred(
      Predicate<BPStageHeroSetupInfo> match)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageHeroSetupInfo GetSetupInfoByActorId(int actorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetBPRule<T>() where T : class, IBPStageRule
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Ban(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Pick(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetPickedActorIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetBannedActorIds()
    {
      // ISSUE: unable to decompile the method.
    }

    public int m_randomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber Random
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private IBPStageRule m_bpRule
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
