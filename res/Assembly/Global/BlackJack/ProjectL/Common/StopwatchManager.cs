﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.StopwatchManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class StopwatchManager
  {
    private static Dictionary<int, Stopwatch> mapStopwatch;
    private static bool isEnable;
    private static List<StopwatchManager.Result> _results;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Stop;
    private static DelegateBridge __Hotfix_SortByTotalTime;
    private static DelegateBridge __Hotfix_LogAllMessage;
    private static DelegateBridge __Hotfix_Enable;
    private static DelegateBridge __Hotfix_WriteLog;

    [MethodImpl((MethodImplOptions) 32768)]
    public StopwatchManager()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Start(int watchID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Stop(int watchID, string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int SortByTotalTime(
      StopwatchManager.StatisticResult left,
      StopwatchManager.StatisticResult right)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogAllMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Enable(bool bEnable = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void WriteLog(
      Action<string> logFuc,
      List<StopwatchManager.Result> results,
      List<StopwatchManager.StatisticResult> statisticResults)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static StopwatchManager()
    {
      // ISSUE: unable to decompile the method.
    }

    public struct StatisticResult
    {
      public string _msg;
      public double _averageTime;
      public int _count;
      public double _totalTime;

      [MethodImpl((MethodImplOptions) 32768)]
      public StatisticResult(string msg, double averageTime, int count, double totalTime)
      {
        // ISSUE: unable to decompile the method.
      }

      public string log
      {
        [MethodImpl((MethodImplOptions) 32768)] get
        {
          // ISSUE: unable to decompile the method.
        }
      }
    }

    public struct Result
    {
      public int _watchID;
      public string _msg;
      public double _time;

      public Result(int watchID, string msg, double time)
      {
        this._watchID = watchID;
        this._msg = msg;
        this._time = time;
      }

      public string key
      {
        get
        {
          return string.Format("[RuningTime - {0:D2}] {1} ", (object) this._watchID, (object) this._msg);
        }
      }

      public string log
      {
        get
        {
          return string.Format("[RuningTime - {0:D2}] {1} {2} ", (object) this._watchID, (object) this._msg, (object) this._time);
        }
      }
    }
  }
}
