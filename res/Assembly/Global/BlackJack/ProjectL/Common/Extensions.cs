﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Extensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public static class Extensions
  {
    private static DelegateBridge __Hotfix_ToPro_3;
    private static DelegateBridge __Hotfix_ToMemory_3;
    private static DelegateBridge __Hotfix_ToPro_2;
    private static DelegateBridge __Hotfix_ToMemory_2;
    private static DelegateBridge __Hotfix_ToPro_1;
    private static DelegateBridge __Hotfix_ToMemory_1;
    private static DelegateBridge __Hotfix_ToPro_0;
    private static DelegateBridge __Hotfix_ToMemory_0;
    private static DelegateBridge __Hotfix_ToPro_4;
    private static DelegateBridge __Hotfix_ToMemory_4;

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildMassiveCombatStrongholdInfo ToPro(
      this GuildMassiveCombatStronghold Stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildMassiveCombatStronghold ToMemory(
      this ProGuildMassiveCombatStrongholdInfo Stronghold)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildMassiveCombatMemberInfo ToPro(
      this GuildMassiveCombatMemberInfo MemberInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildMassiveCombatMemberInfo ToMemory(
      this ProGuildMassiveCombatMemberInfo MemberInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildMassiveCombatInfo ToPro(
      this GuildMassiveCombatInfo CombatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildMassiveCombatInfo ToMemory(
      this ProGuildMassiveCombatInfo CombatInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildMassiveCombatGeneralInfo ToPro(
      this GuildMassiveCombatGeneral General)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildMassiveCombatGeneral ToMemory(
      this ProGuildMassiveCombatGeneralInfo General)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildPlayerMassiveCombatInfo ToPro(
      this GuildPlayerMassiveCombatInfo Info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildPlayerMassiveCombatInfo ToMemory(
      this ProGuildPlayerMassiveCombatInfo Info)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
