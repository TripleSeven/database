﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroPhantomLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroPhantomLevel
  {
    public DateTime FirstClear;
    public List<int> AchievementIdsFinished;
    public HeroPhantom WhichPhantom;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ID;
    private static DelegateBridge __Hotfix_set_ID;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_get_PreLevel;
    private static DelegateBridge __Hotfix_get_EnergySuccess;
    private static DelegateBridge __Hotfix_get_EnergyFail;
    private static DelegateBridge __Hotfix_get_MonsterLevel;
    private static DelegateBridge __Hotfix_get_BattleInfo;
    private static DelegateBridge __Hotfix_get_UserExp;
    private static DelegateBridge __Hotfix_get_HeroExp;
    private static DelegateBridge __Hotfix_get_GoldBonus;
    private static DelegateBridge __Hotfix_get_RandomDropID;
    private static DelegateBridge __Hotfix_get_FirstClearDropItems;
    private static DelegateBridge __Hotfix_get_Achievements;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_get_Cleared;
    private static DelegateBridge __Hotfix_get_IsAvailableForChallenge;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PreLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int EnergySuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int EnergyFail
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MonsterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataBattleInfo BattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int UserExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GoldBonus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RandomDropID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<Goods> FirstClearDropItems
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleLevelAchievement[] Achievements
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroPhantomLevelInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Cleared
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
