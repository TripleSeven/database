﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FriendComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class FriendComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected DataSectionFriend m_friendDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFlushFriend;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_SetBusinessCardInfo;
    private static DelegateBridge __Hotfix_SetLikes;
    private static DelegateBridge __Hotfix_CanSendLikes;
    private static DelegateBridge __Hotfix_AddLikedUser;
    private static DelegateBridge __Hotfix_SendFriendshipPointsCheck;
    private static DelegateBridge __Hotfix_FilterInvalidSendFriendPointsTargets;
    private static DelegateBridge __Hotfix_SendFriendshipPoints;
    private static DelegateBridge __Hotfix_ReceiveFriendshipPointsFromFriend;
    private static DelegateBridge __Hotfix_ClaimFriendshipPointsFromFriendCheck;
    private static DelegateBridge __Hotfix_ClaimFriendshipPointsFromFriend;
    private static DelegateBridge __Hotfix_ClaimFriendShipPoints;
    private static DelegateBridge __Hotfix_GetFriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_AddFriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_CanAddHeroicMomentBattleReport;
    private static DelegateBridge __Hotfix_AddHeroicMomentBattleReport;
    private static DelegateBridge __Hotfix_RemoveHeroicMomentBattleReport;
    private static DelegateBridge __Hotfix_GetHeroicMomentBattleReports;
    private static DelegateBridge __Hotfix_CanUpdateBusinessCardDesc;
    private static DelegateBridge __Hotfix_CanUpdateBusinessCardHeroSets;
    private static DelegateBridge __Hotfix_IsMySelf;
    private static DelegateBridge __Hotfix_HasSendLikesToday;
    private static DelegateBridge __Hotfix_HasSentFriendShipPoints;
    private static DelegateBridge __Hotfix_HasReceivedFriendShipPoints;
    private static DelegateBridge __Hotfix_SetBusinessCardDesc;
    private static DelegateBridge __Hotfix_SetBusinessCardHeroSet;
    private static DelegateBridge __Hotfix_SetRandomHeroAction;
    private static DelegateBridge __Hotfix_RemoveFriendshipPointsReceivedUser;
    private static DelegateBridge __Hotfix_GetFriendshipPointsReceivedUsers;
    private static DelegateBridge __Hotfix_OnInviteFriendSuccess;
    private static DelegateBridge __Hotfix_add_InviteFriendMissionEvent;
    private static DelegateBridge __Hotfix_remove_InviteFriendMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardInfo(BusinessCardInfoSet setInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLikes(int likes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendLikes(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLikedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SendFriendshipPointsCheck(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> FilterInvalidSendFriendPointsTargets(List<string> targetUserIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SendFriendshipPoints(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ReceiveFriendshipPointsFromFriend(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ClaimFriendshipPointsFromFriendCheck(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int ClaimFriendshipPointsFromFriend(string targetUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClaimFriendShipPoints(int point)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFriendshipPointsFromFightWithFriendsToday()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddFriendshipPointsFromFightWithFriendsToday(int pointsToAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAddHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetHeroicMomentBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUpdateBusinessCardDesc(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUpdateBusinessCardHeroSets(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsMySelf(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasSendLikesToday(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasSentFriendShipPoints(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasReceivedFriendShipPoints(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardDesc(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardHeroSet(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomHeroAction(bool actionRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveFriendshipPointsReceivedUser(string UserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<string> GetFriendshipPointsReceivedUsers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnInviteFriendSuccess()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action InviteFriendMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
