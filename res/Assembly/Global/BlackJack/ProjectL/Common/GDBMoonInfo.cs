﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBMoonInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "GDBMoonInfo")]
  [Serializable]
  public class GDBMoonInfo : IExtensible
  {
    private int _Id;
    private string _name;
    private double _locationX;
    private double _locationZ;
    private uint _radius;
    private ulong _orbitRadius;
    private double _orbitalPeriod;
    private float _eccentricity;
    private double _mass;
    private float _gravity;
    private float _escapeVelocity;
    private uint _temperature;
    private uint _artResourcesId;
    private bool _isNeedLocalization;
    private string _localizationKey;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_LocationX;
    private static DelegateBridge __Hotfix_set_LocationX;
    private static DelegateBridge __Hotfix_get_LocationZ;
    private static DelegateBridge __Hotfix_set_LocationZ;
    private static DelegateBridge __Hotfix_get_Radius;
    private static DelegateBridge __Hotfix_set_Radius;
    private static DelegateBridge __Hotfix_get_OrbitRadius;
    private static DelegateBridge __Hotfix_set_OrbitRadius;
    private static DelegateBridge __Hotfix_get_OrbitalPeriod;
    private static DelegateBridge __Hotfix_set_OrbitalPeriod;
    private static DelegateBridge __Hotfix_get_Eccentricity;
    private static DelegateBridge __Hotfix_set_Eccentricity;
    private static DelegateBridge __Hotfix_get_Mass;
    private static DelegateBridge __Hotfix_set_Mass;
    private static DelegateBridge __Hotfix_get_Gravity;
    private static DelegateBridge __Hotfix_set_Gravity;
    private static DelegateBridge __Hotfix_get_EscapeVelocity;
    private static DelegateBridge __Hotfix_set_EscapeVelocity;
    private static DelegateBridge __Hotfix_get_Temperature;
    private static DelegateBridge __Hotfix_set_Temperature;
    private static DelegateBridge __Hotfix_get_ArtResourcesId;
    private static DelegateBridge __Hotfix_set_ArtResourcesId;
    private static DelegateBridge __Hotfix_get_IsNeedLocalization;
    private static DelegateBridge __Hotfix_set_IsNeedLocalization;
    private static DelegateBridge __Hotfix_get_LocalizationKey;
    private static DelegateBridge __Hotfix_set_LocalizationKey;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBMoonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Id")]
    public int Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "name")]
    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "locationX")]
    public double LocationX
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "locationZ")]
    public double LocationZ
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "radius")]
    public uint Radius
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "orbitRadius")]
    public ulong OrbitRadius
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "orbitalPeriod")]
    public double OrbitalPeriod
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "eccentricity")]
    public float Eccentricity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "mass")]
    public double Mass
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "gravity")]
    public float Gravity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.FixedSize, IsRequired = true, Name = "escapeVelocity")]
    public float EscapeVelocity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "temperature")]
    public uint Temperature
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "artResourcesId")]
    public uint ArtResourcesId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "isNeedLocalization")]
    public bool IsNeedLocalization
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = false, Name = "localizationKey")]
    public string LocalizationKey
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
