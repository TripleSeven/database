﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionComment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionComment : DataSection
  {
    public DateTime m_bannedTime;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_InitPlayerHeroCommentEntry;
    private static DelegateBridge __Hotfix_FindPlayerHeroCommentEntry;
    private static DelegateBridge __Hotfix_PraiseHeroComment;
    private static DelegateBridge __Hotfix_CommentHero;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_Unban;
    private static DelegateBridge __Hotfix_get_m_playerHeroCommentEntries;
    private static DelegateBridge __Hotfix_set_m_playerHeroCommentEntries;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionComment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayerHeroCommentEntry(PlayerHeroCommentEntry playerCommentEntry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry FindPlayerHeroCommentEntry(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PraiseHeroComment(int heroId, ulong commentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CommentHero(int heroId, HeroCommentEntry commentEntry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime bannedTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<PlayerHeroCommentEntry> m_playerHeroCommentEntries
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
