﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroPhantomCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroPhantomCompomentCommon : IComponentBase
  {
    private IConfigDataLoader _configDataLoader;
    protected DataSectionHeroPhantom m_heroPhantomDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected RiftComponentCommon m_rift;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_CheckHeroPhantomAvailable;
    private static DelegateBridge __Hotfix_CheckHeroPhantomLevelAvailable;
    private static DelegateBridge __Hotfix_CheckPlayerOutOfBattle;
    private static DelegateBridge __Hotfix_CheckEnergy;
    private static DelegateBridge __Hotfix_CheckBag;
    private static DelegateBridge __Hotfix_CheckChallengeHeroPhantomLevel;
    private static DelegateBridge __Hotfix_ChallengeHeroPhantomLevel;
    private static DelegateBridge __Hotfix_SetCommonSuccessHeroPhantomLevel;
    private static DelegateBridge __Hotfix_FinishedHeroPhantomLevel;
    private static DelegateBridge __Hotfix_GetHeroPhantom;
    private static DelegateBridge __Hotfix_GetHeroPhantomLevel;
    private static DelegateBridge __Hotfix_IsCompleteHeroPhantomLevelAchievement;
    private static DelegateBridge __Hotfix_get_m_configDataLoader;
    private static DelegateBridge __Hotfix_set_m_configDataLoader;
    private static DelegateBridge __Hotfix_GetAllFinishedLevels;
    private static DelegateBridge __Hotfix_add_CompleteHeroPhantomMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteHeroPhantomMissionEvent;
    private static DelegateBridge __Hotfix_add_GetBattleAchievementMissionEvent;
    private static DelegateBridge __Hotfix_remove_GetBattleAchievementMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckHeroPhantomAvailable(int HeroPhantomId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckHeroPhantomLevelAvailable(int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckPlayerOutOfBattle(ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckEnergy(int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckBag(int LevelId, ref int Err)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckChallengeHeroPhantomLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ChallengeHeroPhantomLevel(int LevelId, bool NoCheck = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessHeroPhantomLevel(
      HeroPhantomLevel Level,
      List<int> Heroes,
      List<int> BattleTreasures,
      List<int> newAchievementIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void FinishedHeroPhantomLevel(HeroPhantomLevel Level, List<int> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantom GetHeroPhantom(int HeroPhantomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel GetHeroPhantomLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCompleteHeroPhantomLevelAchievement(int achievementRelatedInfoID)
    {
      // ISSUE: unable to decompile the method.
    }

    protected IConfigDataLoader m_configDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllFinishedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteHeroPhantomMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> GetBattleAchievementMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
