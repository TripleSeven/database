﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleComponentCommon : IComponentBase
  {
    public List<int> m_gotBattleTreasureIds;
    protected IConfigDataLoader m_configDataLoader;
    protected HeroComponentCommon m_hero;
    protected LevelComponentCommon m_level;
    protected ArenaComponentCommon m_arena;
    protected GuildComponentCommon m_guild;
    protected CollectionComponentCommon m_collection;
    protected UnchartedScoreComponentCommon m_unchartedScore;
    protected RealTimePVPComponentCommon m_realtimePVP;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected TeamComponentCommon m_team;
    protected TrainingGroundCompomentCommon m_trainingGround;
    protected ResourceComponentCommon m_resource;
    protected DataSectionBattle m_battleDS;
    protected BattleBase m_battle;
    private BattlePropertyModifier m_propertyModifier;
    private BattleProperty m_battleProperty;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_IsGameFunctionOpenByMonthCard;
    private static DelegateBridge __Hotfix_GetDailyBonusAddNumsByMonthCard;
    private static DelegateBridge __Hotfix_GetDailyBonusMaxNums;
    private static DelegateBridge __Hotfix_GetSinglePveBattleFightEnergyCost;
    private static DelegateBridge __Hotfix_GetSinglePveBattleFailEnergyCostByMonthCard;
    private static DelegateBridge __Hotfix_GetTeamPveBattleFailEnergyCost;
    private static DelegateBridge __Hotfix_GetTeamPveBattleFailEnergyCostByMonthCard;
    private static DelegateBridge __Hotfix_GetFightFailEnergyCostByMonthcard;
    private static DelegateBridge __Hotfix_AddFightFailCompensationEnergyByMonthCard;
    private static DelegateBridge __Hotfix_AddTeamPveFightFailCompensationEnergyByMonthCard;
    private static DelegateBridge __Hotfix_AddSinglePveFightFailCompensationEnergyByMonthCard;
    private static DelegateBridge __Hotfix_BattleType2GameFunctionType;
    private static DelegateBridge __Hotfix_GameFunctionType2BattleType;
    private static DelegateBridge __Hotfix_IsWaypointBattling;
    private static DelegateBridge __Hotfix_IsCollectionWaypointBattling;
    private static DelegateBridge __Hotfix_OnFlushBattle;
    private static DelegateBridge __Hotfix_IsBattleWin;
    private static DelegateBridge __Hotfix_CancelBattle;
    private static DelegateBridge __Hotfix_IsFighting;
    private static DelegateBridge __Hotfix_CanSetupMineTeam;
    private static DelegateBridge __Hotfix_FightFinished;
    private static DelegateBridge __Hotfix_IsWayPointFightExist;
    private static DelegateBridge __Hotfix_WinPveBattle;
    private static DelegateBridge __Hotfix_FinishedArenaFight;
    private static DelegateBridge __Hotfix_GetProcessingBattle;
    private static DelegateBridge __Hotfix_SetProcessingBattleInfo;
    private static DelegateBridge __Hotfix_SetBattleArmyRandomSeed;
    private static DelegateBridge __Hotfix_GetBattleArmyRandomSeed;
    private static DelegateBridge __Hotfix_SetArenaBattleInfo;
    private static DelegateBridge __Hotfix_IsAttackingPveLevel;
    private static DelegateBridge __Hotfix_IsAttackingArenaOpponent;
    private static DelegateBridge __Hotfix_IsArenaBattleInReady;
    private static DelegateBridge __Hotfix_IsAttackingInBattleServer;
    private static DelegateBridge __Hotfix_FinishBattleInBattleServer;
    private static DelegateBridge __Hotfix_CanCreateBattleRoom;
    private static DelegateBridge __Hotfix_CanCreateTeamBattleRoom;
    private static DelegateBridge __Hotfix_CanChangePlayerBattleStatus;
    private static DelegateBridge __Hotfix_SetArenaBattleFighting;
    private static DelegateBridge __Hotfix_GetBattleId_0;
    private static DelegateBridge __Hotfix_GetBattleId_1;
    private static DelegateBridge __Hotfix_GetMonsterLevel;
    private static DelegateBridge __Hotfix_AddFightHeroFightNumsAndExp;
    private static DelegateBridge __Hotfix_GetPveTeam;
    private static DelegateBridge __Hotfix_GetSubGameTypeId;
    private static DelegateBridge __Hotfix_IsMineTeamSetValid;
    private static DelegateBridge __Hotfix_IsArenaDefensiveTeamSetValid;
    private static DelegateBridge __Hotfix_IsActionPositionIndexValid;
    private static DelegateBridge __Hotfix_IsActionValueValid;
    private static DelegateBridge __Hotfix_IsTeamEmpty;
    private static DelegateBridge __Hotfix_SetMineTeam;
    private static DelegateBridge __Hotfix_SetTeamById;
    private static DelegateBridge __Hotfix_GetGamePlayTeamTypeId;
    private static DelegateBridge __Hotfix_FindGamePlayTeamTypeInfo;
    private static DelegateBridge __Hotfix_GetTeam;
    private static DelegateBridge __Hotfix_GetTeamById;
    private static DelegateBridge __Hotfix_CreateMineBattleHeroEquipments;
    private static DelegateBridge __Hotfix_MineHeroToBattleHero;
    private static DelegateBridge __Hotfix_ComputeBattlePowerFromBattleHeroes;
    private static DelegateBridge __Hotfix_ComputeBattlePower_0;
    private static DelegateBridge __Hotfix_ComputeBattlePower_3;
    private static DelegateBridge __Hotfix_ComputeBattlePower_2;
    private static DelegateBridge __Hotfix_ComputeBattlePower_1;
    private static DelegateBridge __Hotfix_ComputeEquipiemntBattlePower;
    private static DelegateBridge __Hotfix_CollectJobMasterPropertyModifier;
    private static DelegateBridge __Hotfix_CollectJobEquipmentRefineryPropertyModifier;
    private static DelegateBridge __Hotfix_CollectJobPropertyModifier;
    private static DelegateBridge __Hotfix_CollectEquipmentPropertyModifierAndSkillBattlePower;
    private static DelegateBridge __Hotfix_CollectPassiveSkillStaticPropertyModifier;
    private static DelegateBridge __Hotfix_CollectStaticPropertyModifier;
    private static DelegateBridge __Hotfix_InitGainBattleTreasures;
    private static DelegateBridge __Hotfix_AddGotBattleTreasuresInThisBattle;
    private static DelegateBridge __Hotfix_AddBattleTreasures;
    private static DelegateBridge __Hotfix_GetGainBattleTreasuresInThisBattle;
    private static DelegateBridge __Hotfix_get_BattleRandomSeed;
    private static DelegateBridge __Hotfix_get_ArmyRandomSeed;
    private static DelegateBridge __Hotfix_get_ProcessingScenarioId;
    private static DelegateBridge __Hotfix_get_ArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_get_BattleRoomId;
    private static DelegateBridge __Hotfix_set_BattleRoomId;
    private static DelegateBridge __Hotfix_OnBattlePracticeMissionEvent;
    private static DelegateBridge __Hotfix_add_BattlePracticeMissionEvent;
    private static DelegateBridge __Hotfix_remove_BattlePracticeMissionEvent;
    private static DelegateBridge __Hotfix_add_CancelBattleEvent;
    private static DelegateBridge __Hotfix_remove_CancelBattleEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGameFunctionOpenByMonthCard(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetDailyBonusAddNumsByMonthCard(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyBonusMaxNums(GameFunctionType gameFuncTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSinglePveBattleFightEnergyCost()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSinglePveBattleFailEnergyCostByMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeamPveBattleFailEnergyCost(GameFunctionType typeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTeamPveBattleFailEnergyCostByMonthCard(GameFunctionType typeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetFightFailEnergyCostByMonthcard(int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddFightFailCompensationEnergyByMonthCard(GameFunctionType typeId, int energyCost)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTeamPveFightFailCompensationEnergyByMonthCard(
      GameFunctionType typeId,
      int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSinglePveFightFailCompensationEnergyByMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameFunctionType BattleType2GameFunctionType(BattleType battleType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleType GameFunctionType2BattleType(GameFunctionType typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaypointBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCollectionWaypointBattling()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattleWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CancelBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSetupMineTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void FightFinished(GameFunctionStatus status, bool win = false, bool needBatlleLog = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWayPointFightExist(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void WinPveBattle(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishedArenaFight()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProcessingBattle GetProcessingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetProcessingBattleInfo(BattleType type, int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleArmyRandomSeed(int armyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleArmyRandomSeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetArenaBattleInfo(int arenaBattleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackingPveLevel(BattleType battleType, int levelId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackingArenaOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaBattleInReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackingInBattleServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishBattleInBattleServer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateBattleRoom(BattleRoomType battleRoomType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateTeamBattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangePlayerBattleStatus(PlayerBattleStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaBattleFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleId(GameFunctionType typeId, int loctionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFightHeroFightNumsAndExp(List<int> heroes, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetPveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSubGameTypeId(BattleType battleType, int processingBattleInfoTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsMineTeamSetValid(int battleId, int gamePlayTeamTypeId, List<int> teamHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsArenaDefensiveTeamSetValid(int battleId, int teamCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActionPositionIndexValid(
      ConfigDataArenaBattleInfo battleInfo,
      int actionPositionIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActionValueValid(ConfigDataArenaBattleInfo battleInfo, int actionValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTeamEmpty(List<int> team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetMineTeam(int battleId, int gamePlayTeamTypeId, List<int> team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTeamById(int gamePlayTeamTypeId, List<int> team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGamePlayTeamTypeId(BattleType battleType, int subGameTypeId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataGamePlayTeamTypeInfo FindGamePlayTeamTypeInfo(
      BattleType battleType,
      int subGameTypeId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetTeam(BattleType battleType, int subGameTypeId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetTeamById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroEquipment> CreateMineBattleHeroEquipments(
      ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero MineHeroToBattleHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePowerFromBattleHeroes(List<BattleHero> heroes, List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(Hero hero, List<EquipmentBagItem> equipments)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBattlePower(BattleHero hero, List<TrainingTech> techs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeEquipiemntBattlePower(Hero hero, ulong[] equipmentIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobMasterPropertyModifier(
      List<BattleHeroJob> jobs,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectJobEquipmentRefineryPropertyModifier(
      BattleHeroJob job,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void CollectJobPropertyModifier(
      ConfigDataJobInfo jobInfo,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CollectEquipmentPropertyModifierAndSkillBattlePower(
      int heroId,
      List<BattleHeroEquipment> equipments,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPassiveSkillStaticPropertyModifier(
      ConfigDataSkillInfo skillInfo,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectStaticPropertyModifier(
      PropertyModifyType modifyType,
      int value,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitGainBattleTreasures()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGotBattleTreasuresInThisBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddBattleTreasures(List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGainBattleTreasuresInThisBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public int BattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArmyRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ProcessingScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaBattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong BattleRoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattlePracticeMissionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action BattlePracticeMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType, int> CancelBattleEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
