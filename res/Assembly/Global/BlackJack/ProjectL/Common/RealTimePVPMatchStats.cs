﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RealTimePVPMatchStats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RealTimePVPMatchStats
  {
    public int Matches;
    public int Wins;
    public int ConsecutiveWins;
    public int ConsecutiveLosses;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_AddWins;
    private static DelegateBridge __Hotfix_AddLosses;
    private static DelegateBridge __Hotfix_DeepDuplicate;

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddWins()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLosses()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RealTimePVPMatchStats DeepDuplicate()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
