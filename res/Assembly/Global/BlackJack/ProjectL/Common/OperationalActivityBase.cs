﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.OperationalActivityBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class OperationalActivityBase
  {
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_ToInfoString;
    private static DelegateBridge __Hotfix_ToPBNtf;
    private static DelegateBridge __Hotfix_ToPBOperationalActivityBasicData;
    private static DelegateBridge __Hotfix_InitOperationalActivityTime;
    private static DelegateBridge __Hotfix_IsInOperationPeriod;
    private static DelegateBridge __Hotfix_IsInOperationShowPeriod;
    private static DelegateBridge __Hotfix_IsInGainRewardPeriod;
    private static DelegateBridge __Hotfix_IsExpiredOperationalActivity;
    private static DelegateBridge __Hotfix_GetExpiredTime;
    private static DelegateBridge __Hotfix_GetInitialTimeInOneDay;
    private static DelegateBridge __Hotfix_get_OperationShowTime;
    private static DelegateBridge __Hotfix_set_OperationShowTime;
    private static DelegateBridge __Hotfix_get_OperationStartTime;
    private static DelegateBridge __Hotfix_set_OperationStartTime;
    private static DelegateBridge __Hotfix_get_OperationEndTime;
    private static DelegateBridge __Hotfix_set_OperationEndTime;
    private static DelegateBridge __Hotfix_get_GainRewardEndTime;
    private static DelegateBridge __Hotfix_set_GainRewardEndTime;
    private static DelegateBridge __Hotfix_get_DaysAfterPlayerCreated;
    private static DelegateBridge __Hotfix_set_DaysAfterPlayerCreated;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_ActivityId;
    private static DelegateBridge __Hotfix_set_ActivityId;
    private static DelegateBridge __Hotfix_get_ActivityType;
    private static DelegateBridge __Hotfix_set_ActivityType;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_set_Config;

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase(
      ulong instanceId,
      int operationalActivityId,
      OperationalActivityType operationalActivityType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string ToInfoString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void ToPBNtf(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ProOperationalActivityBasicInfo ToPBOperationalActivityBasicData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOperationalActivityTime(
      DateTime operationStartTime,
      DateTime operationEndTime,
      DateTime gainRewardEndTime,
      DateTime operationShowTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInOperationPeriod(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInOperationShowPeriod(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInGainRewardPeriod(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExpiredOperationalActivity(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetExpiredTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetInitialTimeInOneDay(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime OperationShowTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime OperationStartTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime OperationEndTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime GainRewardEndTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DaysAfterPlayerCreated
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActivityId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public OperationalActivityType ActivityType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataOperationalActivityInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
