﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaPlayerInfo
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_DefensiveTeam;
    private static DelegateBridge __Hotfix_set_DefensiveTeam;
    private static DelegateBridge __Hotfix_get_Opponents;
    private static DelegateBridge __Hotfix_set_Opponents;
    private static DelegateBridge __Hotfix_get_ArenaLevelId;
    private static DelegateBridge __Hotfix_set_ArenaLevelId;
    private static DelegateBridge __Hotfix_get_ArenaPoints;
    private static DelegateBridge __Hotfix_set_ArenaPoints;
    private static DelegateBridge __Hotfix_get_AttackedOpponent;
    private static DelegateBridge __Hotfix_set_AttackedOpponent;
    private static DelegateBridge __Hotfix_get_WeekLastFlushTime;
    private static DelegateBridge __Hotfix_set_WeekLastFlushTime;
    private static DelegateBridge __Hotfix_get_VictoryPoints;
    private static DelegateBridge __Hotfix_set_VictoryPoints;
    private static DelegateBridge __Hotfix_get_ReceivedVictoryPointsRewardIndexs;
    private static DelegateBridge __Hotfix_set_ReceivedVictoryPointsRewardIndexs;
    private static DelegateBridge __Hotfix_get_ThisWeekBattleIds;
    private static DelegateBridge __Hotfix_set_ThisWeekBattleIds;
    private static DelegateBridge __Hotfix_get_ThisWeekTotalBattleNums;
    private static DelegateBridge __Hotfix_set_ThisWeekTotalBattleNums;
    private static DelegateBridge __Hotfix_get_ThisWeekVictoryNums;
    private static DelegateBridge __Hotfix_set_ThisWeekVictoryNums;
    private static DelegateBridge __Hotfix_get_IsAutoBattle;
    private static DelegateBridge __Hotfix_set_IsAutoBattle;
    private static DelegateBridge __Hotfix_get_RevengeOpponent;
    private static DelegateBridge __Hotfix_set_RevengeOpponent;
    private static DelegateBridge __Hotfix_get_RevengeBattleReportInstanceId;
    private static DelegateBridge __Hotfix_set_RevengeBattleReportInstanceId;
    private static DelegateBridge __Hotfix_get_OpponentDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_set_OpponentDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_get_MineRank;
    private static DelegateBridge __Hotfix_set_MineRank;
    private static DelegateBridge __Hotfix_get_ConsecutiveVictoryNums;
    private static DelegateBridge __Hotfix_set_ConsecutiveVictoryNums;
    private static DelegateBridge __Hotfix_get_NextFlushOpponentTime;
    private static DelegateBridge __Hotfix_set_NextFlushOpponentTime;
    private static DelegateBridge __Hotfix_PBArenaPlayerInfoToArenaPlayerInfo;
    private static DelegateBridge __Hotfix_ArenaPlayerInfoToPBArenaPlayerInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerDefensiveTeam DefensiveTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<ArenaOpponent> Opponents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaLevelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ushort ArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool AttackedOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime WeekLastFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int VictoryPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> ReceivedVictoryPointsRewardIndexs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> ThisWeekBattleIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ThisWeekTotalBattleNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ThisWeekVictoryNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ArenaOpponent RevengeOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong RevengeBattleReportInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ArenaOpponentDefensiveBattleInfo OpponentDefensiveBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MineRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ConsecutiveVictoryNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long NextFlushOpponentTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaPlayerInfo PBArenaPlayerInfoToArenaPlayerInfo(
      ProArenaPlayerInfo pbArenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaPlayerInfo ArenaPlayerInfoToPBArenaPlayerInfo(
      ArenaPlayerInfo arenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
