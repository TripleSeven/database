﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleHero
  {
    private int m_heroId;
    public List<BattleHeroJob> Jobs;
    public List<int> SelectedSkillList;
    private int m_selectedSoldierId;
    public List<BattleHeroEquipment> Equipments;
    public Dictionary<int, int> Fetters;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_set_HeroId;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_get_ActionPositionIndex;
    private static DelegateBridge __Hotfix_set_ActionPositionIndex;
    private static DelegateBridge __Hotfix_get_ActionValue;
    private static DelegateBridge __Hotfix_set_ActionValue;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_StarLevel;
    private static DelegateBridge __Hotfix_set_StarLevel;
    private static DelegateBridge __Hotfix_get_ActiveHeroJobRelatedId;
    private static DelegateBridge __Hotfix_set_ActiveHeroJobRelatedId;
    private static DelegateBridge __Hotfix_get_ModelSkinId;
    private static DelegateBridge __Hotfix_set_ModelSkinId;
    private static DelegateBridge __Hotfix_get_CharSkinId;
    private static DelegateBridge __Hotfix_set_CharSkinId;
    private static DelegateBridge __Hotfix_get_SelectedSoldierSkinId;
    private static DelegateBridge __Hotfix_set_SelectedSoldierSkinId;
    private static DelegateBridge __Hotfix_set_SelectedSoldierId;
    private static DelegateBridge __Hotfix_get_SelectedSoldierId;
    private static DelegateBridge __Hotfix_get_HeartFetterLevel;
    private static DelegateBridge __Hotfix_set_HeartFetterLevel;
    private static DelegateBridge __Hotfix_get_Power;
    private static DelegateBridge __Hotfix_set_Power;
    private static DelegateBridge __Hotfix_get_Mine;
    private static DelegateBridge __Hotfix_set_Mine;
    private static DelegateBridge __Hotfix_GetJob;
    private static DelegateBridge __Hotfix_GetActiveJob;
    private static DelegateBridge __Hotfix_GetHeroCharImageSkinResourceInfo;
    private static DelegateBridge __Hotfix_GetHeroActiveJobModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_GetSelectedSoldierModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_HeroToBattleHero;
    private static DelegateBridge __Hotfix_PBBattleHeroToBattleHero;
    private static DelegateBridge __Hotfix_BattleHeroToPBBattleHero;
    private static DelegateBridge __Hotfix_ViewBattleHeroToPBBattleHero;
    private static DelegateBridge __Hotfix_PBBattleHeroToViewBattleHero;
    private static DelegateBridge __Hotfix_BattleHeroToBPStageHero;
    private static DelegateBridge __Hotfix_BattleHeroToProBPStageHero;
    private static DelegateBridge __Hotfix_UpdateHeroInfo;
    private static DelegateBridge __Hotfix_UpdateSoldierInfo;
    private static DelegateBridge __Hotfix_set_HeroInfo;
    private static DelegateBridge __Hotfix_get_HeroInfo;
    private static DelegateBridge __Hotfix_set_SelectedSoldierInfo;
    private static DelegateBridge __Hotfix_get_SelectedSoldierInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHero()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionPositionIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int StarLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActiveHeroJobRelatedId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ModelSkinId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CharSkinId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SelectedSoldierSkinId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SelectedSoldierId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeartFetterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Power
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Mine
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob GetJob(int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob GetActiveJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo GetHeroCharImageSkinResourceInfo(
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetHeroActiveJobModelSkinResourceInfo(
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetSelectedSoldierModelSkinResourceInfo(
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero HeroToBattleHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero PBBattleHeroToBattleHero(ProBattleHero pbBattleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHero BattleHeroToPBBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHero ViewBattleHeroToPBBattleHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHero PBBattleHeroToViewBattleHero(ProBattleHero pbBattleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BPStageHero BattleHeroToBPStageHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBPStageHero BattleHeroToProBPStageHero(BattleHero battleHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SelectedSoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
