﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected RiftComponentCommon m_rift;
    protected MissionComponentCommon m_mission;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected ResourceComponentCommon m_resource;
    protected GuildComponentCommon m_guild;
    protected TrainingGroundCompomentCommon m_trainingGround;
    protected DataSectionHero m_heroDS;
    protected Dictionary<ulong, int> m_wearedEquipmentHeroDict;
    protected HashSet<int> m_soliders;
    protected HashSet<int> m_skills;
    protected Dictionary<int, DateTime> m_atuoEquipmentTouch;
    public Action<Hero> HeroFavorabilityLevelupMissionEvent;
    public Action<Hero, int> HeroFetterLevelupMissionEvent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_GetProtagonistID;
    private static DelegateBridge __Hotfix_IsProtagonistHero;
    private static DelegateBridge __Hotfix_IsProtagonistExist;
    private static DelegateBridge __Hotfix_SetProtagonist;
    private static DelegateBridge __Hotfix_GetSpecificHeroesBattlePower;
    private static DelegateBridge __Hotfix_GmStrengthenAllHeroes;
    private static DelegateBridge __Hotfix_GetAllStarLvlMaxHeroes;
    private static DelegateBridge __Hotfix_GetAllStarLvlMaxHeroFragments;
    private static DelegateBridge __Hotfix_InitHeroDataByCaculate_0;
    private static DelegateBridge __Hotfix_InitHeroDataByCaculate_1;
    private static DelegateBridge __Hotfix_InitHeroJobs_0;
    private static DelegateBridge __Hotfix_InitHeroJobs_1;
    private static DelegateBridge __Hotfix_AddHeroDefaultInfos;
    private static DelegateBridge __Hotfix_CanHeroJobEquipSkill_0;
    private static DelegateBridge __Hotfix_CanHeroJobEquipSkill_1;
    private static DelegateBridge __Hotfix_CanSelectSkillHero_0;
    private static DelegateBridge __Hotfix_CanSelectSkillHero_1;
    private static DelegateBridge __Hotfix_HasIgnoreCostEquipment;
    private static DelegateBridge __Hotfix_SelectSkills;
    private static DelegateBridge __Hotfix_SelectSoldier;
    private static DelegateBridge __Hotfix_CanHeroSelectSolider;
    private static DelegateBridge __Hotfix_RemoveHero;
    private static DelegateBridge __Hotfix_RemoveAllHero;
    private static DelegateBridge __Hotfix_RemoveAllHeros;
    private static DelegateBridge __Hotfix_GetAllHeros;
    private static DelegateBridge __Hotfix_FindHeroJob_0;
    private static DelegateBridge __Hotfix_IsUnlockedHeroJob;
    private static DelegateBridge __Hotfix_CanGetHeroJobByRank;
    private static DelegateBridge __Hotfix_FindHeroJob_1;
    private static DelegateBridge __Hotfix_FindHero;
    private static DelegateBridge __Hotfix_AddHeroFightNums;
    private static DelegateBridge __Hotfix_AddHeroAllNeedJobsAchievements;
    private static DelegateBridge __Hotfix_AddHero;
    private static DelegateBridge __Hotfix_AddHeroInfos;
    private static DelegateBridge __Hotfix_IsWastefulAddExp;
    private static DelegateBridge __Hotfix_IsCurrentLevelMaxHeroLevel;
    private static DelegateBridge __Hotfix_IsFullCurrentHeroExp;
    private static DelegateBridge __Hotfix_AddHeroesExp;
    private static DelegateBridge __Hotfix_CanAddHeroExp;
    private static DelegateBridge __Hotfix_AddHeroExp;
    private static DelegateBridge __Hotfix_OutPutHeroLevelUpOperateLog;
    private static DelegateBridge __Hotfix_AddHeroExpByUseableBagItem;
    private static DelegateBridge __Hotfix_ComposeHero;
    private static DelegateBridge __Hotfix_IsLvlMaxHeroStar;
    private static DelegateBridge __Hotfix_LevelUpHeroStarLevel;
    private static DelegateBridge __Hotfix_OutPutHeroUpgradeOperateLog;
    private static DelegateBridge __Hotfix_GetGainHeroIds;
    private static DelegateBridge __Hotfix_HasGainHero;
    private static DelegateBridge __Hotfix_IsExistSkillId;
    private static DelegateBridge __Hotfix_IsExistSoliderId;
    private static DelegateBridge __Hotfix_FindTopLevelHeroes;
    private static DelegateBridge __Hotfix_GetActiveHeroJobRelatedIdByHeroId;
    private static DelegateBridge __Hotfix_GmResetHeroJob;
    private static DelegateBridge __Hotfix_CreateDefaultConfigHero;
    private static DelegateBridge __Hotfix_OnHeroLevelUp;
    private static DelegateBridge __Hotfix_GetAdditiveHeroAddExp;
    private static DelegateBridge __Hotfix_GetAdditiveHeroFavourabilityAddExp;
    private static DelegateBridge __Hotfix_AddHeroJobAchievementsAfterBattleLevelEnd;
    private static DelegateBridge __Hotfix_AddHeroJob;
    private static DelegateBridge __Hotfix_IsJobLevelMax_1;
    private static DelegateBridge __Hotfix_IsJobLevelMax_0;
    private static DelegateBridge __Hotfix_CanLevelUpHeroJobLevel;
    private static DelegateBridge __Hotfix_CanTransferHeroJobRank;
    private static DelegateBridge __Hotfix_CanTransferHeroJob;
    private static DelegateBridge __Hotfix_TransferHeroJob;
    private static DelegateBridge __Hotfix_OutPutHeroJobChangeOperateLog;
    private static DelegateBridge __Hotfix_OnTransferHeroJob;
    private static DelegateBridge __Hotfix_TryEquipNewSkill;
    private static DelegateBridge __Hotfix_IsNeedUnlock;
    private static DelegateBridge __Hotfix_IsNeedAddAchievement;
    private static DelegateBridge __Hotfix_UnlockedHeroJob;
    private static DelegateBridge __Hotfix_OutPutHeroJobUnlockOperateLog;
    private static DelegateBridge __Hotfix_CanUnlockedHeroJob;
    private static DelegateBridge __Hotfix_LevelUpHeroJobLevel;
    private static DelegateBridge __Hotfix_OutPutHeroJobLevelUpOperateLog;
    private static DelegateBridge __Hotfix_AddSkill;
    private static DelegateBridge __Hotfix_AddSkillToHero;
    private static DelegateBridge __Hotfix_AddSoldier;
    private static DelegateBridge __Hotfix_AddSoldierToHero;
    private static DelegateBridge __Hotfix_AutoTakeOffEquipments;
    private static DelegateBridge __Hotfix_InitWearedEquipments;
    private static DelegateBridge __Hotfix_CanWearEquipment;
    private static DelegateBridge __Hotfix_CanWearEquipmentByJobAndArmy;
    private static DelegateBridge __Hotfix_IsEquipmentWeared;
    private static DelegateBridge __Hotfix_GetWearedEquipmentHero;
    private static DelegateBridge __Hotfix_TakeOffEquipmentsOrNotWhenTransferHeroJob;
    private static DelegateBridge __Hotfix_WearEquipment;
    private static DelegateBridge __Hotfix_WearEquipmentBySlot;
    private static DelegateBridge __Hotfix_TakeOffEquipmentBySlot;
    private static DelegateBridge __Hotfix_OnTakeOffEquipment;
    private static DelegateBridge __Hotfix_CanTakeOffEquipment;
    private static DelegateBridge __Hotfix_TakeOffEquipment;
    private static DelegateBridge __Hotfix_HasBetterEquipmentByHero;
    private static DelegateBridge __Hotfix_GetBestEquipments;
    private static DelegateBridge __Hotfix_GetTopLevelEquipments;
    private static DelegateBridge __Hotfix_CanAutoEquipmentByClient;
    private static DelegateBridge __Hotfix_CanAutoEquipment;
    private static DelegateBridge __Hotfix_CanWearCharSkin;
    private static DelegateBridge __Hotfix_WearCharSkin;
    private static DelegateBridge __Hotfix_CanTakeOffCharSkin;
    private static DelegateBridge __Hotfix_TakeOffCharSkin;
    private static DelegateBridge __Hotfix_CanWearModelSkin;
    private static DelegateBridge __Hotfix_WearModelSkin;
    private static DelegateBridge __Hotfix_CanTakeOffModelSkin;
    private static DelegateBridge __Hotfix_TakeOffModelSkin;
    private static DelegateBridge __Hotfix_CanWearSoldierSkin;
    private static DelegateBridge __Hotfix_WearSoldierSkin;
    private static DelegateBridge __Hotfix_CanTakeOffSoldierSkin;
    private static DelegateBridge __Hotfix_TakeOffSoldierSkin;
    private static DelegateBridge __Hotfix_TakeOffSoldierSkinApplyToAll;
    private static DelegateBridge __Hotfix_AddHeroFavorabilityExpByUseableBagItem;
    private static DelegateBridge __Hotfix_AddHeroFavorabilityLevel_1;
    private static DelegateBridge __Hotfix_OutPutHeroFavourabilityOperateLog;
    private static DelegateBridge __Hotfix_AddHeroFavorabilityLevel_0;
    private static DelegateBridge __Hotfix_GenerateFavorabilityLevelUpReward;
    private static DelegateBridge __Hotfix_IsFullFavorabilityExp;
    private static DelegateBridge __Hotfix_UpdateProtagonistHeroFavorabilityLevel;
    private static DelegateBridge __Hotfix_CanUnlockHeroFetter;
    private static DelegateBridge __Hotfix_CanReachFetterUnlockCondition;
    private static DelegateBridge __Hotfix_GmLevelUpHeroFetter2SpecificLevel;
    private static DelegateBridge __Hotfix_CanLevelUpHeroFetter;
    private static DelegateBridge __Hotfix_LevelUpHeroFetter;
    private static DelegateBridge __Hotfix_LevelUpHeroFetterCallBack;
    private static DelegateBridge __Hotfix_OutPutHeroFetterOpereateLog;
    private static DelegateBridge __Hotfix_UnlockHeroFetter;
    private static DelegateBridge __Hotfix_GenerateFetterUnlockRewards;
    private static DelegateBridge __Hotfix_ConfessHero;
    private static DelegateBridge __Hotfix_GenerateConfessionRewards;
    private static DelegateBridge __Hotfix_CanConfessHero;
    private static DelegateBridge __Hotfix_GmSetHeroHeartFetter;
    private static DelegateBridge __Hotfix_CanUnlockHeroHeartFetter;
    private static DelegateBridge __Hotfix_CanReachHeroHeartFetterUnlockCondition;
    private static DelegateBridge __Hotfix_UnlockHeroHeartFetter;
    private static DelegateBridge __Hotfix_OnHeroHeartFettterUnlock;
    private static DelegateBridge __Hotfix_GenerateHeroHeartFetterUnlockReward;
    private static DelegateBridge __Hotfix_CanLevelUpHeroHeartFetter;
    private static DelegateBridge __Hotfix_LevelUpHeroHeartFetter;
    private static DelegateBridge __Hotfix_OnHeroHeartFetterLevelUp;
    private static DelegateBridge __Hotfix_OnHeroHeartFetterLevelMax;
    private static DelegateBridge __Hotfix_CanRefineHeroJob;
    private static DelegateBridge __Hotfix_RefineHeroJob;
    private static DelegateBridge __Hotfix_GenerateTemporaryRefinerySlotProperty;
    private static DelegateBridge __Hotfix_SaveHeroJobRefinerySlotProperty;
    private static DelegateBridge __Hotfix_CanOpenHeroJobRefineryReq;
    private static DelegateBridge __Hotfix_OpenHeroJobRefineryReq;
    private static DelegateBridge __Hotfix_FlushHeroInteractNums;
    private static DelegateBridge __Hotfix_InitHeroInteractNums;
    private static DelegateBridge __Hotfix_CanFlushHeroInteractNums;
    private static DelegateBridge __Hotfix_IsFullHeroInteractNums;
    private static DelegateBridge __Hotfix_AddHeroInteractNums;
    private static DelegateBridge __Hotfix_IsHeroInteractNumsEqualToMax;
    private static DelegateBridge __Hotfix_CanInteractHero;
    private static DelegateBridge __Hotfix_GetHeroFavorabilityUpLevel;
    private static DelegateBridge __Hotfix_GetHeroInteractId;
    private static DelegateBridge __Hotfix_GetAllHeroBattlePower;
    private static DelegateBridge __Hotfix_GetLastAllHeroRank;
    private static DelegateBridge __Hotfix_GetTopHeroBattlePower;
    private static DelegateBridge __Hotfix_GetLastTopHeroRank;
    private static DelegateBridge __Hotfix_GetTopFifteenHeroBattlePower;
    private static DelegateBridge __Hotfix_GetLastTopFifteenHeroRank;
    private static DelegateBridge __Hotfix_GetChampionHeroBattlePower;
    private static DelegateBridge __Hotfix_GetChampionHeroId;
    private static DelegateBridge __Hotfix_GetLastChampionHeroRank;
    private static DelegateBridge __Hotfix_ComputeBattlePower;
    private static DelegateBridge __Hotfix_OnHeroBattlePowerChange;
    private static DelegateBridge __Hotfix_CanComposeHero;
    private static DelegateBridge __Hotfix_CanLevelUpHeroStarLevel;
    private static DelegateBridge __Hotfix_GetEquipedEquipments;
    private static DelegateBridge __Hotfix_GetUnlcokJobAchievements;
    private static DelegateBridge __Hotfix_GetSkillEnergyFromConfig_0;
    private static DelegateBridge __Hotfix_GetSkillEnergyFromConfig_1;
    private static DelegateBridge __Hotfix_GetHeroStarLevelUpDataFromConfig;
    private static DelegateBridge __Hotfix_GetSkillCostFromConfig_0;
    private static DelegateBridge __Hotfix_GetSkillCostFromConfig_1;
    private static DelegateBridge __Hotfix_add_AddHeroEvent;
    private static DelegateBridge __Hotfix_remove_AddHeroEvent;
    private static DelegateBridge __Hotfix_add_SpecificHeroFightMissionEvent;
    private static DelegateBridge __Hotfix_remove_SpecificHeroFightMissionEvent;
    private static DelegateBridge __Hotfix_add_HeroNewJobTransferMissionEvent;
    private static DelegateBridge __Hotfix_remove_HeroNewJobTransferMissionEvent;
    private static DelegateBridge __Hotfix_add_HeroJobLevelUpMissionEvent;
    private static DelegateBridge __Hotfix_remove_HeroJobLevelUpMissionEvent;
    private static DelegateBridge __Hotfix_add_HeroJobMasterMissionEvent;
    private static DelegateBridge __Hotfix_remove_HeroJobMasterMissionEvent;
    private static DelegateBridge __Hotfix_add_AddHeroJobMissionEvent;
    private static DelegateBridge __Hotfix_remove_AddHeroJobMissionEvent;
    private static DelegateBridge __Hotfix_add_HeroLevelUpMissionEvent;
    private static DelegateBridge __Hotfix_remove_HeroLevelUpMissionEvent;
    private static DelegateBridge __Hotfix_add_AddHeroJobSkillMissionEvent;
    private static DelegateBridge __Hotfix_remove_AddHeroJobSkillMissionEvent;
    private static DelegateBridge __Hotfix_add_AddHeroJobSoliderMissionEvent;
    private static DelegateBridge __Hotfix_remove_AddHeroJobSoliderMissionEvent;
    private static DelegateBridge __Hotfix_add_AddHeroMissionEvent;
    private static DelegateBridge __Hotfix_remove_AddHeroMissionEvent;
    private static DelegateBridge __Hotfix_add_LevelUpHeroStarLevelMissionEvent;
    private static DelegateBridge __Hotfix_remove_LevelUpHeroStarLevelMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetProtagonistID()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProtagonistHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProtagonistExist()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetProtagonist(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSpecificHeroesBattlePower(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmStrengthenAllHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetAllStarLvlMaxHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<HeroFragmentBagItem> GetAllStarLvlMaxHeroFragments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitHeroDataByCaculate(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InitHeroDataByCaculate(
      Hero hero,
      TrainingGroundCompomentCommon trainingGround,
      IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitHeroJobs(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InitHeroJobs(Hero hero, IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroDefaultInfos(Hero hero, ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanHeroJobEquipSkill(int jobRelatedId, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CanHeroJobEquipSkill(int jobRelatedId, int skillId, IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSelectSkillHero(Hero hero, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CanSelectSkillHero(
      BattleHero hero,
      List<int> skills,
      IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HasIgnoreCostEquipment(BattleHero hero, IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SelectSkills(int heroId, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SelectSoldier(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int CanHeroSelectSolider(Hero hero, int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHero(List<int> changedHeroIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob FindHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUnlockedHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanGetHeroJobByRank(Hero hero, int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob FindHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero FindHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroFightNums(int heroId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroAllNeedJobsAchievements(int heroId, List<int> achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Hero AddHeroInfos(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWastefulAddExp(Hero hero, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCurrentLevelMaxHeroLevel(int heroLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullCurrentHeroExp(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroesExp(List<int> heroIds, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAddHeroExp(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroExp(int heroId, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroLevelUpOperateLog(int heroId, int currentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroExpByUseableBagItem(
      int heroId,
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComposeHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLvlMaxHeroStar(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpHeroStarLevel(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroUpgradeOperateLog(
      int heroId,
      int currentStar,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetGainHeroIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGainHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistSkillId(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistSoliderId(int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<Hero> FindTopLevelHeroes(int topNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActiveHeroJobRelatedIdByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmResetHeroJob(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Hero CreateDefaultConfigHero(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroLevelUp(int oldLevel, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAdditiveHeroAddExp(int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAdditiveHeroFavourabilityAddExp(int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroJobAchievementsAfterBattleLevelEnd(
      List<int> relatedAchievements,
      List<int> fightHeroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroJob(Hero hero, ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsJobLevelMax(ConfigDataJobConnectionInfo jobConnectionInfo, int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsJobLevelMax(HeroJob job)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroJobLevel(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanTransferHeroJobRank(int jobRank, int CanTransferMaxRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTransferHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int TransferHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroJobChangeOperateLog(
      int heroId,
      int preJobConnectionId,
      int postJobConnectionId,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTransferHeroJob(
      Hero hero,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TryEquipNewSkill(Hero hero, int newGotSkillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedUnlock(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedAddAchievement(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UnlockedHeroJob(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroJobUnlockOperateLog(
      int heroId,
      int activeJobId,
      int unlockJobId,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanUnlockedHeroJob(Hero hero, ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int LevelUpHeroJobLevel(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroJobLevelUpOperateLog(
      int heroId,
      int activeJobId,
      int currentLevel,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSkill(Hero hero, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddSkillToHero(Hero hero, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSoldier(Hero hero, int soldierId, bool needExchange = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddSoldierToHero(Hero hero, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AutoTakeOffEquipments(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitWearedEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearEquipment(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearEquipmentByJobAndArmy(Hero hero, BagItemBase equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentWeared(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero GetWearedEquipmentHero(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TakeOffEquipmentsOrNotWhenTransferHeroJob(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearEquipment(int heroId, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void WearEquipmentBySlot(Hero hero, int slot, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TakeOffEquipmentBySlot(Hero hero, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTakeOffEquipment(Hero hero, ulong equipmentInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffEquipment(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffEquipment(int heroId, int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBetterEquipmentByHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ulong[] GetBestEquipments(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Dictionary<int, List<HeroComponentCommon.EquipmentBattlePowerInfo>> GetTopLevelEquipments(
      Hero hero,
      int topNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoEquipmentByClient(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoEquipment(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearCharSkin(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearCharSkin(int heroId, int charSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffCharSkin(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffCharSkin(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearModelSkin(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearModelSkin(int heroId, int jobRelatedId, int modelSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffModelSkin(int heroId, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanWearSoldierSkin(int heroId, int soldierId, int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int WearSoldierSkin(int heroId, int soldierId, int soldierSkinId, bool applyToAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanTakeOffSoldierSkin(int heroId, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int TakeOffSoldierSkin(int heroId, int soldierId, bool applyToAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TakeOffSoldierSkinApplyToAll(int soldierId, int skinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroFavorabilityExpByUseableBagItem(
      int heroId,
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddHeroFavorabilityLevel(int heroId, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroFavourabilityOperateLog(
      int heroId,
      int oldLvl,
      int oldExp,
      int newLvl,
      int newExp,
      List<Goods> rewards = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddHeroFavorabilityLevel(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateFavorabilityLevelUpReward(int heroId, int dropId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFullFavorabilityExp(Hero hero, ConfigDataHeroInformationInfo heroInformationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void UpdateProtagonistHeroFavorabilityLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReachFetterUnlockCondition(HeroFetterCompletionCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmLevelUpHeroFetter2SpecificLevel(int heroId, int fetterId, int reachLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int LevelUpHeroFetter(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void LevelUpHeroFetterCallBack(Hero hero, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutHeroFetterOpereateLog(
      int heroId,
      int fetterId,
      int currentLvl,
      List<Goods> itemCost = null,
      List<Goods> rewards = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UnlockHeroFetter(int heroId, int fetterId, bool check = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateFetterUnlockRewards(int heroId, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ConfessHero(int heroId, bool check = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateConfessionRewards(
      int herId,
      ConfigDataHeroConfessionInfo confessionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanConfessHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GmSetHeroHeartFetter(int heroId, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReachHeroHeartFetterUnlockCondition(
      Hero hero,
      HeroHeartFetterUnlockCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UnlockHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroHeartFettterUnlock(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateHeroHeartFetterUnlockReward(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpHeroHeartFetter(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnHeroHeartFetterLevelUp(
      Hero hero,
      ConfigDataHeroHeartFetterInfo heroHeartFetterInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeroHeartFetterLevelMax(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRefineHeroJob(
      int heroId,
      int jobConnectionId,
      int slotId,
      int index,
      int stoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RefineHeroJob(int heroId, int jobConnectionId, int slotId, int index, int stoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateTemporaryRefinerySlotProperty(
      int heroId,
      int jobConnectionId,
      int slotId,
      int index,
      ConfigDataRefineryStoneInfo stoneInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SaveHeroJobRefinerySlotProperty(
      int heroId,
      int jobConnectionId,
      int slotId,
      HeroJobSlotRefineryProperty newProperty)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanOpenHeroJobRefineryReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int OpenHeroJobRefineryReq(int heroId, int jobConnectionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanFlushHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFullHeroInteractNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddHeroInteractNums(int addNums, bool recoveryByTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeroInteractNumsEqualToMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanInteractHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroFavorabilityUpLevel(
      Hero hero,
      ConfigDataHeroInformationInfo heroInformationInfo,
      int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetHeroInteractId(Hero hero, ConfigDataHeroInformationInfo heroInformationInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastAllHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTopHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastTopHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTopFifteenHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastTopFifteenHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChampionHeroBattlePower()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChampionHeroId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastChampionHeroRank()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ComputeBattlePower(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnHeroBattlePowerChange(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpHeroStarLevel(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ulong> GetEquipedEquipments()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<int> GetUnlcokJobAchievements(ConfigDataJobConnectionInfo jobConnectionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkillEnergyFromConfig(int heroLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetSkillEnergyFromConfig(int heroLevel, IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetHeroStarLevelUpDataFromConfig(int fragmentId, int newStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetSkillCostFromConfig(int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int GetSkillCostFromConfig(int skillId, IConfigDataLoader config)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> AddHeroEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> SpecificHeroFightMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> HeroNewJobTransferMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action HeroJobLevelUpMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, int> HeroJobMasterMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action AddHeroJobMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> HeroLevelUpMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddHeroJobSkillMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddHeroJobSoliderMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddHeroMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> LevelUpHeroStarLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class EquipmentBattlePowerInfo
    {
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_get_InstanceId;
      private static DelegateBridge __Hotfix_set_InstanceId;
      private static DelegateBridge __Hotfix_get_Level;
      private static DelegateBridge __Hotfix_set_Level;

      [MethodImpl((MethodImplOptions) 32768)]
      public EquipmentBattlePowerInfo()
      {
        // ISSUE: unable to decompile the method.
      }

      public ulong InstanceId
      {
        [MethodImpl((MethodImplOptions) 32768)] get
        {
          // ISSUE: unable to decompile the method.
        }
        [MethodImpl((MethodImplOptions) 32768)] set
        {
          // ISSUE: unable to decompile the method.
        }
      }

      public int Level
      {
        [MethodImpl((MethodImplOptions) 32768)] get
        {
          // ISSUE: unable to decompile the method.
        }
        [MethodImpl((MethodImplOptions) 32768)] set
        {
          // ISSUE: unable to decompile the method.
        }
      }
    }
  }
}
