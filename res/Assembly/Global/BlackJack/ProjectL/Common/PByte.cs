﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PByte
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct PByte
  {
    private byte m_Value;
    private byte m_Check;

    [MethodImpl((MethodImplOptions) 32768)]
    public PByte(byte v)
    {
      // ISSUE: unable to decompile the method.
    }

    public static implicit operator PByte(byte value)
    {
      return new PByte(value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static implicit operator byte(PByte p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    private static byte Encode(byte v)
    {
      return (byte) ((uint) v ^ 85U);
    }

    private static byte Decode(byte v)
    {
      return (byte) ((uint) v ^ 85U);
    }

    private static byte Check(byte v)
    {
      return (byte) ((uint) v ^ 119U);
    }
  }
}
