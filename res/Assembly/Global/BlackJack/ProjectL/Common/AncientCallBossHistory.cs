﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCallBossHistory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class AncientCallBossHistory
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_BossId;
    private static DelegateBridge __Hotfix_set_BossId;
    private static DelegateBridge __Hotfix_get_MaxDamage;
    private static DelegateBridge __Hotfix_set_MaxDamage;
    private static DelegateBridge __Hotfix_get_HistoryPeriodMaxRank;
    private static DelegateBridge __Hotfix_set_HistoryPeriodMaxRank;
    private static DelegateBridge __Hotfix_FromPB;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_UpdateHistoryPeriodMaxRank;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossHistory()
    {
      // ISSUE: unable to decompile the method.
    }

    public int BossId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MaxDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HistoryPeriodMaxRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCallBossHistory FromPB(
      ProAncientCallBossHistory pbHistory)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCallBossHistory ToPB(
      AncientCallBossHistory history)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateHistoryPeriodMaxRank(int periodFinalRank)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
