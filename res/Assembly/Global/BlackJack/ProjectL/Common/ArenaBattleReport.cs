﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaBattleReport : BattleReport
  {
    public List<BattleHero> DefenderHeroes;
    public List<TrainingTech> DefenderTechs;
    public List<BattleHero> AttackerHeroes;
    public List<TrainingTech> AttackerTechs;
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_2;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_get_Status;
    private static DelegateBridge __Hotfix_set_Status;
    private static DelegateBridge __Hotfix_get_ArenaDefenderRuleId;
    private static DelegateBridge __Hotfix_set_ArenaDefenderRuleId;
    private static DelegateBridge __Hotfix_get_DefenderUserId;
    private static DelegateBridge __Hotfix_set_DefenderUserId;
    private static DelegateBridge __Hotfix_get_DefenderName;
    private static DelegateBridge __Hotfix_set_DefenderName;
    private static DelegateBridge __Hotfix_get_DefenderLevel;
    private static DelegateBridge __Hotfix_set_DefenderLevel;
    private static DelegateBridge __Hotfix_get_AttackerUserId;
    private static DelegateBridge __Hotfix_set_AttackerUserId;
    private static DelegateBridge __Hotfix_get_AttackerName;
    private static DelegateBridge __Hotfix_set_AttackerName;
    private static DelegateBridge __Hotfix_get_AttackerLevel;
    private static DelegateBridge __Hotfix_set_AttackerLevel;
    private static DelegateBridge __Hotfix_get_AttackerGotArenaPoints;
    private static DelegateBridge __Hotfix_set_AttackerGotArenaPoints;
    private static DelegateBridge __Hotfix_get_DefenderGotArenaPoints;
    private static DelegateBridge __Hotfix_set_DefenderGotArenaPoints;
    private static DelegateBridge __Hotfix_get_CreateTime;
    private static DelegateBridge __Hotfix_set_CreateTime;
    private static DelegateBridge __Hotfix_get_OpponentHeadIcon;
    private static DelegateBridge __Hotfix_set_OpponentHeadIcon;
    private static DelegateBridge __Hotfix_PBArenaBattleReportToArenaBattleReport;
    private static DelegateBridge __Hotfix_ArenaBattleReportToPBArenaBattleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport(BattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport(ArenaBattleReport other)
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaBattleReportStatus Status
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaDefenderRuleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string DefenderUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string DefenderName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DefenderLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string AttackerUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string AttackerName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AttackerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AttackerGotArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DefenderGotArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long CreateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OpponentHeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaBattleReport PBArenaBattleReportToArenaBattleReport(
      ProArenaBattleReport pbArenaBattleReport,
      bool isBattleData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaBattleReport ArenaBattleReportToPBArenaBattleReport(
      ArenaBattleReport arenaBattleReport,
      bool isBattleData)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
