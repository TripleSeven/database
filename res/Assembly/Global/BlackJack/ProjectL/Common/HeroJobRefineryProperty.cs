﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroJobRefineryProperty
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroJobRefineryProperty
  {
    public List<HeroJobSlotRefineryProperty> Properties;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_SlotId;
    private static DelegateBridge __Hotfix_set_SlotId;
    private static DelegateBridge __Hotfix_FindHeroJobSlotRefineryPropertyByIndex;
    private static DelegateBridge __Hotfix_IsExistSameTypeProperty;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_FromPB;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobRefineryProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    public int SlotId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobSlotRefineryProperty FindHeroJobSlotRefineryPropertyByIndex(
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistSameTypeProperty(PropertyModifyType typeId, int ignoreIndex = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroJobRefineryProperty ToPB(
      HeroJobRefineryProperty property)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroJobRefineryProperty FromPB(
      ProHeroJobRefineryProperty pbProperty)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
