﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UnchartedScoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UnchartedScoreComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected IConfigDataLoader m_configDataLoader;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected HeroComponentCommon m_hero;
    protected OperationalActivityCompomentCommon m_operationalActivity;
    protected DataSectionUnchartedScore m_UnchartedScoreDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_IsUnchartedScoreOnActivityTime;
    private static DelegateBridge __Hotfix_GetAllOpenActivityUnchartedScore;
    private static DelegateBridge __Hotfix_FindOperationalActivityByUnchartedScoreId;
    private static DelegateBridge __Hotfix_IsChallengeLevelContains;
    private static DelegateBridge __Hotfix_IsChallengeLevelExist;
    private static DelegateBridge __Hotfix_IsChallengeLevelTimeUnlock;
    private static DelegateBridge __Hotfix_IsChallengePrevLevelComplete;
    private static DelegateBridge __Hotfix_IsScoreLevelContains;
    private static DelegateBridge __Hotfix_IsScoreLevelExist;
    private static DelegateBridge __Hotfix_IsScoreLevelTimeUnlock;
    private static DelegateBridge __Hotfix_IsPlayerLevelVaild;
    private static DelegateBridge __Hotfix_CalcScoreLevelScore;
    private static DelegateBridge __Hotfix_CalcChallengeLevelScore;
    private static DelegateBridge __Hotfix_CalcAllHeroBonus;
    private static DelegateBridge __Hotfix_IsChallengeLevelUnlock;
    private static DelegateBridge __Hotfix_IsScoreLevelUnlock_1;
    private static DelegateBridge __Hotfix_IsScoreLevelUnlock_0;
    private static DelegateBridge __Hotfix_GetAllUnlockedScoreLevels;
    private static DelegateBridge __Hotfix_IsChallengeLevelComplete;
    private static DelegateBridge __Hotfix_IsScoreLevelComplete;
    private static DelegateBridge __Hotfix_IsUnchartedScoreExist;
    private static DelegateBridge __Hotfix_GetReward;
    private static DelegateBridge __Hotfix_CanRewardGain;
    private static DelegateBridge __Hotfix_RemoveUnchartedScore;
    private static DelegateBridge __Hotfix_CheckChallengeLevelEnergy;
    private static DelegateBridge __Hotfix_CheckScoreLevelEnergy;
    private static DelegateBridge __Hotfix_CanAttackChallengeLevel;
    private static DelegateBridge __Hotfix_AttackChallengeLevel;
    private static DelegateBridge __Hotfix_SetCommonSuccessChallengeLevel;
    private static DelegateBridge __Hotfix_SetCommonSuccessScoreLevel;
    private static DelegateBridge __Hotfix_CanAttackScoreLevel;
    private static DelegateBridge __Hotfix_AttackScoreLevel;
    private static DelegateBridge __Hotfix_CheckScoreReward;
    private static DelegateBridge __Hotfix_FlushBounsCount;
    private static DelegateBridge __Hotfix_SetScoreRewardGain;
    private static DelegateBridge __Hotfix_GetUnchartedScore;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsUnchartedScoreOnActivityTime(int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllOpenActivityUnchartedScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityBase FindOperationalActivityByUnchartedScoreId(
      int UnchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelContains(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelExist(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelTimeUnlock(
      OperationalActivityBase operationalActivity,
      ConfigDataChallengeLevelInfo levelInfo,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePrevLevelComplete(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelContains(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelExist(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelTimeUnlock(
      OperationalActivityBase operationalActivity,
      ConfigDataScoreLevelInfo levelInfo,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerLevelVaild(ConfigDataScoreLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcScoreLevelScore(
      UnchartedScore unchartedScore,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> heroIdList,
      bool levelBonus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcChallengeLevelScore(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> heroIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalcAllHeroBonus(UnchartedScore unchartedScore, List<int> heroIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelUnlock(
      UnchartedScore unchartedScore,
      int levelId,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelUnlock(
      UnchartedScore unchartedScore,
      int levelId,
      DateTime curDateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelUnlock(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedScoreLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengeLevelComplete(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScoreLevelComplete(UnchartedScore unchartedScore, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsUnchartedScoreExist(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Goods> GetReward(int unchartedScoreId, int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanRewardGain(int unchartedScoreId, int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveUnchartedScore(int unchartedScoreId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckChallengeLevelEnergy(ConfigDataChallengeLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckScoreLevelEnergy(ConfigDataScoreLevelInfo levelInfo, bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackChallengeLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackChallengeLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessChallengeLevel(
      UnchartedScore unchartedScore,
      ConfigDataChallengeLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCommonSuccessScoreLevel(
      UnchartedScore unchartedScore,
      ConfigDataScoreLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      List<int> allHeroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackScoreLevel(int unchartedScoreId, int levelId, bool isTeamBattle = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackScoreLevel(int unchartedScoreId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void CheckScoreReward(UnchartedScore unchartedScore, GameFunctionType causeId = GameFunctionType.GameFunctionType_None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushBounsCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScoreRewardGain(UnchartedScore unchartedScore, int score)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedScore GetUnchartedScore(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
