﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class TeamComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionTeam m_teamDS;
    protected BagComponentCommon m_bag;
    protected AnikiGymComponentCommon m_anikiGym;
    protected MemoryCorridorCompomentCommon m_memoryCorridor;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected EternalShrineCompomentCommon m_eternalShrine;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected HeroTrainningComponentCommon m_heroTrainning;
    protected CooperateBattleCompomentCommon m_cooperateBattle;
    protected UnchartedScoreComponentCommon m_unchartedScore;
    protected CollectionComponentCommon m_collectionActivity;
    protected BattleComponentCommon m_battle;
    public Action FinishTeamBattleMissionEvent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_IsLevelUnlocked;
    private static DelegateBridge __Hotfix_CanCreateTeam;
    private static DelegateBridge __Hotfix_CreateTeam;
    private static DelegateBridge __Hotfix_IsTeamRoomAuthorityValid;
    private static DelegateBridge __Hotfix_CanAttackTeamGameFunction;
    private static DelegateBridge __Hotfix_CanViewTeamRoom;
    private static DelegateBridge __Hotfix_CanAutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_AutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_JoinTeamRoom;
    private static DelegateBridge __Hotfix_GetTeamRoomInfo;
    private static DelegateBridge __Hotfix_QuitTeamRoom;
    private static DelegateBridge __Hotfix_CanCancelAutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_CanChangeTeamRoomAuthority;
    private static DelegateBridge __Hotfix_IsInTeam;
    private static DelegateBridge __Hotfix_IsInRoom;
    private static DelegateBridge __Hotfix_IsInWaitingList;
    private static DelegateBridge __Hotfix_CancelAutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_CanJoinTeamRoom;
    private static DelegateBridge __Hotfix_CanQuitTeamRoom;
    private static DelegateBridge __Hotfix_CanGetTeamRoom;
    private static DelegateBridge __Hotfix_CanChangeTeamRoomPlayerPosition;
    private static DelegateBridge __Hotfix_CanInviteTeamRoom;
    private static DelegateBridge __Hotfix_InviteTeamRoom;
    private static DelegateBridge __Hotfix_SetTeamRoomInviteInfo;
    private static DelegateBridge __Hotfix_IsInvited;
    private static DelegateBridge __Hotfix_ClearAInviteInfo;
    private static DelegateBridge __Hotfix_ClearTeamRoom;
    private static DelegateBridge __Hotfix_CanRefuseInvitation;
    private static DelegateBridge __Hotfix_GetInviteInfos;
    private static DelegateBridge __Hotfix_DeductTeamPveBattleEnergy;
    private static DelegateBridge __Hotfix_CanGetInviteeInfo;
    private static DelegateBridge __Hotfix_get_WaitingFunctionTypeId;
    private static DelegateBridge __Hotfix_get_WaitingLocationId;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_get_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_get_LocationId;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsLevelUnlocked(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCreateTeam(ProTeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateTeam(int roomId, GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTeamRoomAuthorityValid(int authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackTeamGameFunction(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanViewTeamRoom(GameFunctionType gameFunctionTypeId, int chapterId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAutoMatchTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoMatchTeamRoom(int roomId, GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamRoom(int roomId, GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetTeamRoomInfo(out GameFunctionType typeId, out int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void QuitTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanCancelAutoMatchTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangeTeamRoomAuthority(int authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInWaitingList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelAutoMatchTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanJoinTeamRoom(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanQuitTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangeTeamRoomPlayerPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanInviteTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InviteTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetTeamRoomInviteInfo(TeamRoomInviteInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInvited(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAInviteInfo(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRefuseInvitation(ulong sessionId, int roomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<TeamRoomInviteInfo> GetInviteInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeductTeamPveBattleEnergy(GameFunctionType gameFunctionTypeId, int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetInviteeInfo(List<string> userIds)
    {
      // ISSUE: unable to decompile the method.
    }

    public GameFunctionType WaitingFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int WaitingLocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameFunctionType GameFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
