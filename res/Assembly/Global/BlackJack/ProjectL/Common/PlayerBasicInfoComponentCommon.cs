﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerBasicInfoComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PlayerBasicInfoComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected LevelComponentCommon m_level;
    protected BagComponentCommon m_bag;
    protected MissionComponentCommon m_mission;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected ThearchyTrialCompomentCommon m_thearchyTrial;
    protected ResourceComponentCommon m_resource;
    protected GuildComponentCommon m_guild;
    protected PeakArenaComponentCommon m_peakArena;
    protected List<BagItemBase> m_changedGoods;
    public DataSectionPlayerBasicInfo m_playerBasicDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_BuyJetton;
    private static DelegateBridge __Hotfix_OnBuyJettonGoodsSettlement;
    private static DelegateBridge __Hotfix_CanBuyJetton;
    private static DelegateBridge __Hotfix_TryUpdateSignedDays;
    private static DelegateBridge __Hotfix_TickTitleExpired;
    private static DelegateBridge __Hotfix_IsGameFunctionOpened;
    private static DelegateBridge __Hotfix_GetCreateTime;
    private static DelegateBridge __Hotfix_GetRefluxBeginTime;
    private static DelegateBridge __Hotfix_GetCreateTimeUtc;
    private static DelegateBridge __Hotfix_GetLastLogoutTime;
    private static DelegateBridge __Hotfix_GetLoginTime;
    private static DelegateBridge __Hotfix_GetPlayerName;
    private static DelegateBridge __Hotfix_GetUserId;
    private static DelegateBridge __Hotfix_IsMe;
    private static DelegateBridge __Hotfix_GetCurrentLevelExp;
    private static DelegateBridge __Hotfix_GetRechargedCsystal;
    private static DelegateBridge __Hotfix_GetRechargeRMB;
    private static DelegateBridge __Hotfix_GetHeadIcon;
    private static DelegateBridge __Hotfix_GetDefaultHeadIcon;
    private static DelegateBridge __Hotfix_GetHeadPortrait;
    private static DelegateBridge __Hotfix_GetHeadFrame;
    private static DelegateBridge __Hotfix_IsPlayerLevelMax;
    private static DelegateBridge __Hotfix_CanAddPlayerExp;
    private static DelegateBridge __Hotfix_AddPlayerExp;
    private static DelegateBridge __Hotfix_OnLevelChange;
    private static DelegateBridge __Hotfix_LevelUp;
    private static DelegateBridge __Hotfix_AddChallengePoint;
    private static DelegateBridge __Hotfix_AddFashionPoint;
    private static DelegateBridge __Hotfix_AddGold;
    private static DelegateBridge __Hotfix_IsGoldEnough;
    private static DelegateBridge __Hotfix_IsGoldOverFlow;
    private static DelegateBridge __Hotfix_GetGold;
    private static DelegateBridge __Hotfix_AddBrillianceMithralStone_0;
    private static DelegateBridge __Hotfix_AddMithralStone_0;
    private static DelegateBridge __Hotfix_IsMithralStoneEnough;
    private static DelegateBridge __Hotfix_IsBrillianceMithralStoneEnough;
    private static DelegateBridge __Hotfix_IsCurrencyEnough;
    private static DelegateBridge __Hotfix_AddRechargeRMB;
    private static DelegateBridge __Hotfix_AddCrystal;
    private static DelegateBridge __Hotfix_IsCrystalEnough;
    private static DelegateBridge __Hotfix_InitEnergy;
    private static DelegateBridge __Hotfix_IsReachEnergyMax;
    private static DelegateBridge __Hotfix_FlushEnergy;
    private static DelegateBridge __Hotfix_CanFlushEnergy;
    private static DelegateBridge __Hotfix_CanFlushPlayerAction;
    private static DelegateBridge __Hotfix_ResetPlayerActionNextFlushTime;
    private static DelegateBridge __Hotfix_IncreamentEnergy;
    private static DelegateBridge __Hotfix_DecreaseEnergy;
    private static DelegateBridge __Hotfix_IsEnergyEnough;
    private static DelegateBridge __Hotfix_GetEnergy;
    private static DelegateBridge __Hotfix_GetCurrentTime;
    private static DelegateBridge __Hotfix_IsSigned;
    private static DelegateBridge __Hotfix_CanSignToday;
    private static DelegateBridge __Hotfix_CanBuyEnergy;
    private static DelegateBridge __Hotfix_GetBuyEnergyConfig;
    private static DelegateBridge __Hotfix_IsBoughtNumsUsedOut;
    private static DelegateBridge __Hotfix_BuyEnergy;
    private static DelegateBridge __Hotfix_CanBuyArenaTickets;
    private static DelegateBridge __Hotfix_GetBuyArenaTicketConfig;
    private static DelegateBridge __Hotfix_BuyArenaTickets;
    private static DelegateBridge __Hotfix_GetNextFlushPlayerActionTime;
    private static DelegateBridge __Hotfix_FlushPlayerAction;
    private static DelegateBridge __Hotfix_OnPlayerActionFlushEvent;
    private static DelegateBridge __Hotfix_OnFlush;
    private static DelegateBridge __Hotfix_IsArenaTicketsEnough;
    private static DelegateBridge __Hotfix_IsArenaTicketsFull;
    private static DelegateBridge __Hotfix_AddArenaTickets;
    private static DelegateBridge __Hotfix_GetAreanaTicketNums;
    private static DelegateBridge __Hotfix_AddRechargedCrystal;
    private static DelegateBridge __Hotfix_AddArenaHonour;
    private static DelegateBridge __Hotfix_GetArenaHonour;
    private static DelegateBridge __Hotfix_AddRealTimePVPHonor;
    private static DelegateBridge __Hotfix_GetRealTimePVPHonor;
    private static DelegateBridge __Hotfix_GetFriendshipPoints;
    private static DelegateBridge __Hotfix_GetSkinTickets;
    private static DelegateBridge __Hotfix_AddFriendshipPoints;
    private static DelegateBridge __Hotfix_AddSkinTickets;
    private static DelegateBridge __Hotfix_AddMemoryEssence;
    private static DelegateBridge __Hotfix_AddBrillianceMithralStone_1;
    private static DelegateBridge __Hotfix_AddMithralStone_1;
    private static DelegateBridge __Hotfix_AddGuildMedal;
    private static DelegateBridge __Hotfix_IsGuildMedalEnough;
    private static DelegateBridge __Hotfix_IsFriendshipPointsEnough;
    private static DelegateBridge __Hotfix_IsArenaHonourEnough;
    private static DelegateBridge __Hotfix_IsRealTimePVPHonorEnough;
    private static DelegateBridge __Hotfix_IsSkinTicketEnough;
    private static DelegateBridge __Hotfix_IsMemoryEssenceEnough;
    private static DelegateBridge __Hotfix_IsChallengePointEnough;
    private static DelegateBridge __Hotfix_IsFashionPointEnough;
    private static DelegateBridge __Hotfix_CanSetUserGuide;
    private static DelegateBridge __Hotfix_SetUserGuide;
    private static DelegateBridge __Hotfix_CleanUserGuide;
    private static DelegateBridge __Hotfix_CompleteAllUserGuides;
    private static DelegateBridge __Hotfix_IsUserGuideCompleted;
    private static DelegateBridge __Hotfix_GetLevel;
    private static DelegateBridge __Hotfix_GetRechargedCrystal;
    private static DelegateBridge __Hotfix_GetCrystal;
    private static DelegateBridge __Hotfix_CheckRankingListAddPlayerLevel;
    private static DelegateBridge __Hotfix_CreateRankingPlayerInfo;
    private static DelegateBridge __Hotfix_OnRankingListPlayerInfoChange;
    private static DelegateBridge __Hotfix_OpenGameRating;
    private static DelegateBridge __Hotfix_IsOpenGameRating;
    private static DelegateBridge __Hotfix_SetMemoryStoreOpenStatus;
    private static DelegateBridge __Hotfix_IsMemoryStoreOpen;
    private static DelegateBridge __Hotfix_get_Language;
    private static DelegateBridge __Hotfix_set_Language;
    private static DelegateBridge __Hotfix_CanGainDialogReward;
    private static DelegateBridge __Hotfix_GainDialogReward;
    private static DelegateBridge __Hotfix_GenerateDialogReward;
    private static DelegateBridge __Hotfix_GetLevelUpAddEnergyFromConfig;
    private static DelegateBridge __Hotfix_GetNextLevelExpFromConfig;
    private static DelegateBridge __Hotfix_CanSetHeadPortraitAndHeadFrame;
    private static DelegateBridge __Hotfix_SetHeadPortraitAndHeadFrame;
    private static DelegateBridge __Hotfix_CanSetHeadPortrait;
    private static DelegateBridge __Hotfix_SetHeadPortrait;
    private static DelegateBridge __Hotfix_get_Title;
    private static DelegateBridge __Hotfix_set_Title;
    private static DelegateBridge __Hotfix_HasTitle;
    private static DelegateBridge __Hotfix_CanSetHeadFrame;
    private static DelegateBridge __Hotfix_SetHeadFrame;
    private static DelegateBridge __Hotfix_OnHeadIconChange;
    private static DelegateBridge __Hotfix_CanChangePlayerName;
    private static DelegateBridge __Hotfix_ChangePlayerName;
    private static DelegateBridge __Hotfix_TryChangePlayerName;
    private static DelegateBridge __Hotfix_CheckPlayerName;
    private static DelegateBridge __Hotfix_DoShare;
    private static DelegateBridge __Hotfix_DoClientDecidedAchievement;
    private static DelegateBridge __Hotfix_add_LevelUpPlayerLevelEvent;
    private static DelegateBridge __Hotfix_remove_LevelUpPlayerLevelEvent;
    private static DelegateBridge __Hotfix_add_ConsumeEnergyMissionEvent;
    private static DelegateBridge __Hotfix_remove_ConsumeEnergyMissionEvent;
    private static DelegateBridge __Hotfix_add_ConsumeGoldMissionEvent;
    private static DelegateBridge __Hotfix_remove_ConsumeGoldMissionEvent;
    private static DelegateBridge __Hotfix_add_ConsumeCrystalMissionEvent;
    private static DelegateBridge __Hotfix_remove_ConsumeCrystalMissionEvent;
    private static DelegateBridge __Hotfix_add_LevelUpPlayerLevelMissionEvent;
    private static DelegateBridge __Hotfix_remove_LevelUpPlayerLevelMissionEvent;
    private static DelegateBridge __Hotfix_add_AddBuyEnergyMissionEvent;
    private static DelegateBridge __Hotfix_remove_AddBuyEnergyMissionEvent;
    private static DelegateBridge __Hotfix_add_PlayerActionFlushEvent;
    private static DelegateBridge __Hotfix_remove_PlayerActionFlushEvent;
    private static DelegateBridge __Hotfix_add_ConsumeFriendshipPointsEvent;
    private static DelegateBridge __Hotfix_remove_ConsumeFriendshipPointsEvent;
    private static DelegateBridge __Hotfix_add_SetHeadIconEvent;
    private static DelegateBridge __Hotfix_remove_SetHeadIconEvent;
    private static DelegateBridge __Hotfix_add_AddRechargeRMBEvent;
    private static DelegateBridge __Hotfix_remove_AddRechargeRMBEvent;
    private static DelegateBridge __Hotfix_add_DoShareEvent;
    private static DelegateBridge __Hotfix_remove_DoShareEvent;
    private static DelegateBridge __Hotfix_add_ClientDecidedAchievementEvent;
    private static DelegateBridge __Hotfix_remove_ClientDecidedAchievementEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerBasicInfoComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int BuyJetton(bool check = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnBuyJettonGoodsSettlement()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyJetton()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void TryUpdateSignedDays()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickTitleExpired()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGameFunctionOpened(GameFunctionType gameFunctionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCreateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetRefluxBeginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetCreateTimeUtc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLastLogoutTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLoginTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetPlayerName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMe(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentLevelExp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargedCsystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetRechargeRMB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeadIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDefaultHeadIcon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeadPortrait()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeadFrame()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanAddPlayerExp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddPlayerExp(int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnLevelChange(int upLevel, int addExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LevelUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddChallengePoint(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddFashionPoint(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddGold(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoldEnough(int useGoldCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoldOverFlow(int addNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGold()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddBrillianceMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMithralStoneEnough(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBrillianceMithralStoneEnough(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsCurrencyEnough(GoodsType currencyType, int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual long AddRechargeRMB(int nums, DateTime rechargeTime, bool needSync2Client = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddCrystal(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCrystalEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitEnergy(long secondPast)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachEnergyMax(long currentEnergy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanFlushEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanFlushPlayerAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void ResetPlayerActionNextFlushTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int IncreamentEnergy(
      long energyAddNums,
      bool canAboveMaxEnergy,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int DecreaseEnergy(int energyCost, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnergyEnough(int consumeEnergy = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual DateTime GetCurrentTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSigned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSignToday()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataBuyEnergyInfo GetBuyEnergyConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBoughtNumsUsedOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int BuyEnergy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanBuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataBuyArenaTicketInfo GetBuyArenaTicketConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetNextFlushPlayerActionTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FlushPlayerAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnPlayerActionFlushEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaTicketsEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaTicketsFull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddArenaTickets(
      int nums,
      bool arenaGiven = true,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAreanaTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddRechargedCrystal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddArenaHonour(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetArenaHonour()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddRealTimePVPHonor(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRealTimePVPHonor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFriendshipPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkinTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddFriendshipPoints(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddSkinTickets(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddMemoryEssence(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddBrillianceMithralStone(
      int nums,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddMithralStone(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int AddGuildMedal(int nums, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGuildMedalEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFriendshipPointsEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaHonourEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPHonorEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkinTicketEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryEssenceEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChallengePointEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFashionPointEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int SetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CompleteAllUserGuides()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUserGuideCompleted(int stepId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRechargedCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCrystal()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckRankingListAddPlayerLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingPlayerInfo CreateRankingPlayerInfo(int championHeroId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnRankingListPlayerInfoChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OpenGameRating()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpenGameRating()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMemoryStoreOpenStatus(bool open)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMemoryStoreOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Language
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateDialogReward(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelUpAddEnergyFromConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextLevelExpFromConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetHeadPortraitAndHeadFrame(int headPortraitId, int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetHeadPortrait(int headPortraitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetHeadPortrait(int headPortraitId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasTitle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSetHeadFrame(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SetHeadFrame(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnHeadIconChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int TryChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CheckPlayerName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoShare()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoClientDecidedAchievement(int param1)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> LevelUpPlayerLevelEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<GameFunctionType, int> ConsumeEnergyMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ConsumeGoldMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ConsumeCrystalMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> LevelUpPlayerLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> AddBuyEnergyMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action PlayerActionFlushEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ConsumeFriendshipPointsEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> SetHeadIconEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int, DateTime> AddRechargeRMBEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action DoShareEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> ClientDecidedAchievementEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
