﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionPlayerBasicInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.UtilityTools;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionPlayerBasicInfo : DataSection
  {
    public List<int> RewardedDialogIds;
    public int PlayerLevel;
    public int RechargeCrystal;
    public int Gold;
    public int Crystal;
    public BitArray GuideCompleteFlags;
    public DateTime EnergyFlushTime;
    public DateTime CreateTime;
    public DateTime CreateTimeUtc;
    public DateTime LogoutTime;
    public DateTime LoginTime;
    public DateTime LastSignTime;
    public DateTime RefluxBeginTime;
    public int BuyEngryNums;
    public int BuyArenaTicketsNums;
    public DateTime NextFlushPlayerActionTime;
    public int ArenaTickets;
    public int ArenaHonour;
    public int RealTimePVPHonor;
    public int FriendshipPoints;
    public bool OpenGameRating;
    public int SkinTickets;
    public int MemoryEssence;
    public int MithralStone;
    public int BrillianceMithralStone;
    public int GuildMedal;
    public int ChallengePoint;
    public int FashionPoint;
    public bool MemoryStoreOpen;
    public string Language;
    public int BuyJettonNums;
    public int Title;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SetRefluxBeginTime;
    private static DelegateBridge __Hotfix_SetRefluxEndTime;
    private static DelegateBridge __Hotfix_SetPlayerExp;
    private static DelegateBridge __Hotfix_ChangePlayerName;
    private static DelegateBridge __Hotfix_PlayerLevelUp;
    private static DelegateBridge __Hotfix_AddRechargedCrystal;
    private static DelegateBridge __Hotfix_AddRechargeRMB;
    private static DelegateBridge __Hotfix_AddGold;
    private static DelegateBridge __Hotfix_AddCrystal;
    private static DelegateBridge __Hotfix_SetUserGuide;
    private static DelegateBridge __Hotfix_CleanUserGuide;
    private static DelegateBridge __Hotfix_IsUserGuideCompleted;
    private static DelegateBridge __Hotfix_SetEnergy;
    private static DelegateBridge __Hotfix_InitSignDays;
    private static DelegateBridge __Hotfix_ResetSignDays;
    private static DelegateBridge __Hotfix_SetSignDays;
    private static DelegateBridge __Hotfix_AddSignDays;
    private static DelegateBridge __Hotfix_SetLastSignTime;
    private static DelegateBridge __Hotfix_ResetBuyEngryNums;
    private static DelegateBridge __Hotfix_BuyEngry;
    private static DelegateBridge __Hotfix_BuyArenaTickets;
    private static DelegateBridge __Hotfix_ResetBuyArenaTicketsNums;
    private static DelegateBridge __Hotfix_SetNextPlayerActionFlushTime;
    private static DelegateBridge __Hotfix_SetArenaTickets;
    private static DelegateBridge __Hotfix_AddArenaTickets;
    private static DelegateBridge __Hotfix_AddMemoryEssence;
    private static DelegateBridge __Hotfix_SetMemoryEssence;
    private static DelegateBridge __Hotfix_AddBrillianceMithralStone;
    private static DelegateBridge __Hotfix_AddMithralStone;
    private static DelegateBridge __Hotfix_AddGuildMedal;
    private static DelegateBridge __Hotfix_AddArenaHonour;
    private static DelegateBridge __Hotfix_AddRealTimePVPHonor;
    private static DelegateBridge __Hotfix_AddFriendshipPoints;
    private static DelegateBridge __Hotfix_AddChallengePoint;
    private static DelegateBridge __Hotfix_AddFashionPoint;
    private static DelegateBridge __Hotfix_SetEnergyFlushTime;
    private static DelegateBridge __Hotfix_SetHeadIcon;
    private static DelegateBridge __Hotfix_AddSkinTickets;
    private static DelegateBridge __Hotfix_SetMemoryStoreOpenStatus;
    private static DelegateBridge __Hotfix_SetLanguage;
    private static DelegateBridge __Hotfix_BuyJetton;
    private static DelegateBridge __Hotfix_ResetBuyJettonNums;
    private static DelegateBridge __Hotfix_SetTitle;
    private static DelegateBridge __Hotfix_IsDialogRewarded;
    private static DelegateBridge __Hotfix_AddRewardedDialog;
    private static DelegateBridge __Hotfix_get_UserId;
    private static DelegateBridge __Hotfix_set_UserId;
    private static DelegateBridge __Hotfix_get_PlayerName;
    private static DelegateBridge __Hotfix_set_PlayerName;
    private static DelegateBridge __Hotfix_get_RechargeRMB;
    private static DelegateBridge __Hotfix_set_RechargeRMB;
    private static DelegateBridge __Hotfix_get_Energy;
    private static DelegateBridge __Hotfix_set_Energy;
    private static DelegateBridge __Hotfix_get_Exp;
    private static DelegateBridge __Hotfix_set_Exp;
    private static DelegateBridge __Hotfix_get_SignDays;
    private static DelegateBridge __Hotfix_set_SignDays;
    private static DelegateBridge __Hotfix_get_HeadIcon;
    private static DelegateBridge __Hotfix_set_HeadIcon;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionPlayerBasicInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRefluxBeginTime(DateTime refluxBeginTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRefluxEndTime(DateTime refluxEndTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPlayerExp(int currentExp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangePlayerName(string newName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerLevelUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddRechargedCrystal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long AddRechargeRMB(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddGold(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddCrystal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanUserGuide(List<int> completeStepIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsUserGuideCompleted(int stepId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnergy(int energy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSignDays(int signDays)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetSignDays()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSignDays(int days)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSignDays()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastSignTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBuyEngryNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyEngry()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBuyArenaTicketsNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetNextPlayerActionFlushTime(DateTime setTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaTickets(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaTickets(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddMemoryEssence(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMemoryEssence(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddBrillianceMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddMithralStone(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddGuildMedal(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddArenaHonour(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddRealTimePVPHonor(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddFriendshipPoints(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddChallengePoint(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddFashionPoint(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnergyFlushTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeadIcon(int headIcon)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddSkinTickets(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMemoryStoreOpenStatus(bool open)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLanguage(string language)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuyJetton(int nums = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetBuyJettonNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTitle(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDialogRewarded(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRewardedDialog(int dialogId)
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string PlayerName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long RechargeRMB
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Energy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Exp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SignDays
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
