﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CollectionEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CollectionEvent
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_CollectionInfoEventId;
    private static DelegateBridge __Hotfix_set_CollectionInfoEventId;
    private static DelegateBridge __Hotfix_get_WayPointId;
    private static DelegateBridge __Hotfix_set_WayPointId;
    private static DelegateBridge __Hotfix_get_CompleteTimes;
    private static DelegateBridge __Hotfix_set_CompleteTimes;
    private static DelegateBridge __Hotfix_get_ExpiredTime;
    private static DelegateBridge __Hotfix_set_ExpiredTime;
    private static DelegateBridge __Hotfix_CollectionEventToPBCollectionEvent;
    private static DelegateBridge __Hotfix_CollectionEventsToPBCollectiopnEvents;
    private static DelegateBridge __Hotfix_PBCollectionEventToCollectionEvent;
    private static DelegateBridge __Hotfix_PBCollectionEventsToCollectionEvents;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CollectionInfoEventId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int WayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CompleteTimes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long ExpiredTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProCollectionEvent CollectionEventToPBCollectionEvent(
      CollectionEvent collectionEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProCollectionEvent> CollectionEventsToPBCollectiopnEvents(
      List<CollectionEvent> Events)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static CollectionEvent PBCollectionEventToCollectionEvent(
      ProCollectionEvent pbEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<CollectionEvent> PBCollectionEventsToCollectionEvents(
      List<ProCollectionEvent> pbEvents)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
