﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.IBPStageRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public interface IBPStageRule
  {
    void OnBan(int playerIndex, BPStageCommand command);

    void OnPick(int playerIndex, BPStageCommand command);

    void OnTick(long deltaTicks);

    void OnGiveUp(int playerIndex);

    void OnStart();

    int CanExecuteCommand(int playerIndex, BPStageCommand command);

    bool IsFinished();
  }
}
