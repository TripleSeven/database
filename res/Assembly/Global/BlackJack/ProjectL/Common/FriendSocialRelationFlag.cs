﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FriendSocialRelationFlag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum FriendSocialRelationFlag
  {
    All = -1,
    None = 0,
    Friend = 1,
    Blacklist = 2,
    Invite = 4,
    Invited = 8,
    RecentContactsChat = 16, // 0x00000010
    RecentContactsTeamBattle = 32, // 0x00000020
  }
}
