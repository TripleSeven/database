﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RafflePool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RafflePool
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RafflePools2PbActivityRafflePools;
    private static DelegateBridge __Hotfix_RafflePool2PbRafflePool;
    private static DelegateBridge __Hotfix_PBRafflePoolToRafflePool;
    private static DelegateBridge __Hotfix_PBRafflePoolsToRafflePools;
    private static DelegateBridge __Hotfix_Drawed;
    private static DelegateBridge __Hotfix_get_ActivityInstanceId;
    private static DelegateBridge __Hotfix_set_ActivityInstanceId;
    private static DelegateBridge __Hotfix_get_PoolId;
    private static DelegateBridge __Hotfix_set_PoolId;
    private static DelegateBridge __Hotfix_get_DrawedRaffleIds;
    private static DelegateBridge __Hotfix_set_DrawedRaffleIds;
    private static DelegateBridge __Hotfix_get_DrawedCount;
    private static DelegateBridge __Hotfix_set_DrawedCount;
    private static DelegateBridge __Hotfix_get_FreeDrawedCount;
    private static DelegateBridge __Hotfix_set_FreeDrawedCount;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_set_Config;

    [MethodImpl((MethodImplOptions) 32768)]
    public RafflePool(int poolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRafflePool> RafflePools2PbActivityRafflePools(
      List<RafflePool> rafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static ProRafflePool RafflePool2PbRafflePool(RafflePool rafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RafflePool PBRafflePoolToRafflePool(ProRafflePool pbRafflePool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RafflePool> PBRafflePoolsToRafflePools(
      List<ProRafflePool> pbRafflePools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Drawed(int darwedRaffleId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong ActivityInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PoolId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> DrawedRaffleIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DrawedCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FreeDrawedCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRafflePoolInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
