﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaBetInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PeakArenaBetInfo
  {
    public List<PeakArenaMatchupBet> Bets;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_SeasonId;
    private static DelegateBridge __Hotfix_set_SeasonId;
    private static DelegateBridge __Hotfix_get_TodayBetJettonNums;
    private static DelegateBridge __Hotfix_set_TodayBetJettonNums;
    private static DelegateBridge __Hotfix_FromPB;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_NeedSettleBet;
    private static DelegateBridge __Hotfix_NeedDailyFlush;
    private static DelegateBridge __Hotfix_BetDailyFlush;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_AddBet;
    private static DelegateBridge __Hotfix_SettlePeakArenaMatchupBetReward;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBetInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int SeasonId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TodayBetJettonNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBetInfo FromPB(ProPeakArenaBetInfo pbBetInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBetInfo ToPB(PeakArenaBetInfo betInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedSettleBet()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedDailyFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BetDailyFlush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBet(int matchupId, string userId, int jettonNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SettlePeakArenaMatchupBetReward(int matchupId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
