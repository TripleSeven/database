﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleReportLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Battle;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleReportLog
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_UserId;
    private static DelegateBridge __Hotfix_set_UserId;
    private static DelegateBridge __Hotfix_get_PlayerName;
    private static DelegateBridge __Hotfix_set_PlayerName;
    private static DelegateBridge __Hotfix_get_Type;
    private static DelegateBridge __Hotfix_set_Type;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_MonsterLevel;
    private static DelegateBridge __Hotfix_set_MonsterLevel;
    private static DelegateBridge __Hotfix_get_RandomNumberSeed;
    private static DelegateBridge __Hotfix_set_RandomNumberSeed;
    private static DelegateBridge __Hotfix_get_ArmyRandomNumberSeed;
    private static DelegateBridge __Hotfix_set_ArmyRandomNumberSeed;
    private static DelegateBridge __Hotfix_get_MineTeam;
    private static DelegateBridge __Hotfix_set_MineTeam;
    private static DelegateBridge __Hotfix_get_OpponentTeam;
    private static DelegateBridge __Hotfix_set_OpponentTeam;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_set_Players;
    private static DelegateBridge __Hotfix_get_Commands;
    private static DelegateBridge __Hotfix_set_Commands;
    private static DelegateBridge __Hotfix_get_CheckSums;
    private static DelegateBridge __Hotfix_set_CheckSums;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleReportLog()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string PlayerName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleType Type
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MonsterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RandomNumberSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArmyRandomNumberSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleActorSetup> MineTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleActorSetup> OpponentTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattlePlayer> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleCommand> Commands
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> CheckSums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
