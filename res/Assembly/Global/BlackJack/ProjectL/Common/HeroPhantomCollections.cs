﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroPhantomCollections
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroPhantomCollections
  {
    public CurrentTime Now;
    public List<HeroPhantom> HeroPhantoms;
    private IConfigDataLoader _ConfigDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_GetHeroPhantom;
    private static DelegateBridge __Hotfix_GetHeroPhantomLevel;
    private static DelegateBridge __Hotfix_ReloadConfigData;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomCollections()
    {
      // ISSUE: unable to decompile the method.
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantom GetHeroPhantom(int HeroPhantomId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroPhantomLevel GetHeroPhantomLevel(int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
