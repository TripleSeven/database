﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionLevel : DataSection
  {
    private int m_currentWayPointId;
    private int m_lastFinishedScenarioId;
    public List<RandomEvent> RandomEvents;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_SetScenario;
    private static DelegateBridge __Hotfix_InitScenario;
    private static DelegateBridge __Hotfix_SetCurrentWayPoint;
    private static DelegateBridge __Hotfix_InitCurrentWayPoint;
    private static DelegateBridge __Hotfix_SetArrivedWayPointId;
    private static DelegateBridge __Hotfix_SetRandomEvent;
    private static DelegateBridge __Hotfix_CleanWayPointEvent;
    private static DelegateBridge __Hotfix_SetWayPointStatus;
    private static DelegateBridge __Hotfix_IsSetRandomEvent;
    private static DelegateBridge __Hotfix_IsExistRandomEvent;
    private static DelegateBridge __Hotfix_AddCanMoveWayPointId;
    private static DelegateBridge __Hotfix_AddArrivedWayPoint;
    private static DelegateBridge __Hotfix_IsWayPointArrived;
    private static DelegateBridge __Hotfix_GetWayPointStatus;
    private static DelegateBridge __Hotfix_GetNotEventWayPoints;
    private static DelegateBridge __Hotfix_GetArrivedWayPoints;
    private static DelegateBridge __Hotfix_CanExpandWayPoint;
    private static DelegateBridge __Hotfix_CanMoveWayPoint;
    private static DelegateBridge __Hotfix_set_CurrentWayPointId;
    private static DelegateBridge __Hotfix_get_CurrentWayPointId;
    private static DelegateBridge __Hotfix_set_LastFinishedScenarioId;
    private static DelegateBridge __Hotfix_get_LastFinishedScenarioId;
    private static DelegateBridge __Hotfix_InitRandomEvents;
    private static DelegateBridge __Hotfix_SetRandomEvents;
    private static DelegateBridge __Hotfix_AddRandomEvent;
    private static DelegateBridge __Hotfix_AddDeadLives;
    private static DelegateBridge __Hotfix_RemoveRandomEvent;
    private static DelegateBridge __Hotfix_AddFinishedEventId;
    private static DelegateBridge __Hotfix_IsEventFinished;
    private static DelegateBridge __Hotfix_InitFinishedEventIds;
    private static DelegateBridge __Hotfix_SortByExpiredTimeAscend;
    private static DelegateBridge __Hotfix_ClearRandomEvents;
    private static DelegateBridge __Hotfix_GetRandomEvent;
    private static DelegateBridge __Hotfix_get_CanMoveWayPointIds;
    private static DelegateBridge __Hotfix_set_CanMoveWayPointIds;
    private static DelegateBridge __Hotfix_get_FinishedEventIds;
    private static DelegateBridge __Hotfix_set_FinishedEventIds;
    private static DelegateBridge __Hotfix_UpdateScenarioInfo;
    private static DelegateBridge __Hotfix_UpdateCurrentWaypointInfo;
    private static DelegateBridge __Hotfix_set_LastFinishedScenarioInfo;
    private static DelegateBridge __Hotfix_get_LastFinishedScenarioInfo;
    private static DelegateBridge __Hotfix_set_CurrentWaypointInfo;
    private static DelegateBridge __Hotfix_get_CurrentWaypointInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScenario(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitScenario(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitCurrentWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArrivedWayPointId(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CleanWayPointEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWayPointStatus(int wayPointId, WayPointStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCanMoveWayPointId(int wayPointId, WayPointStatus eventStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArrivedWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWayPointArrived(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWayPointStatus(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetNotEventWayPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetArrivedWayPoints()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanExpandWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanMoveWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int CurrentWayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastFinishedScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitRandomEvents(List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomEvents(List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDeadLives(RandomEvent randomEvent, int deadLives)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFinishedEventId(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventFinished(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedEventIds(List<int> eventIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortByExpiredTimeAscend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRandomEvents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent GetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, WayPointStatus> CanMoveWayPointIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> FinishedEventIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateScenarioInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateCurrentWaypointInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataScenarioInfo LastFinishedScenarioInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataWaypointInfo CurrentWaypointInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
