﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BagItemFactory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BagItemFactory
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_CreateBagItemInstanceId;
    private static DelegateBridge __Hotfix_CreateBagItem;
    private static DelegateBridge __Hotfix_CreateInstanceBagItemByServer;
    private static DelegateBridge __Hotfix_CreateInstanceBagItemByClient;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemFactory()
    {
      // ISSUE: unable to decompile the method.
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ulong CreateBagItemInstanceId(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase CreateBagItem(GoodsType goodsTypeId, int contentId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase CreateInstanceBagItemByServer(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase CreateInstanceBagItemByClient(ProGoods pbGoods)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
