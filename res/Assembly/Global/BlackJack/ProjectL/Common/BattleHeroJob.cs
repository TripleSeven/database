﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleHeroJob
  {
    private int m_jobRelatedId;
    public List<HeroJobRefineryProperty> RefineryProperties;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_set_JobRelatedId;
    private static DelegateBridge __Hotfix_get_JobRelatedId;
    private static DelegateBridge __Hotfix_get_JobLevel;
    private static DelegateBridge __Hotfix_set_JobLevel;
    private static DelegateBridge __Hotfix_BattleHeroJobToHeroJob;
    private static DelegateBridge __Hotfix_HeroJobToBattleHeroJob;
    private static DelegateBridge __Hotfix_PBBattleHeroJobToBattleHeroJob;
    private static DelegateBridge __Hotfix_BattleHeroJobToPBBattleHeroJob;
    private static DelegateBridge __Hotfix_UpdateJobConnectionInfo;
    private static DelegateBridge __Hotfix_set_JobConnectionInfo;
    private static DelegateBridge __Hotfix_get_JobConnectionInfo;
    private static DelegateBridge __Hotfix_IsLevelMax_0;
    private static DelegateBridge __Hotfix_IsLevelMax_1;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroJob()
    {
      // ISSUE: unable to decompile the method.
    }

    public int JobRelatedId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroJob BattleHeroJobToHeroJob(BattleHeroJob battleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroJob HeroJobToBattleHeroJob(HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroJob PBBattleHeroJobToBattleHeroJob(
      ProBattleHeroJob pbBattleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroJob BattleHeroJobToPBBattleHeroJob(
      BattleHeroJob battleHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelMax(int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
