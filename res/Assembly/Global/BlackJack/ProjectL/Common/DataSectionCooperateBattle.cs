﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionCooperateBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionCooperateBattle : DataSection
  {
    public CooperateBattleCollections BattleCollections;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_UpdateInstantiatedData;
    private static DelegateBridge __Hotfix_GetCooperateBattle;
    private static DelegateBridge __Hotfix_GetCooperateBattleLevel;
    private static DelegateBridge __Hotfix_GetFirstCooperateBattleLevel;
    private static DelegateBridge __Hotfix_SetChallengedNums;
    private static DelegateBridge __Hotfix_IncreaseChallengedNums;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionCooperateBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(DSCooperateBattleNtf DS)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattle GetCooperateBattle(int BattleID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleLevel GetCooperateBattleLevel(
      int BattleId,
      int LevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleLevel GetFirstCooperateBattleLevel(
      int LevelId,
      bool MustBeAvailable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChallengedNums(int Nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseChallengedNums(int BattleId, int Nums)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
