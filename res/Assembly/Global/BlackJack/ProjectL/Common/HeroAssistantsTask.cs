﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAssistantsTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroAssistantsTask
  {
    public HeroAssistants WhichHeroAssistants;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ConfigId;
    private static DelegateBridge __Hotfix_set_ConfigId;
    private static DelegateBridge __Hotfix_get_Weekday;
    private static DelegateBridge __Hotfix_get_RecommendHeroIds;
    private static DelegateBridge __Hotfix_get_SoldierTypes;
    private static DelegateBridge __Hotfix_get_CompletePoints;
    private static DelegateBridge __Hotfix_get_RequiredUserLevel;
    private static DelegateBridge __Hotfix_get_RewardCompleteRate;
    private static DelegateBridge __Hotfix_get_RewardDropId;
    private static DelegateBridge __Hotfix_get_Rewards;
    private static DelegateBridge __Hotfix_get_RewardWorkSeconds;
    private static DelegateBridge __Hotfix_get_RewardDropCount;
    private static DelegateBridge __Hotfix_get_RecommendHeroMultiply;
    private static DelegateBridge __Hotfix_get_RecommendHeroAdd;
    private static DelegateBridge __Hotfix_get_Locked;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_GetHeroAssignPoints_0;
    private static DelegateBridge __Hotfix_GetHeroAssignPoints_3;
    private static DelegateBridge __Hotfix_GetHeroAssignPoints_1;
    private static DelegateBridge __Hotfix_GetHeroAssignPoints_2;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantsTask()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ConfigId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Weekday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RecommendHeroIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> SoldierTypes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CompletePoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RequiredUserLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RewardCompleteRate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RewardDropId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<CompleteValueDropID> Rewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RewardWorkSeconds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<int> RewardDropCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RecommendHeroMultiply
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RecommendHeroAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Locked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroAssistantTaskInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(Hero H)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(int HeroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(List<Hero> Heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAssignPoints(List<int> HeroIds)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
