﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAssistantsTaskAssignment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroAssistantsTaskAssignment
  {
    public int TaskId;
    public HeroAssistantsTask Task;
    public List<int> AssignedHeroIds;
    public DateTime StartTime;
    public DateTime EndTime;
    public int Slot;
    public int? _CompleteRate;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_AssignPoints;
    private static DelegateBridge __Hotfix_get_CompleteRate;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAssistantsTaskAssignment()
    {
      // ISSUE: unable to decompile the method.
    }

    public int AssignPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CompleteRate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
