﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroDungeonComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroDungeonComponentCommon : IComponentBase
  {
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected BattleComponentCommon m_battle;
    protected BagComponentCommon m_bag;
    protected DataSectionHeroDungeon m_heroDungeonDS;
    protected IConfigDataLoader m_configDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFlushLevelChallengeNumsEvent;
    private static DelegateBridge __Hotfix_HasGotAchievementRelationId;
    private static DelegateBridge __Hotfix_IsLevelFirstPass;
    private static DelegateBridge __Hotfix_IsFinishedLevel;
    private static DelegateBridge __Hotfix_IsEnoughAttackNums;
    private static DelegateBridge __Hotfix_GetLevelCanChallengeMaxNums;
    private static DelegateBridge __Hotfix_GetDailyChallengeMaxNums;
    private static DelegateBridge __Hotfix_InitLevel;
    private static DelegateBridge __Hotfix_SetLevel;
    private static DelegateBridge __Hotfix_FindLevel;
    private static DelegateBridge __Hotfix_AttackHeroDungeonLevel;
    private static DelegateBridge __Hotfix_SetSuccessHeroDungeonLevel;
    private static DelegateBridge __Hotfix_SetRaidSuccessHeroDungeonLevel;
    private static DelegateBridge __Hotfix_CanAttackHeroDungeonLevel;
    private static DelegateBridge __Hotfix_CanRaidHeroDungeonLevel;
    private static DelegateBridge __Hotfix_CanAttackLevelByEnergyAndSoOn;
    private static DelegateBridge __Hotfix_CanAttackLevelByRewards;
    private static DelegateBridge __Hotfix_CanUnlockHeroDungeonLevel;
    private static DelegateBridge __Hotfix_GetHeroDungeonChapterStar;
    private static DelegateBridge __Hotfix_HasGotChapterStarReward;
    private static DelegateBridge __Hotfix_CanGainHeroDungeonChapterStarRewards;
    private static DelegateBridge __Hotfix_GetHeroDungeonChapterStarRewards;
    private static DelegateBridge __Hotfix_GainHeroDungeonChapterStarRewards;
    private static DelegateBridge __Hotfix_GenerateHeroDungeonChapterStarRewards;
    private static DelegateBridge __Hotfix_get_HasRewardAddRelativeOperationalActivity;
    private static DelegateBridge __Hotfix_set_HasRewardAddRelativeOperationalActivity;
    private static DelegateBridge __Hotfix_get_OperationalActivityChanllengenumsAdd;
    private static DelegateBridge __Hotfix_set_OperationalActivityChanllengenumsAdd;
    private static DelegateBridge __Hotfix_add_CompleteHeroDungeonLevelMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteHeroDungeonLevelMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushLevelChallengeNumsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotAchievementRelationId(int achievementRelationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFirstPass(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinishedLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEnoughAttackNums(ConfigDataHeroDungeonLevelInfo levelInfo, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelCanChallengeMaxNums(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengeMaxNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLevel(int chapterId, int levelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetLevel(int chapterId, int LevelId, int stars, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroDungeonLevel FindLevel(int chapterId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetSuccessHeroDungeonLevel(
      ConfigDataHeroDungeonLevelInfo levelInfo,
      List<int> newGotAchievementRelationInds,
      int stars,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetRaidSuccessHeroDungeonLevel(ConfigDataHeroDungeonLevelInfo levelInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByRewards(ConfigDataHeroDungeonLevelInfo levelInfo, bool isRaid)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnlockHeroDungeonLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroDungeonChapterStar(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGainHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Goods> GetHeroDungeonChapterStarRewards(
      ConfigDataHeroInformationInfo chapterInfo,
      int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainHeroDungeonChapterStarRewards(int chapterId, int index, bool check = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void GenerateHeroDungeonChapterStarRewards(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasRewardAddRelativeOperationalActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OperationalActivityChanllengenumsAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BattleType, int, List<int>> CompleteHeroDungeonLevelMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
