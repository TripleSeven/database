﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.StatisticalDataType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum StatisticalDataType
  {
    CardPool1SelectCard = 1,
    CardPool2SelectCard = 2,
    RiftLevelSimpleLevel = 3,
    RiftLevelDiffcultLevel = 4,
    EventCompleteRandom = 5,
    AnikiGymLevel = 6,
    ThearchyTrial = 7,
    ArenaConsecutiveVictory = 8,
    CrystalComsume = 9,
    GoldConsume = 10, // 0x0000000A
    AccumulateLogin = 11, // 0x0000000B
    ArenaAttack = 12, // 0x0000000C
    EnergyConsume = 13, // 0x0000000D
    HeroExpItemUse = 14, // 0x0000000E
    EnergyBuy = 15, // 0x0000000F
    CardPool3SelectCard = 16, // 0x00000010
    ArenaFight = 17, // 0x00000011
    HeroDungeonLevel = 18, // 0x00000012
    TreasureMapLevel = 19, // 0x00000013
    MemoryCorridor = 20, // 0x00000014
    HeroTrainning = 21, // 0x00000015
    HeroPhantom = 22, // 0x00000016
    CooperateBattle = 23, // 0x00000017
    TeamBattle = 24, // 0x00000018
    InviteFriend = 25, // 0x00000019
    AssignHeroToTask = 26, // 0x0000001A
    BattlePractice = 27, // 0x0000001B
    EquipmentLevelup = 28, // 0x0000001C
    EventCompleteFixed = 29, // 0x0000001D
    RealTimeArenaAnyModeFight = 30, // 0x0000001E
    RealTimeArenaRelaxModeFight = 31, // 0x0000001F
    RealTimeArenaLadderModeFight = 32, // 0x00000020
    RealTimeArenaAnyModeSuccess = 33, // 0x00000021
    RealTimeArenaRelaxModeSuccess = 34, // 0x00000022
    RealTimeArenaLadderModeSuccess = 35, // 0x00000023
    RealTimeReachedMaxDan = 36, // 0x00000024
    BuyGiftStoreGoods = 37, // 0x00000025
    BuyRechargeStoreGoods = 38, // 0x00000026
    AnyCardPoolSelectCard = 39, // 0x00000027
    DoShare = 39, // 0x00000027
    EquipmentCardPoolSelectCard = 40, // 0x00000028
    HeroCardPoolSelectCard = 41, // 0x00000029
    FreeCardPoolSelectCard = 42, // 0x0000002A
    EternalShrine = 43, // 0x0000002B
    KillGoblin = 44, // 0x0000002C
    HeroAnthem = 45, // 0x0000002D
  }
}
