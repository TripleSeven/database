﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RankingPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RankingPlayerInfo
  {
    public SocialConnectionFlag SocialConnection;
    public List<RankingPlayerAncientCallBossInfo> AncientCallBossInfos;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_UserId;
    private static DelegateBridge __Hotfix_set_UserId;
    private static DelegateBridge __Hotfix_get_HeadIcon;
    private static DelegateBridge __Hotfix_set_HeadIcon;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_ChampionHeroId;
    private static DelegateBridge __Hotfix_set_ChampionHeroId;
    private static DelegateBridge __Hotfix_get_HeroActivatedJobId;
    private static DelegateBridge __Hotfix_set_HeroActivatedJobId;
    private static DelegateBridge __Hotfix_get_HeroActivatedJobLevel;
    private static DelegateBridge __Hotfix_set_HeroActivatedJobLevel;
    private static DelegateBridge __Hotfix_get_BossTotalDamage;
    private static DelegateBridge __Hotfix_set_BossTotalDamage;
    private static DelegateBridge __Hotfix_ClearAncientCallBossInfos;
    private static DelegateBridge __Hotfix_AddOrUpdateBossInfo;
    private static DelegateBridge __Hotfix_RemoveBossInfo;
    private static DelegateBridge __Hotfix_RankingPlayerToPBRankingPlayer;
    private static DelegateBridge __Hotfix_PBRankingPlayerToRankingPlayer;

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public string UserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChampionHeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroActivatedJobId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroActivatedJobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long BossTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAncientCallBossInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOrUpdateBossInfo(int bossId, List<int> teamList, int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveBossInfo(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRankingPlayerInfo RankingPlayerToPBRankingPlayer(
      RankingPlayerInfo playerInfo,
      int score,
      long damage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RankingPlayerInfo PBRankingPlayerToRankingPlayer(
      ProRankingPlayerInfo proPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
