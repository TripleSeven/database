﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomNumber
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RandomNumber
  {
    private RandomNumberState m_state;
    private const uint InitW = 521288629;
    private const uint InitZ = 362436069;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SetSeed;
    private static DelegateBridge __Hotfix_SetState;
    private static DelegateBridge __Hotfix_GetState;
    private static DelegateBridge __Hotfix_Next_0;
    private static DelegateBridge __Hotfix_Next_1;
    private static DelegateBridge __Hotfix_GetRandomNumberByAccumulateWeight;
    private static DelegateBridge __Hotfix_GetRandomNums_1;
    private static DelegateBridge __Hotfix_GetRandomNums_0;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSeed(int seed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetState(RandomNumberState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomNumberState GetState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Next()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Next(int min, int max)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRandomNumberByAccumulateWeight(Dictionary<int, int> weights)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetRandomNums(int begin, int end, int needToRandomNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetRandomNums(List<int> inputCollection, int nums)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
