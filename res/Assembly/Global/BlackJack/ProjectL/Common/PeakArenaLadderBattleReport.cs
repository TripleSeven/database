﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaLadderBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PeakArenaLadderBattleReport : BattleReportHead
  {
    public List<PeakArenaBattleReportPlayerSummaryInfo> PlayerSummaryInfos;
    public List<int> WinPlayerIndexes;
    public List<string> FirstHandUserIds;
    public List<PeakArenaBattleReportBattleInfo> BattleInfos;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_CreateTime;
    private static DelegateBridge __Hotfix_set_CreateTime;
    private static DelegateBridge __Hotfix_get_ForfeitUserId;
    private static DelegateBridge __Hotfix_set_ForfeitUserId;
    private static DelegateBridge __Hotfix_get_Mode;
    private static DelegateBridge __Hotfix_set_Mode;
    private static DelegateBridge __Hotfix_get_Round;
    private static DelegateBridge __Hotfix_set_Round;
    private static DelegateBridge __Hotfix_ToPb;
    private static DelegateBridge __Hotfix_ToPbSummary;
    private static DelegateBridge __Hotfix_GetPlayerSummaryInfoByBORound;
    private static DelegateBridge __Hotfix_GetWinPlayerSummaryInfo;
    private static DelegateBridge __Hotfix_GetBattleReportBoRoundCount;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaLadderBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime CreateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string ForfeitUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RealTimePVPMode Mode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Round
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ProCommonBattleReport ToPb()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override ProCommonBattleReport ToPbSummary()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo GetPlayerSummaryInfoByBORound(
      int boRound,
      int leftOrRight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportPlayerSummaryInfo GetWinPlayerSummaryInfo(
      int boRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleReportBoRoundCount()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
