﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CommonReportLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CommonReportLog
  {
    private static string RandomEventFunctionHeader = "###############RandomEventFunction";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RandomEventLevelZonesReportLog;
    private static DelegateBridge __Hotfix_RandomEventLevelZoneReportLog;
    private static DelegateBridge __Hotfix_Message_4;
    private static DelegateBridge __Hotfix_RandomEventsReportLog;
    private static DelegateBridge __Hotfix_RandomEventReportLog;
    private static DelegateBridge __Hotfix_IntListReportLog;
    private static DelegateBridge __Hotfix_InitDictionary;
    private static DelegateBridge __Hotfix_StringDictionaryReportLog;
    private static DelegateBridge __Hotfix_WayPointReportLog;
    private static DelegateBridge __Hotfix_BattleReport;
    private static DelegateBridge __Hotfix_Message_8;
    private static DelegateBridge __Hotfix_Message_0;
    private static DelegateBridge __Hotfix_Message_3;
    private static DelegateBridge __Hotfix_Message_5;
    private static DelegateBridge __Hotfix_Message_1;
    private static DelegateBridge __Hotfix_BattleRoomBattle;
    private static DelegateBridge __Hotfix_Message_9;
    private static DelegateBridge __Hotfix_Message_7;
    private static DelegateBridge __Hotfix_Message_2;
    private static DelegateBridge __Hotfix_Message_6;
    private static DelegateBridge __Hotfix_GoodsListReportLog;
    private static DelegateBridge __Hotfix_ProGoodsListReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public CommonReportLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventLevelZonesReportLog(
      List<RandomEventLevelZone> zones,
      string logHeader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventLevelZoneReportLog(RandomEventLevelZone zone, string zoneName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(RandomEventGroup group)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventsReportLog(List<RandomEvent> randomEvents, string optionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string RandomEventReportLog(RandomEvent randomEvent, string optionType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string IntListReportLog(List<int> list, string listName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string InitDictionary(Dictionary<int, int> dict, string dictName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string StringDictionaryReportLog(Dictionary<string, int> dict)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string WayPointReportLog(
      List<ConfigDataWaypointInfo> wayPointInfos,
      Dictionary<int, WayPointStatus> wayPointStates,
      string functionType,
      string optionType,
      string selectStandard = "NULL")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BattleReport(BattleReportLog report, string owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Message(List<BattleActorSetup> team, string teamName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(BattleActorSetup actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(CommonBattleProperty[] properties)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(List<BattlePlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(BattlePlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string BattleRoomBattle(BattleRoomBattleLog log, string owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Message(List<BattleCommand> commands, string ownerName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Message(BPStageCommand command, string ownerName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(BattleCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string Message(List<int> battleChecksums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GoodsListReportLog(List<Goods> goodsList, string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string ProGoodsListReport(List<ProGoods> pbGoodsList, string desc)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
