﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionMission : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitEverydayMissions;
    private static DelegateBridge __Hotfix_InitOneOffMissions;
    private static DelegateBridge __Hotfix_InitFinishedEverydayMissions;
    private static DelegateBridge __Hotfix_InitFinishedOneOffMissions;
    private static DelegateBridge __Hotfix_ResetEverydayMissions;
    private static DelegateBridge __Hotfix_AddOneOffMission;
    private static DelegateBridge __Hotfix_AddEverydayMission;
    private static DelegateBridge __Hotfix_FinishOneOffMission;
    private static DelegateBridge __Hotfix_FinishEverydayMission;
    private static DelegateBridge __Hotfix_AddMissionProcess;
    private static DelegateBridge __Hotfix_SetMissionProcess;
    private static DelegateBridge __Hotfix_RemoveEveryDayMission;
    private static DelegateBridge __Hotfix_GetExistMissionIds;
    private static DelegateBridge __Hotfix_IsProcessingMission;
    private static DelegateBridge __Hotfix_GetAllProcessingMissions;
    private static DelegateBridge __Hotfix_get_ProcessingEverydayMissions;
    private static DelegateBridge __Hotfix_set_ProcessingEverydayMissions;
    private static DelegateBridge __Hotfix_get_FinishedEverydayMissions;
    private static DelegateBridge __Hotfix_set_FinishedEverydayMissions;
    private static DelegateBridge __Hotfix_get_ProcessingOneOffMissions;
    private static DelegateBridge __Hotfix_set_ProcessingOneOffMissions;
    private static DelegateBridge __Hotfix_get_FinishedOneOffMissions;
    private static DelegateBridge __Hotfix_set_FinishedOneOffMissions;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitEverydayMissions(List<Mission> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitOneOffMissions(List<Mission> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedEverydayMissions(List<int> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitFinishedOneOffMissions(List<int> missions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetEverydayMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddOneOffMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEverydayMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishOneOffMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishEverydayMission(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMissionProcess(Mission mission, long count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMissionProcess(Mission mission, int process)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveEveryDayMission(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetExistMissionIds()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProcessingMission(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingMissions()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<Mission> ProcessingEverydayMissions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> FinishedEverydayMissions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<Mission> ProcessingOneOffMissions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> FinishedOneOffMissions
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
