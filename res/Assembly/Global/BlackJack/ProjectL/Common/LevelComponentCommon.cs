﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.LevelComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class LevelComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    protected DataSectionLevel m_levelDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_TryGenerateActivityEvent;
    private static DelegateBridge __Hotfix_IsWayPointArrived;
    private static DelegateBridge __Hotfix_OutPutRandomEventOperateLog;
    private static DelegateBridge __Hotfix_InitLevelInfo;
    private static DelegateBridge __Hotfix_SetArrivedWayPoint;
    private static DelegateBridge __Hotfix_ArriveWayPoint;
    private static DelegateBridge __Hotfix_HasFirstWayPointWithScenario;
    private static DelegateBridge __Hotfix_AddCanMovePublicWayPoint;
    private static DelegateBridge __Hotfix_SetFinishedScenario;
    private static DelegateBridge __Hotfix_GetLastFinishedScenarioId;
    private static DelegateBridge __Hotfix_IsScenarioCompleted;
    private static DelegateBridge __Hotfix_GetScenarioId;
    private static DelegateBridge __Hotfix_SetBattleWayPointSuccessful;
    private static DelegateBridge __Hotfix_IsExistRandomEvent;
    private static DelegateBridge __Hotfix_IsSetRandomEvent;
    private static DelegateBridge __Hotfix_MoveToWayPoint;
    private static DelegateBridge __Hotfix_HandleWayPoint;
    private static DelegateBridge __Hotfix_InitRandomEvents;
    private static DelegateBridge __Hotfix_PreInitRandomEvent;
    private static DelegateBridge __Hotfix_GetEventExpiredTime;
    private static DelegateBridge __Hotfix_GetEventLives;
    private static DelegateBridge __Hotfix_IsRandomEventTimeOut;
    private static DelegateBridge __Hotfix_IsRandomEventDead;
    private static DelegateBridge __Hotfix_RemoveRandomEvent;
    private static DelegateBridge __Hotfix_AddRandomEvent;
    private static DelegateBridge __Hotfix_CanUnLockScenario;
    private static DelegateBridge __Hotfix_HandleScenario;
    private static DelegateBridge __Hotfix_IsScenarioFinished_0;
    private static DelegateBridge __Hotfix_IsScenarioFinished_1;
    private static DelegateBridge __Hotfix_IsScenarioInWaypoint;
    private static DelegateBridge __Hotfix_GetRandomEvent;
    private static DelegateBridge __Hotfix_GetEventId;
    private static DelegateBridge __Hotfix_CheckGameFunctionOpenByGM;
    private static DelegateBridge __Hotfix_HandleEventWayPoint;
    private static DelegateBridge __Hotfix_OnEventComplete;
    private static DelegateBridge __Hotfix_HandleDialogEvent;
    private static DelegateBridge __Hotfix_HandleTresureEvent;
    private static DelegateBridge __Hotfix_GetEventRewards;
    private static DelegateBridge __Hotfix_AttackScenario;
    private static DelegateBridge __Hotfix_CanAttackScenario;
    private static DelegateBridge __Hotfix_HandleAttackWayPointEvent;
    private static DelegateBridge __Hotfix_AttackEventWayPoint;
    private static DelegateBridge __Hotfix_CanAttackEventWayPoint;
    private static DelegateBridge __Hotfix_CanMoveToWayPoint;
    private static DelegateBridge __Hotfix_CanMoveToWayPointExistRandomEvent;
    private static DelegateBridge __Hotfix_OnCompleteWayPointEvent;
    private static DelegateBridge __Hotfix_IsRegionOpen;
    private static DelegateBridge __Hotfix_OnOpenRegion;
    private static DelegateBridge __Hotfix_GetWayPointStatus;
    private static DelegateBridge __Hotfix_add_CompleteEventMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteEventMissionEvent;
    private static DelegateBridge __Hotfix_add_CompleteScenarioMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteScenarioMissionEvent;
    private static DelegateBridge __Hotfix_add_CompleteNewScenarioMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteNewScenarioMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void TryGenerateActivityEvent(OperationalActivityBase activity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWayPointArrived(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutRandomEventOperateLog(
      int eventId,
      RandomEventStatus status,
      List<Goods> rewards = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitLevelInfo(List<int> arrivedWayPoints)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetArrivedWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ArriveWayPoint(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool HasFirstWayPointWithScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCanMovePublicWayPoint(int newId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetFinishedScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLastFinishedScenarioId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioCompleted(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetScenarioId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void SetBattleWayPointSuccessful(
      ConfigDataWaypointInfo wayPointInfo,
      List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsExistRandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int MoveToWayPoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitRandomEvents(List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PreInitRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected long GetEventExpiredTime(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEventLives(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRandomEventTimeOut(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRandomEventDead(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void RemoveRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddRandomEvent(RandomEvent randomEvent, bool isActivityEvent = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanUnLockScenario(ConfigDataScenarioInfo scenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HandleScenario(int scenarioId, bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsScenarioFinished(ConfigDataScenarioInfo secenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioFinished(int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsScenarioInWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent GetRandomEvent(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEventId(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int CheckGameFunctionOpenByGM(GameFunctionType gameFunctionTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HandleEventWayPoint(ConfigDataWaypointInfo wayPointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventComplete(int wayPointId, RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleDialogEvent(
      ConfigDataWaypointInfo wayPointInfo,
      List<Goods> itemRewards,
      int expReward,
      int goldReward,
      int energyCost,
      int scenarioId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual int HandleTresureEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual List<Goods> GetEventRewards(ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AttackScenario(
      ConfigDataScenarioInfo secenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackScenario(
      ConfigDataScenarioInfo scenarioInfo,
      bool scenarioFinished,
      bool checkBag = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int HandleAttackWayPointEvent(int wayPointId, ConfigDataEventInfo eventInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int AttackEventWayPoint(int wayPointId, ConfigDataEventInfo eventyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackEventWayPoint(int wayPointdId, ConfigDataEventInfo eventyInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanMoveToWayPoint(int destWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanMoveToWayPointExistRandomEvent(int destWayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCompleteWayPointEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRegionOpen(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnOpenRegion(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetWayPointStatus(int wayPointId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<bool> CompleteEventMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteScenarioMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> CompleteNewScenarioMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
