﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ComponenetManager`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ComponenetManager<ComponentType> where ComponentType : class, IComponentBase
  {
    private Dictionary<string, ComponentType> m_components;
    private Dictionary<Type, string> m_type2Name;
    private IComponentOwner m_owner;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Add;
    private static DelegateBridge __Hotfix_Remove;
    private static DelegateBridge __Hotfix_GetComponent_0;
    private static DelegateBridge __Hotfix_GetComponent_1;
    private static DelegateBridge __Hotfix_GetAllcomponents;
    private static DelegateBridge __Hotfix_SerializeComponents;
    private static DelegateBridge __Hotfix_DeSerializeComponents;
    private static DelegateBridge __Hotfix_PostDeSerializeComponents;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_PostInitComponents;
    private static DelegateBridge __Hotfix_RemoveComponents;
    private static DelegateBridge __Hotfix_OnRemove;

    [MethodImpl((MethodImplOptions) 32768)]
    public ComponenetManager(IComponentOwner owner)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Add<T>() where T : class, IComponentBase, new()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Remove<T>() where T : class, IComponentBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetComponent<T>() where T : class, IComponentBase
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ComponentType GetComponent(string componentName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ComponentType> GetAllcomponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SerializeComponents<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerializeComponents<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostDeSerializeComponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostInitComponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveComponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnRemove(IComponentBase component)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
