﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionHero : DataSection
  {
    private int m_heroInteractNums;
    private Dictionary<int, int> m_id2CacheIndex;
    private int m_protagonistId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_AddHeroFightNums;
    private static DelegateBridge __Hotfix_AddHero;
    private static DelegateBridge __Hotfix_InitHero;
    private static DelegateBridge __Hotfix_RemoveHero;
    private static DelegateBridge __Hotfix_ReplaceHero;
    private static DelegateBridge __Hotfix_FindCache;
    private static DelegateBridge __Hotfix_FindHeroIndex;
    private static DelegateBridge __Hotfix_RemoveAllHeros;
    private static DelegateBridge __Hotfix_FindHero;
    private static DelegateBridge __Hotfix_IsNeedAddAchievement;
    private static DelegateBridge __Hotfix_SetHeroDirty;
    private static DelegateBridge __Hotfix_AddHeroJob;
    private static DelegateBridge __Hotfix_FindHeroJob;
    private static DelegateBridge __Hotfix_UnlockHeroJob;
    private static DelegateBridge __Hotfix_AddHeroJobAchievements;
    private static DelegateBridge __Hotfix_ActiveHeroJob;
    private static DelegateBridge __Hotfix_SetHeroExp;
    private static DelegateBridge __Hotfix_AddHeroLevel;
    private static DelegateBridge __Hotfix_AddHeroStarLevel;
    private static DelegateBridge __Hotfix_SelectSkills;
    private static DelegateBridge __Hotfix_SelectSolider;
    private static DelegateBridge __Hotfix_AddSkill;
    private static DelegateBridge __Hotfix_AddSoldier;
    private static DelegateBridge __Hotfix_LevelUpHeroJobLevel;
    private static DelegateBridge __Hotfix_GetAllHeroes;
    private static DelegateBridge __Hotfix_InitProtagonist;
    private static DelegateBridge __Hotfix_SetProtagonist;
    private static DelegateBridge __Hotfix_SetHeroFavorabilityLevel;
    private static DelegateBridge __Hotfix_AddHeroFavorabilityLevel;
    private static DelegateBridge __Hotfix_SetHeroFavorabilityExp;
    private static DelegateBridge __Hotfix_UnlockHeroFetter;
    private static DelegateBridge __Hotfix_UnlockHeroHeartFetter;
    private static DelegateBridge __Hotfix_LevelUpHeroHeartFetter;
    private static DelegateBridge __Hotfix_SetHeroHeartFetterLevelMaxTime;
    private static DelegateBridge __Hotfix_ConfessHero;
    private static DelegateBridge __Hotfix_LevelUpHeroFetter;
    private static DelegateBridge __Hotfix_CanInteractHero;
    private static DelegateBridge __Hotfix_InitHeroInteractNums;
    private static DelegateBridge __Hotfix_InitAllHeroBattlePower;
    private static DelegateBridge __Hotfix_InitTopHeroBattlePower;
    private static DelegateBridge __Hotfix_InitTopFifteenHeroBattlePower;
    private static DelegateBridge __Hotfix_SetTopFifteenHero;
    private static DelegateBridge __Hotfix_InitChampionHeroBattlePower;
    private static DelegateBridge __Hotfix_SetHeroInteractNums;
    private static DelegateBridge __Hotfix_get_HeroInteractNums;
    private static DelegateBridge __Hotfix_SetHeroInteractNumsFlushTime;
    private static DelegateBridge __Hotfix_InitHeroInteractNumsFlushTime;
    private static DelegateBridge __Hotfix_BattlePowerCompare;
    private static DelegateBridge __Hotfix_OnHeroBattlePowerChange;
    private static DelegateBridge __Hotfix_get_HeroInteractNumsFlushTime;
    private static DelegateBridge __Hotfix_set_HeroInteractNumsFlushTime;
    private static DelegateBridge __Hotfix_get_GainHeroIds;
    private static DelegateBridge __Hotfix_set_GainHeroIds;
    private static DelegateBridge __Hotfix_get_Heroes;
    private static DelegateBridge __Hotfix_set_Heroes;
    private static DelegateBridge __Hotfix_get_ProtagonistID;
    private static DelegateBridge __Hotfix_get_AllHeroBattlePower;
    private static DelegateBridge __Hotfix_set_AllHeroBattlePower;
    private static DelegateBridge __Hotfix_get_AllHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_set_AllHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_get_LastAllHeroRank;
    private static DelegateBridge __Hotfix_set_LastAllHeroRank;
    private static DelegateBridge __Hotfix_get_TopHeroBattlePower;
    private static DelegateBridge __Hotfix_set_TopHeroBattlePower;
    private static DelegateBridge __Hotfix_get_TopHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_set_TopHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_get_LastTopHeroRank;
    private static DelegateBridge __Hotfix_set_LastTopHeroRank;
    private static DelegateBridge __Hotfix_get_TopFifteenHeroBattlePower;
    private static DelegateBridge __Hotfix_set_TopFifteenHeroBattlePower;
    private static DelegateBridge __Hotfix_get_TopFifteenHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_set_TopFifteenHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_get_LastTopFifteenHeroRank;
    private static DelegateBridge __Hotfix_set_LastTopFifteenHeroRank;
    private static DelegateBridge __Hotfix_get_ChampionHeroBattlePower;
    private static DelegateBridge __Hotfix_set_ChampionHeroBattlePower;
    private static DelegateBridge __Hotfix_get_ChampionHeroId;
    private static DelegateBridge __Hotfix_set_ChampionHeroId;
    private static DelegateBridge __Hotfix_get_ChampionHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_set_ChampionHeroBattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_get_LastChampionHeroRank;
    private static DelegateBridge __Hotfix_set_LastChampionHeroRank;
    private static DelegateBridge __Hotfix_get_LastHeroRankUpdateTime;
    private static DelegateBridge __Hotfix_set_LastHeroRankUpdateTime;
    private static DelegateBridge __Hotfix_get_TopBattlePowerThreshold;
    private static DelegateBridge __Hotfix_set_TopBattlePowerThreshold;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroFightNums(Hero hero, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero AddHero(int heroId, int starLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ReplaceHero(Hero oldHero, Hero newHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UpdateCache<Hero> FindCache(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindHeroIndex(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHeros()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero FindHero(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedAddAchievement(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroDirty(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob FindHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnlockHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroJobAchievements(HeroJob heroJob, List<int> achievements)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActiveHeroJob(Hero hero, int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroExp(Hero hero, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroLevel(Hero hero, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroStarLevel(Hero hero, int starLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SelectSkills(Hero hero, List<int> skills)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SelectSolider(Hero hero, int soliderId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSkill(Hero hero, int skillId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSoldier(Hero hero, int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelUpHeroJobLevel(Hero hero, HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Hero> GetAllHeroes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitProtagonist(int protagonist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProtagonist(int protagonistId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroFavorabilityLevel(Hero hero, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroFavorabilityLevel(Hero hero, int addLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroFavorabilityExp(Hero hero, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnlockHeroFetter(Hero hero, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnlockHeroHeartFetter(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelUpHeroHeartFetter(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroHeartFetterLevelMaxTime(Hero hero, DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ConfessHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LevelUpHeroFetter(Hero hero, int fetterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanInteractHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroInteractNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAllHeroBattlePower(
      int power,
      DateTime updateTime,
      int lastRank,
      DateTime lastUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTopHeroBattlePower(
      int power,
      DateTime updateTime,
      int powerThreshold,
      int lastRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTopFifteenHeroBattlePower(
      int power,
      DateTime updateTime,
      int powerThreshold,
      int lastRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTopFifteenHero(int power, DateTime updateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitChampionHeroBattlePower(
      int power,
      DateTime updateTime,
      int heroId,
      int lastRank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroInteractNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroInteractNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroInteractNumsFlushTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitHeroInteractNumsFlushTime(DateTime time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int BattlePowerCompare(Hero x, Hero y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnHeroBattlePowerChange(DateTime time, Hero hero = null)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime HeroInteractNumsFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> GainHeroIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HeroUpdateCache Heroes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ProtagonistID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int AllHeroBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime AllHeroBattlePowerUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastAllHeroRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TopHeroBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime TopHeroBattlePowerUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastTopHeroRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TopFifteenHeroBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime TopFifteenHeroBattlePowerUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastTopFifteenHeroRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChampionHeroBattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ChampionHeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime ChampionHeroBattlePowerUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastChampionHeroRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastHeroRankUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TopBattlePowerThreshold
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
