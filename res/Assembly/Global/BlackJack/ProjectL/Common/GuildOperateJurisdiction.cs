﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildOperateJurisdiction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public enum GuildOperateJurisdiction
  {
    UpdateApplyPlayerLevel = 1,
    SetAutojoin = 2,
    ManualAcceptJoinApply = 3,
    ClearJoinApply = 4,
    KickOutVicePresident = 5,
    KickNormalGuildMember = 6,
    AppointVicePresident = 7,
    AppointPresident = 8,
    RelievePresident = 9,
    SetGuildAnnouncement = 10, // 0x0000000A
    SetHiringDeclaration = 11, // 0x0000000B
    ChangeName = 12, // 0x0000000C
    BuyGuildGiftStoreGoods = 13, // 0x0000000D
    UpdateGuildActivity = 14, // 0x0000000E
    GenerateGuildActivities = 15, // 0x0000000F
    InviteToJoin = 16, // 0x00000010
  }
}
