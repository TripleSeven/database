﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionBag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionBag : DataSection
  {
    private Dictionary<ulong, int> m_instanceId2CacheIndex;
    private const int MaxBagNumsPerNtf = 1000;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeMultipleToClient;
    private static DelegateBridge __Hotfix_InitBagItem;
    private static DelegateBridge __Hotfix_AddBagItem;
    private static DelegateBridge __Hotfix_SetBagItemNums;
    private static DelegateBridge __Hotfix_RemoveBagItem;
    private static DelegateBridge __Hotfix_RemoveAllBagItems;
    private static DelegateBridge __Hotfix_FindBagItem_0;
    private static DelegateBridge __Hotfix_SetBagItemDirty;
    private static DelegateBridge __Hotfix_FindBagItem_1;
    private static DelegateBridge __Hotfix_FindBagItemIndex;
    private static DelegateBridge __Hotfix_FindCache;
    private static DelegateBridge __Hotfix_Size;
    private static DelegateBridge __Hotfix_GetAllBagItems;
    private static DelegateBridge __Hotfix_IterateAllBagItems;
    private static DelegateBridge __Hotfix_get_InstanceId2CacheIndex;
    private static DelegateBridge __Hotfix_get_Bag;
    private static DelegateBridge __Hotfix_set_Bag;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionBag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<object> SerializeMultipleToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBagItem(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBagItem(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase SetBagItemNums(BagItemBase bagItem, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase RemoveBagItem(BagItemBase bagItem, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItem(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBagItemDirty(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItem(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int FindBagItemIndex(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UpdateCache<BagItemBase> FindCache(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Size()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> GetAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<BagItemBase> IterateAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<ulong, int> InstanceId2CacheIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BagItemUpdateCache Bag
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
