﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GDBLinkInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [ProtoContract(Name = "GDBLinkInfo")]
  [Serializable]
  public class GDBLinkInfo : IExtensible
  {
    private int _FromStarfieldID;
    private int _FromStargroupID;
    private int _FromSolarSystemID;
    private int _FromStargateID;
    private int _ToStarfieldID;
    private int _ToStargroupID;
    private int _ToSolarSystemID;
    private int _ToStargateID;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_FromStarfieldID;
    private static DelegateBridge __Hotfix_set_FromStarfieldID;
    private static DelegateBridge __Hotfix_get_FromStargroupID;
    private static DelegateBridge __Hotfix_set_FromStargroupID;
    private static DelegateBridge __Hotfix_get_FromSolarSystemID;
    private static DelegateBridge __Hotfix_set_FromSolarSystemID;
    private static DelegateBridge __Hotfix_get_FromStargateID;
    private static DelegateBridge __Hotfix_set_FromStargateID;
    private static DelegateBridge __Hotfix_get_ToStarfieldID;
    private static DelegateBridge __Hotfix_set_ToStarfieldID;
    private static DelegateBridge __Hotfix_get_ToStargroupID;
    private static DelegateBridge __Hotfix_set_ToStargroupID;
    private static DelegateBridge __Hotfix_get_ToSolarSystemID;
    private static DelegateBridge __Hotfix_set_ToSolarSystemID;
    private static DelegateBridge __Hotfix_get_ToStargateID;
    private static DelegateBridge __Hotfix_set_ToStargateID;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public GDBLinkInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "FromStarfieldID")]
    [DefaultValue(0)]
    public int FromStarfieldID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "FromStargroupID")]
    public int FromStargroupID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FromSolarSystemID")]
    public int FromSolarSystemID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FromStargateID")]
    public int FromStargateID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ToStarfieldID")]
    [DefaultValue(0)]
    public int ToStarfieldID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ToStargroupID")]
    public int ToStargroupID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ToSolarSystemID")]
    public int ToSolarSystemID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ToStargateID")]
    public int ToStargateID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
