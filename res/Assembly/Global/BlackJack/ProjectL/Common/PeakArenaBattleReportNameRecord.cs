﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaBattleReportNameRecord
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PeakArenaBattleReportNameRecord
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_ToProtocol;
    private static DelegateBridge __Hotfix_ToProtocols;
    private static DelegateBridge __Hotfix_FromProtocol;
    private static DelegateBridge __Hotfix_FromProtocols;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaBattleReportNameRecord()
    {
      // ISSUE: unable to decompile the method.
    }

    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleReportNameRecord ToProtocol(
      PeakArenaBattleReportNameRecord record)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProPeakArenaBattleReportNameRecord> ToProtocols(
      List<PeakArenaBattleReportNameRecord> records)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBattleReportNameRecord FromProtocol(
      ProPeakArenaBattleReportNameRecord pbRecord)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaBattleReportNameRecord> FromProtocols(
      List<ProPeakArenaBattleReportNameRecord> pbRecords)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
