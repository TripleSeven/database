﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRechargeStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionRechargeStore : DataSection
  {
    private HashSet<int> m_boughtGoodsList;
    public Dictionary<int, DateTime> m_banBuyingGoodsList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitBoughtGoodsList;
    private static DelegateBridge __Hotfix_ClearBoughtGoodsList;
    private static DelegateBridge __Hotfix_IsGoodsBought;
    private static DelegateBridge __Hotfix_BuyGoods;
    private static DelegateBridge __Hotfix_GetBoughtGoodsList;
    private static DelegateBridge __Hotfix_IsGoodsOnBanBuyingPeriod;
    private static DelegateBridge __Hotfix_SetBanBuyingGoodsTime;
    private static DelegateBridge __Hotfix_InitmBanBuyingGoodsList;
    private static DelegateBridge __Hotfix_get_BanBuyingGoodsList;
    private static DelegateBridge __Hotfix_get_CurrentId;
    private static DelegateBridge __Hotfix_set_CurrentId;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRechargeStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBoughtGoodsList(List<int> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBoughtGoodsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsBought(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyGoods(int goodsId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetBoughtGoodsList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGoodsOnBanBuyingPeriod(int goodsId, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBanBuyingGoodsTime(int goodsId, DateTime expiredTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitmBanBuyingGoodsList(Dictionary<int, DateTime> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, DateTime> BanBuyingGoodsList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
