﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GlobalRankingListComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GlobalRankingListComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected RiftComponentCommon m_rift;
    protected HeroAnthemComponentCommon m_heroAnthem;
    protected HeroComponentCommon m_hero;
    protected AncientCallComponentCommon m_ancientCall;
    private DateTime[] LastRankingListQueryTime;
    private Dictionary<int, DateTime> AncientCallSingleBossQueryTimeDict;
    public Dictionary<int, DateTime> FriendAndGuildAncientCallSingleBossQueryTimeDict;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_CheckRankingListInfoQuery;
    private static DelegateBridge __Hotfix_CheckRankingListInfoQueryCare4AncientCall;
    private static DelegateBridge __Hotfix_UpdateLastRankingListQueryTime;
    private static DelegateBridge __Hotfix_GetRankingListQueryTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalRankingListComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckRankingListInfoQuery(RankingListType rankingListType, out int errCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckRankingListInfoQueryCare4AncientCall(
      RankingListType rankingListType,
      int bossId,
      out int errCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateLastRankingListQueryTime(
      RankingListType rankingListType,
      int bossId,
      DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetRankingListQueryTime(RankingListType rankingListType, int bossId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
