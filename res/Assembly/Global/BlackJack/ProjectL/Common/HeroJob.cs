﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroJob
  {
    private int m_jobRelatedId;
    public HashSet<int> Achievements;
    public List<HeroJobRefineryProperty> RefineryProperties;
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_set_JobRelatedId;
    private static DelegateBridge __Hotfix_get_JobRelatedId;
    private static DelegateBridge __Hotfix_get_JobLevel;
    private static DelegateBridge __Hotfix_set_JobLevel;
    private static DelegateBridge __Hotfix_get_ModelSkinId;
    private static DelegateBridge __Hotfix_set_ModelSkinId;
    private static DelegateBridge __Hotfix_get_OpenHeroJobRefinery;
    private static DelegateBridge __Hotfix_set_OpenHeroJobRefinery;
    private static DelegateBridge __Hotfix_FindHeroJobRefineryPropertyBySlotId;
    private static DelegateBridge __Hotfix_HeroJobToPBHeroJob;
    private static DelegateBridge __Hotfix_PbHeroJobToHeroJob;
    private static DelegateBridge __Hotfix_UpdateJobConnectionInfo;
    private static DelegateBridge __Hotfix_set_JobConnectionInfo;
    private static DelegateBridge __Hotfix_get_JobConnectionInfo;
    private static DelegateBridge __Hotfix_IsLevelMax_0;
    private static DelegateBridge __Hotfix_IsLevelMax_1;
    private static DelegateBridge __Hotfix_GetModelSkinResourceInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob(HeroJob other)
    {
      // ISSUE: unable to decompile the method.
    }

    public int JobRelatedId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ModelSkinId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool OpenHeroJobRefinery
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJobRefineryProperty FindHeroJobRefineryPropertyBySlotId(
      int slotId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroJob HeroJobToPBHeroJob(HeroJob heroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroJob PbHeroJobToHeroJob(ProHeroJob pbHeroJob)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateJobConnectionInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelMax(int jobLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetModelSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
