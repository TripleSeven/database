﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.IComponentOwner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Common
{
  public interface IComponentOwner
  {
    T AddOwnerComponent<T>() where T : class, IComponentBase, new();

    void RemoveOwnerComponent<T>() where T : class, IComponentBase;

    T GetOwnerComponent<T>() where T : class, IComponentBase;

    IComponentBase GetOwnerComponent(string componentName);
  }
}
