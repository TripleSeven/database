﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionResource : DataSection
  {
    public PlayerOutOfBagItem m_resource;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_GetAllMonthCards;
    private static DelegateBridge __Hotfix_FindMonthCardById;
    private static DelegateBridge __Hotfix_AddMonthCard;
    private static DelegateBridge __Hotfix_RemoveMonthCard;
    private static DelegateBridge __Hotfix_HasHeadFrameId;
    private static DelegateBridge __Hotfix_AddHeadFrame;
    private static DelegateBridge __Hotfix_AddTitle;
    private static DelegateBridge __Hotfix_RemoveTitle;
    private static DelegateBridge __Hotfix_HasOwn;
    private static DelegateBridge __Hotfix_AddHeroSkin;
    private static DelegateBridge __Hotfix_RemoveHeroSkin;
    private static DelegateBridge __Hotfix_RemoveAllHeroSkin;
    private static DelegateBridge __Hotfix_AddSoldierSkin;
    private static DelegateBridge __Hotfix_RemoveSoldierSkin;
    private static DelegateBridge __Hotfix_RemoveAllSoldierSkin;
    private static DelegateBridge __Hotfix_AddEquipmentId;
    private static DelegateBridge __Hotfix_InitResource;
    private static DelegateBridge __Hotfix_get_Resource;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> GetAllMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthCard FindMonthCardById(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMonthCard(int monthCardId, DateTime expiredTime, string goodsId = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveMonthCard(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasHeadFrameId(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddHeadFrame(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddTitle(int titleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveTitle(int titleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodtypeId, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroSkin(int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveHeroSkin(int heroSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllHeroSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSoldierSkin(int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveSoldierSkin(int soldierSkinId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllSoldierSkin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddEquipmentId(int equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitResource(PlayerOutOfBagItem resource)
    {
      // ISSUE: unable to decompile the method.
    }

    public PlayerOutOfBagItem Resource
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
