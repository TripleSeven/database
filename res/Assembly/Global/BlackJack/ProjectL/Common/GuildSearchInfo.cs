﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildSearchInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GuildSearchInfo
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_MemberCount;
    private static DelegateBridge __Hotfix_set_MemberCount;
    private static DelegateBridge __Hotfix_get_JoinLevel;
    private static DelegateBridge __Hotfix_set_JoinLevel;
    private static DelegateBridge __Hotfix_get_LastWeekActivities;
    private static DelegateBridge __Hotfix_set_LastWeekActivities;
    private static DelegateBridge __Hotfix_get_HiringDeclaration;
    private static DelegateBridge __Hotfix_set_HiringDeclaration;
    private static DelegateBridge __Hotfix_get_PresidentMemberInfo;
    private static DelegateBridge __Hotfix_set_PresidentMemberInfo;
    private static DelegateBridge __Hotfix_get_HaveSendJoinReq;
    private static DelegateBridge __Hotfix_set_HaveSendJoinReq;
    private static DelegateBridge __Hotfix_ToPBGuildSearchInfo;
    private static DelegateBridge __Hotfix_ToGuildSearchInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildSearchInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MemberCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JoinLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastWeekActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string HiringDeclaration
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GuildMember PresidentMemberInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HaveSendJoinReq
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildSearchInfo ToPBGuildSearchInfo(GuildSearchInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildSearchInfo ToGuildSearchInfo(ProGuildSearchInfo proInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
