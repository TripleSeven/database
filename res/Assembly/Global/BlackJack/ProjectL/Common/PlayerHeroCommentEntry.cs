﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PlayerHeroCommentEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class PlayerHeroCommentEntry
  {
    public List<ulong> CommentedEntryInstanceIds;
    public HashSet<ulong> PraisedEntryInstanceIds;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_set_HeroId;
    private static DelegateBridge __Hotfix_get_CommentedNums;
    private static DelegateBridge __Hotfix_set_CommentedNums;
    private static DelegateBridge __Hotfix_PlayerHeroCommentEntryToPBPlayerHeroCommentEntry;
    private static DelegateBridge __Hotfix_PBPlayerHeroCommentEntryToPlayerHeroCommentEntry;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerHeroCommentEntry()
    {
      // ISSUE: unable to decompile the method.
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CommentedNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPlayerHeroCommentEntry PlayerHeroCommentEntryToPBPlayerHeroCommentEntry(
      PlayerHeroCommentEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PlayerHeroCommentEntry PBPlayerHeroCommentEntryToPlayerHeroCommentEntry(
      ProPlayerHeroCommentEntry pbEntry)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
