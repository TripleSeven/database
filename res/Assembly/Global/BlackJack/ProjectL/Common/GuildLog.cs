﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GuildLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GuildLog
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ModeId;
    private static DelegateBridge __Hotfix_set_ModeId;
    private static DelegateBridge __Hotfix_get_Content;
    private static DelegateBridge __Hotfix_set_Content;
    private static DelegateBridge __Hotfix_get_SendTime;
    private static DelegateBridge __Hotfix_set_SendTime;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_ToPBs;
    private static DelegateBridge __Hotfix_FromPBs;
    private static DelegateBridge __Hotfix_FromPB;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildLog()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ModeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Content
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime SendTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGuildLog ToPB(GuildLog log)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProGuildLog> ToPBs(List<GuildLog> logs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<GuildLog> FromPBs(List<ProGuildLog> pbLogs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GuildLog FromPB(ProGuildLog pbLog)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
