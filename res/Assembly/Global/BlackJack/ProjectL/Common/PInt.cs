﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PInt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct PInt
  {
    private int m_Value;
    private int m_Check;

    [MethodImpl((MethodImplOptions) 32768)]
    public PInt(int v)
    {
      // ISSUE: unable to decompile the method.
    }

    public static implicit operator PInt(int value)
    {
      return new PInt(value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static implicit operator int(PInt p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    private static int Encode(int v)
    {
      return v ^ 1431655765;
    }

    private static int Decode(int v)
    {
      return v ^ 1431655765;
    }

    private static int Check(int v)
    {
      return v ^ 2004318071;
    }
  }
}
