﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BusinessCardInfoSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BusinessCardInfoSet
  {
    public List<BusinessCardHeroSet> Heroes;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Desc;
    private static DelegateBridge __Hotfix_set_Desc;
    private static DelegateBridge __Hotfix_get_IsActionRandom;
    private static DelegateBridge __Hotfix_set_IsActionRandom;
    private static DelegateBridge __Hotfix_ToProtocol;
    private static DelegateBridge __Hotfix_FromProtocol;

    [MethodImpl((MethodImplOptions) 32768)]
    public BusinessCardInfoSet()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Desc
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsActionRandom
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBusinessCardInfoSet ToProtocol(BusinessCardInfoSet set)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BusinessCardInfoSet FromProtocol(ProBusinessCardInfoSet pbSet)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
