﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ResourceComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ResourceComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionResource m_resourceDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BagComponentCommon m_bag;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_EffectValidMonthCard;
    private static DelegateBridge __Hotfix_GetAllValidMonthCards;
    private static DelegateBridge __Hotfix_AddMonthCard;
    private static DelegateBridge __Hotfix_RemoveMonthCard;
    private static DelegateBridge __Hotfix_FindMonthCardById;
    private static DelegateBridge __Hotfix_IsMonthCardVaild;
    private static DelegateBridge __Hotfix_OnMonthCardAdd;
    private static DelegateBridge __Hotfix_OnMonthCardValid;
    private static DelegateBridge __Hotfix_OnMonthCardInvalid;
    private static DelegateBridge __Hotfix_HasHeadFrameId;
    private static DelegateBridge __Hotfix_AddHeadFrame;
    private static DelegateBridge __Hotfix_AddTitle;
    private static DelegateBridge __Hotfix_AddHeroSkin;
    private static DelegateBridge __Hotfix_AddSoldierSkin;
    private static DelegateBridge __Hotfix_HasOwn;
    private static DelegateBridge __Hotfix_OnCreateBagItemEventCallBack;
    private static DelegateBridge __Hotfix_AddEquipmentId;
    private static DelegateBridge __Hotfix_add_MonthCardValidEvent;
    private static DelegateBridge __Hotfix_remove_MonthCardValidEvent;
    private static DelegateBridge __Hotfix_add_MonthCardAddEvent;
    private static DelegateBridge __Hotfix_remove_MonthCardAddEvent;
    private static DelegateBridge __Hotfix_add_MonthCardInvalidEvent;
    private static DelegateBridge __Hotfix_remove_MonthCardInvalidEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public ResourceComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void EffectValidMonthCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<MonthCard> GetAllValidMonthCards()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddMonthCard(int monthCardId, DateTime expiredTime, string goodsId = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveMonthCard(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MonthCard FindMonthCardById(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonthCardVaild(int cardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardAdd(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardValid(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnMonthCardInvalid(int monthCardId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasHeadFrameId(int headFrameId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddHeadFrame(int headFrameId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool AddTitle(int titileId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddHeroSkin(int heroSkinId, GameFunctionType causeId = GameFunctionType.GameFunctionType_None, string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AddSoldierSkin(
      int soldierSkinId,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwn(GoodsType goodtypeId, int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnCreateBagItemEventCallBack(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEquipmentId(int equipmentId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> MonthCardValidEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> MonthCardAddEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<int> MonthCardInvalidEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
