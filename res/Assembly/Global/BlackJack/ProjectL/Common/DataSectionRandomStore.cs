﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRandomStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionRandomStore : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_InitStore;
    private static DelegateBridge __Hotfix_AddStore;
    private static DelegateBridge __Hotfix_FindStore;
    private static DelegateBridge __Hotfix_SetStore;
    private static DelegateBridge __Hotfix_BuyStoreItem;
    private static DelegateBridge __Hotfix_GetManualFlushNums;
    private static DelegateBridge __Hotfix_SetStoreNextFlushTime;
    private static DelegateBridge __Hotfix_AddStoreItems;
    private static DelegateBridge __Hotfix_ClearStoreItems;
    private static DelegateBridge __Hotfix_GetStoreItem;
    private static DelegateBridge __Hotfix_SetManualFlushNums;
    private static DelegateBridge __Hotfix_AddManualFlushNums;
    private static DelegateBridge __Hotfix_get_Stores;
    private static DelegateBridge __Hotfix_set_Stores;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRandomStore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore FindStore(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BuyStoreItem(RandomStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetManualFlushNums(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStoreNextFlushTime(RandomStore store, DateTime flushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStoreItems(RandomStore store, List<RandomStoreItem> storeItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearStoreItems(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStoreItem GetStoreItem(RandomStore store, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetManualFlushNums(RandomStore store, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddManualFlushNums(RandomStore store, int addNums)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<RandomStore> Stores
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
