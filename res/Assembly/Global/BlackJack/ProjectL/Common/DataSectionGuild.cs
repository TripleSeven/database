﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionGuild : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_IsInInvitedGuild;
    private static DelegateBridge __Hotfix_AddJoinGuildInvitation;
    private static DelegateBridge __Hotfix_RemoveJoinGuildInvitation;
    private static DelegateBridge __Hotfix_ClearJoinGuildInvitation;
    private static DelegateBridge __Hotfix_get_GuildId;
    private static DelegateBridge __Hotfix_set_GuildId;
    private static DelegateBridge __Hotfix_get_NextJoinTime;
    private static DelegateBridge __Hotfix_set_NextJoinTime;
    private static DelegateBridge __Hotfix_get_InvitedGuildIds;
    private static DelegateBridge __Hotfix_set_InvitedGuildIds;
    private static DelegateBridge __Hotfix_get_MassiveCombat;
    private static DelegateBridge __Hotfix_set_MassiveCombat;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionGuild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInInvitedGuild(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveJoinGuildInvitation(string guildId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearJoinGuildInvitation()
    {
      // ISSUE: unable to decompile the method.
    }

    public string GuildId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextJoinTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<string> InvitedGuildIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GuildPlayerMassiveCombatInfo MassiveCombat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
