﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionRift
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionRift : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitLevel_1;
    private static DelegateBridge __Hotfix_FindChapter;
    private static DelegateBridge __Hotfix_AddChapter;
    private static DelegateBridge __Hotfix_FindLevel_1;
    private static DelegateBridge __Hotfix_FindLevel_0;
    private static DelegateBridge __Hotfix_AddLevel;
    private static DelegateBridge __Hotfix_InitLevel_0;
    private static DelegateBridge __Hotfix_AddLevelChallengeNums;
    private static DelegateBridge __Hotfix_GetLevelChallengeNums;
    private static DelegateBridge __Hotfix_ResetLevelChallengeNums;
    private static DelegateBridge __Hotfix_HasGotAchievementRelationId;
    private static DelegateBridge __Hotfix_AddAchievementRelationId;
    private static DelegateBridge __Hotfix_InitAchievementRelationIds;
    private static DelegateBridge __Hotfix_InitAllChapterStar;
    private static DelegateBridge __Hotfix_SetChapterStarReward;
    private static DelegateBridge __Hotfix_IsGainChapterStarReward;
    private static DelegateBridge __Hotfix_AddChapterStarReward;
    private static DelegateBridge __Hotfix_GetChapterTotalStars;
    private static DelegateBridge __Hotfix_GetAllRiftLevelStars;
    private static DelegateBridge __Hotfix_get_FinishedRiftLevelIds;
    private static DelegateBridge __Hotfix_set_FinishedRiftLevelIds;
    private static DelegateBridge __Hotfix_get_AchievementRelationIds;
    private static DelegateBridge __Hotfix_set_AchievementRelationIds;
    private static DelegateBridge __Hotfix_get_AchievementUpdateTime;
    private static DelegateBridge __Hotfix_set_AchievementUpdateTime;
    private static DelegateBridge __Hotfix_get_LastRiftAchievementRank;
    private static DelegateBridge __Hotfix_set_LastRiftAchievementRank;
    private static DelegateBridge __Hotfix_get_Chapters;
    private static DelegateBridge __Hotfix_set_Chapters;
    private static DelegateBridge __Hotfix_get_RiftStars;
    private static DelegateBridge __Hotfix_get_RiftChaptersStarUpdateTime;
    private static DelegateBridge __Hotfix_set_RiftChaptersStarUpdateTime;
    private static DelegateBridge __Hotfix_get_LastRiftStarsRank;
    private static DelegateBridge __Hotfix_set_LastRiftStarsRank;
    private static DelegateBridge __Hotfix_get_LastRiftRankUpdateTime;
    private static DelegateBridge __Hotfix_set_LastRiftRankUpdateTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionRift()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLevel(int chapterId, int levelId, int nums, int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapter FindChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftChapter AddChapter(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel FindLevel(int chapterId, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevel FindLevel(RiftChapter chapter, int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevel(RiftChapter chapter, int levelId, int nums, int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLevel(RiftChapter chapter, int levelId, int nums, int stars)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLevelChallengeNums(RiftLevel riftLevel, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelChallengeNums(RiftLevel riftLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetLevelChallengeNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGotAchievementRelationId(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAchievementRelationId(int achievementId, DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievementRelationIds(
      List<int> achievementRelationIds,
      DateTime updateTime,
      int lastRiftAchievment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAllChapterStar(
      DateTime updateTime,
      int lastRiftStars,
      DateTime lastRiftRankUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGainChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddChapterStarReward(int chapterId, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetChapterTotalStars(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllRiftLevelStars()
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<int> FinishedRiftLevelIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> AchievementRelationIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime AchievementUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastRiftAchievementRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Dictionary<int, RiftChapter> Chapters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RiftStars
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime RiftChaptersStarUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastRiftStarsRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastRiftRankUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
