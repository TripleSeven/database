﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Hero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class Hero
  {
    private int m_heroId;
    public List<int> SkillIds;
    public List<int> SoldierIds;
    public List<int> SelectedSkills;
    public List<int> NeedGetAchievementsJobRelatedIdList;
    public ulong[] EquipmentIds;
    public Dictionary<int, int> Fetters;
    private int m_selectedSoldierId;
    public List<HeroJob> Jobs;
    public List<int> UnlockedJobs;
    public List<SoldierSkin> SoldierSkins;
    private static DelegateBridge _c__Hotfix_ctor_0;
    private static DelegateBridge _c__Hotfix_ctor_1;
    private static DelegateBridge __Hotfix_GetHeroCopy;
    private static DelegateBridge __Hotfix_get_ActionPositionIndex;
    private static DelegateBridge __Hotfix_set_ActionPositionIndex;
    private static DelegateBridge __Hotfix_get_ActionValue;
    private static DelegateBridge __Hotfix_set_ActionValue;
    private static DelegateBridge __Hotfix_set_HeroId;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_Exp;
    private static DelegateBridge __Hotfix_set_Exp;
    private static DelegateBridge __Hotfix_get_StarLevel;
    private static DelegateBridge __Hotfix_set_StarLevel;
    private static DelegateBridge __Hotfix_get_FightNums;
    private static DelegateBridge __Hotfix_set_FightNums;
    private static DelegateBridge __Hotfix_get_ActiveHeroJobRelatedId;
    private static DelegateBridge __Hotfix_set_ActiveHeroJobRelatedId;
    private static DelegateBridge __Hotfix_get_CharSkinId;
    private static DelegateBridge __Hotfix_set_CharSkinId;
    private static DelegateBridge __Hotfix_get_FavorabilityLevel;
    private static DelegateBridge __Hotfix_set_FavorabilityLevel;
    private static DelegateBridge __Hotfix_get_FavorabilityExp;
    private static DelegateBridge __Hotfix_set_FavorabilityExp;
    private static DelegateBridge __Hotfix_get_Confessed;
    private static DelegateBridge __Hotfix_set_Confessed;
    private static DelegateBridge __Hotfix_get_HeartFetterLevel;
    private static DelegateBridge __Hotfix_set_HeartFetterLevel;
    private static DelegateBridge __Hotfix_get_HeroHeartFetterUnlockTime;
    private static DelegateBridge __Hotfix_set_HeroHeartFetterUnlockTime;
    private static DelegateBridge __Hotfix_get_HeroHeartFetterLevelMaxTime;
    private static DelegateBridge __Hotfix_set_HeroHeartFetterLevelMaxTime;
    private static DelegateBridge __Hotfix_get_Mine;
    private static DelegateBridge __Hotfix_set_Mine;
    private static DelegateBridge __Hotfix_GetHeroFetterTotalLevel;
    private static DelegateBridge __Hotfix_IsHeroHeartFetterLocked;
    private static DelegateBridge __Hotfix_IsEquipmentWeared;
    private static DelegateBridge __Hotfix_TakeOffEquipment;
    private static DelegateBridge __Hotfix_HasOwnSoldier;
    private static DelegateBridge __Hotfix_GetSoldierSkinId;
    private static DelegateBridge __Hotfix_set_SelectedSoldierId;
    private static DelegateBridge __Hotfix_get_SelectedSoldierId;
    private static DelegateBridge __Hotfix_get_CanTransferMaxRank;
    private static DelegateBridge __Hotfix_set_CanTransferMaxRank;
    private static DelegateBridge __Hotfix_get_BattlePower;
    private static DelegateBridge __Hotfix_set_BattlePower;
    private static DelegateBridge __Hotfix_get_BattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_set_BattlePowerUpdateTime;
    private static DelegateBridge __Hotfix_get_LastRank;
    private static DelegateBridge __Hotfix_set_LastRank;
    private static DelegateBridge __Hotfix_GetJob;
    private static DelegateBridge __Hotfix_GetActiveJob;
    private static DelegateBridge __Hotfix_HeroToPBHero;
    private static DelegateBridge __Hotfix_PBHeroToHero;
    private static DelegateBridge __Hotfix_UpdateHeroInfo;
    private static DelegateBridge __Hotfix_UpdateSoldierInfo;
    private static DelegateBridge __Hotfix_GetHeroCharImageSkinResourceInfo;
    private static DelegateBridge __Hotfix_GetSoldierModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_set_HeroInfo;
    private static DelegateBridge __Hotfix_get_HeroInfo;
    private static DelegateBridge __Hotfix_set_SelectedSoldierInfo;
    private static DelegateBridge __Hotfix_get_SelectedSoldierInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero(Hero other)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Hero GetHeroCopy()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ActionPositionIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Exp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int StarLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FightNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActiveHeroJobRelatedId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CharSkinId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FavorabilityLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FavorabilityExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Confessed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeartFetterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime HeroHeartFetterUnlockTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime HeroHeartFetterLevelMaxTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Mine
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroFetterTotalLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroHeartFetterLocked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEquipmentWeared(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TakeOffEquipment(int slot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasOwnSoldier(int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierSkinId(int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int SelectedSoldierId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CanTransferMaxRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattlePower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime BattlePowerUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LastRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob GetJob(int jobRelatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroJob GetActiveJob()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHero HeroToPBHero(Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Hero PBHeroToHero(ProHero pbHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHeroInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataCharImageSkinResourceInfo GetHeroCharImageSkinResourceInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataModelSkinResourceInfo GetSoldierModelSkinResourceInfo(
      int soldierId)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SelectedSoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
