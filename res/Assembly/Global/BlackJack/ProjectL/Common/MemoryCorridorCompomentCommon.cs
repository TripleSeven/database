﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.MemoryCorridorCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class MemoryCorridorCompomentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionMemoryCorridor m_memoryCorridorDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_IsLevelFinished;
    private static DelegateBridge __Hotfix_IsGameFunctionOpened;
    private static DelegateBridge __Hotfix_AttackMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_CanAttackMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_CanOpenLevel;
    private static DelegateBridge __Hotfix_CanAttackLevelByEnergyAndSoOn;
    private static DelegateBridge __Hotfix_SetCommonSuccessMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_GetAllFinishedLevels;
    private static DelegateBridge __Hotfix_IsBlessing;
    private static DelegateBridge __Hotfix_get_HasRewardAddRelativeOperationalActivity;
    private static DelegateBridge __Hotfix_set_HasRewardAddRelativeOperationalActivity;
    private static DelegateBridge __Hotfix_get_OperationalActivityDailyRewardNums;
    private static DelegateBridge __Hotfix_set_OperationalActivityDailyRewardNums;
    private static DelegateBridge __Hotfix_get_HasDailyExtraReward;
    private static DelegateBridge __Hotfix_set_HasDailyExtraReward;
    private static DelegateBridge __Hotfix_FlushChallengNums;
    private static DelegateBridge __Hotfix_IsDailyChallenge;
    private static DelegateBridge __Hotfix_GetDailyChallengNums;
    private static DelegateBridge __Hotfix_AddChallengedNums;
    private static DelegateBridge __Hotfix_GetAllUnlockedLevels;
    private static DelegateBridge __Hotfix_IsLevelUnlocked;
    private static DelegateBridge __Hotfix_CanRaidMemoryCorridorLevel;
    private static DelegateBridge __Hotfix_add_CompleteMemoryCorridorMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteMemoryCorridorMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public MemoryCorridorCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGameFunctionOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackMemoryCorridorLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackMemoryCorridorLevel(int levelId, bool isTeamBattle = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanOpenLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CanAttackLevelByEnergyAndSoOn(
      ConfigDataMemoryCorridorLevelInfo levelInfo,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetCommonSuccessMemoryCorridorLevel(
      ConfigDataMemoryCorridorLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetAllFinishedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasRewardAddRelativeOperationalActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OperationalActivityDailyRewardNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool HasDailyExtraReward
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlushChallengNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDailyChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddChallengedNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelUnlocked(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidMemoryCorridorLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteMemoryCorridorMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
