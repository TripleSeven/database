﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BPStagePeakArenaRule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BPStagePeakArenaRule : IBPStageRule
  {
    public List<long> PublicTimeSpan;
    public bool EarlyFinished;
    private BPStage m_stage;
    private IConfigDataLoader m_configDataLoader;
    private readonly long BanTimeSpan;
    private readonly long PickTimeSpan;
    private readonly List<PeakAreanaBPFlowInfo> m_flowInfos;
    private IBPStageListener m_listener;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_CanExecuteCommand;
    private static DelegateBridge __Hotfix_OnBan;
    private static DelegateBridge __Hotfix_OnPick;
    private static DelegateBridge __Hotfix_OnGiveUp;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_NextTurn;
    private static DelegateBridge __Hotfix_GeneratePlayerCommand;
    private static DelegateBridge __Hotfix_IsFinished;
    private static DelegateBridge __Hotfix_GetCurrentFlowInfo;
    private static DelegateBridge __Hotfix_get_CurrentTurn;
    private static DelegateBridge __Hotfix_set_CurrentTurn;
    private static DelegateBridge __Hotfix_get_CurrentPlayerIndex;
    private static DelegateBridge __Hotfix_set_CurrentPlayerIndex;
    private static DelegateBridge __Hotfix_get_TurnTimeSpan;
    private static DelegateBridge __Hotfix_set_TurnTimeSpan;

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStagePeakArenaRule(
      BPStage stage,
      IConfigDataLoader configDataLoader,
      IBPStageListener listener = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanExecuteCommand(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBan(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPick(int playerIndex, BPStageCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGiveUp(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnTick(long deltaTicks)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BPStageCommand GeneratePlayerCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakAreanaBPFlowInfo GetCurrentFlowInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CurrentTurn
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentPlayerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long TurnTimeSpan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
