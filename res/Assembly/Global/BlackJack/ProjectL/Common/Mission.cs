﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Mission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class Mission
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_MissionId;
    private static DelegateBridge __Hotfix_set_MissionId;
    private static DelegateBridge __Hotfix_get_CompletedProcess;
    private static DelegateBridge __Hotfix_set_CompletedProcess;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_set_Config;
    private static DelegateBridge __Hotfix_PBMissionToMission;
    private static DelegateBridge __Hotfix_PBMissionListToMissionList;
    private static DelegateBridge __Hotfix_MissionToPBMission;
    private static DelegateBridge __Hotfix_MissionListToPBMissionList;

    [MethodImpl((MethodImplOptions) 32768)]
    public Mission(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    public int MissionId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long CompletedProcess
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataMissionInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Mission PBMissionToMission(ProMission pbMission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<Mission> PBMissionListToMissionList(List<ProMission> pbMissionList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProMission MissionToPBMission(Mission misson)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProMission> MissionListToPBMissionList(
      List<Mission> missionList)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
