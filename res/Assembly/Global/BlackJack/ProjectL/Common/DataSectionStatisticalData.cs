﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionStatisticalData : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsExistStatisticalData;
    private static DelegateBridge __Hotfix_AddStatisticalData;
    private static DelegateBridge __Hotfix_AddNewStatisticalData;
    private static DelegateBridge __Hotfix_GetStatisticalDataByTypeId;
    private static DelegateBridge __Hotfix_InitStatisticalData;
    private static DelegateBridge __Hotfix_get_StatisticalData;
    private static DelegateBridge __Hotfix_set_StatisticalData;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionStatisticalData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExistStatisticalData(int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddStatisticalData(int typeId, long nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddNewStatisticalData(int typeId, long nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public long GetStatisticalDataByTypeId(int typeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStatisticalData(Dictionary<int, long> dataDict)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, long> StatisticalData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
