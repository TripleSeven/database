﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [Serializable]
  public class DataSection
  {
    protected ushort m_dbVersion;
    protected ushort m_clientVersion;
    protected ushort m_dbCommitedVersion;
    protected ushort m_clientCommitedVersion;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitVersion;
    private static DelegateBridge __Hotfix_SetDirty;
    private static DelegateBridge __Hotfix_NeedSyncToDB;
    private static DelegateBridge __Hotfix_OnDBSynced;
    private static DelegateBridge __Hotfix_SetClientCommitedVersion;
    private static DelegateBridge __Hotfix_NeedSyncToClient;
    private static DelegateBridge __Hotfix_OnClientSynced;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_get_ClientVersion;
    private static DelegateBridge __Hotfix_get_ClientCommitedVersion;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSection()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitVersion(ushort dbVersion, ushort clientVersion)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirty(bool needCommit2Client = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedSyncToDB()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnDBSynced()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClientCommitedVersion(ushort version)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool NeedSyncToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnClientSynced()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    public ushort Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ushort ClientVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ushort ClientCommitedVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
