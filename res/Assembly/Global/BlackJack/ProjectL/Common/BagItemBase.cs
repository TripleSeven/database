﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BagItemBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BagItemBase
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clone;
    private static DelegateBridge __Hotfix_IsInstanceBagItem;
    private static DelegateBridge __Hotfix_ToString;
    private static DelegateBridge __Hotfix_get_ListItemName;
    private static DelegateBridge __Hotfix_set_GoodsTypeId;
    private static DelegateBridge __Hotfix_get_GoodsTypeId;
    private static DelegateBridge __Hotfix_set_ContentId;
    private static DelegateBridge __Hotfix_get_ContentId;
    private static DelegateBridge __Hotfix_set_Nums;
    private static DelegateBridge __Hotfix_get_Nums;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_IsBagItem;
    private static DelegateBridge __Hotfix_IsMoney;
    private static DelegateBridge __Hotfix_IsEnough;
    private static DelegateBridge __Hotfix_IsThisGoodsType;
    private static DelegateBridge __Hotfix_ToPBGoods;
    private static DelegateBridge __Hotfix_ToGoods;
    private static DelegateBridge __Hotfix_PBGoodsToBagItem;
    private static DelegateBridge __Hotfix_PBGoodsToBagItems;
    private static DelegateBridge __Hotfix_BagItemsToPBGoods;
    private static DelegateBridge __Hotfix_GoodsListToPBGoodsList;
    private static DelegateBridge __Hotfix_PBGoodsListToGoodsList;
    private static DelegateBridge __Hotfix_GoodsToPBGoods;
    private static DelegateBridge __Hotfix_PBGoodsToGoods;
    private static DelegateBridge __Hotfix_UpdateConfigData;
    private static DelegateBridge __Hotfix_set_ItemInfo;
    private static DelegateBridge __Hotfix_get_ItemInfo;
    private static DelegateBridge __Hotfix_set_JobMaterialInfo;
    private static DelegateBridge __Hotfix_get_JobMaterialInfo;
    private static DelegateBridge __Hotfix_set_EquipmentInfo;
    private static DelegateBridge __Hotfix_get_EquipmentInfo;
    private static DelegateBridge __Hotfix_set_EnchantStoneInfo;
    private static DelegateBridge __Hotfix_get_EnchantStoneInfo;
    private static DelegateBridge __Hotfix_set_RefineryStoneInfo;
    private static DelegateBridge __Hotfix_get_RefineryStoneInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase(GoodsType goodsTypeId, int contentId, int nums, ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual BagItemBase Clone()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInstanceBagItem(GoodsType goodsTypeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    public string ListItemName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GoodsType GoodsTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ContentId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Nums
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsBagItem(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsMoney(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnough(int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsThisGoodsType(GoodsType goodsType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual ProGoods ToPBGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Goods ToGoods()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BagItemBase PBGoodsToBagItem(
      BagItemFactory bagItemFactory,
      ProGoods pbGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<BagItemBase> PBGoodsToBagItems(
      BagItemFactory bagItemFactory,
      List<ProGoods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProGoods> BagItemsToPBGoods(List<BagItemBase> bagItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProGoods> GoodsListToPBGoodsList(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<Goods> PBGoodsListToGoodsList(List<ProGoods> pbGoodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGoods GoodsToPBGoods(Goods goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Goods PBGoodsToGoods(ProGoods pbGoods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataItemInfo ItemInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobMaterialInfo JobMaterialInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataEquipmentInfo EquipmentInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataEnchantStoneInfo EnchantStoneInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRefineryStoneInfo RefineryStoneInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
