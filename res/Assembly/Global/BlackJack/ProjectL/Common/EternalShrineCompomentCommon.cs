﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.EternalShrineCompomentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class EternalShrineCompomentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected DataSectionEternalShrine m_eternalShrine;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected RiftComponentCommon m_rift;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_CanRaidEternalShrineLevel;
    private static DelegateBridge __Hotfix_AttackEternalShrineLevel;
    private static DelegateBridge __Hotfix_IsLevelFinished;
    private static DelegateBridge __Hotfix_IsGameFunctionOpened;
    private static DelegateBridge __Hotfix_CanAttackEternalShrineLevel;
    private static DelegateBridge __Hotfix_CheckEternalShrineOpened;
    private static DelegateBridge __Hotfix_CanOpenLevel;
    private static DelegateBridge __Hotfix_CanAttackLevelByEnergyAndSoOn;
    private static DelegateBridge __Hotfix_SetCommonSuccessEternalShrineLevel;
    private static DelegateBridge __Hotfix_GetAllFinishedLevels;
    private static DelegateBridge __Hotfix_IsBlessing;
    private static DelegateBridge __Hotfix_get_HasRewardAddRelativeOperationalActivity;
    private static DelegateBridge __Hotfix_set_HasRewardAddRelativeOperationalActivity;
    private static DelegateBridge __Hotfix_get_OperationalActivityChanllengenumsAdd;
    private static DelegateBridge __Hotfix_set_OperationalActivityChanllengenumsAdd;
    private static DelegateBridge __Hotfix_get_OperationalActivityDailyRewardNums;
    private static DelegateBridge __Hotfix_set_OperationalActivityDailyRewardNums;
    private static DelegateBridge __Hotfix_Flush;
    private static DelegateBridge __Hotfix_IsDailyChallenge;
    private static DelegateBridge __Hotfix_GetDailyChallengNums;
    private static DelegateBridge __Hotfix_GetDailyChallengeMaxNums;
    private static DelegateBridge __Hotfix_AddChallengedNums;
    private static DelegateBridge __Hotfix_GetAllUnlockedLevels;
    private static DelegateBridge __Hotfix_IsLevelUnlocked;
    private static DelegateBridge __Hotfix_add_CompleteEternalShrineMissionEvent;
    private static DelegateBridge __Hotfix_remove_CompleteEternalShrineMissionEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public EternalShrineCompomentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRaidEternalShrineLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackEternalShrineLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelFinished(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsGameFunctionOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanAttackEternalShrineLevel(int levelId, bool isTeamBattle = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CheckEternalShrineOpened(int eternalShrineId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanOpenLevel(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanAttackLevelByEnergyAndSoOn(
      ConfigDataEternalShrineLevelInfo levelInfo,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void SetCommonSuccessEternalShrineLevel(
      ConfigDataEternalShrineLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      int energyCost,
      bool isBattleTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public HashSet<int> GetAllFinishedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBlessing(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool HasRewardAddRelativeOperationalActivity
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OperationalActivityChanllengenumsAdd
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OperationalActivityDailyRewardNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Flush()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDailyChallenge()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDailyChallengeMaxNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddChallengedNums(int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetAllUnlockedLevels()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelUnlocked(int levelId)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<BattleType, int, List<int>> CompleteEternalShrineMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
