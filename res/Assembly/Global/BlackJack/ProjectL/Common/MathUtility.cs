﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.MathUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using FixMath.NET;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace BlackJack.ProjectL.Common
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  public struct MathUtility
  {
    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Min(Fix64 a, Fix64 b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Max(Fix64 a, Fix64 b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Lerp(Fix64 a, Fix64 b, Fix64 t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Clamp(Fix64 a, Fix64 min, Fix64 max)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint SetByte(uint a, int idx, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetByte(uint a, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static uint SetBit(uint a, int idx, bool b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ulong SetBit(ulong a, int idx, bool b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetBit(uint a, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool GetBit(ulong a, int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLineSegmentIntersectCircle(
      Vector2i p0,
      Vector2i p1,
      Vector2i center,
      Fix64 radius)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
