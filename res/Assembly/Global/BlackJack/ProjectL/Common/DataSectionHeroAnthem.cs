﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionHeroAnthem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionHeroAnthem : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitNeedInfo;
    private static DelegateBridge __Hotfix_AddHeroAnthemAchievement_1;
    private static DelegateBridge __Hotfix_AddHeroAnthemAchievement_0;
    private static DelegateBridge __Hotfix_InitAchievementRelationIdsRank;
    private static DelegateBridge __Hotfix_get_LastHeroAnthemAchievementRank;
    private static DelegateBridge __Hotfix_set_LastHeroAnthemAchievementRank;
    private static DelegateBridge __Hotfix_get_AchievementUpdateTime;
    private static DelegateBridge __Hotfix_set_AchievementUpdateTime;
    private static DelegateBridge __Hotfix_get_LastHeroAnthemRankUpdateTime;
    private static DelegateBridge __Hotfix_set_LastHeroAnthemRankUpdateTime;
    private static DelegateBridge __Hotfix_get_HasAttackHeroAnthemLevelInfo;
    private static DelegateBridge __Hotfix_set_HasAttackHeroAnthemLevelInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionHeroAnthem()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitNeedInfo(
      List<ProHeroAnthemSuccessLevel> HasSuccessLevel,
      DateTime update,
      int lastHeroAnthemAchievment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroAnthemAchievement(
      int heroAnthemLevelId,
      int achievementRelationId,
      DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroAnthemAchievement(int heroAnthemLevelID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievementRelationIdsRank(
      DateTime updateTime,
      int lastHeroAnthemAchievment,
      DateTime lastHeroAnthemRankUpdateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public int LastHeroAnthemAchievementRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime AchievementUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastHeroAnthemRankUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<HeroAnthemHasAttackLevelInfo> HasAttackHeroAnthemLevelInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
