﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaComponentCommon : IComponentBase
  {
    protected DataSectionArena m_arenaDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected BattleComponentCommon m_battle;
    protected HeroComponentCommon m_hero;
    protected BagComponentCommon m_bag;
    protected RiftComponentCommon m_rift;
    protected GiftStoreComponentCommon m_gift;
    protected IConfigDataLoader m_configDataLoader;
    public Action<int> ArenaConsecutiveVictoryEvent;
    public Action<BattleType, int, List<int>> ArenaAttackMissionEvent;
    public Action ArenaFightEvent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_OnBattleTimeOut;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFinishedArenaBattle;
    private static DelegateBridge __Hotfix_SetThisWeekBattleInfo;
    private static DelegateBridge __Hotfix_ResetThisWeekBattleInfo;
    private static DelegateBridge __Hotfix_InitArenaPlayerInfo;
    private static DelegateBridge __Hotfix_AddReceivedVictoryPointsRewardIndex;
    private static DelegateBridge __Hotfix_OnFlushVictoryPointsEvent;
    private static DelegateBridge __Hotfix_OnArenaGiveTicketsEvent;
    private static DelegateBridge __Hotfix_SystemGiveArenaTickets;
    private static DelegateBridge __Hotfix_GetArenaDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_SetSuccessArenaBattle;
    private static DelegateBridge __Hotfix_AddArenaBattleReport;
    private static DelegateBridge __Hotfix_SetArenaLevelIdAndPoints;
    private static DelegateBridge __Hotfix_GetNextBattleReportIndex;
    private static DelegateBridge __Hotfix_CanGetVictoryPointsReward;
    private static DelegateBridge __Hotfix_IsFullBattleReportNums;
    private static DelegateBridge __Hotfix_CanViewOpponent;
    private static DelegateBridge __Hotfix_CanAttackOpponent;
    private static DelegateBridge __Hotfix_CanAttackOpponentByClient;
    private static DelegateBridge __Hotfix_CanRevengeOpponent;
    private static DelegateBridge __Hotfix_SetBattleReportRevenged;
    private static DelegateBridge __Hotfix_CanFlushOpponents;
    private static DelegateBridge __Hotfix_IsInSettleCacheTime;
    private static DelegateBridge __Hotfix_IsInRealWeekSettleTime;
    private static DelegateBridge __Hotfix_IsInSettleTime;
    private static DelegateBridge __Hotfix_CanFlushArenaByWeek;
    private static DelegateBridge __Hotfix_CalculateWeekNextFlushTime;
    private static DelegateBridge __Hotfix_IsArenaOpen;
    private static DelegateBridge __Hotfix_IsEmptyArenaPlayerInfo;
    private static DelegateBridge __Hotfix_IsArenaInited;
    private static DelegateBridge __Hotfix_IsVictoryPointsRewardGot;
    private static DelegateBridge __Hotfix_AttackOpponentFighting;
    private static DelegateBridge __Hotfix_ArenaBattleFinish;
    private static DelegateBridge __Hotfix_get_IsFirstFindOpponents;
    private static DelegateBridge __Hotfix_SetUpFirstDefensiveTeam;
    private static DelegateBridge __Hotfix_SetMineRank;
    private static DelegateBridge __Hotfix_GetMineRank;
    private static DelegateBridge __Hotfix_get_ArenaPlayerInfo;
    private static DelegateBridge __Hotfix_GetAllArenaBattleReports;
    private static DelegateBridge __Hotfix_ArenaConsecutiveSuccess;
    private static DelegateBridge __Hotfix_CanGetArenaTopRankPlayers;
    private static DelegateBridge __Hotfix_CanGetArenaPlayerInfo;
    private static DelegateBridge __Hotfix_CanReconnectArenaBattle;
    private static DelegateBridge __Hotfix_ReconnectArenaBattle;
    private static DelegateBridge __Hotfix_get_m_arenaBattleReportDS;
    private static DelegateBridge __Hotfix_set_m_arenaBattleReportDS;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnBattleTimeOut()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFinishedArenaBattle(
      ArenaOpponentDefensiveBattleInfo defensivBattleInfo,
      bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetThisWeekBattleInfo(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ResetThisWeekBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void InitArenaPlayerInfo(ArenaPlayerInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void AddReceivedVictoryPointsRewardIndex(int victoryPointsIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnFlushVictoryPointsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void OnArenaGiveTicketsEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SystemGiveArenaTickets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo GetArenaDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetSuccessArenaBattle(int arenaOpponentPointZoneId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddArenaBattleReport(ArenaBattleReport arenaBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaLevelIdAndPoints(int arenaLevelId, ushort arenaPoints)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static byte GetNextBattleReportIndex(
      byte currentNextBattleReportIndex,
      int arenaBattleReportMaxNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetVictoryPointsReward(int victoryPointsRewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFullBattleReportNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanViewOpponent(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanAttackOpponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanAttackOpponentByClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanRevengeOpponent(ulong battleReportInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetBattleReportRevenged(ArenaBattleReport battleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanFlushOpponents()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInSettleCacheTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsInRealWeekSettleTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInSettleTime(
      IConfigDataLoader configDataLoader,
      int offsetSeconds,
      DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool CanFlushArenaByWeek(
      DateTime current,
      DateTime lastFlushTime,
      int relativeSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static DateTime CalculateWeekNextFlushTime(
      DateTime lastFlushTime,
      int relativeSeconds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaOpen()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEmptyArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsArenaInited()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVictoryPointsRewardGot(int victoryPointsRewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AttackOpponentFighting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ArenaBattleFinish(GameFunctionStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    protected bool IsFirstFindOpponents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void SetUpFirstDefensiveTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMineRank(int rank)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMineRank()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaPlayerInfo ArenaPlayerInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ArenaBattleReport> GetAllArenaBattleReports()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void ArenaConsecutiveSuccess(int consecutiveVictoryNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetArenaTopRankPlayers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanGetArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanReconnectArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ReconnectArenaBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public DataSectionArenaBattleReport m_arenaBattleReportDS
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
