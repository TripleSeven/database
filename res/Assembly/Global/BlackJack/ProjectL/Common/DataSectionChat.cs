﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionChat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionChat : DataSection
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitBannedTime;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_IsBannedTimeExpired;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_Unban;
    private static DelegateBridge __Hotfix_InitSilentBannedTime;
    private static DelegateBridge __Hotfix_IsSilentlyBanned;
    private static DelegateBridge __Hotfix_IsSilentBannedTimeExpired;
    private static DelegateBridge __Hotfix_SilentBan;
    private static DelegateBridge __Hotfix_SilentUnban;
    private static DelegateBridge __Hotfix_CanSendWorldChatByTime;
    private static DelegateBridge __Hotfix_get_m_bannedTime;
    private static DelegateBridge __Hotfix_set_m_bannedTime;
    private static DelegateBridge __Hotfix_get_m_silentlyBannedTime;
    private static DelegateBridge __Hotfix_set_m_silentlyBannedTime;
    private static DelegateBridge __Hotfix_get_LastWorldChannelChatSendTime;
    private static DelegateBridge __Hotfix_set_LastWorldChannelChatSendTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionChat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBannedTime(long bannedTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime bannedTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Unban()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSilentBannedTime(long bannedTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSilentlyBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSilentBannedTimeExpired(DateTime current)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SilentBan(DateTime bannedTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SilentUnban()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanSendWorldChatByTime(int intervalTime, DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime m_bannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime m_silentlyBannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime LastWorldChannelChatSendTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
