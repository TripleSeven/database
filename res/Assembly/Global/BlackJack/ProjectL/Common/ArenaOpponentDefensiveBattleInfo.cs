﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ArenaOpponentDefensiveBattleInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class ArenaOpponentDefensiveBattleInfo
  {
    public ArenaPlayerDefensiveTeamSnapshot BattleTeamSnapshot;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Status;
    private static DelegateBridge __Hotfix_set_Status;
    private static DelegateBridge __Hotfix_get_OpponentUserId;
    private static DelegateBridge __Hotfix_set_OpponentUserId;
    private static DelegateBridge __Hotfix_get_BattleRandomSeed;
    private static DelegateBridge __Hotfix_set_BattleRandomSeed;
    private static DelegateBridge __Hotfix_get_BattleExpiredTime;
    private static DelegateBridge __Hotfix_set_BattleExpiredTime;
    private static DelegateBridge __Hotfix_get_ArenaOpponentPointZoneId;
    private static DelegateBridge __Hotfix_set_ArenaOpponentPointZoneId;
    private static DelegateBridge __Hotfix_PBDefensiveBattleInfoToDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_DefensiveBattleInfoToPBDefensiveBattleInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentDefensiveBattleInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public ArenaBattleReportStatus Status
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string OpponentUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long BattleExpiredTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ArenaOpponentPointZoneId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ArenaOpponentDefensiveBattleInfo PBDefensiveBattleInfoToDefensiveBattleInfo(
      ProArenaDefensiveBattleInfo pbDefensiveBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProArenaDefensiveBattleInfo DefensiveBattleInfoToPBDefensiveBattleInfo(
      ArenaOpponentDefensiveBattleInfo defensiveBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
