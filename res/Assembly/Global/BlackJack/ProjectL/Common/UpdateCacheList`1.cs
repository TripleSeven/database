﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.UpdateCacheList`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class UpdateCacheList<T> where T : class
  {
    private LinkedList<int> m_freeIndices;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Add_0;
    private static DelegateBridge __Hotfix_Add_1;
    private static DelegateBridge __Hotfix_Remove_0;
    private static DelegateBridge __Hotfix_Set;
    private static DelegateBridge __Hotfix_Remove_1;
    private static DelegateBridge __Hotfix_Find;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_GetAllVaildDatas;
    private static DelegateBridge __Hotfix_IterateAllBagItems;
    private static DelegateBridge __Hotfix_GetAllVaildCaches;
    private static DelegateBridge __Hotfix_GetAllDirtyCaches;
    private static DelegateBridge __Hotfix_get_Count;
    private static DelegateBridge __Hotfix_get_ValidCount;
    private static DelegateBridge __Hotfix_IsIndexValid;
    private static DelegateBridge __Hotfix_get_Caches;
    private static DelegateBridge __Hotfix_set_Caches;

    [MethodImpl((MethodImplOptions) 32768)]
    public UpdateCacheList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Init(T data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Add(T data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Add(UpdateCache<T> cache, bool dirty = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Remove(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Set(int index, T data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Remove(UpdateCache<T> cache, int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UpdateCache<T> Find(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<T> GetAllVaildDatas()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<T> IterateAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UpdateCache<T>> GetAllVaildCaches()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<UpdateCache<T>> GetAllDirtyCaches()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Count
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ValidCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsIndexValid(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    public List<UpdateCache<T>> Caches
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
