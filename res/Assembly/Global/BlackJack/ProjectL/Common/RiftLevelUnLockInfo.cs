﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RiftLevelUnLockInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RiftLevelUnLockInfo
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_FinishedRiftLevelIds;
    private static DelegateBridge __Hotfix_set_FinishedRiftLevelIds;
    private static DelegateBridge __Hotfix_get_GainRiftAchievementRelationIds;
    private static DelegateBridge __Hotfix_set_GainRiftAchievementRelationIds;
    private static DelegateBridge __Hotfix_get_GainHeroIds;
    private static DelegateBridge __Hotfix_set_GainHeroIds;

    [MethodImpl((MethodImplOptions) 32768)]
    public RiftLevelUnLockInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public HashSet<int> FinishedRiftLevelIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> GainRiftAchievementRelationIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public HashSet<int> GainHeroIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
