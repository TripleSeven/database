﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PeakArenaExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public static class PeakArenaExtensions
  {
    private static DelegateBridge __Hotfix_ToPro_3;
    private static DelegateBridge __Hotfix_ToMemory_2;
    private static DelegateBridge __Hotfix_ToPro_2;
    private static DelegateBridge __Hotfix_ToMemory_1;
    private static DelegateBridge __Hotfix_ToMemory_3;
    private static DelegateBridge __Hotfix_ToPro_1;
    private static DelegateBridge __Hotfix_ToMemory_0;
    private static DelegateBridge __Hotfix_ToMemory_4;
    private static DelegateBridge __Hotfix_ToMemory_5;
    private static DelegateBridge __Hotfix_ToPro_0;
    private static DelegateBridge __Hotfix_IsWinnerDecided;
    private static DelegateBridge __Hotfix_IsHumanPlayer;

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaMatchStats ToPro(this PeakArenaMatchStats Stats)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaMatchStats ToMemory(this ProPeakArenaMatchStats Stats)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaHiredHero ToPro(this PeakArenaHiredHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaHiredHero ToMemory(this ProPeakArenaHiredHero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaHiredHero> ToMemory(
      this List<ProPeakArenaHiredHero> heroes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArenaBattleTeam ToPro(this PeakArenaBattleTeam bt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaBattleTeam ToMemory(this ProPeakArenaBattleTeam bt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PeakArenaHeroWithEquipments ToMemory(
      this ProHeroWithEquipments hero,
      BagItemFactory factory)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<PeakArenaHeroWithEquipments> ToMemory(
      this List<ProHeroWithEquipments> heroes,
      BagItemFactory factory)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProPeakArena ToPro(this DataSectionPeakArena ds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsWinnerDecided(this string winnerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsHumanPlayer(this string userId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
