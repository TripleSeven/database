﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.Vector2i
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using FixMath.NET;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct Vector2i
  {
    public Fix64 x;
    public Fix64 y;

    public Vector2i(Fix64 x, Fix64 y)
    {
      this.x = x;
      this.y = y;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2i(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    public void Set(Fix64 x, Fix64 y)
    {
      this.x = x;
      this.y = y;
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Set(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Normalize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Dot(Vector2i a, Vector2i b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 Distance(Vector2i a, Vector2i b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i Lerp(Vector2i a, Vector2i b, Fix64 t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i MoveTowards(
      Vector2i current,
      Vector2i target,
      Fix64 maxDistanceDelta)
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2i normalized
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Fix64 magnitude
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Fix64 sqrMagnitude
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static Vector2i zero
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static Vector2i one
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i operator +(Vector2i a, Vector2i b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i operator -(Vector2i a, Vector2i b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i operator -(Vector2i a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i operator *(Vector2i a, Fix64 d)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2i operator /(Vector2i a, Fix64 d)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator ==(Vector2i a, Vector2i b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool operator !=(Vector2i a, Vector2i b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string ToString(string format)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int GetHashCode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool Equals(object other)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
