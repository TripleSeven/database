﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionFriend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionFriend : DataSection
  {
    public List<string> LikedUsers;
    public List<string> FriendshipPointsSent;
    public List<string> FriendshipPointsReceived;
    public List<ulong> HeroicMomentBattleReports;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_SetBusinessCardInfo;
    private static DelegateBridge __Hotfix_OnFlushOfTime;
    private static DelegateBridge __Hotfix_ClearLikedUsers;
    private static DelegateBridge __Hotfix_SetLikes;
    private static DelegateBridge __Hotfix_AddLikes;
    private static DelegateBridge __Hotfix_AddLikedUser;
    private static DelegateBridge __Hotfix_AddFriendshipPointsReceivedUser;
    private static DelegateBridge __Hotfix_RemoveFriendshipPointsReceivedUser;
    private static DelegateBridge __Hotfix_RemoveFriendshipPointsSentUser;
    private static DelegateBridge __Hotfix_AddFriendshipPointsSentUser;
    private static DelegateBridge __Hotfix_SetBusinessCardDesc;
    private static DelegateBridge __Hotfix_SetBusinessCardHeroSet;
    private static DelegateBridge __Hotfix_SetRandomHeroAction;
    private static DelegateBridge __Hotfix_SetFriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_SetFriendshipPointsClaimedToday;
    private static DelegateBridge __Hotfix_IsBanned;
    private static DelegateBridge __Hotfix_Ban;
    private static DelegateBridge __Hotfix_AddHeroicMomentBattleReport;
    private static DelegateBridge __Hotfix_RemoveHeroicMomentBattleReport;
    private static DelegateBridge __Hotfix_ExistHeroicMomentBattleReport;
    private static DelegateBridge __Hotfix_get_BannedTime;
    private static DelegateBridge __Hotfix_set_BannedTime;
    private static DelegateBridge __Hotfix_get_BusinessCardSetInfo;
    private static DelegateBridge __Hotfix_set_BusinessCardSetInfo;
    private static DelegateBridge __Hotfix_get_Likes;
    private static DelegateBridge __Hotfix_set_Likes;
    private static DelegateBridge __Hotfix_get_FriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_set_FriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_get_FriendshipPointsClaimedToday;
    private static DelegateBridge __Hotfix_set_FriendshipPointsClaimedToday;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionFriend()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardInfo(BusinessCardInfoSet setInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnFlushOfTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLikedUsers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLikes(int likes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddLikes(int addValue = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddLikedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendshipPointsReceivedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveFriendshipPointsReceivedUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RemoveFriendshipPointsSentUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendshipPointsSentUser(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardDesc(string desc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBusinessCardHeroSet(List<BusinessCardHeroSet> heroSets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRandomHeroAction(bool actionRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendshipPointsFromFightWithFriendsToday(int points)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFriendshipPointsClaimedToday(int times)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBanned(DateTime currentTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Ban(DateTime banTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExistHeroicMomentBattleReport(ulong battleReportId)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime BannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BusinessCardInfoSet BusinessCardSetInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Likes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FriendshipPointsFromFightWithFriendsToday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FriendshipPointsClaimedToday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
