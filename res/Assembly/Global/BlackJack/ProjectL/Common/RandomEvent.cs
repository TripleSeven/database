﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RandomEvent
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_EventId;
    private static DelegateBridge __Hotfix_set_EventId;
    private static DelegateBridge __Hotfix_get_WayPointId;
    private static DelegateBridge __Hotfix_set_WayPointId;
    private static DelegateBridge __Hotfix_get_Lives;
    private static DelegateBridge __Hotfix_set_Lives;
    private static DelegateBridge __Hotfix_get_DeadLives;
    private static DelegateBridge __Hotfix_set_DeadLives;
    private static DelegateBridge __Hotfix_get_ExpiredTime;
    private static DelegateBridge __Hotfix_set_ExpiredTime;
    private static DelegateBridge __Hotfix_RandomEventToPBRandomEvent;
    private static DelegateBridge __Hotfix_RandomEventsToPBRandomEvents;
    private static DelegateBridge __Hotfix_PBRandomEventToRandomEvent;
    private static DelegateBridge __Hotfix_PBRandomEventsToRandomEvents;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomEvent()
    {
      // ISSUE: unable to decompile the method.
    }

    public int EventId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int WayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Lives
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DeadLives
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public long ExpiredTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRandomEvent RandomEventToPBRandomEvent(RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRandomEvent> RandomEventsToPBRandomEvents(
      List<RandomEvent> randomEvents)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RandomEvent PBRandomEventToRandomEvent(ProRandomEvent pbRadomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RandomEvent> PBRandomEventsToRandomEvents(
      List<ProRandomEvent> pbRadomEvents)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
