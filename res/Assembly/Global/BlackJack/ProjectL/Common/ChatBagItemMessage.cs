﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.ChatBagItemMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  [Serializable]
  public class ChatBagItemMessage : ChatMessage
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Text;
    private static DelegateBridge __Hotfix_set_Text;
    private static DelegateBridge __Hotfix_get_SystemContentTemplateId;
    private static DelegateBridge __Hotfix_set_SystemContentTemplateId;
    private static DelegateBridge __Hotfix_get_ChatBagItemDataList;
    private static DelegateBridge __Hotfix_set_ChatBagItemDataList;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatBagItemMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    public string Text
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SystemContentTemplateId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<ChatBagItemMessage.ChatBagItemData> ChatBagItemDataList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class ChatBagItemData
    {
      public ulong m_instanceId;
      public int m_resonanceCount;
      public BagItemBase m_bagItem;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public ChatBagItemData()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
