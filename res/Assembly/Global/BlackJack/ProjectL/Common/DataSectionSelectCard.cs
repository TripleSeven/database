﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionSelectCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionSelectCard : DataSection
  {
    private int m_guaranteedAccumulatedValue;
    private int m_guaranteedSelectCardCount;
    private SelectCardGuaranteedStatus m_selectCardGuaranteedStatus;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ClearInitedData;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_InitCardPools;
    private static DelegateBridge __Hotfix_SetCardPools;
    private static DelegateBridge __Hotfix_CanTenSelectDiscount;
    private static DelegateBridge __Hotfix_FindCardPool;
    private static DelegateBridge __Hotfix_RemoveCardPool;
    private static DelegateBridge __Hotfix_AddCardPool;
    private static DelegateBridge __Hotfix_AddSelectCount;
    private static DelegateBridge __Hotfix_SummonRareHero;
    private static DelegateBridge __Hotfix_AddGuaranteedSelectCount;
    private static DelegateBridge __Hotfix_AddTenSelectDiscountCount;
    private static DelegateBridge __Hotfix_IsFirstSingleSelectCard;
    private static DelegateBridge __Hotfix_IsFirstTenSelectCard;
    private static DelegateBridge __Hotfix_IsFirstSelectCard;
    private static DelegateBridge __Hotfix_FinishFirstSingleSelect;
    private static DelegateBridge __Hotfix_FinishFirstTenSelect;
    private static DelegateBridge __Hotfix_SetGuaranteedAccumulatedValue;
    private static DelegateBridge __Hotfix_InitGuaranteedAccumulatedValue;
    private static DelegateBridge __Hotfix_IsReachGuaranteedAccumulatedValue;
    private static DelegateBridge __Hotfix_IsAboveGuaranteedAccumulatedValue;
    private static DelegateBridge __Hotfix_IsCardPoolExistGuaranteedMechanism;
    private static DelegateBridge __Hotfix_SetGuaranteedSelectCardCount;
    private static DelegateBridge __Hotfix_InitGuaranteedSelectCardCount;
    private static DelegateBridge __Hotfix_InitSelectCardGuaranteedStatus;
    private static DelegateBridge __Hotfix_SetSelectCardGuaranteedStatus;
    private static DelegateBridge __Hotfix_get_CardPools;
    private static DelegateBridge __Hotfix_set_CardPools;
    private static DelegateBridge __Hotfix_get_GuaranteedAccumulatedValue;
    private static DelegateBridge __Hotfix_get_GuaranteedSelectCardCount;
    private static DelegateBridge __Hotfix_get_GuaranteedStatus;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionSelectCard()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void ClearInitedData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitCardPools(List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCardPools(List<CardPool> cardPools)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanTenSelectDiscount(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool FindCardPool(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveCardPool(int cardPoolId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CardPool AddCardPool(CardPool newCardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSelectCount(CardPool cardPool, int count = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SummonRareHero(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGuaranteedSelectCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddTenSelectDiscountCount(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstSingleSelectCard(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstTenSelectCard(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFirstSelectCard(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishFirstSingleSelect(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinishFirstTenSelect(CardPool cardPool)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuaranteedAccumulatedValue(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGuaranteedAccumulatedValue(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReachGuaranteedAccumulatedValue(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAboveGuaranteedAccumulatedValue()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCardPoolExistGuaranteedMechanism()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGuaranteedSelectCardCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGuaranteedSelectCardCount(int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitSelectCardGuaranteedStatus(SelectCardGuaranteedStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSelectCardGuaranteedStatus(SelectCardGuaranteedStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    public Dictionary<int, CardPool> CardPools
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GuaranteedAccumulatedValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GuaranteedSelectCardCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public SelectCardGuaranteedStatus GuaranteedStatus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
