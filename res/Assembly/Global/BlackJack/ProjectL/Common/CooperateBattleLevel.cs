﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.CooperateBattleLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class CooperateBattleLevel
  {
    public DateTime FirstClear;
    public CooperateBattle WhichCooperateBattle;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ID;
    private static DelegateBridge __Hotfix_set_ID;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_get_PlayerLevelRequirement;
    private static DelegateBridge __Hotfix_get_EnergySuccess;
    private static DelegateBridge __Hotfix_get_EnergyFail;
    private static DelegateBridge __Hotfix_get_MonsterLevel;
    private static DelegateBridge __Hotfix_get_BattleID;
    private static DelegateBridge __Hotfix_get_UserExp;
    private static DelegateBridge __Hotfix_get_HeroExp;
    private static DelegateBridge __Hotfix_get_GoldBonus;
    private static DelegateBridge __Hotfix_get_RandomDropID;
    private static DelegateBridge __Hotfix_get_TeamRandomDropID;
    private static DelegateBridge __Hotfix_get_ItemDropCountDisplay;
    private static DelegateBridge __Hotfix_get_DayBonusDropID;
    private static DelegateBridge __Hotfix_get_DayBonusExtraDropID;
    private static DelegateBridge __Hotfix_get_Cleared;
    private static DelegateBridge __Hotfix_get_IsAvailableForChallenge;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_Config;

    [MethodImpl((MethodImplOptions) 32768)]
    public CooperateBattleLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PlayerLevelRequirement
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int EnergySuccess
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int EnergyFail
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int MonsterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BattleID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int UserExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GoldBonus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RandomDropID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamRandomDropID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ItemDropCountDisplay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DayBonusDropID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DayBonusExtraDropID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Cleared
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAvailableForChallenge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCooperateBattleLevelInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
