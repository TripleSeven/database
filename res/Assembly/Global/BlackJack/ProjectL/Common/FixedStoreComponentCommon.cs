﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.FixedStoreComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class FixedStoreComponentCommon : IComponentBase
  {
    protected IConfigDataLoader m_configDataLoader;
    protected BagComponentCommon m_bag;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ResourceComponentCommon m_resource;
    protected DataSectionFixedStore m_fixedStoreDS;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_OnFlushBoughtNums;
    private static DelegateBridge __Hotfix_IsSoldOut;
    private static DelegateBridge __Hotfix_IsInSaleTime;
    private static DelegateBridge __Hotfix_CanBuyGoods;
    private static DelegateBridge __Hotfix_CanBuyFixedStoreItem;
    private static DelegateBridge __Hotfix_CaculateCurrencyCount;
    private static DelegateBridge __Hotfix_IsOnDiscountPeriod;
    private static DelegateBridge __Hotfix_GetStore;
    private static DelegateBridge __Hotfix_GetStoreOfferingById;
    private static DelegateBridge __Hotfix_BuyStoreItem;
    private static DelegateBridge __Hotfix_OnBuyStoreItem;
    private static DelegateBridge __Hotfix_OnAddSkin;
    private static DelegateBridge __Hotfix_add_BuyStoreItemEvent;
    private static DelegateBridge __Hotfix_remove_BuyStoreItemEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStoreComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnFlushBoughtNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSoldOut(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInSaleTime(ConfigDataFixedStoreItemInfo itemConfig)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int CanBuyGoods(int storeId, int goodsId, int selectedIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int CanBuyFixedStoreItem(ConfigDataFixedStoreItemInfo itemConfig, FixedStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateCurrencyCount(ConfigDataFixedStoreItemInfo storeItemConfig, bool isFirstBuy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOnDiscountPeriod(ConfigDataFixedStoreItemInfo storeItemConfig)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public FixedStore GetStore(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private FixedStore GetStoreOfferingById(int storeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void BuyStoreItem(int storeId, FixedStoreItem storeItem, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuyStoreItem(FixedStoreItem storeItem, int count)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnAddSkin(FixedStoreItem storeItem)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int, int> BuyStoreItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
