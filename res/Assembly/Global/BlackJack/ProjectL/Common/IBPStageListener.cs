﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.IBPStageListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace BlackJack.ProjectL.Common
{
  public interface IBPStageListener
  {
    BPStage GetBPStage();

    void OnExecuteCommand(int playerIndex, BPStageCommand command);

    void OnBPFinish(bool early);

    void OnBan(int playerIndex, int currentTurn, List<int> actorIds);

    void OnPick(int playerIndex, int currentTurn, List<int> actorIds);
  }
}
