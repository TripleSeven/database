﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BagComponentCommon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BagComponentCommon : IComponentBase
  {
    protected DataSectionBag m_bagDS;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ResourceComponentCommon m_resource;
    protected HeroComponentCommon m_hero;
    protected IConfigDataLoader m_configDataLoader;
    private int m_enhanceEquipmentMaterialExp;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetName;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_get_Owner;
    private static DelegateBridge __Hotfix_set_Owner;
    private static DelegateBridge __Hotfix_IsGoodsEnough;
    private static DelegateBridge __Hotfix_ConsumeGoods;
    private static DelegateBridge __Hotfix_CountIncreaseBagSize;
    private static DelegateBridge __Hotfix_CountDecreaseBagSize;
    private static DelegateBridge __Hotfix_IsBagFullByRandomGoods;
    private static DelegateBridge __Hotfix_IsBagFullByGoods;
    private static DelegateBridge __Hotfix_IsBagFullByGoodsOperation;
    private static DelegateBridge __Hotfix_IsBagFullByBagItems;
    private static DelegateBridge __Hotfix_IsBagFull;
    private static DelegateBridge __Hotfix_GetBagSize;
    private static DelegateBridge __Hotfix_IsBagFullByCurrentSize;
    private static DelegateBridge __Hotfix_IsHeuristicBagFull;
    private static DelegateBridge __Hotfix_FilterNonBagItem;
    private static DelegateBridge __Hotfix_FilterNonBagItemsWhenAdd;
    private static DelegateBridge __Hotfix_TransformHeroGoods;
    private static DelegateBridge __Hotfix_IsBagItemContentIdInConfig;
    private static DelegateBridge __Hotfix_RemoveAllBagItems;
    private static DelegateBridge __Hotfix_RemoveBagItem;
    private static DelegateBridge __Hotfix_RemoveBagItemByItem;
    private static DelegateBridge __Hotfix_RemoveBagItemByType;
    private static DelegateBridge __Hotfix_RemoveBagItemByInstanceId;
    private static DelegateBridge __Hotfix_RemoveBagItemDirectly;
    private static DelegateBridge __Hotfix_FindBagItem;
    private static DelegateBridge __Hotfix_FindBagItemByInstanceId;
    private static DelegateBridge __Hotfix_FindBagItemByType;
    private static DelegateBridge __Hotfix_FindUseableBagItem;
    private static DelegateBridge __Hotfix_GetAllBagItems;
    private static DelegateBridge __Hotfix_IterateAllBagItems;
    private static DelegateBridge __Hotfix_FindEnoughBagItems;
    private static DelegateBridge __Hotfix_UseBagItem_1;
    private static DelegateBridge __Hotfix_UseBagItem_0;
    private static DelegateBridge __Hotfix_UseBagItemDirectly;
    private static DelegateBridge __Hotfix_OpenSelfSelectedBox;
    private static DelegateBridge __Hotfix_OpenHeroSkinSelfSelectedBox;
    private static DelegateBridge __Hotfix_HasEnoughBagItem;
    private static DelegateBridge __Hotfix_IsBagItemEnough_0;
    private static DelegateBridge __Hotfix_IsBagItemEnough_1;
    private static DelegateBridge __Hotfix_GetTicketId;
    private static DelegateBridge __Hotfix_IsLevelTicketsMaxByUI;
    private static DelegateBridge __Hotfix_IsLevelTicketsMax;
    private static DelegateBridge __Hotfix_SellBagItem;
    private static DelegateBridge __Hotfix_CombineSameGoodsAndReplaceExistHeroToFragment;
    private static DelegateBridge __Hotfix_CanDecomposeBagItems;
    private static DelegateBridge __Hotfix_CanComposeBagItems;
    private static DelegateBridge __Hotfix_CanComposeABagItem;
    private static DelegateBridge __Hotfix_CanDecomposeABagItem;
    private static DelegateBridge __Hotfix_SetBagItemDirty;
    private static DelegateBridge __Hotfix_IsPercentBaseBattleProperty;
    private static DelegateBridge __Hotfix_CanLockAndUnLockEquipment;
    private static DelegateBridge __Hotfix_LockAndUnLockEquipment;
    private static DelegateBridge __Hotfix_CanEnhanceEquipment;
    private static DelegateBridge __Hotfix_CanWearEquipmentByEquipmentType;
    private static DelegateBridge __Hotfix_IsThisEquipmentType;
    private static DelegateBridge __Hotfix_IsLevelUpEquipmentStarLevel;
    private static DelegateBridge __Hotfix_CalculateEnhanceEquipmentExp;
    private static DelegateBridge __Hotfix_CalculateEnhanceEquipmentGold;
    private static DelegateBridge __Hotfix_EnhanceEquipment;
    private static DelegateBridge __Hotfix_CanEnchantEquipment;
    private static DelegateBridge __Hotfix_OutPutEqipmentEnhanceOperateLog;
    private static DelegateBridge __Hotfix_OutPutEquipmentUpgrageOperateLog;
    private static DelegateBridge __Hotfix_AddEquipmentExp;
    private static DelegateBridge __Hotfix_LevelUpEquipment;
    private static DelegateBridge __Hotfix_IsEquipmentMaxExp;
    private static DelegateBridge __Hotfix_CaculateEquipmentNextLevelExp;
    private static DelegateBridge __Hotfix_IsEquipmentMaxLevel;
    private static DelegateBridge __Hotfix_GetEquipmentLevelLimit;
    private static DelegateBridge __Hotfix_CanLevelUpEquipmentStar;
    private static DelegateBridge __Hotfix_GetLevelUpEquipmentStarItems;
    private static DelegateBridge __Hotfix_CalculateLevelUpEquipmentStarGold;
    private static DelegateBridge __Hotfix_LevelUpEquipmentStar_1;
    private static DelegateBridge __Hotfix_CalculateEquipmentTotalExp;
    private static DelegateBridge __Hotfix_CalculateDecomposeEquipmentBackGold;
    private static DelegateBridge __Hotfix_LevelUpEquipmentStar_0;
    private static DelegateBridge __Hotfix_GetEquipmentResonanceNumsByHero;
    private static DelegateBridge __Hotfix_OutPutItemOperationLog;
    private static DelegateBridge __Hotfix_OnSaveEnchantSaveMissionEvent;
    private static DelegateBridge __Hotfix_OnEnchantMissionEvent;
    private static DelegateBridge __Hotfix_OnCreateBagItemEvent;
    private static DelegateBridge __Hotfix_add_UseHeroExpItemMissionEvent;
    private static DelegateBridge __Hotfix_remove_UseHeroExpItemMissionEvent;
    private static DelegateBridge __Hotfix_set_m_bagItemFactory;
    private static DelegateBridge __Hotfix_get_m_bagItemFactory;
    private static DelegateBridge __Hotfix_add_EquipmentLevelupMissionEvent;
    private static DelegateBridge __Hotfix_remove_EquipmentLevelupMissionEvent;
    private static DelegateBridge __Hotfix_add_EnchantEquipmentPropertiesSaveMissionEvent;
    private static DelegateBridge __Hotfix_remove_EnchantEquipmentPropertiesSaveMissionEvent;
    private static DelegateBridge __Hotfix_add_EnchantEquipmentsMissionEvent;
    private static DelegateBridge __Hotfix_remove_EnchantEquipmentsMissionEvent;
    private static DelegateBridge __Hotfix_add_CreateBagItemEvent;
    private static DelegateBridge __Hotfix_remove_CreateBagItemEvent;
    private static DelegateBridge __Hotfix_add_EquipmentStarLevelUpEvent;
    private static DelegateBridge __Hotfix_remove_EquipmentStarLevelUpEvent;

    [MethodImpl((MethodImplOptions) 32768)]
    public BagComponentCommon()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetName()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool Serialize<T>(T dest)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DeSerialize<T>(T source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    public IComponentOwner Owner
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsGoodsEnough(List<Goods> conditions, out List<BagItemBase> BagItemsInBag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ConsumeGoods(
      List<Goods> needToConsumeGoods,
      List<BagItemBase> BagItemsInBag = null,
      List<BagItemBase> changedGoods = null,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CountIncreaseBagSize(List<Goods> addBagItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int CountDecreaseBagSize(List<Goods> deleteBagItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFullByRandomGoods(
      int addRandomRewardExpectSize,
      List<Goods> addGoods = null,
      List<Goods> deleteGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFullByGoods(List<Goods> addGoods = null, List<Goods> deleteGoods = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagFullByGoodsOperation(
      List<Goods> addGoods = null,
      List<Goods> deleteGoods = null,
      int addRandomRewardExpectSize = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBagFullByBagItems(
      List<Goods> addBagItems = null,
      List<Goods> deleteBagItems = null,
      int addRandomRewardExpectSize = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFull(int expectSize)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBagSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagFullByCurrentSize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeuristicBagFull(List<Goods> adds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> FilterNonBagItem(List<Goods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> FilterNonBagItemsWhenAdd(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected List<Goods> TransformHeroGoods(int heroId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int IsBagItemContentIdInConfig(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllBagItems(List<BagItemBase> changedBagItems = null, int removeItemMaxNums = 2147483647)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      ulong instanceId,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RemoveBagItemByItem(
      BagItemBase bagItem,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItemByType(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBagItemByInstanceId(
      ulong instanceId,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase RemoveBagItemDirectly(
      BagItemBase bagItem,
      int consumeNums,
      GameFunctionType caseId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItem(
      GoodsType goodsTypeId,
      int contentId,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItemByInstanceId(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BagItemBase FindBagItemByType(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UseableBagItem FindUseableBagItem(GoodsType goodsTypeId, int contentId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> GetAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IEnumerable<BagItemBase> IterateAllBagItems()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BagItemBase> FindEnoughBagItems(List<Goods> conditions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int UseBagItem(
      GoodsType goodsTypeId,
      int contentId,
      int consumeNums,
      params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int UseBagItem(UseableBagItem useableBagItem, int consumeNums, params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual int UseBagItemDirectly(
      UseableBagItem useableBagItem,
      int consumeNums,
      params object[] param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int OpenSelfSelectedBox(
      int selectedIndex,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int OpenHeroSkinSelfSelectedBox(
      List<int> selectedIndexes,
      GoodsType goodsTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int HasEnoughBagItem(BagItemBase bagItem, int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool IsBagItemEnough(BagItemBase bagItem, int consumeNums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBagItemEnough(GoodsType bagItemType, int bagItemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTicketId(GameFunctionType causeId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLevelTicketsMaxByUI(
      GameFunctionType causeId,
      GoodsType goodTypeId,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLevelTicketsMax(
      ConfigDataTicketLimitInfo ticketLimitInfo,
      int contentId,
      int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int SellBagItem(ulong instanceId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombineSameGoodsAndReplaceExistHeroToFragment(List<Goods> goodsList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDecomposeBagItems(List<ProGoods> goods)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeBagItems(int itemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanComposeABagItem(int itemId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanDecomposeABagItem(
      GoodsType goodsTypeId,
      int contentId,
      int nums,
      ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBagItemDirty(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPercentBaseBattleProperty(PropertyModifyType id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLockAndUnLockEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LockAndUnLockEquipment(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanEnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanWearEquipmentByEquipmentType(BagItemBase equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsThisEquipmentType(BagItemBase equipment, EquipmentType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLevelUpEquipmentStarLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentExp(List<BagItemBase> materials)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEnhanceEquipmentGold(int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int EnhanceEquipment(ulong instanceId, List<ulong> materialIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanEnchantEquipment(ulong equipmentInstanceId, ulong enchantStoneInstanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutEqipmentEnhanceOperateLog(
      EquipmentBagItem equipment,
      int preLevel,
      int preExp,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutEquipmentUpgrageOperateLog(
      EquipmentBagItem equipment,
      EquipmentBagItem material,
      int preLvlLimit,
      int postLvlLimit,
      List<Goods> costItems)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddEquipmentExp(EquipmentBagItem equipment, int exp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void LevelUpEquipment(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentMaxExp(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CaculateEquipmentNextLevelExp(int equipmentContentId, int equipmentLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEquipmentMaxLevel(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEquipmentLevelLimit(int equipmentStarLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanLevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<Goods> GetLevelUpEquipmentStarItems(
      int star,
      ConfigDataEquipmentInfo equipmentInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateLevelUpEquipmentStarGold(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int LevelUpEquipmentStar(ulong instanceId, ulong materialId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateEquipmentTotalExp(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CalculateDecomposeEquipmentBackGold(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void LevelUpEquipmentStar(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected int GetEquipmentResonanceNumsByHero(ulong equipmentInstanceId, Hero hero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OutPutItemOperationLog(
      GoodsType itemTypeId,
      int itemId,
      int nums,
      GameFunctionType causeId = GameFunctionType.GameFunctionType_None,
      string location = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSaveEnchantSaveMissionEvent(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEnchantMissionEvent(EquipmentBagItem equipment)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCreateBagItemEvent(BagItemBase bagItem)
    {
      // ISSUE: unable to decompile the method.
    }

    public event Action<int> UseHeroExpItemMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BagItemFactory m_bagItemFactory
    {
      [MethodImpl((MethodImplOptions) 32768)] private set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<EquipmentBagItem> EquipmentLevelupMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<EquipmentBagItem> EnchantEquipmentPropertiesSaveMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<EquipmentBagItem> EnchantEquipmentsMissionEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action<BagItemBase> CreateBagItemEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EquipmentStarLevelUpEvent
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
