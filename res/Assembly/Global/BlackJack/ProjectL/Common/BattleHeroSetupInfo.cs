﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleHeroSetupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleHeroSetupInfo
  {
    public int PlayerIndex;
    public int Position;
    private BattleHero m_hero;
    public SetupBattleHeroFlag Flag;
    public BattleHero PrevHero;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Hero;
    private static DelegateBridge __Hotfix_set_Hero;
    private static DelegateBridge __Hotfix_HasFlag;
    private static DelegateBridge __Hotfix_BattleHeroSetupInfoToPbProBattleHeroSetupInfo;
    private static DelegateBridge __Hotfix_PbBattleHeroSetupInfoToProBattleHeroSetupInfo;
    private static DelegateBridge __Hotfix_Copy;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleHero Hero
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFlag(SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProBattleHeroSetupInfo BattleHeroSetupInfoToPbProBattleHeroSetupInfo(
      BattleHeroSetupInfo info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleHeroSetupInfo PbBattleHeroSetupInfoToProBattleHeroSetupInfo(
      ProBattleHeroSetupInfo pbInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo Copy()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
