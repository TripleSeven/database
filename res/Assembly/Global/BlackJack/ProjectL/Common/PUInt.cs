﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.PUInt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public struct PUInt
  {
    private uint m_Value;
    private uint m_Check;

    [MethodImpl((MethodImplOptions) 32768)]
    public PUInt(uint v)
    {
      // ISSUE: unable to decompile the method.
    }

    public static implicit operator PUInt(uint value)
    {
      return new PUInt(value);
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static implicit operator uint(PUInt p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override string ToString()
    {
      // ISSUE: unable to decompile the method.
    }

    private static uint Encode(uint v)
    {
      return v ^ 1431655765U;
    }

    private static uint Decode(uint v)
    {
      return v ^ 1431655765U;
    }

    private static uint Check(uint v)
    {
      return v ^ 2004318071U;
    }
  }
}
