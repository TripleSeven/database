﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.AncientCallPeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class AncientCallPeriod
  {
    public List<AncientCallBoss> Bosses;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_CurrentPeriodId;
    private static DelegateBridge __Hotfix_set_CurrentPeriodId;
    private static DelegateBridge __Hotfix_get_CurrentBossIndex;
    private static DelegateBridge __Hotfix_set_CurrentBossIndex;
    private static DelegateBridge __Hotfix_FromPB;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_GetBossCurrentPeriodMaxDamage;
    private static DelegateBridge __Hotfix_GetBossCurrentMaxDamage;
    private static DelegateBridge __Hotfix_UpdateBossDamage;
    private static DelegateBridge __Hotfix_FindBossById;
    private static DelegateBridge __Hotfix_SetCurrentBoss;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallPeriod()
    {
      // ISSUE: unable to decompile the method.
    }

    public int CurrentPeriodId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int CurrentBossIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AncientCallPeriod FromPB(ProAncientCallPeriod pbPeriod)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProAncientCallPeriod ToPB(AncientCallPeriod period)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossCurrentPeriodMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossCurrentMaxDamage(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool UpdateBossDamage(
      int bossId,
      int damage,
      List<int> teamList,
      DateTime updateTime,
      int teamListBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBoss FindBossById(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCurrentBoss(int currentBossIndex)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
