﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TeamRoomSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class TeamRoomSetting
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_set_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_get_LocationId;
    private static DelegateBridge __Hotfix_set_LocationId;
    private static DelegateBridge __Hotfix_get_JoinMinLevel;
    private static DelegateBridge __Hotfix_set_JoinMinLevel;
    private static DelegateBridge __Hotfix_get_JoinMaxLevel;
    private static DelegateBridge __Hotfix_set_JoinMaxLevel;
    private static DelegateBridge __Hotfix_get_Authority;
    private static DelegateBridge __Hotfix_set_Authority;
    private static DelegateBridge __Hotfix_TeamRoomSettingToPbTeamRoomSetting;
    private static DelegateBridge __Hotfix_PbTeamRoomSettingToTeamRoomSetting;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    public GameFunctionType GameFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int LocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JoinMinLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JoinMaxLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TeamRoomAuthority Authority
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProTeamRoomSetting TeamRoomSettingToPbTeamRoomSetting(
      TeamRoomSetting setting)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static TeamRoomSetting PbTeamRoomSettingToTeamRoomSetting(
      ProTeamRoomSetting pbSetting)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
