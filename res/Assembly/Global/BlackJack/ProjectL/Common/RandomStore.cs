﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.RandomStore
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class RandomStore
  {
    public List<RandomStoreItem> Items;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Id;
    private static DelegateBridge __Hotfix_set_Id;
    private static DelegateBridge __Hotfix_get_NextFlushTime;
    private static DelegateBridge __Hotfix_set_NextFlushTime;
    private static DelegateBridge __Hotfix_get_ManualFlushNums;
    private static DelegateBridge __Hotfix_set_ManualFlushNums;
    private static DelegateBridge __Hotfix_StoreToPBStore;
    private static DelegateBridge __Hotfix_StoresToPBStores;
    private static DelegateBridge __Hotfix_PBStoreToStore;
    private static DelegateBridge __Hotfix_PBStoresToStores;

    [MethodImpl((MethodImplOptions) 32768)]
    public RandomStore()
    {
      // ISSUE: unable to decompile the method.
    }

    public int Id
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ManualFlushNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProRandomStore StoreToPBStore(RandomStore store)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<ProRandomStore> StoresToPBStores(List<RandomStore> stores)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static RandomStore PBStoreToStore(ProRandomStore pbStore)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<RandomStore> PBStoresToStores(List<ProRandomStore> pbStores)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
