﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.TrainingCourse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class TrainingCourse
  {
    public List<TrainingTech> Techs;
    private IConfigDataLoader _ConfigDataLoader;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ConfigId;
    private static DelegateBridge __Hotfix_set_ConfigId;
    private static DelegateBridge __Hotfix_get_RoomLevelRequired;
    private static DelegateBridge __Hotfix_get_WhichRoom;
    private static DelegateBridge __Hotfix_set_WhichRoom;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_set_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_Locked;
    private static DelegateBridge __Hotfix_ReloadConfigData;
    private static DelegateBridge __Hotfix_UpdateInstantiatedData;

    [MethodImpl((MethodImplOptions) 32768)]
    public TrainingCourse()
    {
      // ISSUE: unable to decompile the method.
    }

    public int ConfigId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int RoomLevelRequired
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public TrainingRoom WhichRoom
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataTrainingCourseInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool Locked
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReloadConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateInstantiatedData(List<TrainingTech> AvailableTechs)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
