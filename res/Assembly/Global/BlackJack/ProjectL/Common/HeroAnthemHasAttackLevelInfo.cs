﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.HeroAnthemHasAttackLevelInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class HeroAnthemHasAttackLevelInfo
  {
    public int LevelId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_GainHeroAnthemAchievementRelationIds;
    private static DelegateBridge __Hotfix_set_GainHeroAnthemAchievementRelationIds;
    private static DelegateBridge __Hotfix_ProHeroAnthemToHeroAnthemHasAttackLevelInfo;
    private static DelegateBridge __Hotfix_HeroAnthemHasAttackLevelInfoToProHeroAnthem;

    [MethodImpl((MethodImplOptions) 32768)]
    public HeroAnthemHasAttackLevelInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    public List<int> GainHeroAnthemAchievementRelationIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static HeroAnthemHasAttackLevelInfo ProHeroAnthemToHeroAnthemHasAttackLevelInfo(
      ProHeroAnthemSuccessLevel level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProHeroAnthemSuccessLevel HeroAnthemHasAttackLevelInfoToProHeroAnthem(
      HeroAnthemHasAttackLevelInfo level)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
