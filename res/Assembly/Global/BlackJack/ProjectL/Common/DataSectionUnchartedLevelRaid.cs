﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.DataSectionUnchartedLevelRaid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class DataSectionUnchartedLevelRaid : DataSection
  {
    public DateTime m_lastRefreshTime;
    public int m_surplusTimes;
    public bool m_isAddTimes;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitUnchartedLevelRaidInfo;
    private static DelegateBridge __Hotfix_SerializeToClient;
    private static DelegateBridge __Hotfix_ReduceTimes;
    private static DelegateBridge __Hotfix_RefreshUnchartedLevelRaidTimes;
    private static DelegateBridge __Hotfix_RefreshUnchartedLevelRaidByMonthCard;
    private static DelegateBridge __Hotfix_RefreshUnchartedLevelRaidByGM;
    private static DelegateBridge __Hotfix_GetUnchartedLevelRaidTimes;
    private static DelegateBridge __Hotfix_IsSameDay;

    [MethodImpl((MethodImplOptions) 32768)]
    public DataSectionUnchartedLevelRaid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitUnchartedLevelRaidInfo(
      DateTime lastRefreshTime,
      int surplusTimes,
      bool isAddTimes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override object SerializeToClient()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReduceTimes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshUnchartedLevelRaidTimes(int times, bool isAddTimes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshUnchartedLevelRaidByMonthCard(int times, bool isAddTimes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RefreshUnchartedLevelRaidByGM(int times)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnchartedLevelRaidTimes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSameDay(DateTime now)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
