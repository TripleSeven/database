﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.GiftStoreItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class GiftStoreItem
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_GoodsId;
    private static DelegateBridge __Hotfix_set_GoodsId;
    private static DelegateBridge __Hotfix_get_IsFirstBuy;
    private static DelegateBridge __Hotfix_set_IsFirstBuy;
    private static DelegateBridge __Hotfix_get_BoughtNums;
    private static DelegateBridge __Hotfix_set_BoughtNums;
    private static DelegateBridge __Hotfix_get_NextFlushTime;
    private static DelegateBridge __Hotfix_set_NextFlushTime;
    private static DelegateBridge __Hotfix_get_SaleStartTime;
    private static DelegateBridge __Hotfix_set_SaleStartTime;
    private static DelegateBridge __Hotfix_get_SaleEndTime;
    private static DelegateBridge __Hotfix_set_SaleEndTime;
    private static DelegateBridge __Hotfix_get_Config;
    private static DelegateBridge __Hotfix_set_Config;
    private static DelegateBridge __Hotfix_ToPB;
    private static DelegateBridge __Hotfix_FromPB;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreItem()
    {
      // ISSUE: unable to decompile the method.
    }

    public int GoodsId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFirstBuy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int BoughtNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime NextFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime SaleStartTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public DateTime SaleEndTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataGiftStoreItemInfo Config
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ProGiftStoreItem ToPB(GiftStoreItem item)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GiftStoreItem FromPB(ProGiftStoreItem pbItem)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
