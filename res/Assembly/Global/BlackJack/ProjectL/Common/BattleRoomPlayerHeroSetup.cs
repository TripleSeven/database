﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Common.BattleRoomPlayerHeroSetup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Common
{
  public class BattleRoomPlayerHeroSetup
  {
    private List<BattleHeroSetupInfo> m_battleHeroSetupInfos;
    private int m_pvpHeroPositionCount0;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitTeamBattle;
    private static DelegateBridge __Hotfix_InitPVPBattle;
    private static DelegateBridge __Hotfix_InitPlayerIndex;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_GetCount;
    private static DelegateBridge __Hotfix_SetHero;
    private static DelegateBridge __Hotfix_SwapHeros;
    private static DelegateBridge __Hotfix_UpdateBattleHeroActionPositionIndex;
    private static DelegateBridge __Hotfix_MarkSetupHeroFlag;
    private static DelegateBridge __Hotfix_GetBattleHeroSetupInfo;
    private static DelegateBridge __Hotfix_FindHeroPosition;
    private static DelegateBridge __Hotfix_GetHeroCountByPlayerIndex;
    private static DelegateBridge __Hotfix_GetHeroIdsByPlayerIndex;
    private static DelegateBridge __Hotfix_GetHeroPositionCountByPlayerIndex;
    private static DelegateBridge __Hotfix_GetBattleHeroSetupInfos;
    private static DelegateBridge __Hotfix_PVPRoomPositionToTeamPosition;
    private static DelegateBridge __Hotfix_PVPTeamPositionToRoomPosition;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayerHeroSetup()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitTeamBattle(int playerCount, int heroPosCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPVPBattle(int heroPosCount0, int heroPosCount1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayerIndex(int playerIdx, int heroPos, int heroPosCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SetHero(int playerIdx, int heroPos, BattleHero hero, bool checkHeroDup = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SwapHeros(int playerIdx, int heroPos1, int heroPos2, bool checkPos2HeroExist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBattleHeroActionPositionIndex(int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool MarkSetupHeroFlag(int playerIdx, int heroPos, SetupBattleHeroFlag Flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleHeroSetupInfo GetBattleHeroSetupInfo(int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindHeroPosition(int playerIdx, int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroCountByPlayerIndex(int playerIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetHeroIdsByPlayerIndex(int playerIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroPositionCountByPlayerIndex(int playerIdx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleHeroSetupInfo> GetBattleHeroSetupInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PVPRoomPositionToTeamPosition(int playerIndex, int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int PVPTeamPositionToRoomPosition(int playerIndex, int heroPos)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
