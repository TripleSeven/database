﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Static.ProjectLGameEntryUITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using IL;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Static
{
  public class ProjectLGameEntryUITask : GameEntryUITaskBase
  {
    private UITaskBase.LayerDesc[] m_layerDescArray;
    private UITaskBase.UIControllerDesc[] m_uiCtrlDescArray;
    private ProjectLGameEntryUIController m_mainCtrl;
    private float m_lastUpdateProgress;
    private long m_lastDownloadedBytes;
    private float m_lastUpdateProgressTime;
    private bool m_isUpdatePreloadProgress;
    private bool m_isUpdateLoadConfigProgress;
    private List<string> m_preloadMessage;
    private float m_lastUpdateMessageTime;
    private long m_downloadAudioLength;
    private const double m_byte2MB = 9.5367431640625E-07;
    public const string m_isUseLowResFlagName = "IsUseLowResFlagName";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnTick;
    private static DelegateBridge __Hotfix_OnStartHotfixManagerStart;
    private static DelegateBridge __Hotfix_OnStartHotfixManagerEnd;
    private static DelegateBridge __Hotfix_CheckWorkingPathChar;
    private static DelegateBridge __Hotfix_OnBasicVersionUnmatch;
    private static DelegateBridge __Hotfix_InitAllUIControllers;
    private static DelegateBridge __Hotfix_UpdateView;
    private static DelegateBridge __Hotfix_CheckNetwork;
    private static DelegateBridge __Hotfix_CheckIfUseLowResource;
    private static DelegateBridge __Hotfix_get_IsUseLowResource;
    private static DelegateBridge __Hotfix_set_IsUseLowResource;
    private static DelegateBridge __Hotfix_get_HasEverDownloadLowResource;
    private static DelegateBridge __Hotfix_EntryPipeLine;
    private static DelegateBridge __Hotfix_DownloadAudioFiles;
    private static DelegateBridge __Hotfix_OnStreamingAssetsFilesProcessingStart;
    private static DelegateBridge __Hotfix_OnStreamingAssetsFilesProcessingEnd;
    private static DelegateBridge __Hotfix_OnBundleDataLoadingStart;
    private static DelegateBridge __Hotfix_OnBundleDataLoadingEnd;
    private static DelegateBridge __Hotfix_NotifyUserDownloadAndWait;
    private static DelegateBridge __Hotfix_GetAudioDownloadLengthAndNotifyUser;
    private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingRefuse;
    private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingStart;
    private static DelegateBridge __Hotfix_OnAssetBundlePreUpdateingEnd;
    private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingStart;
    private static DelegateBridge __Hotfix_OnAssetBundleManifestLoadingEnd;
    private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysStart;
    private static DelegateBridge __Hotfix_OnLoadDynamicAssemblysEnd;
    private static DelegateBridge __Hotfix_OnLoadConfigDataStart;
    private static DelegateBridge __Hotfix_OnLoadConfigDataEnd;
    private static DelegateBridge __Hotfix_OnStartAudioManagerStart;
    private static DelegateBridge __Hotfix_OnStartAudioManagerEnd;
    private static DelegateBridge __Hotfix_CollectPreloadMessage;
    private static DelegateBridge __Hotfix_RandomUpdatePreloadMessage;
    private static DelegateBridge __Hotfix_LaunchLogin;
    private static DelegateBridge __Hotfix_UpdateLoadConfigProgress;
    private static DelegateBridge __Hotfix_UpdatePreloadProgress;
    private static DelegateBridge __Hotfix_CalcSkipAndForceUpdateBundleList;
    private static DelegateBridge __Hotfix_UpdateView4StreamingAssetsFilesProcessing;
    private static DelegateBridge __Hotfix_UpdateView4BundleDataLoading;
    private static DelegateBridge __Hotfix_UpdateView4AssetBundlePreUpdateing;
    private static DelegateBridge __Hotfix_UpdateView4AssetBundleManifestLoading;
    private static DelegateBridge __Hotfix_get_LayerDescArray;
    private static DelegateBridge __Hotfix_get_UICtrlDescArray;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProjectLGameEntryUITask(string taskName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnTick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartHotfixManagerStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartHotfixManagerEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckWorkingPathChar()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator OnBasicVersionUnmatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void InitAllUIControllers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckNetwork()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator CheckIfUseLowResource()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsUseLowResource
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private static bool HasEverDownloadLowResource
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator EntryPipeLine()
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    protected override IEnumerator DownloadAudioFiles()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStreamingAssetsFilesProcessingStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStreamingAssetsFilesProcessingEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBundleDataLoadingStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBundleDataLoadingEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void NotifyUserDownloadAndWait(long totalDownloadByte)
    {
      // ISSUE: unable to decompile the method.
    }

    [DebuggerHidden]
    [MethodImpl((MethodImplOptions) 32768)]
    private IEnumerator GetAudioDownloadLengthAndNotifyUser(long downloadAssetbundleBytes)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundlePreUpdateingRefuse()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundlePreUpdateingStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundlePreUpdateingEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundleManifestLoadingStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnAssetBundleManifestLoadingEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadDynamicAssemblysStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadDynamicAssemblysEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadConfigDataStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnLoadConfigDataEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartAudioManagerStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnStartAudioManagerEnd(bool ret)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPreloadMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RandomUpdatePreloadMessage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void LaunchLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateLoadConfigProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePreloadProgress()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CalcSkipAndForceUpdateBundleList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView4StreamingAssetsFilesProcessing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView4BundleDataLoading()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView4AssetBundlePreUpdateing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void UpdateView4AssetBundleManifestLoading()
    {
      // ISSUE: unable to decompile the method.
    }

    protected override UITaskBase.LayerDesc[] LayerDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    protected override UITaskBase.UIControllerDesc[] UICtrlDescArray
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
