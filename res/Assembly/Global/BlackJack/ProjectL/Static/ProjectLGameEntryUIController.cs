﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Static.ProjectLGameEntryUIController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using BlackJack.BJFramework.Runtime.UI;
using IL;
using System;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;

namespace BlackJack.ProjectL.Static
{
  public class ProjectLGameEntryUIController : UIControllerBase
  {
    private Action m_onShowCompanyEnd;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/Msg", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_messageText;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/ProgressValue", AutoBindAttribute.InitState.NotInit, false)]
    private Text m_progressText;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar", AutoBindAttribute.InitState.NotInit, false)]
    private Slider m_progressBar;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/Fill Area", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressBarFill;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/Background", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressBarBackground;
    [AutoBind("EntryUIPrefab/UpdateClient/ProcessBar/ProgressValue", AutoBindAttribute.InitState.NotInit, false)]
    private GameObject m_progressBarValue;
    [AutoBind("EntryUIPrefab/UpdateClient", AutoBindAttribute.InitState.Inactive, false)]
    private GameObject m_updateClientUI;
    [AutoBind("EntryUIPrefab/Company", AutoBindAttribute.InitState.Inactive, false)]
    private CommonUIStateController m_companyUI;
    [AutoBind("EntryUIPrefab/Company/Button", AutoBindAttribute.InitState.NotInit, false)]
    private Button m_closeCompanyButton;
    private static DelegateBridge __Hotfix_SetMesssage;
    private static DelegateBridge __Hotfix_ShowMessageText;
    private static DelegateBridge __Hotfix_ShowUpdateClientUI;
    private static DelegateBridge __Hotfix_IsTipActive;
    private static DelegateBridge __Hotfix_SetProgress;
    private static DelegateBridge __Hotfix_AddProgress;
    private static DelegateBridge __Hotfix_OnBindFiledsCompleted;
    private static DelegateBridge __Hotfix_OnCloseCompanyClicked;
    private static DelegateBridge __Hotfix_ShowCompanyUI;

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMesssage(string msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowMessageText(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowUpdateClientUI(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTipActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProgress(float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddProgress(float value, float threshhold = 1f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnBindFiledsCompleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnCloseCompanyClicked()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowCompanyUI(Action onEnd)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
