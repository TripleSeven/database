﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.IClientEventHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.LibClient
{
  public interface IClientEventHandler
  {
    void OnConnected();

    void OnDisconnected();

    void OnError(int err, string excepionInfo = null);

    void OnLoginByAuthTokenAck(int result, bool needRedirect, string sessionToken);

    void OnLoginBySessionTokenAck(int result);

    void OnMessage(object msg, int msgId);
  }
}
