﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.Client
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.LibClient;
using IL;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.LibClient
{
  public class Client : IClient
  {
    private LibClientStateMachine m_csm;
    private IClientEventHandler m_clientEventHandler;
    private string m_authToken;
    private string m_clientVersion;
    private string m_clientDeviceId;
    private string m_localization;
    private int m_loginChannelId;
    private int m_bornChannelId;
    private string m_sessionToken;
    private Connection m_connect;
    private LibClientProtoProvider m_protoProvider;
    private bool m_isBlockProcessMsg;
    private Func<Stream, Type, int, object> m_messageDeserializeAction;
    private string m_connectedAddress;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SetClientToInit;
    private static DelegateBridge __Hotfix_BlockProcessMsg;
    private static DelegateBridge __Hotfix_LoginByAuthToken;
    private static DelegateBridge __Hotfix_LoginBySessionToken;
    private static DelegateBridge __Hotfix_Disconnect;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_SendMessage;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickAuthLogin;
    private static DelegateBridge __Hotfix_TickSessionLogin;
    private static DelegateBridge __Hotfix_TickLoginOk;
    private static DelegateBridge __Hotfix_TickDisconnecting;
    private static DelegateBridge __Hotfix_TickDisconnected;
    private static DelegateBridge __Hotfix_DebugWarning;
    private static DelegateBridge __Hotfix_ProcessMessages;
    private static DelegateBridge __Hotfix_GetEndPoint;
    private static DelegateBridge __Hotfix_RegConnectionLogEvent;
    private static DelegateBridge __Hotfix_GetIp;

    [MethodImpl((MethodImplOptions) 32768)]
    public Client(
      IClientEventHandler handler,
      Func<Stream, Type, int, object> deserializeMessageAction = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetClientToInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BlockProcessMsg(bool isBlock)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization = "",
      int loginChannelId = 0,
      int bornChannelId = 0,
      string serverDomain = "")
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization = "",
      int loginChannelId = 0,
      int bornChannelId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Disconnect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMessage(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickAuthLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickSessionLogin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickLoginOk()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickDisconnecting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void TickDisconnected()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DebugWarning(string fun, string msg1, string msg2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessMessages(MessageProc proc = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEndPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegConnectionLogEvent(Action<string> logEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetIp()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
