﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.LibClient.IClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.LibClient
{
  public interface IClient
  {
    bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain);

    bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization,
      int loginChannelId,
      int bornChannelId);

    bool Disconnect();

    bool SendMessage(object msg);

    void Tick();

    void Close();

    void BlockProcessMsg(bool isBlock);
  }
}
