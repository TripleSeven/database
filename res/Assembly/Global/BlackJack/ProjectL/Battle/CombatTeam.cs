﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class CombatTeam
  {
    private Combat m_combat;
    private int m_teamNumber;
    private BattleActor m_battleActor;
    private List<CombatActor> m_actors;
    private List<CombatFlyObject> m_flyObjects;
    private static Position2i[] s_formationPositions;
    private ConfigDataSkillInfo m_heroSkillInfo;
    private bool m_shouldHeroToHeroCriticalAttack;
    private bool m_shouldHeroToSoldierCriticalAttack;
    private bool m_shouldSoldierToHeroCriticalAttack;
    private bool m_shouldSoldierToSoldierCriticalAttack;
    private bool m_isCastAnyDamageSkill;
    private bool m_isBeCriticalAttack;
    private ConfigDataSkillInfo m_lastDamageBySkillInfo;
    private int m_heroReceiveTotalDamage;
    private int m_soldierReceiveTotalDamage;
    private int m_heroApplyTotalDamage;
    private int m_soldierApplyTotalDamage;
    private int m_reboundPercent;
    private bool m_isTryApplyBuff;
    private ConfigDataSkillInfo m_attachBuffSourceSkillInfo;
    private BuffState m_doubleAttackBuffState;
    private bool m_isStun;
    private int m_fightStartDelay;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_ComputeHeroCriticalAttack;
    private static DelegateBridge __Hotfix_ComputeSoldierCriticalAttack;
    private static DelegateBridge __Hotfix_IsAttackHeroOnly;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_RemoveDeleted;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_GetColor;
    private static DelegateBridge __Hotfix_CreateActor;
    private static DelegateBridge __Hotfix_CreateFlyObject;
    private static DelegateBridge __Hotfix_RemoveAll;
    private static DelegateBridge __Hotfix_OnMyActorCastSkill;
    private static DelegateBridge __Hotfix_OnMyActorAttackBy;
    private static DelegateBridge __Hotfix_OnMyActorReboundDamage;
    private static DelegateBridge __Hotfix_OnMyActorDie;
    private static DelegateBridge __Hotfix_OnMyActorFightAgain;
    private static DelegateBridge __Hotfix_GetFormationPosition;
    private static DelegateBridge __Hotfix_GetFormationLine;
    private static DelegateBridge __Hotfix_GetHero;
    private static DelegateBridge __Hotfix_GetActors;
    private static DelegateBridge __Hotfix_IsFirstStrike;
    private static DelegateBridge __Hotfix_EnterCombat;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_StopCombat;
    private static DelegateBridge __Hotfix_CanStopCombat;
    private static DelegateBridge __Hotfix_LogCanNotStopCombat;
    private static DelegateBridge __Hotfix_CanReturn;
    private static DelegateBridge __Hotfix_HasAliveActor;
    private static DelegateBridge __Hotfix_GetAliveActorCount;
    private static DelegateBridge __Hotfix_SetGraphicSkillFade;
    private static DelegateBridge __Hotfix_IsAllAliveActorState;
    private static DelegateBridge __Hotfix_GetTargetTeam;
    private static DelegateBridge __Hotfix_ComputeSoldierTotalHealthPoint;
    private static DelegateBridge __Hotfix_GetLastDamageBySkill;
    private static DelegateBridge __Hotfix_IsReceiveAnyDamage;
    private static DelegateBridge __Hotfix_GetAttachBuffSourceSkillInfo;
    private static DelegateBridge __Hotfix_GetDoubleAttackBuff;
    private static DelegateBridge __Hotfix_get_Combat;
    private static DelegateBridge __Hotfix_get_TeamNumber;
    private static DelegateBridge __Hotfix_get_BattleActor;
    private static DelegateBridge __Hotfix_get_HeroInfo;
    private static DelegateBridge __Hotfix_get_HeroArmyInfo;
    private static DelegateBridge __Hotfix_get_JobConnectionInfo;
    private static DelegateBridge __Hotfix_get_JobInfo;
    private static DelegateBridge __Hotfix_get_SoldierInfo;
    private static DelegateBridge __Hotfix_get_SoldierArmyInfo;
    private static DelegateBridge __Hotfix_get_HeroSkillInfo;
    private static DelegateBridge __Hotfix_get_HeroLevel;
    private static DelegateBridge __Hotfix_get_HeroStar;
    private static DelegateBridge __Hotfix_get_JobLevel;
    private static DelegateBridge __Hotfix_get_ShouldHeroToHeroCriticalAttack;
    private static DelegateBridge __Hotfix_get_ShouldHeroToSoldierCriticalAttack;
    private static DelegateBridge __Hotfix_get_ShouldSoldierToHeroCriticalAttack;
    private static DelegateBridge __Hotfix_get_ShouldSoldierToSoldierCriticalAttack;
    private static DelegateBridge __Hotfix_get_IsStun;
    private static DelegateBridge __Hotfix_get_FightStartDelay;
    private static DelegateBridge __Hotfix_get_IsCastAnyDamageSkill;
    private static DelegateBridge __Hotfix_get_IsBeCriticalAttack;
    private static DelegateBridge __Hotfix_get_HeroReceiveTotalDamage;
    private static DelegateBridge __Hotfix_get_SoldierReceiveTotalDamage;
    private static DelegateBridge __Hotfix_get_HeroApplyTotalDamage;
    private static DelegateBridge __Hotfix_get_SoldierApplyTotalDamage;
    private static DelegateBridge __Hotfix_get_ReboundPercent;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static CombatTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      Combat combat,
      int team,
      BattleActor battleActor,
      ConfigDataSkillInfo heroSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeHeroCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeSoldierCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAttackHeroOnly()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveDeleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Colori GetColor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor CreateActor(bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatFlyObject CreateFlyObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorCastSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorAttackBy(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      bool isCritical,
      ArmyRelationData armyRelation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorReboundDamage(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int hpReboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorFightAgain(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Position2i GetFormationPosition(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFormationLine(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor GetHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CombatActor> GetActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsFirstStrike()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasAliveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetAliveActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAllAliveActorState(
      CombatActorState state1,
      CombatActorState state2,
      CombatActorState state3)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam GetTargetTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeSoldierTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetLastDamageBySkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsReceiveAnyDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetAttachBuffSourceSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState GetDoubleAttackBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleActor BattleActor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo HeroArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobInfo JobInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo SoldierArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSkillInfo HeroSkillInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldHeroToHeroCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldHeroToSoldierCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldSoldierToHeroCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool ShouldSoldierToSoldierCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsStun
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FightStartDelay
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCastAnyDamageSkill
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsBeCriticalAttack
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroReceiveTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierReceiveTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroApplyTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierApplyTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ReboundPercent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
