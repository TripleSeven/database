﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using FixMath.NET;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class CombatConst
  {
    public const int TickRate = 30;
    public static readonly Fix64 TickTime;
    public const int CameraAngle = 30;
    public const float CameraAngleSin = 0.5f;
    public const float CameraAngleCos = 0.866f;
    public const float CombatToWorldXScale = 0.02f;
    public const float CombatToWorldYScale = 0.01f;
    public const float CombatToWorldZScale = 0.01732f;
    public const float CombatToWorldXOblique = -0.005f;
    public const float CombatToWorldZOffset = 0.005f;
    public const int CombatStartDelay = 200;
    public const int CombatStopDelay = 200;
    public const int HeroInitY = -20;
    public const int HeroSoldierColumnSpace = 60;
    public const int SoldierColumnSpace = 50;
    public const int SoldierRowSpace = 40;
    public const int EnterMoveDistance = 500;
    public const int EnterMoveTime = 250;
    public const int ChargeReturnDistance = 300;
    public const int HitMaxDistance = 100;
    public const int SkillFadeZOffset = 25;
    public const int FlyObjectDownStraightZ = 80;
    public const int FlyObjectUpStraightZ = 150;
    public const int SkillFxTag = 1;
    public const int BuffFxTag = 2;
    public const int SkillFxTagMask = 2;
    public const int BuffFxTagMask = 4;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatConst()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static CombatConst()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
