﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleBase
  {
    private IConfigDataLoader m_configDataLoader;
    private IBattleListener m_battleListener;
    private RandomNumber m_randomNumber;
    private RandomNumber m_aiRandomNumber1;
    private RandomNumber m_aiRandomNumber2;
    private RandomNumber m_armyRandomNumber;
    private BattleMap m_map;
    private Pathfinder m_pathfinder;
    private Combat m_combat;
    private ConfigDataBattlefieldInfo m_battlefieldInfo;
    private ConfigDataBattleInfo m_battleInfo;
    private ConfigDataArenaBattleInfo m_arenaBattleInfo;
    private ConfigDataPVPBattleInfo m_pvpBattleInfo;
    private ConfigDataRealTimePVPBattleInfo m_realtimePvpBattleInfo;
    private ConfigDataPeakArenaBattleInfo m_peakArenaBattleInfo;
    private BattleType m_battleType;
    private int m_monsterLevel;
    private BattleTeam[] m_teams;
    private BattlePlayer[] m_players;
    private ConfigDataPlayerLevelInfo m_maxPlayerLevelInfo;
    private BattleState m_state;
    private int m_step;
    private int m_turn;
    private int m_turnMax;
    private int m_actionTeam;
    private int m_prevActionPlayerIndex;
    private BattleActor m_actionActor;
    private BattleActor m_execCommandActor;
    private BattleActor m_checkingDieActor;
    private ActionOrderType m_actionOrderType;
    private int m_entityIdCounter;
    private bool m_needCheckBattleStopEventTrigger;
    private int m_bossPhase;
    private bool m_isGiveupBattle;
    private int m_giveupBattlePlayerIndex;
    private int m_combatRandomSeed;
    private BattleActor m_combatActorA;
    private BattleActor m_combatActorB;
    private ConfigDataSkillInfo m_combatSkillInfoA;
    private BattleActor m_beGuardedCombatActor;
    private int m_enterCombatHealthPointPercentA;
    private int m_enterCombatHealthPointPercentB;
    private List<AfterCombatApplyBuff> m_afterCombatApplyBuffList;
    private bool m_isPerformCombat;
    private ConfigDataBattlePerformInfo m_afterComatPerform;
    private BattleEventTriggerState m_afterCombatExecuteTriggerState;
    private int m_afterCombatExecuteTriggerEventActionIndex;
    private bool m_isAfterCombatNextStep;
    private bool m_isAfterCombatNextStepActorActive;
    private ListPool<int> m_tempIntListPool;
    private ListPool<GridPosition> m_tempGridPositionListPool;
    private ListPool<BattleActor> m_tempActorListPool;
    private ListPool<ConfigDataBuffInfo> m_tempBuffInfoListPool;
    private ListPool<BuffState> m_tempBuffStateListPool;
    private BattlePropertyModifier m_tempBattlePropertyModifier;
    private LinkedList<BattleCommand> m_battleCommands;
    private LinkedList<BattleCommand> m_logBattleCommands;
    private LinkedList<BattleCommand> m_tempBattleCommands;
    private List<BattleCommand> m_stepExecutedCommands;
    private bool m_enableLogBattleCommands;
    private bool m_enableTempBattleCommands;
    private bool m_enableDebugLog;
    private bool m_enableErrorLog;
    private bool m_enableChecksum;
    private List<int> m_checksums;
    private List<BattleWinConditionState> m_winConditionStates;
    private List<BattleLoseConditionState> m_loseConditionStates;
    private List<BattleAchievementState> m_achievementStates;
    private List<BattleEventTriggerState> m_eventTriggerStates;
    private List<BattleTreasureState> m_battleTreasureStates;
    private List<BattleActor> m_needPostProcessDieActors;
    private List<BattleActor> m_needCheckDamageEventTriggerActors;
    private Dictionary<int, int> m_eventVariables;
    private int m_stars;
    private int m_starTurnMax;
    private int m_starDeadMax;
    private int m_myPlayerTeam;
    private int m_cheatPassiveSkillId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_SetWinForCheat;
    private static DelegateBridge __Hotfix_SetCheatPassiveSkillId;
    private static DelegateBridge __Hotfix_GetCheatPassiveSkillId;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_GetStarTurnMax;
    private static DelegateBridge __Hotfix_GetStarDeadMax;
    private static DelegateBridge __Hotfix_IsPerformOnlyBattle;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Stop;
    private static DelegateBridge __Hotfix_FirstStep;
    private static DelegateBridge __Hotfix_NextStep;
    private static DelegateBridge __Hotfix_NextStep_Normal;
    private static DelegateBridge __Hotfix_NextStep_Pvp;
    private static DelegateBridge __Hotfix_NextStepActorActive;
    private static DelegateBridge __Hotfix_NextTurn;
    private static DelegateBridge __Hotfix_GetDefaultActionActor;
    private static DelegateBridge __Hotfix_HasNotActionFinishedActor;
    private static DelegateBridge __Hotfix_GetActionTeam;
    private static DelegateBridge __Hotfix_GetActionActor;
    private static DelegateBridge __Hotfix_ChangeActionActor;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_ChangeMapTerrain;
    private static DelegateBridge __Hotfix_ChangeMapTerrainEffect;
    private static DelegateBridge __Hotfix_GetTeam;
    private static DelegateBridge __Hotfix_GetBattlePlayer;
    private static DelegateBridge __Hotfix_GetActorById;
    private static DelegateBridge __Hotfix_GetActorByHeroId;
    private static DelegateBridge __Hotfix_GetAliveActorByHeroId;
    private static DelegateBridge __Hotfix_GetAliveActorsByHeroIdsAndGroupIds;
    private static DelegateBridge __Hotfix_GetAliveActorsByTeamTypeAndPosition;
    private static DelegateBridge __Hotfix_GetNextEntityId;
    private static DelegateBridge __Hotfix_CheckOnActorMove;
    private static DelegateBridge __Hotfix_CheckOnActorDie;
    private static DelegateBridge __Hotfix_OnActorHit;
    private static DelegateBridge __Hotfix_CheckActorDamageEventTriggers;
    private static DelegateBridge __Hotfix_OnActorDie;
    private static DelegateBridge __Hotfix_PostProcessDieActors;
    private static DelegateBridge __Hotfix_SetProcessingDieActor;
    private static DelegateBridge __Hotfix_GetProcessingDieActor;
    private static DelegateBridge __Hotfix_OnActorRetreat;
    private static DelegateBridge __Hotfix_OnActorChangeTeam;
    private static DelegateBridge __Hotfix_IsPlay;
    private static DelegateBridge __Hotfix_IsGiveupBattle;
    private static DelegateBridge __Hotfix_GetGiveupBattlePlayerIndex;
    private static DelegateBridge __Hotfix_AllocateTempIntList;
    private static DelegateBridge __Hotfix_FreeTempIntList;
    private static DelegateBridge __Hotfix_AllocateTempGridPositionList;
    private static DelegateBridge __Hotfix_FreeTempGridPositionList;
    private static DelegateBridge __Hotfix_AllocateTempActorList;
    private static DelegateBridge __Hotfix_FreeTempActorList;
    private static DelegateBridge __Hotfix_AllocateTempBuffList;
    private static DelegateBridge __Hotfix_FreeTempBuffList;
    private static DelegateBridge __Hotfix_AllocateTempBuffStateList;
    private static DelegateBridge __Hotfix_FreeTempBuffStateList;
    private static DelegateBridge __Hotfix_RandomizeBuffStateList;
    private static DelegateBridge __Hotfix_GetTempBattlePropertyModifier;
    private static DelegateBridge __Hotfix_IsProbabilitySatisfied;
    private static DelegateBridge __Hotfix_GetRandomBuffList;
    private static DelegateBridge __Hotfix_ComputeArmyRelationData;
    private static DelegateBridge __Hotfix_FindPath;
    private static DelegateBridge __Hotfix_FindMoveRegion;
    private static DelegateBridge __Hotfix_FindAttackRegion;
    private static DelegateBridge __Hotfix_FindDirectionalAttackRegion;
    private static DelegateBridge __Hotfix_ComputeAttackDirection;
    private static DelegateBridge __Hotfix_FindRingRegion;
    private static DelegateBridge __Hotfix_FindFixedDistanceRegion;
    private static DelegateBridge __Hotfix_IsInRegion;
    private static DelegateBridge __Hotfix_IsInDangerRegion;
    private static DelegateBridge __Hotfix_GetStars;
    private static DelegateBridge __Hotfix_IsWin;
    private static DelegateBridge __Hotfix_GetPlayerActors;
    private static DelegateBridge __Hotfix_GetPlayerActorsLogInfos;
    private static DelegateBridge __Hotfix_GetPlayerKillActorCount;
    private static DelegateBridge __Hotfix_GetTotalKillActorCount;
    private static DelegateBridge __Hotfix_GetDeadEnemyActors;
    private static DelegateBridge __Hotfix_SaveMonsterTurnTotalDamage;
    private static DelegateBridge __Hotfix_GetMonsterTotalDamage;
    private static DelegateBridge __Hotfix_GetMonsterTurnTotalDamage;
    private static DelegateBridge __Hotfix_GetMonsterAllTurnDamages;
    private static DelegateBridge __Hotfix_GetMonsterTotalDamageByHeroId;
    private static DelegateBridge __Hotfix_SetBossPhase;
    private static DelegateBridge __Hotfix_GetBossPhase;
    private static DelegateBridge __Hotfix_IsAnyArenaOrPvpBattle;
    private static DelegateBridge __Hotfix_get_AliveActorsInfo;
    private static DelegateBridge __Hotfix_EnableChecksum;
    private static DelegateBridge __Hotfix_GetChecksums;
    private static DelegateBridge __Hotfix_ComputeStepChecksum;
    private static DelegateBridge __Hotfix_EnableDebugLog;
    private static DelegateBridge __Hotfix_IsEnableDebugLog;
    private static DelegateBridge __Hotfix_EnableErrorLog;
    private static DelegateBridge __Hotfix_IsEnableErrorLog;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_Listener;
    private static DelegateBridge __Hotfix_get_RandomNumber;
    private static DelegateBridge __Hotfix_get_AIRandomNumber1;
    private static DelegateBridge __Hotfix_get_AIRandomNumber2;
    private static DelegateBridge __Hotfix_get_BattleInfo;
    private static DelegateBridge __Hotfix_get_ArenaBattleInfo;
    private static DelegateBridge __Hotfix_get_PVPBattleInfo;
    private static DelegateBridge __Hotfix_get_RealTimePVPBattleInfo;
    private static DelegateBridge __Hotfix_get_PeakArenaBattleInfo;
    private static DelegateBridge __Hotfix_get_BattlefieldInfo;
    private static DelegateBridge __Hotfix_get_BattleType;
    private static DelegateBridge __Hotfix_get_ActionOrderType;
    private static DelegateBridge __Hotfix_get_BattlePlayers;
    private static DelegateBridge __Hotfix_get_MaxPlayerLevelInfo;
    private static DelegateBridge __Hotfix_get_State;
    private static DelegateBridge __Hotfix_get_Step;
    private static DelegateBridge __Hotfix_get_Turn;
    private static DelegateBridge __Hotfix_get_TurnMax;
    private static DelegateBridge __Hotfix_get_Map;
    private static DelegateBridge __Hotfix_get_Combat;
    private static DelegateBridge __Hotfix_CheckAchievementOnActorMove;
    private static DelegateBridge __Hotfix_CheckAchievementOnActorDie;
    private static DelegateBridge __Hotfix_CheckAchievementOnNextStep;
    private static DelegateBridge __Hotfix_CheckAchievementOnTurnEndOrBattleStop;
    private static DelegateBridge __Hotfix_CheckAchievementOnBattleStop;
    private static DelegateBridge __Hotfix_CheckAchievementOnWin;
    private static DelegateBridge __Hotfix_CheckAchievementOnLose;
    private static DelegateBridge __Hotfix_CheckAchievementOnEventTrigger;
    private static DelegateBridge __Hotfix_CheckAchievementOnBossPhaseChange;
    private static DelegateBridge __Hotfix_ChangeAchievementStatus;
    private static DelegateBridge __Hotfix_IsActorsAllAlive;
    private static DelegateBridge __Hotfix_IsActorsAllKillBySkillClass;
    private static DelegateBridge __Hotfix_IsActorReachPositions_0;
    private static DelegateBridge __Hotfix_IsActorReachPositions_1;
    private static DelegateBridge __Hotfix_IsActorsAnyReachPositions_0;
    private static DelegateBridge __Hotfix_IsActorsAnyReachPositions_1;
    private static DelegateBridge __Hotfix_GetActorsCountSatisfyCondition;
    private static DelegateBridge __Hotfix_IsActorInPositions;
    private static DelegateBridge __Hotfix_IsSkillClassMatch;
    private static DelegateBridge __Hotfix_IsActorClassMatch;
    private static DelegateBridge __Hotfix_IsAchievementComplete;
    private static DelegateBridge __Hotfix_GetCompleteAchievements;
    private static DelegateBridge __Hotfix_GetBattleAchievementStates;
    private static DelegateBridge __Hotfix_AddBattleCommand;
    private static DelegateBridge __Hotfix_AddLogBattleCommand;
    private static DelegateBridge __Hotfix_AddGiveupLogBattleCommand;
    private static DelegateBridge __Hotfix_AddCancelLogBattleCommand;
    private static DelegateBridge __Hotfix_AddGiveupOrCancelLogBattleCommand;
    private static DelegateBridge __Hotfix_AddBattleCommandToList;
    private static DelegateBridge __Hotfix_ClearBattleCommandsAndNextStep;
    private static DelegateBridge __Hotfix_ClearBattleCommands;
    private static DelegateBridge __Hotfix_GetBattleCommands;
    private static DelegateBridge __Hotfix_HasBattleCommand;
    private static DelegateBridge __Hotfix_HasCurrentStepBattleCommand;
    private static DelegateBridge __Hotfix_HasWrongStepBattleCommand;
    private static DelegateBridge __Hotfix_ExecuteBattleCommand;
    private static DelegateBridge __Hotfix_ExecuteGiveupCommand;
    private static DelegateBridge __Hotfix_RemoveStepCommands;
    private static DelegateBridge __Hotfix_RunCurrentStepBattleCommand;
    private static DelegateBridge __Hotfix_RunCombat;
    private static DelegateBridge __Hotfix_Run;
    private static DelegateBridge __Hotfix_RunAIActors;
    private static DelegateBridge __Hotfix_RunPlayerBattleCommands;
    private static DelegateBridge __Hotfix_AddExecutedCommand;
    private static DelegateBridge __Hotfix_FixExecutedCommands;
    private static DelegateBridge __Hotfix_GetStepExecutedCommands;
    private static DelegateBridge __Hotfix_EnableLogBattleCommands;
    private static DelegateBridge __Hotfix_IsEnableLogBattleCommands;
    private static DelegateBridge __Hotfix_GetLogBattleCommands;
    private static DelegateBridge __Hotfix_EnableTempBattleCommands;
    private static DelegateBridge __Hotfix_IsEnableTempBattleCommands;
    private static DelegateBridge __Hotfix_GetTempBattleCommands;
    private static DelegateBridge __Hotfix_ClearTempBattleCommands;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_StopCombat;
    private static DelegateBridge __Hotfix_PostProcessCombatActorsDie;
    private static DelegateBridge __Hotfix_RestartCombat;
    private static DelegateBridge __Hotfix_CopyCombatProperty;
    private static DelegateBridge __Hotfix_CheckWinConditionOnActorMove;
    private static DelegateBridge __Hotfix_CheckLoseConditionOnActorMove;
    private static DelegateBridge __Hotfix_CheckWinConditionOnActorDie;
    private static DelegateBridge __Hotfix_CheckLoseConditionOnActorDie;
    private static DelegateBridge __Hotfix_CheckWinConditionOnActorRetreat;
    private static DelegateBridge __Hotfix_CheckLoseConditionOnActorRetreat;
    private static DelegateBridge __Hotfix_CheckWinConditionOnEventTrigger;
    private static DelegateBridge __Hotfix_CheckLoseConditionOnEventTrigger;
    private static DelegateBridge __Hotfix_CheckWinConditionOnNextStep;
    private static DelegateBridge __Hotfix_CheckWinConditionOnTurnEnd;
    private static DelegateBridge __Hotfix_CheckWinLoseConditionOnTurnMax;
    private static DelegateBridge __Hotfix_ChangeWinConditionStatus;
    private static DelegateBridge __Hotfix_ChangeLoseConditionStatus;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorMove;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorDie;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorDamage;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorBeforeActionBegin;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorActionBegin;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorCombatAttackBefore;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorCombatAttackAfter;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorBeCombatAttack;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorUseSkillBefore;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnActorUseSkillAfter;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnNextStep;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnNextTeam;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnCompleteAchievement;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnWin;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnLose;
    private static DelegateBridge __Hotfix_CheckEventTriggerOnTurnEnd;
    private static DelegateBridge __Hotfix_CheckBattleStopEventTrigger;
    private static DelegateBridge __Hotfix_CheckMultiEventTrigger;
    private static DelegateBridge __Hotfix_CheckVariableEventTrigger;
    private static DelegateBridge __Hotfix_IsEventTriggerd;
    private static DelegateBridge __Hotfix_IsEventTriggerdOrConditionSatisfied;
    private static DelegateBridge __Hotfix_GetEventTriggerState;
    private static DelegateBridge __Hotfix_ResetEventTriggerState;
    private static DelegateBridge __Hotfix_DisableEventTriggerState;
    private static DelegateBridge __Hotfix_TriggerEvent;
    private static DelegateBridge __Hotfix_ExecuteTriggerEventActions;
    private static DelegateBridge __Hotfix_ExecuteEventAction;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Relief;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Retreat;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Dialog;
    private static DelegateBridge __Hotfix_ExecuteEventAction_ChangeTeam;
    private static DelegateBridge __Hotfix_ExecuteEventAction_AttachBuff;
    private static DelegateBridge __Hotfix_ExecuteEventAction_ChangeBehavior;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Music;
    private static DelegateBridge __Hotfix_ExecuteEventAction_ChangeTerrain;
    private static DelegateBridge __Hotfix_ExecuteEventAction_TerrainEffect;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Perform;
    private static DelegateBridge __Hotfix_ExecuteEventAction_RetreatPosition;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Replace;
    private static DelegateBridge __Hotfix_ExecuteEventAction_RemoveBuff;
    private static DelegateBridge __Hotfix_ExecuteEventAction_AttachBuffPosition;
    private static DelegateBridge __Hotfix_ExecuteEventAction_Teleport;
    private static DelegateBridge __Hotfix_ExecuteEventAction_ResetEventTrigger;
    private static DelegateBridge __Hotfix_ExecuteEventAction_DisableEventTrigger;
    private static DelegateBridge __Hotfix_ExecuteEventAction_SetVariable;
    private static DelegateBridge __Hotfix_ExecuteEventAction_AddVariable;
    private static DelegateBridge __Hotfix_ExecuteEventAction_RandomAction;
    private static DelegateBridge __Hotfix_ExecuteEventAction_ActorSkill;
    private static DelegateBridge __Hotfix_ExecuteEventAction_BossPhase;
    private static DelegateBridge __Hotfix_ExecuteEventAction_AddHp;
    private static DelegateBridge __Hotfix_IsEmptyPosition;
    private static DelegateBridge __Hotfix_GetRandomBattleArmyActors;
    private static DelegateBridge __Hotfix_CreateEventActor;
    private static DelegateBridge __Hotfix_ReplaceEventActor;
    private static DelegateBridge __Hotfix_SetEventVariable;
    private static DelegateBridge __Hotfix_GetEventVariable;
    private static DelegateBridge __Hotfix_FindEmptyFixedDistancePosition_1;
    private static DelegateBridge __Hotfix_FindEmptyFixedDistancePosition_0;
    private static DelegateBridge __Hotfix_FindEmptyNearPosition;
    private static DelegateBridge __Hotfix_GetBattleEventTriggerStates;
    private static DelegateBridge __Hotfix_GetBattleEventVariables;
    private static DelegateBridge __Hotfix_CreateMap;
    private static DelegateBridge __Hotfix_ResetMap;
    private static DelegateBridge __Hotfix_SetMyPlayerTeamNumber;
    private static DelegateBridge __Hotfix_GetMyPlayerTeamNumber;
    private static DelegateBridge __Hotfix_InitBattle;
    private static DelegateBridge __Hotfix_InitArenaBattle;
    private static DelegateBridge __Hotfix_InitPVPBattle;
    private static DelegateBridge __Hotfix_InitRealTimePVPBattle;
    private static DelegateBridge __Hotfix_InitPeakArenaBattle;
    private static DelegateBridge __Hotfix_InitRandomNumbers;
    private static DelegateBridge __Hotfix_InitPlayers;
    private static DelegateBridge __Hotfix_InitWinConditionStates;
    private static DelegateBridge __Hotfix_InitLoseConditionStates;
    private static DelegateBridge __Hotfix_InitEventTriggerStates;
    private static DelegateBridge __Hotfix_InitBattleTreasureStates;
    private static DelegateBridge __Hotfix_CreateActors;
    private static DelegateBridge __Hotfix_InitBattleReport;
    private static DelegateBridge __Hotfix_InitArenaBattleReport;
    private static DelegateBridge __Hotfix_InitPVPBattleReport;
    private static DelegateBridge __Hotfix_InitRealTimePVPBattleReport;
    private static DelegateBridge __Hotfix_InitPeakArenaBattleReport;
    private static DelegateBridge __Hotfix_InitStarAndAchievement;
    private static DelegateBridge __Hotfix_InitAchievement;
    private static DelegateBridge __Hotfix_InitGainBattleTreasures;
    private static DelegateBridge __Hotfix_ExecuteBattlePerforms;
    private static DelegateBridge __Hotfix_ExecuteAfterCombatSteps;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_Dialog;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_PlayMusic;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_PlaySound;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_PlayFx;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_PlayActorFx;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ChangeTerrain;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_TerrainEffect;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_CameraFocusPosition;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_CameraFocusActor;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_CreateActor;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_CreateActorNear;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_RemoveActor;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorMove;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorMoveNear;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorAttack;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorSkill;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorDir;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorAnimation;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_ActorIdle;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_WaitTime;
    private static DelegateBridge __Hotfix_ExecuteBattlePerform_StopBattle;
    private static DelegateBridge __Hotfix_CheckBattleTreasureOnActorMove;
    private static DelegateBridge __Hotfix_ChangeBattleTeasureStatus;
    private static DelegateBridge __Hotfix_IsGainBattleTreasure;
    private static DelegateBridge __Hotfix_GetGainBattleTreasures;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleBase(IConfigDataLoader configDataLoader, IBattleListener battleListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetWinForCheat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCheatPassiveSkillId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCheatPassiveSkillId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStarTurnMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStarDeadMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPerformOnlyBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Stop(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FirstStep(bool skipPerformCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStep_Normal(BattleActor actionActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStep_Pvp(BattleActor actionActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextStepActorActive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor GetDefaultActionActor(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasNotActionFinishedActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActionTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActionActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ChangeActionActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrain(List<GridPosition> positions, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam GetTeam(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer GetBattlePlayer(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor GetAliveActorByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetAliveActorsByHeroIdsAndGroupIds(
      List<int> heroIds,
      List<int> groupIds,
      List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetAliveActorsByTeamTypeAndPosition(
      int teamType,
      List<ParamPosition> posList,
      List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorHit(
      BattleActor actor,
      BattleActor attacker,
      int heroHpModify,
      int soldierHpModify)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckActorDamageEventTriggers()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostProcessDieActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetProcessingDieActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetProcessingDieActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorRetreat(
      BattleActor actor,
      int effectType,
      string fxName,
      bool notifyListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorChangeTeam(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGiveupBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGiveupBattlePlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> AllocateTempIntList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempIntList(List<int> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<GridPosition> AllocateTempGridPositionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempGridPositionList(List<GridPosition> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> AllocateTempActorList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempActorList(List<BattleActor> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBuffInfo> AllocateTempBuffList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempBuffList(List<ConfigDataBuffInfo> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BuffState> AllocateTempBuffStateList(
      List<BuffState> initList = null,
      BuffType initBuffType = BuffType.BuffType_None)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeTempBuffStateList(List<BuffState> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RandomizeBuffStateList(List<BuffState> buffStateList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePropertyModifier GetTempBattlePropertyModifier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProbabilitySatisfied(int rate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetRandomBuffList(
      List<int> buffIds,
      int num,
      List<ConfigDataBuffInfo> buffList,
      List<int> excludeBuffIds = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArmyRelationData ComputeArmyRelationData(
      BattleActor attacker,
      bool attackerIsHero,
      BattleActor target,
      bool targetIsHero,
      bool isMagic,
      bool isUi = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      GridPosition start,
      GridPosition goal,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindMoveRegion(
      GridPosition start,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindAttackRegion(
      GridPosition start,
      int range,
      int shape,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindDirectionalAttackRegion(
      GridPosition start,
      AttackDirection direction,
      int range,
      int shape,
      int distance,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AttackDirection ComputeAttackDirection(
      GridPosition start,
      GridPosition target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindRingRegion(GridPosition start, int range, List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindFixedDistanceRegion(
      GridPosition start,
      int distance,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsInRegion(GridPosition start, GridPosition target, int range, int shape)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInDangerRegion(int team, GridPosition pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStars()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetPlayerActors(int teamId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, List<BattleActorLogInfo>> GetPlayerActorsLogInfos(
      int teamId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerKillActorCount(int teamId, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalKillActorCount(int teamId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetDeadEnemyActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveMonsterTurnTotalDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterTotalDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterTurnTotalDamage(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetMonsterAllTurnDamages()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterTotalDamageByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBossPhase(int phase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBossPhase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyArenaOrPvpBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    public string AliveActorsInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableChecksum(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetChecksums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeStepChecksum()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableDebugLog(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableDebugLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableErrorLog(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableErrorLog()
    {
      // ISSUE: unable to decompile the method.
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IBattleListener Listener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber RandomNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber AIRandomNumber1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber AIRandomNumber2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataBattleInfo BattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArenaBattleInfo ArenaBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataPVPBattleInfo PVPBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRealTimePVPBattleInfo RealTimePVPBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataPeakArenaBattleInfo PeakArenaBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataBattlefieldInfo BattlefieldInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleType BattleType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ActionOrderType ActionOrderType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattlePlayer[] BattlePlayers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataPlayerLevelInfo MaxPlayerLevelInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Step
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Turn
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TurnMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleMap Map
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnTurnEndOrBattleStop(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnBattleStop(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnLose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnEventTrigger(BattleEventTriggerState eventState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAchievementOnBossPhaseChange(int phase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeAchievementStatus(BattleAchievementState bs, bool success)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAllAlive(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAllKillBySkillClass(List<int> heroIds, int skillClass)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorReachPositions(
      BattleActor a,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorReachPositions(
      BattleActor a,
      NpcCondition npcCondition,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAnyReachPositions(
      List<int> heroIds,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsActorsAnyReachPositions(
      int team,
      NpcCondition npcCondition,
      List<ParamPosition> positions,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetActorsCountSatisfyCondition(
      int team,
      NpcCondition npcCondition,
      int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsActorInPositions(BattleActor a, List<ParamPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsSkillClassMatch(ConfigDataSkillInfo skillInfo, int skillClass)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsActorClassMatch(BattleActor actor, int actorClass)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAchievementComplete(int achievementReleatedId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetCompleteAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleAchievementState> GetBattleAchievementStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBattleCommand(BattleCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddLogBattleCommand(BattleCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGiveupLogBattleCommand(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddCancelLogBattleCommand(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddGiveupOrCancelLogBattleCommand(BattleCommandType cmdType, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddBattleCommandToList(LinkedList<BattleCommand> commands, BattleCommand command)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleCommandsAndNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> GetBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasCurrentStepBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasWrongStepBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteGiveupCommand(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveStepCommands(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RunCurrentStepBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RunCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Run(bool strictCheckCommand = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RunAIActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool RunPlayerBattleCommands(int playerIndex, List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddExecutedCommand(BattleCommand cmd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FixExecutedCommands(BattleActor actor, int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleCommand> GetStepExecutedCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableLogBattleCommands(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableLogBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> GetLogBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTempBattleCommands(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEnableTempBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LinkedList<BattleCommand> GetTempBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTempBattleCommands()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat(BattleActor actorA, BattleActor actorB, ConfigDataSkillInfo skillInfoA)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PostProcessCombatActorsDie(
      BattleActor actorA,
      BattleActor actorB,
      BattleActor beGrardActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RestartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CopyCombatProperty(CombatTeam combatTeam, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnActorRetreat(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnActorRetreat(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnEventTrigger(BattleEventTriggerState eventState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckLoseConditionOnEventTrigger(BattleEventTriggerState eventState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinConditionOnTurnEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckWinLoseConditionOnTurnMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeWinConditionStatus(BattleWinConditionState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeLoseConditionStatus(BattleLoseConditionState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorDamage(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorBeforeActionBegin(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorActionBegin(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorCombatAttackBefore(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorCombatAttackAfter(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnActorBeCombatAttack(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckEventTriggerOnActorUseSkillBefore(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckEventTriggerOnActorUseSkillAfter(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnNextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnCompleteAchievement(int achievementId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnWin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnLose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckEventTriggerOnTurnEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckBattleStopEventTrigger()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckMultiEventTrigger(int subTriggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckVariableEventTrigger(int variableId, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEventTriggerd(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsEventTriggerdOrConditionSatisfied(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleEventTriggerState GetEventTriggerState(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetEventTriggerState(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DisableEventTriggerState(int triggerId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TriggerEvent(BattleEventTriggerState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteTriggerEventActions(BattleEventTriggerState state, int fromIdx = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Relief(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Retreat(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Dialog(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ChangeTeam(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AttachBuff(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ChangeBehavior(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Music(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ChangeTerrain(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_TerrainEffect(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Perform(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_RetreatPosition(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Replace(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_RemoveBuff(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AttachBuffPosition(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_Teleport(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ResetEventTrigger(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_DisableEventTrigger(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_SetVariable(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AddVariable(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_RandomAction(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_ActorSkill(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_BossPhase(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteEventAction_AddHp(ConfigDataBattleEventActionInfo e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEmptyPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<RandomArmyActor> GetRandomBattleArmyActors(
      List<int> randomArmies)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CreateEventActor(
      GridPosition p,
      int dir,
      int team,
      bool isNpc,
      int heroId,
      int heroLevel,
      int behaviorId,
      int groupId,
      int effectType,
      string fxName,
      BattleActorSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ReplaceEventActor(BattleActor a, int heroId, int heroLevel, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetEventVariable(int variableId, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetEventVariable(int variableId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindEmptyFixedDistancePosition(
      GridPosition target,
      int distance,
      MoveType moveType,
      bool isRandom = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindEmptyFixedDistancePosition(
      GridPosition start,
      GridPosition target,
      int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition FindEmptyNearPosition(
      GridPosition target,
      MoveType moveType,
      bool isRandom)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleEventTriggerState> GetBattleEventTriggerStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Dictionary<int, int> GetBattleEventVariables()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CreateMap(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyPlayerTeamNumber(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMyPlayerTeamNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitBattle(
      ConfigDataBattleInfo battleInfo,
      BattleType battleType,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int armyRandomSeed,
      int monsterLevel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitArenaBattle(
      ConfigDataArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      ConfigDataArenaDefendRuleInfo arenaDefendRuleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPVPBattle(
      ConfigDataPVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitRealTimePVPBattle(
      ConfigDataRealTimePVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPeakArenaBattle(
      ConfigDataPeakArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitRandomNumbers(int randomSeed, int armyRandomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitPlayers(BattlePlayer[] players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitWinConditionStates(List<int> winConditionIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitLoseConditionStates(List<int> loseConditionIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitEventTriggerStates(List<int> triggerIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitBattleTreasureStates(List<int> battleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateActors(int team, List<BattleActorSetup> setups)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitBattleReport(
      ConfigDataBattleInfo battleInfo,
      BattleType battleType,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int armyRandomSeed,
      int monsterLevel,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitArenaBattleReport(
      ConfigDataArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      ConfigDataArenaDefendRuleInfo arenaDefendRuleInfo,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPVPBattleReport(
      ConfigDataPVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitRealTimePVPBattleReport(
      ConfigDataRealTimePVPBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitPeakArenaBattleReport(
      ConfigDataPeakArenaBattleInfo battleInfo,
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      List<BattleCommand> commands)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitStarAndAchievement(
      int starTurnMax,
      int starDeadMax,
      List<ConfigDataBattleAchievementRelatedInfo> achievementRelatedInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitAchievement(
      List<ConfigDataBattleAchievementRelatedInfo> achievementRelatedInfos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitGainBattleTreasures(List<int> gainBattleTreasureIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerforms(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteAfterCombatSteps()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_Dialog(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlayMusic(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlaySound(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlayFx(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_PlayActorFx(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ChangeTerrain(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_TerrainEffect(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CameraFocusPosition(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CameraFocusActor(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CreateActor(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_CreateActorNear(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_RemoveActor(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorMove(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorMoveNear(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorAttack(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorSkill(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorDir(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorAnimation(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_ActorIdle(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_WaitTime(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ExecuteBattlePerform_StopBattle(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckBattleTreasureOnActorMove(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeBattleTeasureStatus(BattleTreasureState state, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGainBattleTreasure(int treasureId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetGainBattleTreasures()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
