﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Combat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class Combat
  {
    private BattleBase m_battle;
    private RandomNumber m_randomNumber;
    private CombatState m_state;
    private bool m_isPaused;
    private int m_entityIdCounter;
    private ushort m_hitIdCounter;
    private int m_combatGridDistance;
    private int m_frameCount;
    private int m_startCountdown;
    private int m_endCountdown;
    private int m_cutscenePauseCountdown;
    private CombatTeam[] m_teams;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Stop;
    private static DelegateBridge __Hotfix_SetupTeam;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_GetTeam;
    private static DelegateBridge __Hotfix_GetNextEntityId;
    private static DelegateBridge __Hotfix_GetNextHitId;
    private static DelegateBridge __Hotfix_GetCombatGridDistance;
    private static DelegateBridge __Hotfix_OnActorCastSkill;
    private static DelegateBridge __Hotfix_OnActorCastPassiveSkill;
    private static DelegateBridge __Hotfix_OnActorDie;
    private static DelegateBridge __Hotfix_IsPlay;
    private static DelegateBridge __Hotfix_IsPaused;
    private static DelegateBridge __Hotfix_IsProbabilitySatisfied;
    private static DelegateBridge __Hotfix_GetFrameCount;
    private static DelegateBridge __Hotfix_FrameToMillisecond;
    private static DelegateBridge __Hotfix_MillisecondToFrame;
    private static DelegateBridge __Hotfix_MillisecondToFrame1;
    private static DelegateBridge __Hotfix_get_Battle;
    private static DelegateBridge __Hotfix_get_RandomNumber;
    private static DelegateBridge __Hotfix_get_Listener;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_State;

    [MethodImpl((MethodImplOptions) 32768)]
    public Combat(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      BattleActor actor0,
      BattleActor actor1,
      ConfigDataSkillInfo heroSkillInfo0,
      ConfigDataSkillInfo heroSkillInfo1,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetupTeam(
      int teamNumber,
      BattleActor battleActor,
      ConfigDataSkillInfo heroSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatTeam GetTeam(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetNextHitId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCombatGridDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastPassiveSkill(CombatActor a, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPaused()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProbabilitySatisfied(int rate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFrameCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int FrameToMillisecond(int frame)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber RandomNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IBattleListener Listener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
