﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class CombatActor : Entity
  {
    private Vector2i m_position;
    private Vector2i m_initPosition;
    private int m_direction;
    private int m_initDirection;
    private int m_formationLine;
    private CombatTeam m_team;
    private bool m_isHero;
    private CombatActorState m_state;
    private int m_stateFrame;
    private bool m_isStateChanged;
    private int m_stateWaitFrame;
    private int m_stateMoveDistance;
    private int m_frameCount;
    private int m_canStopFrame;
    private bool m_isVisible;
    private CombatActor m_targetActor;
    private int m_preAttackHp;
    private ushort[] m_beHitIds;
    private ConfigDataSkillInfo m_skillInfo;
    private CombatSkillState m_curSkillState;
    private List<CombatSkillState> m_skillStates;
    private int m_skillUseCount;
    private bool m_isDoubleAttacked;
    private int m_healthPoint;
    private int m_healthPointMax;
    private int m_radius;
    private int m_height;
    private int m_footHeight;
    private IBattleGraphic m_graphic;
    private Vector2i m_graphicPrevPosition;
    private Fix64 m_graphicMoveDistance;
    private bool m_isGraphicSkillFade;
    private int m_deathAnimType;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_DoPause;
    private static DelegateBridge __Hotfix_EnterCombat;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_StopCombat;
    private static DelegateBridge __Hotfix_CanStopCombat;
    private static DelegateBridge __Hotfix_LogCanNotStopCombat;
    private static DelegateBridge __Hotfix_SetCanStopFrame;
    private static DelegateBridge __Hotfix_SetPosition_1;
    private static DelegateBridge __Hotfix_SetPosition_0;
    private static DelegateBridge __Hotfix_SetDirection;
    private static DelegateBridge __Hotfix_SetFormationLine;
    private static DelegateBridge __Hotfix_FaceTo;
    private static DelegateBridge __Hotfix_Move;
    private static DelegateBridge __Hotfix_MoveX;
    private static DelegateBridge __Hotfix_CreateGraphic;
    private static DelegateBridge __Hotfix_SetGraphicSkillFade;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_PlayDeathAnimation;
    private static DelegateBridge __Hotfix_PlayFx;
    private static DelegateBridge __Hotfix_PlaySkillFx;
    private static DelegateBridge __Hotfix_PlaySound;
    private static DelegateBridge __Hotfix_SetGraphicEffect;
    private static DelegateBridge __Hotfix_ClearGraphicEffect;
    private static DelegateBridge __Hotfix_ClearAttachFxs;
    private static DelegateBridge __Hotfix_IsDead;
    private static DelegateBridge __Hotfix_SetVisible;
    private static DelegateBridge __Hotfix_IsVisible;
    private static DelegateBridge __Hotfix_IsHero;
    private static DelegateBridge __Hotfix_GetBattleProperty;
    private static DelegateBridge __Hotfix_GetArmyInfo;
    private static DelegateBridge __Hotfix_GetMoveSpeed;
    private static DelegateBridge __Hotfix_GetAttackDistance;
    private static DelegateBridge __Hotfix_GetMoveType;
    private static DelegateBridge __Hotfix_GetGraphic;
    private static DelegateBridge __Hotfix_get_Position;
    private static DelegateBridge __Hotfix_get_InitPosition;
    private static DelegateBridge __Hotfix_get_Direction;
    private static DelegateBridge __Hotfix_get_FormationLine;
    private static DelegateBridge __Hotfix_get_Radius;
    private static DelegateBridge __Hotfix_get_Height;
    private static DelegateBridge __Hotfix_get_FootHeight;
    private static DelegateBridge __Hotfix_get_HealthPoint;
    private static DelegateBridge __Hotfix_get_HealthPointMax;
    private static DelegateBridge __Hotfix_get_State;
    private static DelegateBridge __Hotfix_get_StateFrame;
    private static DelegateBridge __Hotfix_get_Team;
    private static DelegateBridge __Hotfix_get_TeamNumber;
    private static DelegateBridge __Hotfix_get_Combat;
    private static DelegateBridge __Hotfix_TickSkillStates;
    private static DelegateBridge __Hotfix_TickSkillHits;
    private static DelegateBridge __Hotfix_Attack;
    private static DelegateBridge __Hotfix_Shoot;
    private static DelegateBridge __Hotfix_AttackBy;
    private static DelegateBridge __Hotfix_SetHealthPoint;
    private static DelegateBridge __Hotfix_PreAttack;
    private static DelegateBridge __Hotfix_GetPreAttackHealthPoint;
    private static DelegateBridge __Hotfix_AddBeHitId;
    private static DelegateBridge __Hotfix_HasBeHitId;
    private static DelegateBridge __Hotfix_CreateSkillState;
    private static DelegateBridge __Hotfix_AddSkillDelayHit;
    private static DelegateBridge __Hotfix_AddMagicDamageSkillDelayHits;
    private static DelegateBridge __Hotfix_AddChargeSkillDelayHits;
    private static DelegateBridge __Hotfix_CanBeTarget;
    private static DelegateBridge __Hotfix_ChangeFightTarget;
    private static DelegateBridge __Hotfix_SearchTarget;
    private static DelegateBridge __Hotfix_SearchRangeTarget;
    private static DelegateBridge __Hotfix_SearchHitTarget;
    private static DelegateBridge __Hotfix_SearchRandomHitTarget;
    private static DelegateBridge __Hotfix_ComputeTargetScore;
    private static DelegateBridge __Hotfix_IsTargetInAttackDistance;
    private static DelegateBridge __Hotfix_TickState;
    private static DelegateBridge __Hotfix_ChangeState;
    private static DelegateBridge __Hotfix_StateEnter;
    private static DelegateBridge __Hotfix_StateExit;
    private static DelegateBridge __Hotfix_StateIdle;
    private static DelegateBridge __Hotfix_StateFight;
    private static DelegateBridge __Hotfix_StateFightEnd;
    private static DelegateBridge __Hotfix_StateFightAgain;
    private static DelegateBridge __Hotfix_StateSkill;
    private static DelegateBridge __Hotfix_StateDie;
    private static DelegateBridge __Hotfix_StateReturn;
    private static DelegateBridge __Hotfix_StateStop;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(CombatTeam team, bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnterCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanStopFrame(int delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(int x, int y)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(Vector2i p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int d)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFormationLine(int line)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FaceTo(Vector2i pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Move(Vector2i dir, Fix64 distance, bool changeDirection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void MoveX(int dir, Fix64 distance, bool changeDirection)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateGraphic(
      string assetName,
      float scale,
      float height,
      float footHeight,
      string name,
      List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayDeathAnimation(int deathType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, int tag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySkillFx(string name, string nameFar)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearAttachFxs(int tagMask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleProperty GetBattleProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataArmyInfo GetArmyInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetMoveSpeed()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private MoveType GetMoveType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic GetGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2i Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Vector2i InitPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FormationLine
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Radius
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Height
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int FootHeight
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HealthPointMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatActorState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int StateFrame
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatTeam Team
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickSkillStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickSkillHits(CombatSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Attack(CombatActor target, ConfigDataSkillInfo skillInfo, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Shoot(CombatSkillState ss, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBy(CombatActor attacker, ConfigDataSkillInfo skillInfo, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHealthPoint(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PreAttack(CombatActor target, ConfigDataSkillInfo skillInfo, bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPreAttackHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBeHitId(ushort id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBeHitId(ushort id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatSkillState CreateSkillState(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSkillDelayHit(CombatSkillState ss, CombatActor target, int delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMagicDamageSkillDelayHits(CombatSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddChargeSkillDelayHits(CombatSkillState ss)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanBeTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeFightTarget(CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchRangeTarget(Vector2i pos, int maxDistance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchHitTarget(int attackDistance, ushort hitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchRandomHitTarget(ushort hitId, int targetType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeTargetScore(Vector2i pos, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTargetInAttackDistance(CombatActor target, int attackDistance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ChangeState(CombatActorState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateEnter()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateExit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateIdle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateFight()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateFightEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateFightAgain()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateDie()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateReturn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StateStop()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
