﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleTimeEstimator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleTimeEstimator
  {
    private int m_time;
    private BattleBase m_battle;
    private int m_moveGridTime;
    private int m_cameraSpeed;
    private int m_viewWidthHalf;
    private int m_viewHeighHalf;
    private GridPosition m_cameraPositionMin;
    private GridPosition m_cameraPositionMax;
    private int m_buffHitStep;
    private int m_dieStep;
    private int m_passiveSkillStep;
    private int m_passiveSkillBuffStateId;
    private GridPosition m_cameraPosition;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_InitCameraPositionBound;
    private static DelegateBridge __Hotfix_GetTime;
    private static DelegateBridge __Hotfix_ClearTime;
    private static DelegateBridge __Hotfix_OnBattleStart;
    private static DelegateBridge __Hotfix_OnBattleNextTurn;
    private static DelegateBridge __Hotfix_OnBattleNextTeam;
    private static DelegateBridge __Hotfix_OnBattleNextPlayer;
    private static DelegateBridge __Hotfix_OnBattleActorActive;
    private static DelegateBridge __Hotfix_OnBattleActorMove;
    private static DelegateBridge __Hotfix_OnBattleActorPerformMove;
    private static DelegateBridge __Hotfix_OnBattleActorPunchMove;
    private static DelegateBridge __Hotfix_OnBattleActorExchangeMove;
    private static DelegateBridge __Hotfix_OnBattleActorPlayAnimation;
    private static DelegateBridge __Hotfix_OnBattleActorSkill;
    private static DelegateBridge __Hotfix_OnBattleActorImmune;
    private static DelegateBridge __Hotfix_OnBattleActorPassiveSkill;
    private static DelegateBridge __Hotfix_OnBattleActorBuffHit;
    private static DelegateBridge __Hotfix_OnBattleActorTerrainHit;
    private static DelegateBridge __Hotfix_OnBattleActorTeleport;
    private static DelegateBridge __Hotfix_OnBattleActorSummon;
    private static DelegateBridge __Hotfix_OnBattleActorDie;
    private static DelegateBridge __Hotfix_OnBattleActorAppear;
    private static DelegateBridge __Hotfix_OnBattleActorDisappear;
    private static DelegateBridge __Hotfix_OnBattleActorChangeTeam;
    private static DelegateBridge __Hotfix_OnBattleActorReplace;
    private static DelegateBridge __Hotfix_OnBattleActorCameraFocus;
    private static DelegateBridge __Hotfix_OnBattleActorGainBattleTreasure;
    private static DelegateBridge __Hotfix_OnStartGuard;
    private static DelegateBridge __Hotfix_OnStopGuard;
    private static DelegateBridge __Hotfix_OnStartCombat;
    private static DelegateBridge __Hotfix_OnStopCombat;
    private static DelegateBridge __Hotfix_OnCameraFocus;
    private static DelegateBridge __Hotfix_OnWaitTime;
    private static DelegateBridge __Hotfix_AddTime;
    private static DelegateBridge __Hotfix_AddCameraMoveTime;
    private static DelegateBridge __Hotfix_SetCameraPosition;
    private static DelegateBridge __Hotfix_BoundCameraPosition;
    private static DelegateBridge __Hotfix_ClampCameraPosition;
    private static DelegateBridge __Hotfix_ComputeCameraBoundPosition;
    private static DelegateBridge __Hotfix_ComputeSkillCastTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTimeEstimator()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitCameraPositionBound(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTime(bool isFastBattle = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPlayAnimation(BattleActor a, string animationName, int animationTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkill(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorImmune(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleport(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDie(BattleActor actor, bool isAfterCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDisappear(BattleActor actor, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeTeam(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCameraFocus(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorGainBattleTreasure(BattleActor a, ConfigDataBattleTreasureInfo t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraFocus(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaitTime(int timeInMs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddTime(int timeInMs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddCameraMoveTime(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetCameraPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BoundCameraPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition ClampCameraPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition ComputeCameraBoundPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeSkillCastTime(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
