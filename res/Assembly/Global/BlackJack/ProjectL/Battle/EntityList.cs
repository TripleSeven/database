﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.EntityList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class EntityList
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RemoveAll;
    private static DelegateBridge __Hotfix_RemoveDeleted;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_Pause;

    [MethodImpl((MethodImplOptions) 32768)]
    public EntityList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void RemoveAll<T>(List<T> list) where T : Entity
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void RemoveDeleted<T>(List<T> list) where T : Entity
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Tick<T>(List<T> list) where T : Entity
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void TickGraphic<T>(List<T> list, float dt) where T : Entity
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Draw<T>(List<T> list) where T : Entity
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Pause<T>(List<T> list, bool pause) where T : Entity
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
