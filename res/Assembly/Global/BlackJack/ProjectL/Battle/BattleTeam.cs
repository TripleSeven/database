﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleTeam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleTeam
  {
    private int m_initNotNpcActorCount;
    private int m_deadActorCount;
    private int m_monsterTotalDamage;
    private List<int> m_monsterTurnTotalDamages;
    private List<BattleActor> m_actors;
    private BattleBase m_battle;
    private int m_teamNumber;
    private Dictionary<int, int> m_groupBehaviorDict;
    private List<BehaviorGroup> m_groups;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_RemoveDeleted;
    private static DelegateBridge __Hotfix_CreateActor;
    private static DelegateBridge __Hotfix_RemoveAll;
    private static DelegateBridge __Hotfix_GetActors;
    private static DelegateBridge __Hotfix_GetActorsWithoutId;
    private static DelegateBridge __Hotfix_GetActorById;
    private static DelegateBridge __Hotfix_GetActorByHeroId;
    private static DelegateBridge __Hotfix_StartBattle;
    private static DelegateBridge __Hotfix_StopBattle;
    private static DelegateBridge __Hotfix_HasAliveActor;
    private static DelegateBridge __Hotfix_HasNotActionFinishedActor;
    private static DelegateBridge __Hotfix_HasFinishActionActor;
    private static DelegateBridge __Hotfix_HasFinishActionNpcActor;
    private static DelegateBridge __Hotfix_GetOtherTeam;
    private static DelegateBridge __Hotfix_OnMyActorHit;
    private static DelegateBridge __Hotfix_OnMyActorDie;
    private static DelegateBridge __Hotfix_OnMyActorRetreat;
    private static DelegateBridge __Hotfix_ComputeDeadActorCount;
    private static DelegateBridge __Hotfix_ComputeAliveActorCount;
    private static DelegateBridge __Hotfix_IsGroupDie;
    private static DelegateBridge __Hotfix_NextTurn;
    private static DelegateBridge __Hotfix_GetMonsterTotalDamage;
    private static DelegateBridge __Hotfix_SaveMonsterTurnTotalDamage;
    private static DelegateBridge __Hotfix_GetMonsterTurnTotalDamage;
    private static DelegateBridge __Hotfix_GetMonsterAllTurnDamages;
    private static DelegateBridge __Hotfix_get_Battle;
    private static DelegateBridge __Hotfix_get_TeamNumber;
    private static DelegateBridge __Hotfix_get_InitNotNpcActorCount;
    private static DelegateBridge __Hotfix_get_DeadActorCount;
    private static DelegateBridge __Hotfix_SetGroupBehaviorDict;
    private static DelegateBridge __Hotfix_AddGroupBehaviors;
    private static DelegateBridge __Hotfix_CreateGroups;
    private static DelegateBridge __Hotfix_GetGroup;
    private static DelegateBridge __Hotfix_CreateGroup;
    private static DelegateBridge __Hotfix_DoGroupBehavior;
    private static DelegateBridge __Hotfix_UpdateGroups;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam(BattleBase battle, int teamNumer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveDeleted()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor CreateActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAll()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> GetActorsWithoutId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorById(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActorByHeroId(int heroId, bool ignoreDead = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAliveActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasNotActionFinishedActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFinishActionActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFinishActionNpcActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleTeam GetOtherTeam()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorHit(
      BattleActor actor,
      BattleActor attacker,
      int heroHpModify,
      int soldierHpModify)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorDie(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnMyActorRetreat(
      BattleActor actor,
      int effectType,
      string fxName,
      bool notifyListener)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeDeadActorCount(
      List<int> heroIds,
      NpcCondition npcCondition,
      int killerHeroId = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeAliveActorCount(List<int> heroIds, NpcCondition npcCondition)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGroupDie(int groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterTotalDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveMonsterTurnTotalDamage(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterTurnTotalDamage(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetMonsterAllTurnDamages()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int InitNotNpcActorCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int DeadActorCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupBehaviorDict(Dictionary<int, int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddGroupBehaviors(Dictionary<int, int> behaviors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateGroups()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorGroup GetGroup(int groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorGroup CreateGroup(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoGroupBehavior()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGroups()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
