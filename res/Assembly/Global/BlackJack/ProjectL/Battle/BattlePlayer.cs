﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattlePlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattlePlayer
  {
    public ulong SessionId;
    public int PlayerLevel;
    public BattlePlayerTrainingTech[] TrainingTechs;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetTrainingTechSoldierSkillInfos;
    private static DelegateBridge __Hotfix_GetTrainingTechSummonSkills;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> GetTrainingTechSoldierSkillInfos(
      IConfigDataLoader configDataLoader,
      ConfigDataSoldierInfo soldierInfo,
      out int soldierSkillLevelUp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataSkillInfo> GetTrainingTechSummonSkills(
      IConfigDataLoader configDataLoader,
      ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
