﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleProperty
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleProperty
  {
    public int HealthPointMax;
    public int Attack;
    public int Defense;
    public int Magic;
    public int MagicDefense;
    public int Dexterity;
    public int Buff_PhysicalDamageMul;
    public int Buff_PhysicalDamageReceiveMul;
    public int Buff_SuperPhysicalDamageReceiveMul;
    public int Buff_HealMul;
    public int Buff_HealReceiveMul;
    public int Buff_MagicalDamageMul;
    public int Buff_MagicalDamageReceiveMul;
    public int Buff_SuperMagicalDamageReceiveMul;
    public int Buff_IgnoreDFMul;
    public int Buff_SkillDamageMul;
    public int Buff_BFSkillDamageMul;
    public int Buff_RangeDamageReceiveMul;
    public int Buff_ReceiveCriticalDamageAdd;
    public int Buff_ReceiveCriticalRateAdd;
    public int Buff_TrueDamageMul;
    public int Buff_ReceiveTrueDamageMul;
    public int Buff_ReceiveBFSkillDamageMul;
    public int Cmd_HealthPoint;
    public int Cmd_Attack;
    public int Cmd_Defense;
    public int Cmd_MagicDefense;
    public int CriticalDamage;
    public int CriticalRate;
    public int AttackDistanceAdd;
    public int MagicSkillDistanceAdd;
    public int PhysicalSkillDistanceAdd;
    public int SupportSkillDistanceAdd;
    public int MagicSkillRangeAdd;
    public int PhysicalSkillRangeAdd;
    public int MovePointAdd;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_CopyTo;
    private static DelegateBridge __Hotfix_ComputeHeroProperties;
    private static DelegateBridge __Hotfix_ClampNegativeValue;
    private static DelegateBridge __Hotfix_ApplyExchangeProperty;
    private static DelegateBridge __Hotfix_ApplySummonData;
    private static DelegateBridge __Hotfix_ComputeSoldierProperties;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleProperty()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CopyTo(BattleProperty p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeHeroProperties(
      IConfigDataLoader configDataLoader,
      ConfigDataHeroInfo heroInfo,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      int jobLevel,
      int heroLevel,
      int heroStar,
      BattlePropertyModifier pm,
      bool isPvp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ClampNegativeValue(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyExchangeProperty(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplySummonData(BattleActorSummonData summonData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ComputeSoldierProperties(
      IConfigDataLoader configDataLoader,
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataHeroInfo heroInfo,
      int heroLevel,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
