﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatSkillState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class CombatSkillState
  {
    public ConfigDataSkillInfo m_skillInfo;
    public ushort m_hitId;
    public int m_hitCount;
    public int m_preAttackHitCount;
    public List<DelayHit> m_delayHits;
    public bool m_isToHeroCritical;
    public bool m_isToSoldierCritical;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_AddDelayHit;
    private static DelegateBridge __Hotfix_GetDelayHitFrameMax;
    private static DelegateBridge __Hotfix_IsCritical;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatSkillState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddDelayHit(int frame, Vector2i pos, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDelayHitFrameMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCritical(bool targetIsHero)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
