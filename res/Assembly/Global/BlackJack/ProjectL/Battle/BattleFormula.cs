﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleFormula
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using FixMath.NET;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleFormula
  {
    private static Fix64 d10;
    private static Fix64 d100;
    private static Fix64 d10000;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_ComputePhysicalDamageValue;
    private static DelegateBridge __Hotfix_ComputeMagicDamageValue;
    private static DelegateBridge __Hotfix_ComputeHealFinalValue;
    private static DelegateBridge __Hotfix_ComputeHealValue_1;
    private static DelegateBridge __Hotfix_ComputePercentHealValue;
    private static DelegateBridge __Hotfix_ComputeHealValue_0;
    private static DelegateBridge __Hotfix_ComputeSkillHpModifyValue;
    private static DelegateBridge __Hotfix_ComputeBuffHealValue;
    private static DelegateBridge __Hotfix_ComputeBuffDamageValue;
    private static DelegateBridge __Hotfix_ComputePercentValue;
    private static DelegateBridge __Hotfix_ComputeHeroBattleProperty;
    private static DelegateBridge __Hotfix_ComputeSoldierBattleProperty;
    private static DelegateBridge __Hotfix_ComputeEquipmentPropertyAdd;
    private static DelegateBridge __Hotfix_ComputeHeroBattlePower;
    private static DelegateBridge __Hotfix_ComputeSoldierBattlePower;
    private static DelegateBridge __Hotfix_ComputeDamangeNumberType;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleFormula()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Fix64 ComputePhysicalDamageValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ConfigDataSkillInfo skillInfo,
      int skillPower,
      bool isCritical,
      int targetTerrainBonus,
      int armyAttack,
      int armyDefend,
      int meleePunish,
      bool useMagicAsAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Fix64 ComputeMagicDamageValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ConfigDataSkillInfo skillInfo,
      int skillPower,
      bool isCritical,
      int targetTerrainBonus,
      int armyMagic,
      int armyMagicDefend,
      int meleePunish,
      bool useAttackAsMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 ComputeHealFinalValue(
      Fix64 baseHeal,
      int attackerBuff_HealMul,
      int targetBuff_HealReceiveMul)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 ComputeHealValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      int skillPower,
      int attackerMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Fix64 ComputePercentHealValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      int skillPower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Fix64 ComputeHealValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ConfigDataSkillInfo skillInfo,
      int skillPower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeSkillHpModifyValue(
      BattleProperty attackerProperty,
      BattleProperty targetProperty,
      ArmyRelationData armyRelation,
      ConfigDataSkillInfo skillInfo,
      int skillPower,
      bool isCritical,
      bool isBuffForceMagicDamage,
      bool isBuffForcePhysicalDamage,
      bool isBanMeleePunish,
      ConfigDataTerrainInfo targetTerrain,
      int gridDistance,
      bool isSameTeam,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeBuffHealValue(
      int value,
      int percent,
      int applyerBuff_HealMul,
      int targetBuff_HealReceiveMul)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeBuffDamageValue(
      int value,
      int percent,
      int applyerBuff_TrueDamageMul,
      int targetBuff_RecvTrueDamageMul)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputePercentValue(int value, int percent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeHeroBattleProperty(
      int jobBase,
      int jobUp,
      int heroLevel,
      int heroStarMul,
      int growMul,
      int growAdd,
      int buffMul,
      int buffAdd,
      int selfMul,
      int selfAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeSoldierBattleProperty(
      int soldierBase,
      int soldierUp,
      int heroLevel,
      int heroCmdMul,
      int growMul,
      int growAdd,
      int buffMul,
      int buffAdd)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeEquipmentPropertyAdd(int baseValue, int levelUpValue, int level)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeHeroBattlePower(
      ConfigDataJobInfo jobInfo,
      int hp,
      int at,
      int df,
      int magic,
      int magicDf,
      int dex,
      int skillBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int ComputeSoldierBattlePower(
      ConfigDataSoldierInfo soldierInfo,
      int hp,
      int at,
      int df,
      int magicDf,
      int skillBattlePower)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static DamageNumberType ComputeDamangeNumberType(
      ConfigDataSkillInfo skillInfo,
      bool isCritical,
      ArmyRelationData armyRelation,
      bool isSameTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static BattleFormula()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
