﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ActorAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class ActorAnim
  {
    public const string Idle = "idle";
    public const string Walk = "walk";
    public const string Run = "run";
    public const string Attack = "attack1";
    public const string SuperAttack = "superattack";
    public const string Sing = "sing";
    public const string Cast = "cast";
    public const string Hurt = "hurt";
    public const string Stun = "stun";
    public const string Die = "death";
    public const string Die1 = "death1";
    public const string Die2 = "death2";
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ActorAnim()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
