﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleEventTriggerState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleEventTriggerState
  {
    public BattleConditionStatus m_status;
    public int m_triggerCount;
    public int m_triggerTurnCount;
    public int m_triggerTurn;
    public ConfigDataBattleEventTriggerInfo m_triggerInfo;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsProgress;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleEventTriggerState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsProgress()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
