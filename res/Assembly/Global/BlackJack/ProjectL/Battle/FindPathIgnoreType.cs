﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.FindPathIgnoreType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 972A2846-EBE4-4323-8745-F884A18FA509
// Assembly location: D:\Games\Langrisser5\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.Battle
{
  public enum FindPathIgnoreType
  {
    IgnoreTeam0,
    IgnoreTeam1,
    IgnoreNone,
    IgnoreAllTeam,
    IgnoreTeam0_Cost,
    IgnoreTeam1_Cost,
  }
}
