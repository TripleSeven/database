﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.UtilityTools;
using FixMath.NET;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleActor : Entity
  {
    private BattleTeam m_team;
    private int m_initTeamNumber;
    private GridPosition m_position;
    private int m_direction;
    private GridPosition m_initPosition;
    private GridPosition m_beforeGuardPosition;
    private BattleProperty m_heroBattleProperty;
    private BattleProperty m_soldierBattleProperty;
    private int m_heroHealthPoint;
    private int m_soldierTotalHealthPoint;
    private int m_soldierSingleHealthPointMax;
    private int m_initSoldierCount;
    private int m_heroLevel;
    private int m_heroStar;
    private int m_jobLevel;
    private BattleActorMasterJob[] m_masterJobs;
    private BattleActorEquipment[] m_equipments;
    private ConfigDataSkillInfo[] m_resonanceSkillInfos;
    private ConfigDataSkillInfo[] m_fetterSkillInfos;
    private int m_actionValue;
    private MoveType m_moveType;
    private bool m_isInAction;
    private bool m_isActionFinished;
    private int m_buffNewTurnCount;
    private int m_hasExtraActionMovePoint;
    private int m_hasExtraMovingMovePoint;
    private ExtraActionType m_curExtraActionType;
    private int m_curExtraActionMovePoint;
    private bool m_isVisible;
    private bool m_isProcessingDie;
    private bool m_isProcessedDie;
    private bool m_isPostProcessedDie;
    private int m_beforeDieHeroHealthPoint;
    private int m_beforeDieSoldierHealthPoint;
    private bool m_isRetreat;
    private List<BattleSkillState> m_skillStates;
    private List<BuffState> m_buffStates;
    private int m_buffIdCounter;
    private ulong m_buffTypes;
    private uint m_buffTypes2;
    private uint m_fightTags;
    private BattleActorSummonData m_summonData;
    private BattleActorSummonData m_summonSourceData;
    private BattleActor m_killerActor;
    private List<BattleActor> m_pendingProcessDieActors;
    private bool m_isNpc;
    private bool m_isPlayerNpc;
    private bool m_isInitNpc;
    private BattleActorSourceType m_sourceType;
    private ConfigDataHeroInfo m_heroInfo;
    private ConfigDataJobConnectionInfo m_jobConnectionInfo;
    private ConfigDataArmyInfo m_heroArmyInfo;
    private ConfigDataSoldierInfo m_soldierInfo;
    private ConfigDataArmyInfo m_soldierArmyInfo;
    private ConfigDataCharImageSkinResourceInfo m_heroCharImageSkinResourceInfo;
    private ConfigDataModelSkinResourceInfo m_heroModelSkinResourceInfo;
    private ConfigDataModelSkinResourceInfo m_soldierModelSkinResourceInfo;
    private ConfigDataCharImageInfo m_heroVoiceCharImageInfo;
    private ConfigDataSkillInfo[] m_extraPassiveSkillInfos;
    private ConfigDataSkillInfo m_extraTalentSkillInfo;
    private bool m_isActionCriticalAttack;
    private bool m_isActionKillActor;
    private bool m_isActionDamageActor;
    private bool m_isActionAttacker;
    private bool m_isActionGuarder;
    private short m_actionBattlefieldDamageSkillTargetCount;
    private bool m_isBeCriticalAttack;
    private int m_actionMoveGrids;
    private int m_actionRemainMovePoint;
    private int m_actionRemainSpecialMovePoint;
    private GridPosition m_actionBeginPosition;
    private ConfigDataSkillInfo m_lastDamageBySkillInfo;
    private int m_firstDamageTurn;
    private bool m_isTurnDamage;
    private ulong m_satisfyConditions;
    private short m_actionCount;
    private short m_combatAttackCount;
    private short m_beCombatAttackCount;
    private short m_useSkillCount;
    private short m_killActorCount;
    private int m_applyMonsterTotalDamage;
    private int m_dieTurn;
    private int m_dieBossPhase;
    private int m_deathAnimType;
    private uint m_executedCommandTypes;
    private ConfigDataSkillInfo m_executedSkillInfo;
    private int m_playerIndex;
    private ConfigDataBehavior m_curBehaviorCfg;
    private BattleActor.BehaviorState m_curBehaviorState;
    private BehaviorTarget m_moveTarget;
    private BehaviorTarget m_attackTarget;
    private int m_attackSkillIndex;
    private int[] m_beAttackedCountOfTurns;
    private int m_groupId;
    private int m_aiCreateBattleCommandCount;
    private int m_aiBeginTurn;
    private Fix64 m_healSkillTargetHPThresh;
    private GridPosition m_directionalAttackPosition;
    private BehaviorTarget m_directionalAttackTarget;
    private bool m_isHPEverDecreased;
    private bool m_isEverDebuffedByEnemy;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_InitializeSkin;
    private static DelegateBridge __Hotfix_InitializeExtraPassiveSkillAndTalent;
    private static DelegateBridge __Hotfix_InitializeEnd;
    private static DelegateBridge __Hotfix_UpdateBattleProperties;
    private static DelegateBridge __Hotfix_UpdateBattlePropertiesInCombat;
    private static DelegateBridge __Hotfix_UpdateBattlePropertiesInBattlefieldSkill;
    private static DelegateBridge __Hotfix_ComputeBattleProperties;
    private static DelegateBridge __Hotfix_CollectJobMasterPropertyModifiers;
    private static DelegateBridge __Hotfix_CollectJobEquipmentRefineryPropertyModifier;
    private static DelegateBridge __Hotfix_CollectEquipmentPropertyModifiers;
    private static DelegateBridge __Hotfix_SetPosition;
    private static DelegateBridge __Hotfix_SetDirection;
    private static DelegateBridge __Hotfix_FaceTo;
    private static DelegateBridge __Hotfix_GuardMove;
    private static DelegateBridge __Hotfix_UnguardMove;
    private static DelegateBridge __Hotfix_MoveTo;
    private static DelegateBridge __Hotfix_ClearMapActor;
    private static DelegateBridge __Hotfix_GetTerrainInfo;
    private static DelegateBridge __Hotfix_GetTerrainEffectInfo;
    private static DelegateBridge __Hotfix_GetBuffEffectedTerrainInfo;
    private static DelegateBridge __Hotfix_HasBuffEffectedHeroTag;
    private static DelegateBridge __Hotfix_ChangeTeam;
    private static DelegateBridge __Hotfix_CreateBattleCommand;
    private static DelegateBridge __Hotfix_StartBattle;
    private static DelegateBridge __Hotfix_StopBattle;
    private static DelegateBridge __Hotfix_NextTurn;
    private static DelegateBridge __Hotfix_ActionBegin;
    private static DelegateBridge __Hotfix_ActionEnd;
    private static DelegateBridge __Hotfix_IsActionFinished;
    private static DelegateBridge __Hotfix_CanAction;
    private static DelegateBridge __Hotfix_PostActionTerrainDamage;
    private static DelegateBridge __Hotfix_OnTerrainDamage;
    private static DelegateBridge __Hotfix_FindPath_0;
    private static DelegateBridge __Hotfix_FindPath_1;
    private static DelegateBridge __Hotfix_FindMoveRegion;
    private static DelegateBridge __Hotfix_ShouldLog;
    private static DelegateBridge __Hotfix_AddExecutedCommandType;
    private static DelegateBridge __Hotfix_HasExecutedCommandType;
    private static DelegateBridge __Hotfix_IsExecutedCommandType;
    private static DelegateBridge __Hotfix_ExecuteMoveCommand;
    private static DelegateBridge __Hotfix_ExecutePerformMoveCommand;
    private static DelegateBridge __Hotfix_ExecuteCombatCommand;
    private static DelegateBridge __Hotfix_ExecuteSkillCommand_0;
    private static DelegateBridge __Hotfix_ExecuteSkillCommand_1;
    private static DelegateBridge __Hotfix_ExecuteDoneCommand;
    private static DelegateBridge __Hotfix_IsDead;
    private static DelegateBridge __Hotfix_IsRetreat;
    private static DelegateBridge __Hotfix_IsDeadOrRetreat;
    private static DelegateBridge __Hotfix_SetVisible;
    private static DelegateBridge __Hotfix_IsVisible;
    private static DelegateBridge __Hotfix_IsInvincible;
    private static DelegateBridge __Hotfix_IsSummoned;
    private static DelegateBridge __Hotfix_IsNpc;
    private static DelegateBridge __Hotfix_IsAINpc;
    private static DelegateBridge __Hotfix_IsPlayerNpc;
    private static DelegateBridge __Hotfix_IsPlayerActor;
    private static DelegateBridge __Hotfix_IsAIActor;
    private static DelegateBridge __Hotfix_IsEventOrPerformActor;
    private static DelegateBridge __Hotfix_IsMonsterBoss;
    private static DelegateBridge __Hotfix_IsMonsterNoDie;
    private static DelegateBridge __Hotfix_IsMonsterAddDamage;
    private static DelegateBridge __Hotfix_GetMonsterActionLoopCount;
    private static DelegateBridge __Hotfix_CanBeTarget;
    private static DelegateBridge __Hotfix_SetHeroHealthPoint;
    private static DelegateBridge __Hotfix_SetSoldierTotalHealthPoint;
    private static DelegateBridge __Hotfix_ModifyHeroAndSoldierHealthPoint;
    private static DelegateBridge __Hotfix_CheckDie;
    private static DelegateBridge __Hotfix_ProcessDie;
    private static DelegateBridge __Hotfix_PostProcessDie;
    private static DelegateBridge __Hotfix_AppendPendingProcessDieActor;
    private static DelegateBridge __Hotfix_Retreat;
    private static DelegateBridge __Hotfix_GetHeroBaseAttackDistance;
    private static DelegateBridge __Hotfix_GetSoldierBaseAttackDistance;
    private static DelegateBridge __Hotfix_GetHeroAttackDistance;
    private static DelegateBridge __Hotfix_GetSoldierAttackDistance;
    private static DelegateBridge __Hotfix_GetMaxAttackDistance;
    private static DelegateBridge __Hotfix_GetTalentSkillInfo;
    private static DelegateBridge __Hotfix_GetSkillDistance;
    private static DelegateBridge __Hotfix_GetSkillRange;
    private static DelegateBridge __Hotfix_GetHeroBaseMovePoint;
    private static DelegateBridge __Hotfix_GetSoldierBaseMovePoint;
    private static DelegateBridge __Hotfix_GetMovePoint;
    private static DelegateBridge __Hotfix_GetHeroMoveType;
    private static DelegateBridge __Hotfix_GetSoldierMoveType;
    private static DelegateBridge __Hotfix_GetMoveType;
    private static DelegateBridge __Hotfix_ComputeMoveType;
    private static DelegateBridge __Hotfix_ComputeDefaultMoveType;
    private static DelegateBridge __Hotfix_ComputeMoveTypeMin;
    private static DelegateBridge __Hotfix_ComputeMoveTypeMax;
    private static DelegateBridge __Hotfix_GetMoveTypePriority;
    private static DelegateBridge __Hotfix_GetTotalHealthPoint;
    private static DelegateBridge __Hotfix_GetTotalHealthPointMax;
    private static DelegateBridge __Hotfix_GetTotalHealthPointPercent;
    private static DelegateBridge __Hotfix_ComputeCombatSoldierCount;
    private static DelegateBridge __Hotfix_SetLastDamageBySkill;
    private static DelegateBridge __Hotfix_SetLastDamageByBuff;
    private static DelegateBridge __Hotfix_GetLastDamageBySkill;
    private static DelegateBridge __Hotfix_GetFirstDamageTurn;
    private static DelegateBridge __Hotfix_IsTurnDamage;
    private static DelegateBridge __Hotfix_SetSetisfyCondition;
    private static DelegateBridge __Hotfix_IsSatisfyCondition;
    private static DelegateBridge __Hotfix_GetDeathAnimType;
    private static DelegateBridge __Hotfix_SetBeCriticalAttack;
    private static DelegateBridge __Hotfix_SetActionCriticalAttack;
    private static DelegateBridge __Hotfix_SetActionKillActor;
    private static DelegateBridge __Hotfix_SetActionDamageActor;
    private static DelegateBridge __Hotfix_SetBeforeDieHealthPoint;
    private static DelegateBridge __Hotfix_IsExtraAction;
    private static DelegateBridge __Hotfix_IsExtraMoving;
    private static DelegateBridge __Hotfix_GetActionCount;
    private static DelegateBridge __Hotfix_IncreaseCombatAttackCount;
    private static DelegateBridge __Hotfix_GetCombatAttackCount;
    private static DelegateBridge __Hotfix_IncreaseBeCombatAttackCount;
    private static DelegateBridge __Hotfix_GetBeCombatAttackCount;
    private static DelegateBridge __Hotfix_IncreaseUseSkillCount;
    private static DelegateBridge __Hotfix_GetUseSkillCount;
    private static DelegateBridge __Hotfix_IncreaseKillActorCount;
    private static DelegateBridge __Hotfix_GetKillActorCount;
    private static DelegateBridge __Hotfix_GetKillerActor;
    private static DelegateBridge __Hotfix_AddApplyMonsterTotalDamage;
    private static DelegateBridge __Hotfix_GetApplyMonsterTotalDamage;
    private static DelegateBridge __Hotfix_GetDieTurn;
    private static DelegateBridge __Hotfix_GetDieBossPhase;
    private static DelegateBridge __Hotfix_GetSourceType;
    private static DelegateBridge __Hotfix_GetTrainingTechBattlePlayer;
    private static DelegateBridge __Hotfix_get_Position;
    private static DelegateBridge __Hotfix_get_InitPosition;
    private static DelegateBridge __Hotfix_get_Direction;
    private static DelegateBridge __Hotfix_get_Battle;
    private static DelegateBridge __Hotfix_get_Team;
    private static DelegateBridge __Hotfix_get_TeamNumber;
    private static DelegateBridge __Hotfix_get_HeroBattleProperty;
    private static DelegateBridge __Hotfix_get_SoldierBattleProperty;
    private static DelegateBridge __Hotfix_get_HeroHealthPoint;
    private static DelegateBridge __Hotfix_get_SoldierTotalHealthPoint;
    private static DelegateBridge __Hotfix_get_SoldierSingleHealthPointMax;
    private static DelegateBridge __Hotfix_get_FightTags;
    private static DelegateBridge __Hotfix_get_SummonData;
    private static DelegateBridge __Hotfix_get_HeroLevel;
    private static DelegateBridge __Hotfix_get_HeroStar;
    private static DelegateBridge __Hotfix_get_JobLevel;
    private static DelegateBridge __Hotfix_get_ActionValue;
    private static DelegateBridge __Hotfix_set_ActionValue;
    private static DelegateBridge __Hotfix_get_HeroInfo;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_get_HeroArmyInfo;
    private static DelegateBridge __Hotfix_get_HeroArmyId;
    private static DelegateBridge __Hotfix_get_JobConnectionInfo;
    private static DelegateBridge __Hotfix_get_JobInfo;
    private static DelegateBridge __Hotfix_get_SoldierInfo;
    private static DelegateBridge __Hotfix_get_SoldierArmyInfo;
    private static DelegateBridge __Hotfix_get_HeroCharImageSkinResourceInfo;
    private static DelegateBridge __Hotfix_get_HeroModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_get_SoldierModelSkinResourceInfo;
    private static DelegateBridge __Hotfix_get_HeroVoiceCharImageInfo;
    private static DelegateBridge __Hotfix_get_PlayerIndex;
    private static DelegateBridge __Hotfix_FindMoveAndAttackRegion;
    private static DelegateBridge __Hotfix_FindRandomEmptyPosition;
    private static DelegateBridge __Hotfix_FindAttackPosition;
    private static DelegateBridge __Hotfix_ComputeActorScoreDamage;
    private static DelegateBridge __Hotfix_ComputeActorScoreHeal;
    private static DelegateBridge __Hotfix_ComputeActorScoreBuff;
    private static DelegateBridge __Hotfix_SelectNearestTarget;
    private static DelegateBridge __Hotfix_GetAIRandomNumber;
    private static DelegateBridge __Hotfix_GetSkillTargetTeam;
    private static DelegateBridge __Hotfix_CanAttackOrUseSkill;
    private static DelegateBridge __Hotfix_get_Group;
    private static DelegateBridge __Hotfix_set_Group;
    private static DelegateBridge __Hotfix_get_GroupId;
    private static DelegateBridge __Hotfix_IncreaseBeAttackedCount;
    private static DelegateBridge __Hotfix_get_IsAttackedByEnemy;
    private static DelegateBridge __Hotfix_get_IsAttackedByEnemyInLastTrun;
    private static DelegateBridge __Hotfix_get_InstanceID;
    private static DelegateBridge __Hotfix_GetBehaviorId;
    private static DelegateBridge __Hotfix_SetBehavior;
    private static DelegateBridge __Hotfix_SetBehaviorState;
    private static DelegateBridge __Hotfix_CheckBehaviorCondition;
    private static DelegateBridge __Hotfix_DoBehaviorChangeRules;
    private static DelegateBridge __Hotfix_get_NextBehaviorByChangeRules;
    private static DelegateBridge __Hotfix_FindEmptyGridInCanAttackAndTouchRange;
    private static DelegateBridge __Hotfix_FindActorsInCanAttackAndTouchRange;
    private static DelegateBridge __Hotfix_FindFarthestPosition;
    private static DelegateBridge __Hotfix_FindNearestPosition;
    private static DelegateBridge __Hotfix_FindNearestActor;
    private static DelegateBridge __Hotfix_DoSelectTarget;
    private static DelegateBridge __Hotfix_FindActorsByIDFilter;
    private static DelegateBridge __Hotfix_Contains;
    private static DelegateBridge __Hotfix_SelectExtraMoveTarget;
    private static DelegateBridge __Hotfix_SelectMoveTarget;
    private static DelegateBridge __Hotfix_GenerateCommandOfMove_0;
    private static DelegateBridge __Hotfix_GenerateCommandOfMove_1;
    private static DelegateBridge __Hotfix_FindPositionToMoveToTarget_0;
    private static DelegateBridge __Hotfix_GetMovePoint4AI;
    private static DelegateBridge __Hotfix_GetSpecialMoveCostType4AI;
    private static DelegateBridge __Hotfix_FindMoveRegion4AI;
    private static DelegateBridge __Hotfix_FindPositionToMoveToTarget_1;
    private static DelegateBridge __Hotfix_ComputeRestrictScore;
    private static DelegateBridge __Hotfix_GetArmyRistrictScore;
    private static DelegateBridge __Hotfix_get_TotalHPPercent;
    private static DelegateBridge __Hotfix_SelectMinHPPercentActor;
    private static DelegateBridge __Hotfix_FindActorsAlive;
    private static DelegateBridge __Hotfix_DefaultSelectDamageSkillTarget;
    private static DelegateBridge __Hotfix_IsSelectRangeSkill;
    private static DelegateBridge __Hotfix_get_BehaviorSelectSkillInfo;
    private static DelegateBridge __Hotfix_FindActorsInGrids;
    private static DelegateBridge __Hotfix_FindMaxAoeSkillCoverPosition;
    private static DelegateBridge __Hotfix_FindMaxAoeSkillCoverActor;
    private static DelegateBridge __Hotfix_DefaultSelectAttackTarget;
    private static DelegateBridge __Hotfix_SelectAttackTargetWithSummonSkill;
    private static DelegateBridge __Hotfix_FindGridsLessEqualDistance;
    private static DelegateBridge __Hotfix_FindActorsInCrossLine;
    private static DelegateBridge __Hotfix_FindActorsLessEqualDistance;
    private static DelegateBridge __Hotfix_DefaultSelectHealSkillTarget;
    private static DelegateBridge __Hotfix_DefaultSelectBuffSkillTarget;
    private static DelegateBridge __Hotfix_SelectAttackTargetInSkillRange;
    private static DelegateBridge __Hotfix_FindActorsWithBuffN;
    private static DelegateBridge __Hotfix_FindActorsWithSkillAITypeN;
    private static DelegateBridge __Hotfix_SelectAttackTarget;
    private static DelegateBridge __Hotfix_SelectSkill;
    private static DelegateBridge __Hotfix_SelectSkillDirectReachTarget;
    private static DelegateBridge __Hotfix_HasInt;
    private static DelegateBridge __Hotfix_DefaultSelectSkill;
    private static DelegateBridge __Hotfix_FindActorsInCanNormalAttackAndTouchRange;
    private static DelegateBridge __Hotfix_Direction2Position;
    private static DelegateBridge __Hotfix_IsSkillAGoodAISelection;
    private static DelegateBridge __Hotfix_FindCastSkillPosition;
    private static DelegateBridge __Hotfix_FindAttackPositions;
    private static DelegateBridge __Hotfix_GenerateCommandOfAttack;
    private static DelegateBridge __Hotfix_DoBehaviorMove;
    private static DelegateBridge __Hotfix_IsAttackTargetStillValid;
    private static DelegateBridge __Hotfix_DoBehaviorAttack;
    private static DelegateBridge __Hotfix_GenerateAIBattleCommand;
    private static DelegateBridge __Hotfix_AICreateBattleCommand;
    private static DelegateBridge __Hotfix_SelectAttackRegionTarget;
    private static DelegateBridge __Hotfix_GetBuffStates;
    private static DelegateBridge __Hotfix_GetBuffIds;
    private static DelegateBridge __Hotfix_InitializeBuffs;
    private static DelegateBridge __Hotfix_AttachPassiveSkillBuffs;
    private static DelegateBridge __Hotfix_AttachBuff;
    private static DelegateBridge __Hotfix_AttachBuffList;
    private static DelegateBridge __Hotfix_CompareBuffOrder;
    private static DelegateBridge __Hotfix_RemoveBuff;
    private static DelegateBridge __Hotfix_RemoveBuffList_0;
    private static DelegateBridge __Hotfix_RemoveBuffList_1;
    private static DelegateBridge __Hotfix_FindBuff;
    private static DelegateBridge __Hotfix_RemoveAllBuffs;
    private static DelegateBridge __Hotfix_SkillDispelBuff;
    private static DelegateBridge __Hotfix_HasBuff;
    private static DelegateBridge __Hotfix_CollectBuffPropertyModifiersAndFightTags_0;
    private static DelegateBridge __Hotfix_CollectBuffPropertyModifiersAndFightTags_1;
    private static DelegateBridge __Hotfix_CollectPropertyModifier;
    private static DelegateBridge __Hotfix_CollectOtherActorBuffPropertyModifiersAndFightTags;
    private static DelegateBridge __Hotfix_CollectBuffPropertyModifiersAndFightTagsInCombat;
    private static DelegateBridge __Hotfix_CollectDistancePropertyModifiers;
    private static DelegateBridge __Hotfix_CollectBuffPropertyExchange;
    private static DelegateBridge __Hotfix_CollectBuffSummonData;
    private static DelegateBridge __Hotfix_CollectBuffPropertyReplace;
    private static DelegateBridge __Hotfix_BuffMoveTypeChange;
    private static DelegateBridge __Hotfix_BuffSpecialMoveCostType;
    private static DelegateBridge __Hotfix_UpdateAllAuras;
    private static DelegateBridge __Hotfix_RemoveAuraAppliedBuffs;
    private static DelegateBridge __Hotfix_RemovePackChildBuffs;
    private static DelegateBridge __Hotfix_RemoveZeroTimeBuff;
    private static DelegateBridge __Hotfix_OnBuffHit;
    private static DelegateBridge __Hotfix_HasFightTag;
    private static DelegateBridge __Hotfix_UpdateBuffTypes;
    private static DelegateBridge __Hotfix_SetBuffType;
    private static DelegateBridge __Hotfix_HasBuffType;
    private static DelegateBridge __Hotfix_IsBuffEffective;
    private static DelegateBridge __Hotfix_IsBuffHpConditionSatisfied_0;
    private static DelegateBridge __Hotfix_IsBuffHpConditionSatisfied_1;
    private static DelegateBridge __Hotfix_IsBuffEffectiveConditionSatisfied;
    private static DelegateBridge __Hotfix_IsBuffCombatConditionSatisfied_1;
    private static DelegateBridge __Hotfix_IsBuffCombatConditionSatisfied_0;
    private static DelegateBridge __Hotfix_ComputeNeighborAliveActorCount;
    private static DelegateBridge __Hotfix_IsBuffCooldown;
    private static DelegateBridge __Hotfix_StartBuffCooldown;
    private static DelegateBridge __Hotfix_ComputeBuffCount;
    private static DelegateBridge __Hotfix_ComputeEnhanceOrDebuffCount;
    private static DelegateBridge __Hotfix_IsImmuneBuffSubType;
    private static DelegateBridge __Hotfix_BuffReboundDamage;
    private static DelegateBridge __Hotfix_BuffHealChangeDamage;
    private static DelegateBridge __Hotfix_BuffBeHealChangeDamage;
    private static DelegateBridge __Hotfix_OnBuffHealOrChangeDamageHit;
    private static DelegateBridge __Hotfix_GetBuffOverrideMovePointCost;
    private static DelegateBridge __Hotfix_GetBuffOverrideTerrainInfo;
    private static DelegateBridge __Hotfix_AddBuffArmyRelationAttack;
    private static DelegateBridge __Hotfix_AddBuffArmyRelationDefend;
    private static DelegateBridge __Hotfix_AddBuffArmyRelationModifyAttack;
    private static DelegateBridge __Hotfix_AddBuffArmyRelationModifyDefend;
    private static DelegateBridge __Hotfix_IsBuffIgnoreArmyRelation;
    private static DelegateBridge __Hotfix_IsBuffForceMagicDamage;
    private static DelegateBridge __Hotfix_IsBuffForcePhysicalDamage;
    private static DelegateBridge __Hotfix_GetBuffTimeChange;
    private static DelegateBridge __Hotfix_GetBuffTimeChange2;
    private static DelegateBridge __Hotfix_IsBuffModifyHeroTag;
    private static DelegateBridge __Hotfix_GetBuffHeroAuraModify;
    private static DelegateBridge __Hotfix_GetBuffReplaceRuleModify;
    private static DelegateBridge __Hotfix_ActionBeginBuffEffect;
    private static DelegateBridge __Hotfix_ActionEndBuffEffect;
    private static DelegateBridge __Hotfix_UpdateBuffTime;
    private static DelegateBridge __Hotfix_ActionEndBuffDamageTeleport;
    private static DelegateBridge __Hotfix_ActionEndBuffDoubleMove;
    private static DelegateBridge __Hotfix_ActionEndBuffHealOverTime;
    private static DelegateBridge __Hotfix_ActionEndBuffDamageOverTime;
    private static DelegateBridge __Hotfix_ActionEndAddBuff;
    private static DelegateBridge __Hotfix_ActionEndAddBuffSuper;
    private static DelegateBridge __Hotfix_ActionEndAddMovePoint;
    private static DelegateBridge __Hotfix_ActionEndRemoveDebuff;
    private static DelegateBridge __Hotfix_ActionEndRemoveEnhanceBuff;
    private static DelegateBridge __Hotfix_ActionEndBuffBattlefieldSkill;
    private static DelegateBridge __Hotfix_ActionEndBuffNewTurn;
    private static DelegateBridge __Hotfix_CompareHealActor;
    private static DelegateBridge __Hotfix_CombatBuffHeal;
    private static DelegateBridge __Hotfix_CombatBuffHealOther;
    private static DelegateBridge __Hotfix_CombatBuffDamage;
    private static DelegateBridge __Hotfix_CombatBuffApplyBuff;
    private static DelegateBridge __Hotfix_IsValueMultiply;
    private static DelegateBridge __Hotfix_CombatBuffRemoveDebuff;
    private static DelegateBridge __Hotfix_CombatBuffBanGuard;
    private static DelegateBridge __Hotfix_CombatBuffAttackAid;
    private static DelegateBridge __Hotfix_CombatBuffDefenseAid;
    private static DelegateBridge __Hotfix_ComputeBuffSkillHpModify;
    private static DelegateBridge __Hotfix_GetBuffCombatAidModify;
    private static DelegateBridge __Hotfix_AttackApplyBuff;
    private static DelegateBridge __Hotfix_KillApplyBuff;
    private static DelegateBridge __Hotfix_AttackBuffDamage;
    private static DelegateBridge __Hotfix_AttackRemoveBuff;
    private static DelegateBridge __Hotfix_AttackRemoveSkillCooldown;
    private static DelegateBridge __Hotfix_BeforeBattlefieldSkillApplyBuff;
    private static DelegateBridge __Hotfix_BattleFieldSkillApplyBuff;
    private static DelegateBridge __Hotfix_CompareActorByPositionX_Ascending;
    private static DelegateBridge __Hotfix_CompareActorByPositionX_Descending;
    private static DelegateBridge __Hotfix_CompareActorByPositionY_Ascending;
    private static DelegateBridge __Hotfix_CompareActorByPositionY_Descending;
    private static DelegateBridge __Hotfix_AttackBuffPunch;
    private static DelegateBridge __Hotfix_AttackBuffDrag;
    private static DelegateBridge __Hotfix_AttackBuffExchangePosition;
    private static DelegateBridge __Hotfix_CombatBuffArmyChange;
    private static DelegateBridge __Hotfix_DamageBuffHealOther;
    private static DelegateBridge __Hotfix_BuffDoubleAttack;
    private static DelegateBridge __Hotfix_BuffDoubleSkill;
    private static DelegateBridge __Hotfix_BuffUndead;
    private static DelegateBridge __Hotfix_DieBuffBattlefieldSkill;
    private static DelegateBridge __Hotfix_DieBuffSummonSkill;
    private static DelegateBridge __Hotfix_UndeadBuffBattlefieldSkill;
    private static DelegateBridge __Hotfix_CanBuffGuard;
    private static DelegateBridge __Hotfix_GetGuardActor;
    private static DelegateBridge __Hotfix_GetSkillState;
    private static DelegateBridge __Hotfix_GetSkillStateById;
    private static DelegateBridge __Hotfix_GetSkillStates;
    private static DelegateBridge __Hotfix_IsSkillUseable;
    private static DelegateBridge __Hotfix_IsCombineSkillUseable;
    private static DelegateBridge __Hotfix_GetCombineSkillActors;
    private static DelegateBridge __Hotfix_IsSkillBanned;
    private static DelegateBridge __Hotfix_CanUseSkillOnTarget;
    private static DelegateBridge __Hotfix_CanBeTeleport;
    private static DelegateBridge __Hotfix_IsBattlefiledSkillApplyTarget;
    private static DelegateBridge __Hotfix_FindBattlefieldSkillAttackRegion;
    private static DelegateBridge __Hotfix_FindBattlefieldSkillApplyTargets;
    private static DelegateBridge __Hotfix_ExecuteBattlefieldSkill;
    private static DelegateBridge __Hotfix_AttachSkillSelfBuffs;
    private static DelegateBridge __Hotfix_SkillAttack;
    private static DelegateBridge __Hotfix_SkillAttackBy;
    private static DelegateBridge __Hotfix_OnSkillHit;
    private static DelegateBridge __Hotfix_SkillAttackEnd;
    private static DelegateBridge __Hotfix_SkillSummon;
    private static DelegateBridge __Hotfix_SummonBy;
    private static DelegateBridge __Hotfix_SkillDamageSummon;
    private static DelegateBridge __Hotfix_SkillTeleport;
    private static DelegateBridge __Hotfix_SkillTeleportToNear;
    private static DelegateBridge __Hotfix_CombatBy;
    private static DelegateBridge __Hotfix_BuildSummonSourceData;
    private static DelegateBridge __Hotfix_ClearSummonSourceData;
    private static DelegateBridge __Hotfix_HasSummonSourceData;
    private static DelegateBridge __Hotfix_GetHeroAttackSkillInfo_1;
    private static DelegateBridge __Hotfix_GetSoldierAttackSkillInfo_1;
    private static DelegateBridge __Hotfix_GetHeroAttackSkillInfo_0;
    private static DelegateBridge __Hotfix_GetSoldierAttackSkillInfo_0;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      BattleTeam team,
      ConfigDataHeroInfo heroInfo,
      ConfigDataJobConnectionInfo jobConnectionInfo,
      ConfigDataSoldierInfo soldierInfo,
      ConfigDataSkillInfo[] skillInfos,
      BattleActorMasterJob[] masterJobs,
      BattleActorEquipment[] equipments,
      ConfigDataSkillInfo[] resonanceSkillInfos,
      ConfigDataSkillInfo[] fetterSkillInfos,
      int heroLevel,
      int heroStar,
      int jobLevel,
      int soldierCount,
      GridPosition pos,
      int dir,
      bool isNpc,
      int actionValue,
      int behaviorId,
      int groupId,
      BattleActorSourceType sourceType,
      int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeSkin(
      ConfigDataCharImageSkinResourceInfo heroCharImageSkinInfo,
      ConfigDataModelSkinResourceInfo heroModelSkinInfo,
      ConfigDataModelSkinResourceInfo soldierModelSkinInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeExtraPassiveSkillAndTalent(
      List<ConfigDataSkillInfo> skillInfos,
      ConfigDataSkillInfo talentSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeEnd(bool visible = true, int heroHp = -1, int soldierHp = -1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBattleProperties()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBattlePropertiesInCombat(
      BattleActor other,
      bool isAttacker,
      int distance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateBattlePropertiesInBattlefieldSkill(
      BattleActor other,
      bool isAttacker,
      int distance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeBattleProperties(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectJobMasterPropertyModifiers(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectJobEquipmentRefineryPropertyModifier(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectEquipmentPropertyModifiers(BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FaceTo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GuardMove(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnguardMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void MoveTo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainEffectInfo GetTerrainEffectInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetBuffEffectedTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasBuffEffectedHeroTag(int heroTagId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCommand CreateBattleCommand(BattleCommandType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattle(bool win)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionBegin()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsActionFinished()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PostActionTerrainDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnTerrainDamage(int damage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      GridPosition start,
      GridPosition goal,
      int movePoint,
      int inRegion,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      GridPosition start,
      GridPosition goal,
      int movePoint,
      FindPathIgnoreTeamType ignoreTeamType,
      int inRegion,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindMoveRegion(GridPosition start, List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ShouldLog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddExecutedCommandType(BattleCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasExecutedCommandType(BattleCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsExecutedCommandType(BattleCommandType cmdType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteMoveCommand(GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecutePerformMoveCommand(GridPosition targetPos, bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteCombatCommand(
      BattleActor target,
      ConfigDataSkillInfo skillInfo,
      bool isPerform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteSkillCommand(int skillIndex, GridPosition p, GridPosition p2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteSkillCommand(
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      GridPosition p2,
      bool isPerform)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool ExecuteDoneCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRetreat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDeadOrRetreat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool v)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsInvincible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSummoned()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAINpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerNpc()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAIActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsEventOrPerformActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonsterBoss()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonsterNoDie()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsMonsterAddDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMonsterActionLoopCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanBeTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroHealthPoint(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSoldierTotalHealthPoint(int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ModifyHeroAndSoldierHealthPoint(int heroHpModify, int soldierHpModify)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CheckDie(BattleActor attacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ProcessDie()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostProcessDie()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendPendingProcessDieActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Retreat(int effectType, string fxName, bool notifyListener = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroBaseAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierBaseAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxAttackDistance()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetTalentSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkillDistance(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSkillRange(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroBaseMovePoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierBaseMovePoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMovePoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType GetHeroMoveType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType GetSoldierMoveType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType GetMoveType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeMoveType(
      ConfigDataJobInfo heroJobInfo,
      ConfigDataSoldierInfo soldierInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeDefaultMoveType(ConfigDataHeroInfo heroInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeMoveTypeMin(MoveType m1, MoveType m2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static MoveType ComputeMoveTypeMax(MoveType m1, MoveType m2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetMoveTypePriority(MoveType m)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPointMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPointPercent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeCombatSoldierCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastDamageBySkill(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastDamageByBuff(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetLastDamageBySkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetFirstDamageTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTurnDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSetisfyCondition(int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSatisfyCondition(int conditionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDeathAnimType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBeCriticalAttack(bool a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionCriticalAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionKillActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActionDamageActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBeforeDieHealthPoint(int heroHp, int soldierHp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExtraAction()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsExtraMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetActionCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseBeCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetBeCombatAttackCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseUseSkillCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public short GetUseSkillCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseKillActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetKillActorCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetKillerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddApplyMonsterTotalDamage(int damage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetApplyMonsterTotalDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDieTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetDieBossPhase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSourceType GetSourceType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePlayer GetTrainingTechBattlePlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GridPosition InitPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleTeam Team
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty HeroBattleProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleProperty SoldierBattleProperty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierTotalHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierSingleHealthPointMax
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public uint FightTags
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleActorSummonData SummonData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroStar
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int JobLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int ActionValue
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataHeroInfo HeroInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo HeroArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroArmyId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobConnectionInfo JobConnectionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataJobInfo JobInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataSoldierInfo SoldierInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataArmyInfo SoldierArmyInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCharImageSkinResourceInfo HeroCharImageSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataModelSkinResourceInfo HeroModelSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataModelSkinResourceInfo SoldierModelSkinResourceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataCharImageInfo HeroVoiceCharImageInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int PlayerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindMoveAndAttackRegion(int distance, int shape)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool FindRandomEmptyPosition(int attackDistance, int shape, ref GridPosition position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindAttackPosition(
      int attackDistance,
      int shape,
      GridPosition targetPos,
      bool checkMoveRegion,
      bool farAway)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeActorScoreDamage(BattleActor a, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeActorScoreHeal(BattleActor a, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeActorScoreBuff(BattleActor a, int param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor SelectNearestTarget(BattleTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private RandomNumber GetAIRandomNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleTeam GetSkillTargetTeam(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanAttackOrUseSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    public BehaviorGroup Group
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int GroupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IncreaseBeAttackedCount()
    {
      // ISSUE: unable to decompile the method.
    }

    private bool IsAttackedByEnemy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    private bool IsAttackedByEnemyInLastTrun
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string InstanceID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBehaviorId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBehavior(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBehaviorState(BattleActor.BehaviorState state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CheckBehaviorCondition(
      BehaviorCondition condition,
      ConfigDataBehavior.ParamData param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBehaviorChangeRules()
    {
      // ISSUE: unable to decompile the method.
    }

    private int NextBehaviorByChangeRules
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition FindEmptyGridInCanAttackAndTouchRange(
      GridPosition startPoint,
      int attackDistance,
      int shape)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleActor> FindActorsInCanAttackAndTouchRange(
      List<BattleActor> destActors,
      GridPosition startPoint,
      int attackDistance,
      int shape,
      bool excludeSelf = true,
      bool isDirection = false,
      int range = 1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindFarthestPosition(
      List<GridPosition> positions,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindNearestPosition(
      List<GridPosition> positions,
      GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor FindNearestActor(List<BattleActor> actors, GridPosition startPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorTarget DoSelectTarget(
      SelectTarget st,
      ConfigDataBehavior.ParamData param)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsByIDFilter(
      List<BattleActor> actors,
      int[] priorIDs,
      int[] excludeIDs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool Contains(int[] target, int nr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectExtraMoveTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectMoveTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GenerateCommandOfMove(GridPosition target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GenerateCommandOfMove(GridPosition target, GridPosition target2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindPositionToMoveToTarget(GridPosition target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMovePoint4AI()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private SpecialMoveCostType GetSpecialMoveCostType4AI(
      out int specialMovePoint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindMoveRegion4AI(GridPosition start, List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindPositionToMoveToTarget(
      GridPosition target,
      out BattleActor blockingEnemy)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeRestrictScore(BattleActor srcActor, BattleActor destActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetArmyRistrictScore(ArmyTag a, ArmyTag b)
    {
      // ISSUE: unable to decompile the method.
    }

    private Fix64 TotalHPPercent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BattleActor SelectMinHPPercentActor(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<BattleActor> FindActorsAlive(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor DefaultSelectDamageSkillTarget(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSelectRangeSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    private ConfigDataSkillInfo BehaviorSelectSkillInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsInGrids(
      List<BattleActor> actors,
      List<GridPosition> grids)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindMaxAoeSkillCoverPosition(
      ConfigDataSkillInfo si,
      List<GridPosition> asCenterPositions,
      List<BattleActor> beCoveredActors,
      ClassValue<int> maxCoverActorsCount = null,
      ClassValue<int> maxCoverDirection = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor FindMaxAoeSkillCoverActor(
      ConfigDataSkillInfo si,
      List<BattleActor> asCenterActors,
      List<BattleActor> beCoveredActors,
      ClassValue<int> maxCoverActorsCount = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DefaultSelectAttackTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BehaviorTarget SelectAttackTargetWithSummonSkill(ConfigDataSkillInfo skill)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<GridPosition> FindGridsLessEqualDistance(
      GridPosition startPos,
      int dist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsInCrossLine(
      List<BattleActor> actors,
      GridPosition centerPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsLessEqualDistance(
      List<BattleActor> actors,
      GridPosition startPos,
      int dist)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor DefaultSelectHealSkillTarget(List<BattleActor> actors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor DefaultSelectBuffSkillTarget(
      List<BattleActor> actors,
      ConfigDataSkillInfo skill)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectAttackTargetInSkillRange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsWithBuffN(
      List<BattleActor> actors,
      int buffID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsWithSkillAITypeN(
      List<BattleActor> actors,
      int paramN)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectAttackTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SelectSkillDirectReachTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool HasInt(int value, int[] arr)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DefaultSelectSkill(int[] includeSkillIDs, int[] excludeSkillIDs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> FindActorsInCanNormalAttackAndTouchRange(
      List<BattleActor> destActors,
      List<BattleActor> srcActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition Direction2Position(
      GridPosition startPosition,
      AttackDirection dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSkillAGoodAISelection(ConfigDataSkillInfo si)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition FindCastSkillPosition(ConfigDataSkillInfo si, BehaviorTarget t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindAttackPositions(
      int attackDistance,
      int shape,
      GridPosition targetPos,
      List<GridPosition> positions)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GenerateCommandOfAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBehaviorMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsAttackTargetStillValid()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoBehaviorAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GenerateAIBattleCommand()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleCommand AICreateBattleCommand(BattleCommandType type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleActor SelectAttackRegionTarget(
      BattleTeam team,
      BattleActor.ComputeActorScoreFunc computeScore,
      int computeScoreParam = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BuffState> GetBuffStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetBuffIds(List<int> buffIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitializeBuffs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AttachPassiveSkillBuffs(ConfigDataSkillInfo skillInfo, BuffSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AttachBuff(
      ConfigDataBuffInfo buffInfo,
      BattleActor applyer,
      BuffSourceType sourceType,
      ConfigDataSkillInfo sourceSkillInfo,
      BuffState sourceBuffState,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttachBuffList(
      List<int> buffIds,
      BattleActor applyer,
      BuffSourceType sourceType,
      ConfigDataSkillInfo sourceSkillInfo,
      BuffState sourceBuffState,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareBuffOrder(BuffState b0, BuffState b1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool RemoveBuff(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RemoveBuffList(List<int> buffIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int RemoveBuffList(List<int> buffIds, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BuffState FindBuff(int buffId, BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveAllBuffs()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SkillDispelBuff(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasBuff(int buffId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyModifiersAndFightTags(
      BattlePropertyModifier pm,
      ref uint fightTags)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectBuffPropertyModifiersAndFightTags(
      BattlePropertyModifier pm,
      ref uint fightTags,
      bool collectStatic,
      bool collectDynamic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectPropertyModifier(
      BattlePropertyModifier pm,
      PropertyModifyType modifyType,
      int value,
      bool collectStatic,
      bool collectDynamic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectOtherActorBuffPropertyModifiersAndFightTags(
      BattlePropertyModifier pm,
      ref uint fightTags)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffPropertyModifiersAndFightTagsInCombat(
      BattlePropertyModifier pm,
      ref uint fightTags,
      BattleActor target,
      bool isAttacker,
      int distance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectDistancePropertyModifiers(
      BattlePropertyModifier pm,
      BattleActor target,
      bool isAttacker,
      int distance,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectBuffPropertyExchange(
      BattleProperty battleProperty,
      BattlePropertyModifier pm)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CollectBuffSummonData(
      BattleActorSummonData summonData,
      BattleActorSummonData summonSourceData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CollectBuffPropertyReplace()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public MoveType BuffMoveTypeChange()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public SpecialMoveCostType BuffSpecialMoveCostType(out int specialMovePoint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateAllAuras()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemoveAuraAppliedBuffs(BuffState auraBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RemovePackChildBuffs(BuffState packBuff)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveZeroTimeBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuffHit(
      BattleActor attacker,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasFightTag(FightTag ft)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBuffTypes()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetBuffType(BuffType bt, bool on)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasBuffType(BuffType bt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffEffective(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffHpConditionSatisfied(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffHpConditionSatisfied(int operatorType, int value, int targetType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffEffectiveConditionSatisfied(ConfigDataBuffInfo buffInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffCombatConditionSatisfied(
      BattleActor target,
      bool isAttacker,
      int distance,
      int param1,
      int param2,
      int param3,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffCombatConditionSatisfied(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      List<int> paramList,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeNeighborAliveActorCount(int teamType, int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffCooldown(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBuffCooldown(BuffState bs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeBuffCount(int buffId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeEnhanceOrDebuffCount(int enhanceDebuffType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsImmuneBuffSubType(int subType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int BuffReboundDamage(ConfigDataSkillInfo skillInfo, int damage, out int reboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor.HealChangeDamageResult> BuffHealChangeDamage(
      int heroHeal,
      int soldierHeal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor.HealChangeDamageResult> BuffBeHealChangeDamage(
      int heroHeal,
      int soldierHeal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnBuffHealOrChangeDamageHit(BuffState bs, int heroHeal, int soldierHeal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuffOverrideMovePointCost()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetBuffOverrideTerrainInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationAttack(
      int targetArmyId,
      bool targetIsHero,
      ref ArmyRelationData armyRelation,
      bool isUi = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationDefend(
      int targetArmyId,
      bool targetIsHero,
      ref ArmyRelationData armyRelation,
      bool isUi = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationModifyAttack(
      int restrainValue,
      bool targetIsHero,
      ref ArmyRelationData armyRelation,
      bool isUi = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddBuffArmyRelationModifyDefend(
      int restrainValue,
      bool targetIsHero,
      ref ArmyRelationData armyRelation,
      bool isUi = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffIgnoreArmyRelation(int armyId, bool isHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffForceMagicDamage(bool attackerIsHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBuffForcePhysicalDamage(bool attackerIsHero)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuffTimeChange(BattleActor applyer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBuffTimeChange2(BattleActor target, ConfigDataSkillInfo sourceSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsBuffModifyHeroTag(int heroTagId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetBuffHeroAuraModify(BuffSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetBuffReplaceRuleModify(BuffSourceType sourceType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionBeginBuffEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffEffect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateBuffTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionEndBuffDamageTeleport(
      bool isAttacker,
      bool isKill,
      bool isCritical,
      GridPosition actionBeginPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ActionEndBuffDoubleMove(bool isKill, bool isCritical, int remainMovePoint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffHealOverTime(bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffDamageOverTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndAddBuff()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndAddBuffSuper(
      bool isKill,
      bool isCritical,
      bool isDamage,
      int battlefieldSkillTargetCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndAddMovePoint(int remainMovePoint)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndRemoveDebuff(bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndRemoveEnhanceBuff(bool isKill, bool isCritical, bool isDamage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffBattlefieldSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ActionEndBuffNewTurn(
      bool isKill,
      bool isCritical,
      int battlefieldSkillTargetCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareHealActor(BattleActor a0, BattleActor a1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffHeal(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      int heroDamage,
      int soldierDamage,
      int reboundPercent,
      int enterCombatHpPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffHealOther(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      int heroDamage,
      int soldierDamage,
      int reboundPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffDamage(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffApplyBuff(
      BattleActor target,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical,
      bool isKill,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static bool IsValueMultiply(int valueA, int valueB, int compareValue)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffRemoveDebuff(
      BattleActor target,
      List<AfterCombatApplyBuff> afterCombatApplyBuffList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CombatBuffBanGuard(BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffAttackAid(
      BattleActor actor,
      BattleActor target,
      ref int attackAidPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffDefenseAid(
      BattleActor actor,
      BattleActor target,
      ref int defenseAidPercent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeBuffSkillHpModify(
      ConfigDataSkillInfo skillInfo,
      int skillPower,
      int percent,
      BattleActor target,
      out int heroHpModify,
      out int soldierHpModify)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetBuffCombatAidModify(
      bool isAttack,
      out int distanceModify,
      out int damagePercentModify)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackApplyBuff(List<BattleActor> targets, bool isAttacker, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void KillApplyBuff(bool isCritical, bool isUseSkill, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffDamage(
      List<BattleActor> targets,
      bool beforeCombat,
      bool isAttacker,
      int combatDistance,
      ConfigDataSkillInfo attackerSkillInfo,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackRemoveBuff(
      List<BattleActor> targets,
      bool beforeCombat,
      bool isAttacker,
      int combatDistance,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackRemoveSkillCooldown(
      ConfigDataSkillInfo skillInfo,
      bool isAttacker,
      bool isCritical,
      bool isKill,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BeforeBattlefieldSkillApplyBuff(int battlefieldSkillTargetCount, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleFieldSkillApplyBuff(List<BattleActor> targets, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareActorByPositionX_Ascending(BattleActor a1, BattleActor a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareActorByPositionX_Descending(BattleActor a1, BattleActor a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareActorByPositionY_Ascending(BattleActor a1, BattleActor a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareActorByPositionY_Descending(BattleActor a1, BattleActor a2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffPunch(List<BattleActor> targets, AttackDirection dir, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffDrag(List<BattleActor> targets, AttackDirection dir, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AttackBuffExchangePosition(
      BattleActor target,
      bool isTargetGurad,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBuffArmyChange(
      BattleActor target,
      ConfigDataArmyInfo targetHeroArmyInfo,
      ConfigDataArmyInfo targetSoldierArmyInfo,
      bool beforeCombat,
      bool isAttacker,
      int distance,
      bool isCritical)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DamageBuffHealOther()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState BuffDoubleAttack()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState BuffDoubleSkill()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool BuffUndead(
      bool isAttacker,
      bool isGuarder,
      int beforeDieHeroHp,
      int beforeDieSoldierHp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DieBuffBattlefieldSkill(bool isAttacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataSkillInfo DieBuffSummonSkill(bool isAttacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UndeadBuffBattlefieldSkill(bool isAttacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanBuffGuard(BattleActor target, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetGuardActor(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleSkillState GetSkillState(int index)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleSkillState GetSkillStateById(int skillID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<BattleSkillState> GetSkillStates()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsSkillUseable(int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCombineSkillUseable(int skillIndex, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private List<BattleActor> GetCombineSkillActors(
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkillBanned(int skillIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanUseSkillOnTarget(ConfigDataSkillInfo skillInfo, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CanBeTeleport(int teamNumber)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattlefiledSkillApplyTarget(ConfigDataSkillInfo skillInfo, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FindBattlefieldSkillAttackRegion(
      ConfigDataSkillInfo skillInfo,
      GridPosition startPos,
      GridPosition targetPos,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FindBattlefieldSkillApplyTargets(
      ConfigDataSkillInfo skillInfo,
      GridPosition targetPos,
      List<BattleActor> targetActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool ExecuteBattlefieldSkill(
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      GridPosition p2,
      bool isBattleCommand,
      bool checkSelfDie = true,
      bool cancelIfNoTarget = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AttachSkillSelfBuffs(ConfigDataSkillInfo skillInfo, bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillAttack(ConfigDataSkillInfo skillInfo, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkillAttackBy(BattleActor attacker, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnSkillHit(
      BattleActor attacker,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkillAttackEnd(
      ConfigDataSkillInfo skillInfo,
      GridPosition skillPos,
      List<BattleActor> targets,
      bool checkSelfDie = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillSummon(ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SummonBy(
      BattleActor summoner,
      BattleActorSummonData summonSourceData,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillDamageSummon(ConfigDataSkillInfo skillInfo, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool SkillTeleport(
      ConfigDataSkillInfo skillInfo,
      BattleActor target,
      GridPosition teleportPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SkillTeleportToNear(ConfigDataSkillInfo skillInfo, List<BattleActor> targets)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CombatBy(BattleActor attacker)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void BuildSummonSourceData(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearSummonSourceData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasSummonSourceData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetHeroAttackSkillInfo(
      MoveType targetMoveType,
      int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetSoldierAttackSkillInfo(
      MoveType targetMoveType,
      int distance)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataSkillInfo GetHeroAttackSkillInfo(bool isMelee)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ConfigDataSkillInfo GetSoldierAttackSkillInfo(bool isMelee)
    {
      // ISSUE: unable to decompile the method.
    }

    public delegate int ComputeActorScoreFunc(BattleActor a, int param);

    public enum BehaviorState
    {
      Move,
      Attack,
    }

    public struct HealChangeDamageResult
    {
      public BuffState ChangeBuffState;
      public int HeroHpModify;
      public int SoldierHpModify;
    }
  }
}
