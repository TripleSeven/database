﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.ServerBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class ServerBattleListener : NullBattleListener
  {
    private BattleTimeEstimator m_battleTimeEstimator;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitTimeEstimator;
    private static DelegateBridge __Hotfix_GetEstimateTime;
    private static DelegateBridge __Hotfix_ClearEstimateTime;
    private static DelegateBridge __Hotfix_OnBattleStart;
    private static DelegateBridge __Hotfix_OnBattleNextTurn;
    private static DelegateBridge __Hotfix_OnBattleNextTeam;
    private static DelegateBridge __Hotfix_OnBattleNextPlayer;
    private static DelegateBridge __Hotfix_OnBattleActorActive;
    private static DelegateBridge __Hotfix_OnBattleActorMove;
    private static DelegateBridge __Hotfix_OnBattleActorPerformMove;
    private static DelegateBridge __Hotfix_OnBattleActorPunchMove;
    private static DelegateBridge __Hotfix_OnBattleActorExchangeMove;
    private static DelegateBridge __Hotfix_OnBattleActorPlayAnimation;
    private static DelegateBridge __Hotfix_OnBattleActorSkill;
    private static DelegateBridge __Hotfix_OnBattleActorImmune;
    private static DelegateBridge __Hotfix_OnBattleActorPassiveSkill;
    private static DelegateBridge __Hotfix_OnBattleActorBuffHit;
    private static DelegateBridge __Hotfix_OnBattleActorTerrainHit;
    private static DelegateBridge __Hotfix_OnBattleActorTeleport;
    private static DelegateBridge __Hotfix_OnBattleActorSummon;
    private static DelegateBridge __Hotfix_OnBattleActorDie;
    private static DelegateBridge __Hotfix_OnBattleActorAppear;
    private static DelegateBridge __Hotfix_OnBattleActorDisappear;
    private static DelegateBridge __Hotfix_OnBattleActorChangeTeam;
    private static DelegateBridge __Hotfix_OnBattleActorReplace;
    private static DelegateBridge __Hotfix_OnBattleActorCameraFocus;
    private static DelegateBridge __Hotfix_OnBattleActorGainBattleTreasure;
    private static DelegateBridge __Hotfix_OnStartGuard;
    private static DelegateBridge __Hotfix_OnStopGuard;
    private static DelegateBridge __Hotfix_OnStartCombat;
    private static DelegateBridge __Hotfix_OnStopCombat;
    private static DelegateBridge __Hotfix_OnCameraFocus;
    private static DelegateBridge __Hotfix_OnWaitTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public ServerBattleListener()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitTimeEstimator(BattleBase battle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetEstimateTime(bool isFastBattle = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEstimateTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorPlayAnimation(
      BattleActor a,
      string animationName,
      int animationTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorImmune(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorTeleport(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorChangeTeam(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorCameraFocus(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStartGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStopGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnCameraFocus(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnWaitTime(int timeInMs)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
