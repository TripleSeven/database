﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.DelayHit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;

namespace BlackJack.ProjectL.Battle
{
  public struct DelayHit
  {
    public int m_frame;
    public Vector2i m_position;
    public CombatActor m_targetActor;
  }
}
