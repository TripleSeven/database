﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.CombatFlyObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class CombatFlyObject : Entity
  {
    private CombatTeam m_team;
    private Vector2i m_startPosition;
    private Vector2i m_endPosition;
    private int m_startZ;
    private int m_endZ;
    private Fix64 m_vz;
    private int m_life;
    private int m_lifeMax;
    private int m_frameCount;
    private ushort m_hitId;
    private List<DelayHit> m_delayHits;
    private CombatActor m_targetActor;
    private int m_targetTeamNumber;
    private CombatActor m_sourceActor;
    private CombatSkillState m_sourceSkillState;
    private ConfigDataFlyObjectInfo m_flyObjectInfo;
    private IBattleGraphic m_fx;
    private Fix64 m_fxLife;
    private bool m_isGraphicSkillFade;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Setup;
    private static DelegateBridge __Hotfix_IsHero;
    private static DelegateBridge __Hotfix_SetGraphicSkillFade;
    private static DelegateBridge __Hotfix_ComputeTargetScore;
    private static DelegateBridge __Hotfix_SearchHitTarget;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_AddSkillDelayHits;
    private static DelegateBridge __Hotfix_Attack;
    private static DelegateBridge __Hotfix_AddDelayHit;
    private static DelegateBridge __Hotfix_GetDelayHitFrameMax;
    private static DelegateBridge __Hotfix_HasDelayHit;
    private static DelegateBridge __Hotfix_TickDelayHits;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_DoPause;
    private static DelegateBridge __Hotfix_ComputeParabolicPosition;
    private static DelegateBridge __Hotfix_CreateFx;
    private static DelegateBridge __Hotfix_LogCanNotStopCombat;
    private static DelegateBridge __Hotfix_get_Combat;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatFlyObject()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(CombatTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(
      ConfigDataFlyObjectInfo flyObjInfo,
      CombatSkillState ss,
      CombatActor sourceActor,
      CombatActor targetActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeTargetScore(Fix64 x, CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CombatActor SearchHitTarget(Fix64 x0, Fix64 x1, ushort hitId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSkillDelayHits(CombatSkillState ss, Fix64 x0, Fix64 x1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool Attack(CombatActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddDelayHit(CombatActor target, int delay)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetDelayHitFrameMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasDelayHit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickDelayHits()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ComputeParabolicPosition(Fix64 life, out Vector2i pos, out Fix64 z)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateFx(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogCanNotStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    public Combat Combat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
