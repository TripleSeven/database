﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleActorSetup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleActorSetup
  {
    public ConfigDataHeroInfo HeroInfo;
    public ConfigDataJobConnectionInfo JobConnectionInfo;
    public ConfigDataSoldierInfo SoldierInfo;
    public ConfigDataSkillInfo[] SkillInfos;
    public BattleActorMasterJob[] MasterJobs;
    public BattleActorEquipment[] Equipments;
    public ConfigDataSkillInfo[] ResonanceSkillInfos;
    public ConfigDataSkillInfo[] FetterSkillInfos;
    public int HeroLevel;
    public int HeroStar;
    public int JobLevel;
    public int SoldierCount;
    public GridPosition Position;
    public int Direction;
    public bool IsNpc;
    public int BehaviorId;
    public int GroupId;
    public int ActionValue;
    public ConfigDataCharImageSkinResourceInfo HeroCharImageSkinResourceInfo;
    public ConfigDataModelSkinResourceInfo HeroModelSkinResourceInfo;
    public ConfigDataModelSkinResourceInfo SoldierModelSkinResourceInfo;
    public List<ConfigDataSkillInfo> ExtraPassiveSkillInfos;
    public ConfigDataSkillInfo ExtraTalentSkillInfo;
    public int HeroHealthPoint;
    public int SoldierHealthPoint;
    public int ActorId;
    public int PlayerIndex;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSetup()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
