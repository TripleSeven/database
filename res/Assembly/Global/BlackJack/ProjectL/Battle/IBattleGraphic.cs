﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.IBattleGraphic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  public interface IBattleGraphic
  {
    void PlayAnimation(string name, bool loop);

    void PlayDeathAnimation(int deathType);

    void PlayFx(string name, int tag);

    void SetCombatPosition(Vector2i pos, Fix64 z, int zoffset);

    void SetCombatDirection(Vector2i front, Fix64 frontZ);

    void SetCombatDirection(int dir);

    void SetName(string name);

    void SetReplaceAnimations(List<ReplaceAnim> replaceAnims);

    void SetHeight(float height, float footHeight);

    void SetVisible(bool visible);

    void SetEffect(GraphicEffect e, float param1, float param2);

    void ClearEffect(GraphicEffect e);

    void ClearAttachFxs(int tagMask);

    void Pause(bool pause);

    void Tick(float dt);

    void FxStop(bool fadeOut);
  }
}
