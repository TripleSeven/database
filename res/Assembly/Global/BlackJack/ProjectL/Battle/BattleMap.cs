﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleMap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleMap
  {
    public const int MoveRegion = 1;
    public const int AttackRegion = 2;
    public const int SkillRegion = 3;
    public const int TeleportRegion = 4;
    private int m_width;
    private int m_height;
    private BattleMapCell[,] m_cells;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_InitializeTerrain;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_ClearActors;
    private static DelegateBridge __Hotfix_ClearRegion;
    private static DelegateBridge __Hotfix_IsValidPosition;
    private static DelegateBridge __Hotfix_SetActor;
    private static DelegateBridge __Hotfix_GetActor;
    private static DelegateBridge __Hotfix_SetTerrainInfo;
    private static DelegateBridge __Hotfix_GetTerrainInfo;
    private static DelegateBridge __Hotfix_SetTerrainEffectInfo;
    private static DelegateBridge __Hotfix_GetTerrainEffectInfo;
    private static DelegateBridge __Hotfix_SetRegion;
    private static DelegateBridge __Hotfix_GetRegion;
    private static DelegateBridge __Hotfix_SetRegionBit;
    private static DelegateBridge __Hotfix_ClearRegionBit;
    private static DelegateBridge __Hotfix_HasRegionBit;
    private static DelegateBridge __Hotfix_GetMovePointCost_1;
    private static DelegateBridge __Hotfix_GetMovePointCost_0;
    private static DelegateBridge __Hotfix_IsSpecialMoveCost;
    private static DelegateBridge __Hotfix_GetCell;
    private static DelegateBridge __Hotfix_get_Width;
    private static DelegateBridge __Hotfix_get_Height;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(int w, int h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool InitializeTerrain(
      ConfigDataBattlefieldInfo battlefieldInfo,
      IConfigDataLoader configDataLoader)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsValidPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActor(GridPosition p, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActor GetActor(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTerrainInfo(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainInfo GetTerrainInfo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTerrainEffectInfo(GridPosition p, ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataTerrainEffectInfo GetTerrainEffectInfo(
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegion(GridPosition p, int region)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRegion(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRegionBit(GridPosition p, int bit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearRegionBit(GridPosition p, int bit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasRegionBit(GridPosition p, int bit)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMovePointCost(GridPosition p, MoveType moveType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int GetMovePointCost(ConfigDataTerrainInfo terrain, MoveType moveType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSpecialMoveCost(SpecialMoveCostType costType, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private BattleMapCell GetCell(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    public int Width
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Height
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
