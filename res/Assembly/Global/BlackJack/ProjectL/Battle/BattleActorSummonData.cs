﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleActorSummonData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleActorSummonData
  {
    public BattleActor Summoner;
    public ConfigDataSkillInfo SummonSkillInfo;
    public int Attack;
    public int Defense;
    public int Magic;
    public int MagicDefense;
    public int Dexterity;
    public int HealthPointMax;
    public int HealthPoint;
    public int AttackDistance;
    public ConfigDataSkillInfo MeleeSkillInfo;
    public ConfigDataSkillInfo RangeSkillInfo;
    public ConfigDataArmyInfo ArmyInfo;
    public MoveType MoveType;
    public int MovePoint;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorSummonData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
