﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.Pathfinder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class Pathfinder
  {
    private LinkedList<Pathfinder.Node> m_OpenList;
    private List<Pathfinder.Node> m_ClosedList;
    private List<Pathfinder.Node> m_Successors;
    private Pathfinder.SearchState m_State;
    private int m_Steps;
    private Pathfinder.Node m_Start;
    private Pathfinder.Node m_Goal;
    private Pathfinder.Node m_CurrentSolutionNode;
    private bool m_CancelRequest;
    private List<Pathfinder.Node> m_NodePool;
    private int m_AllocateNodeCount;
    private List<PathNode> m_PathNodePool;
    private int m_AllocatedPathNodeCount;
    private const int kPreallocatedNodes = 64;
    private BattleMap m_Map;
    private int m_MovePoint;
    private MoveType m_MoveType;
    private FindPathIgnoreTeamType m_IgnoreTeamType;
    private int m_InRegion;
    private int m_OverrideMovePointCost;
    private SpecialMoveCostType m_SpecialMoveCostType;
    private int m_SpecialMovePoint;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_SortedAddToOpenList;
    private static DelegateBridge __Hotfix_AllocateNode;
    private static DelegateBridge __Hotfix_AllocatePathNode;
    private static DelegateBridge __Hotfix_InitiatePathfind;
    private static DelegateBridge __Hotfix_CancelSearch;
    private static DelegateBridge __Hotfix_SetStartAndGoalStates;
    private static DelegateBridge __Hotfix_SearchStep;
    private static DelegateBridge __Hotfix_SetRegionStartState;
    private static DelegateBridge __Hotfix_SearchRegionStep;
    private static DelegateBridge __Hotfix_AddSuccessor;
    private static DelegateBridge __Hotfix_GetSolutionStart;
    private static DelegateBridge __Hotfix_GetSolutionNext;
    private static DelegateBridge __Hotfix_FreeSolutionNodes;
    private static DelegateBridge __Hotfix_get_Map;
    private static DelegateBridge __Hotfix_get_MoveType;
    private static DelegateBridge __Hotfix_get_IgnoreTeamType;
    private static DelegateBridge __Hotfix_get_OverrideMovePointCost;
    private static DelegateBridge __Hotfix_get_InRegion;
    private static DelegateBridge __Hotfix_get_StartNode;
    private static DelegateBridge __Hotfix_get_GoalNode;
    private static DelegateBridge __Hotfix_HasStartNode;
    private static DelegateBridge __Hotfix_HasGoalNode;
    private static DelegateBridge __Hotfix_FindPath;
    private static DelegateBridge __Hotfix_FindRegion;

    [MethodImpl((MethodImplOptions) 32768)]
    public Pathfinder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortedAddToOpenList(Pathfinder.Node node)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Pathfinder.Node AllocateNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode AllocatePathNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitiatePathfind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelSearch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStartAndGoalStates(PathNode start, PathNode goal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Pathfinder.SearchState SearchStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetRegionStartState(PathNode start)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Pathfinder.SearchState SearchRegionStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSuccessor(PathNode state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode GetSolutionStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PathNode GetSolutionNext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeSolutionNodes()
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleMap Map
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public MoveType MoveType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FindPathIgnoreTeamType IgnoreTeamType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int OverrideMovePointCost
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int InRegion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PathNode StartNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public PathNode GoalNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasStartNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGoalNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      BattleMap map,
      GridPosition start,
      GridPosition goal,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindRegion(
      BattleMap map,
      GridPosition start,
      int movePoint,
      MoveType moveType,
      FindPathIgnoreTeamType ignoreTeamType,
      int overrideMovePointCost,
      int inRegion,
      SpecialMoveCostType specialMoveCostType,
      int specialMovePoint,
      List<GridPosition> region)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum SearchState
    {
      NotInitialized,
      Searching,
      Succeeded,
      Failed,
    }

    public class Node
    {
      public Pathfinder.Node parent;
      public Pathfinder.Node child;
      public int g;
      public int g2;
      public int h;
      public int f;
      public PathNode m_UserState;
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_Reinitialize;

      public Node()
      {
        this.Reinitialize();
        Pathfinder.Node._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }

      public void Reinitialize()
      {
        DelegateBridge hotfixReinitialize = Pathfinder.Node.__Hotfix_Reinitialize;
        if (hotfixReinitialize != null)
        {
          hotfixReinitialize.__Gen_Delegate_Imp4((object) this);
        }
        else
        {
          this.parent = (Pathfinder.Node) null;
          this.child = (Pathfinder.Node) null;
          this.g = 0;
          this.g2 = 0;
          this.h = 0;
          this.f = 0;
        }
      }
    }
  }
}
