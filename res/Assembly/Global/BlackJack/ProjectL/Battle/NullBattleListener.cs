﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.NullBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class NullBattleListener : IBattleListener
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnBattleStart;
    private static DelegateBridge __Hotfix_OnBattleNextTurn;
    private static DelegateBridge __Hotfix_OnBattleNextTeam;
    private static DelegateBridge __Hotfix_OnBattleNextPlayer;
    private static DelegateBridge __Hotfix_OnBattleNextActor;
    private static DelegateBridge __Hotfix_OnBattleActorCreate;
    private static DelegateBridge __Hotfix_OnBattleActorCreateEnd;
    private static DelegateBridge __Hotfix_OnBattleActorActive;
    private static DelegateBridge __Hotfix_OnBattleActorActionBegin;
    private static DelegateBridge __Hotfix_OnBattleActorActionEnd;
    private static DelegateBridge __Hotfix_OnBattleActorMove;
    private static DelegateBridge __Hotfix_OnBattleActorMoveEnd;
    private static DelegateBridge __Hotfix_OnBattleActorPerformMove;
    private static DelegateBridge __Hotfix_OnBattleActorPunchMove;
    private static DelegateBridge __Hotfix_OnBattleActorExchangeMove;
    private static DelegateBridge __Hotfix_OnBattleActorSetDir;
    private static DelegateBridge __Hotfix_OnBattleActorPlayFx;
    private static DelegateBridge __Hotfix_OnBattleActorPlayAnimation;
    private static DelegateBridge __Hotfix_OnBattleActorChangeIdleAnimation;
    private static DelegateBridge __Hotfix_OnBattleActorSkill;
    private static DelegateBridge __Hotfix_OnBattleActorSkillHitBegin;
    private static DelegateBridge __Hotfix_OnBattleActorSkillHit;
    private static DelegateBridge __Hotfix_OnBattleActorSkillHitEnd;
    private static DelegateBridge __Hotfix_OnBattleActorAttachBuff;
    private static DelegateBridge __Hotfix_OnBattleActorDetachBuff;
    private static DelegateBridge __Hotfix_OnBattleActorImmune;
    private static DelegateBridge __Hotfix_OnBattleActorModifyHp;
    private static DelegateBridge __Hotfix_OnBattleActorPassiveSkill;
    private static DelegateBridge __Hotfix_OnBattleActorBuffHit;
    private static DelegateBridge __Hotfix_OnBattleActorTerrainHit;
    private static DelegateBridge __Hotfix_OnBattleActorTeleport;
    private static DelegateBridge __Hotfix_OnBattleActorSummon;
    private static DelegateBridge __Hotfix_OnBattleActorDie;
    private static DelegateBridge __Hotfix_OnBattleActorAppear;
    private static DelegateBridge __Hotfix_OnBattleActorDisappear;
    private static DelegateBridge __Hotfix_OnBattleActorChangeTeam;
    private static DelegateBridge __Hotfix_OnBattleActorChangeArmy;
    private static DelegateBridge __Hotfix_OnBattleActorReplace;
    private static DelegateBridge __Hotfix_OnBattleActorCameraFocus;
    private static DelegateBridge __Hotfix_OnBattleActorGainBattleTreasure;
    private static DelegateBridge __Hotfix_OnStartGuard;
    private static DelegateBridge __Hotfix_OnStopGuard;
    private static DelegateBridge __Hotfix_OnBeforeStartCombat;
    private static DelegateBridge __Hotfix_OnCancelCombat;
    private static DelegateBridge __Hotfix_OnStartCombat;
    private static DelegateBridge __Hotfix_OnPreStopCombat;
    private static DelegateBridge __Hotfix_OnStopCombat;
    private static DelegateBridge __Hotfix_OnCombatActorSkill;
    private static DelegateBridge __Hotfix_OnCombatActorHit;
    private static DelegateBridge __Hotfix_OnCombatActorDie;
    private static DelegateBridge __Hotfix_OnStartSkillCutscene;
    private static DelegateBridge __Hotfix_OnStartPassiveSkillCutscene;
    private static DelegateBridge __Hotfix_OnStopSkillCutscene;
    private static DelegateBridge __Hotfix_OnStartBattleDialog;
    private static DelegateBridge __Hotfix_OnStartBattlePerform;
    private static DelegateBridge __Hotfix_OnStopBattlePerform;
    private static DelegateBridge __Hotfix_OnChangeMapTerrain;
    private static DelegateBridge __Hotfix_OnChangeMapTerrainEffect;
    private static DelegateBridge __Hotfix_OnCameraFocus;
    private static DelegateBridge __Hotfix_OnPlayMusic;
    private static DelegateBridge __Hotfix_OnPlaySound;
    private static DelegateBridge __Hotfix_OnPlayFx;
    private static DelegateBridge __Hotfix_OnWaitTime;
    private static DelegateBridge __Hotfix_OnBossPhase;
    private static DelegateBridge __Hotfix_OnBattleTreasureCreate;
    private static DelegateBridge __Hotfix_CreateCombatGraphic;
    private static DelegateBridge __Hotfix_DestroyCombatGraphic;
    private static DelegateBridge __Hotfix_PlayCombatFx;
    private static DelegateBridge __Hotfix_PlaySound;
    private static DelegateBridge __Hotfix_DrawLine_0;
    private static DelegateBridge __Hotfix_DrawLine_1;
    private static DelegateBridge __Hotfix_LogBattleStart;
    private static DelegateBridge __Hotfix_LogBattleStop;
    private static DelegateBridge __Hotfix_LogBattleTeam;
    private static DelegateBridge __Hotfix_LogActorMove;
    private static DelegateBridge __Hotfix_LogActorStandby;
    private static DelegateBridge __Hotfix_LogActorAttack;
    private static DelegateBridge __Hotfix_LogActorSkill;
    private static DelegateBridge __Hotfix_LogActorDie;

    [MethodImpl((MethodImplOptions) 32768)]
    public NullBattleListener()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleNextActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorCreate(BattleActor a, bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorCreateEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorActionBegin(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorActionEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorMoveEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorSetDir(BattleActor a, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorPlayAnimation(
      BattleActor a,
      string animationName,
      int animationTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorChangeIdleAnimation(BattleActor a, string idleAnimationName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorSkillHitBegin(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorSkillHitEnd(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorImmune(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorModifyHp(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorTeleport(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorChangeTeam(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorChangeArmy(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorCameraFocus(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStartGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStopGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBeforeStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCancelCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnPreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCombatActorSkill(CombatActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCombatActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      ConfigDataCutsceneInfo cutsceneInfo2,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStopSkillCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnChangeMapTerrain(
      List<GridPosition> positions,
      ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnCameraFocus(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnPlayMusic(string musicName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnPlaySound(string soundName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnPlayFx(string fxName, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnWaitTime(int timeInMs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBossPhase(int phase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnBattleTreasureCreate(
      ConfigDataBattleTreasureInfo treasureInfo,
      bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual IBattleGraphic CreateCombatGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DestroyCombatGraphic(IBattleGraphic model)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual IBattleGraphic PlayCombatFx(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DrawLine(Vector2i p0, Vector2i p1, Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogBattleStop(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogBattleTeam(BattleTeam team0, BattleTeam team1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogActorMove(BattleActor actor, GridPosition fromPos, GridPosition toPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogActorStandby(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogActorAttack(BattleActor actor, BattleActor targetActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void LogActorDie(BattleActor actor, BattleActor killerActor)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
