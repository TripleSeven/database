﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BehaviorGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BehaviorGroup
  {
    private int m_ID;
    private BattleTeam m_team;
    private List<BattleActor> m_actors;
    private ConfigDataGroupBehavior m_curBehaviorCfg;
    private BattleActor m_leader;
    private int m_leaderNormalActionPriority;
    private List<int> m_singleBehaviors;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ID;
    private static DelegateBridge __Hotfix_AddActor;
    private static DelegateBridge __Hotfix_get_Leader;
    private static DelegateBridge __Hotfix_SetBahvior;
    private static DelegateBridge __Hotfix_get_Behavior;
    private static DelegateBridge __Hotfix_get_Actors;
    private static DelegateBridge __Hotfix_get_InstanceID;
    private static DelegateBridge __Hotfix_CheckGroupMemberDead;
    private static DelegateBridge __Hotfix_DoBehavor;
    private static DelegateBridge __Hotfix_DoOtherBehavorCheck;

    [MethodImpl((MethodImplOptions) 32768)]
    public BehaviorGroup(int id, BattleTeam team)
    {
      // ISSUE: unable to decompile the method.
    }

    public int ID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleActor Leader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBahvior(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    public ConfigDataGroupBehavior Behavior
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public List<BattleActor> Actors
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string InstanceID
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool CheckGroupMemberDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DoBehavor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DoOtherBehavorCheck(BehaviorCondition condition)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
