﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattleAchievementState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattleAchievementState
  {
    public int m_conditionId;
    public BattleConditionStatus m_status;
    public ConfigDataBattleAchievementRelatedInfo m_achievementRelatedInfo;
    public int m_turnCount;
    public List<int> m_intList;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleAchievementState()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
