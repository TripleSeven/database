﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.IBattleListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using System.Collections.Generic;

namespace BlackJack.ProjectL.Battle
{
  public interface IBattleListener
  {
    void OnBattleStart();

    void OnBattleNextTurn(int turn);

    void OnBattleNextTeam(int team, bool isNpc);

    void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex);

    void OnBattleNextActor(BattleActor actor);

    void OnBattleActorCreate(BattleActor a, bool visible);

    void OnBattleActorCreateEnd(BattleActor a);

    void OnBattleActorActive(BattleActor a, bool newStep);

    void OnBattleActorActionBegin(BattleActor a);

    void OnBattleActorActionEnd(BattleActor a);

    void OnBattleActorMove(BattleActor a, GridPosition p, int dir);

    void OnBattleActorMoveEnd(BattleActor a);

    void OnBattleActorPerformMove(BattleActor a, GridPosition p, int dir, bool cameraFollow);

    void OnBattleActorPunchMove(BattleActor a, string fxName, bool isHurt);

    void OnBattleActorExchangeMove(BattleActor a, BattleActor b, int moveType, string fxName);

    void OnBattleActorSetDir(BattleActor a, int dir);

    void OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode);

    void OnBattleActorPlayAnimation(BattleActor a, string animationName, int animationTime);

    void OnBattleActorChangeIdleAnimation(BattleActor a, string idleAnimationName);

    void OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActor);

    void OnBattleActorSkillHitBegin(BattleActor a, ConfigDataSkillInfo skillInfo, bool isRebound);

    void OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound);

    void OnBattleActorSkillHitEnd(BattleActor a, ConfigDataSkillInfo skillInfo, bool isRebound);

    void OnBattleActorAttachBuff(BattleActor a, BuffState buffState);

    void OnBattleActorDetachBuff(BattleActor a, BuffState buffState);

    void OnBattleActorImmune(BattleActor a);

    void OnBattleActorModifyHp(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      string fxName);

    void OnBattleActorPassiveSkill(BattleActor a, BattleActor target, BuffState sourceBuffState);

    void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType);

    void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType);

    void OnBattleActorTeleport(BattleActor a, ConfigDataSkillInfo skillInfo, GridPosition p);

    void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo);

    void OnBattleActorDie(BattleActor a, bool isAfterCombat);

    void OnBattleActorAppear(BattleActor a, int effectType, string fxName);

    void OnBattleActorDisappear(BattleActor a, int effectType, string fxName);

    void OnBattleActorChangeTeam(BattleActor a);

    void OnBattleActorChangeArmy(BattleActor a);

    void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName);

    void OnBattleActorCameraFocus(BattleActor a);

    void OnBattleActorGainBattleTreasure(BattleActor a, ConfigDataBattleTreasureInfo treasureInfo);

    void OnStartGuard(BattleActor a, BattleActor target);

    void OnStopGuard(BattleActor a, BattleActor target);

    void OnBeforeStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo);

    void OnCancelCombat();

    void OnStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo);

    void OnPreStopCombat();

    void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack);

    void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType);

    void OnCombatActorDie(CombatActor a);

    void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      ConfigDataCutsceneInfo cutsceneInfo2,
      int team);

    void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team);

    void OnStopSkillCutscene();

    void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo);

    void OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo);

    void OnStopBattlePerform();

    void OnChangeMapTerrain(List<GridPosition> positions, ConfigDataTerrainInfo terrainInfo);

    void OnChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo);

    void OnCameraFocus(GridPosition p);

    void OnPlayMusic(string musicName);

    void OnPlaySound(string soundName);

    void OnPlayFx(string fxName, GridPosition p);

    void OnWaitTime(int timeInMs);

    void OnBossPhase(int phase);

    void OnBattleTreasureCreate(ConfigDataBattleTreasureInfo treasureInfo, bool isOpened);

    IBattleGraphic CreateCombatGraphic(string assetName, float scale);

    void DestroyCombatGraphic(IBattleGraphic g);

    IBattleGraphic PlayCombatFx(string assetName, float scale);

    void PlaySound(string name);

    void DrawLine(Vector2i p0, Vector2i p1, Colori color);

    void DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color);

    void LogBattleStart();

    void LogBattleStop(bool isWin);

    void LogBattleTeam(BattleTeam team0, BattleTeam team1);

    void LogActorMove(BattleActor actor, GridPosition fromPos, GridPosition toPos);

    void LogActorStandby(BattleActor actor);

    void LogActorAttack(BattleActor actor, BattleActor targetActor);

    void LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos);

    void LogActorDie(BattleActor actor, BattleActor killerActor);
  }
}
