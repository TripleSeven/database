﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BattlePropertyModifier
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BattlePropertyModifier
  {
    private int[] m_values;
    public int ExchangeAttack;
    public int ExchangeDefense;
    public int ExchangeMagic;
    public int ExchangeMagicDefense;
    public int ExchangeDexterity;
    public int ExchangeHealthPointMax;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Get;
    private static DelegateBridge __Hotfix_Collect;
    private static DelegateBridge __Hotfix_Clear;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattlePropertyModifier()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int Get(PropertyModifyType t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Collect(PropertyModifyType t, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
