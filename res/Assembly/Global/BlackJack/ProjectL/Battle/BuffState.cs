﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Battle.BuffState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Battle
{
  public class BuffState
  {
    public int m_id;
    public int m_displayOrder;
    public ConfigDataBuffInfo m_buffInfo;
    public BuffSourceType m_sourceType;
    public ConfigDataSkillInfo m_sourceSkillInfo;
    public BuffState m_sourceBuffState;
    public int m_time;
    public int m_effectTimes;
    public bool m_isEffective;
    public bool m_hasExtraTime;
    public bool m_checkSelfDie;
    public BattleActor m_applyer;
    public BuffState m_parent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_CanChangeTime;
    private static DelegateBridge __Hotfix_IsApplyerTeam;
    private static DelegateBridge __Hotfix_GetRootSourceType;
    private static DelegateBridge __Hotfix_GetRootSourceSkillInfo;
    private static DelegateBridge __Hotfix_IsChild;
    private static DelegateBridge __Hotfix_CanNotDispel;

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffState(
      int id,
      ConfigDataBuffInfo buffInfo,
      BattleActor applyer,
      BuffSourceType sourceType,
      ConfigDataSkillInfo sourceSkillInfo,
      BuffState sourceBuffState,
      bool checkSelfDie)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanChangeTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsApplyerTeam(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BuffSourceType GetRootSourceType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ConfigDataSkillInfo GetRootSourceSkillInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsChild()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanNotDispel()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
