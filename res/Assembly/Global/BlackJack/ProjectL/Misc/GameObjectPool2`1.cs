﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.GameObjectPool2`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  public class GameObjectPool2<T> where T : MonoBehaviour
  {
    private List<T> m_freeList;
    private GameObject m_prefab;
    private Transform m_parent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Setup;
    private static DelegateBridge __Hotfix_Allocate_0;
    private static DelegateBridge __Hotfix_Allocate_1;
    private static DelegateBridge __Hotfix_Free;
    private static DelegateBridge __Hotfix_Destroy;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObjectPool2()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Setup(GameObject prefab, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Allocate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T Allocate(out bool isNew)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Free(T ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
