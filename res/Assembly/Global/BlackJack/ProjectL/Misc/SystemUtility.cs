﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.SystemUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  public class SystemUtility
  {
    private static readonly System.Random _random;
    private static int _timeScale;
    private static readonly int _timeScaleMask;
    private static ConfigDataDeviceSetting s_deviceSetting;
    private static bool s_needUpdateDeviceSetting;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_TimeScale;
    private static DelegateBridge __Hotfix_set_TimeScale;
    private static DelegateBridge __Hotfix_SetTimeScale;
    private static DelegateBridge __Hotfix_GetConfigDataDeviceSetting;
    private static DelegateBridge __Hotfix_IsiPhoneX;
    private static DelegateBridge __Hotfix_IsLowSystemMemory;
    private static DelegateBridge __Hotfix_IsLargeSystemMemory;
    private static DelegateBridge __Hotfix_GetBatteryStatus;
    private static DelegateBridge __Hotfix_GetBatteryLevel;
    private static DelegateBridge __Hotfix_LogBatteryStatus;

    [MethodImpl((MethodImplOptions) 32768)]
    public SystemUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    public static float TimeScale
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static ConfigDataDeviceSetting GetConfigDataDeviceSetting()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsiPhoneX()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLowSystemMemory()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsLargeSystemMemory()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static BatteryStatus GetBatteryStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float GetBatteryLevel()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void LogBatteryStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    static SystemUtility()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
