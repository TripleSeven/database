﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalAccountConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  public class LocalAccountConfig
  {
    private static LocalAccountConfig s_instance;
    private static string m_fileName;
    private LocalAccountConfigData m_data;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SetFileName;
    private static DelegateBridge __Hotfix_Save;
    private static DelegateBridge __Hotfix_Load;
    private static DelegateBridge __Hotfix_ResetLocalAccountConfigData;
    private static DelegateBridge __Hotfix_AddHaveReadHeroBiography;
    private static DelegateBridge __Hotfix_AddHaveReadHeroPerformance;
    private static DelegateBridge __Hotfix_AddUnlockHeroBiography;
    private static DelegateBridge __Hotfix_AddUnlockHeroPerformance;
    private static DelegateBridge __Hotfix_AddUnlockHeroDungeonLevelId;
    private static DelegateBridge __Hotfix_AddUnlockHeroFetterId;
    private static DelegateBridge __Hotfix_SetArenaAttackerHeroIds;
    private static DelegateBridge __Hotfix_SetHaveDoneMemoryExtraction;
    private static DelegateBridge __Hotfix_SetLastNotifyPeakArenaTime1;
    private static DelegateBridge __Hotfix_GetLastNotifyPeakArenaTime1;
    private static DelegateBridge __Hotfix_SetLastNotifyPeakArenaTime2;
    private static DelegateBridge __Hotfix_GetLastNotifyPeakArenaTime2;
    private static DelegateBridge __Hotfix_SetLastPeakArenaMatchRound;
    private static DelegateBridge __Hotfix_GetIsPeakArenaMatchRoundChanged;
    private static DelegateBridge __Hotfix_RemoveExpireActivityLastReadTime;
    private static DelegateBridge __Hotfix_GetActivityLastReadTime;
    private static DelegateBridge __Hotfix_SetActivityLastReadTime;
    private static DelegateBridge __Hotfix_IsNeedDetectLanguage;
    private static DelegateBridge __Hotfix_GetChatMsgLanguage;
    private static DelegateBridge __Hotfix_SaveChatMsgLanguageToArray;
    private static DelegateBridge __Hotfix_CheckAndSaveLanguageToChatMsgLanguage;
    private static DelegateBridge __Hotfix_SaveLastCheckMsgLanguageTime;
    private static DelegateBridge __Hotfix_RemoveExpireRecommendGiftBoxReadTime;
    private static DelegateBridge __Hotfix_GetRecommendGiftBoxReadTime;
    private static DelegateBridge __Hotfix_SetRecommendGiftBoxReadTime;
    private static DelegateBridge __Hotfix_get_Data;
    private static DelegateBridge __Hotfix_set_Instance;
    private static DelegateBridge __Hotfix_get_Instance;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalAccountConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFileName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Save()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetLocalAccountConfigData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHaveReadHeroBiography(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddHaveReadHeroPerformance(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroBiography(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroPerformance(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroDungeonLevelId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUnlockHeroFetterId(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetArenaAttackerHeroIds(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHaveDoneMemoryExtraction(bool haveDoneMemoryExtraction)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastNotifyPeakArenaTime1(DateTime finishNotifyTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLastNotifyPeakArenaTime1()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastNotifyPeakArenaTime2(DateTime finishNotifyTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetLastNotifyPeakArenaTime2()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLastPeakArenaMatchRound(int round)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetIsPeakArenaMatchRoundChanged()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpireActivityLastReadTime(List<OperationalActivityBase> existActivityList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetActivityLastReadTime(ulong activityInstanceID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActivityLastReadTime(ulong activityInstanceId, DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsNeedDetectLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetChatMsgLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveChatMsgLanguageToArray(string lang, DateTime t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckAndSaveLanguageToChatMsgLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SaveLastCheckMsgLanguageTime(DateTime t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveExpireRecommendGiftBoxReadTime(
      List<ConfigDataGiftStoreItemInfo> giftStoreItemList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetRecommendGiftBoxReadTime(int ID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecommendGiftBoxReadTime(int ID, DateTime dateTime)
    {
      // ISSUE: unable to decompile the method.
    }

    public LocalAccountConfigData Data
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static LocalAccountConfig Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
