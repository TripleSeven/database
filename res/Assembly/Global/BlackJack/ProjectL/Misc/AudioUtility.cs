﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.AudioUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime;
using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  public class AudioUtility
  {
    public const string MusicCategory = "Music_VolumeControl";
    public const string SoundCategory = "SFX_VolumeControl";
    public const string VoiceCategory = "Voice_VolumeControl";
    public const string StopAllMusic = "Action_StopMusic";
    public const string StopAllVoice = "Action_StopVoice";
    public const string AllSoundsCategory = "All_Game_Sounds";
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnPlaySound;
    private static DelegateBridge __Hotfix_NormalizeVolume;
    private static DelegateBridge __Hotfix_ClipAudioByLength;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_PlaySound_0;
    private static DelegateBridge __Hotfix_GetLanguages;
    private static DelegateBridge __Hotfix_SetLanguage;
    private static DelegateBridge __Hotfix_GetLanguage;
    private static DelegateBridge __Hotfix_StopSound;
    private static DelegateBridge __Hotfix_SearchAndPlaySpineAnimEventSound;
    private static DelegateBridge __Hotfix_SoundIDToName;
    private static DelegateBridge __Hotfix_PlaySound_1;
    private static DelegateBridge __Hotfix_PlaySound_2;
    private static DelegateBridge __Hotfix_SetVolume;
    private static DelegateBridge __Hotfix_GetVolume;

    [MethodImpl((MethodImplOptions) 32768)]
    public AudioUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlaySound(OnClickSound e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float[] NormalizeVolume(float[] voiceData)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static AudioClip ClipAudioByLength(AudioClip ac, float realRecordLength)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static IAudioPlayback PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static List<string> GetLanguages()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetLanguage(string language)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetLanguage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void StopSound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SearchAndPlaySpineAnimEventSound(
      string spineDataName,
      string animationName,
      string eventName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static string SoundIDToName(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void PlaySound(AudioClip a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void SetVolume(string category, float volume, bool isSmooth = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float GetVolume(string category)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
