﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.CheckIfTurnOnLowResourcePrompt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  public class CheckIfTurnOnLowResourcePrompt
  {
    private static float m_lastMemWarningTime;
    private static float m_memWarningCount;
    private const float m_memWarningTimeStep = 3f;
    private const float m_showPromptThreshhold = 8.5f;
    private const string m_isShowPromptKey = "IsShowLowResourcePrompt";
    private const long m_showPromptMemThreshhold = 440000000;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_OnMemWarning;
    private static DelegateBridge __Hotfix_get_IsShowPrompt;
    private static DelegateBridge __Hotfix_set_IsShowPrompt;

    [MethodImpl((MethodImplOptions) 32768)]
    public CheckIfTurnOnLowResourcePrompt()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void OnMemWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    public static bool IsShowPrompt
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
