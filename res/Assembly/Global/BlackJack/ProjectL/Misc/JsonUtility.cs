﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.JsonUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using SimpleJson;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  public class JsonUtility
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Serialize;
    private static DelegateBridge __Hotfix_Deserialize_0;
    private static DelegateBridge __Hotfix_Deserialize_1;
    private static DelegateBridge __Hotfix_SerializeObject;
    private static DelegateBridge __Hotfix_SerializeArray;
    private static DelegateBridge __Hotfix_DeserializeObject;
    private static DelegateBridge __Hotfix_DeserializeArray;
    private static DelegateBridge __Hotfix_AddArray;
    private static DelegateBridge __Hotfix_AddObject_1;
    private static DelegateBridge __Hotfix_AddObject_0;
    private static DelegateBridge __Hotfix_GetObject;
    private static DelegateBridge __Hotfix_GetArray;
    private static DelegateBridge __Hotfix_GetObj;
    private static DelegateBridge __Hotfix_Set_6;
    private static DelegateBridge __Hotfix_Get_13;
    private static DelegateBridge __Hotfix_Set_5;
    private static DelegateBridge __Hotfix_Get_12;
    private static DelegateBridge __Hotfix_Set_11;
    private static DelegateBridge __Hotfix_Get_18;
    private static DelegateBridge __Hotfix_Set_4;
    private static DelegateBridge __Hotfix_Get_11;
    private static DelegateBridge __Hotfix_Set_10;
    private static DelegateBridge __Hotfix_Get_17;
    private static DelegateBridge __Hotfix_Set_7;
    private static DelegateBridge __Hotfix_Get_14;
    private static DelegateBridge __Hotfix_Set_1;
    private static DelegateBridge __Hotfix_Get_8;
    private static DelegateBridge __Hotfix_Set_3;
    private static DelegateBridge __Hotfix_Get_10;
    private static DelegateBridge __Hotfix_Set_8;
    private static DelegateBridge __Hotfix_Get_15;
    private static DelegateBridge __Hotfix_Set_0;
    private static DelegateBridge __Hotfix_Get_7;
    private static DelegateBridge __Hotfix_Set_9;
    private static DelegateBridge __Hotfix_Get_16;
    private static DelegateBridge __Hotfix_Set_2;
    private static DelegateBridge __Hotfix_Get_9;
    private static DelegateBridge __Hotfix_Get_2;
    private static DelegateBridge __Hotfix_Get_1;
    private static DelegateBridge __Hotfix_Get_5;
    private static DelegateBridge __Hotfix_Get_4;
    private static DelegateBridge __Hotfix_Get_6;
    private static DelegateBridge __Hotfix_Get_3;
    private static DelegateBridge __Hotfix_Get_0;

    [MethodImpl((MethodImplOptions) 32768)]
    public JsonUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string Serialize(object j)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static object Deserialize(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T Deserialize<T>(string txt) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string SerializeObject(JsonObject j)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string SerializeArray(JsonArray j)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonObject DeserializeObject(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonArray DeserializeArray(string txt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonArray AddArray(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonObject AddObject(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonObject AddObject(JsonArray a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonObject GetObject(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static JsonArray GetArray(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static object GetObj(JsonObject j, string key)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, long value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref long value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, uint value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref uint value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, short value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref short value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, ushort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref ushort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, sbyte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref sbyte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, byte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref byte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, double value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref double value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref float value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref bool value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref string value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void Set(JsonObject j, string key, DateTime value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref DateTime value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PLong value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PInt value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PUInt value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PShort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PUShort value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PSByte value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool Get(JsonObject j, string key, ref PByte value)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
