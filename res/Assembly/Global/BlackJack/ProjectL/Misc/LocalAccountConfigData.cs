﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalAccountConfigData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  public class LocalAccountConfigData
  {
    public string[] HaveReadAnnounceActivities;
    public Dictionary<ulong, string> LastReadActivityTimeList;
    public Dictionary<int, string> LastReadRecommendGiftBoxTimeDic;
    public int[] HaveReadHeroBiographyIds;
    public int[] HaveReadHeroPerformanceIds;
    public int[] UnlockHeroBiographyIds;
    public int[] UnlockHeroPerformanceIds;
    public int[] UnlockHeroDungeonLevelIds;
    public int[] UnlockHeroFetterIds;
    public int[] ArenaAttackerHeroIds;
    public int TeamPlayerLevelMin;
    public int TeamPlayerLevelMax;
    public bool IsRealtimePVPShowNotice;
    public bool HaveDoneMemoryExtraction;
    public DateTime LastNotifyPeakArenaTime1;
    public DateTime LastNotifyPeakArenaTime2;
    public string GMString;
    public int LastPeakArenaMatchRound;
    public bool IsPeakArenaMatchRoundChanged;
    public DateTime LastCheckMsgLanguageTime;
    public string[] ChatMsgLanguageArray;
    public string ChatMsgLanguage;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalAccountConfigData()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
