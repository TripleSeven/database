﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.AssetUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.TaskNs;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  public class AssetUtility
  {
    public const string DataPath = "Assets/GameProject/RuntimeAssets/";
    private static AssetUtility s_instance;
    private List<IDynamicAssetProvider> m_dynamicAssetProviders;
    private List<Dictionary<string, UnityEngine.Object>> m_dynamicAssetCaches;
    private List<LruAssetCache> m_lruAssetCaches;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_MakeSpriteAssetName_0;
    private static DelegateBridge __Hotfix_MakeSpriteAssetName_1;
    private static DelegateBridge __Hotfix_AddAssetToList;
    private static DelegateBridge __Hotfix_AddSpriteAssetToList;
    private static DelegateBridge __Hotfix_RegisterDynamicAssetProvider;
    private static DelegateBridge __Hotfix_UnregisterDynamicAssetProvider;
    private static DelegateBridge __Hotfix_RegisterDynamicAssetCache;
    private static DelegateBridge __Hotfix_UnregisterDynamicAssetCache;
    private static DelegateBridge __Hotfix_GetAsset;
    private static DelegateBridge __Hotfix_GetSprite;
    private static DelegateBridge __Hotfix__GetAsset;
    private static DelegateBridge __Hotfix_InitLruAssetCache;
    private static DelegateBridge __Hotfix_UninitAllLruAssetCache;
    private static DelegateBridge __Hotfix_AddAssetToLruCache_0;
    private static DelegateBridge __Hotfix_AddAssetToLruCache_1;
    private static DelegateBridge __Hotfix_ClearLruCache;
    private static DelegateBridge __Hotfix_ClearAllLruCache;
    private static DelegateBridge __Hotfix_GetLruAssetCache;
    private static DelegateBridge __Hotfix_TestMemoryWarning;
    private static DelegateBridge __Hotfix_OnLowMemoryWarning;
    private static DelegateBridge __Hotfix_set_Instance;
    private static DelegateBridge __Hotfix_get_Instance;
    private static DelegateBridge __Hotfix_add_EventOnMemoryWarning;
    private static DelegateBridge __Hotfix_remove_EventOnMemoryWarning;

    [MethodImpl((MethodImplOptions) 32768)]
    public AssetUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string MakeSpriteAssetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string MakeSpriteAssetName(string name, string subName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddAssetToList(string name, List<string> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void AddSpriteAssetToList(string name, List<string> list)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterDynamicAssetProvider(IDynamicAssetProvider assetProvider)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterDynamicAssetProvider(IDynamicAssetProvider assetProvider)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RegisterDynamicAssetCache(Dictionary<string, UnityEngine.Object> assetCache)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UnregisterDynamicAssetCache(Dictionary<string, UnityEngine.Object> assetCache)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public T GetAsset<T>(string name) where T : UnityEngine.Object
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Sprite GetSprite(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private UnityEngine.Object _GetAsset(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitLruAssetCache(int cacheType, int maxCount)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UninitAllLruAssetCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAssetToLruCache(int cacheType, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAssetToLruCache(int cacheType, UnityEngine.Object a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLruCache(int cacheType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAllLruCache()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LruAssetCache GetLruAssetCache(int cacheType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TestMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnLowMemoryWarning()
    {
      // ISSUE: unable to decompile the method.
    }

    public static AssetUtility Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public event Action EventOnMemoryWarning
    {
      [MethodImpl((MethodImplOptions) 32768)] add
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] remove
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
