﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalProcessingBattleData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  public class LocalProcessingBattleData
  {
    public int Version;
    public int Type;
    public int TypeId;
    public int RandomSeed;
    public int ArmyRandomSeed;
    public int Step;
    public int RegretCount;
    public LocalProcessingBattleData.LocalBattleCommand[] BattleCommands;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalProcessingBattleData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    public class LocalBattleCommand
    {
      public int Type;
      public int Step;
      public int Actor;
      public int Skill;
      public int PX;
      public int PY;
      public int P2X;
      public int P2Y;
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_ToBattleCommand;
      private static DelegateBridge __Hotfix_FromBattleCommand;

      [MethodImpl((MethodImplOptions) 32768)]
      public LocalBattleCommand()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public BattleCommand ToBattleCommand()
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      public void FromBattleCommand(BattleCommand cmd)
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
