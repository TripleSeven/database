﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.LocalConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Misc
{
  public class LocalConfig
  {
    private static LocalConfig s_instance;
    private static string m_fileName;
    private LocalConfigData m_data;
    private const int m_maxRecentLoginServerCount = 2;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SetFileName;
    private static DelegateBridge __Hotfix_get_IsAgreeDataNotice;
    private static DelegateBridge __Hotfix_set_IsAgreeDataNotice;
    private static DelegateBridge __Hotfix_Save;
    private static DelegateBridge __Hotfix_Load;
    private static DelegateBridge __Hotfix_ApplyVolume;
    private static DelegateBridge __Hotfix_Apply;
    private static DelegateBridge __Hotfix_AddRecentLoginServerID;
    private static DelegateBridge __Hotfix_SetAutoBattleId;
    private static DelegateBridge __Hotfix_GetAutoBattleId;
    private static DelegateBridge __Hotfix_SetTestUIId;
    private static DelegateBridge __Hotfix_GetTestUIId;
    private static DelegateBridge __Hotfix_SetBattleDanmakuState;
    private static DelegateBridge __Hotfix_SetHeroListSortType;
    private static DelegateBridge __Hotfix_SetIsOnlyShowCurJobSkin;
    private static DelegateBridge __Hotfix_SetIsSetSkinToAllSoldier;
    private static DelegateBridge __Hotfix_UpdateGMUser;
    private static DelegateBridge __Hotfix_get_Data;
    private static DelegateBridge __Hotfix_set_Instance;
    private static DelegateBridge __Hotfix_get_Instance;

    [MethodImpl((MethodImplOptions) 32768)]
    public LocalConfig()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFileName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    public bool IsAgreeDataNotice
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Save()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Load()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ApplyVolume(string category, bool isInitialize = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Apply(bool isInitialize = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRecentLoginServerID(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattleId(int battleId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAutoBattleId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetTestUIId(int testType, int testId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTestUIId(int testType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetBattleDanmakuState(bool isShow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeroListSortType(int type)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsOnlyShowCurJobSkin(bool onlyShowCurJobSkin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsSetSkinToAllSoldier(bool setSkinToAllSoldier)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateGMUser(bool isGMUser)
    {
      // ISSUE: unable to decompile the method.
    }

    public LocalConfigData Data
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public static LocalConfig Instance
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
