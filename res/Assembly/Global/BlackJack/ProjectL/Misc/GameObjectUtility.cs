﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Misc.GameObjectUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.Prefab;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Misc
{
  public class GameObjectUtility
  {
    private static GameObject m_sceneRoot;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_SceneRoot;
    private static DelegateBridge __Hotfix_FindChildObject_0;
    private static DelegateBridge __Hotfix_FindChildObject_1;
    private static DelegateBridge __Hotfix_FindChildGameObject_R;
    private static DelegateBridge __Hotfix_GetDefaultName;
    private static DelegateBridge __Hotfix_AddControllerToGameObject_0;
    private static DelegateBridge __Hotfix_AddControllerToGameObject_1;
    private static DelegateBridge __Hotfix_FindGameObjectByName;
    private static DelegateBridge __Hotfix_FindComponentByName;
    private static DelegateBridge __Hotfix_CloneGameObject;
    private static DelegateBridge __Hotfix_DestroyChildren;
    private static DelegateBridge __Hotfix_DestroyChildrenImmediate;
    private static DelegateBridge __Hotfix_DestroyComponentList;
    private static DelegateBridge __Hotfix_InactiveComponentList;
    private static DelegateBridge __Hotfix_HasChinese;

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObjectUtility()
    {
      // ISSUE: unable to decompile the method.
    }

    public static GameObject SceneRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildObject(GameObject parentObj, string[] path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void FindChildObject(
      GameObject parentObj,
      string[] path,
      Action<GameObject> action)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindChildGameObject_R(GameObject go, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static string GetDefaultName(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T AddControllerToGameObject<T>(GameObject go) where T : class
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static PrefabControllerBase AddControllerToGameObject(
      System.Type type,
      GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject FindGameObjectByName(Transform parent, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static T FindComponentByName<T>(Transform parent, string name) where T : MonoBehaviour
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static GameObject CloneGameObject(GameObject cloneObj, Transform parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroyChildren(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroyChildrenImmediate(GameObject obj)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void DestroyComponentList<T>(List<T> list) where T : MonoBehaviour
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void InactiveComponentList<T>(List<T> list) where T : MonoBehaviour
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool HasChinese(string str)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
