﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.CommonReflection.IComponentBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

namespace BlackJack.ProjectL.CommonReflection
{
  public interface IComponentBase
  {
    string GetName();

    void Init();

    void PostInit();

    void DeInit();

    bool Serialize<T>(T dest);

    void DeSerialize<T>(T source);

    void PostDeSerialize();

    IComponentOwner Owner { get; set; }
  }
}
