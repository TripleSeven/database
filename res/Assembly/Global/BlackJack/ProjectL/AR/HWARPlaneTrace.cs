﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.AR.HWARPlaneTrace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using HuaweiARUnitySDK;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.AR
{
  public class HWARPlaneTrace : ARPlaneTrace
  {
    private ARConfigBase m_config;
    private bool m_installRequested;
    private bool m_isSessionCreated;
    private List<ARAnchor> m_addedAnchors;
    private const int m_maxAnchorCount = 16;
    private bool isCachedOrientation;
    private ScreenOrientation orientation;
    private bool autorotateToLandscapeLeft;
    private bool autorotateToLandscapeRight;
    private bool autorotateToPortrait;
    private bool autorotateToPortraitUpsideDown;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_AlwaysUpdate;
    private static DelegateBridge __Hotfix_Raycast;
    private static DelegateBridge __Hotfix_OnInit;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_HandleStartOrResume;
    private static DelegateBridge __Hotfix_HandleStopOrPause;
    private static DelegateBridge __Hotfix_Connect;
    private static DelegateBridge __Hotfix_ConnectToService;

    [MethodImpl((MethodImplOptions) 32768)]
    public HWARPlaneTrace()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void AlwaysUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool Raycast(Vector3 screenPos, out Vector3 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override void OnInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnResume()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleStartOrResume()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleStopOrPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Connect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ConnectToService()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
