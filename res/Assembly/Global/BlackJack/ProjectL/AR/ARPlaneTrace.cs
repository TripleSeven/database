﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.AR.ARPlaneTrace
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.AR
{
  public abstract class ARPlaneTrace
  {
    private const float m_findingSquareDist = 0.5f;
    protected Vector3 m_centerPos;
    protected Quaternion m_centerRotation;
    protected Camera m_camera;
    private bool m_isInitialized;
    private ARPlaneTrace.EFocusState m_focusState;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_CenterPos;
    private static DelegateBridge __Hotfix_get_CenterRotation;
    private static DelegateBridge __Hotfix_get_FocusState;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_CalculateLookVector;
    private static DelegateBridge __Hotfix_OnInit;
    private static DelegateBridge __Hotfix_OnStart;
    private static DelegateBridge __Hotfix_OnStop;
    private static DelegateBridge __Hotfix_OnPause;
    private static DelegateBridge __Hotfix_OnResume;
    private static DelegateBridge __Hotfix_AlwaysUpdate;

    [MethodImpl((MethodImplOptions) 32768)]
    protected ARPlaneTrace()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector3 CenterPos
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Quaternion CenterRotation
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ARPlaneTrace.EFocusState FocusState
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Init(Camera camera)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Quaternion CalculateLookVector(Vector3 srcPos, Vector3 destPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected virtual void OnInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnPause()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void OnResume()
    {
      // ISSUE: unable to decompile the method.
    }

    protected abstract bool Raycast(Vector3 center, out Vector3 pos);

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual void AlwaysUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum EFocusState
    {
      Initializing,
      Finding,
      Found,
    }
  }
}
