﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldPathfinder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class WorldPathfinder
  {
    private LinkedList<WorldPathfinder.Node> m_OpenList;
    private List<WorldPathfinder.Node> m_ClosedList;
    private List<WorldPathfinder.Node> m_Successors;
    private WorldPathfinder.SearchState m_State;
    private int m_Steps;
    private WorldPathfinder.Node m_Start;
    private WorldPathfinder.Node m_Goal;
    private WorldPathfinder.Node m_CurrentSolutionNode;
    private bool m_CancelRequest;
    private List<WorldPathfinder.Node> m_NodePool;
    private int m_AllocateNodeCount;
    private List<WorldPathNode> m_PathNodePool;
    private int m_AllocatedPathNodeCount;
    private const int kPreallocatedNodes = 64;
    private ClientWorld m_clientWorld;
    private CollectionActivityWorld m_collectionActivityWorld;
    private bool m_isCheckWaypointStatus;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_SortedAddToOpenList;
    private static DelegateBridge __Hotfix_AllocateNode;
    private static DelegateBridge __Hotfix_AllocatePathNode;
    private static DelegateBridge __Hotfix_InitiatePathfind;
    private static DelegateBridge __Hotfix_CancelSearch;
    private static DelegateBridge __Hotfix_SetStartAndGoalStates;
    private static DelegateBridge __Hotfix_SearchStep;
    private static DelegateBridge __Hotfix_AddSuccessor;
    private static DelegateBridge __Hotfix_GetSolutionStart;
    private static DelegateBridge __Hotfix_GetSolutionNext;
    private static DelegateBridge __Hotfix_FreeSolutionNodes;
    private static DelegateBridge __Hotfix_get_StartNode;
    private static DelegateBridge __Hotfix_get_GoalNode;
    private static DelegateBridge __Hotfix_HasStartNode;
    private static DelegateBridge __Hotfix_HasGoalNode;
    private static DelegateBridge __Hotfix_GetWaypointNeighbors;
    private static DelegateBridge __Hotfix_GetWaypointPosition;
    private static DelegateBridge __Hotfix_CanMoveToWaypoint;
    private static DelegateBridge __Hotfix_FindPath_0;
    private static DelegateBridge __Hotfix_FindPath_1;
    private static DelegateBridge __Hotfix_FindPath_2;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathfinder()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SortedAddToOpenList(WorldPathfinder.Node node)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private WorldPathfinder.Node AllocateNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode AllocatePathNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitiatePathfind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelSearch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStartAndGoalStates(WorldPathNode start, WorldPathNode goal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathfinder.SearchState SearchStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSuccessor(WorldPathNode state)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode GetSolutionStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode GetSolutionNext()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FreeSolutionNodes()
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldPathNode StartNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public WorldPathNode GoalNode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasStartNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasGoalNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetWaypointNeighbors(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetWaypointPosition(int id, out Vector2 position)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool CanMoveToWaypoint(int id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      ClientWorld world,
      int start,
      int goal,
      List<int> path,
      bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(
      CollectionActivityWorld world,
      int start,
      int goal,
      List<int> path,
      bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    public enum SearchState
    {
      NotInitialized,
      Searching,
      Succeeded,
      Failed,
    }

    public class Node
    {
      public WorldPathfinder.Node parent;
      public WorldPathfinder.Node child;
      public float g;
      public float h;
      public float f;
      public WorldPathNode m_UserState;
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_Reinitialize;

      public Node()
      {
        this.Reinitialize();
        WorldPathfinder.Node._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }

      public void Reinitialize()
      {
        DelegateBridge hotfixReinitialize = WorldPathfinder.Node.__Hotfix_Reinitialize;
        if (hotfixReinitialize != null)
        {
          hotfixReinitialize.__Gen_Delegate_Imp4((object) this);
        }
        else
        {
          this.parent = (WorldPathfinder.Node) null;
          this.child = (WorldPathfinder.Node) null;
          this.g = 0.0f;
          this.h = 0.0f;
          this.f = 0.0f;
        }
      }
    }
  }
}
