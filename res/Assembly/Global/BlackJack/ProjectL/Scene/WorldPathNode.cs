﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldPathNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class WorldPathNode
  {
    public int m_waypointId;
    public Vector2 m_position;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_IsSameState;
    private static DelegateBridge __Hotfix_GetGoalHeuristic;
    private static DelegateBridge __Hotfix_GetCost;
    private static DelegateBridge __Hotfix_GetSuccessors;
    private static DelegateBridge __Hotfix_AddSuccessor;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldPathNode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSameState(WorldPathNode rhs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetGoalHeuristic(WorldPathfinder pathfinder)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetCost(WorldPathfinder pathfinder, WorldPathNode successor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetSuccessors(WorldPathfinder pathfinder, WorldPathNode parentNode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddSuccessor(WorldPathfinder pathfinder, int id, WorldPathNode parentNode)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
