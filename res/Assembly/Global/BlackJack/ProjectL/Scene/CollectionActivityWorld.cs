﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CollectionActivityWorld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.UI;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class CollectionActivityWorld
  {
    private int m_entityIdCounter;
    private int m_frameCount;
    private float m_worldTickTime;
    private bool m_isPaused;
    private bool m_enableDebugDraw;
    private RandomNumber m_randomNumber;
    private ProjectLPlayerContext m_playerContext;
    private List<CollectionActivityPlayerActor> m_playerActors;
    private List<CollectionActivityEventActor> m_eventActors;
    private List<CollectionActivityWaypoint> m_waypoints;
    private IConfigDataLoader m_configDataLoader;
    private ICollectionActivityWorldListener m_collectionActivityWorldListener;
    private WorldCamera m_worldCamera;
    private WorldPathfinder m_pathfinder;
    private GameObject m_worldRoot;
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_background;
    private GameObject m_backgroundWaypointsRoot;
    private GameObject m_worldUIRoot;
    private GameObject m_waypointUIRoot;
    private GameObject m_eventUIRoot;
    private GameObject m_playerUIRoot;
    private GameObject m_waypointUIPrefab;
    private GameObject m_waypoint2UIPrefab;
    private GameObject m_eventUIPrefab;
    private GameObject m_playerUIPrefab;
    private WorldScenarioUIController m_worldScenarioUIController;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;
    private Quaternion m_faceCameraRotation;
    private bool m_isCameraFollowPlayer;
    private ConfigDataCollectionActivityInfo m_collectionActivityInfo;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Is2D;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickCollectionActivityWorld;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_ShowWorld;
    private static DelegateBridge __Hotfix_CreateWaypoint;
    private static DelegateBridge __Hotfix_CreatePlayerActor;
    private static DelegateBridge __Hotfix_CreateEventActor;
    private static DelegateBridge __Hotfix_UpdateWaypointState;
    private static DelegateBridge __Hotfix_UpdatePlayerGraphicAndLocation;
    private static DelegateBridge __Hotfix_UpdateActiveScenarioAndWaypointState;
    private static DelegateBridge __Hotfix_UpdateEventActorState;
    private static DelegateBridge __Hotfix_GetWaypoint;
    private static DelegateBridge __Hotfix_GetEventActorAt;
    private static DelegateBridge __Hotfix_GetEventActoryByEventId;
    private static DelegateBridge __Hotfix_GetEventActors;
    private static DelegateBridge __Hotfix_GetPlayerActor;
    private static DelegateBridge __Hotfix_OnWaypointClick;
    private static DelegateBridge __Hotfix_OnEventActorClick;
    private static DelegateBridge __Hotfix_CreateGraphic;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_GetCamera;
    private static DelegateBridge __Hotfix_GetCameraPosition;
    private static DelegateBridge __Hotfix_ComputeActorPosition;
    private static DelegateBridge __Hotfix_IsCulled_0;
    private static DelegateBridge __Hotfix_IsCulled_1;
    private static DelegateBridge __Hotfix_GetNextEntityId;
    private static DelegateBridge __Hotfix_CreateBackground;
    private static DelegateBridge __Hotfix_ClearBackground;
    private static DelegateBridge __Hotfix_ShowBackgroundChild;
    private static DelegateBridge __Hotfix_PlaySound_1;
    private static DelegateBridge __Hotfix_PlaySound_0;
    private static DelegateBridge __Hotfix_FindPath;
    private static DelegateBridge __Hotfix_FrameToMillisecond;
    private static DelegateBridge __Hotfix_MillisecondToFrame;
    private static DelegateBridge __Hotfix_MillisecondToFrame1;
    private static DelegateBridge __Hotfix_get_WorldCamera;
    private static DelegateBridge __Hotfix_set_EnableDebugDraw;
    private static DelegateBridge __Hotfix_get_EnableDebugDraw;
    private static DelegateBridge __Hotfix_get_GraphicRoot;
    private static DelegateBridge __Hotfix_get_WorldUIRoot;
    private static DelegateBridge __Hotfix_get_WaypointUIRoot;
    private static DelegateBridge __Hotfix_get_EventUIRoot;
    private static DelegateBridge __Hotfix_get_PlayerUIRoot;
    private static DelegateBridge __Hotfix_get_WaypointUIPrefab;
    private static DelegateBridge __Hotfix_get_Waypoint2UIPrefab;
    private static DelegateBridge __Hotfix_get_EventUIPrefab;
    private static DelegateBridge __Hotfix_get_PlayerUIPrefab;
    private static DelegateBridge __Hotfix_get_FaceCameraRotation;
    private static DelegateBridge __Hotfix_get_IsCameraFollowPlayer;
    private static DelegateBridge __Hotfix_get_FxPlayer;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_ClientWorldListener;
    private static DelegateBridge __Hotfix_get_PlayerContext;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(
      ICollectionActivityWorldListener collectionActivityWorldListener,
      GameObject worldRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Is2D()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickCollectionActivityWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      ConfigDataCollectionActivityInfo collectionActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityWaypoint CreateWaypoint(
      ConfigDataCollectionActivityWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityPlayerActor CreatePlayerActor(
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CollectionActivityEventActor CreateEventActor(
      ConfigDataCollectionEventInfo collectionEventInfo,
      ConfigDataCollectionActivityWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdatePlayerGraphicAndLocation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveScenarioAndWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateEventActorState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityWaypoint GetWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor GetEventActorAt(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityEventActor GetEventActoryByEventId(
      int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<CollectionActivityEventActor> GetEventActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityPlayerActor GetPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(CollectionActivityWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventActorClick(CollectionActivityEventActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CameraBase GetCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 ComputeActorPosition(Vector2 pos, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 p, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackground(ConfigDataCollectionActivityMapInfo mapInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBackgroundChild(string childName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path, bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int FrameToMillisecond(int frame)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldCamera WorldCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool EnableDebugDraw
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject GraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WorldUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject Waypoint2UIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Quaternion FaceCameraRotation
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsCameraFollowPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer FxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ICollectionActivityWorldListener ClientWorldListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ProjectLPlayerContext PlayerContext
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
