﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Misc;
using BlackJack.ProjectL.UI;
using FixMath.NET;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class ClientBattle : IBattleListener, IFxEventListener, IGenericGraphicContainer
  {
    private ClientBattleState m_state;
    private int m_entityIdCounter;
    private bool m_isStopBattleWin;
    private bool m_isLoadingCombatCharImages;
    private int m_frameCount;
    private int m_endCountdown;
    private int m_cutscenePauseCountdown;
    private int m_restoreNonSkillTargetsCountdown;
    private string m_tickSoundName;
    private float m_battleTickTime;
    private float m_combatTickTime;
    private float m_timeScale;
    private float m_globalTimeScale;
    private SlowMotionState m_slowMotionState;
    private float m_slowMotionCountdown;
    private bool m_isPaused;
    private bool m_isAutoBattle;
    private bool m_isFastBattle;
    private bool m_isEnableDebugDraw;
    private bool m_isEnableSdkLogBattle;
    private bool m_isBattleReport;
    private bool m_isBattleLive;
    private RandomNumber m_randomNumber;
    private BattleBase m_battle;
    private ClientBattleActor m_nullActor;
    private List<ClientBattleActor> m_actors;
    private ClientBattleActor m_activeActor;
    private ClientBattleActor m_cameraFollowActor;
    private List<ClientActorAct> m_actorActs;
    private List<ClientBattleTreasure> m_treasures;
    private List<int> m_enforceActionOrderHeroIds;
    private BattleActor m_combatingBattleActorA;
    private BattleActor m_combatingBattleActorB;
    private ConfigDataSkillInfo m_combatSkillInfoA;
    private int m_ignoreMoveStep;
    private int m_ignoreSkillStep;
    private int m_ignoreTeleportDisappearStep;
    private int m_ignoreActiveTeam;
    private int m_ignoreActiveTurn;
    private ClientActorActSkillHit m_actorActSkillHit;
    private ClientActorActSkillHit m_actorActSkillRebound;
    private ConfigDataBattleDialogInfo m_curBattleDialogInfo;
    private int m_battleDialogResult;
    private bool m_isWaitBattleTreasureDialog;
    private bool m_isWaitBattleTreasureReward;
    private bool m_isWaitFastCombat;
    private bool m_isBattlePerforming;
    private bool m_isBattleBasePerforming;
    private int m_myPlayerIndex;
    private int m_myPlayerTeam;
    private int m_rebuildBattleStepMax;
    private int m_stopCombatMonsterTotalDamage;
    private SkipCombatMode m_skipCombatMode;
    private SkipCombatMode m_curSkipCombatMode;
    private IConfigDataLoader m_configDataLoader;
    private IClientBattleListener m_clientBattleListener;
    private ConfigDataBattleInfo m_battleInfo;
    private ConfigDataArenaBattleInfo m_arenaBattleInfo;
    private ConfigDataPVPBattleInfo m_pvpBattleInfo;
    private ConfigDataRealTimePVPBattleInfo m_realtimePvpBattleInfo;
    private ConfigDataPeakArenaBattleInfo m_peakArenaBattleInfo;
    private int m_peakArenaBoRound;
    private BattleType m_battleType;
    private BattleCamera m_battleCamera;
    private CombatCamera m_combatCamera;
    private CutsceneCamera m_cutsceneCamera;
    private CutsceneCamera m_cutsceneCamera2;
    private GameObject m_battleRoot;
    private GameObject m_combatRoot;
    private GameObject m_cutsceneRoot;
    private GameObject m_battleGraphicRoot;
    private GameObject m_combatGraphicRoot;
    private GameObject m_cutsceneGraphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_mapBackground;
    private GameObject m_mapTerrainFxRoot;
    private GameObject m_mapTreasureRoot;
    private Dictionary<GridPosition, GenericGraphic> m_mapTerrainFxs;
    private Dictionary<GridPosition, GenericGraphic> m_mapTerrainEffectFxs;
    private GameObject m_battleUIRoot;
    private GameObject m_battleActorUIRoot;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_battleFxPlayer;
    private FxPlayer m_combatFxPlayer;
    private FxPlayer m_terrainFxPlayer;
    private FxPlayer m_cutscenePlayer;
    private GameObjectPool2<BattleActorUIController> m_battleActorUIControllerPool;
    private List<string> m_tempStringList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TestPathFind;
    private static DelegateBridge __Hotfix_TickSlowMotion;
    private static DelegateBridge __Hotfix_TickCombat;
    private static DelegateBridge __Hotfix_TickClientBattle;
    private static DelegateBridge __Hotfix_TickClientBattle_Play;
    private static DelegateBridge __Hotfix_TickClientBattle_PreStartCombat;
    private static DelegateBridge __Hotfix_TickClientBattle_Stop;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_StopCombat;
    private static DelegateBridge __Hotfix_SetTimeScale;
    private static DelegateBridge __Hotfix_SetGlobalTimeScale;
    private static DelegateBridge __Hotfix_UpdateFinalTimeScale;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_ClearBattle;
    private static DelegateBridge __Hotfix_ClearBattleInfos;
    private static DelegateBridge __Hotfix_CreateMap;
    private static DelegateBridge __Hotfix_CreateArenaMap;
    private static DelegateBridge __Hotfix_CreatePVPMap;
    private static DelegateBridge __Hotfix_CreateRealTimePVPMap;
    private static DelegateBridge __Hotfix_CreatePeakArenaMap;
    private static DelegateBridge __Hotfix__CreateMap;
    private static DelegateBridge __Hotfix_ResetMap;
    private static DelegateBridge __Hotfix_SetIsBattleReport;
    private static DelegateBridge __Hotfix_IsBattleReport;
    private static DelegateBridge __Hotfix_SetIsBattleLive;
    private static DelegateBridge __Hotfix_IsBattleLive;
    private static DelegateBridge __Hotfix_StartBattlePlay;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_StartArena;
    private static DelegateBridge __Hotfix_StartPVP;
    private static DelegateBridge __Hotfix_StartRealTimePVP;
    private static DelegateBridge __Hotfix_StartPeakArena;
    private static DelegateBridge __Hotfix_IsTeamBattle;
    private static DelegateBridge __Hotfix_GetPeakArenaBoRound;
    private static DelegateBridge __Hotfix_SetEnforceActionOrderHeros;
    private static DelegateBridge __Hotfix_FirstStep;
    private static DelegateBridge __Hotfix_Stop;
    private static DelegateBridge __Hotfix_GetWinConditionTargetPosition;
    private static DelegateBridge __Hotfix_GetLoseConditionTargetPosition;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_SetAutoBattle;
    private static DelegateBridge __Hotfix_SetFastBattle;
    private static DelegateBridge __Hotfix_StartBattleDialog;
    private static DelegateBridge __Hotfix_StopBattleDialog;
    private static DelegateBridge __Hotfix_GetBattleDialogResult;
    private static DelegateBridge __Hotfix_IsWaitBattleDialog;
    private static DelegateBridge __Hotfix_StartBattleTreasureDialog;
    private static DelegateBridge __Hotfix_StopBattleTreasureDialog;
    private static DelegateBridge __Hotfix_IsWaitBattleTreasureDialog;
    private static DelegateBridge __Hotfix_StartBattleTreasureReward;
    private static DelegateBridge __Hotfix_StopBattleTreasureReward;
    private static DelegateBridge __Hotfix_IsWaitBattleTreasureReward;
    private static DelegateBridge __Hotfix_StartFastCombat;
    private static DelegateBridge __Hotfix_StopFastCombat;
    private static DelegateBridge __Hotfix_IsWaitFastCombat;
    private static DelegateBridge __Hotfix_StartBattlePerform;
    private static DelegateBridge __Hotfix_StopBattlePerform;
    private static DelegateBridge __Hotfix_IsBattlePerforming;
    private static DelegateBridge __Hotfix_FadeNonSkillTargets;
    private static DelegateBridge __Hotfix_RestoreNonSkillTargets;
    private static DelegateBridge __Hotfix_GetActors;
    private static DelegateBridge __Hotfix_GetActor;
    private static DelegateBridge __Hotfix_FindActorByBattleActorId;
    private static DelegateBridge __Hotfix_GetDisabledBuffStateIdList;
    private static DelegateBridge __Hotfix_IsNeedTargetIcon;
    private static DelegateBridge __Hotfix_CameraFocusActor;
    private static DelegateBridge __Hotfix_CameraFocusPosition;
    private static DelegateBridge __Hotfix_CameraFollowActor;
    private static DelegateBridge __Hotfix_IsCameraFocusing;
    private static DelegateBridge __Hotfix_ComputeTotalHealthPoint;
    private static DelegateBridge __Hotfix_ComputeTotalHealthPointMax;
    private static DelegateBridge __Hotfix_EndAllAction;
    private static DelegateBridge __Hotfix_IgnoreMoveStep;
    private static DelegateBridge __Hotfix_IgnoreSkillStep;
    private static DelegateBridge __Hotfix_IgnoreTeleportDisappearStep;
    private static DelegateBridge __Hotfix_GridPositionToWorldPosition;
    private static DelegateBridge __Hotfix_WorldPositionToGridPosition;
    private static DelegateBridge __Hotfix_ScreenPositionToWorldPosition;
    private static DelegateBridge __Hotfix_ScreenPositionToGridPosition;
    private static DelegateBridge __Hotfix_GridPositionToScreenPosition;
    private static DelegateBridge __Hotfix_DrawLine_1;
    private static DelegateBridge __Hotfix_DrawMap;
    private static DelegateBridge __Hotfix_DrawCell;
    private static DelegateBridge __Hotfix_DrawGrid;
    private static DelegateBridge __Hotfix_CreateBattleGraphic;
    private static DelegateBridge __Hotfix_DestroyBattleGraphic;
    private static DelegateBridge __Hotfix_GetCurrentCamera;
    private static DelegateBridge __Hotfix_BattleActorTryMove;
    private static DelegateBridge __Hotfix_CreateActor;
    private static DelegateBridge __Hotfix_CreateTreasure;
    private static DelegateBridge __Hotfix_GetTreasure;
    private static DelegateBridge __Hotfix_GetNextEntityId;
    private static DelegateBridge __Hotfix_PlayScreenEffect;
    private static DelegateBridge __Hotfix_CreateMapBackground;
    private static DelegateBridge __Hotfix_ClearMapBackground;
    private static DelegateBridge __Hotfix_CreateMapTerrainFx;
    private static DelegateBridge __Hotfix_ClearMapTerrainFx;
    private static DelegateBridge __Hotfix_FastForwardMapTerrainFx;
    private static DelegateBridge __Hotfix_AddMapTerrainFx;
    private static DelegateBridge __Hotfix_AddMapTerrainEffectFx;
    private static DelegateBridge __Hotfix_ChangeMapTerrainFx;
    private static DelegateBridge __Hotfix_ChangeMapTerrainEffectFx;
    private static DelegateBridge __Hotfix_CreateBattleActorUIController;
    private static DelegateBridge __Hotfix_DestroyBattleActorUIController;
    private static DelegateBridge __Hotfix_RebuildBattle_0;
    private static DelegateBridge __Hotfix_RebuildBattle_1;
    private static DelegateBridge __Hotfix_IsRebuildingBattle;
    private static DelegateBridge __Hotfix_StopRebuildingBattle;
    private static DelegateBridge __Hotfix_SetSkipCombatMode;
    private static DelegateBridge __Hotfix_IsSkipCombatMode;
    private static DelegateBridge __Hotfix_IsSkippingCombat;
    private static DelegateBridge __Hotfix_GetStopCombatMonsterTotalDamage;
    private static DelegateBridge __Hotfix_ComputeCombatArmyRelationValue;
    private static DelegateBridge __Hotfix__ComputeCombatArmyRelationValue;
    private static DelegateBridge __Hotfix__ComputeArmyRelationValue;
    private static DelegateBridge __Hotfix_IsCombatMagicAttack;
    private static DelegateBridge __Hotfix_EnableSdkLogBattle;
    private static DelegateBridge __Hotfix_GetMyPlayerIndex;
    private static DelegateBridge __Hotfix_GetMyPlayerTeamNumber;
    private static DelegateBridge __Hotfix_ClearGraphicPools;
    private static DelegateBridge __Hotfix_FrameToMillisecond;
    private static DelegateBridge __Hotfix_MillisecondToFrame;
    private static DelegateBridge __Hotfix_MillisecondToFrame1;
    private static DelegateBridge __Hotfix_get_Battle;
    private static DelegateBridge __Hotfix_get_BattleCamera;
    private static DelegateBridge __Hotfix_get_CombatCamera;
    private static DelegateBridge __Hotfix_get_State;
    private static DelegateBridge __Hotfix_set_EnableDebugDraw;
    private static DelegateBridge __Hotfix_get_EnableDebugDraw;
    private static DelegateBridge __Hotfix_get_IsAutoBattle;
    private static DelegateBridge __Hotfix_get_IsFastBattle;
    private static DelegateBridge __Hotfix_get_SkipCombatMode;
    private static DelegateBridge __Hotfix_get_IsPaused;
    private static DelegateBridge __Hotfix_get_BattleGraphicRoot;
    private static DelegateBridge __Hotfix_get_CombatGraphicRoot;
    private static DelegateBridge __Hotfix_get_MapTreasureRoot;
    private static DelegateBridge __Hotfix_get_BattleActorUIRoot;
    private static DelegateBridge __Hotfix_get_BattleFxPlayer;
    private static DelegateBridge __Hotfix_get_CombatFxPlayer;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_ClientBattleListener;
    private static DelegateBridge __Hotfix_get_RandomNumber;
    private static DelegateBridge __Hotfix_AppendActToActor;
    private static DelegateBridge __Hotfix_AppendActsToActor_0;
    private static DelegateBridge __Hotfix_AppendActsToActor_1;
    private static DelegateBridge __Hotfix_IsAnyActorHasAct;
    private static DelegateBridge __Hotfix_HasWaitingAct_1;
    private static DelegateBridge __Hotfix_HasWaitingAct_0;
    private static DelegateBridge __Hotfix_OnActorActive;
    private static DelegateBridge __Hotfix_OnActorPreStartCombat;
    private static DelegateBridge __Hotfix_OnActorStopCombatEnd;
    private static DelegateBridge __Hotfix_OnActorCastSkill;
    private static DelegateBridge __Hotfix_OnActorCastSkillEnd;
    private static DelegateBridge __Hotfix_OnBattleStart;
    private static DelegateBridge __Hotfix_OnBattleNextTurn;
    private static DelegateBridge __Hotfix_OnBattleNextTeam;
    private static DelegateBridge __Hotfix_OnBattleNextPlayer;
    private static DelegateBridge __Hotfix_OnBattleNextActor;
    private static DelegateBridge __Hotfix_OnBattleActorCreate;
    private static DelegateBridge __Hotfix_OnBattleActorCreateEnd;
    private static DelegateBridge __Hotfix_OnBattleActorActive;
    private static DelegateBridge __Hotfix_OnBattleActorActionBegin;
    private static DelegateBridge __Hotfix_OnBattleActorActionEnd;
    private static DelegateBridge __Hotfix_OnBattleActorMove;
    private static DelegateBridge __Hotfix_OnBattleActorMoveEnd;
    private static DelegateBridge __Hotfix_OnBattleActorPerformMove;
    private static DelegateBridge __Hotfix_OnBattleActorPunchMove;
    private static DelegateBridge __Hotfix_OnBattleActorExchangeMove;
    private static DelegateBridge __Hotfix_OnBattleActorSetDir;
    private static DelegateBridge __Hotfix_OnBattleActorPlayFx;
    private static DelegateBridge __Hotfix_OnBattleActorPlayAnimation;
    private static DelegateBridge __Hotfix_OnBattleActorChangeIdleAnimation;
    private static DelegateBridge __Hotfix_OnBattleActorSkill;
    private static DelegateBridge __Hotfix_OnBattleActorSkillHitBegin;
    private static DelegateBridge __Hotfix_OnBattleActorSkillHit;
    private static DelegateBridge __Hotfix_OnBattleActorSkillHitEnd;
    private static DelegateBridge __Hotfix_OnBattleActorAttachBuff;
    private static DelegateBridge __Hotfix_OnBattleActorDetachBuff;
    private static DelegateBridge __Hotfix_OnBattleActorImmune;
    private static DelegateBridge __Hotfix_OnBattleActorModifyHp;
    private static DelegateBridge __Hotfix_OnBattleActorPassiveSkill;
    private static DelegateBridge __Hotfix_OnBattleActorBuffHit;
    private static DelegateBridge __Hotfix_OnBattleActorTerrainHit;
    private static DelegateBridge __Hotfix_OnBattleActorTeleport;
    private static DelegateBridge __Hotfix_OnBattleActorTeleportDisappear;
    private static DelegateBridge __Hotfix_OnBattleActorTeleportAppear;
    private static DelegateBridge __Hotfix_OnBattleActorSummon;
    private static DelegateBridge __Hotfix_OnBattleActorDie;
    private static DelegateBridge __Hotfix_OnBattleActorAppear;
    private static DelegateBridge __Hotfix_OnBattleActorDisappear;
    private static DelegateBridge __Hotfix_OnBattleActorChangeTeam;
    private static DelegateBridge __Hotfix_OnBattleActorChangeArmy;
    private static DelegateBridge __Hotfix_OnBattleActorReplace;
    private static DelegateBridge __Hotfix_OnBattleActorCameraFocus;
    private static DelegateBridge __Hotfix_OnBattleActorGainBattleTreasure;
    private static DelegateBridge __Hotfix_OnStartGuard;
    private static DelegateBridge __Hotfix_OnStopGuard;
    private static DelegateBridge __Hotfix_OnBeforeStartCombat;
    private static DelegateBridge __Hotfix_OnCancelCombat;
    private static DelegateBridge __Hotfix_OnStartCombat;
    private static DelegateBridge __Hotfix_OnPreStopCombat;
    private static DelegateBridge __Hotfix_OnStopCombat;
    private static DelegateBridge __Hotfix_OnCombatActorHit;
    private static DelegateBridge __Hotfix_OnCombatActorDie;
    private static DelegateBridge __Hotfix_OnStartSkillCutscene;
    private static DelegateBridge __Hotfix_OnStartPassiveSkillCutscene;
    private static DelegateBridge __Hotfix_OnStopSkillCutscene;
    private static DelegateBridge __Hotfix_OnStartBattleDialog;
    private static DelegateBridge __Hotfix_OnStartBattlePerform;
    private static DelegateBridge __Hotfix_OnStopBattlePerform;
    private static DelegateBridge __Hotfix_OnChangeMapTerrain;
    private static DelegateBridge __Hotfix_OnChangeMapTerrainEffect;
    private static DelegateBridge __Hotfix_OnCameraFocus;
    private static DelegateBridge __Hotfix_OnPlayMusic;
    private static DelegateBridge __Hotfix_OnPlaySound;
    private static DelegateBridge __Hotfix_OnPlayFx;
    private static DelegateBridge __Hotfix_OnWaitTime;
    private static DelegateBridge __Hotfix_OnBossPhase;
    private static DelegateBridge __Hotfix_OnBattleTreasureCreate;
    private static DelegateBridge __Hotfix_CreateCombatGraphic;
    private static DelegateBridge __Hotfix_DestroyCombatGraphic;
    private static DelegateBridge __Hotfix_PlayCombatFx;
    private static DelegateBridge __Hotfix_PlaySound_1;
    private static DelegateBridge __Hotfix_PlaySound_0;
    private static DelegateBridge __Hotfix_DrawLine_0;
    private static DelegateBridge __Hotfix_DrawLine_2;
    private static DelegateBridge __Hotfix_IsCombatGraphicMirrorX;
    private static DelegateBridge __Hotfix_OnAudio;
    private static DelegateBridge __Hotfix_OnSound;
    private static DelegateBridge __Hotfix_OnCameraEffect;
    private static DelegateBridge __Hotfix_OnScreenEffect;
    private static DelegateBridge __Hotfix_OnGeneral;
    private static DelegateBridge __Hotfix_GetCameraPosition;
    private static DelegateBridge __Hotfix_CombatPositionToWorldPosition;
    private static DelegateBridge __Hotfix_IsCulled;
    private static DelegateBridge __Hotfix_LogBattleStart;
    private static DelegateBridge __Hotfix_LogBattleStop;
    private static DelegateBridge __Hotfix_LogBattleTeam;
    private static DelegateBridge __Hotfix_LogActorMove;
    private static DelegateBridge __Hotfix_LogActorStandby;
    private static DelegateBridge __Hotfix_LogActorAttack;
    private static DelegateBridge __Hotfix_LogActorSkill;
    private static DelegateBridge __Hotfix_LogActorDie;
    private static DelegateBridge __Hotfix_SdkLogBattle;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(IClientBattleListener clientBattleListener, GameObject root)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TestPathFind()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickSlowMotion(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle_Play()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle_PreStartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientBattle_Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGlobalTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFinalTimeScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBattleInfos()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateMap(
      ConfigDataBattleInfo battleInfo,
      BattleType battleType,
      int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateArenaMap(ConfigDataArenaBattleInfo battleInfo, int myPlayerTeam)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreatePVPMap(ConfigDataPVPBattleInfo battleInfo, int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateRealTimePVPMap(ConfigDataRealTimePVPBattleInfo battleInfo, int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreatePeakArenaMap(ConfigDataPeakArenaBattleInfo battleInfo, int myPlayerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void _CreateMap(ConfigDataBattlefieldInfo battlefieldInfo, int cameraX, int cameraY)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ResetMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsBattleReport(bool isBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIsBattleLive(bool isBattleLive)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattleLive()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StartBattlePlay()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int armyRandomSeed,
      int monsterLevel,
      int starTurnMax,
      int starDeadMax,
      List<ConfigDataBattleAchievementRelatedInfo> achievements,
      List<int> gainBattleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartArena(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      ConfigDataArenaDefendRuleInfo arenaDefendRuleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartPVP(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartRealTimePVP(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartPeakArena(
      BattleTeamSetup teamSetup0,
      BattleTeamSetup teamSetup1,
      BattlePlayer[] players,
      int randomSeed,
      int boRound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeamBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPeakArenaBoRound()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEnforceActionOrderHeros(List<int> heroIds)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FirstStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop(bool win, bool abnormal)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetWinConditionTargetPosition(
      ConfigDataBattleWinConditionInfo winConditionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private GridPosition GetLoseConditionTargetPosition(
      ConfigDataBattleLoseConditionInfo loseConditionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAutoBattle(bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFastBattle(bool fastBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattleDialog(int result)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetBattleDialogResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitBattleDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattleTreasureDialog(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattleTreasureDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitBattleTreasureDialog()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattleTreasureReward(ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattleTreasureReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitBattleTreasureReward()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFastCombat(FastCombatActorInfo a, FastCombatActorInfo b)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopFastCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsWaitFastCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattlePerforming()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeNonSkillTargets(
      ClientBattleActor ca,
      ConfigDataSkillInfo skillInfo,
      GridPosition targetPos,
      List<BattleActor> targetActors,
      List<BattleActor> combineActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void RestoreNonSkillTargets()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ClientBattleActor> GetActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor GetActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor FindActorByBattleActorId(int battleActorId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<int> GetDisabledBuffStateIdList(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNeedTargetIcon(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CameraFocusActor(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CameraFocusPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CameraFollowActor(ClientBattleActor ca)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCameraFocusing()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeTotalHealthPoint(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeTotalHealthPointMax(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EndAllAction(int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreMoveStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreSkillStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void IgnoreTeleportDisappearStep(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GridPositionToWorldPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition WorldPositionToGridPosition(Vector2 sp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 ScreenPositionToWorldPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GridPosition ScreenPositionToGridPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GridPositionToScreenPosition(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawLine(Vector3 p0, Vector3 p1, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawMap()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawCell(GridPosition p, BattleActor actor, ConfigDataTerrainInfo terrain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawGrid(GridPosition p, float scale, Color color, bool cross = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateBattleGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyBattleGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CameraBase GetCurrentCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BattleActorTryMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor CreateActor(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientBattleTreasure CreateTreasure(
      ConfigDataBattleTreasureInfo treasureInfo,
      bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleTreasure GetTreasure(
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapBackground(ConfigDataBattlefieldInfo battlefieldInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateMapTerrainFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearMapTerrainFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FastForwardMapTerrainFx(float t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMapTerrainFx(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMapTerrainEffectFx(
      GridPosition p,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrainFx(GridPosition p, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeMapTerrainEffectFx(
      GridPosition p,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleActorUIController CreateBattleActorUIController()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyBattleActorUIController(BattleActorUIController ctrl)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RebuildBattle(LocalProcessingBattleData data)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RebuildBattle(List<BattleCommand> commands, int fromStep = 0, int toStep = 2147483647)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRebuildingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopRebuildingBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSkipCombatMode(SkipCombatMode mode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkipCombatMode()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSkippingCombat(bool checkState = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetStopCombatMonsterTotalDamage()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ComputeCombatArmyRelationValue(BattleActor a, BattleActor b, bool isMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int _ComputeCombatArmyRelationValue(BattleActor a, BattleActor b, bool isMagic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static void _ComputeArmyRelationValue(ArmyRelationData r, bool isMagic, ref int value)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCombatMagicAttack(
      BattleActor attacker,
      BattleActor target,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableSdkLogBattle(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMyPlayerIndex()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMyPlayerTeamNumber()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicPools()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int FrameToMillisecond(int frame)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public BattleBase Battle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleCamera BattleCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public CombatCamera CombatCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ClientBattleState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool EnableDebugDraw
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsFastBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public SkipCombatMode SkipCombatMode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsPaused
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject BattleGraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject CombatGraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject MapTreasureRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject BattleActorUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer BattleFxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer CombatFxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IClientBattleListener ClientBattleListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public RandomNumber RandomNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendActToActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendActsToActor(int step, System.Type type, System.Type type2 = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AppendActsToActor(ClientBattleActor ca, int step, System.Type type, System.Type type2 = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyActorHasAct()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasWaitingAct(BattleActor a, int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasWaitingAct(int step)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorActive(ClientBattleActor a, bool newStep, int step, int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorPreStartCombat(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorStopCombatEnd(ClientBattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkill(
      ClientBattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition targetPos,
      List<BattleActor> targetActors,
      List<BattleActor> combineActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnActorCastSkillEnd(ClientBattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextTeam(int team, bool isNpc)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextPlayer(int prevPlayerIndex, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleNextActor(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCreate(BattleActor a, bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCreateEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActive(BattleActor a, bool newStep)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActionBegin(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorActionEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorMove(BattleActor a, GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorMoveEnd(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPerformMove(
      BattleActor a,
      GridPosition p,
      int dir,
      bool cameraFollow)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPunchMove(BattleActor a, string fxName, bool isDragExchange)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorExchangeMove(
      BattleActor a,
      BattleActor b,
      int moveType,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSetDir(BattleActor a, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPlayFx(BattleActor a, string fxName, int attachMode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPlayAnimation(BattleActor a, string animationName, int animationTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeIdleAnimation(BattleActor a, string idleAnimationName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkill(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      GridPosition p,
      List<BattleActor> targets,
      List<BattleActor> combineActors)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkillHitBegin(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkillHit(
      BattleActor a,
      ConfigDataSkillInfo skill,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSkillHitEnd(
      BattleActor a,
      ConfigDataSkillInfo skillInfo,
      bool isRebound)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorAttachBuff(BattleActor a, BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDetachBuff(BattleActor a, BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorImmune(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorModifyHp(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorPassiveSkill(
      BattleActor a,
      BattleActor target,
      BuffState sourceBuffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorBuffHit(
      BattleActor a,
      BuffState buffState,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTerrainHit(
      BattleActor a,
      int heroHpModify,
      int soldierHpModify,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleport(BattleActor a, ConfigDataSkillInfo skill, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleportDisappear(
      BattleActor a,
      ConfigDataSkillInfo skill,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorTeleportAppear(
      BattleActor a,
      ConfigDataSkillInfo skill,
      GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorSummon(BattleActor a, ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDie(BattleActor a, bool isAfterCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorAppear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorDisappear(BattleActor a, int effectType, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeTeam(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorChangeArmy(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorReplace(BattleActor a0, BattleActor a1, string fxName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorCameraFocus(BattleActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleActorGainBattleTreasure(
      BattleActor a,
      ConfigDataBattleTreasureInfo treasureInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopGuard(BattleActor a, BattleActor target)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBeforeStartCombat(
      BattleActor a,
      BattleActor b,
      ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCancelCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartCombat(BattleActor a, BattleActor b, ConfigDataSkillInfo attackerSkillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPreStopCombat()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopCombat(
      int teamAHeroTotalDamage,
      int teamASoldierTotalDamage,
      bool teamACriticalAttack,
      int teamBHeroTotalDamage,
      int teamBSoldierTotalDamage,
      bool teamBCriticalAttack)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCombatActorHit(
      CombatActor a,
      CombatActor attacker,
      ConfigDataSkillInfo skillInfo,
      int hpModify,
      int totalDamage,
      DamageNumberType damageNumberType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCombatActorDie(CombatActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartSkillCutscene(
      ConfigDataSkillInfo skillInfo,
      ConfigDataCutsceneInfo cutsceneInfo,
      ConfigDataCutsceneInfo cutsceneInfo2,
      int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartPassiveSkillCutscene(BuffState sourceBuffState, int team)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopSkillCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattleDialog(ConfigDataBattleDialogInfo dialogInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStartBattlePerform(ConfigDataBattlePerformInfo performInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnStopBattlePerform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnChangeMapTerrain(List<GridPosition> positions, ConfigDataTerrainInfo terrainInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnChangeMapTerrainEffect(
      List<GridPosition> positions,
      ConfigDataTerrainEffectInfo terrainEffectInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraFocus(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayMusic(string musicName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlaySound(string soundName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayFx(string fxName, GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaitTime(int timeInMs)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBossPhase(int phase)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnBattleTreasureCreate(ConfigDataBattleTreasureInfo treasureInfo, bool isOpened)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic CreateCombatGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyCombatGraphic(IBattleGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public IBattleGraphic PlayCombatFx(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawLine(Vector2i p0, Vector2i p1, Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DrawLine(Vector2i p0, Fix64 z0, Vector2i p1, Fix64 z1, Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCombatGraphicMirrorX()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAudio(FxEvent e, AudioClip a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSound(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGeneral(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 CombatPositionToWorldPosition(Vector2i p, Fix64 z, bool computeZOffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogBattleStart()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogBattleStop(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogBattleTeam(BattleTeam team0, BattleTeam team1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorMove(BattleActor actor, GridPosition fromPos, GridPosition toPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorStandby(BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorAttack(BattleActor actor, BattleActor targetActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorSkill(
      BattleActor actor,
      ConfigDataSkillInfo skillInfo,
      BattleActor targetActor,
      GridPosition targetPos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LogActorDie(BattleActor actor, BattleActor killerActor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SdkLogBattle(string eventId, object logData)
    {
      // ISSUE: unable to decompile the method.
    }

    public class LogDataBattleStart
    {
      public string Type;
      public int BattleId;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public LogDataBattleStart()
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class LogDataArenaBattleStart
    {
      public string Type = "ArenaBattleStart";
      public int ArenaBattleId;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataArenaBattleStart()
      {
        ClientBattle.LogDataArenaBattleStart._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataPVPBattleStart
    {
      public string Type = "PVPBattleStart";
      public int PVPBattleId;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataPVPBattleStart()
      {
        ClientBattle.LogDataPVPBattleStart._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataRealTimePVPBattleStart
    {
      public string Type = "RealTimePVPBattleStart";
      public int RealTimePVPBattleId;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataRealTimePVPBattleStart()
      {
        ClientBattle.LogDataRealTimePVPBattleStart._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataPeakArenaBattleStart
    {
      public string Type = "PeakArenaBattleStart";
      public int PeakArenaBattleId;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataPeakArenaBattleStart()
      {
        ClientBattle.LogDataPeakArenaBattleStart._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataBattleStop
    {
      public string Type = "BattleStop";
      public bool IsWin;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataBattleStop()
      {
        ClientBattle.LogDataBattleStop._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataBattleTeam
    {
      public string Type = "Team";
      public List<int> HeroId = new List<int>();
      public List<GridPosition> Pos = new List<GridPosition>();
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataBattleTeam()
      {
        ClientBattle.LogDataBattleTeam._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataActorMove
    {
      public string Type = "Move";
      public int Turn;
      public int HeroId;
      public GridPosition FromPos;
      public GridPosition ToPos;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataActorMove()
      {
        ClientBattle.LogDataActorMove._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataActorStandby
    {
      public string Type = "Standby";
      public int Turn;
      public int HeroId;
      public GridPosition Pos;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataActorStandby()
      {
        ClientBattle.LogDataActorStandby._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataActorAttack
    {
      public string Type = "Attack";
      public int Turn;
      public int HeroId;
      public int TargetHeroId;
      public GridPosition TargetPos;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataActorAttack()
      {
        ClientBattle.LogDataActorAttack._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataActorSkill
    {
      public string Type = "Skill";
      public int Turn;
      public int HeroId;
      public int TargetHeroId;
      public GridPosition TargetPos;
      public int SkillId;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataActorSkill()
      {
        ClientBattle.LogDataActorSkill._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class LogDataActorDie
    {
      public string Type = "Die";
      public int Turn;
      public int HeroId;
      public int KillerHeroId;
      private static DelegateBridge _c__Hotfix_ctor;

      public LogDataActorDie()
      {
        ClientBattle.LogDataActorDie._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }
  }
}
