﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.BattleCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.Scene
{
  public class BattleCamera : CameraBase
  {
    private Vector2 m_position;
    private Vector2 m_smoothMoveStartPosition;
    private Vector2 m_smoothMoveEndPosition;
    private Vector2 m_touchMoveVelocity;
    private bool m_enableTouchMove;
    private bool m_enableTouchMove2;
    private float m_smoothMoveTime;
    private float m_smoothMoveTotalTime;
    private EventSystem m_eventSystem;
    private PointerEventData m_pointData;
    private List<RaycastResult> m_raycastResults;
    private TouchStatus[] m_touchStatus;
    private Vector3 m_initPosition;
    private Vector2 m_mapSizeHalf;
    private float m_viewSizeMin;
    private float m_viewSizeMax;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_PrepareBattle;
    private static DelegateBridge __Hotfix_StartBattle;
    private static DelegateBridge __Hotfix_SetOrthographicSize;
    private static DelegateBridge __Hotfix_EnableTouchMove;
    private static DelegateBridge __Hotfix_EnableTouchMove2;
    private static DelegateBridge __Hotfix_UpdateEnableTouchMove;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_ScreenPositionToViewPosition;
    private static DelegateBridge __Hotfix_IsTouchUI;
    private static DelegateBridge __Hotfix_UpdateTouch;
    private static DelegateBridge __Hotfix_ClampCameraPosition;
    private static DelegateBridge __Hotfix_ComputeBoundPosition;
    private static DelegateBridge __Hotfix_Look;
    private static DelegateBridge __Hotfix_SmoothLook;
    private static DelegateBridge __Hotfix_IsSmoothMoving;
    private static DelegateBridge __Hotfix_Bound;
    private static DelegateBridge __Hotfix_GetPosition;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PrepareBattle(ConfigDataBattlefieldInfo battlefieldInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOrthographicSize(float size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTouchMove(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTouchMove2(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEnableTouchMove()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Vector2 ScreenPositionToViewPosition(
      Vector2 p,
      float viewWidth,
      float viewHeight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTouchUI(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTouch(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ClampCameraPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeBoundPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Look(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SmoothLook(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSmoothMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Bound(Vector2 p, float dt, int speed = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
