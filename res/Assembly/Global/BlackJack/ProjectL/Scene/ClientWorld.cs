﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.PlayerContext;
using BlackJack.ProjectL.UI;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class ClientWorld : IFxEventListener
  {
    private ClientWorldState m_state;
    private int m_entityIdCounter;
    private int m_frameCount;
    private float m_worldTickTime;
    private float m_timeScale;
    private float m_globalTimeScale;
    private bool m_isPaused;
    private bool m_enableDebugDraw;
    private RandomNumber m_randomNumber;
    private ProjectLPlayerContext m_playerContext;
    private List<ClientWorldPlayerActor> m_playerActors;
    private List<ClientWorldEventActor> m_eventActors;
    private List<ClientWorldWaypoint> m_waypoints;
    private List<ClientWorldRegion> m_regions;
    private IConfigDataLoader m_configDataLoader;
    private IClientWorldListener m_clientWorldListener;
    private WorldCamera m_worldCamera;
    private WorldPathfinder m_pathfinder;
    private GameObject m_worldRoot;
    private GameObject m_graphicRoot;
    private GameObject m_mapRoot;
    private GameObject m_background;
    private GameObject m_backgroundWaypointsRoot;
    private GameObject m_backgroundRegionsRoot;
    private GameObject m_worldUIRoot;
    private GameObject m_waypointUIRoot;
    private GameObject m_eventUIRoot;
    private GameObject m_playerUIRoot;
    private GameObject m_waypointUIPrefab;
    private GameObject m_waypoint2UIPrefab;
    private GameObject m_eventUIPrefab;
    private GameObject m_playerUIPrefab;
    private WorldScenarioUIController m_worldScenarioUIController;
    private GraphicPool m_graphicPool;
    private GraphicPool m_fxPool;
    private FxPlayer m_fxPlayer;
    private Quaternion m_faceCameraRotation;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_OnWaypointUpdate;
    private static DelegateBridge __Hotfix_OnPlayerIconUpdate;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickClientWorld;
    private static DelegateBridge __Hotfix_SetTimeScale;
    private static DelegateBridge __Hotfix_SetGlobalTimeScale;
    private static DelegateBridge __Hotfix_UpdateFinalTimeScale;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_Start;
    private static DelegateBridge __Hotfix_Stop;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_ShowWorld;
    private static DelegateBridge __Hotfix_CreateRegion;
    private static DelegateBridge __Hotfix_CreateWaypoint;
    private static DelegateBridge __Hotfix_CreatePlayerActor;
    private static DelegateBridge __Hotfix_CreateEventActor;
    private static DelegateBridge __Hotfix_UpdateRegionState;
    private static DelegateBridge __Hotfix_UpdateWaypointState;
    private static DelegateBridge __Hotfix_UpdatePlayerLocation;
    private static DelegateBridge __Hotfix_UpdateEventActorState;
    private static DelegateBridge __Hotfix_UpdateActiveScenario;
    private static DelegateBridge __Hotfix_GetRegion;
    private static DelegateBridge __Hotfix_GetWaypoint;
    private static DelegateBridge __Hotfix_GetEventActorAt;
    private static DelegateBridge __Hotfix_GetEventActoryByEventId;
    private static DelegateBridge __Hotfix_GetEventActors;
    private static DelegateBridge __Hotfix_GetPlayerActor;
    private static DelegateBridge __Hotfix_OnWaypointClick;
    private static DelegateBridge __Hotfix_OnEventActorClick;
    private static DelegateBridge __Hotfix_CreateGraphic;
    private static DelegateBridge __Hotfix_DestroyGraphic;
    private static DelegateBridge __Hotfix_GetCamera;
    private static DelegateBridge __Hotfix_GetCameraPosition;
    private static DelegateBridge __Hotfix_ComputeActorPosition;
    private static DelegateBridge __Hotfix_IsCulled_0;
    private static DelegateBridge __Hotfix_IsCulled_1;
    private static DelegateBridge __Hotfix_GetNextEntityId;
    private static DelegateBridge __Hotfix_PlayScreenEffect;
    private static DelegateBridge __Hotfix_CreateBackground;
    private static DelegateBridge __Hotfix_ClearBackground;
    private static DelegateBridge __Hotfix_ShowBackgroundChild;
    private static DelegateBridge __Hotfix_PlaySound_1;
    private static DelegateBridge __Hotfix_PlaySound_0;
    private static DelegateBridge __Hotfix_FindPath;
    private static DelegateBridge __Hotfix_FrameToMillisecond;
    private static DelegateBridge __Hotfix_MillisecondToFrame;
    private static DelegateBridge __Hotfix_MillisecondToFrame1;
    private static DelegateBridge __Hotfix_get_WorldCamera;
    private static DelegateBridge __Hotfix_get_State;
    private static DelegateBridge __Hotfix_set_EnableDebugDraw;
    private static DelegateBridge __Hotfix_get_EnableDebugDraw;
    private static DelegateBridge __Hotfix_get_GraphicRoot;
    private static DelegateBridge __Hotfix_get_WorldUIRoot;
    private static DelegateBridge __Hotfix_get_WaypointUIRoot;
    private static DelegateBridge __Hotfix_get_EventUIRoot;
    private static DelegateBridge __Hotfix_get_PlayerUIRoot;
    private static DelegateBridge __Hotfix_get_WaypointUIPrefab;
    private static DelegateBridge __Hotfix_get_Waypoint2UIPrefab;
    private static DelegateBridge __Hotfix_get_EventUIPrefab;
    private static DelegateBridge __Hotfix_get_PlayerUIPrefab;
    private static DelegateBridge __Hotfix_get_FaceCameraRotation;
    private static DelegateBridge __Hotfix_get_FxPlayer;
    private static DelegateBridge __Hotfix_get_ConfigDataLoader;
    private static DelegateBridge __Hotfix_get_ClientWorldListener;
    private static DelegateBridge __Hotfix_OnAudio;
    private static DelegateBridge __Hotfix_OnSound;
    private static DelegateBridge __Hotfix_OnCameraEffect;
    private static DelegateBridge __Hotfix_OnScreenEffect;
    private static DelegateBridge __Hotfix_OnGeneral;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(IClientWorldListener clientWorldListener, GameObject worldRoot)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnPlayerIconUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickClientWorld()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGlobalTimeScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateFinalTimeScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataWorldMapInfo mapInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Stop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowWorld(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldRegion CreateRegion(ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldWaypoint CreateWaypoint(
      ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldPlayerActor CreatePlayerActor(
      ConfigDataWaypointInfo waypointInfo,
      int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ClientWorldEventActor CreateEventActor(
      ConfigDataEventInfo eventInfo,
      ConfigDataWaypointInfo waypointInfo,
      int dir,
      RandomEvent randomEvent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateRegionState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateWaypointState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdatePlayerLocation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateEventActorState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateActiveScenario()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldRegion GetRegion(int regionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint GetWaypoint(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor GetEventActorAt(int waypointId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldEventActor GetEventActoryByEventId(int eventId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ClientWorldEventActor> GetEventActors()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldPlayerActor GetPlayerActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnWaypointClick(ClientWorldWaypoint wp)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnEventActorClick(ClientWorldEventActor a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic CreateGraphic(string assetName, float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DestroyGraphic(GenericGraphic graphic)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private CameraBase GetCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetCameraPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 ComputeActorPosition(Vector2 pos, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 p, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetNextEntityId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayScreenEffect(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateBackground(ConfigDataWorldMapInfo worldMapInfo, GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearBackground()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowBackgroundChild(string childName, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool FindPath(int start, int goal, List<int> path, bool checkWaypointStatus)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int FrameToMillisecond(int frame)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int MillisecondToFrame1(int ms)
    {
      // ISSUE: unable to decompile the method.
    }

    public WorldCamera WorldCamera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ClientWorldState State
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool EnableDebugDraw
    {
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject GraphicRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WorldUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIRoot
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject WaypointUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject Waypoint2UIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject EventUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public GameObject PlayerUIPrefab
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public Quaternion FaceCameraRotation
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public FxPlayer FxPlayer
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IConfigDataLoader ConfigDataLoader
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public IClientWorldListener ClientWorldListener
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnAudio(FxEvent e, AudioClip a)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnSound(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnCameraEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnScreenEffect(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void OnGeneral(FxEvent e, string name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
