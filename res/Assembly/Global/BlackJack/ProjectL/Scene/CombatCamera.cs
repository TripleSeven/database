﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CombatCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class CombatCamera : CameraBase
  {
    private bool m_isSplitScreen;
    private GameObject m_gameObject;
    private Camera m_camera1;
    private Camera m_camera2;
    private Vector3 m_initPosition;
    private float m_initOrthographicSize;
    private float m_initSplitScreenOrthographicSize;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_StartCombat;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_IsCulled_0;
    private static DelegateBridge __Hotfix_IsCulled_1;

    [MethodImpl((MethodImplOptions) 32768)]
    public CombatCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCombat(bool splitScreen)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsCulled(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override bool IsCulled(Vector2 bmin, Vector2 bmax)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
