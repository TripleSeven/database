﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldRegion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class ClientWorldRegion : Entity
  {
    private ConfigDataRegionInfo m_regionInfo;
    private GameObject m_gameObject;
    private Animator m_animator;
    private bool m_isOpened;
    private bool m_isVisible;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_SetGameObject;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_SetOpen;
    private static DelegateBridge __Hotfix_IsOpened;
    private static DelegateBridge __Hotfix_DoPause;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_SetVisible;
    private static DelegateBridge __Hotfix_IsVisible;
    private static DelegateBridge __Hotfix_get_Position;
    private static DelegateBridge __Hotfix_get_RegionInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldRegion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientWorld world, ConfigDataRegionInfo regionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGameObject(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetOpen(bool open)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsOpened()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2 Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataRegionInfo RegionInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
