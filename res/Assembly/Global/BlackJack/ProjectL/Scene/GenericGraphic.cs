﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.GenericGraphic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Art;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using FixMath.NET;
using IL;
using Spine;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class GenericGraphic : IBattleGraphic
  {
    private IGenericGraphicContainer m_container;
    private FxPlayer m_fxPlayer;
    private GameObject m_gameObject;
    private GameObject m_prefab;
    private Animator m_animator;
    private int m_animatorTriggerNameId;
    private string m_assetName;
    private Vector3 m_initScale;
    private Vector3 m_prefabScale;
    private int m_direction;
    private int m_layer;
    private int m_sortingOrder;
    private Colori m_color;
    private float m_intensity;
    private float m_saturation;
    private bool m_isPaused;
    private bool m_isVisible;
    private GenericGraphic.SpineInfo m_spineInfo;
    private GenericGraphic.FxInfo m_fxInfo;
    private GenericGraphic.EffectInfo m_effectInfo;
    private List<GenericGraphic> m_attachFxs;
    private MaterialPropertyBlock s_materialPropertyBlock;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Create;
    private static DelegateBridge __Hotfix_HandleSpineAnimationEvent;
    private static DelegateBridge __Hotfix_HandleSpineAnimationStart;
    private static DelegateBridge __Hotfix_HandleSpineAnimationEnd;
    private static DelegateBridge __Hotfix_Destroy;
    private static DelegateBridge __Hotfix_SetActive;
    private static DelegateBridge __Hotfix_GetAnimationInfo;
    private static DelegateBridge __Hotfix_SetParent;
    private static DelegateBridge __Hotfix_SetLayer;
    private static DelegateBridge __Hotfix_SetSortingOrder;
    private static DelegateBridge __Hotfix_GetPrefab;
    private static DelegateBridge __Hotfix_SetContainer;
    private static DelegateBridge __Hotfix_SetFxPlayer;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_PlayDeathAnimation;
    private static DelegateBridge __Hotfix_GetAnimationDuration;
    private static DelegateBridge __Hotfix_IsPlayAnimation;
    private static DelegateBridge __Hotfix_PlayFx;
    private static DelegateBridge __Hotfix_SetReplaceAnimations;
    private static DelegateBridge __Hotfix_SetHeight;
    private static DelegateBridge __Hotfix_SetCombatPosition;
    private static DelegateBridge __Hotfix_SetCombatDirection_1;
    private static DelegateBridge __Hotfix_SetCombatDirection_0;
    private static DelegateBridge __Hotfix_SetDirection;
    private static DelegateBridge __Hotfix_SetRotationZ;
    private static DelegateBridge __Hotfix_SetRotation;
    private static DelegateBridge __Hotfix_SetPrefabScale_0;
    private static DelegateBridge __Hotfix_SetPrefabScale_1;
    private static DelegateBridge __Hotfix_SetName;
    private static DelegateBridge __Hotfix_SetVisible;
    private static DelegateBridge __Hotfix_SetColor;
    private static DelegateBridge __Hotfix_SetIntensity;
    private static DelegateBridge __Hotfix_SetSaturation;
    private static DelegateBridge __Hotfix_UpdateMaterialPropertyBlock;
    private static DelegateBridge __Hotfix_SetEffect;
    private static DelegateBridge __Hotfix_ClearEffect;
    private static DelegateBridge __Hotfix_ClearAttachFxs;
    private static DelegateBridge __Hotfix_Pause;
    private static DelegateBridge __Hotfix_FastForward;
    private static DelegateBridge __Hotfix_FadeStop;
    private static DelegateBridge __Hotfix_ForceUpdateSpine;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_FxStop;
    private static DelegateBridge __Hotfix_SetFxCanLoop;
    private static DelegateBridge __Hotfix_IsFxLoop;
    private static DelegateBridge __Hotfix_TickEffect;
    private static DelegateBridge __Hotfix_TickFx;
    private static DelegateBridge __Hotfix_IsFxLifeEnd;
    private static DelegateBridge __Hotfix_TickAttachFx;
    private static DelegateBridge __Hotfix_SetPosition;
    private static DelegateBridge __Hotfix_GetPosition;
    private static DelegateBridge __Hotfix_SetAnchoredPosition;
    private static DelegateBridge __Hotfix_SetWorldPosition;
    private static DelegateBridge __Hotfix_GetWorldPosition;
    private static DelegateBridge __Hotfix_CheckIsCulled;
    private static DelegateBridge __Hotfix_IsCulled;
    private static DelegateBridge __Hotfix_IsVisible;
    private static DelegateBridge __Hotfix_FlashEffect;
    private static DelegateBridge __Hotfix_GhostEffect;
    private static DelegateBridge __Hotfix_ClearGhosts;
    private static DelegateBridge __Hotfix_HighlightEffect;
    private static DelegateBridge __Hotfix_FadeInEffect;
    private static DelegateBridge __Hotfix_FadeOutEffect;
    private static DelegateBridge __Hotfix_AutoDelete;
    private static DelegateBridge __Hotfix_DeleteMe;
    private static DelegateBridge __Hotfix_get_AssetName;
    private static DelegateBridge __Hotfix_get_IsDeleteMe;
    private static DelegateBridge __Hotfix_get_IsAutoDelete;

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Create(string assetName, bool isFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationStart(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEnd(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActive(bool active)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GetAnimationInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetParent(GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLayer(int layer)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSortingOrder(int order)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GameObject GetPrefab()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetContainer(IGenericGraphicContainer container)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFxPlayer(FxPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayDeathAnimation(int deathType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayFx(string name, int tag = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReplaceAnimations(List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeight(float height, float footHeight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCombatPosition(Vector2i pos, Fix64 z, int zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCombatDirection(Vector2i front, Fix64 frontZ)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCombatDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRotationZ(float rz)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRotation(Quaternion q)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrefabScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrefabScale(Vector3 scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetName(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetColor(Colori color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetIntensity(float intensity)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSaturation(float saturation)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateMaterialPropertyBlock()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetEffect(GraphicEffect e, float param1, float param2)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAttachFxs(int tagMask)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Pause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FastForward(float t)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeStop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdateSpine()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FxStop(bool fadeOut)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFxCanLoop(bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFxLoop()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickEffect(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickFx(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsFxLifeEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void TickAttachFx(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector3 GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnchoredPosition(Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetWorldPosition(Vector3 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 GetWorldPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CheckIsCulled(Vector3 p, bool isCombat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsCulled()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FlashEffect(Colori color, float intensity, float time, int repeat)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void GhostEffect(int count, float distance, float fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGhosts()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HighlightEffect(
      GraphicEffect effectType,
      Colori color,
      float intensity,
      float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeInEffect(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FadeOutEffect(float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoDelete(bool autoDelete)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeleteMe()
    {
      // ISSUE: unable to decompile the method.
    }

    public string AssetName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsDeleteMe
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsAutoDelete
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class SpineInfo
    {
      public SkeletonAnimation m_spine;
      public string m_initAnimationName;
      public bool m_isAnimationLoop;
      public Vector2 m_boundsMin;
      public Vector2 m_boundsMax;
      public bool m_isCulled;
      public float m_height;
      public float m_footHeight;
      public List<ReplaceAnim> m_replaceAnims;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public SpineInfo()
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public class FxInfo
    {
      public FxDesc m_fxDesc;
      public float m_initLife;
      public float m_lifeCountdown;
      public bool m_isInitLoop;
      public bool m_isPlayLoop;
      public bool m_isDeleteMe;
      public bool m_isAutoDelete;
      public int m_tag;
      public List<ParticleSystem> m_stopParticleSystems;
      public List<Renderer> m_stopRenderers;
      public TrailRenderer[] m_trailRenderers;
      public Bone m_attachBone;
      public float m_yOffset;
      private static DelegateBridge _c__Hotfix_ctor;

      public FxInfo()
      {
        GenericGraphic.FxInfo._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class Ghost
    {
      public float m_speed;
      public float m_followTime;
      public Vector2 m_followStartPosition;
      public int m_state;
      public int m_turnCount;
      public Vector2 m_targetPosition;
      public GenericGraphic m_fx;
      private static DelegateBridge _c__Hotfix_ctor;

      public Ghost()
      {
        GenericGraphic.Ghost._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }

    public class EffectInfo
    {
      public Colori m_color;
      public float m_intensity;
      public float m_totalTime;
      public float m_curTime;
      public int m_repeatCount;
      public GraphicEffect m_type;
      public float m_ghostDistance;
      public List<GenericGraphic.Ghost> m_ghosts;
      private static DelegateBridge _c__Hotfix_ctor;

      public EffectInfo()
      {
        GenericGraphic.EffectInfo._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }
  }
}
