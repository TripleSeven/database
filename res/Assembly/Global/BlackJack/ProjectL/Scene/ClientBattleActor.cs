﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleActor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.UI;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class ClientBattleActor : Entity
  {
    private ClientBattle m_clientBattle;
    private BattleActor m_battleActor;
    private int m_heroHealthPoint;
    private int m_soldierTotalHealthPoint;
    private int m_heroHealthPointMax;
    private int m_soldierTotalHealthPointMax;
    private uint m_fightTags;
    private ClientBattleActor.GraphicState[] m_graphicStates;
    private int m_soldierGraphicCount;
    private Vector2 m_graphicPosition;
    private bool m_isGraphicSkillFade;
    private bool m_isActionFinishedGray;
    private BattleActorUIController m_uiController;
    private List<ClientActorBuff> m_buffs;
    private List<GenericGraphic> m_attachFxs;
    private GridPosition m_position;
    private int m_direction;
    private bool m_isVisible;
    private bool m_isIdleRun;
    private bool m_isGuarding;
    private bool m_isTeleportDisappear;
    private string m_idleAnimationName;
    private int m_actionCount;
    private ClientBattleActorActionState m_actionState;
    private List<ClientActorAct> m_acts;
    private int m_actFrame;
    private int m_tempActFrame;
    private int m_deleteMeCountdown;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_InitializeEnd;
    private static DelegateBridge __Hotfix_PostRebuildBattle;
    private static DelegateBridge __Hotfix_CreateHeroGraphic;
    private static DelegateBridge __Hotfix_CreateSoldierGraphics;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_ComputeGraphicPosition;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_IsNullActor;
    private static DelegateBridge __Hotfix_DoPause;
    private static DelegateBridge __Hotfix_IsDead;
    private static DelegateBridge __Hotfix_GetTotalHealthPoint;
    private static DelegateBridge __Hotfix_GetTotalHealthPointMax;
    private static DelegateBridge __Hotfix_HasFightTag;
    private static DelegateBridge __Hotfix_IsStun;
    private static DelegateBridge __Hotfix_Locate;
    private static DelegateBridge __Hotfix_FaceTo;
    private static DelegateBridge __Hotfix_AddAct;
    private static DelegateBridge __Hotfix_HasAct;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_GetHeroAnimationDuration;
    private static DelegateBridge __Hotfix_IsHeroPlayAnimation;
    private static DelegateBridge __Hotfix_PlayDeathFx;
    private static DelegateBridge __Hotfix_PlayIdleAnimation;
    private static DelegateBridge __Hotfix_PlayHeroFx;
    private static DelegateBridge __Hotfix_PlayFx;
    private static DelegateBridge __Hotfix_PlayAttachFx;
    private static DelegateBridge __Hotfix_ClearAttachFx;
    private static DelegateBridge __Hotfix_SetGraphicEffect;
    private static DelegateBridge __Hotfix_ClearGraphicEffect;
    private static DelegateBridge __Hotfix_PlaySound_1;
    private static DelegateBridge __Hotfix_PlaySound_0;
    private static DelegateBridge __Hotfix_SetGraphicActionFinishedEffect;
    private static DelegateBridge __Hotfix_SetGraphicColor;
    private static DelegateBridge __Hotfix_AttachBuff;
    private static DelegateBridge __Hotfix_CompareBuffDisplayOrder;
    private static DelegateBridge __Hotfix_DetachBuff;
    private static DelegateBridge __Hotfix_SetHpBarType;
    private static DelegateBridge __Hotfix_StartIdleRun;
    private static DelegateBridge __Hotfix_StopIdleRun;
    private static DelegateBridge __Hotfix_CancelTeleportDisappear;
    private static DelegateBridge __Hotfix_GetActionCount;
    private static DelegateBridge __Hotfix_GetActionState;
    private static DelegateBridge __Hotfix_SetVisible;
    private static DelegateBridge __Hotfix_IsVisible;
    private static DelegateBridge __Hotfix_ComputeSoldierGraphicCount;
    private static DelegateBridge __Hotfix_UpdateSoldierGraphicCount;
    private static DelegateBridge __Hotfix_GetSoldierGraphicCount;
    private static DelegateBridge __Hotfix_UpdateGraphicVisible;
    private static DelegateBridge __Hotfix_ComputeGraphicOffset_0;
    private static DelegateBridge __Hotfix_ComputeGraphicOffset_1;
    private static DelegateBridge __Hotfix_GetGraphic;
    private static DelegateBridge __Hotfix_GetGraphicPosition;
    private static DelegateBridge __Hotfix_SetGraphicSkillFade;
    private static DelegateBridge __Hotfix_IsGraphicSkillFade;
    private static DelegateBridge __Hotfix_UpdateHpAndBuffUI;
    private static DelegateBridge __Hotfix_ShowPlayerIndex;
    private static DelegateBridge __Hotfix_get_Position;
    private static DelegateBridge __Hotfix_get_Direction;
    private static DelegateBridge __Hotfix_get_BattleActor;
    private static DelegateBridge __Hotfix_get_TeamNumber;
    private static DelegateBridge __Hotfix_get_HeroHealthPoint;
    private static DelegateBridge __Hotfix_get_SoldierTotalHealthPoint;
    private static DelegateBridge __Hotfix_TickActDispatch;
    private static DelegateBridge __Hotfix_TickAct_2;
    private static DelegateBridge __Hotfix_TickAct_0;
    private static DelegateBridge __Hotfix_TickAct_1;
    private static DelegateBridge __Hotfix_InitMoveAct;
    private static DelegateBridge __Hotfix_ComputeMoveTime;
    private static DelegateBridge __Hotfix_TickAct_19;
    private static DelegateBridge __Hotfix_TickAct_20;
    private static DelegateBridge __Hotfix_TickAct_40;
    private static DelegateBridge __Hotfix_TickAct_23;
    private static DelegateBridge __Hotfix_TickAct_26;
    private static DelegateBridge __Hotfix_TickAct_15;
    private static DelegateBridge __Hotfix_TickAct_28;
    private static DelegateBridge __Hotfix_TickAct_25;
    private static DelegateBridge __Hotfix_TickAct_24;
    private static DelegateBridge __Hotfix_TickAct_9;
    private static DelegateBridge __Hotfix_TickAct_36;
    private static DelegateBridge __Hotfix_TickAct_29;
    private static DelegateBridge __Hotfix_TickAct_11;
    private static DelegateBridge __Hotfix_TickAct_30;
    private static DelegateBridge __Hotfix_ComputeSkillCastTime;
    private static DelegateBridge __Hotfix_ComputeSkillHitTime;
    private static DelegateBridge __Hotfix_TickSkillHits;
    private static DelegateBridge __Hotfix_TickAct_4;
    private static DelegateBridge __Hotfix_TickAct_12;
    private static DelegateBridge __Hotfix_TickAct_17;
    private static DelegateBridge __Hotfix_TickAct_18;
    private static DelegateBridge __Hotfix_TickAct_21;
    private static DelegateBridge __Hotfix_TickAct_22;
    private static DelegateBridge __Hotfix_TickAct_6;
    private static DelegateBridge __Hotfix_TickAct_39;
    private static DelegateBridge __Hotfix_TickAct_38;
    private static DelegateBridge __Hotfix_TickAct_37;
    private static DelegateBridge __Hotfix_TickAct_35;
    private static DelegateBridge __Hotfix_TickAct_13;
    private static DelegateBridge __Hotfix_TickAct_3;
    private static DelegateBridge __Hotfix_TickAct_14;
    private static DelegateBridge __Hotfix_TickAct_10;
    private static DelegateBridge __Hotfix_TickAct_8;
    private static DelegateBridge __Hotfix_TickAct_27;
    private static DelegateBridge __Hotfix_TickAct_7;
    private static DelegateBridge __Hotfix_TickAct_16;
    private static DelegateBridge __Hotfix_TickAct_32;
    private static DelegateBridge __Hotfix_TickAct_34;
    private static DelegateBridge __Hotfix_TickAct_5;
    private static DelegateBridge __Hotfix_TickAct_46;
    private static DelegateBridge __Hotfix_TickAct_31;
    private static DelegateBridge __Hotfix_TickAct_33;
    private static DelegateBridge __Hotfix_TickAct_57;
    private static DelegateBridge __Hotfix_TickAct_43;
    private static DelegateBridge __Hotfix_TickAct_42;
    private static DelegateBridge __Hotfix_TickAct_53;
    private static DelegateBridge __Hotfix_TickAct_52;
    private static DelegateBridge __Hotfix_TickAct_51;
    private static DelegateBridge __Hotfix_TickAct_50;
    private static DelegateBridge __Hotfix_TickAct_49;
    private static DelegateBridge __Hotfix_TickAct_41;
    private static DelegateBridge __Hotfix_TickAct_58;
    private static DelegateBridge __Hotfix_TickAct_59;
    private static DelegateBridge __Hotfix_TickAct_47;
    private static DelegateBridge __Hotfix_TickAct_48;
    private static DelegateBridge __Hotfix_TickAct_45;
    private static DelegateBridge __Hotfix_TickAct_55;
    private static DelegateBridge __Hotfix_TickAct_56;
    private static DelegateBridge __Hotfix_TickAct_54;
    private static DelegateBridge __Hotfix_TickAct_60;
    private static DelegateBridge __Hotfix_TickAct_44;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientBattle battle, BattleActor actor)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitializeEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostRebuildBattle()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateHeroGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void CreateSoldierGraphics()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector3 ComputeGraphicPosition(Vector2 p, float zoffset)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsNullActor()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsDead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetTotalHealthPointMax()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool HasFightTag(FightTag ft)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsStun()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(GridPosition p, int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FaceTo(GridPosition p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAct(ClientActorAct act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAct()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop, bool onAll)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int GetHeroAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsHeroPlayAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayDeathFx(int deathType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayIdleAnimation()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayHeroFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayFx(string name, GridPosition p, float rotation = 0.0f, bool canLoop = false)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAttachFx(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearAttachFx()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicEffect(GraphicEffect e, float param1 = 0.0f, float param2 = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearGraphicEffect(GraphicEffect e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlaySound(SoundTableId id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicActionFinishedEffect(bool finished)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetGraphicColor(Colori c)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AttachBuff(BuffState buffState, List<int> disabledBuffStateIdList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int CompareBuffDisplayOrder(ClientActorBuff b0, ClientActorBuff b1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DetachBuff(BuffState buffState)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void SetHpBarType(int team, bool isAINpcOrTeamMate)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartIdleRun()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopIdleRun()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CancelTeleportDisappear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetActionCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleActorActionState GetActionState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeSoldierGraphicCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateSoldierGraphicCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetSoldierGraphicCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateGraphicVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ComputeGraphicOffset(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static Vector2 ComputeGraphicOffset(
      int idx,
      int dir,
      int soldierCount,
      bool hasHeroAndSoldier)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic GetGraphic(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GetGraphicPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicSkillFade(bool fade)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsGraphicSkillFade()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateHpAndBuffUI(List<int> disabledBuffStateIdList = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowPlayerIndex(bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    public GridPosition Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int Direction
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public BattleActor BattleActor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int TeamNumber
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int HeroHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public int SoldierTotalHealthPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickActDispatch(ClientActorAct act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActActive act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActActionBegin act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActActionEnd act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void InitMoveAct(GridPosition pos, int dir, bool sound, bool endDelay = true)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private int ComputeMoveTime(GridPosition p0, GridPosition p1)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActMoveEnd act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTryMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPerformMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPunchMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActExchangeMove act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSetDir act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPlayFx act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPlayAnimation act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActChangeIdleAnimation act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTarget act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSkill act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActCombineSkill act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSkillHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeSkillCastTime(ConfigDataSkillInfo skillInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static int ComputeSkillHitTime(
      ConfigDataSkillInfo skillInfo,
      List<ClientActorHitInfo> hits)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickSkillHits(ConfigDataSkillInfo skillInfo, List<ClientActorHitInfo> hits)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActAttachBuff act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActDetachBuff act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActImmune act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActModifyHp act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPassiveSkill act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActPassiveSkillHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActBuffHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTerrainHit act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTeleportDisappear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActTeleportAppear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActSummon act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActDie act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActAppear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActDisappear act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActChangeTeam act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActChangeArmy act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActReplace act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActCameraFocus act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActGainBattleTreasure act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStartGuard act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStopGuard act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActBeforeStartCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActCancelCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStartCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientActorActStopCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActStartBattle act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBattleWinCondition act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBattleLoseCondition act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextTurn act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextTeam act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextPlayer act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActNextActor act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActFastCombat act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBattleDialog act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActStartBattlePerform act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActStopBattlePerform act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActChangeMapTerrain act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActChangeMapTerrainEffect act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActCameraFocus act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActPlayMusic act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActPlaySound act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActPlayFx act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActWaitTime act)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool TickAct(ClientNullActorActBossPhase act)
    {
      // ISSUE: unable to decompile the method.
    }

    public class GraphicState
    {
      public GenericGraphic m_graphic;
      public float m_hideCountdown;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public GraphicState()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
