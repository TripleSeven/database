﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.FastCombatActorInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Battle;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Scene
{
  public class FastCombatActorInfo
  {
    public BattleActor m_battleActor;
    public int m_heroHpMax;
    public int m_soldierHpMax;
    public int m_beforeHeroHp;
    public int m_beforeSoldierHp;
    public int m_afterHeroHp;
    public int m_afterSoldierHp;
    public int m_heroDamage;
    public int m_soldierDamage;
    public bool m_isReceiveCriticalAttack;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public FastCombatActorInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
