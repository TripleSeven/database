﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.UISpineGraphic
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using Spine.Unity;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class UISpineGraphic
  {
    private SkeletonGraphic m_spine;
    private Material m_material;
    private GameObject m_gameObject;
    private bool m_isAnimationLoop;
    private List<ReplaceAnim> m_replaceAnims;
    private string m_assetName;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Create;
    private static DelegateBridge __Hotfix_HandleSpineAnimationEvent;
    private static DelegateBridge __Hotfix_HandleSpineAnimationStart;
    private static DelegateBridge __Hotfix_HandleSpineAnimationEnd;
    private static DelegateBridge __Hotfix_Destroy;
    private static DelegateBridge __Hotfix_SetDirection;
    private static DelegateBridge __Hotfix_SetParent;
    private static DelegateBridge __Hotfix_SetPosition;
    private static DelegateBridge __Hotfix_SetScale;
    private static DelegateBridge __Hotfix_SetRectTransformSize;
    private static DelegateBridge __Hotfix_SetColor;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_StopAnimation;
    private static DelegateBridge __Hotfix_IsLoop;
    private static DelegateBridge __Hotfix_IsPlayAnimation;
    private static DelegateBridge __Hotfix_GetAnimationDuration_0;
    private static DelegateBridge __Hotfix_GetAnimationDuration_1;
    private static DelegateBridge __Hotfix_GetAnimationTime;
    private static DelegateBridge __Hotfix_SetAnimationTime;
    private static DelegateBridge __Hotfix_SetReplaceAnimations;
    private static DelegateBridge __Hotfix_HasAnimation;
    private static DelegateBridge __Hotfix_SetAnimationSpeed;
    private static DelegateBridge __Hotfix_ForceUpdate;
    private static DelegateBridge __Hotfix_ShowSlot;
    private static DelegateBridge __Hotfix_EnableCanvasGroupAlpha;
    private static DelegateBridge __Hotfix_get_GameObject;
    private static DelegateBridge __Hotfix_get_AssetName;

    [MethodImpl((MethodImplOptions) 32768)]
    public UISpineGraphic()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Create(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEvent(Spine.AnimationState state, int trackIndex, Spine.Event e)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationStart(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void HandleSpineAnimationEnd(Spine.AnimationState state, int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDirection(int dir)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetParent(GameObject parent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScale(float scale)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRectTransformSize(Vector2 size)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetColor(Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string name, bool loop, int trackIndex = 0)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLoop(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayAnimation(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationDuration(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetAnimationTime(int trackIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimationTime(int trackIndex, float time)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetReplaceAnimations(List<ReplaceAnim> replaceAnims)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetAnimationSpeed(float s)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ForceUpdate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ShowSlot(string name, bool show)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableCanvasGroupAlpha(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    public GameObject GameObject
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public string AssetName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
