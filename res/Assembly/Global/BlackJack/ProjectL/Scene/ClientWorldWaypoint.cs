﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldWaypoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.UI;
using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.UI;
using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class ClientWorldWaypoint : Entity
  {
    private ClientWorld m_clientWorld;
    private ConfigDataWaypointInfo m_waypointInfo;
    private WayPointStatus m_status;
    private GenericGraphic m_graphic;
    private Vector2 m_position;
    private bool m_isVisible;
    private WorldWaypointUIController m_uiController;
    private CommonUIStateController m_graphicUIStateController;
    private Vector3 m_uiInitScale;
    private float m_graphicInitScale;
    private bool m_isPointerDown;
    private float m_scale;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Dispose;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_TickGraphic;
    private static DelegateBridge __Hotfix_Draw;
    private static DelegateBridge __Hotfix_DrawArrow;
    private static DelegateBridge __Hotfix_DoPause;
    private static DelegateBridge __Hotfix_Locate;
    private static DelegateBridge __Hotfix_SetStatus;
    private static DelegateBridge __Hotfix_GetStatus;
    private static DelegateBridge __Hotfix_SetScenarioState;
    private static DelegateBridge __Hotfix_SetGraphicState;
    private static DelegateBridge __Hotfix_SetCanClick;
    private static DelegateBridge __Hotfix_GetClickTransform;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_SetVisible;
    private static DelegateBridge __Hotfix_IsVisible;
    private static DelegateBridge __Hotfix_SetUISiblingIndex;
    private static DelegateBridge __Hotfix_OnUIPointerDown;
    private static DelegateBridge __Hotfix_OnUIPointerUp;
    private static DelegateBridge __Hotfix_OnUIClick;
    private static DelegateBridge __Hotfix_get_Position;
    private static DelegateBridge __Hotfix_get_WaypointInfo;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldWaypoint()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(ClientWorld world, ConfigDataWaypointInfo waypointInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Dispose()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void TickGraphic(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Draw()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void DrawArrow(Vector3 p0, Vector3 p1, Color color)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DoPause(bool pause)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Locate(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetStatus(WayPointStatus status)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WayPointStatus GetStatus()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetScenarioState(
      ConfigDataWaypointInfo waypointInfo,
      ConfigDataScenarioInfo lastFinishedScenarioInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGraphicState(string stateName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetCanClick(bool canClick)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RectTransform GetClickTransform()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void PlayAnimation(string name, bool loop)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetVisible(bool visible)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsVisible()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetUISiblingIndex(int idx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIPointerDown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIPointerUp()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnUIClick()
    {
      // ISSUE: unable to decompile the method.
    }

    public Vector2 Position
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public ConfigDataWaypointInfo WaypointInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
