﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Battle;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Scene
{
  public class ClientActorActSkill : ClientActorAct
  {
    public int m_direction;
    public ConfigDataSkillInfo m_skillInfo;
    public GridPosition m_targetPosition;
    public List<BattleActor> m_targetActors;
    public List<BattleActor> m_combineActors;
    public List<ClientActorHitInfo> m_selfHits;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientActorActSkill()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
