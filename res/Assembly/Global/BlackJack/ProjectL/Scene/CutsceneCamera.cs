﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CutsceneCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class CutsceneCamera : CameraBase
  {
    private Animator m_clipAnimator;
    private float m_initOrthographicSize;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_StartCutscene;
    private static DelegateBridge __Hotfix_StopCutscene;
    private static DelegateBridge __Hotfix_PlayClipAnimation;

    [MethodImpl((MethodImplOptions) 32768)]
    public CutsceneCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StopCutscene()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayClipAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
