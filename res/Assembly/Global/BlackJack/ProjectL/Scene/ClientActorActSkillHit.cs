﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorActSkillHit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Scene
{
  public class ClientActorActSkillHit : ClientActorAct
  {
    public ConfigDataSkillInfo m_skillInfo;
    public GridPosition m_position;
    public int m_direction;
    public List<ClientActorHitInfo> m_hits;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientActorActSkillHit()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
