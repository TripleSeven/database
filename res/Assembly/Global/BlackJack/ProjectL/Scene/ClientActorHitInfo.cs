﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientActorHitInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Battle;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Scene
{
  public class ClientActorHitInfo
  {
    public int m_heroHp;
    public int m_soldierHp;
    public int m_heroHpModify;
    public int m_soldierHpModify;
    public int m_frame;
    public DamageNumberType m_damageNumberType;
    public List<int> m_disabledBuffStateIdList;
    public int m_monsterTotalDamage;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientActorHitInfo()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
