﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ICollectionActivityWorldListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;

namespace BlackJack.ProjectL.Scene
{
  public interface ICollectionActivityWorldListener
  {
    void OnWaypointClick(
      ConfigDataCollectionActivityWaypointInfo waypointInfo);
  }
}
