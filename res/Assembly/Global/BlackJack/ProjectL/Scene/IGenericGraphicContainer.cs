﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.IGenericGraphicContainer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using FixMath.NET;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public interface IGenericGraphicContainer
  {
    Vector3 GetCameraPosition();

    Vector3 CombatPositionToWorldPosition(Vector2i p, Fix64 z, bool computeZOffset);

    bool IsCulled(Vector2 bmin, Vector2 bmax, bool isCombat);

    bool IsCombatGraphicMirrorX();
  }
}
