﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientWorldConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Scene
{
  public class ClientWorldConst
  {
    public const int TickRate = 30;
    public const float TickTime = 0.03333334f;
    public const float CameraAngle = 20f;
    public const int WorldActorSortingOrder = 2;
    public const int WorldNextScenarioSortingOrder = 5;
    public const int World2DWaypointSortingOrder = 1;
    public const int World2DWaypointUISortingOrder = 2;
    public const int World2DEventActorSortingOrder = 3;
    public const int World2DPlayerActorSortingOrder = 4;
    public const float WaypointZOffset = 0.0f;
    public const float EventActorZOffset = -0.04f;
    public const float PlayerActorZOffset = -0.08f;
    public const float WorldActorUIZOffset = -0.02f;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientWorldConst()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
