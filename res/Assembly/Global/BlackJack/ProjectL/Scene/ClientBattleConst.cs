﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.ClientBattleConst
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Scene
{
  public class ClientBattleConst
  {
    public const int TickRate = 30;
    public const float TickTime = 0.03333334f;
    public const float GridWidth = 2f;
    public const float GridHeight = 2f;
    public const float FootHeightScale = 0.25f;
    public const float SkillFadeZOffset = 30f;
    public const float GuardZOffset = -1f;
    public const float ActorFxZOffset = -20f;
    public const float TreasureIdleZOffset = 1f;
    public const float TreasureOpenZOffset = -1f;
    public const int HpBarBuffCountMax = 3;
    public const int BattleResultHeroCountMax = 5;
    private static DelegateBridge _c__Hotfix_ctor;

    [MethodImpl((MethodImplOptions) 32768)]
    public ClientBattleConst()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
