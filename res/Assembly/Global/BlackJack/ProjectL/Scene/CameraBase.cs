﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.CameraBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class CameraBase
  {
    protected Camera m_camera;
    protected Animator m_animator;
    protected GameObject m_animationGameObject;
    private bool m_isPlayAnimation;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_GetViewSize_0;
    private static DelegateBridge __Hotfix_PlayAnimation;
    private static DelegateBridge __Hotfix_GetAnimationOffset;
    private static DelegateBridge __Hotfix_IsCulled_0;
    private static DelegateBridge __Hotfix_IsCulled_2;
    private static DelegateBridge __Hotfix_GetViewSize_1;
    private static DelegateBridge __Hotfix_IsCulled_1;
    private static DelegateBridge __Hotfix_IsCulled_3;
    private static DelegateBridge __Hotfix_AspectScale;
    private static DelegateBridge __Hotfix_SetActive;
    private static DelegateBridge __Hotfix_get_Camera;

    [MethodImpl((MethodImplOptions) 32768)]
    public CameraBase()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected void Initialize(GameObject cameraGo, GameObject animatorGo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GetViewSize(out float width, out float height)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayAnimation(string name)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected Vector3 GetAnimationOffset()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool IsCulled(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public virtual bool IsCulled(Vector2 bmin, Vector2 bmax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static void GetViewSize(Camera cam, out float width, out float height)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCulled(Camera cam, Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static bool IsCulled(Camera cam, Vector2 bmin, Vector2 bmax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static float AspectScale()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetActive(bool a)
    {
      // ISSUE: unable to decompile the method.
    }

    public Camera Camera
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
