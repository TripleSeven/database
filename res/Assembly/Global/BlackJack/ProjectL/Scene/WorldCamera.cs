﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.WorldCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.UI;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BlackJack.ProjectL.Scene
{
  public class WorldCamera : CameraBase
  {
    private Vector2 m_position;
    private float m_viewHeight;
    private Vector2 m_touchMoveVelocity;
    private Vector2 m_followTargetPosition;
    private Vector2 m_followVelocity;
    private bool m_enableTouchMove;
    private bool m_isFollowing;
    private float m_viewAngle;
    private Vector2 m_smoothMoveStartPosition;
    private Vector2 m_smoothMoveEndPosition;
    private float m_smoothMoveStartHeight;
    private float m_smoothMoveEndHeight;
    private float m_smoothMoveTime;
    private float m_smoothMoveTotalTime;
    private EventSystem m_eventSystem;
    private PointerEventData m_pointData;
    private List<RaycastResult> m_raycastResults;
    private TouchStatus[] m_touchStatus;
    private Vector3 m_initPosition;
    private float m_initFov;
    private Vector2 m_mapSizeHalf;
    private float m_viewHeightMin;
    private float m_viewHeightMax;
    private WorldWaypointUIController m_hitWaypointUIController;
    private WorldEventActorUIController m_hitEventActorUIController;
    private CollectionActivityWaypointUIController m_hitCollectionActivityWaypointUIController;
    private Action m_onSmoothMoveEnd;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Initialize;
    private static DelegateBridge __Hotfix_Start_1;
    private static DelegateBridge __Hotfix_Start_0;
    private static DelegateBridge __Hotfix_EnableTouchMove;
    private static DelegateBridge __Hotfix_Update;
    private static DelegateBridge __Hotfix_ScreenPositionToViewPosition;
    private static DelegateBridge __Hotfix_IsTouchUI;
    private static DelegateBridge __Hotfix_UpdateTouch;
    private static DelegateBridge __Hotfix_ClampCameraPosition;
    private static DelegateBridge __Hotfix_GroundPositionToCameraPosition;
    private static DelegateBridge __Hotfix_Look;
    private static DelegateBridge __Hotfix_SmoothLook;
    private static DelegateBridge __Hotfix_IsSmoothMoving;
    private static DelegateBridge __Hotfix_StartFollow;
    private static DelegateBridge __Hotfix_SetFollowPosition;
    private static DelegateBridge __Hotfix_GetPosition;
    private static DelegateBridge __Hotfix_SetHeight;
    private static DelegateBridge __Hotfix_GetZoomFactor;

    [MethodImpl((MethodImplOptions) 32768)]
    public WorldCamera()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Initialize(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataWorldMapInfo mapInfo, float viewAngle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Start(ConfigDataCollectionActivityMapInfo mapInfo, float viewAngle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void EnableTouchMove(bool enable)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Update(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private static Vector2 ScreenPositionToViewPosition(
      Vector2 p,
      float viewWidth,
      float viewHeight)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsTouchUI(Vector2 pos)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void UpdateTouch(float dt)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 ClampCameraPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private Vector2 GroundPositionToCameraPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Look(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SmoothLook(Vector2 p, Action onEnd = null, float height = 0.0f)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSmoothMoving()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartFollow()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetFollowPosition(Vector2 p)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public Vector2 GetPosition()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetHeight(float h)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public float GetZoomFactor()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
