﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Scene.GraphicPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace BlackJack.ProjectL.Scene
{
  public class GraphicPool
  {
    private bool m_isFx;
    private GameObject m_parentGameObject;
    private Dictionary<string, List<GenericGraphic>> m_pool;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_SetDefaultParent;
    private static DelegateBridge __Hotfix_Create;
    private static DelegateBridge __Hotfix_Destroy;
    private static DelegateBridge __Hotfix_Clear;

    [MethodImpl((MethodImplOptions) 32768)]
    public GraphicPool(bool isFx)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefaultParent(GameObject go)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public GenericGraphic Create(string assetName)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Destroy(GenericGraphic g)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
