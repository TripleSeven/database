﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PlayerInfoInitAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PlayerInfoInitAck")]
  [Serializable]
  public class PlayerInfoInitAck : IExtensible
  {
    private int _Result;
    private long _ServerCurrTime;
    private bool _NeedCreateCharactor;
    private string _CharactorNickName;
    private string _GameUserId;
    private bool _IsNeedBattleReportLog;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_ServerCurrTime;
    private static DelegateBridge __Hotfix_set_ServerCurrTime;
    private static DelegateBridge __Hotfix_get_NeedCreateCharactor;
    private static DelegateBridge __Hotfix_set_NeedCreateCharactor;
    private static DelegateBridge __Hotfix_get_CharactorNickName;
    private static DelegateBridge __Hotfix_set_CharactorNickName;
    private static DelegateBridge __Hotfix_get_GameUserId;
    private static DelegateBridge __Hotfix_set_GameUserId;
    private static DelegateBridge __Hotfix_get_IsNeedBattleReportLog;
    private static DelegateBridge __Hotfix_set_IsNeedBattleReportLog;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoInitAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ServerCurrTime")]
    public long ServerCurrTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "NeedCreateCharactor")]
    public bool NeedCreateCharactor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "CharactorNickName")]
    public string CharactorNickName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "GameUserId")]
    [DefaultValue("")]
    public string GameUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "IsNeedBattleReportLog")]
    [DefaultValue(false)]
    public bool IsNeedBattleReportLog
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
