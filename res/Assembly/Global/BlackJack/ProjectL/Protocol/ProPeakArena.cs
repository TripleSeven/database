﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArena
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArena")]
  [Serializable]
  public class ProPeakArena : IExtensible
  {
    private ProPeakArenaMatchStats _FriendlyMatchStats;
    private ProPeakArenaMatchStats _LadderMatchStats;
    private ProPeakArenaMatchStats _FriendlyCareerMatchStats;
    private ProPeakArenaMatchStats _LadderCareerMatchStats;
    private readonly List<int> _WinsBonusIdAcquired;
    private ProPeakArenaBattleTeam _BattleTeam;
    private readonly List<int> _RegularRewardIndexAcquired;
    private readonly List<ProPeakArenaBattleReportNameRecord> _BattleReportNameRecords;
    private ProPeakArenaBetInfo _BetInfo;
    private int _CurrentSeason;
    private int _FinalRank;
    private ulong _LiveRoomId;
    private int _TodaysQuickLeavingCount;
    private long _BannedTime;
    private bool _NoticeEnterPlayOffRace;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_FriendlyMatchStats;
    private static DelegateBridge __Hotfix_set_FriendlyMatchStats;
    private static DelegateBridge __Hotfix_get_LadderMatchStats;
    private static DelegateBridge __Hotfix_set_LadderMatchStats;
    private static DelegateBridge __Hotfix_get_FriendlyCareerMatchStats;
    private static DelegateBridge __Hotfix_set_FriendlyCareerMatchStats;
    private static DelegateBridge __Hotfix_get_LadderCareerMatchStats;
    private static DelegateBridge __Hotfix_set_LadderCareerMatchStats;
    private static DelegateBridge __Hotfix_get_WinsBonusIdAcquired;
    private static DelegateBridge __Hotfix_get_BattleTeam;
    private static DelegateBridge __Hotfix_set_BattleTeam;
    private static DelegateBridge __Hotfix_get_RegularRewardIndexAcquired;
    private static DelegateBridge __Hotfix_get_BattleReportNameRecords;
    private static DelegateBridge __Hotfix_get_BetInfo;
    private static DelegateBridge __Hotfix_set_BetInfo;
    private static DelegateBridge __Hotfix_get_CurrentSeason;
    private static DelegateBridge __Hotfix_set_CurrentSeason;
    private static DelegateBridge __Hotfix_get_FinalRank;
    private static DelegateBridge __Hotfix_set_FinalRank;
    private static DelegateBridge __Hotfix_get_LiveRoomId;
    private static DelegateBridge __Hotfix_set_LiveRoomId;
    private static DelegateBridge __Hotfix_get_TodaysQuickLeavingCount;
    private static DelegateBridge __Hotfix_set_TodaysQuickLeavingCount;
    private static DelegateBridge __Hotfix_get_BannedTime;
    private static DelegateBridge __Hotfix_set_BannedTime;
    private static DelegateBridge __Hotfix_get_NoticeEnterPlayOffRace;
    private static DelegateBridge __Hotfix_set_NoticeEnterPlayOffRace;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArena()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyMatchStats")]
    public ProPeakArenaMatchStats FriendlyMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderMatchStats")]
    public ProPeakArenaMatchStats LadderMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyCareerMatchStats")]
    public ProPeakArenaMatchStats FriendlyCareerMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderCareerMatchStats")]
    public ProPeakArenaMatchStats LadderCareerMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "WinsBonusIdAcquired")]
    public List<int> WinsBonusIdAcquired
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "BattleTeam")]
    public ProPeakArenaBattleTeam BattleTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "RegularRewardIndexAcquired")]
    public List<int> RegularRewardIndexAcquired
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "BattleReportNameRecords")]
    public List<ProPeakArenaBattleReportNameRecord> BattleReportNameRecords
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, IsRequired = true, Name = "BetInfo")]
    public ProPeakArenaBetInfo BetInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentSeason")]
    public int CurrentSeason
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FinalRank")]
    public int FinalRank
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LiveRoomId")]
    public ulong LiveRoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TodaysQuickLeavingCount")]
    public int TodaysQuickLeavingCount
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BannedTime")]
    public long BannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = true, Name = "NoticeEnterPlayOffRace")]
    public bool NoticeEnterPlayOffRace
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
