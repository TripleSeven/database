﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProBusinessCardStatisticalData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProBusinessCardStatisticalData")]
  [Serializable]
  public class ProBusinessCardStatisticalData : IExtensible
  {
    private int _MostSkilledHeroId;
    private int _HeroTotalPower;
    private int _AchievementMissionNums;
    private int _MasterJobNums;
    private int _RiftAchievementNums;
    private int _ChooseLevelAchievementNums;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_MostSkilledHeroId;
    private static DelegateBridge __Hotfix_set_MostSkilledHeroId;
    private static DelegateBridge __Hotfix_get_HeroTotalPower;
    private static DelegateBridge __Hotfix_set_HeroTotalPower;
    private static DelegateBridge __Hotfix_get_AchievementMissionNums;
    private static DelegateBridge __Hotfix_set_AchievementMissionNums;
    private static DelegateBridge __Hotfix_get_MasterJobNums;
    private static DelegateBridge __Hotfix_set_MasterJobNums;
    private static DelegateBridge __Hotfix_get_RiftAchievementNums;
    private static DelegateBridge __Hotfix_set_RiftAchievementNums;
    private static DelegateBridge __Hotfix_get_ChooseLevelAchievementNums;
    private static DelegateBridge __Hotfix_set_ChooseLevelAchievementNums;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProBusinessCardStatisticalData()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MostSkilledHeroId")]
    public int MostSkilledHeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroTotalPower")]
    public int HeroTotalPower
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AchievementMissionNums")]
    public int AchievementMissionNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MasterJobNums")]
    public int MasterJobNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RiftAchievementNums")]
    public int RiftAchievementNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChooseLevelAchievementNums")]
    public int ChooseLevelAchievementNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
