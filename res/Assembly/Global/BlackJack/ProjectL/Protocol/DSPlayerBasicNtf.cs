﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSPlayerBasicNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSPlayerBasicNtf")]
  [Serializable]
  public class DSPlayerBasicNtf : IExtensible
  {
    private uint _Version;
    private int _PlayerLevel;
    private long _RechargeCsystal;
    private int _ExpCurr;
    private int _Energy;
    private long _Gold;
    private long _Crystal;
    private long _LastSignTime;
    private int _SignDays;
    private int _HeadIcon;
    private int _BuyEngryNums;
    private long _PlayerActionNextFlushTime;
    private readonly List<int> _GuideCompleteFlags;
    private int _ArenaTickets;
    private int _ArenaHonour;
    private int _BuyArenaTicketsNums;
    private long _EnergyFlushTime;
    private long _CreateTime;
    private int _FriendshipPoints;
    private bool _OpenGameRating;
    private long _RechargeRMB;
    private int _SkinTickets;
    private int _RealTimePVPHonor;
    private int _MemoryEssence;
    private int _MithralStone;
    private int _BrillianceMithralStone;
    private int _VipLevel;
    private bool _MemoryStoreOpen;
    private int _GuildMedal;
    private long _RefluxBeginTime;
    private int _ChallengePoint;
    private int _FashionPoint;
    private int _BuyJettonNums;
    private readonly List<int> _RewardedDialogIds;
    private int _Title;
    private string _Language;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_PlayerLevel;
    private static DelegateBridge __Hotfix_set_PlayerLevel;
    private static DelegateBridge __Hotfix_get_RechargeCsystal;
    private static DelegateBridge __Hotfix_set_RechargeCsystal;
    private static DelegateBridge __Hotfix_get_ExpCurr;
    private static DelegateBridge __Hotfix_set_ExpCurr;
    private static DelegateBridge __Hotfix_get_Energy;
    private static DelegateBridge __Hotfix_set_Energy;
    private static DelegateBridge __Hotfix_get_Gold;
    private static DelegateBridge __Hotfix_set_Gold;
    private static DelegateBridge __Hotfix_get_Crystal;
    private static DelegateBridge __Hotfix_set_Crystal;
    private static DelegateBridge __Hotfix_get_LastSignTime;
    private static DelegateBridge __Hotfix_set_LastSignTime;
    private static DelegateBridge __Hotfix_get_SignDays;
    private static DelegateBridge __Hotfix_set_SignDays;
    private static DelegateBridge __Hotfix_get_HeadIcon;
    private static DelegateBridge __Hotfix_set_HeadIcon;
    private static DelegateBridge __Hotfix_get_BuyEngryNums;
    private static DelegateBridge __Hotfix_set_BuyEngryNums;
    private static DelegateBridge __Hotfix_get_PlayerActionNextFlushTime;
    private static DelegateBridge __Hotfix_set_PlayerActionNextFlushTime;
    private static DelegateBridge __Hotfix_get_GuideCompleteFlags;
    private static DelegateBridge __Hotfix_get_ArenaTickets;
    private static DelegateBridge __Hotfix_set_ArenaTickets;
    private static DelegateBridge __Hotfix_get_ArenaHonour;
    private static DelegateBridge __Hotfix_set_ArenaHonour;
    private static DelegateBridge __Hotfix_get_BuyArenaTicketsNums;
    private static DelegateBridge __Hotfix_set_BuyArenaTicketsNums;
    private static DelegateBridge __Hotfix_get_EnergyFlushTime;
    private static DelegateBridge __Hotfix_set_EnergyFlushTime;
    private static DelegateBridge __Hotfix_get_CreateTime;
    private static DelegateBridge __Hotfix_set_CreateTime;
    private static DelegateBridge __Hotfix_get_FriendshipPoints;
    private static DelegateBridge __Hotfix_set_FriendshipPoints;
    private static DelegateBridge __Hotfix_get_OpenGameRating;
    private static DelegateBridge __Hotfix_set_OpenGameRating;
    private static DelegateBridge __Hotfix_get_RechargeRMB;
    private static DelegateBridge __Hotfix_set_RechargeRMB;
    private static DelegateBridge __Hotfix_get_SkinTickets;
    private static DelegateBridge __Hotfix_set_SkinTickets;
    private static DelegateBridge __Hotfix_get_RealTimePVPHonor;
    private static DelegateBridge __Hotfix_set_RealTimePVPHonor;
    private static DelegateBridge __Hotfix_get_MemoryEssence;
    private static DelegateBridge __Hotfix_set_MemoryEssence;
    private static DelegateBridge __Hotfix_get_MithralStone;
    private static DelegateBridge __Hotfix_set_MithralStone;
    private static DelegateBridge __Hotfix_get_BrillianceMithralStone;
    private static DelegateBridge __Hotfix_set_BrillianceMithralStone;
    private static DelegateBridge __Hotfix_get_VipLevel;
    private static DelegateBridge __Hotfix_set_VipLevel;
    private static DelegateBridge __Hotfix_get_MemoryStoreOpen;
    private static DelegateBridge __Hotfix_set_MemoryStoreOpen;
    private static DelegateBridge __Hotfix_get_GuildMedal;
    private static DelegateBridge __Hotfix_set_GuildMedal;
    private static DelegateBridge __Hotfix_get_RefluxBeginTime;
    private static DelegateBridge __Hotfix_set_RefluxBeginTime;
    private static DelegateBridge __Hotfix_get_ChallengePoint;
    private static DelegateBridge __Hotfix_set_ChallengePoint;
    private static DelegateBridge __Hotfix_get_FashionPoint;
    private static DelegateBridge __Hotfix_set_FashionPoint;
    private static DelegateBridge __Hotfix_get_BuyJettonNums;
    private static DelegateBridge __Hotfix_set_BuyJettonNums;
    private static DelegateBridge __Hotfix_get_RewardedDialogIds;
    private static DelegateBridge __Hotfix_get_Title;
    private static DelegateBridge __Hotfix_set_Title;
    private static DelegateBridge __Hotfix_get_Language;
    private static DelegateBridge __Hotfix_set_Language;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSPlayerBasicNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerLevel")]
    public int PlayerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RechargeCsystal")]
    public long RechargeCsystal
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExpCurr")]
    public int ExpCurr
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Energy")]
    public int Energy
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Gold")]
    public long Gold
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Crystal")]
    public long Crystal
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastSignTime")]
    public long LastSignTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SignDays")]
    public int SignDays
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeadIcon")]
    public int HeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyEngryNums")]
    public int BuyEngryNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PlayerActionNextFlushTime")]
    public long PlayerActionNextFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, Name = "GuideCompleteFlags")]
    public List<int> GuideCompleteFlags
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaTickets")]
    public int ArenaTickets
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaHonour")]
    public int ArenaHonour
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyArenaTicketsNums")]
    public int BuyArenaTicketsNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EnergyFlushTime")]
    public long EnergyFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CreateTime")]
    public long CreateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPoints")]
    public int FriendshipPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.Default, IsRequired = true, Name = "OpenGameRating")]
    public bool OpenGameRating
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RechargeRMB")]
    public long RechargeRMB
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SkinTickets")]
    public int SkinTickets
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RealTimePVPHonor")]
    public int RealTimePVPHonor
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MemoryEssence")]
    public int MemoryEssence
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MithralStone")]
    public int MithralStone
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BrillianceMithralStone")]
    public int BrillianceMithralStone
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "VipLevel")]
    public int VipLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.Default, IsRequired = true, Name = "MemoryStoreOpen")]
    public bool MemoryStoreOpen
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuildMedal")]
    public int GuildMedal
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RefluxBeginTime")]
    public long RefluxBeginTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChallengePoint")]
    public int ChallengePoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FashionPoint")]
    public int FashionPoint
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BuyJettonNums")]
    public int BuyJettonNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, Name = "RewardedDialogIds")]
    public List<int> RewardedDialogIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Title")]
    public int Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.Default, IsRequired = true, Name = "Language")]
    public string Language
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
