﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProChatContentVoice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProChatContentVoice")]
  [Serializable]
  public class ProChatContentVoice : IExtensible
  {
    private ulong _InstanceId;
    private byte[] _Voice;
    private int _VoiceLenth;
    private int _AudioFrequency;
    private int _AudioSampleLength;
    private string _TranslateText;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_Voice;
    private static DelegateBridge __Hotfix_set_Voice;
    private static DelegateBridge __Hotfix_get_VoiceLenth;
    private static DelegateBridge __Hotfix_set_VoiceLenth;
    private static DelegateBridge __Hotfix_get_AudioFrequency;
    private static DelegateBridge __Hotfix_set_AudioFrequency;
    private static DelegateBridge __Hotfix_get_AudioSampleLength;
    private static DelegateBridge __Hotfix_set_AudioSampleLength;
    private static DelegateBridge __Hotfix_get_TranslateText;
    private static DelegateBridge __Hotfix_set_TranslateText;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatContentVoice()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0.0f)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "Voice")]
    public byte[] Voice
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "VoiceLenth")]
    public int VoiceLenth
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AudioFrequency")]
    [DefaultValue(0)]
    public int AudioFrequency
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AudioSampleLength")]
    public int AudioSampleLength
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "TranslateText")]
    public string TranslateText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
