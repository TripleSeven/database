﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSLevelNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSLevelNtf")]
  [Serializable]
  public class DSLevelNtf : IExtensible
  {
    private uint _Version;
    private int _CurrentWayPointId;
    private int _ScenarioId;
    private readonly List<int> _ArrivedWayPointIds;
    private readonly List<ProRandomEvent> _RandomEvents;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_CurrentWayPointId;
    private static DelegateBridge __Hotfix_set_CurrentWayPointId;
    private static DelegateBridge __Hotfix_get_ScenarioId;
    private static DelegateBridge __Hotfix_set_ScenarioId;
    private static DelegateBridge __Hotfix_get_ArrivedWayPointIds;
    private static DelegateBridge __Hotfix_get_RandomEvents;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSLevelNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentWayPointId")]
    public int CurrentWayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScenarioId")]
    public int ScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "ArrivedWayPointIds")]
    public List<int> ArrivedWayPointIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "RandomEvents")]
    public List<ProRandomEvent> RandomEvents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
