﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.AncientCallBossBattleFinishedAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "AncientCallBossBattleFinishedAck")]
  [Serializable]
  public class AncientCallBossBattleFinishedAck : IExtensible
  {
    private int _Result;
    private int _AncientCallBossId;
    private bool _Win;
    private int _AncientCallBossTotalDamage;
    private int _AncientCallBossCurrentRanking;
    private int _AncientCallBossLastRankingTotalDamage;
    private int _AncientCallBossOldMaxDamage;
    private readonly List<int> _AchievementIds;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_AncientCallBossId;
    private static DelegateBridge __Hotfix_set_AncientCallBossId;
    private static DelegateBridge __Hotfix_get_Win;
    private static DelegateBridge __Hotfix_set_Win;
    private static DelegateBridge __Hotfix_get_AncientCallBossTotalDamage;
    private static DelegateBridge __Hotfix_set_AncientCallBossTotalDamage;
    private static DelegateBridge __Hotfix_get_AncientCallBossCurrentRanking;
    private static DelegateBridge __Hotfix_set_AncientCallBossCurrentRanking;
    private static DelegateBridge __Hotfix_get_AncientCallBossLastRankingTotalDamage;
    private static DelegateBridge __Hotfix_set_AncientCallBossLastRankingTotalDamage;
    private static DelegateBridge __Hotfix_get_AncientCallBossOldMaxDamage;
    private static DelegateBridge __Hotfix_set_AncientCallBossOldMaxDamage;
    private static DelegateBridge __Hotfix_get_AchievementIds;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public AncientCallBossBattleFinishedAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossId")]
    public int AncientCallBossId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Win")]
    public bool Win
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossTotalDamage")]
    public int AncientCallBossTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossCurrentRanking")]
    public int AncientCallBossCurrentRanking
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossLastRankingTotalDamage")]
    public int AncientCallBossLastRankingTotalDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "AncientCallBossOldMaxDamage")]
    public int AncientCallBossOldMaxDamage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "AchievementIds")]
    public List<int> AchievementIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
