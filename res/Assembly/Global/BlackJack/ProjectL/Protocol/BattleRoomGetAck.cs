﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomGetAck")]
  [Serializable]
  public class BattleRoomGetAck : IExtensible
  {
    private int _Result;
    private readonly List<ProBattleRoomPlayer> _Players;
    private int _BattleId;
    private int _BattleRoomType;
    private int _LeaderIndex;
    private int _GameFunctionTypeId;
    private int _LocationId;
    private int _ArmyRandomSeed;
    private int _RandomSeed;
    private int _BattleRoomStatus;
    private long _ReadyTimeOut;
    private readonly List<ProBattleHeroSetupInfo> _AllSetupInfos;
    private readonly List<ProBattleCommand> _Commands;
    private BattleRoomDataChangeNtf _RealTimePVPRoomData;
    private long _LastPlayerBeginActionTime;
    private readonly List<int> _PreferredHeroTagIds;
    private ulong _InstanceId;
    private ProBattleRoomBPStage _BattleRoomBPStage;
    private long _BattleTurnActionTimeSpan;
    private int _PeakArenaMode;
    private int _Round;
    private int _BORound;
    private int _MatchId;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_BattleRoomType;
    private static DelegateBridge __Hotfix_set_BattleRoomType;
    private static DelegateBridge __Hotfix_get_LeaderIndex;
    private static DelegateBridge __Hotfix_set_LeaderIndex;
    private static DelegateBridge __Hotfix_get_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_set_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_get_LocationId;
    private static DelegateBridge __Hotfix_set_LocationId;
    private static DelegateBridge __Hotfix_get_ArmyRandomSeed;
    private static DelegateBridge __Hotfix_set_ArmyRandomSeed;
    private static DelegateBridge __Hotfix_get_RandomSeed;
    private static DelegateBridge __Hotfix_set_RandomSeed;
    private static DelegateBridge __Hotfix_get_BattleRoomStatus;
    private static DelegateBridge __Hotfix_set_BattleRoomStatus;
    private static DelegateBridge __Hotfix_get_ReadyTimeOut;
    private static DelegateBridge __Hotfix_set_ReadyTimeOut;
    private static DelegateBridge __Hotfix_get_AllSetupInfos;
    private static DelegateBridge __Hotfix_get_Commands;
    private static DelegateBridge __Hotfix_get_RealTimePVPRoomData;
    private static DelegateBridge __Hotfix_set_RealTimePVPRoomData;
    private static DelegateBridge __Hotfix_get_LastPlayerBeginActionTime;
    private static DelegateBridge __Hotfix_set_LastPlayerBeginActionTime;
    private static DelegateBridge __Hotfix_get_PreferredHeroTagIds;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_BattleRoomBPStage;
    private static DelegateBridge __Hotfix_set_BattleRoomBPStage;
    private static DelegateBridge __Hotfix_get_BattleTurnActionTimeSpan;
    private static DelegateBridge __Hotfix_set_BattleTurnActionTimeSpan;
    private static DelegateBridge __Hotfix_get_PeakArenaMode;
    private static DelegateBridge __Hotfix_set_PeakArenaMode;
    private static DelegateBridge __Hotfix_get_Round;
    private static DelegateBridge __Hotfix_set_Round;
    private static DelegateBridge __Hotfix_get_BORound;
    private static DelegateBridge __Hotfix_set_BORound;
    private static DelegateBridge __Hotfix_get_MatchId;
    private static DelegateBridge __Hotfix_set_MatchId;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProBattleRoomPlayer> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleId")]
    [DefaultValue(0)]
    public int BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleRoomType")]
    [DefaultValue(0)]
    public int BattleRoomType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LeaderIndex")]
    public int LeaderIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LocationId")]
    [DefaultValue(0)]
    public int LocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArmyRandomSeed")]
    public int ArmyRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RandomSeed")]
    [DefaultValue(0)]
    public int RandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleRoomStatus")]
    public int BattleRoomStatus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadyTimeOut")]
    public long ReadyTimeOut
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "AllSetupInfos")]
    public List<ProBattleHeroSetupInfo> AllSetupInfos
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, Name = "Commands")]
    public List<ProBattleCommand> Commands
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = false, Name = "RealTimePVPRoomData")]
    [DefaultValue(null)]
    public BattleRoomDataChangeNtf RealTimePVPRoomData
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "LastPlayerBeginActionTime")]
    [DefaultValue(0)]
    public long LastPlayerBeginActionTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, Name = "PreferredHeroTagIds")]
    public List<int> PreferredHeroTagIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    [DefaultValue(0.0f)]
    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleRoomBPStage")]
    [DefaultValue(null)]
    public ProBattleRoomBPStage BattleRoomBPStage
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleTurnActionTimeSpan")]
    [DefaultValue(0)]
    public long BattleTurnActionTimeSpan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "PeakArenaMode")]
    public int PeakArenaMode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BORound")]
    public int BORound
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MatchId")]
    public int MatchId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
