﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProArenaBattleReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProArenaBattleReport")]
  [Serializable]
  public class ProArenaBattleReport : IExtensible
  {
    private int _Version;
    private ulong _InstanceId;
    private int _BattleType;
    private int _BattleId;
    private int _RandomSeed;
    private readonly List<ProBattleCommand> _Commands;
    private int _Status;
    private int _ArenaDefenderRuleId;
    private string _DefenderUserId;
    private string _DefenderName;
    private int _DefenderLevel;
    private readonly List<ProBattleHero> _DefenderHeroes;
    private string _AttackerUserId;
    private string _AttackerName;
    private int _AttackerLevel;
    private readonly List<ProBattleHero> _AttackerHeroes;
    private int _AttackerGotArenaPoints;
    private int _DefenderGotArenaPoints;
    private long _CreateTime;
    private int _OpponentHeadIcon;
    private readonly List<ProTrainingTech> _DefenderTechs;
    private readonly List<ProTrainingTech> _AttackerTechs;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_BattleType;
    private static DelegateBridge __Hotfix_set_BattleType;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_RandomSeed;
    private static DelegateBridge __Hotfix_set_RandomSeed;
    private static DelegateBridge __Hotfix_get_Commands;
    private static DelegateBridge __Hotfix_get_Status;
    private static DelegateBridge __Hotfix_set_Status;
    private static DelegateBridge __Hotfix_get_ArenaDefenderRuleId;
    private static DelegateBridge __Hotfix_set_ArenaDefenderRuleId;
    private static DelegateBridge __Hotfix_get_DefenderUserId;
    private static DelegateBridge __Hotfix_set_DefenderUserId;
    private static DelegateBridge __Hotfix_get_DefenderName;
    private static DelegateBridge __Hotfix_set_DefenderName;
    private static DelegateBridge __Hotfix_get_DefenderLevel;
    private static DelegateBridge __Hotfix_set_DefenderLevel;
    private static DelegateBridge __Hotfix_get_DefenderHeroes;
    private static DelegateBridge __Hotfix_get_AttackerUserId;
    private static DelegateBridge __Hotfix_set_AttackerUserId;
    private static DelegateBridge __Hotfix_get_AttackerName;
    private static DelegateBridge __Hotfix_set_AttackerName;
    private static DelegateBridge __Hotfix_get_AttackerLevel;
    private static DelegateBridge __Hotfix_set_AttackerLevel;
    private static DelegateBridge __Hotfix_get_AttackerHeroes;
    private static DelegateBridge __Hotfix_get_AttackerGotArenaPoints;
    private static DelegateBridge __Hotfix_set_AttackerGotArenaPoints;
    private static DelegateBridge __Hotfix_get_DefenderGotArenaPoints;
    private static DelegateBridge __Hotfix_set_DefenderGotArenaPoints;
    private static DelegateBridge __Hotfix_get_CreateTime;
    private static DelegateBridge __Hotfix_set_CreateTime;
    private static DelegateBridge __Hotfix_get_OpponentHeadIcon;
    private static DelegateBridge __Hotfix_set_OpponentHeadIcon;
    private static DelegateBridge __Hotfix_get_DefenderTechs;
    private static DelegateBridge __Hotfix_get_AttackerTechs;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaBattleReport()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(0)]
    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "Version")]
    public int Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0.0f)]
    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "InstanceId")]
    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleType")]
    public int BattleType
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BattleId")]
    [DefaultValue(0)]
    public int BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "RandomSeed")]
    public int RandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Commands")]
    public List<ProBattleCommand> Commands
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "Status")]
    [DefaultValue(0)]
    public int Status
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaDefenderRuleId")]
    [DefaultValue(0)]
    public int ArenaDefenderRuleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = false, Name = "DefenderUserId")]
    [DefaultValue("")]
    public string DefenderUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = false, Name = "DefenderName")]
    public string DefenderName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "DefenderLevel")]
    [DefaultValue(0)]
    public int DefenderLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.Default, Name = "DefenderHeroes")]
    public List<ProBattleHero> DefenderHeroes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = false, Name = "AttackerUserId")]
    [DefaultValue("")]
    public string AttackerUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = false, Name = "AttackerName")]
    public string AttackerName
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AttackerLevel")]
    public int AttackerLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, Name = "AttackerHeroes")]
    public List<ProBattleHero> AttackerHeroes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "AttackerGotArenaPoints")]
    public int AttackerGotArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "DefenderGotArenaPoints")]
    [DefaultValue(0)]
    public int DefenderGotArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "CreateTime")]
    [DefaultValue(0)]
    public long CreateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "OpponentHeadIcon")]
    public int OpponentHeadIcon
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.Default, Name = "DefenderTechs")]
    public List<ProTrainingTech> DefenderTechs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, Name = "AttackerTechs")]
    public List<ProTrainingTech> AttackerTechs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
