﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProChatCententInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProChatCententInfo")]
  [Serializable]
  public class ProChatCententInfo : IExtensible
  {
    private ProChatContentText _ChatContentText;
    private readonly List<ProGoodsWithInstanceId> _GoodWithInstanceIdList;
    private ProPeakArenaBattleReportShareInfo _PeakArenaBattleReportInfo;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ChatContentText;
    private static DelegateBridge __Hotfix_set_ChatContentText;
    private static DelegateBridge __Hotfix_get_GoodWithInstanceIdList;
    private static DelegateBridge __Hotfix_get_PeakArenaBattleReportInfo;
    private static DelegateBridge __Hotfix_set_PeakArenaBattleReportInfo;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProChatCententInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [DefaultValue(null)]
    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatContentText")]
    public ProChatContentText ChatContentText
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "GoodWithInstanceIdList")]
    public List<ProGoodsWithInstanceId> GoodWithInstanceIdList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "PeakArenaBattleReportInfo")]
    [DefaultValue(null)]
    public ProPeakArenaBattleReportShareInfo PeakArenaBattleReportInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
