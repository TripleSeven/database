﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.TeamRoomAutoMatchAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "TeamRoomAutoMatchAck")]
  [Serializable]
  public class TeamRoomAutoMatchAck : IExtensible
  {
    private int _Result;
    private int _GameFunctionTypeId;
    private int _LocationId;
    private ProTeamRoom _Room;
    private int _FrontOfYouWaitingNums;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_set_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_get_LocationId;
    private static DelegateBridge __Hotfix_set_LocationId;
    private static DelegateBridge __Hotfix_get_Room;
    private static DelegateBridge __Hotfix_set_Room;
    private static DelegateBridge __Hotfix_get_FrontOfYouWaitingNums;
    private static DelegateBridge __Hotfix_set_FrontOfYouWaitingNums;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamRoomAutoMatchAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LocationId")]
    public int LocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "Room")]
    public ProTeamRoom Room
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(0)]
    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "FrontOfYouWaitingNums")]
    public int FrontOfYouWaitingNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
