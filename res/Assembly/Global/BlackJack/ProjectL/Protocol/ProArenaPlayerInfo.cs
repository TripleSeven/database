﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProArenaPlayerInfo")]
  [Serializable]
  public class ProArenaPlayerInfo : IExtensible
  {
    private ProArenaPlayerDefensiveTeam _DefensiveTeam;
    private readonly List<ProArenaOpponent> _Opponents;
    private int _ArenaLevelId;
    private int _ArenaPoints;
    private bool _AttackedOpponent;
    private int _VictoryPoints;
    private long _WeekLastFlushTime;
    private readonly List<int> _ReceivedVictoryPointsRewardIndexs;
    private readonly List<int> _ThisWeekBattleIds;
    private int _ConsecutiveVictoryNums;
    private int _ThisWeekTotalBattleNums;
    private int _ThisWeekVictoryNums;
    private bool _IsAutoBattle;
    private long _NextFlushOpponentTime;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_DefensiveTeam;
    private static DelegateBridge __Hotfix_set_DefensiveTeam;
    private static DelegateBridge __Hotfix_get_Opponents;
    private static DelegateBridge __Hotfix_get_ArenaLevelId;
    private static DelegateBridge __Hotfix_set_ArenaLevelId;
    private static DelegateBridge __Hotfix_get_ArenaPoints;
    private static DelegateBridge __Hotfix_set_ArenaPoints;
    private static DelegateBridge __Hotfix_get_AttackedOpponent;
    private static DelegateBridge __Hotfix_set_AttackedOpponent;
    private static DelegateBridge __Hotfix_get_VictoryPoints;
    private static DelegateBridge __Hotfix_set_VictoryPoints;
    private static DelegateBridge __Hotfix_get_WeekLastFlushTime;
    private static DelegateBridge __Hotfix_set_WeekLastFlushTime;
    private static DelegateBridge __Hotfix_get_ReceivedVictoryPointsRewardIndexs;
    private static DelegateBridge __Hotfix_get_ThisWeekBattleIds;
    private static DelegateBridge __Hotfix_get_ConsecutiveVictoryNums;
    private static DelegateBridge __Hotfix_set_ConsecutiveVictoryNums;
    private static DelegateBridge __Hotfix_get_ThisWeekTotalBattleNums;
    private static DelegateBridge __Hotfix_set_ThisWeekTotalBattleNums;
    private static DelegateBridge __Hotfix_get_ThisWeekVictoryNums;
    private static DelegateBridge __Hotfix_set_ThisWeekVictoryNums;
    private static DelegateBridge __Hotfix_get_IsAutoBattle;
    private static DelegateBridge __Hotfix_set_IsAutoBattle;
    private static DelegateBridge __Hotfix_get_NextFlushOpponentTime;
    private static DelegateBridge __Hotfix_set_NextFlushOpponentTime;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "DefensiveTeam")]
    public ProArenaPlayerDefensiveTeam DefensiveTeam
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Opponents")]
    public List<ProArenaOpponent> Opponents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaLevelId")]
    public int ArenaLevelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ArenaPoints")]
    public int ArenaPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "AttackedOpponent")]
    public bool AttackedOpponent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "VictoryPoints")]
    public int VictoryPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WeekLastFlushTime")]
    public long WeekLastFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "ReceivedVictoryPointsRewardIndexs")]
    public List<int> ReceivedVictoryPointsRewardIndexs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "ThisWeekBattleIds")]
    public List<int> ThisWeekBattleIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ConsecutiveVictoryNums")]
    public int ConsecutiveVictoryNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ThisWeekTotalBattleNums")]
    public int ThisWeekTotalBattleNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ThisWeekVictoryNums")]
    public int ThisWeekVictoryNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAutoBattle")]
    public bool IsAutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextFlushOpponentTime")]
    public long NextFlushOpponentTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
