﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PlayerInfoInitReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PlayerInfoInitReq")]
  [Serializable]
  public class PlayerInfoInitReq : IExtensible
  {
    private uint _DSBasicInfoVersion;
    private uint _DSBagVersion;
    private uint _DSHeroVersion;
    private uint _DSLevelVersion;
    private uint _DSRiftVersion;
    private uint _DSBattleVersion;
    private uint _DSMailVersion;
    private uint _DSFixedStoreVersion;
    private uint _DSChatVersion;
    private uint _DSSelectCardVersion;
    private uint _DSMissionVersion;
    private uint _DSThearchyTrialVersion;
    private uint _DSAnikiGymVersion;
    private uint _DSTrainingGroundVersion;
    private uint _DSOperationalActivityVersion;
    private uint _ClientAnnouncementVersion;
    private uint _DSRandomStoreVersion;
    private uint _DSArenaVersion;
    private uint _DSSurveyVersion;
    private uint _DSTreasureMapVersion;
    private uint _DSMemoryCorridorVersion;
    private uint _DSHeroDungeonVersion;
    private uint _DSHerotrainningVersion;
    private uint _DSHeroAssistantsVersion;
    private uint _DSFriendVersion;
    private uint _DSHeroPhantomVersion;
    private uint _DSCooperateBattleVersion;
    private uint _DSNoviceVersion;
    private uint _DSRechargeStoreVersion;
    private uint _DSResourceVersion;
    private uint _DSRealTimePVPVersion;
    private uint _DSRaffleVersion;
    private uint _DSRiftStoreVersion;
    private uint _DSUnchartedScoreVersion;
    private uint _DSClimbTowerVersion;
    private uint _DSEternalShrineVersion;
    private uint _DSRefluxVersion;
    private uint _DSCollectionActivityVersion;
    private uint _DSHeroAnthemVersion;
    private uint _DSPeakArenaVersion;
    private uint _DSAncientCallVersion;
    private uint _DSUnchartedLevelRaidVersion;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_DSBasicInfoVersion;
    private static DelegateBridge __Hotfix_set_DSBasicInfoVersion;
    private static DelegateBridge __Hotfix_get_DSBagVersion;
    private static DelegateBridge __Hotfix_set_DSBagVersion;
    private static DelegateBridge __Hotfix_get_DSHeroVersion;
    private static DelegateBridge __Hotfix_set_DSHeroVersion;
    private static DelegateBridge __Hotfix_get_DSLevelVersion;
    private static DelegateBridge __Hotfix_set_DSLevelVersion;
    private static DelegateBridge __Hotfix_get_DSRiftVersion;
    private static DelegateBridge __Hotfix_set_DSRiftVersion;
    private static DelegateBridge __Hotfix_get_DSBattleVersion;
    private static DelegateBridge __Hotfix_set_DSBattleVersion;
    private static DelegateBridge __Hotfix_get_DSMailVersion;
    private static DelegateBridge __Hotfix_set_DSMailVersion;
    private static DelegateBridge __Hotfix_get_DSFixedStoreVersion;
    private static DelegateBridge __Hotfix_set_DSFixedStoreVersion;
    private static DelegateBridge __Hotfix_get_DSChatVersion;
    private static DelegateBridge __Hotfix_set_DSChatVersion;
    private static DelegateBridge __Hotfix_get_DSSelectCardVersion;
    private static DelegateBridge __Hotfix_set_DSSelectCardVersion;
    private static DelegateBridge __Hotfix_get_DSMissionVersion;
    private static DelegateBridge __Hotfix_set_DSMissionVersion;
    private static DelegateBridge __Hotfix_get_DSThearchyTrialVersion;
    private static DelegateBridge __Hotfix_set_DSThearchyTrialVersion;
    private static DelegateBridge __Hotfix_get_DSAnikiGymVersion;
    private static DelegateBridge __Hotfix_set_DSAnikiGymVersion;
    private static DelegateBridge __Hotfix_get_DSTrainingGroundVersion;
    private static DelegateBridge __Hotfix_set_DSTrainingGroundVersion;
    private static DelegateBridge __Hotfix_get_DSOperationalActivityVersion;
    private static DelegateBridge __Hotfix_set_DSOperationalActivityVersion;
    private static DelegateBridge __Hotfix_get_ClientAnnouncementVersion;
    private static DelegateBridge __Hotfix_set_ClientAnnouncementVersion;
    private static DelegateBridge __Hotfix_get_DSRandomStoreVersion;
    private static DelegateBridge __Hotfix_set_DSRandomStoreVersion;
    private static DelegateBridge __Hotfix_get_DSArenaVersion;
    private static DelegateBridge __Hotfix_set_DSArenaVersion;
    private static DelegateBridge __Hotfix_get_DSSurveyVersion;
    private static DelegateBridge __Hotfix_set_DSSurveyVersion;
    private static DelegateBridge __Hotfix_get_DSTreasureMapVersion;
    private static DelegateBridge __Hotfix_set_DSTreasureMapVersion;
    private static DelegateBridge __Hotfix_get_DSMemoryCorridorVersion;
    private static DelegateBridge __Hotfix_set_DSMemoryCorridorVersion;
    private static DelegateBridge __Hotfix_get_DSHeroDungeonVersion;
    private static DelegateBridge __Hotfix_set_DSHeroDungeonVersion;
    private static DelegateBridge __Hotfix_get_DSHerotrainningVersion;
    private static DelegateBridge __Hotfix_set_DSHerotrainningVersion;
    private static DelegateBridge __Hotfix_get_DSHeroAssistantsVersion;
    private static DelegateBridge __Hotfix_set_DSHeroAssistantsVersion;
    private static DelegateBridge __Hotfix_get_DSFriendVersion;
    private static DelegateBridge __Hotfix_set_DSFriendVersion;
    private static DelegateBridge __Hotfix_get_DSHeroPhantomVersion;
    private static DelegateBridge __Hotfix_set_DSHeroPhantomVersion;
    private static DelegateBridge __Hotfix_get_DSCooperateBattleVersion;
    private static DelegateBridge __Hotfix_set_DSCooperateBattleVersion;
    private static DelegateBridge __Hotfix_get_DSNoviceVersion;
    private static DelegateBridge __Hotfix_set_DSNoviceVersion;
    private static DelegateBridge __Hotfix_get_DSRechargeStoreVersion;
    private static DelegateBridge __Hotfix_set_DSRechargeStoreVersion;
    private static DelegateBridge __Hotfix_get_DSResourceVersion;
    private static DelegateBridge __Hotfix_set_DSResourceVersion;
    private static DelegateBridge __Hotfix_get_DSRealTimePVPVersion;
    private static DelegateBridge __Hotfix_set_DSRealTimePVPVersion;
    private static DelegateBridge __Hotfix_get_DSRaffleVersion;
    private static DelegateBridge __Hotfix_set_DSRaffleVersion;
    private static DelegateBridge __Hotfix_get_DSRiftStoreVersion;
    private static DelegateBridge __Hotfix_set_DSRiftStoreVersion;
    private static DelegateBridge __Hotfix_get_DSUnchartedScoreVersion;
    private static DelegateBridge __Hotfix_set_DSUnchartedScoreVersion;
    private static DelegateBridge __Hotfix_get_DSClimbTowerVersion;
    private static DelegateBridge __Hotfix_set_DSClimbTowerVersion;
    private static DelegateBridge __Hotfix_get_DSEternalShrineVersion;
    private static DelegateBridge __Hotfix_set_DSEternalShrineVersion;
    private static DelegateBridge __Hotfix_get_DSRefluxVersion;
    private static DelegateBridge __Hotfix_set_DSRefluxVersion;
    private static DelegateBridge __Hotfix_get_DSCollectionActivityVersion;
    private static DelegateBridge __Hotfix_set_DSCollectionActivityVersion;
    private static DelegateBridge __Hotfix_get_DSHeroAnthemVersion;
    private static DelegateBridge __Hotfix_set_DSHeroAnthemVersion;
    private static DelegateBridge __Hotfix_get_DSPeakArenaVersion;
    private static DelegateBridge __Hotfix_set_DSPeakArenaVersion;
    private static DelegateBridge __Hotfix_get_DSAncientCallVersion;
    private static DelegateBridge __Hotfix_set_DSAncientCallVersion;
    private static DelegateBridge __Hotfix_get_DSUnchartedLevelRaidVersion;
    private static DelegateBridge __Hotfix_set_DSUnchartedLevelRaidVersion;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public PlayerInfoInitReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSBasicInfoVersion")]
    public uint DSBasicInfoVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSBagVersion")]
    public uint DSBagVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroVersion")]
    public uint DSHeroVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSLevelVersion")]
    public uint DSLevelVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRiftVersion")]
    public uint DSRiftVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSBattleVersion")]
    public uint DSBattleVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSMailVersion")]
    public uint DSMailVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSFixedStoreVersion")]
    public uint DSFixedStoreVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSChatVersion")]
    public uint DSChatVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSSelectCardVersion")]
    public uint DSSelectCardVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSMissionVersion")]
    public uint DSMissionVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSThearchyTrialVersion")]
    public uint DSThearchyTrialVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSAnikiGymVersion")]
    public uint DSAnikiGymVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSTrainingGroundVersion")]
    public uint DSTrainingGroundVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSOperationalActivityVersion")]
    public uint DSOperationalActivityVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ClientAnnouncementVersion")]
    public uint ClientAnnouncementVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRandomStoreVersion")]
    public uint DSRandomStoreVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSArenaVersion")]
    public uint DSArenaVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSSurveyVersion")]
    public uint DSSurveyVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSTreasureMapVersion")]
    public uint DSTreasureMapVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSMemoryCorridorVersion")]
    public uint DSMemoryCorridorVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroDungeonVersion")]
    public uint DSHeroDungeonVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(23, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHerotrainningVersion")]
    public uint DSHerotrainningVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(24, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroAssistantsVersion")]
    public uint DSHeroAssistantsVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(25, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSFriendVersion")]
    public uint DSFriendVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(26, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroPhantomVersion")]
    public uint DSHeroPhantomVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(27, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSCooperateBattleVersion")]
    public uint DSCooperateBattleVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(28, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSNoviceVersion")]
    public uint DSNoviceVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(29, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRechargeStoreVersion")]
    public uint DSRechargeStoreVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(30, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSResourceVersion")]
    public uint DSResourceVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(31, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRealTimePVPVersion")]
    public uint DSRealTimePVPVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(32, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRaffleVersion")]
    public uint DSRaffleVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(33, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRiftStoreVersion")]
    public uint DSRiftStoreVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(34, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSUnchartedScoreVersion")]
    public uint DSUnchartedScoreVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(35, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSClimbTowerVersion")]
    public uint DSClimbTowerVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(36, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSEternalShrineVersion")]
    public uint DSEternalShrineVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(37, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSRefluxVersion")]
    public uint DSRefluxVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(38, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSCollectionActivityVersion")]
    public uint DSCollectionActivityVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(39, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSHeroAnthemVersion")]
    public uint DSHeroAnthemVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(40, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSPeakArenaVersion")]
    public uint DSPeakArenaVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(41, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSAncientCallVersion")]
    public uint DSAncientCallVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(42, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DSUnchartedLevelRaidVersion")]
    public uint DSUnchartedLevelRaidVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
