﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProTeamRoomInviteInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProTeamRoomInviteInfo")]
  [Serializable]
  public class ProTeamRoomInviteInfo : IExtensible
  {
    private ulong _SessionId;
    private int _ChannelId;
    private string _Name;
    private int _Level;
    private int _RoomId;
    private int _GameFunctionTypeId;
    private int _LocationId;
    private long _TimeOut;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_SessionId;
    private static DelegateBridge __Hotfix_set_SessionId;
    private static DelegateBridge __Hotfix_get_ChannelId;
    private static DelegateBridge __Hotfix_set_ChannelId;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_set_RoomId;
    private static DelegateBridge __Hotfix_get_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_set_GameFunctionTypeId;
    private static DelegateBridge __Hotfix_get_LocationId;
    private static DelegateBridge __Hotfix_set_LocationId;
    private static DelegateBridge __Hotfix_get_TimeOut;
    private static DelegateBridge __Hotfix_set_TimeOut;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoomInviteInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SessionId")]
    public ulong SessionId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public int RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GameFunctionTypeId")]
    public int GameFunctionTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LocationId")]
    public int LocationId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TimeOut")]
    public long TimeOut
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
