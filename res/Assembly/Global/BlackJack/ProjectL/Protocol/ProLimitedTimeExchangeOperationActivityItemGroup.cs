﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProLimitedTimeExchangeOperationActivityItemGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProLimitedTimeExchangeOperationActivityItemGroup")]
  [Serializable]
  public class ProLimitedTimeExchangeOperationActivityItemGroup : IExtensible
  {
    private int _ItemGroupIndex;
    private int _ExchangedNums;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ItemGroupIndex;
    private static DelegateBridge __Hotfix_set_ItemGroupIndex;
    private static DelegateBridge __Hotfix_get_ExchangedNums;
    private static DelegateBridge __Hotfix_set_ExchangedNums;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProLimitedTimeExchangeOperationActivityItemGroup()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ItemGroupIndex")]
    public int ItemGroupIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ExchangedNums")]
    public int ExchangedNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
