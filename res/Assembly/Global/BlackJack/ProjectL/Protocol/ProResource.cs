﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProResource")]
  [Serializable]
  public class ProResource : IExtensible
  {
    private readonly List<int> _HeadFrames;
    private readonly List<int> _HeroSkinIds;
    private readonly List<int> _SoldierSkinIds;
    private readonly List<ProMonthCard> _MonthCards;
    private readonly List<int> _EquipmentIds;
    private readonly List<int> _TitleIds;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_HeadFrames;
    private static DelegateBridge __Hotfix_get_HeroSkinIds;
    private static DelegateBridge __Hotfix_get_SoldierSkinIds;
    private static DelegateBridge __Hotfix_get_MonthCards;
    private static DelegateBridge __Hotfix_get_EquipmentIds;
    private static DelegateBridge __Hotfix_get_TitleIds;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProResource()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, Name = "HeadFrames")]
    public List<int> HeadFrames
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "HeroSkinIds")]
    public List<int> HeroSkinIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "SoldierSkinIds")]
    public List<int> SoldierSkinIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "MonthCards")]
    public List<ProMonthCard> MonthCards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, Name = "EquipmentIds")]
    public List<int> EquipmentIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, Name = "TitleIds")]
    public List<int> TitleIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
