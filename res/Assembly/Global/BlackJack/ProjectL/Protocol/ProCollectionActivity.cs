﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProCollectionActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProCollectionActivity")]
  [Serializable]
  public class ProCollectionActivity : IExtensible
  {
    private ulong _ActivityInstanceId;
    private readonly List<int> _FinishedChallengeLevelIds;
    private int _LastFinishedScenarioId;
    private readonly List<int> _FinishedLootLevelIds;
    private int _CurrentWayPointId;
    private readonly List<ProCollectionActivityPlayerExchangeInfo> _ExchangeInfoList;
    private int _Score;
    private readonly List<ProCollectionEvent> _CollectionEvents;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ActivityInstanceId;
    private static DelegateBridge __Hotfix_set_ActivityInstanceId;
    private static DelegateBridge __Hotfix_get_FinishedChallengeLevelIds;
    private static DelegateBridge __Hotfix_get_LastFinishedScenarioId;
    private static DelegateBridge __Hotfix_set_LastFinishedScenarioId;
    private static DelegateBridge __Hotfix_get_FinishedLootLevelIds;
    private static DelegateBridge __Hotfix_get_CurrentWayPointId;
    private static DelegateBridge __Hotfix_set_CurrentWayPointId;
    private static DelegateBridge __Hotfix_get_ExchangeInfoList;
    private static DelegateBridge __Hotfix_get_Score;
    private static DelegateBridge __Hotfix_set_Score;
    private static DelegateBridge __Hotfix_get_CollectionEvents;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProCollectionActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActivityInstanceId")]
    public ulong ActivityInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "FinishedChallengeLevelIds")]
    public List<int> FinishedChallengeLevelIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LastFinishedScenarioId")]
    public int LastFinishedScenarioId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "FinishedLootLevelIds")]
    public List<int> FinishedLootLevelIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CurrentWayPointId")]
    public int CurrentWayPointId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "ExchangeInfoList")]
    public List<ProCollectionActivityPlayerExchangeInfo> ExchangeInfoList
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Score")]
    public int Score
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "CollectionEvents")]
    public List<ProCollectionEvent> CollectionEvents
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
