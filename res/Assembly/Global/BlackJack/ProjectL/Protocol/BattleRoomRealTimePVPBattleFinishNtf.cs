﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomRealTimePVPBattleFinishNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomRealTimePVPBattleFinishNtf")]
  [Serializable]
  public class BattleRoomRealTimePVPBattleFinishNtf : IExtensible
  {
    private ulong _WinSessionId;
    private int _Mode;
    private int _DanDiff;
    private int _LocalRankDiff;
    private int _GlobalRankDiff;
    private bool _IsPromotion;
    private ProRealTimePVPBattleReport _Report;
    private ulong _BattleInstanceId;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_WinSessionId;
    private static DelegateBridge __Hotfix_set_WinSessionId;
    private static DelegateBridge __Hotfix_get_Mode;
    private static DelegateBridge __Hotfix_set_Mode;
    private static DelegateBridge __Hotfix_get_DanDiff;
    private static DelegateBridge __Hotfix_set_DanDiff;
    private static DelegateBridge __Hotfix_get_LocalRankDiff;
    private static DelegateBridge __Hotfix_set_LocalRankDiff;
    private static DelegateBridge __Hotfix_get_GlobalRankDiff;
    private static DelegateBridge __Hotfix_set_GlobalRankDiff;
    private static DelegateBridge __Hotfix_get_IsPromotion;
    private static DelegateBridge __Hotfix_set_IsPromotion;
    private static DelegateBridge __Hotfix_get_Report;
    private static DelegateBridge __Hotfix_set_Report;
    private static DelegateBridge __Hotfix_get_BattleInstanceId;
    private static DelegateBridge __Hotfix_set_BattleInstanceId;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomRealTimePVPBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinSessionId")]
    public ulong WinSessionId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiff")]
    public int DanDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LocalRankDiff")]
    public int LocalRankDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GlobalRankDiff")]
    public int GlobalRankDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsPromotion")]
    public bool IsPromotion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "Report")]
    public ProRealTimePVPBattleReport Report
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleInstanceId")]
    public ulong BattleInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
