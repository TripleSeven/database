﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProMail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProMail")]
  [Serializable]
  public class ProMail : IExtensible
  {
    private int _TemplateId;
    private ulong _InstanceId;
    private int _Status;
    private long _SendTime;
    private long _ReadedOrGotAttachmentTime;
    private string _Title;
    private string _Content;
    private readonly List<ProGoods> _Attachments;
    private uint _ExpiredTime;
    private int _ReadedExpiredTime;
    private bool _GotDeleted;
    private int _MailTypeId;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_TemplateId;
    private static DelegateBridge __Hotfix_set_TemplateId;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_Status;
    private static DelegateBridge __Hotfix_set_Status;
    private static DelegateBridge __Hotfix_get_SendTime;
    private static DelegateBridge __Hotfix_set_SendTime;
    private static DelegateBridge __Hotfix_get_ReadedOrGotAttachmentTime;
    private static DelegateBridge __Hotfix_set_ReadedOrGotAttachmentTime;
    private static DelegateBridge __Hotfix_get_Title;
    private static DelegateBridge __Hotfix_set_Title;
    private static DelegateBridge __Hotfix_get_Content;
    private static DelegateBridge __Hotfix_set_Content;
    private static DelegateBridge __Hotfix_get_Attachments;
    private static DelegateBridge __Hotfix_get_ExpiredTime;
    private static DelegateBridge __Hotfix_set_ExpiredTime;
    private static DelegateBridge __Hotfix_get_ReadedExpiredTime;
    private static DelegateBridge __Hotfix_set_ReadedExpiredTime;
    private static DelegateBridge __Hotfix_get_GotDeleted;
    private static DelegateBridge __Hotfix_set_GotDeleted;
    private static DelegateBridge __Hotfix_get_MailTypeId;
    private static DelegateBridge __Hotfix_set_MailTypeId;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProMail()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TemplateId")]
    public int TemplateId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InstanceId")]
    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Status")]
    public int Status
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SendTime")]
    public long SendTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadedOrGotAttachmentTime")]
    public long ReadedOrGotAttachmentTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "Title")]
    [DefaultValue("")]
    public string Title
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "Content")]
    public string Content
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "Attachments")]
    public List<ProGoods> Attachments
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ExpiredTime")]
    [DefaultValue(0)]
    public uint ExpiredTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ReadedExpiredTime")]
    [DefaultValue(0)]
    public int ReadedExpiredTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(false)]
    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = false, Name = "GotDeleted")]
    public bool GotDeleted
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MailTypeId")]
    public int MailTypeId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
