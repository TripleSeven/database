﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.RedeemInfoAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "RedeemInfoAck")]
  [Serializable]
  public class RedeemInfoAck : IExtensible
  {
    private int _TotalRecharge;
    private readonly List<RedeemCrystalInfo> _Crystals;
    private RedeemBundleInfo _Bundle;
    private bool _Claimed;
    private long _StartDate;
    private long _EndDate;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_TotalRecharge;
    private static DelegateBridge __Hotfix_set_TotalRecharge;
    private static DelegateBridge __Hotfix_get_Crystals;
    private static DelegateBridge __Hotfix_get_Bundle;
    private static DelegateBridge __Hotfix_set_Bundle;
    private static DelegateBridge __Hotfix_get_Claimed;
    private static DelegateBridge __Hotfix_set_Claimed;
    private static DelegateBridge __Hotfix_get_StartDate;
    private static DelegateBridge __Hotfix_set_StartDate;
    private static DelegateBridge __Hotfix_get_EndDate;
    private static DelegateBridge __Hotfix_set_EndDate;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public RedeemInfoAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "TotalRecharge")]
    public int TotalRecharge
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Crystals")]
    public List<RedeemCrystalInfo> Crystals
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Bundle")]
    public RedeemBundleInfo Bundle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "Claimed")]
    public bool Claimed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartDate")]
    public long StartDate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "EndDate")]
    public long EndDate
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
