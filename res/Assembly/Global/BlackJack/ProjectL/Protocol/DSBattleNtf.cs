﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSBattleNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSBattleNtf")]
  [Serializable]
  public class DSBattleNtf : IExtensible
  {
    private uint _Version;
    private readonly List<ProTeam> _Teams;
    private ProBattleProcessing _ProcessingBattleInfo;
    private readonly List<int> _GotBattleTreasureIds;
    private int _ArenaBattleStatus;
    private int _ArenaBattleId;
    private int _ArenaBattleRandomSeed;
    private ulong _BattleRoomId;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_Teams;
    private static DelegateBridge __Hotfix_get_ProcessingBattleInfo;
    private static DelegateBridge __Hotfix_set_ProcessingBattleInfo;
    private static DelegateBridge __Hotfix_get_GotBattleTreasureIds;
    private static DelegateBridge __Hotfix_get_ArenaBattleStatus;
    private static DelegateBridge __Hotfix_set_ArenaBattleStatus;
    private static DelegateBridge __Hotfix_get_ArenaBattleId;
    private static DelegateBridge __Hotfix_set_ArenaBattleId;
    private static DelegateBridge __Hotfix_get_ArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_set_ArenaBattleRandomSeed;
    private static DelegateBridge __Hotfix_get_BattleRoomId;
    private static DelegateBridge __Hotfix_set_BattleRoomId;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSBattleNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Teams")]
    public List<ProTeam> Teams
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "ProcessingBattleInfo")]
    public ProBattleProcessing ProcessingBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "GotBattleTreasureIds")]
    public List<int> GotBattleTreasureIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaBattleStatus")]
    [DefaultValue(0)]
    public int ArenaBattleStatus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaBattleId")]
    [DefaultValue(0)]
    public int ArenaBattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "ArenaBattleRandomSeed")]
    [DefaultValue(0)]
    public int ArenaBattleRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleRoomId")]
    public ulong BattleRoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
