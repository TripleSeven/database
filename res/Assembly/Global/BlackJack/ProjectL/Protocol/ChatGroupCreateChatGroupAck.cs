﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ChatGroupCreateChatGroupAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ChatGroupCreateChatGroupAck")]
  [Serializable]
  public class ChatGroupCreateChatGroupAck : IExtensible
  {
    private int _Result;
    private ProChatUserInfo _FailedUser;
    private ChatGroupCreateChatGroupReq _Req;
    private ProChatGroupInfo _ChatGroup;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_FailedUser;
    private static DelegateBridge __Hotfix_set_FailedUser;
    private static DelegateBridge __Hotfix_get_Req;
    private static DelegateBridge __Hotfix_set_Req;
    private static DelegateBridge __Hotfix_get_ChatGroup;
    private static DelegateBridge __Hotfix_set_ChatGroup;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCreateChatGroupAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "FailedUser")]
    public ProChatUserInfo FailedUser
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "Req")]
    public ChatGroupCreateChatGroupReq Req
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatGroup")]
    public ProChatGroupInfo ChatGroup
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
