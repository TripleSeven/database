﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.UnchartedLevelRaidSurplusTimesNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "UnchartedLevelRaidSurplusTimesNtf")]
  [Serializable]
  public class UnchartedLevelRaidSurplusTimesNtf : IExtensible
  {
    private int _SurplusTimes;
    private bool _IsAddTimes;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_SurplusTimes;
    private static DelegateBridge __Hotfix_set_SurplusTimes;
    private static DelegateBridge __Hotfix_get_IsAddTimes;
    private static DelegateBridge __Hotfix_set_IsAddTimes;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public UnchartedLevelRaidSurplusTimesNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SurplusTimes")]
    public int SurplusTimes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsAddTimes")]
    public bool IsAddTimes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
