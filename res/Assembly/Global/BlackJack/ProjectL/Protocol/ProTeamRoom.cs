﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProTeamRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProTeamRoom")]
  [Serializable]
  public class ProTeamRoom : IExtensible
  {
    private int _RoomId;
    private int _LeaderPosition;
    private readonly List<ProTeamRoomPlayer> _Players;
    private long _LeaderKickOutTime;
    private ProTeamRoomSetting _Setting;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_set_RoomId;
    private static DelegateBridge __Hotfix_get_LeaderPosition;
    private static DelegateBridge __Hotfix_set_LeaderPosition;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_get_LeaderKickOutTime;
    private static DelegateBridge __Hotfix_set_LeaderKickOutTime;
    private static DelegateBridge __Hotfix_get_Setting;
    private static DelegateBridge __Hotfix_set_Setting;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public int RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderPosition")]
    public int LeaderPosition
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProTeamRoomPlayer> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LeaderKickOutTime")]
    public long LeaderKickOutTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Setting")]
    public ProTeamRoomSetting Setting
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
