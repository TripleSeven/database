﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomPeakArenaBattleFinishNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomPeakArenaBattleFinishNtf")]
  [Serializable]
  public class BattleRoomPeakArenaBattleFinishNtf : IExtensible
  {
    private int _WinPlayerIndex;
    private int _Mode;
    private int _DanDiff;
    private int _RankDiff;
    private ulong _RoomId;
    private int _NewScore;
    private ProPeakArenaMatchStats _FriendlyMatchStats;
    private ProPeakArenaMatchStats _LadderMatchStats;
    private ProPeakArenaMatchStats _FriendlyCareerMatchStats;
    private ProPeakArenaMatchStats _LadderCareerMatchStats;
    private int _ScoreDiff;
    private bool _DanProtectionTriggered;
    private bool _DanGroupProtectionTriggered;
    private ulong _InstanceId;
    private ProCommonBattleReport _BattleReport;
    private ProPeakArenaBattleReportNameRecord _Record;
    private readonly List<int> _MinePickedHeroes;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_WinPlayerIndex;
    private static DelegateBridge __Hotfix_set_WinPlayerIndex;
    private static DelegateBridge __Hotfix_get_Mode;
    private static DelegateBridge __Hotfix_set_Mode;
    private static DelegateBridge __Hotfix_get_DanDiff;
    private static DelegateBridge __Hotfix_set_DanDiff;
    private static DelegateBridge __Hotfix_get_RankDiff;
    private static DelegateBridge __Hotfix_set_RankDiff;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_set_RoomId;
    private static DelegateBridge __Hotfix_get_NewScore;
    private static DelegateBridge __Hotfix_set_NewScore;
    private static DelegateBridge __Hotfix_get_FriendlyMatchStats;
    private static DelegateBridge __Hotfix_set_FriendlyMatchStats;
    private static DelegateBridge __Hotfix_get_LadderMatchStats;
    private static DelegateBridge __Hotfix_set_LadderMatchStats;
    private static DelegateBridge __Hotfix_get_FriendlyCareerMatchStats;
    private static DelegateBridge __Hotfix_set_FriendlyCareerMatchStats;
    private static DelegateBridge __Hotfix_get_LadderCareerMatchStats;
    private static DelegateBridge __Hotfix_set_LadderCareerMatchStats;
    private static DelegateBridge __Hotfix_get_ScoreDiff;
    private static DelegateBridge __Hotfix_set_ScoreDiff;
    private static DelegateBridge __Hotfix_get_DanProtectionTriggered;
    private static DelegateBridge __Hotfix_set_DanProtectionTriggered;
    private static DelegateBridge __Hotfix_get_DanGroupProtectionTriggered;
    private static DelegateBridge __Hotfix_set_DanGroupProtectionTriggered;
    private static DelegateBridge __Hotfix_get_InstanceId;
    private static DelegateBridge __Hotfix_set_InstanceId;
    private static DelegateBridge __Hotfix_get_BattleReport;
    private static DelegateBridge __Hotfix_set_BattleReport;
    private static DelegateBridge __Hotfix_get_Record;
    private static DelegateBridge __Hotfix_set_Record;
    private static DelegateBridge __Hotfix_get_MinePickedHeroes;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPeakArenaBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "WinPlayerIndex")]
    public int WinPlayerIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "DanDiff")]
    public int DanDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RankDiff")]
    public int RankDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public ulong RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NewScore")]
    public int NewScore
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyMatchStats")]
    public ProPeakArenaMatchStats FriendlyMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderMatchStats")]
    public ProPeakArenaMatchStats LadderMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyCareerMatchStats")]
    public ProPeakArenaMatchStats FriendlyCareerMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderCareerMatchStats")]
    public ProPeakArenaMatchStats LadderCareerMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ScoreDiff")]
    public int ScoreDiff
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.Default, IsRequired = true, Name = "DanProtectionTriggered")]
    public bool DanProtectionTriggered
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, IsRequired = true, Name = "DanGroupProtectionTriggered")]
    public bool DanGroupProtectionTriggered
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "InstanceId")]
    public ulong InstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleReport")]
    [DefaultValue(null)]
    public ProCommonBattleReport BattleReport
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.Default, IsRequired = false, Name = "Record")]
    [DefaultValue(null)]
    public ProPeakArenaBattleReportNameRecord Record
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.TwosComplement, Name = "MinePickedHeroes")]
    public List<int> MinePickedHeroes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
