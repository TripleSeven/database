﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.PeakArenaPlayOffInfoGetAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "PeakArenaPlayOffInfoGetAck")]
  [Serializable]
  public class PeakArenaPlayOffInfoGetAck : IExtensible
  {
    private PeakArenaPlayOffInfoGetReq _Req;
    private ProPeakArenaSeasonInfo _Info;
    private readonly List<ProPeakArenaLiveRoomInfo> _LiveRoomInfo;
    private int _StartMatchIndex;
    private int _NextMatchIndex;
    private int _Result;
    private long _Version;
    private long _LiveRoomVersion;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Req;
    private static DelegateBridge __Hotfix_set_Req;
    private static DelegateBridge __Hotfix_get_Info;
    private static DelegateBridge __Hotfix_set_Info;
    private static DelegateBridge __Hotfix_get_LiveRoomInfo;
    private static DelegateBridge __Hotfix_get_StartMatchIndex;
    private static DelegateBridge __Hotfix_set_StartMatchIndex;
    private static DelegateBridge __Hotfix_get_NextMatchIndex;
    private static DelegateBridge __Hotfix_set_NextMatchIndex;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_LiveRoomVersion;
    private static DelegateBridge __Hotfix_set_LiveRoomVersion;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffInfoGetAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Req")]
    public PeakArenaPlayOffInfoGetReq Req
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Info")]
    public ProPeakArenaSeasonInfo Info
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "LiveRoomInfo")]
    public List<ProPeakArenaLiveRoomInfo> LiveRoomInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StartMatchIndex")]
    public int StartMatchIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextMatchIndex")]
    public int NextMatchIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public long Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "LiveRoomVersion")]
    public long LiveRoomVersion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
