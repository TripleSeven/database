﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.GuildCreateReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "GuildCreateReq")]
  [Serializable]
  public class GuildCreateReq : IExtensible
  {
    private string _Name;
    private string _HiringDeclaration;
    private bool _AutoJoin;
    private int _JoinLevel;
    private string _Announcement;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Name;
    private static DelegateBridge __Hotfix_set_Name;
    private static DelegateBridge __Hotfix_get_HiringDeclaration;
    private static DelegateBridge __Hotfix_set_HiringDeclaration;
    private static DelegateBridge __Hotfix_get_AutoJoin;
    private static DelegateBridge __Hotfix_set_AutoJoin;
    private static DelegateBridge __Hotfix_get_JoinLevel;
    private static DelegateBridge __Hotfix_set_JoinLevel;
    private static DelegateBridge __Hotfix_get_Announcement;
    private static DelegateBridge __Hotfix_set_Announcement;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public GuildCreateReq()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "Name")]
    public string Name
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = false, Name = "HiringDeclaration")]
    [DefaultValue("")]
    public string HiringDeclaration
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "AutoJoin")]
    public bool AutoJoin
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "JoinLevel")]
    public int JoinLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "Announcement")]
    public string Announcement
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
