﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProAccumulateLoginOperationalActivity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProAccumulateLoginOperationalActivity")]
  [Serializable]
  public class ProAccumulateLoginOperationalActivity : IExtensible
  {
    private ProOperationalActivityBasicInfo _BasicInfo;
    private readonly List<long> _SpecificLoginTimes;
    private readonly List<int> _GainRewardIndexs;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_BasicInfo;
    private static DelegateBridge __Hotfix_set_BasicInfo;
    private static DelegateBridge __Hotfix_get_SpecificLoginTimes;
    private static DelegateBridge __Hotfix_get_GainRewardIndexs;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProAccumulateLoginOperationalActivity()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "BasicInfo")]
    public ProOperationalActivityBasicInfo BasicInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "SpecificLoginTimes")]
    public List<long> SpecificLoginTimes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "GainRewardIndexs")]
    public List<int> GainRewardIndexs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
