﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomGuildMassiveCombatBattleJoinNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomGuildMassiveCombatBattleJoinNtf")]
  [Serializable]
  public class BattleRoomGuildMassiveCombatBattleJoinNtf : IExtensible
  {
    private ulong _RoomId;
    private int _BattleId;
    private readonly List<ProBattleRoomPlayer> _Players;
    private int _RandomSeed;
    private long _ReadyTimeOut;
    private int _GuildMassiveCombatRandomSeed;
    private readonly List<int> _PreferredHeroTagIds;
    private ulong _GuildMassiveCombatInstanceId;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_RoomId;
    private static DelegateBridge __Hotfix_set_RoomId;
    private static DelegateBridge __Hotfix_get_BattleId;
    private static DelegateBridge __Hotfix_set_BattleId;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_get_RandomSeed;
    private static DelegateBridge __Hotfix_set_RandomSeed;
    private static DelegateBridge __Hotfix_get_ReadyTimeOut;
    private static DelegateBridge __Hotfix_set_ReadyTimeOut;
    private static DelegateBridge __Hotfix_get_GuildMassiveCombatRandomSeed;
    private static DelegateBridge __Hotfix_set_GuildMassiveCombatRandomSeed;
    private static DelegateBridge __Hotfix_get_PreferredHeroTagIds;
    private static DelegateBridge __Hotfix_get_GuildMassiveCombatInstanceId;
    private static DelegateBridge __Hotfix_set_GuildMassiveCombatInstanceId;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomGuildMassiveCombatBattleJoinNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomId")]
    public ulong RoomId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleId")]
    public int BattleId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProBattleRoomPlayer> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RandomSeed")]
    public int RandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ReadyTimeOut")]
    public long ReadyTimeOut
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuildMassiveCombatRandomSeed")]
    public int GuildMassiveCombatRandomSeed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "PreferredHeroTagIds")]
    public List<int> PreferredHeroTagIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GuildMassiveCombatInstanceId")]
    public ulong GuildMassiveCombatInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
