﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaPlayOffMatchupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArenaPlayOffMatchupInfo")]
  [Serializable]
  public class ProPeakArenaPlayOffMatchupInfo : IExtensible
  {
    private int _Round;
    private int _MatchupId;
    private ProPeakArenaPlayOffPlayerInfo _PlayerOnLeft;
    private ProPeakArenaPlayOffPlayerInfo _PlayerOnRight;
    private string _WinnerId;
    private readonly List<ProPeakArenaPlayOffBattleInfo> _BattleInfos;
    private ulong _BattleReportId;
    private int _NextMatchupId;
    private int _PrevLeftMatchupId;
    private int _PrevRightMatchupId;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Round;
    private static DelegateBridge __Hotfix_set_Round;
    private static DelegateBridge __Hotfix_get_MatchupId;
    private static DelegateBridge __Hotfix_set_MatchupId;
    private static DelegateBridge __Hotfix_get_PlayerOnLeft;
    private static DelegateBridge __Hotfix_set_PlayerOnLeft;
    private static DelegateBridge __Hotfix_get_PlayerOnRight;
    private static DelegateBridge __Hotfix_set_PlayerOnRight;
    private static DelegateBridge __Hotfix_get_WinnerId;
    private static DelegateBridge __Hotfix_set_WinnerId;
    private static DelegateBridge __Hotfix_get_BattleInfos;
    private static DelegateBridge __Hotfix_get_BattleReportId;
    private static DelegateBridge __Hotfix_set_BattleReportId;
    private static DelegateBridge __Hotfix_get_NextMatchupId;
    private static DelegateBridge __Hotfix_set_NextMatchupId;
    private static DelegateBridge __Hotfix_get_PrevLeftMatchupId;
    private static DelegateBridge __Hotfix_set_PrevLeftMatchupId;
    private static DelegateBridge __Hotfix_get_PrevRightMatchupId;
    private static DelegateBridge __Hotfix_set_PrevRightMatchupId;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaPlayOffMatchupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "MatchupId")]
    public int MatchupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayerOnLeft")]
    public ProPeakArenaPlayOffPlayerInfo PlayerOnLeft
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "PlayerOnRight")]
    public ProPeakArenaPlayOffPlayerInfo PlayerOnRight
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "WinnerId")]
    public string WinnerId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "BattleInfos")]
    public List<ProPeakArenaPlayOffBattleInfo> BattleInfos
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleReportId")]
    public ulong BattleReportId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextMatchupId")]
    public int NextMatchupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PrevLeftMatchupId")]
    public int PrevLeftMatchupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "PrevRightMatchupId")]
    public int PrevRightMatchupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
