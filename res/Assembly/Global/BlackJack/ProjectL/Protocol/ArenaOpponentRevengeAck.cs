﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ArenaOpponentRevengeAck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ArenaOpponentRevengeAck")]
  [Serializable]
  public class ArenaOpponentRevengeAck : IExtensible
  {
    private int _Result;
    private ulong _BattleReportInstanceId;
    private bool _AutoBattle;
    private ProArenaDefensiveBattleInfo _OpponentDefensiveBattleInfo;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Result;
    private static DelegateBridge __Hotfix_set_Result;
    private static DelegateBridge __Hotfix_get_BattleReportInstanceId;
    private static DelegateBridge __Hotfix_set_BattleReportInstanceId;
    private static DelegateBridge __Hotfix_get_AutoBattle;
    private static DelegateBridge __Hotfix_set_AutoBattle;
    private static DelegateBridge __Hotfix_get_OpponentDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_set_OpponentDefensiveBattleInfo;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaOpponentRevengeAck()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Result")]
    public int Result
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleReportInstanceId")]
    public ulong BattleReportInstanceId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = true, Name = "AutoBattle")]
    public bool AutoBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "OpponentDefensiveBattleInfo")]
    public ProArenaDefensiveBattleInfo OpponentDefensiveBattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
