﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProRealTimePVP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProRealTimePVP")]
  [Serializable]
  public class ProRealTimePVP : IExtensible
  {
    private bool _IsPromotion;
    private readonly List<ProRealTimePVPBattleReport> _PromotionReports;
    private readonly List<ProRealTimePVPBattleReport> _Reports;
    private ProRealTimePVPMatchStats _FriendlyMatchStats;
    private ProRealTimePVPMatchStats _LadderMatchStats;
    private ProRealTimePVPMatchStats _FriendlyCareerMatchStats;
    private ProRealTimePVPMatchStats _LadderCareerMatchStats;
    private readonly List<int> _WinsBonusIdAcquired;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_IsPromotion;
    private static DelegateBridge __Hotfix_set_IsPromotion;
    private static DelegateBridge __Hotfix_get_PromotionReports;
    private static DelegateBridge __Hotfix_get_Reports;
    private static DelegateBridge __Hotfix_get_FriendlyMatchStats;
    private static DelegateBridge __Hotfix_set_FriendlyMatchStats;
    private static DelegateBridge __Hotfix_get_LadderMatchStats;
    private static DelegateBridge __Hotfix_set_LadderMatchStats;
    private static DelegateBridge __Hotfix_get_FriendlyCareerMatchStats;
    private static DelegateBridge __Hotfix_set_FriendlyCareerMatchStats;
    private static DelegateBridge __Hotfix_get_LadderCareerMatchStats;
    private static DelegateBridge __Hotfix_set_LadderCareerMatchStats;
    private static DelegateBridge __Hotfix_get_WinsBonusIdAcquired;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProRealTimePVP()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "IsPromotion")]
    public bool IsPromotion
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "PromotionReports")]
    public List<ProRealTimePVPBattleReport> PromotionReports
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Reports")]
    public List<ProRealTimePVPBattleReport> Reports
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyMatchStats")]
    public ProRealTimePVPMatchStats FriendlyMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderMatchStats")]
    public ProRealTimePVPMatchStats LadderMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "FriendlyCareerMatchStats")]
    public ProRealTimePVPMatchStats FriendlyCareerMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "LadderCareerMatchStats")]
    public ProRealTimePVPMatchStats LadderCareerMatchStats
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, Name = "WinsBonusIdAcquired")]
    public List<int> WinsBonusIdAcquired
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
