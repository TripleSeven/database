﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.BattleRoomTeamBattleFinishNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "BattleRoomTeamBattleFinishNtf")]
  [Serializable]
  public class BattleRoomTeamBattleFinishNtf : IExtensible
  {
    private int _Stars;
    private ProChangedGoodsNtf _Ntf;
    private readonly List<ProGoods> _NormalRewards;
    private readonly List<ProGoods> _DailyRewards;
    private readonly List<ProGoods> _TeamRewards1;
    private readonly List<ProGoods> _TeamRewards2;
    private readonly List<ProGoods> _FriendRewards;
    private int _FriendshipPoints;
    private readonly List<ProGoods> _UnchartedScoreRewards;
    private readonly List<ProGoods> _AdditionalRewards;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Stars;
    private static DelegateBridge __Hotfix_set_Stars;
    private static DelegateBridge __Hotfix_get_Ntf;
    private static DelegateBridge __Hotfix_set_Ntf;
    private static DelegateBridge __Hotfix_get_NormalRewards;
    private static DelegateBridge __Hotfix_get_DailyRewards;
    private static DelegateBridge __Hotfix_get_TeamRewards1;
    private static DelegateBridge __Hotfix_get_TeamRewards2;
    private static DelegateBridge __Hotfix_get_FriendRewards;
    private static DelegateBridge __Hotfix_get_FriendshipPoints;
    private static DelegateBridge __Hotfix_set_FriendshipPoints;
    private static DelegateBridge __Hotfix_get_UnchartedScoreRewards;
    private static DelegateBridge __Hotfix_get_AdditionalRewards;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomTeamBattleFinishNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Stars")]
    public int Stars
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "NormalRewards")]
    public List<ProGoods> NormalRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "DailyRewards")]
    public List<ProGoods> DailyRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "TeamRewards1")]
    public List<ProGoods> TeamRewards1
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "TeamRewards2")]
    public List<ProGoods> TeamRewards2
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "FriendRewards")]
    public List<ProGoods> FriendRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPoints")]
    public int FriendshipPoints
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "UnchartedScoreRewards")]
    public List<ProGoods> UnchartedScoreRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.Default, Name = "AdditionalRewards")]
    public List<ProGoods> AdditionalRewards
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
