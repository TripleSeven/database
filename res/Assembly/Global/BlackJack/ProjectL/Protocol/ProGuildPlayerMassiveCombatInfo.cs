﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProGuildPlayerMassiveCombatInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProGuildPlayerMassiveCombatInfo")]
  [Serializable]
  public class ProGuildPlayerMassiveCombatInfo : IExtensible
  {
    private string _BindedGuildId;
    private readonly List<int> _UsedHeroIds;
    private int _Points;
    private readonly List<int> _PointsRewardsClaimed;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_BindedGuildId;
    private static DelegateBridge __Hotfix_set_BindedGuildId;
    private static DelegateBridge __Hotfix_get_UsedHeroIds;
    private static DelegateBridge __Hotfix_get_Points;
    private static DelegateBridge __Hotfix_set_Points;
    private static DelegateBridge __Hotfix_get_PointsRewardsClaimed;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProGuildPlayerMassiveCombatInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.Default, IsRequired = true, Name = "BindedGuildId")]
    public string BindedGuildId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, Name = "UsedHeroIds")]
    public List<int> UsedHeroIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Points")]
    public int Points
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, Name = "PointsRewardsClaimed")]
    public List<int> PointsRewardsClaimed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
