﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSFriendNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSFriendNtf")]
  [Serializable]
  public class DSFriendNtf : IExtensible
  {
    private int _Likes;
    private readonly List<string> _LikedUsers;
    private readonly List<string> _FriendshipPointsSent;
    private readonly List<string> _FriendshipPointsReceived;
    private int _FriendshipPointsFromFightWithFriendsToday;
    private int _FriendshipPointsClaimedToday;
    private ProBusinessCardInfoSet _SetInfo;
    private uint _Version;
    private long _BannedTime;
    private readonly List<ulong> _HeroicMomentBattleReports;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Likes;
    private static DelegateBridge __Hotfix_set_Likes;
    private static DelegateBridge __Hotfix_get_LikedUsers;
    private static DelegateBridge __Hotfix_get_FriendshipPointsSent;
    private static DelegateBridge __Hotfix_get_FriendshipPointsReceived;
    private static DelegateBridge __Hotfix_get_FriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_set_FriendshipPointsFromFightWithFriendsToday;
    private static DelegateBridge __Hotfix_get_FriendshipPointsClaimedToday;
    private static DelegateBridge __Hotfix_set_FriendshipPointsClaimedToday;
    private static DelegateBridge __Hotfix_get_SetInfo;
    private static DelegateBridge __Hotfix_set_SetInfo;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_BannedTime;
    private static DelegateBridge __Hotfix_set_BannedTime;
    private static DelegateBridge __Hotfix_get_HeroicMomentBattleReports;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSFriendNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Likes")]
    public int Likes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "LikedUsers")]
    public List<string> LikedUsers
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "FriendshipPointsSent")]
    public List<string> FriendshipPointsSent
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "FriendshipPointsReceived")]
    public List<string> FriendshipPointsReceived
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPointsFromFightWithFriendsToday")]
    public int FriendshipPointsFromFightWithFriendsToday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FriendshipPointsClaimedToday")]
    public int FriendshipPointsClaimedToday
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = true, Name = "SetInfo")]
    public ProBusinessCardInfoSet SetInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "BannedTime")]
    [DefaultValue(0)]
    public long BannedTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, Name = "HeroicMomentBattleReports")]
    public List<ulong> HeroicMomentBattleReports
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
