﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProPeakArenaBattleLiveRoomInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProPeakArenaBattleLiveRoomInfo")]
  [Serializable]
  public class ProPeakArenaBattleLiveRoomInfo : IExtensible
  {
    private long _BORoundStartTime;
    private int _RoomStatus;
    private readonly List<int> _WinPlayerIndexes;
    private readonly List<ProUserSummary> _Players;
    private ProPeakArenaBattleReportBattleInfo _BattleInfo;
    private ProBattleSyncCommand _BattleCommand;
    private ProBPStageSyncCommand _BPCommand;
    private int _Mode;
    private int _Round;
    private int _BORound;
    private readonly List<string> _FirstHandUserIds;
    private long _BattleStartTime;
    private long _BattleStartTurnActionTimeSpan;
    private readonly List<long> _BattleStartPublicTimeSpan;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_BORoundStartTime;
    private static DelegateBridge __Hotfix_set_BORoundStartTime;
    private static DelegateBridge __Hotfix_get_RoomStatus;
    private static DelegateBridge __Hotfix_set_RoomStatus;
    private static DelegateBridge __Hotfix_get_WinPlayerIndexes;
    private static DelegateBridge __Hotfix_get_Players;
    private static DelegateBridge __Hotfix_get_BattleInfo;
    private static DelegateBridge __Hotfix_set_BattleInfo;
    private static DelegateBridge __Hotfix_get_BattleCommand;
    private static DelegateBridge __Hotfix_set_BattleCommand;
    private static DelegateBridge __Hotfix_get_BPCommand;
    private static DelegateBridge __Hotfix_set_BPCommand;
    private static DelegateBridge __Hotfix_get_Mode;
    private static DelegateBridge __Hotfix_set_Mode;
    private static DelegateBridge __Hotfix_get_Round;
    private static DelegateBridge __Hotfix_set_Round;
    private static DelegateBridge __Hotfix_get_BORound;
    private static DelegateBridge __Hotfix_set_BORound;
    private static DelegateBridge __Hotfix_get_FirstHandUserIds;
    private static DelegateBridge __Hotfix_get_BattleStartTime;
    private static DelegateBridge __Hotfix_set_BattleStartTime;
    private static DelegateBridge __Hotfix_get_BattleStartTurnActionTimeSpan;
    private static DelegateBridge __Hotfix_set_BattleStartTurnActionTimeSpan;
    private static DelegateBridge __Hotfix_get_BattleStartPublicTimeSpan;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProPeakArenaBattleLiveRoomInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BORoundStartTime")]
    public long BORoundStartTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "RoomStatus")]
    public int RoomStatus
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, Name = "WinPlayerIndexes")]
    public List<int> WinPlayerIndexes
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Players")]
    public List<ProUserSummary> Players
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleInfo")]
    [DefaultValue(null)]
    public ProPeakArenaBattleReportBattleInfo BattleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "BattleCommand")]
    [DefaultValue(null)]
    public ProBattleSyncCommand BattleCommand
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "BPCommand")]
    [DefaultValue(null)]
    public ProBPStageSyncCommand BPCommand
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Mode")]
    public int Mode
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Round")]
    public int Round
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BORound")]
    public int BORound
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.Default, Name = "FirstHandUserIds")]
    public List<string> FirstHandUserIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleStartTime")]
    public long BattleStartTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BattleStartTurnActionTimeSpan")]
    public long BattleStartTurnActionTimeSpan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.TwosComplement, Name = "BattleStartPublicTimeSpan")]
    public List<long> BattleStartPublicTimeSpan
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
