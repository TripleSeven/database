﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ProHero
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ProHero")]
  [Serializable]
  public class ProHero : IExtensible
  {
    private int _HeroId;
    private int _Level;
    private int _Exp;
    private int _StarLevel;
    private int _ActiveJobRelatedId;
    private readonly List<ProHeroJob> _Jobs;
    private readonly List<int> _SelectedSkills;
    private int _SelectedSoldierId;
    private readonly List<int> _UnlockedJobs;
    private int _FightNums;
    private readonly List<ulong> _EquipmentIds;
    private int _FavorabilityLevel;
    private int _FavorabilityExp;
    private readonly List<ProHeroFetter> _Fetters;
    private bool _Confessed;
    private int _Power;
    private int _CharSkinId;
    private readonly List<ProSoldierSkin> _SoldierSkins;
    private int _HeartFetterLevel;
    private long _HeroHeartFetterUnlockTime;
    private long _HeroHeartFetterLevelMaxTime;
    private bool _Mine;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_HeroId;
    private static DelegateBridge __Hotfix_set_HeroId;
    private static DelegateBridge __Hotfix_get_Level;
    private static DelegateBridge __Hotfix_set_Level;
    private static DelegateBridge __Hotfix_get_Exp;
    private static DelegateBridge __Hotfix_set_Exp;
    private static DelegateBridge __Hotfix_get_StarLevel;
    private static DelegateBridge __Hotfix_set_StarLevel;
    private static DelegateBridge __Hotfix_get_ActiveJobRelatedId;
    private static DelegateBridge __Hotfix_set_ActiveJobRelatedId;
    private static DelegateBridge __Hotfix_get_Jobs;
    private static DelegateBridge __Hotfix_get_SelectedSkills;
    private static DelegateBridge __Hotfix_get_SelectedSoldierId;
    private static DelegateBridge __Hotfix_set_SelectedSoldierId;
    private static DelegateBridge __Hotfix_get_UnlockedJobs;
    private static DelegateBridge __Hotfix_get_FightNums;
    private static DelegateBridge __Hotfix_set_FightNums;
    private static DelegateBridge __Hotfix_get_EquipmentIds;
    private static DelegateBridge __Hotfix_get_FavorabilityLevel;
    private static DelegateBridge __Hotfix_set_FavorabilityLevel;
    private static DelegateBridge __Hotfix_get_FavorabilityExp;
    private static DelegateBridge __Hotfix_set_FavorabilityExp;
    private static DelegateBridge __Hotfix_get_Fetters;
    private static DelegateBridge __Hotfix_get_Confessed;
    private static DelegateBridge __Hotfix_set_Confessed;
    private static DelegateBridge __Hotfix_get_Power;
    private static DelegateBridge __Hotfix_set_Power;
    private static DelegateBridge __Hotfix_get_CharSkinId;
    private static DelegateBridge __Hotfix_set_CharSkinId;
    private static DelegateBridge __Hotfix_get_SoldierSkins;
    private static DelegateBridge __Hotfix_get_HeartFetterLevel;
    private static DelegateBridge __Hotfix_set_HeartFetterLevel;
    private static DelegateBridge __Hotfix_get_HeroHeartFetterUnlockTime;
    private static DelegateBridge __Hotfix_set_HeroHeartFetterUnlockTime;
    private static DelegateBridge __Hotfix_get_HeroHeartFetterLevelMaxTime;
    private static DelegateBridge __Hotfix_set_HeroHeartFetterLevelMaxTime;
    private static DelegateBridge __Hotfix_get_Mine;
    private static DelegateBridge __Hotfix_set_Mine;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ProHero()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroId")]
    public int HeroId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Level")]
    public int Level
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Exp")]
    public int Exp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "StarLevel")]
    public int StarLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ActiveJobRelatedId")]
    public int ActiveJobRelatedId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "Jobs")]
    public List<ProHeroJob> Jobs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.TwosComplement, Name = "SelectedSkills")]
    public List<int> SelectedSkills
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "SelectedSoldierId")]
    public int SelectedSoldierId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.TwosComplement, Name = "UnlockedJobs")]
    public List<int> UnlockedJobs
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(10, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FightNums")]
    public int FightNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(11, DataFormat = DataFormat.TwosComplement, Name = "EquipmentIds")]
    public List<ulong> EquipmentIds
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(12, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavorabilityLevel")]
    public int FavorabilityLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(13, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "FavorabilityExp")]
    public int FavorabilityExp
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(14, DataFormat = DataFormat.Default, Name = "Fetters")]
    public List<ProHeroFetter> Fetters
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(15, DataFormat = DataFormat.Default, IsRequired = true, Name = "Confessed")]
    public bool Confessed
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(16, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Power")]
    public int Power
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(17, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "CharSkinId")]
    public int CharSkinId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(18, DataFormat = DataFormat.Default, Name = "SoldierSkins")]
    public List<ProSoldierSkin> SoldierSkins
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(19, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeartFetterLevel")]
    public int HeartFetterLevel
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(20, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroHeartFetterUnlockTime")]
    public long HeroHeartFetterUnlockTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(21, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "HeroHeartFetterLevelMaxTime")]
    public long HeroHeartFetterLevelMaxTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(22, DataFormat = DataFormat.Default, IsRequired = true, Name = "Mine")]
    public bool Mine
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
