﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.DSOperationalActivityNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "DSOperationalActivityNtf")]
  [Serializable]
  public class DSOperationalActivityNtf : IExtensible
  {
    private uint _Version;
    private readonly List<ProPlayerLevelUpOperationalActivity> _PlayerLevelUpAwardOperationalActivities;
    private readonly List<ProSpecificDaysLoginOperationalActivity> _SpecificDaysLoginAwardOperationalActivities;
    private readonly List<ProAccumulateLoginOperationalActivity> _AccumulateLoginAwardOperationalActivities;
    private readonly List<ProLimitedTimeExchangeOperationActivity> _LimitedTimeExchangeOperationActivities;
    private readonly List<ProEffectOperationActivity> _EffectOperationActivities;
    private readonly List<ProAccumulateRechargeOperationalActivity> _AccumulateRechargeOperationalActivities;
    private readonly List<ProAccumulateConsumeCrystalOperationalActivity> _AccumulateConsumeCrystalOperationalActivitites;
    private readonly List<ProEventOperationalActivity> _EventOperationalActivities;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Version;
    private static DelegateBridge __Hotfix_set_Version;
    private static DelegateBridge __Hotfix_get_PlayerLevelUpAwardOperationalActivities;
    private static DelegateBridge __Hotfix_get_SpecificDaysLoginAwardOperationalActivities;
    private static DelegateBridge __Hotfix_get_AccumulateLoginAwardOperationalActivities;
    private static DelegateBridge __Hotfix_get_LimitedTimeExchangeOperationActivities;
    private static DelegateBridge __Hotfix_get_EffectOperationActivities;
    private static DelegateBridge __Hotfix_get_AccumulateRechargeOperationalActivities;
    private static DelegateBridge __Hotfix_get_AccumulateConsumeCrystalOperationalActivitites;
    private static DelegateBridge __Hotfix_get_EventOperationalActivities;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public DSOperationalActivityNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Version")]
    public uint Version
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "PlayerLevelUpAwardOperationalActivities")]
    public List<ProPlayerLevelUpOperationalActivity> PlayerLevelUpAwardOperationalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "SpecificDaysLoginAwardOperationalActivities")]
    public List<ProSpecificDaysLoginOperationalActivity> SpecificDaysLoginAwardOperationalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "AccumulateLoginAwardOperationalActivities")]
    public List<ProAccumulateLoginOperationalActivity> AccumulateLoginAwardOperationalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "LimitedTimeExchangeOperationActivities")]
    public List<ProLimitedTimeExchangeOperationActivity> LimitedTimeExchangeOperationActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "EffectOperationActivities")]
    public List<ProEffectOperationActivity> EffectOperationActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "AccumulateRechargeOperationalActivities")]
    public List<ProAccumulateRechargeOperationalActivity> AccumulateRechargeOperationalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.Default, Name = "AccumulateConsumeCrystalOperationalActivitites")]
    public List<ProAccumulateConsumeCrystalOperationalActivity> AccumulateConsumeCrystalOperationalActivitites
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(9, DataFormat = DataFormat.Default, Name = "EventOperationalActivities")]
    public List<ProEventOperationalActivity> EventOperationalActivities
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
