﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.FriendSocialRelationUpdateNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "FriendSocialRelationUpdateNtf")]
  [Serializable]
  public class FriendSocialRelationUpdateNtf : IExtensible
  {
    private int _Flag;
    private readonly List<ProUserSummary> _Friends;
    private readonly List<ProUserSummary> _Blacklist;
    private readonly List<ProUserSummary> _Invite;
    private readonly List<ProUserSummary> _Invited;
    private readonly List<ProUserSummary> _RecentContactsChat;
    private readonly List<ProUserSummary> _RecentContactsTeamBattle;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_Flag;
    private static DelegateBridge __Hotfix_set_Flag;
    private static DelegateBridge __Hotfix_get_Friends;
    private static DelegateBridge __Hotfix_get_Blacklist;
    private static DelegateBridge __Hotfix_get_Invite;
    private static DelegateBridge __Hotfix_get_Invited;
    private static DelegateBridge __Hotfix_get_RecentContactsChat;
    private static DelegateBridge __Hotfix_get_RecentContactsTeamBattle;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public FriendSocialRelationUpdateNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "Flag")]
    public int Flag
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, Name = "Friends")]
    public List<ProUserSummary> Friends
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.Default, Name = "Blacklist")]
    public List<ProUserSummary> Blacklist
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.Default, Name = "Invite")]
    public List<ProUserSummary> Invite
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(5, DataFormat = DataFormat.Default, Name = "Invited")]
    public List<ProUserSummary> Invited
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, Name = "RecentContactsChat")]
    public List<ProUserSummary> RecentContactsChat
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, Name = "RecentContactsTeamBattle")]
    public List<ProUserSummary> RecentContactsTeamBattle
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
