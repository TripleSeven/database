﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.GiftStoreBuyGoodsNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "GiftStoreBuyGoodsNtf")]
  [Serializable]
  public class GiftStoreBuyGoodsNtf : IExtensible
  {
    private int _GoodsId;
    private string _GoodsRegisterId;
    private int _BoughtNums;
    private long _NextFlushTime;
    private ProChangedGoodsNtf _Ntf;
    private ProOrderReward _Reward;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_GoodsId;
    private static DelegateBridge __Hotfix_set_GoodsId;
    private static DelegateBridge __Hotfix_get_GoodsRegisterId;
    private static DelegateBridge __Hotfix_set_GoodsRegisterId;
    private static DelegateBridge __Hotfix_get_BoughtNums;
    private static DelegateBridge __Hotfix_set_BoughtNums;
    private static DelegateBridge __Hotfix_get_NextFlushTime;
    private static DelegateBridge __Hotfix_set_NextFlushTime;
    private static DelegateBridge __Hotfix_get_Ntf;
    private static DelegateBridge __Hotfix_set_Ntf;
    private static DelegateBridge __Hotfix_get_Reward;
    private static DelegateBridge __Hotfix_set_Reward;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public GiftStoreBuyGoodsNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "GoodsId")]
    public int GoodsId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "GoodsRegisterId")]
    public string GoodsRegisterId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(3, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "BoughtNums")]
    public int BoughtNums
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(4, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "NextFlushTime")]
    public long NextFlushTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "Ntf")]
    public ProChangedGoodsNtf Ntf
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = true, Name = "Reward")]
    public ProOrderReward Reward
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
