﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.Protocol.ChatMessageNtf
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using ProtoBuf;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.Protocol
{
  [ProtoContract(Name = "ChatMessageNtf")]
  [Serializable]
  public class ChatMessageNtf : IExtensible
  {
    private int _ChannelId;
    private ProChatInfo _ChatInfo;
    private ProChatCententInfo _ChatCententInfo;
    private ProChatContentVoice _VoiceInfo;
    private ProChatVoiceSimpleInfo _VoiceSimpleInfo;
    private ProChatEnterRoomInfo _EnterRoomInfo;
    private string _DestGameUserId;
    private long _SendTime;
    private string _ChatGroupId;
    private bool _JustForSelf;
    private IExtension extensionObject;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_get_ChannelId;
    private static DelegateBridge __Hotfix_set_ChannelId;
    private static DelegateBridge __Hotfix_get_ChatInfo;
    private static DelegateBridge __Hotfix_set_ChatInfo;
    private static DelegateBridge __Hotfix_get_ChatCententInfo;
    private static DelegateBridge __Hotfix_set_ChatCententInfo;
    private static DelegateBridge __Hotfix_get_VoiceInfo;
    private static DelegateBridge __Hotfix_set_VoiceInfo;
    private static DelegateBridge __Hotfix_get_VoiceSimpleInfo;
    private static DelegateBridge __Hotfix_set_VoiceSimpleInfo;
    private static DelegateBridge __Hotfix_get_EnterRoomInfo;
    private static DelegateBridge __Hotfix_set_EnterRoomInfo;
    private static DelegateBridge __Hotfix_get_DestGameUserId;
    private static DelegateBridge __Hotfix_set_DestGameUserId;
    private static DelegateBridge __Hotfix_get_SendTime;
    private static DelegateBridge __Hotfix_set_SendTime;
    private static DelegateBridge __Hotfix_get_ChatGroupId;
    private static DelegateBridge __Hotfix_set_ChatGroupId;
    private static DelegateBridge __Hotfix_get_JustForSelf;
    private static DelegateBridge __Hotfix_set_JustForSelf;
    private static DelegateBridge __Hotfix_ProtoBuf\u002EIExtensible\u002EGetExtensionObject;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessageNtf()
    {
      // ISSUE: unable to decompile the method.
    }

    [ProtoMember(1, DataFormat = DataFormat.TwosComplement, IsRequired = true, Name = "ChannelId")]
    public int ChannelId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(2, DataFormat = DataFormat.Default, IsRequired = true, Name = "ChatInfo")]
    public ProChatInfo ChatInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(3, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatCententInfo")]
    public ProChatCententInfo ChatCententInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(4, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceInfo")]
    public ProChatContentVoice VoiceInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(null)]
    [ProtoMember(5, DataFormat = DataFormat.Default, IsRequired = false, Name = "VoiceSimpleInfo")]
    public ProChatVoiceSimpleInfo VoiceSimpleInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(6, DataFormat = DataFormat.Default, IsRequired = false, Name = "EnterRoomInfo")]
    [DefaultValue(null)]
    public ProChatEnterRoomInfo EnterRoomInfo
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(7, DataFormat = DataFormat.Default, IsRequired = false, Name = "DestGameUserId")]
    [DefaultValue("")]
    public string DestGameUserId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [ProtoMember(8, DataFormat = DataFormat.TwosComplement, IsRequired = false, Name = "SendTime")]
    [DefaultValue(0)]
    public long SendTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue("")]
    [ProtoMember(9, DataFormat = DataFormat.Default, IsRequired = false, Name = "ChatGroupId")]
    public string ChatGroupId
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [DefaultValue(false)]
    [ProtoMember(10, DataFormat = DataFormat.Default, IsRequired = false, Name = "JustForSelf")]
    public bool JustForSelf
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [MethodImpl((MethodImplOptions) 32768)]
    IExtension IExtensible.GetExtensionObject(bool createIfMissing)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
