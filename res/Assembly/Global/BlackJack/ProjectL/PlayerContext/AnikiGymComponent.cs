﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.AnikiGymComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class AnikiGymComponent : AnikiGymComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_GetCurrentTicketNums;
    private static DelegateBridge __Hotfix_FinishedAnikiGymLevel;
    private static DelegateBridge __Hotfix_SetSuccessAnikiGymLevelRaid;
    private static DelegateBridge __Hotfix_SetSuccessAnikiGymLevel;
    private static DelegateBridge __Hotfix_IsAnikiGymOpened;
    private static DelegateBridge __Hotfix_IsAnikiLevelOpened;
    private static DelegateBridge __Hotfix_GetMaxFinishedLevelId;
    private static DelegateBridge __Hotfix_RaidLevel;

    [MethodImpl((MethodImplOptions) 32768)]
    public AnikiGymComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSAnikiGymNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetCurrentTicketNums()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FinishedAnikiGymLevel(int levelId, bool isWin, List<int> battleTreasures)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessAnikiGymLevelRaid(
      ConfigDataAnikiLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetSuccessAnikiGymLevel(
      ConfigDataAnikiLevelInfo levelInfo,
      List<int> battleTreasures,
      List<int> heroes,
      bool isTeamBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnikiGymOpened(int anikiGymId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnikiLevelOpened(int anikiLevelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMaxFinishedLevelId(int chapterId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int RaidLevel(int levelId, int nums)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
