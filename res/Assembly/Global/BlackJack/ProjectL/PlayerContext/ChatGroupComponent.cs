﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatGroupComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class ChatGroupComponent : ChatGroupComponentCommon
  {
    public DateTime m_dataLastUpdateTime;
    private Dictionary<string, ChatGroupCompactInfo> m_currChatGroupDict;
    protected PlayerBasicInfoComponentCommon m_basicInfo;
    protected ChatComponent m_chatComponent;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_GetChatGroupSimpleInfo;
    private static DelegateBridge __Hotfix_NotifyChatGroupInfo;
    private static DelegateBridge __Hotfix_ChatGroupStateChangedNtf;
    private static DelegateBridge __Hotfix_get_DataLastUpdateTime;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatGroupCompactInfo GetChatGroupSimpleInfo(string groupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyChatGroupInfo(
      List<ProChatGroupCompactInfo> chatGroupInfoList,
      DateTime nowServerTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChatGroupStateChangedNtf(ProChatGroupInfo groupInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    public DateTime DataLastUpdateTime
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
