﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.DanmakuComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class DanmakuComponent : DanmakuComponentCommon
  {
    private List<PostDanmakuEntry> m_postDanmakuEntries;
    private LevelDanmaku m_levelDanmaku;
    private List<int> m_newSendDanmakuTurnList;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_SetLevelDanmaku;
    private static DelegateBridge __Hotfix_FilterSensitiveWords;
    private static DelegateBridge __Hotfix_BuildLevelDanmakuOrderByTime;
    private static DelegateBridge __Hotfix_GetLevelDanmaku;
    private static DelegateBridge __Hotfix_PostLevelDanmaku;
    private static DelegateBridge __Hotfix_AddPostDanmakuEntryToLocalLevelDanmaku;
    private static DelegateBridge __Hotfix_AddTurnDanmakuToLevelDanmaku;
    private static DelegateBridge __Hotfix_CreateDanmakuEntry;
    private static DelegateBridge __Hotfix_ClearLevelDanmaku;
    private static DelegateBridge __Hotfix_GetPostedLevelDanmaku;
    private static DelegateBridge __Hotfix_ClearNewSendDanmakuTurnList;

    [MethodImpl((MethodImplOptions) 32768)]
    public DanmakuComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLevelDanmaku(LevelDanmaku levelDanmaku)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void FilterSensitiveWords()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private LevelDanmaku BuildLevelDanmakuOrderByTime(LevelDanmaku source)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public LevelDanmaku GetLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PostLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddPostDanmakuEntryToLocalLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddTurnDanmakuToLevelDanmaku(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private DanmakuEntry CreateDanmakuEntry(PostDanmakuEntry entry)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<PostDanmakuEntry> GetPostedLevelDanmaku()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ClearNewSendDanmakuTurnList()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
