﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaSeasonInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaSeasonInfo
  {
    public int Season;
    public long Version;
    public long LiveRoomVersion;
    public List<PeakArenaPlayOffMatchupInfo> MatchupInfos;
    public Dictionary<int, PeakArenaLiveRoomInfo> liveRoomInfoDic;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_UpdateMatchupInfoGroupId;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaSeasonInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateMatchupInfoGroupId()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
