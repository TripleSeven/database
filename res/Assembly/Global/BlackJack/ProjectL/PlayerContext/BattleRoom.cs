﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.BattleRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class BattleRoom
  {
    public ulong RoomId;
    public ulong LiveRoomId;
    public BattleRoomType BattleRoomType;
    public int BattleId;
    public GameFunctionType GameFunctionType;
    public int LocationId;
    public BattleRoomStatus BattleRoomStatus;
    public BattleLiveRoomStatus BattleLiveRoomStatus;
    public DateTime ReadyTimeout;
    public DateTime LastPlayerBeginActionTime;
    public List<BattleRoomPlayer> Players;
    public BattleRoomPlayerHeroSetup BattleRoomPlayerHeroSetup;
    public int MyPlayerIndex;
    public int LeaderPlayerIndex;
    public List<BattleCommand> BattleCommands;
    public List<BattleRoomQuitNtf> PlayerQuitNtfs;
    public int BattleStars;
    public BattleReward BattleReward;
    public int PVPWinPlayerIndex;
    public RealTimePVPMode RealtimePVPBattleMode;
    public BattleRoomBPRule RealtimePVPBPRule;
    public int RealtimePVPBPTurn;
    public BattleRoomBPStatus RealtimePVPBPStatus;
    public DateTime RealtimePVPLatestTurnChangeDateTime;
    public ulong RealtimePVPBattleInstanceId;
    public List<int> GuildMassiveCombatPreferredHeroTagIds;
    public ulong GuildMassiveCombatInstanceId;
    public RealTimePVPMode PeakArenaMode;
    public int PeakArenaRound;
    public int PeakArenaBoRound;
    public int PeakArenaPlayOffMatchupId;
    public List<string> PeakArenaWinPlayerUserIds;
    public List<DateTime> PeakArenaBattleEndTimes;
    public BPStage PeakArenaBPStage;
    public BPStagePeakArenaRule PeakArenaBPStageRule;
    public DateTime PeakArenaLastTickBPStageCountdownTime;
    public long PeakArenaBattleTurnActionTimeSpan;
    public DateTime PeakArenaLastTickBattleActionCountdownTime;
    public ulong PeakArenaBattleInstanceId;
    public string LivePeakArenaBPStageHeros0UserId;
    public List<BPStageHero> LivePeakArenaBPStageHeros0;
    public List<BPStageHero> LivePeakArenaBPStageHeros1;
    public DateTime LivePeakArenaBoRoundStartTime;
    public string LivePeakArenaForfeitUserId;
    public TimeSpan LivePeakArenaBattleTurnActionTimeSpanFix;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_InitPlayers_1;
    private static DelegateBridge __Hotfix_InitPlayers_2;
    private static DelegateBridge __Hotfix_InitPlayers_0;
    private static DelegateBridge __Hotfix_InitPeakArenaBPStage;
    private static DelegateBridge __Hotfix_FindPlayerBySessionId;
    private static DelegateBridge __Hotfix_FindPlayerByUserId;
    private static DelegateBridge __Hotfix_SetMyPlayer;
    private static DelegateBridge __Hotfix_SetLeaderPlayer;
    private static DelegateBridge __Hotfix_FindPlayerIndexByUserId;
    private static DelegateBridge __Hotfix_FindPlayerIndexBySessiongId;
    private static DelegateBridge __Hotfix_GetMyPlayer;
    private static DelegateBridge __Hotfix_IsTeamOrGuildMassiveCombatRoomType;
    private static DelegateBridge __Hotfix_IsAnyPVPBattleRoomType;
    private static DelegateBridge __Hotfix_IsRealTimePVPBattleRoomType;
    private static DelegateBridge __Hotfix_IsPeakArenaBattleRoomType;
    private static DelegateBridge __Hotfix_IsPeakArenaPlayOffMatch;
    private static DelegateBridge __Hotfix_IsAnyPVPOrPeakArenaBattleRoomType;
    private static DelegateBridge __Hotfix_IsBattleRoomStatusBPStageOrReady;
    private static DelegateBridge __Hotfix_GetPlayerIndexInCurrentPickTurn;
    private static DelegateBridge __Hotfix_GetHeroSetupCountMaxInPickTurn;
    private static DelegateBridge __Hotfix_GetHeroSetupCountInCurrentPickTurn;
    private static DelegateBridge __Hotfix_GetRemainHeroSetupCountInCurrentPickTurn;
    private static DelegateBridge __Hotfix_GetHeroSetupFlagCount;
    private static DelegateBridge __Hotfix_IsHeroSetupHasFlag;
    private static DelegateBridge __Hotfix_TickPeakArenaBPStageCountdown_0;
    private static DelegateBridge __Hotfix_TickPeakArenaBPStageCountdown_1;
    private static DelegateBridge __Hotfix_TickPeakArenaBattleActionCountdown_0;
    private static DelegateBridge __Hotfix_TickPeakArenaBattleActionCountdown_1;
    private static DelegateBridge __Hotfix_SetPeakArenaWinPlayerId;
    private static DelegateBridge __Hotfix_IsLive;

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<ProBattleRoomPlayer> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<ProUserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPlayers(List<UserSummary> players)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitPeakArenaBPStage(
      IConfigDataLoader configDataLoader,
      int randomSeed,
      BPStage bpStage = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer FindPlayerBySessionId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer FindPlayerByUserId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetMyPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetLeaderPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindPlayerIndexByUserId(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int FindPlayerIndexBySessiongId(ulong sessionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleRoomPlayer GetMyPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsTeamOrGuildMassiveCombatRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyPVPBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRealTimePVPBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPeakArenaPlayOffMatch()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAnyPVPOrPeakArenaBattleRoomType()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsBattleRoomStatusBPStageOrReady()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPlayerIndexInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupCountMaxInPickTurn(int turn)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupCountInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRemainHeroSetupCountInCurrentPickTurn()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetHeroSetupFlagCount(int playerIndex, SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHeroSetupHasFlag(int playerIndex, int heroId, SetupBattleHeroFlag flag)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBPStageCountdown()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBPStageCountdown(TimeSpan ts)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBattleActionCountdown(int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void TickPeakArenaBattleActionCountdown(TimeSpan ts, int playerIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPeakArenaWinPlayerId(int boRound, string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLive()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
