﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.PeakArenaPlayOffMatchupInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class PeakArenaPlayOffMatchupInfo
  {
    public int Round;
    public int MatchupId;
    public ulong RoomId;
    public int GroupId;
    public PeakArenaPlayOffPlayerInfo PlayerOnLeft;
    public PeakArenaPlayOffPlayerInfo PlayerOnRight;
    public string WinnerId;
    public List<PeakArenaPlayOffBattleInfo> BattleInfos;
    public ulong BattleReportId;
    public int NextMatchupId;
    public int PrevLeftMatchupId;
    public int PrevRightMatchupId;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_GetMatchupSate;
    private static DelegateBridge __Hotfix_GetMatchResultByUserId;
    private static DelegateBridge __Hotfix_GetLeftUserMatchResult;
    private static DelegateBridge __Hotfix_GetRightUserMatchResult;
    private static DelegateBridge __Hotfix_HasLeftPlayer;
    private static DelegateBridge __Hotfix_HasRightPlayer;
    private static DelegateBridge __Hotfix_IsLeftPlayer;
    private static DelegateBridge __Hotfix_GetLeftUserId;
    private static DelegateBridge __Hotfix_IsRightPlayer;
    private static DelegateBridge __Hotfix_GetRightUserId;
    private static DelegateBridge __Hotfix_IsPlayerInThisMatch;
    private static DelegateBridge __Hotfix_GetPlayerByID;
    private static DelegateBridge __Hotfix_GatBattleCount;
    private static DelegateBridge __Hotfix_GetLeftPlayerBattleScore;
    private static DelegateBridge __Hotfix_GetRightPlayerBattleScore;

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.MatchState GetMatchupSate()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult GetMatchResultByUserId(
      string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult GetLeftUserMatchResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffMatchupInfo.PlayerMatchResult GetRightUserMatchResult()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasLeftPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool HasRightPlayer()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsLeftPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetLeftUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRightPlayer(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetRightUserId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsPlayerInThisMatch(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public PeakArenaPlayOffPlayerInfo GetPlayerByID(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GatBattleCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLeftPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetRightPlayerBattleScore()
    {
      // ISSUE: unable to decompile the method.
    }

    public enum MatchState
    {
      None,
      WaitMatch,
      InMatch,
      FinishMatch,
    }

    public enum PlayerMatchResult
    {
      None,
      Win,
      Lose,
      Abstention,
    }
  }
}
