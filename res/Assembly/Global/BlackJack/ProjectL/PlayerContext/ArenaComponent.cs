﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ArenaComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class ArenaComponent : ArenaComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_GetArenaTicketNextGivenTime;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_AddArenaBattleReportPlayBackData;
    private static DelegateBridge __Hotfix_StartArenaBattle;
    private static DelegateBridge __Hotfix_ReconnectArenaBattle;
    private static DelegateBridge __Hotfix_RevengeOpponent;
    private static DelegateBridge __Hotfix_OnStartArenaBattle;
    private static DelegateBridge __Hotfix_SetDefensiveTeam;
    private static DelegateBridge __Hotfix_FinsihArenaBattle;
    private static DelegateBridge __Hotfix_FlushOpponents;
    private static DelegateBridge __Hotfix_InitArenaPlayInfo;
    private static DelegateBridge __Hotfix_InitBattleReportBasicInfo;
    private static DelegateBridge __Hotfix_GainVictoryPointsReward;
    private static DelegateBridge __Hotfix_GetArenaPlayerInfo;
    private static DelegateBridge __Hotfix_GetArenaBattleReport;

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Tick(uint deltaMillisecond)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public DateTime GetArenaTicketNextGivenTime()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSArenaNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int AddArenaBattleReportPlayBackData(ProArenaBattleReport pbArenaBattleReport)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void StartArenaBattle(ProArenaDefensiveBattleInfo pbBattleInfo, bool autoBattle)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReconnectArenaBattle(ProArenaDefensiveBattleInfo pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RevengeOpponent(
      ulong battleReportInstanceId,
      bool autoBattle,
      ProArenaDefensiveBattleInfo pbBattleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void OnStartArenaBattle(ArenaOpponentDefensiveBattleInfo battleInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetDefensiveTeam(ProArenaPlayerDefensiveTeam pbDefensiveTeamInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FinsihArenaBattle(bool isWin)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void FlushOpponents(List<ProArenaOpponent> pbOpponents, long nextFlushTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitArenaPlayInfo(ProArenaPlayerInfo pbArenaPlayerInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleReportBasicInfo(
      List<ProArenaBattleReport> pbArenaBattleReports,
      int nextBattleReportIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void GainVictoryPointsReward(int victoryPointsRewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaPlayerInfo GetArenaPlayerInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ArenaBattleReport GetArenaBattleReport(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
