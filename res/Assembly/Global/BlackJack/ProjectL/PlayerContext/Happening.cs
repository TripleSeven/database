﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.Happening
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class Happening
  {
    public HappeningStep Step;
    public ConfigDataScenarioInfo ScenarioInfo;
    public ConfigDataEventInfo EventInfo;
    public ConfigDataRiftLevelInfo RiftLevelInfo;
    public ConfigDataHeroDungeonLevelInfo HeroDungeonLevelInfo;
    public ConfigDataHeroPhantomLevelInfo HeroPhantomLevelInfo;
    public ConfigDataAnikiLevelInfo AnikiLevelInfo;
    public ConfigDataThearchyTrialLevelInfo ThearchyLevelInfo;
    public ConfigDataTreasureLevelInfo TreasureLevelInfo;
    public ConfigDataMemoryCorridorLevelInfo MemoryCorridorLevelInfo;
    public ConfigDataHeroTrainningLevelInfo HeroTrainningLevelInfo;
    public ConfigDataCooperateBattleLevelInfo CooperateBattleLevelInfo;
    public ConfigDataScoreLevelInfo UnchartedScoreLevelInfo;
    public ConfigDataChallengeLevelInfo UnchartedChallengeLevelInfo;
    public ConfigDataTowerFloorInfo TowerFloorInfo;
    public ConfigDataEternalShrineLevelInfo EternalShrineLevelInfo;
    public ConfigDataHeroAnthemLevelInfo HeroAnthemLevelInfo;
    public ConfigDataAncientCallBossInfo AncientCallBossLevelInfo;
    public ConfigDataCollectionActivityScenarioLevelInfo CollectionActivityScenarioLevelInfo;
    public ConfigDataCollectionActivityChallengeLevelInfo CollectionActivityChallengeLevelInfo;
    public ConfigDataCollectionActivityLootLevelInfo CollectionActivityLootLevelInfo;
    public ConfigDataCollectionEventInfo CollectionActivityEventInfo;
    public ConfigDataGuildMassiveCombatLevelInfo GuildMassiveCombatLevelInfo;
    public ulong GuildMassiveCombatInstanceId;
    public ConfigDataWaypointInfo WaypointInfo;
    public ConfigDataWaypointInfo PrevWaypointInfo;
    public ConfigDataDialogInfo DialogInfoBefore;
    public ConfigDataDialogInfo DialogInfoAfter;
    public ConfigDataBattleInfo BattleInfo;
    public int MonsterLevel;
    public BattleType BattleType;
    public Action OnStepEnd;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_InitBattleAndDialog;
    private static DelegateBridge __Hotfix_NextStep;
    private static DelegateBridge __Hotfix_StepEnd;
    private static DelegateBridge __Hotfix_Clear;
    private static DelegateBridge __Hotfix_IsLevelInfoNull;
    private static DelegateBridge __Hotfix_GetGameFunctionTypeAndLocationId;
    private static DelegateBridge __Hotfix_GetStrategy;
    private static DelegateBridge __Hotfix_GetLevelId;
    private static DelegateBridge __Hotfix_GetStarCondition;
    private static DelegateBridge __Hotfix_GetBattleLevelAchievements;
    private static DelegateBridge __Hotfix_GetBattleLevelAchievementRelatedInfos;

    [MethodImpl((MethodImplOptions) 32768)]
    public Happening()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void InitBattleAndDialog(
      BattleType battleType,
      ConfigDataBattleInfo battleInfo,
      int monsterLevel,
      ConfigDataDialogInfo dialogBefore = null,
      ConfigDataDialogInfo dialogAfter = null)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NextStep()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void StepEnd()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Clear()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private bool IsLevelInfoNull()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetGameFunctionTypeAndLocationId(
      out GameFunctionType gameFunctionType,
      out int locationId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetStrategy()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetLevelId()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool GetStarCondition(out int starTurnMax, out int starDeadMax)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public BattleLevelAchievement[] GetBattleLevelAchievements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ConfigDataBattleAchievementRelatedInfo> GetBattleLevelAchievementRelatedInfos()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
