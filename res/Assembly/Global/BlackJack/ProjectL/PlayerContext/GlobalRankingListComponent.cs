﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.GlobalRankingListComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class GlobalRankingListComponent : GlobalRankingListComponentCommon
  {
    protected Dictionary<RankingListType, GlobalRankingListComponent.CachedRankingListInfo> m_cachedRankingListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_cachedSingleHeroRankListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_cachedSingleAncientCallBossRankListDict;
    protected Dictionary<int, GlobalRankingListComponent.CachedRankingListInfo> m_friendAndGuildSingleAncientCallBossRankListDict;
    protected const float OutDateTime = 15f;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_AddRankingListInfoToCache;
    private static DelegateBridge __Hotfix_GetRankingListInfoByType;
    private static DelegateBridge __Hotfix_IsRankingListInfoValid;
    private static DelegateBridge __Hotfix_CheckRankingListInfoQueryCare4AncientCall;
    private static DelegateBridge __Hotfix_AddSingleHeroRankingListInfoToCache;
    private static DelegateBridge __Hotfix_AddSingleAncientCallBossRankingListInfoToCache;
    private static DelegateBridge __Hotfix_AddFriendAndGuildSingleAncientCallBossRankingListInfoToCache;
    private static DelegateBridge __Hotfix_GetSingleBossRankingListInfoByBossId;
    private static DelegateBridge __Hotfix_GetFriendAndGuildSingleBossRankingListInfoByBossId;
    private static DelegateBridge __Hotfix_GetSingleHeroRankingListInfoByHeroId;
    private static DelegateBridge __Hotfix_IsAbleQueryRankingListInfo;
    private static DelegateBridge __Hotfix_IsSingleHeroRankingListInfoValid;

    [MethodImpl((MethodImplOptions) 32768)]
    public GlobalRankingListComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddRankingListInfoToCache(
      RankingListType rankingType,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetRankingListInfoByType(RankingListType rankingType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsRankingListInfoValid(RankingListType rankingListType)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public new bool CheckRankingListInfoQueryCare4AncientCall(
      RankingListType rankingListType,
      int bossId,
      out int errCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSingleHeroRankingListInfoToCache(int heroId, RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddSingleAncientCallBossRankingListInfoToCache(
      int bossId,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddFriendAndGuildSingleAncientCallBossRankingListInfoToCache(
      int bossId,
      RankingListInfo rankingListInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleBossRankingListInfoByBossId(int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetFriendAndGuildSingleBossRankingListInfoByBossId(
      int bossId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public RankingListInfo GetSingleHeroRankingListInfoByHeroId(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsAbleQueryRankingListInfo(RankingListType rankingListType, out int errorCode)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsSingleHeroRankingListInfoValid(int heroId)
    {
      // ISSUE: unable to decompile the method.
    }

    public class CachedRankingListInfo
    {
      public DateTime m_outDateTime;
      public RankingListInfo m_rankingListInfo;
      private static DelegateBridge _c__Hotfix_ctor;

      [MethodImpl((MethodImplOptions) 32768)]
      public CachedRankingListInfo()
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
