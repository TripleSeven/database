﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.MissionComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class MissionComponent : MissionComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_AddMissionById;
    private static DelegateBridge __Hotfix_CanUnlockMissionInLogicFromInit;
    private static DelegateBridge __Hotfix_CanUnlockMissionInUIInterface;
    private static DelegateBridge __Hotfix_GetMissionReward;
    private static DelegateBridge __Hotfix_GetAllProcessingMissionList;
    private static DelegateBridge __Hotfix_GetAllCompletedMissionList;
    private static DelegateBridge __Hotfix_GetAllFinishedMissionList;

    [MethodImpl((MethodImplOptions) 32768)]
    public MissionComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSMissionNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddMissionById(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected override bool CanUnlockMissionInLogicFromInit(ConfigDataMissionInfo missionInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    protected bool CanUnlockMissionInUIInterface(Mission mission)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetMissionReward(int missionId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllProcessingMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllCompletedMissionList()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Mission> GetAllFinishedMissionList()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
