﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.TeamComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class TeamComponent : TeamComponentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_CreateTeam;
    private static DelegateBridge __Hotfix_AutoMatchTeamRoom;
    private static DelegateBridge __Hotfix_ChangeTeamRoomAuthority;
    private static DelegateBridge __Hotfix_JoinTeamRoom;
    private static DelegateBridge __Hotfix_QuitTeamRoom;
    private static DelegateBridge __Hotfix_PlayerJoinTeamRoom;
    private static DelegateBridge __Hotfix_PlayerQuitTeamRoom;
    private static DelegateBridge __Hotfix_LeaveWaitingListAndJoinRoom;
    private static DelegateBridge __Hotfix_DeductTeamPveBattleEnergyByClient;
    private static DelegateBridge __Hotfix_get_Room;
    private static DelegateBridge __Hotfix_set_Room;
    private static DelegateBridge __Hotfix_get_IsTeamRoomInviteAgain;
    private static DelegateBridge __Hotfix_set_IsTeamRoomInviteAgain;

    [MethodImpl((MethodImplOptions) 32768)]
    public TeamComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSTeamNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void CreateTeam(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AutoMatchTeamRoom(
      GameFunctionType gameFunctionTypeId,
      int locationId,
      TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ChangeTeamRoomAuthority(TeamRoomAuthority authority)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void JoinTeamRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void QuitTeamRoom()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerJoinTeamRoom(TeamRoomPlayer player)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void PlayerQuitTeamRoom(
      TeamRoomPlayer quitPlayer,
      ulong leaderSessionId,
      long leaderKickOutTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LeaveWaitingListAndJoinRoom(TeamRoom room)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeductTeamPveBattleEnergyByClient()
    {
      // ISSUE: unable to decompile the method.
    }

    public TeamRoom Room
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool IsTeamRoomInviteAgain
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }
  }
}
