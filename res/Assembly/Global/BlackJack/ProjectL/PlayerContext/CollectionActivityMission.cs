﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.CollectionActivityMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ConfigData;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class CollectionActivityMission
  {
    public ulong activityUid;
    public int exchangeRid;
    public List<Goods> rewardList;
    public List<CollectionActivityItem> itemList;
    public int curCount;
    public int maxCount;
    public bool canExchange;
    public bool isExchangeMax;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_UpdateState;
    private static DelegateBridge __Hotfix_OnSort;

    [MethodImpl((MethodImplOptions) 32768)]
    public CollectionActivityMission()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateState()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public static int OnSort(CollectionActivityMission x, CollectionActivityMission y)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
