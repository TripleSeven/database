﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.OperationalActivityCompoment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class OperationalActivityCompoment : OperationalActivityCompomentCommon
  {
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_ExchangeItemGroup;
    private static DelegateBridge __Hotfix_GainReward;
    private static DelegateBridge __Hotfix_Deserialize_2;
    private static DelegateBridge __Hotfix_Deserialize_1;
    private static DelegateBridge __Hotfix_Deserialize_0;
    private static DelegateBridge __Hotfix_PostDeSerialize;
    private static DelegateBridge __Hotfix_GetWebInfo;
    private static DelegateBridge __Hotfix_ResetAllEffectFlag;
    private static DelegateBridge __Hotfix_Deserialize_3;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_GetAnnouncementDSVersion;
    private static DelegateBridge __Hotfix_AddNewOperationalActivity;
    private static DelegateBridge __Hotfix_AddRedeemActivity;
    private static DelegateBridge __Hotfix_AddFansRewardFromPBTCBTActivity;
    private static DelegateBridge __Hotfix_AddAnnouncement;
    private static DelegateBridge __Hotfix_UpdateOperationalActivity;
    private static DelegateBridge __Hotfix_GetValidAnnouncements;
    private static DelegateBridge __Hotfix_RemoveGlobalAnnouncement;
    private static DelegateBridge __Hotfix_RemoveAllExpiredAnnouncements;

    [MethodImpl((MethodImplOptions) 32768)]
    public OperationalActivityCompoment()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int ExchangeItemGroup(ulong operationalActivityInstanceId, int itemGroupIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GainReward(ulong operationalActivityInstanceId, int rewardIndex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSOperationalActivityNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(DSAnnouncementNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(AdvertisementFlowLayoutUpdateNtf ntf)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostDeSerialize()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public WebInfoControl GetWebInfo()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ResetAllEffectFlag()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Deserialize(WebControlInfoUpdateNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetAnnouncementDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddNewOperationalActivity(
      ProOperationalActivityBasicInfo operationalActivityBasicInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddRedeemActivity(int activityId, RedeemInfoAck info)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool AddFansRewardFromPBTCBTActivity(
      int activityID,
      long startDate,
      long endDate,
      bool isClaimed)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddAnnouncement(ProAnnouncement pbAnnouncement)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void UpdateOperationalActivity(
      ProOperationalActivityBasicInfo opertioanlActivityInfo)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<Announcement> GetValidAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveGlobalAnnouncement(ulong instanceId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void RemoveAllExpiredAnnouncements()
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
