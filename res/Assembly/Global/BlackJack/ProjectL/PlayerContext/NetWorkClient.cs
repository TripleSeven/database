﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.NetWorkClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.BJFramework.Runtime.PlayerContext;
using BlackJack.ProjectL.LibClient;
using IL;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class NetWorkClient : IPlayerContextNetworkClient
  {
    private IClient m_client;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_LoginByAuthToken;
    private static DelegateBridge __Hotfix_LoginBySessionToken;
    private static DelegateBridge __Hotfix_Disconnect;
    private static DelegateBridge __Hotfix_SendMessage;
    private static DelegateBridge __Hotfix_Tick;
    private static DelegateBridge __Hotfix_Close;
    private static DelegateBridge __Hotfix_BlockProcessMsg;

    [MethodImpl((MethodImplOptions) 32768)]
    public NetWorkClient(IClient realClient)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginByAuthToken(
      string serverAddress,
      int serverPort,
      string authToken,
      string clientVersion,
      string clientDeviceId,
      string localization,
      int loginChannelId,
      int bornChannelId,
      string serverDomain)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool LoginBySessionToken(
      string sessionToken,
      string clientVersion,
      string localization,
      int loginChannelId,
      int bornChannelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool Disconnect()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool SendMessage(object msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Tick()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void Close()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void BlockProcessMsg(bool isBlock)
    {
      // ISSUE: unable to decompile the method.
    }
  }
}
