﻿// Decompiled with JetBrains decompiler
// Type: BlackJack.ProjectL.PlayerContext.ChatComponent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 8E3000C9-CACF-4D37-8556-C63315993543
// Assembly location: D:\Games\Langrisser\client\Langrisser_Data\Managed\Assembly-CSharp.dll

using BlackJack.ProjectL.Common;
using BlackJack.ProjectL.Protocol;
using IL;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace BlackJack.ProjectL.PlayerContext
{
  public class ChatComponent : ChatComponentCommon
  {
    private static int s_chatRecentLinkManMaxCount = 10;
    private static int s_chatRecentLinkChatGroupCount = 10;
    public int m_loadChatHistoryMessageCount;
    public List<ChatComponent.ChatMessageClient> m_worldChatMsgList;
    public List<ChatComponent.ChatMessageClient> m_sysChatMsgList;
    public List<ChatComponent.ChatMessageClient> m_teamChatMsgList;
    public List<ChatComponent.ChatMessageClient> m_guildChatMsgList;
    public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_groupChatMsgDict;
    public List<string> m_recentLinkChatGroupIdList;
    public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_privateChatMsgDict;
    public List<string> m_recentPrivateChatPlayerIdList;
    public Dictionary<string, UserSummary> m_recentLinkPlayerInfoDict;
    public Dictionary<string, bool> m_privateChatHistoryStateList;
    public Dictionary<string, bool> m_groupChatHistoryStateList;
    public string m_currGroupChatGroupId;
    public string m_currPrivateChatPlayerGameUserId;
    public ChatGroupComponent m_chatGroupComponent;
    public FriendComponent m_friendComponent;
    private BagComponent m_bagComponent;
    public DateTime m_guildChatLastReadTime;
    public int m_currRoomIndex;
    private bool m_dataDirtyState;
    private bool m_isLoadHistoryRecordSuccess;
    private string m_chatDirectoryPath;
    private string s_chatDataSavePath;
    protected static int s_chatHistoryRecordTimeLimit;
    protected static int s_chatHistoryRecordCountLimit;
    protected static int s_chatSaveHistoryRecordCountOnClear;
    private static DelegateBridge _c__Hotfix_ctor;
    private static DelegateBridge __Hotfix_Init;
    private static DelegateBridge __Hotfix_PostInit;
    private static DelegateBridge __Hotfix_DeInit;
    private static DelegateBridge __Hotfix_DeSerialize;
    private static DelegateBridge __Hotfix_ReadChatBeforeDate;
    private static DelegateBridge __Hotfix_CanSendChatMessage;
    private static DelegateBridge __Hotfix_CanSendDanmaku;
    private static DelegateBridge __Hotfix_NotifyGetPlayerInfo;
    private static DelegateBridge __Hotfix_NotifyChatMessage;
    private static DelegateBridge __Hotfix_CreateASystemTip;
    private static DelegateBridge __Hotfix_IsHasHistoryRecord4Chat;
    private static DelegateBridge __Hotfix_LoadChatHistoryData;
    private static DelegateBridge __Hotfix_SaveChatHistoryData;
    private static DelegateBridge __Hotfix_GetSaveFileName;
    private static DelegateBridge __Hotfix_GetGroupChatTarget;
    private static DelegateBridge __Hotfix_SetGroupChatTarget;
    private static DelegateBridge __Hotfix_SetPrivateChatTarget;
    private static DelegateBridge __Hotfix_GetUserInfo;
    private static DelegateBridge __Hotfix_AddUserInfo;
    private static DelegateBridge __Hotfix_NotifyEnterNewRoom;
    private static DelegateBridge __Hotfix_GetDSVersion;
    private static DelegateBridge __Hotfix_GetChatMessageList;
    private static DelegateBridge __Hotfix_GetUnReadChatMsgCount4PointPlayerOrGroup;
    private static DelegateBridge __Hotfix_GetRecentChatTargetList;
    private static DelegateBridge __Hotfix_GetAllUnReadChatMsgCount;
    private static DelegateBridge __Hotfix_GetGroupUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetAssignGroupUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetAssignPrivateUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetPrivateUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_GetGuildUnreadChatMsgCount;
    private static DelegateBridge __Hotfix_ReadChat;
    private static DelegateBridge __Hotfix_SetRecentTeamMsgRead;
    private static DelegateBridge __Hotfix_SetGroupChatMsgReadByID;
    private static DelegateBridge __Hotfix_SetPrivateChatMsgReadByID;
    private static DelegateBridge __Hotfix_ClearChatMessage4LimitCondition;
    private static DelegateBridge __Hotfix_AddMsgToGroupChatDict;
    private static DelegateBridge __Hotfix_AddMsgToPrivateChatDict;
    private static DelegateBridge __Hotfix_NotifySystemChatMessage;
    private static DelegateBridge __Hotfix_CombineSystemSelectHeroMessage;
    private static DelegateBridge __Hotfix_CombineSystemSelectEquipmentMessage;
    private static DelegateBridge __Hotfix_NotifyWorldChatMessage;
    private static DelegateBridge __Hotfix_NotifyTeamChatMessage;
    private static DelegateBridge __Hotfix_NotifyGroupChatMessage;
    private static DelegateBridge __Hotfix_NotifyPrivateChatMessage;
    private static DelegateBridge __Hotfix_NotifyGuildChatMessage;
    private static DelegateBridge __Hotfix_get_RoomIndex;
    private static DelegateBridge __Hotfix_get_DataDirty;
    private static DelegateBridge __Hotfix_set_DataDirty;

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void Init()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void PostInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override void DeInit()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void DeSerialize(DSChatNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadChatBeforeDate(ChatChannel type, DateTime date)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public override int CanSendChatMessage(int channelId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int CanSendDanmaku(DateTime lastDanmakuSendTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyGetPlayerInfo(List<ProUserSummary> userInfoList)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatMessage NotifyChatMessage(ChatMessageNtf msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ChatComponent.ChatMessageClient CreateASystemTip(
      ChatChannel channel,
      string tips)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public bool IsHasHistoryRecord4Chat(ChatChannel channel, string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void LoadChatHistoryData(DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SaveChatHistoryData()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetSaveFileName(string userId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public string GetGroupChatTarget()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupChatTarget(string newGroupId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrivateChatTarget(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public UserSummary GetUserInfo(string gameUserId)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void AddUserInfo(UserSummary userSummary)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void NotifyEnterNewRoom(int roomindex)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public ushort GetDSVersion()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public List<ChatComponent.ChatMessageClient> GetChatMessageList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetUnReadChatMsgCount4PointPlayerOrGroup(ChatChannel channel, string id)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public KeyValuePair<List<string>, List<string>> GetRecentChatTargetList(
      ChatChannel channel)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAllUnReadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGroupUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAssignGroupUnreadChatMsgCount(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetAssignPrivateUnreadChatMsgCount(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetPrivateUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public int GetGuildUnreadChatMsgCount()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void ReadChat(ChatComponent.ChatMessageClient msg)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetRecentTeamMsgRead()
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetGroupChatMsgReadByID(string groupID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    public void SetPrivateChatMsgReadByID(string userID)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void ClearChatMessage4LimitCondition(DateTime currTime)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMsgToGroupChatDict(ChatComponent.ChatMessageClient chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private void AddMsgToPrivateChatDict(ChatComponent.ChatMessageClient chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifySystemChatMessage(ChatTextMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CombineSystemSelectHeroMessage(int contentId, string sourceContent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private string CombineSystemSelectEquipmentMessage(int contentId, string sourceContent)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifyWorldChatMessage(ChatMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifyTeamChatMessage(ChatMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifyGroupChatMessage(ChatMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifyPrivateChatMessage(ChatMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    [MethodImpl((MethodImplOptions) 32768)]
    private ChatMessage NotifyGuildChatMessage(ChatMessage chatMessage)
    {
      // ISSUE: unable to decompile the method.
    }

    public int RoomIndex
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
    }

    public bool DataDirty
    {
      [MethodImpl((MethodImplOptions) 32768)] get
      {
        // ISSUE: unable to decompile the method.
      }
      [MethodImpl((MethodImplOptions) 32768)] set
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [Serializable]
    public class ChatMessageClient
    {
      public ChatMessage ChatMessageInfo;
      public bool isRead;
      public bool isHistoryRecord;
      public bool isLocalSystemTip;
      public string wordsSourceLanguage;
      public string wordsAfterTranslate;
      public bool isWordTextTransLatedState;
      private static DelegateBridge _c__Hotfix_ctor;
      private static DelegateBridge __Hotfix_GetWordsSrcLanguage;

      [MethodImpl((MethodImplOptions) 32768)]
      public ChatMessageClient(ChatMessage msgInfo)
      {
        // ISSUE: unable to decompile the method.
      }

      [MethodImpl((MethodImplOptions) 32768)]
      private void GetWordsSrcLanguage()
      {
        // ISSUE: unable to decompile the method.
      }
    }

    [Serializable]
    public class ChatSaveData
    {
      public List<string> m_recentLinkManIdList = new List<string>();
      public List<string> m_recentLinkGroupIdList = new List<string>();
      public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_privateChatDataDict = new Dictionary<string, List<ChatComponent.ChatMessageClient>>();
      public Dictionary<string, List<ChatComponent.ChatMessageClient>> m_groupChatDataDict = new Dictionary<string, List<ChatComponent.ChatMessageClient>>();
      public string m_userId;
      public DateTime m_guildChatLastReadTime;
      private static DelegateBridge _c__Hotfix_ctor;

      public ChatSaveData()
      {
        ChatComponent.ChatSaveData._c__Hotfix_ctor?.__Gen_Delegate_Imp4((object) this);
      }
    }
  }
}
